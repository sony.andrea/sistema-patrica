  ---------------------DATOS PARA INSERTAR--------------------------
  -----DA------
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (1,'CONCEJO MUNICIPAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (2,'DESPACHO ALCALDE MUNICIPAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (3,'DIRECCIÓN GENERAL DEL SISTEMA INTEGRADO DE TRANSPORTE',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (4,'SECRETARÍA EJECUTIVA MUNICIPAL',2018);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (5,'SERVICIOS Y GASTOS INSTITUCIONALES',2018);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (6,'SECRETARÍA MUNICIPAL DE EDUCACIÓN Y CULTURA CIUDADANA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (7,'SECRETARÍA MUNICIPAL DE DESARROLLO SOCIAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (9,'SECRETARÍA MUNICIPAL DE SALUD INTEGRAL Y DEPORTES',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (10,'SECRETARÍA MUNICIPAL DE INFRAESTRUCTURA PÚBLICA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (11,'PROGRAMA BARRIOS Y COMUNIDADES DE VERDAD',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (12,'PROGRAMA CENTRALIDADES URBANAS',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (13,'SECRETARÍA MUNICIPAL DE CONTROL Y CALIDAD DE OBRAS',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (14,'SECRETARÍA MUNICIPAL DE PLANIFICACIÓN PARA EL DESARROLLO',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (15,'SECRETARÍA MUNICIPAL DE FINANZAS',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (16,'CCRÉDITO PÚBLICO',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (17,'TESORO MUNICIPAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (18,'SECRETARÍA MUNICIPAL DE DESARROLLO ECONÓMICO',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (19,'SECRETARÍA MUNICIPAL DE CULTURAS',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (20,'SECRETARÍA MUNICIPAL DE GESTIÓN INTEGRAL DE RIESGOS',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (21,'PROGRAMA DE DRENAJE PLUVIAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (22,'SECRETARÍA MUNICIPAL DE SEGURIDAD CIUDADANA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (23,'SECRETARÍA MUNICIPAL DE MOVILIDAD',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (24,'SECRETARÍA MUNICIPAL DE GESTIÓN AMBIENTAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (25,'SERVICIO DE TRANSPORTE MUNICIPAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (26,'HOSPITAL LA PAZ',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (27,'HOSPITAL MUNICIPAL LOS PINOS',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (28,'HOSPITAL MUNICIPAL LA MERCED',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (30,'ADMINISTRACIÓN TRIBUTARIA MUNICIPAL',2018);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (31,'CEMENTERIO GENERAL',2018);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (32,'ZOOLÓGICO MUNICIPAL VESTY PAKOS',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (33,'TERMINAL DE BUSES',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (34,'CENTRO MUNICIPAL DE FAENO',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (35,'SISTEMA DE REGULACIÓN Y SUPERVISIÓN MUNICIPAL',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (36,'ESCUELA DE GESTORES MUNICIPALES',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (37,'SUBALCALDÍA I COTAHUMA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (38,'SUBALCALDÍA II MAXIMILIANO PAREDES',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (39,'SUBALCALDÍA III PERIFÉRICA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (40,'SUBALCALDÍA IV SAN ANTONIO',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (41,'SUBALCALDÍA V SUR',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (42,'SUBALCALDÍA VI MALLASA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (43,'SUBALCALDÍA VII CENTRO',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (44,'SUBALCALDÍA VIII HAMPATURI',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (45,'SUBALCALDÍA IX ZONGO',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (46,'HOSPITAL MUNICIPAL COTAHUMA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (47,'HOSPITAL MUNICIPAL LA PORTADA',2018,1);
INSERT INTO presupuesto.direccion_administrativa(da_codigo,da_descripcion,da_gestion,da_usr_id) VALUES (48,'AUTORIDAD DE FISCALIZACIÓN TERRITORIAL',2018,1);

---------UE------------
INSERT INTO presupuesto.unidad_ejecutora(ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (1, 'ADMINISTRACIÓN CONCEJO MUNICIPAL', 1,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion)VALUES (2, 'ADMINISTRACIÓN DESPACHO ALCALDE MUNICIPAL', 2, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (3, 'DIRECCIÓN DE GABINETE', 2, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (4, 'AUDITORÍA INTERNA', 2,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (5, 'DIRECCIÓN GENERAL DE ASUNTOS JURÍDICOS', 2, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (6, 'DIRECCIÓN DE TRANSPARENCIA Y LUCHA CONTRA LA CORRUPCIÓN', 2, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (7, 'DIRECCIÓN DE COORDINACIÓN DE POLÍTICAS DE IGUALDAD', 2,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (8, 'DIRECCIÓN MUNICIPAL DE GOBERNABILIDAD', 2, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (9, 'DIRECCIÓN DE COMUNICACIÓN SOCIAL', 2, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (10, 'DIRECCIÓN DE EMPRESAS, ENTIDADES Y SERVICIOS PÚBLICOS', 2,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (11, 'AGENCIA MUNICIPAL DE COOPERACIÓN', 2,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion)VALUES (12, 'AGENCIA MUNICIPAL PARA EL DESARROLLO TURÍSTICO LA PAZ MARAVILLOSA', 2, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (13, 'DESPACHO DIRECCIÓN GENERAL DEL SISTEMA INTEGRADO DE TRANSPORTE', 3, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion)VALUES (14, 'DIRECCIÓN DE PLANIFICACIÓN DEL SISTEMA INTEGRADO DE TRANSPORTE', 3, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (186, 'DIRECCIÓN DE GESTIÓN DE CALIDAD DEL SISTEMA INTEGRADO DE TRANSPORTE', 3, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (16, 'DESPACHO SECRETARIA EJECUTIVA MUNICIPAL', 4, 2018);


INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (17, 'DIRECCIÓN DE GOBIERNO ELECTRÓNICO Y MODERNIZACIÓN DE LA GESTIÓN', 4, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (18, 'DIRECCIÓN DE ADMINISTRACIÓN GENERAL', 4, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (19, 'DIRECCIÓN DE GESTIÓN DE RECURSOS HUMANOS', 4, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (20, 'DIRECCIÓN DE LICITACIONES Y CONTRATACIONES', 4, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (191, 'UNIDAD DE GESTORES MUNICIPALES', 4, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (21, 'ADMINISTRACIÓN SERVICIOS Y GASTOS INSTITUCIONALES', 5, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (22, 'DESPACHO SECRETARIA MUNICIPAL DE EDUCACIÓN Y CULTURA CIUDADANA', 6, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (23, 'DIRECCIÓN DE EDUCACIÓN', 6, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (24, 'DIRECCIÓN DE LA FELICIDAD Y FORMACIÓN CIUDADANA INTEGRAL', 6, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (25, 'DESPACHO SECRETARÍA MUNICIPAL DE DESARROLLO SOCIAL', 7, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (26, 'DIRECCIÓN DE ATENCIÓN SOCIAL INTEGRAL', 7, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (27, 'DIRECCIÓN DEFENSORÍA  MUNICIPAL', 7, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (29, 'DESPACHO SECRETARÍA MUNICIPAL DE SALUD INTEGRAL Y DEPORTES', 9, 2018;
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (30, 'DIRECCIÓN DE DEPORTES', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (31, 'DIRECCIÓN DE SALUD', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (32, 'CENTRO MATERNO INFANTIL VILLANUEVA POTOSÍ', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (33, 'CENTRO DE SALUD ALCOREZA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (34, 'CENTRO DE SALUD LA GRUTA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (35, 'CENTRO DE SALUD BAJO SAN PEDRO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (36, 'CENTRO DE SALUD 8 DE DICIEMBRE', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (37, 'CENTRO DE SALUD SAN LUIS', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (38, 'CENTRO DE SALUD EL ROSAL', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (39, 'CENTRO DE SALUD LLOJETA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (40, 'CENTRO DE SALUD PASANKERY', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (41, 'CENTRO DE SALUD BAJO TACAGUA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (42, 'CENTRO DE SALUD NIÑO KOLLO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (43, 'CENTRO DE SALUD ALTO TACAGUA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (44, 'CENTRO DE SALUD BIBLIOTECA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (45, 'CENTRO MATERNO INFANTIL TEMBLADERANI', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (46, 'CENTRO MATERNO INFANTIL EL TEJAR', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (47, 'CENTRO MATERNO INFANTIL LAPORTADA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (48, 'CENTRO DE SALUD ALTO MARISCAL SANTA CRUZ', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (49, 'CENTRO DE SALUD MUNAYPATA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (50, 'CENTRO DE SALUD CHAMOCO CHICO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (51, 'CENTRO DE SALUD OBISPO INDABURO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (52, 'CENTRO DE SALUD BAJO TEJAR', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (53, 'CENTRO DE SALUD VILLA VICTORIA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (54, 'CENTRO DE SALUD CIUDADELA FERROVIARIA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion)VALUES (55, 'CENTRO DE SALUD SAID', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (56, 'CENTRO DE SALUD PANTICIRCA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (57, 'CENTRO DE SALUD CAMSIQUE', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (58, 'CENTRO MATERNO INFANTIL ACHACHICALA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (59, 'CENTRO DE SALUD ASISTENCIA PÚBLICA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (60, 'CENTRO DE SALUD SAN JOSÉ DE NATIVIDAD', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (61, 'CENTRO DE SALUD VILLA FÁTIMA -LAS DELICIAS', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (62, 'CENTRO DE SALUD VINO TINTO', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (64, 'CENTRO DE SALUD CALVARIO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (65, 'CENTRO DE SALUD AGUA DE LA VIDA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (66, 'CENTRO DE SALUD ALTO MIRAFLORES', 9, 2018);


INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (67, 'CENTRO DE SALUD JUANCITO PINTO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (68, 'CENTRO DE SALUD SAN JUAN LAZARETO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (69, 'CENTRO DE SALUD PLAN AUTOPISTA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (70, 'CENTRO DE SALUD VILLA SALOMÉ', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (71, 'CENTRO DE SALUD VALLE HERMOSO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion)VALUES (72, 'CENTRO DE SALUD SAN ANTONIO BAJO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora  (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (73, 'CENTRO DE SALUD SAN ISIDRO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (74, 'CENTRO DE SALUD SAN ANTONIO ALTO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (75, 'CENTRO DE SALUD PAMPAHASI ALTO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (76, 'CENTRO DE SALUD KUPINI', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (77, 'CENTRO MATERNO INFANTIL PAMPAHASI BAJO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (78, 'CENTRO DE SALUD VILLA COPACABANA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (79, 'CENTRO MATERNO INFANTIL VILLA ARMONIA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (80, 'CENTRO DE SALUD CHOQUECHIHUANI', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (81, 'CENTRO DE SALUD ESCOBAR URIA', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (82, 'CENTRO DE SALUD ALTO IRPAVI', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion)VALUES (83, 'CENTRO DE SALUD BAJO LLOJETA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (84, 'CENTRO DE SALUD ACHUMANI', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (85, 'CENTRO DE SALUD ALTO SEGUENCOMA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (86, 'CENTRO DE SALUD BOLOGNIA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (87, 'CENTRO DE SALUD COTA COTA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (88, 'CENTRO DE SALUD OBRAJES', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (89, 'CENTRO DE SALUD C.O.F. OBRAJES', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (90, 'CENTRO DE SALUD MALLASA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (91, 'CENTRO DE SALUD CHASQUIPAMPA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (92, 'CENTRO MATERNO INFANTIL BELLA VISTA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (93, 'CENTRO DE SALUD MALLASILLA', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (94, 'CENTRO DE SALUD APUMALLA', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (95, 'CENTRO DE SALUD CHUQUIAGUILLO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (96, 'CENTRO DE SALUD ALTO OBRAJES', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (101, 'CENTRO DE SALUD 18 DE MAYO', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (182, 'CENTRO DE SALUD LAS DELICIAS CENTRAL', 9, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (183, 'CENTRO DE SALUD ZONGO CHORO', 9,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (102, 'DESPACHO SECRETARÍA MUNICIPAL DE INFRAESTRUCTURA PÚBLICA', 10, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (103, 'DIRECCIÓN DE PUENTES E INFRAESTRUCTURAS ESPECIALES', 10,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (104, 'DIRECCIÓN DE MANTENIMIENTO', 10,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (105, 'DIRECCIÓN DE ESTUDIOS DE PREINVERSIÓN', 10, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (106, 'ADMINISTRACIÓN PROGRAMA BARRIOS Y COMUNIDADES DE VERDAD', 11, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (107, 'ADMINISTRACIÓN PROGRAMA CENTRALIDADES URBANAS', 12, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (108, 'DESPACHO SECRETARÍA MUNICIPAL DE CONTROL Y CALIDAD DE OBRAS', 13, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (109, 'DIRECCIÓN DE FISCALIZACIÓN DE OBRAS Y SERVICIOS', 13, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (110, 'DIRECCIÓN DE SUPERVISIÓN DE OBRAS', 13, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (111, 'DESPACHO SECRETARÍA MUNICIPAL DE PLANIFICACIÓN PARA EL DESARROLLO', 14,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (112, 'DIRECCIÓN DE PLANIFICACIÓN ESTRATÉGICA', 14, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (113, 'DIRECCIÓN DE INVESTIGACIÓN E INFORMACIÓN MUNICIPAL', 14, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (114, 'DIRECCIÓN DE GESTIÓN POR RESULTADOS', 14, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (115, 'DIRECCIÓN DE ADMINISTRACIÓN TERRITORIAL Y CATASTRAL', 14,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (116, 'DESPACHO SECRETARÍA MUNICIPAL DE FINANZAS', 15, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (117, 'DIRECCIÓN DE GESTIÓN FINANCIERA', 15, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (118, 'DIRECCIÓN DE FINANCIAMIENTO Y ANÁLISIS FINANCIERO', 15, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (119, 'ADMINISTRACIÓN CRÉDITO PÚBLICO', 16, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (120, 'ADMINISTRACIÓN TESORO MUNICIPAL', 17,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (121, 'DESPACHO SECRETARÍA MUNICIPAL DE DESARROLLO ECONÓMICO', 18, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (122, 'DIRECCIÓN DE MERCADOS Y COMERCIO EN VÍAS PÚBLICAS', 18, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (123, 'DIRECCIÓN DE COMPETITIVIDAD Y EMPRENDIMIENTO', 18, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (124, 'DIRECCIÓN DE ACTIVIDADES ECONÓMICAS Y GESTIÓN DE INVERSIONES', 18, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (125, 'DESPACHO SECRETARÍA MUNICIPAL DE CULTURAS', 19, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (126, 'DIRECCIÓN DE ESPACIOS CULTURALES MUNICIPALES', 19, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (127, 'DIRECCIÓN DE PATRIMONIO CULTURAL', 19, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (128, 'DIRECCIÓN DE FOMENTO A LA PRODUCCIÓN ARTÍSTICA CULTURAL', 19, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (129, 'DESPACHO SECRETARÍA MUNICIPAL DE GESTIÓN INTEGRAL DE RIESGOS', 20,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (130, 'DIRECCIÓN DE PREVENCIÓN DE RIESGOS', 20, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (131, 'DIRECCIÓN DE PLANIFICACIÓN Y ANÁLISIS DE RIESGOS', 20, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (132, 'DIRECCIÓN DE ATENCIÓN DE EMERGENCIAS', 20,2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (133, 'ADMINISTRACIÓN PROGRAMA DRENAJE PLUVIAL', 21, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (134, 'DESPACHO SECRETARÍA MUNICIPAL DE SEGURIDAD CIUDADANA', 22, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (135, 'DIRECCIÓN DE PREVENCIÓN SITUACIONAL', 22, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (136, 'DIRECCIÓN DE PREVENCIÓN SOCIAL Y COMUNITARIA', 22, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (137, 'DESPACHO SECRETARÍA MUNICIPAL DE MOVILIDAD', 23, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion) VALUES (138, 'DIRECCIÓN DE REGULACIÓN Y ORDENAMIENTO DE LA MOVILIDAD', 23, 2018);
INSERT INTO presupuesto.unidad_ejecutora (ue_codigo,ue_descripcion,ue_da_codigo,ue_gestion)VALUES (139, 'DIRECCIÓN DE PLANIFICACIÓN Y TRANSPORTE ALTERNATIVO', 23, 2018);




INSERT INTO presupuesto.unidad_ejecutora VALUES (140, 'DESPACHO SECRETARÍA MUNICIPAL DE GESTIÓN AMBIENTAL', 24, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (141, 'DIRECCIÓN DE PREVENCIÓN Y CONTROL AMBIENTAL', 24, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (142, 'DIRECCIÓN DE ÁREAS PROTEGIDAS, BOSQUES Y ARBOLADO URBANO', 24, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (143, 'ADMINISTRACIÓN SERVICIO DE TRANSPORTE MUNICIPAL', 25, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (144, 'ADMINISTRACIÓN HOSPITAL LA PAZ', 26, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (145, 'ADMINISTRACIÓN HOSPITAL LOS PINOS', 27, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (146, 'ADMINISTRACIÓN HOSPITAL LA MERCED', 28, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (148, 'ADMINISTRACIÓN TRIBUTARIA MUNICIPAL', 30, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (188, 'UNIDAD DE RECAUDACIONES', 30, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (189, 'UNIDAD DE PLANIFICACIÓN Y CONTROL FISCAL', 30, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (190, 'UNIDAD DE NORMAS Y DEFENSA LEGAL', 30, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (149, 'ADMINISTRACIÓN CEMENTERIO GENERAL', 31, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (150, 'ADMINISTRACIÓN ZOOLÓGICO MUNICIPAL VESTY PAKOS', 32, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (151, 'ADMINISTRACIÓN TERMINAL DE BUSES', 33, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (152, 'ADMINISTRACIÓN CENTRO MUNICIPAL DE FAENO', 34, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (153, 'ADMINISTRACIÓN SISTEMA DE REGULACIÓN Y SUPERVISIÓN MUNICIPAL', 35, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (154, 'ADMINISTRACIÓN ESCUELA DE GESTORES MUNICIPALES', 36, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (155, 'ADMINISTRACIÓN SUBALCALDÍA I COTAHUMA', 37, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (157, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. I', 37, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (192, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB. I', 37, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (158, 'ADMINISTRACIÓN SUBALCALDÍA II MAXIMILIANO PAREDES', 38, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (160, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. II', 38, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (193, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB. II', 38, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (161, 'ADMINISTRACIÓN SUBALCALDÍA III PERIFÉRICA', 39, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (163, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. III', 39, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (194, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB.III', 39, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (164, 'ADMINISTRACIÓN SUBALCALDÍA IV SAN ANTONIO', 40, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (166, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. IV', 40, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (195, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB. IV', 40, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (167, 'ADMINISTRACIÓN SUBALCALDÍA V SUR', 41, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (169, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. V', 41, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (196, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB. V', 41, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (170, 'ADMINISTRACIÓN SUBALCALDÍA VI MALLASA', 42, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (172, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. VI', 42, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (197, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB. VI', 42, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (173, 'ADMINISTRACIÓN SUBALCALDÍA VII CENTRO', 43, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (175, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. VII', 43, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (198, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB.VII', 43, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (176, 'ADMINISTRACIÓN SUBALCALDÍA VIII HAMPATURI', 44, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (177, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB. VIII', 44, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (178, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. VIII', 44, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (179, 'ADMINISTRACIÓN SUBALCALDÍA IX ZONGO', 45, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (180, 'DIRECCIÓN DE INFRAESTRUCTURA Y FISCALIZACIÓN TERRITORIAL SUB.IX', 45, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (181, 'DIRECCIÓN DE GESTIÓN SOCIAL Y PROMOCIÓN ECONÓMICA SUB. IX', 45, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (184, 'ADMINISTRACIÓN HOSPITAL MUNICIPAL COTAHUMA', 46, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (185, 'ADMINISTRACIÓN HOSPITAL MUNICIPAL LA PORTADA', 47, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');
INSERT INTO presupuesto.unidad_ejecutora VALUES (187, 'AUTORIDAD DE FISCALIZACIÓN TERRITORIAL', 48, '2018-04-17 12:08:46.347674', '2018-04-17 12:08:46.347674', 1, 'A');




-------------GR------------------------------------------
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (12000, 'Venta de Bienes y Servicios de las Administraciones Públicas');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (13000, 'Ingreso por Impuestos');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (14000, 'Regalías');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (15000, 'Tasas, Derechos y Otros Ingresos');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (16000, 'Intereses y Otras Rentas de la Propiedad');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (18000, 'Donaciones corrientes');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (19000, 'Transferencias Corrientes');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (21000, 'Recursos Propios de Capital');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (22000, 'Donaciones de Capital');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (23000, 'Transferencias de Capital');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (35000, 'Disminución y Cobro de Otros Activos Financieros');
INSERT INTO presupuesto.grupo_rubro (gr_codigo,gr_descripcion)VALUES (37000, 'Obtención de Préstamos del Exterior');

------------------------R-------------------------------------------------------------
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (12100, 'Venta de Bienes y Servicios de las Administraciones Públicas' ,12000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (12200, 'Venta de Bienes y Servicios de las Administraciones Públicas' ,12000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (12300, 'Venta de Bienes y Servicios de las Administraciones Públicas' ,12000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (14100, 'Regalías' ,14000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (15100, 'Tasas, Derechos y Otros Ingresos' ,15000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (16300, 'Intereses y Otras Rentas de la Propiedad' ,16000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (13310, 'Ingreso por Impuestos' ,13000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (13330, 'Ingreso por Impuestos' ,13000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (13360, 'Ingreso por Impuestos' ,13000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (13370, 'Ingreso por Impuestos' ,13000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (15310, 'Tasas, Derechos y Otros Ingresos' ,15000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (15340, 'Tasas, Derechos y Otros Ingresos' ,15000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (15910, 'Tasas, Derechos y Otros Ingresos' ,15000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (15920, 'Tasas, Derechos y Otros Ingresos' ,15000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (15990, 'Tasas, Derechos y Otros Ingresos' ,15000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (23220, 'Transferencias de Capital' ,23000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (35110, 'Disminución y Cobro de Otros Activos Financieros' ,35000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (35410, 'Disminución y Cobro de Otros Activos Financieros' ,35000);

INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (36110, 'Obtención de Préstamos Internos y de Fondos - Fideicomisos' ,36000);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (36210, 'Obtención de Préstamos Internos y de Fondos - Fideicomisos' ,36000);

INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (18111, 'Donaciones corrientes' ,18000);


INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);
INSERT INTO presupuesto.rubro (rubro_codigo,rubro_descripcion,rubro_gr_codigo)VALUES (, '' ,);


-----------------------GI---------------------------------------presupuesto.grupo_item
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (1, 'CAJA FERROVIARIA SUCURSAL 125' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (2, 'CHASQUIPAMPA SUCURSAL 124' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (3, 'CHASQUIPAMPA SUCURSAL 125' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (4, 'ESTACION DE TRANSFERENCIA SUCURSAL 123' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (5, 'ESTACION DE TRANSFERENCIA SUCURSAL 123' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (6, 'ESTACION DE TRANSFERENCIA SUCURSAL 125' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (7, 'FIM' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (8, 'INCA LLOJETA SUCURSAL 124' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (9, 'IRPAVI SUCURSAL 124' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (10, 'KALAJAHUIRA SUCURSAL 124' ,12100);
INSERT INTO presupuesto.grupo_item (gi_codigo,gi_descripcion,gi_r_codigo)VALUES (11, 'MEDICINA GENERAL' ,12200);



---------------IR------------------------
INSERT INTO presupuesto.item_recaudador (ir_codigo,ir_descripcion,ir_gi_codigo)VALUES (1, 'ATENCION DIFERENCIADA INTERCONSULTA' ,11);
INSERT INTO presupuesto.item_recaudador (ir_codigo,ir_descripcion,ir_gi_codigo)VALUES (2, 'CONSULTA MEDICINA GENERAL ' ,11);
INSERT INTO presupuesto.item_recaudador (ir_codigo,ir_descripcion,ir_gi_codigo)VALUES (3, 'CONSULTA MEDICINA GENERAL ' ,11);
INSERT INTO presupuesto.item_recaudador (ir_codigo,ir_descripcion,ir_gi_codigo)VALUES (4, 'RE CONSULTA MEDICINA GENERAL ' ,11);
INSERT INTO presupuesto.item_recaudador (ir_codigo,ir_descripcion,ir_gi_codigo)VALUES (5, 'RE-CONSULTA MEDICINA GENERAL' ,11);

