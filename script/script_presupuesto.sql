CREATE TABLE presupuesto.direccion_administrativa
(
  da_codigo integer NOT NULL,
  da_descripcion text not null,
  da_gestion integer NOT NULL,
  da_registrado timestamp without time zone NOT NULL DEFAULT now(),
  da_modificado timestamp without time zone NOT NULL DEFAULT now(),
  da_usr_id integer NOT NULL,
  da_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT direccion_administrativa_pkey PRIMARY KEY (da_codigo)
);


CREATE TABLE presupuesto.unidad_ejecutora
(
  ue_codigo integer NOT NULL,
  ue_descripcion text NOT NULL,
  ue_da_codigo integer NOT NULL,
  ue_gestion integer not null,
  ue_registrado timestamp without time zone NOT NULL DEFAULT now(),
  ue_modificado timestamp without time zone NOT NULL DEFAULT now(),
  ue_usr_id integer NOT NULL DEFAULT 1,
  ue_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT unidad_ejecutora_pkey PRIMARY KEY (ue_codigo),
  CONSTRAINT unidad_ejecutora_ue_da_codigo_fkey FOREIGN KEY (ue_da_codigo)
      REFERENCES presupuesto.direccion_administrativa (da_codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE presupuesto.unidad_recaudadora
(
  ur_codigo integer NOT NULL,
  ur_descripcion text NOT NULL,
  ur_ue_codigo integer NOT NULL,
  ur_gestion integer not null,
  ur_registrado timestamp without time zone NOT NULL DEFAULT now(),
  ur_modificado timestamp without time zone NOT NULL DEFAULT now(),
  ur_usr_id integer NOT NULL DEFAULT 1,
  ur_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT unidad_recaudadora_pkey PRIMARY KEY (ur_codigo),
  CONSTRAINT unidad_recaudadora_ur_ue_codigo_fkey FOREIGN KEY (ur_ue_codigo)
      REFERENCES presupuesto.unidad_ejecutora (ue_codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE presupuesto.grupo_rubro
(
  gr_codigo integer NOT NULL,
  gr_descripcion text NOT NULL,
  gr_registrado timestamp without time zone NOT NULL DEFAULT now(),
  gr_modificado timestamp without time zone NOT NULL DEFAULT now(),
  gr_usr_id integer NOT NULL DEFAULT 1,
  gr_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT grupo_rubro_pkey PRIMARY KEY (gr_codigo)
);

CREATE TABLE presupuesto.rubro
(
  rubro_codigo integer NOT NULL,
  rubro_descripcion text NOT NULL,
  rubro_gr_codigo integer NOT NULL,
  rubro_registrado timestamp without time zone NOT NULL DEFAULT now(),
  rubro_modificado timestamp without time zone NOT NULL DEFAULT now(),
  rubro_usr_id integer NOT NULL DEFAULT 1,
  rubro_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT rubro_pkey PRIMARY KEY (rubro_codigo),
  CONSTRAINT rubro_r_gr_codigo_fkey FOREIGN KEY (rubro_gr_codigo)
    REFERENCES presupuesto.grupo_rubro (gr_codigo) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE presupuesto.grupo_item
(
  gi_codigo integer NOT NULL,
  gi_descripcion text NOT NULL,
  gi_r_codigo integer NOT NULL,
  gi_registrado timestamp without time zone NOT NULL DEFAULT now(),
  gi_modificado timestamp without time zone NOT NULL DEFAULT now(),
  gi_usr_id integer NOT NULL DEFAULT 1,
  gi_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT grupo_item_pkey PRIMARY KEY (gi_codigo),
  CONSTRAINT grupo_item_gi_r_codigo_fkey FOREIGN KEY (gi_r_codigo)
    REFERENCES presupuesto.rubro (rubro_codigo) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE presupuesto.item_recaudador
(
  ir_codigo integer NOT NULL,
  ir_descripcion text NOT NULL,
  ir_gi_codigo integer NOT NULL,
  ir_registrado timestamp without time zone NOT NULL DEFAULT now(),
  ir_modificado timestamp without time zone NOT NULL DEFAULT now(),
  ir_usr_id integer NOT NULL DEFAULT 1,
  ir_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT item_recaudador_pkey PRIMARY KEY (ir_codigo),
  CONSTRAINT item_recaudador_ir_gi_codigo_fkey FOREIGN KEY (ir_gi_codigo)
    REFERENCES presupuesto.grupo_item (gi_codigo) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE presupuesto.fuente_financiamiento
(
  ff_codigo integer NOT NULL,
  ff_descripcion text NOT NULL,
  ff_sigla text NOT NULL,
  ff_registrado timestamp without time zone NOT NULL DEFAULT now(),
  ff_modificado timestamp without time zone NOT NULL DEFAULT now(),
  ff_usr_id integer NOT NULL DEFAULT 1,
  ff_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT fuente_financiamiento_pkey PRIMARY KEY (ff_codigo)
);


CREATE TABLE presupuesto.orgamisno_financiador
(
  of_codigo integer NOT NULL,
  of_descripcion text NOT NULL,
  of_sigla text NOT NULL,
  of_registrado timestamp without time zone NOT NULL DEFAULT now(),
  of_modificado timestamp without time zone NOT NULL DEFAULT now(),
  of_usr_id integer NOT NULL DEFAULT 1,
  of_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT orgamisno_financiador_pkey PRIMARY KEY (of_codigo)
);

CREATE TABLE presupuesto.fuente_orgamismo
(
  fo_codigo integer NOT NULL,
  fo_of_codigo integer NOT NULL,
  fo_ff_codigo integer NOT NULL,
  fo_tipo integer NOT NULL,
  fo_registrado timestamp without time zone NOT NULL DEFAULT now(),
  fo_modificado timestamp without time zone NOT NULL DEFAULT now(),
  fo_usr_id integer NOT NULL DEFAULT 1,
  fo_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT fuente_orgamismo_pkey PRIMARY KEY (fo_codigo),
  CONSTRAINT fuente_orgamismo_fo_of_codigo_fkey FOREIGN KEY (fo_of_codigo)
    REFERENCES presupuesto.orgamisno_financiador (of_codigo) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fuente_orgamismo_fo_ff_codigo_fkey FOREIGN KEY (fo_ff_codigo)
    REFERENCES presupuesto.fuente_financiamiento (ff_codigo) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE presupuesto.presupuesto_estimado
(
  pe_id serial NOT NULL,
  pe_ur_codigo integer NOT NULL,
  pe_gr_codigo integer NOT NULL,
  pe_ir_codigo integer NOT NULL,
  pe_fo_codigo integer NOT NULL,
  pe_gestion integer NOT NULL,
  pe_periodo integer NOT NULL,
  pe_detalle_monto jsonb,
  pe_monto integer NOT NULL,
  pe_padre_id integer NOT NULL,
  pe_registrado timestamp without time zone NOT NULL DEFAULT now(),
  pe_modificado timestamp without time zone NOT NULL DEFAULT now(),
  pe_usr_id integer NOT NULL DEFAULT 1,
  pe_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT presupuesto_estimado_pkey PRIMARY KEY (pe_id),
  CONSTRAINT presupuesto_estimado_pe_gr_codigo_fkey FOREIGN KEY (pe_gr_codigo)
      REFERENCES presupuesto.grupo_rubro (gr_codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT presupuesto_estimado_pe_ir_codigo_fkey FOREIGN KEY (pe_ir_codigo)
      REFERENCES presupuesto.item_recaudador (ir_codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT presupuesto_estimado_pe_fo_codigo_fkey FOREIGN KEY (pe_fo_codigo)
      REFERENCES presupuesto.fuente_orgamismo (fo_codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT presupuesto_estimado_pe_ur_codigo_fkey FOREIGN KEY (pe_ur_codigo)
      REFERENCES presupuesto.unidad_recaudadora (ur_codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);



CREATE TABLE presupuesto.ingreso_efectivo
(
  ie_id serial NOT NULL,
  ie_estimado_id integer not null,
  ie_fecha date NOT NULL,
  ie_monto double precision NOT NULL,
  ie_registrado timestamp without time zone NOT NULL DEFAULT now(),
  ie_modificado timestamp without time zone NOT NULL DEFAULT now(),
  ie_usr_id integer NOT NULL DEFAULT 1,
  ie_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT ingreso_efectivo_pkey PRIMARY KEY (ie_id),
  CONSTRAINT ingreso_efectivo_ie_estimado_id_fkey FOREIGN KEY (ie_estimado_id)
    REFERENCES presupuesto.presupuesto_estimado (pe_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)


----sp de listados y obtener-------------
CREATE OR REPLACE FUNCTION presupuesto.sp_listar_direccion_administrativa()
  RETURNS TABLE(xda_codigo integer, xda_descripcion text, xda_usr_id integer, xda_estado character, xda_registrado timestamp without time zone, xda_modificado timestamp without time zone) AS
$BODY$
BEGIN
  RETURN QUERY select da_codigo,
  da_descripcion,
  da_usr_id,
  da_estado,
  da_registrado,
  da_modificado 
  from presupuesto.direccion_administrativa
  where da_estado ='A';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;


CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_unidad_ejecutora(IN da_id integer)
  RETURNS TABLE(xue_codigo integer, xue_descripcion text, xue_da_codigo integer) AS
$BODY$
BEGIN
  RETURN QUERY select ue_codigo,
  ue_descripcion,
  ue_da_codigo
  from presupuesto.unidad_ejecutora
  where ue_estado ='A' and ue_da_codigo = da_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_unidad_recaudadora(IN ue_id integer)
  RETURNS TABLE(xur_codigo integer, xur_descripcion text, xur_ue_codigo integer) AS
$BODY$
BEGIN
  RETURN QUERY select ur_codigo,
  ur_descripcion,
  ur_ue_codigo
  from presupuesto.unidad_recaudadora
  where ur_estado ='A' and ur_ue_codigo = ue_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;



CREATE OR REPLACE FUNCTION presupuesto.sp_listar_grupo_rubro()
  RETURNS TABLE(xgr_codigo integer, xgr_descripcion text, xgr_usr_id integer, xgr_estado character, xgr_registrado timestamp without time zone, xgr_modificado timestamp without time zone) AS
$BODY$
BEGIN
  RETURN QUERY select gr_codigo,
  gr_descripcion,
  gr_usr_id,
  gr_estado,
  gr_registrado,
  gr_modificado
  from presupuesto.grupo_rubro
  where gr_estado ='A' and gr_codigo != 0 ;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  

CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_rubro(IN gr_codigo integer)
  RETURNS TABLE(xr_codigo integer, xr_descripcion text, xr_gr_codigo integer) AS
$BODY$
BEGIN
  RETURN QUERY select rubro_codigo,
  rubro_descripcion,
  rubro_gr_codigo
  from presupuesto.rubro
  where rubro_estado ='A' and rubro_gr_codigo = gr_codigo;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;



CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_grupo_item(IN r_codigo integer)
  RETURNS TABLE(xgi_codigo integer, xgi_descripcion text, xgi_r_codigo integer) AS
$BODY$
BEGIN
  RETURN QUERY select gi_codigo,
  gi_descripcion,
  gi_r_codigo
  from presupuesto.grupo_item
  where gi_estado ='A' and gi_r_codigo = rubro_codigo;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;


CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_item_recaudador(IN gi_codigo integer)
  RETURNS TABLE(xir_codigo integer, xir_descripcion text, xir_gi_codigo integer) AS
$BODY$
BEGIN
  RETURN QUERY select ir_codigo,
  ir_descripcion,
  ir_gi_codigo
  from presupuesto.item_recaudador
  where ir_estado ='A' and ir_gi_codigo = gi_codigo;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;



CREATE OR REPLACE FUNCTION presupuesto.sp_listar_fuente()
  RETURNS TABLE(xf_codigo integer, xf_descripcion text, xf_sigla text, xf_estado character, xf_registrado timestamp without time zone, xf_modificado timestamp without time zone) AS
$BODY$
BEGIN
  RETURN QUERY select f_codigo,
  f_descripcion,
  f_sigla,
  f_estado,
  f_registrado,
  f_modificado
  from presupuesto.fuente
  where f_estado ='A';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

 CREATE OR REPLACE FUNCTION presupuesto.sp_listar_orgamisno_financiador()
  RETURNS TABLE(xof_codigo integer, xof_descripcion text, xof_sigla text, xof_estado character, xof_registrado timestamp without time zone, xof_modificado timestamp without time zone) AS
$BODY$
BEGIN
  RETURN QUERY select of_codigo,
  of_descripcion,
  of_sigla,
  of_estado,
  of_registrado,
  of_modificado
  from presupuesto.orgamisno_financiador
  where of_estado ='A';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

CREATE OR REPLACE FUNCTION presupuesto.sp_listar_presupuesto_estimado_hijos(IN xpe_padre_id integer)
  RETURNS TABLE(xpe_id integer, xpe_ir_codigo integer, xir_descripcion text,xpe_periodo integer,xpe_monto integer) AS
$BODY$
  BEGIN
      RETURN QUERY select pe.pe_id,pe.pe_ir_codigo,ir.ir_descripcion,pe.pe_periodo,pe.pe_monto
       from presupuesto.presupuesto_estimado AS pe
       inner join presupuesto.item_recaudador AS ir on ir.ir_codigo = pe.pe_ir_codigo
       where pe.pe_padre_id = xpe_padre_id
       order by pe.pe_modificado;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;



  -------INSERTAR----------------------
CREATE OR REPLACE FUNCTION presupuesto.sp_insertar_presupuesto_estimado(
    xpe_ur_codigo integer,
    xpe_gr_codigo integer,
    xpe_ir_codigo integer,
    xpe_f_codigo integer,
    xpe_of_codigo integer,
    xpe_gestion integer,
    xpe_periodo integer,
    xpe_detalle_monto text,
    xpe_monto integer,
    xpe_padre_id integer,
    xpe_usr_id integer)
  RETURNS boolean AS
$BODY$
BEGIN
   INSERT INTO presupuesto.presupuesto_estimado(
   pe_ur_codigo,
   pe_gr_codigo,
   pe_ir_codigo,
   pe_f_codigo,
   pe_of_codigo,
   pe_gestion,
   pe_periodo,
   pe_detalle_monto,
   pe_monto,
   pe_padre_id,
   pe_usr_id
   )
   VALUES (
   xpe_ur_codigo,
   xpe_gr_codigo,
   xpe_ir_codigo,
   xpe_f_codigo,
   xpe_of_codigo,
   xpe_gestion,
   xpe_periodo,
   cast(xpe_detalle_monto as jsonb),
   xpe_monto,
   xpe_padre_id,
   xpe_usr_id);
    RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE FUNCTION presupuesto.sp_insertar_ingreso_efectivo(
    xie_estimado_id integer,
    xie_fecha date,
    xie_monto integer)
  RETURNS boolean AS
$BODY$
BEGIN
   INSERT INTO presupuesto.ingreso_efectivo(
   ie_estimado_id,
   ie_fecha,
   ie_monto
   )
   VALUES (
   xie_estimado_id,
   xie_fecha,
   xie_monto);
   RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;



CREATE OR REPLACE FUNCTION presupuesto.sp_lista_presupuesto_estimado_padre(IN xgestion integer)
  RETURNS TABLE(xtotal_hijos bigint, xtotalefectivo double precision, xpe_id integer, xur_descripcion text,xpe_ur_codigo integer, xgr_descripcion text,xpe_gr_codigo integer, xpe_gestion integer, xpe_monto integer) AS
$BODY$

  BEGIN  
      RETURN QUERY 
  select  j.total_hijos,ifp.totalefectivo, pe.pe_id,ur.ur_descripcion,pe.pe_ur_codigo,gr.gr_descripcion,pe.pe_gr_codigo,
  pe.pe_gestion, pe.pe_monto
  from presupuesto.presupuesto_estimado AS pe
  inner join presupuesto.unidad_recaudadora as ur on ur.ur_codigo = pe.pe_ur_codigo
  inner join presupuesto.grupo_rubro as gr on gr.gr_codigo = pe.pe_gr_codigo
  left outer join ( 
    select pp.pe_padre_id, sum(pp.pe_monto) as total_hijos
    from  presupuesto.presupuesto_estimado as pp
    where  pp.pe_gestion = xgestion and pp.pe_padre_id in (
              select pe_padre_id from presupuesto.presupuesto_estimado where pe_padre_id = 0 order by 1 asc )
    group by pp.pe_padre_id
    order by pp.pe_padre_id
  ) as j on j.pe_padre_id = pe.pe_id
  
  left outer join (
    select datoss.pe_padre_id, sum(montoefectivo) as totalefectivo 
    from (SELECT ie_estimado_id, SUM(ie_monto) as montoefectivo, kkk.pe_padre_id
    FROM presupuesto.ingreso_efectivo inf  
    inner join 
      ( select pppeee.pe_padre_id, pppeee.pe_id 
        from  presupuesto.presupuesto_estimado pppeee ) as kkk on inf.ie_estimado_id= kkk.pe_id
    GROUP BY ie_estimado_id, kkk.pe_padre_id
    order by 1 asc) as datoss
     group by datoss.pe_padre_id
  )  as ifp on ifp.pe_padre_id = pe.pe_id
  where pe.pe_padre_id = 0 and pe.pe_gestion = xgestion
  order by pe.pe_modificado desc;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;




CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_fuente_financiamiento_asignada()
  RETURNS TABLE(xfo_codigo integer, xfo_ff_codigo integer, xff_descripcion text) AS
$BODY$
BEGIN
  RETURN QUERY select fo_codigo,
  fo_ff_codigo,
  ff.ff_descripcion
  from presupuesto.fuente_orgamismo
  inner join presupuesto.fuente_financiamiento as ff on ff.ff_codigo= fo_ff_codigo and ff.ff_estado='A' 
  where fo_estado ='A';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_organismo_financiamiento_asignada(IN xfo_ff_codigo integer)
  RETURNS TABLE(xfo_codigo integer, xfo_of_codigo integer, xof_descripcion text) AS
$BODY$
BEGIN
  RETURN QUERY select fo_codigo,
  fo_of_codigo,
  orf.of_descripcion
  from presupuesto.fuente_orgamismo
  inner join presupuesto.orgamisno_financiador as orf on orf.of_codigo= fo_of_codigo and orf.of_estado='A' 
  where fo_estado ='A' and fo_ff_codigo = xfo_ff_codigo;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;


CREATE OR REPLACE FUNCTION presupuesto.sp_obtener_fuente_organismo(xff_codigo integer, xof_codigo integer)
  RETURNS TABLE(xfo_codigo integer, xfo_of_codigo integer, xfo_ff_codigo integer) AS
$BODY$
BEGIN
  RETURN QUERY select fo_codigo,
  fo_of_codigo,
  fo_ff_codigo
  from presupuesto.fuente_orgamismo
  where fo_estado ='A' and fo_of_codigo = xfo_of_codigo and fo_ff_codigo = xfo_ff_codigo;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;