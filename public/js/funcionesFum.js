var urlRegla = "http://172.19.161.3/api/reglaNegocio/ejecutarWeb";
var urlToken = "prueba/VALLE/public/v0.0/getToken";
var urlFactura = "http://192.168.5.69/ifacturacion247_pruebas/public/api/planificacion/ifactura247";
var urlTokenFactura ="prueba/tribNoTrib/public/v0.0/getTokenFac";
var grupo = 0;
var datoFum = 0;
var descripcionBanco = '';
var idfumnuevo= 0;
var imagenQRfum = '';
var dataBanco= [];


function generarFum1(idDeudas,gestion,contribuyente,glosa,trans,tipo,idUsuario){
  var formData = {"identificador": 'SERVICIO_SIERRA-275',"parametros":'{}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {   
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataGrupo) {
          if(dataGrupo[0].sp_obtener_grupo == null){
          	grupo = 1;
          }
          else{
          	grupo = dataGrupo[0].sp_obtener_grupo;
          }
          formData = {"identificador": 'SERVICIO_SIERRA-165',"parametros":'{"yusuario_id":'+idUsuario+'}'};
          $.ajax({
  	        type        : 'POST',            
  	        url         : urlRegla,
  	        data        : formData,
  	        dataType    : 'json',
  	        crossDomain : true,
  	        headers: {
  	          'authorization': 'Bearer '+token,
  	        },
  	        success: function(dataUnidadRecaucadora) {
  	          var urId = dataUnidadRecaucadora[0].xusuario_unidad_recaudadora_codigo_ur;
  	          idDeudas = idDeudas + ',';
  			      var deudasId = idDeudas.split(',');
  			      gestion = gestion + ',';
  			      var ges = gestion.split(',');
  			      trans = trans + '-';
  			      trans = trans.split('-');
      			  for(var i = 0;i<deudasId.length-1;i++){
      			  	var transac = JSON.parse(trans[i]);
      			  	var monto = 0;
  	          	for(var j = 0;j<transac.length;j++){
  	          	  monto = transac[j].Monto + monto;
  	          	}
  	          	var transac1 = JSON.stringify(trans[i]);
  	          	var data = '{"observaciones":" ","gestion":'+ges[i]+', "urlfum":" "}';
  	          	data = JSON.stringify(data);
  	          	formData = {"identificador": 'SERVICIO_SIERRA-274',"parametros": '{"xfum_ur_id":'+urId+',"xfum_deuda_id":'+deudasId[i]+',"xfum_grupo":'+grupo+',"xfum_tipo_origen":"'+tipo+'","xfum_monto_pagar":'+monto+',"xfum_data":'+data+',"xfum_data_contribuyente":'+contribuyente+',"xfum_data_transaccion":'+transac1+',"xfum_data_glosa":'+glosa+',"xfum_usr_id":'+idUsuario+'}'};
  	          	$.ajax({
                  type        : 'POST',            
                  url         : urlRegla,
                  data        : formData,
                  dataType    : 'json',
                  crossDomain : true,
                  headers: {
                   'authorization': 'Bearer '+token,
                  },
                  success: function(data){ 
                    idfumnuevo =  data[0].sp_insertar_fum1;
                    console.log('Se liquido exitosamente')
                  },
                  error: function(result) {
                    console.log('No se puede generar Fum');
                  }
                })
    	        }
	          }
	        })
        },     
        error: function (xhr, status, error) { 
          console.log('Error al obtener grupo');
        }
      });
    },
    error: function(result) {
      console.log('Error al generar el token');
    },
  })
}

function listarBanco(){
  var formDataBanco = {"identificador":"SERVICIO_SIERRA-217","parametros": '{}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formDataBanco,
        dataType    : 'json',
        crossDomain : true,
        headers: {
         'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          console.log(dataIN);
          dataBanco = dataIN;
          var tam=dataIN.length;
          if (dataIN=="[{ }]") {
            tam = 0;
          }
          for (var i = 0; i < tam; i++) 
          {
            descripcionBanco=descripcionBanco+JSON.parse(dataIN[i].banco_data).descripcion+',';
          }
          descripcionBanco=descripcionBanco.substring(0,descripcionBanco.length-1);
        },
        error: function (xhr, status, error) {
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};



function codigoQRfum(array){ 

    var route1="codigoQRfum";
    $.ajax({
              url: route1,
              type: 'GET',
              dataType: 'text',
              data: array,
          success: function(data){
            imagenQRfum = data; 
          },
          error: function(result) {
              swal( "Error..!", "Verifique que los campos esten llenados Gracias...!", "error" );
          }
    });
};
function codigoQRfum2(array){ 

  var datoFum = array;
    var route1="/codigoQRfum";
    console.log(datoFum);
       $.ajax({
          url: route1,
              type: 'POST',              
              data: { datoFum : datoFum}               
              ,
          success: function(data){
            imagenQRfum = data;             
          },
          error: function(result) {              
          }
    });
};

function imprimirFum1(){
  datoFum = grupo;
}