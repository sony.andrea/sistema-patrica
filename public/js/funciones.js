function LoadBootstrapValidatorScript(callback){
	if (!$.fn.bootstrapValidator){
		$.getScript('plugins/bootstrapvalidator/bootstrapValidator.min.js', callback);
	}
	else {
		if (callback && typeof(callback) === "function") {
			callback();
		}
	}
}

function DemoFormValidator(){
	$('#defaultForm').bootstrapValidator({
		message: 'This value is not valid',
		fields: {
			usuario: {
				message: 'introdusca solo letras',
				validators: {
					notEmpty: {
						message: 'se requiere que introdusca su nombre completo\''
					},
					stringLength: {
						min: 6,
						max: 30,
						message: 'El nombre del contacto tiene que tener minimo 6  y 30 maximo caracteres de largo'
					},
					regexp: {
						regexp: /^[a-z A-Z\.]+$/,
						message: 'por favor no introdusca numeros en esta caja de texto'
					}
				}
			},
			rubro: {
				message: 'introdusca solo letras',
				validators: {
					notEmpty: {
						message: 'se requiere que introdusca un rubro\''
					},
					stringLength: {
						min: 5,
						max: 30,
						message: 'El rubro tiene que tener minimo 5  y 30 maximo caracteres de largo'
					},
					regexp: {
						regexp: /^[a-z A-Z\.]+$/,
						message: 'por favor no introdusca numeros en esta caja de texto'
					}
				}
			},
			institucion: {
				message: 'introdusca solo letras',
				validators: {
					notEmpty: {
						message: 'se requiere que introdusca una institucion\''
					},
					stringLength: {
						min: 5,
						max: 30,
						message: 'La institucion tiene que tener minimo 5  y 30 maximo caracteres de largo'
					},
					regexp: {
						regexp: /^[a-z A-Z\.]+$/,
						message: 'por favor no introdusca numeros en esta caja de texto'
					}
				}
			},
			descripcion: {
				message: 'Usted inserto mal los datos vuelva a intentarlo',
				validators: {
					notEmpty: {
						message: 'Se requiere que escriba un mensaje de texto puede que este \t vacío '
					},
					stringLength: {
						min: 10,
						max: 255,
						message: 'El mensaje de texto  debe ser mayor de 10 y menos de 255 letras de longitud'
					}
				}
			},
			telefonos: {
				message: 'Usted inserto mal los datos vuelva a intentarlo',
				validators: {
					notEmpty: {
						message: 'Se requiere que escriba un mensaje de texto puede que este \t vacío '
					},
					stringLength: {
						min: 8,
						max: 300,
						message: 'El numero telefonico debe ser mayor de 8 numeros y menos de 300 numeros de longitud'
					},
					regexp: {
						regexp: /^[0-9\.]+;/,
						message: 'Escriba el numero telefonico seguido de un punto y coma'
					}
				}
			},
			telefono: {
				message: 'Usted inserto mal los datos vuelva a intentarlo',
				validators: {
					notEmpty: {
						message: 'Se requiere que escriba un numero de celular puede que este \t vacío '
					},
					stringLength: {
						min: 8,
						max: 300,
						message: 'El numero telefonico debe ser mayor de 8 numeros y menos de 300 numeros de longitud'
					},
					regexp: {
						regexp: /^[0-9]/,
						message: 'Escriba solo numeros'
					}
				}
			},
			direccion: {
				validators: {
					notEmpty: {
						message: 'ingrese la direccion donde trabaja\''
					}
				}
			},
			acceptTerms: {
				validators: {
					notEmpty: {
						message: 'You have to accept the terms and policies'
					}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'The email address is required and can\'t be empty'
					},
					emailAddress: {
						message: 'The input is not a valid email address'
					}
				}
			},
			website: {
				validators: {
					uri: {
						message: 'The input is not a valid URL'
					}
				}
			},
			phoneNumber: {
				validators: {
					digits: {
						message: 'The value can contain only digits'
					}
				}
			},
			color: {
				validators: {
					hexColor: {
						message: 'The input is not a valid hex color'
					}
				}
			},
			zipCode: {
				validators: {
					usZipCode: {
						message: 'The input is not a valid US zip code'
					}
				}
			},
			password: {
				validators: {
					notEmpty: {
						message: 'The password is required and can\'t be empty'
					},
					identical: {
						field: 'confirmPassword',
						message: 'The password and its confirm are not the same'
					}
				}
			},
			confirmPassword: {
				validators: {
					notEmpty: {
						message: 'The confirm password is required and can\'t be empty'
					},
					identical: {
						field: 'password',
						message: 'The password and its confirm are not the same'
					}
				}
			},
			ages: {
				validators: {
					lessThan: {
						value: 250,
						inclusive: true,
						message: 'The ages has to be less than 250'
					},
					greaterThan: {
						value: 10,
						inclusive: false,
						message: 'The ages has to be greater than or equals to 10'
					}
				}
			}
		}
	});
}