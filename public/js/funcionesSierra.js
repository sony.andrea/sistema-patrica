

var urlRegla = "http://172.19.161.3/api/reglaNegocio/ejecutarWeb";
//var urlRegla ='http://13.72.107.250/MotorServicio/public/api/apiLogin';
var urlToken = "prueba/VALLE/public/v0.0/getToken";
var urlFactura = "http://192.168.5.69/ifacturacion247_pruebas/public/api/planificacion/ifactura247";
var urlTokenFactura ="prueba/VALLE/public/v0.0/getTokenFac";

function caracteresEspecialesRegistrar(textoE){
  var cuerpo = textoE;
        cuerpo = cuerpo.split("\n").join("");  
        cuerpo = (cuerpo.replace(/['"]+/g, "&040;"));
        cuerpo = (cuerpo.replace(/['<]+/g, "&041;"));
        cuerpo = (cuerpo.replace(/['>]+/g, "&042;"));
        cuerpo = (cuerpo.replace(/['\\]+/g, "&043;"));
        cuerpo = (cuerpo.replace(/['{]+/g, "&044;"));
        cuerpo = (cuerpo.replace(/['}]+/g, "&045;"));
        cuerpo = (cuerpo.replace(/['[]+/g, "&046;"));
        cuerpo = (cuerpo.replace(/[\]]+/g, "&047;"));
        cuerpo = (cuerpo.replace(/['(]+/g, "&048;"));
        cuerpo = (cuerpo.replace(/[')]+/g, "&049;"));
        cuerpo = (cuerpo.replace(/['/]+/g, "&050;"));
        cuerpo = (cuerpo.replace(/[':]+/g, "&051;"));
        cuerpo = (cuerpo.replace(/\s/g,"&052"));
        cuerpo = (cuerpo.replace(/['#]+/g, "&053;"));
        
        return cuerpo;
}

function caracteresEspecialesActualizar(textoE) {

  var cuerpovalor = textoE;
        cuerpovalor = cuerpovalor.replace(/<x>/g, "");
        //cuerpovalor = cuerpovalor.replace(/&039;/g, "'");
        cuerpovalor = cuerpovalor.replace(/&040;/g, "\"");
        cuerpovalor = cuerpovalor.replace(/&041;/g, "<");
        cuerpovalor = cuerpovalor.replace(/&042;/g, ">");
        cuerpovalor = cuerpovalor.replace(/&043;/g, "\\");
        cuerpovalor = cuerpovalor.replace(/&044;/g, "{");
        cuerpovalor = cuerpovalor.replace(/&045;/g, "}");
        cuerpovalor = cuerpovalor.replace(/&046;/g, "[");
        cuerpovalor = cuerpovalor.replace(/&047;/g, "]");
        cuerpovalor = cuerpovalor.replace(/&048;/g, "(");
        cuerpovalor = cuerpovalor.replace(/&049;/g, ")");
        cuerpovalor = cuerpovalor.replace(/&050;/g, "/");
        cuerpovalor = cuerpovalor.replace(/&051;/g, ":");
        cuerpovalor = cuerpovalor.replace(/&053;/g, "#");
        cuerpovalor = cuerpovalor.replace(/&052/g, " ");
        return cuerpovalor;
}

function reemComillasPorBarras(textoC){
          var cuerpo = textoC;
          //cuerpo = (cuerpo.replace(""", "\', \'"));
          cuerpo = (cuerpo.replace(/['"]+/g, "*"));
          
          return cuerpo;
}

function reemBarrasPorComillas(textoC) {
         var cuerpo = textoC;
          //cuerpo = (cuerpo.replace(""", "\', \'"));
          cuerpo = (cuerpo.replace(/['*]+/g, '"'));
          
          return cuerpo;
}

function cambiarValorBool(textoB){
  var res ;
  if(textoB == "NO"){
    res = false;
  }
  if(textoB == "SI"){
    res = true;
  }
  
  return res;
}
function cambiarBoolAString(textoB){
  var res ;
  if(textoB){
    res = "SI";
  }
  else 
  {
    res = "NO";
  }
   
  return res;
}


function cambiarDatosBool(datos){
  var res;
  if(datos == "SI" || datos == true){
    res = "SI";
  }
  else{
    res = "NO";
  }

  return res;
}

function validarMaterial(datos)
{ 
  var datos1;
  if(datos == 'Adobe'){
   datos1 = 'Adobe';
  }
  if(datos == 'Piedra'){
  datos1 = 'Piedra';
  }
  if(datos == 'Madera'){
   datos1 = 'Madera';
  }
  if(datos == 'Ladrillo'){
   datos1 = 'Ladrillo';
  }
  if(datos == 'Hormigo Armado'){
   datos1 = 'Hormigo Armado';
  }
  if(datos == 'Concreto'){
   datos1 = 'Concreto';
  }
  if(datos == "me<x>tal"){
   datos1 = 'Metal';
  }
   if(datos == 'N'){
    datos1 = '--Seleccione--';
  }
  return datos1;
}


/*function validarCampoMaterial(datos)
{ 
  var datos;
  
    if(datos == 'Metal' || datos == 'me<x>tal'){
    datos = 'Metal';
    }
    return datos;
  }
*/
function formatearNumero(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function formatearNumero2(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
function mod(dividendo , divisor)
{
  var resDiv = dividendo / divisor ;  
  var parteEnt = Math.floor(resDiv);          
  var parteFrac = resDiv - parteEnt ;   
  var modulo = Math.round(parteFrac * divisor)
  return modulo;
}

function ObtenerParteEntDiv(dividendo , divisor)
{
  var resDiv = dividendo / divisor ;  
  var parteEntDiv = Math.floor(resDiv);
  return parteEntDiv;
}

function fraction_part(dividendo , divisor)
{
  var resDiv = dividendo / divisor ;  
  var f_part = Math.floor(resDiv);
  return f_part;
}

function string_literal_conversion(numero)
{  
   var centenas = ObtenerParteEntDiv(numero, 100);
   var numero = mod(numero, 100);
   var decenas = ObtenerParteEntDiv(numero, 10);
   numero = mod(numero, 10);
 
   var unidades = ObtenerParteEntDiv(numero, 1);
   numero = mod(numero, 1);  
   var string_hundreds="";
   var string_tens="";
   var string_units="";
   
   if(centenas == 1){
      string_hundreds = "ciento ";
   }
   if(centenas == 2){
      string_hundreds = "doscientos ";
   }
   if(centenas == 3){
      string_hundreds = "trescientos ";
   }
   if(centenas == 4){
      string_hundreds = "cuatrocientos ";
   }
   if(centenas == 5){
      string_hundreds = "quinientos ";
   }
   if(centenas == 6){
      string_hundreds = "seiscientos ";
   }
   if(centenas == 7){
      string_hundreds = "setecientos ";
   }
   if(centenas == 8){
      string_hundreds = "ochocientos ";
   }
   if(centenas == 9){
      string_hundreds = "novecientos ";
   }

   if(decenas == 1){
           
      if(unidades == 1){
         string_tens = "once";
      }
      if(unidades == 2){
         string_tens = "doce";
      }
      if(unidades == 3){
         string_tens = "trece";
      }
      if(unidades == 4){
         string_tens = "catorce";
      }
      if(unidades == 5){
         string_tens = "quince";
      }
      if(unidades == 6){
         string_tens = "dieciseis";
      }
      if(unidades == 7){
         string_tens = "diecisiete";
      }
      if(unidades == 8){
         string_tens = "dieciocho";
      }
      if(unidades == 9){
         string_tens = "diecinueve";
      }
   }
   
   if(decenas == 2){
      string_tens = "veinti";
   }
   if(decenas == 3){
      string_tens = "treinta";
   }
   if(decenas == 4){
      string_tens = "cuarenta";
   }
   if(decenas == 5){
      string_tens = "cincuenta";
   }
   if(decenas == 6){
      string_tens = "sesenta";
   }
   if(decenas == 7){
      string_tens = "setenta";
   }
   if(decenas == 8){
      string_tens = "ochenta";
   }
   if(decenas == 9){
      string_tens = "noventa";
   }

   if (decenas == 1)
   {
      string_units="";  

   }
   else
   {
      if(unidades == 1){
         string_units = "un";
      }
      if(unidades == 2){
         string_units = "dos";
      }
      if(unidades == 3){
         string_units = "tres";
      }
      if(unidades == 4){
         string_units = "cuatro";
      }
      if(unidades == 5){
         string_units = "cinco";
      }
      if(unidades == 6){
         string_units = "seis";
      }
      if(unidades == 7){
         string_units = "siete";
      }
      if(unidades == 8){
         string_units = "ocho";
      }
      if(unidades == 9){
         string_units = "nueve";
      }
       
   } 

if (centenas == 1 && decenas == 0 && unidades == 0)
{
   string_hundreds = "cien " ;
}  

if (decenas == 1 && unidades ==0)
{
   string_tens = "diez " ;
}

if (decenas == 2 && unidades ==0)
{
  string_tens = "veinte " ;
}

if (decenas >=3 && unidades >=1)
{
   string_tens = string_tens+" y ";
}

var final_string = string_hundreds+string_tens+string_units;
return final_string ;
}

function convertirNumLetras(numero)
{
   //var numero = $('#Monto').val();
   //var numero = "2627483.52";

   var numero1=numero.toString();
   var cent = numero1.split(".");  
   var centavos = cent[1];
   var centenas_final_string, millions_final_string, thousands_final_string, centenas, moneda, cad;
   
   numero=cent[0];
   if (centavos == 0 || centavos == undefined)
   {
        centavos = "00";
   }
   if (numero == 0 || numero == "")
   { 
      centenas_final_string=" cero "; 
  }
   else
   {
     var millions  = ObtenerParteEntDiv(numero, 1000000);
      numero = mod(numero, 1000000); 
     
     if (millions != 0)
      {                      
         if (millions == 1)
         {    
            descriptor= " millon ";  
            }
         else
         {                          
              descriptor = " millones ";
            }
      }
      else
      {    
         descriptor = " ";            
      }
      millions_final_string = string_literal_conversion(millions)+descriptor;
      thousands = ObtenerParteEntDiv(numero, 1000);  
        numero = mod(numero, 1000);          

     if (thousands != 1)
      {  
         thousands_final_string =string_literal_conversion(thousands) + " mil ";

      }
      if (thousands == 1)
      {
         thousands_final_string = " mil ";
     }
      if (thousands < 1)
      {
         thousands_final_string = " ";
      }

     centenas  = numero;                    
      centenas_final_string = string_literal_conversion(centenas) ;
   } 
   cad = millions_final_string+thousands_final_string+centenas_final_string;

   cad = cad.toUpperCase(); 

   //console.log(cad,'VALOR');


   if (centavos.length>2)
   {  
      if(centavos.substring(2,3)>= 5){
         centavos = centavos.substring(0,1)+(parseInt(centavos.substring(1,2))+1).toString();
      }   else{
         
        centavos = centavos.substring(0,1);
      }
   }
 
   if (centavos.length==1)
   {
      centavos = centavos+"0";
   }
   centavos = centavos+ "/100";

   //console.log(centavos,'CENTAVOS');

 
   if (numero == 1)
   {
      moneda = " BS. ";  
   }
   else
   {
      moneda = " BS. ";  
   }

  //console.log(moneda,'TIPO MONEDA');

   if(cad == '  MIL ')
   {
        cad=' UN MIL ';
   }
 $('#Monto2').val(cad+' '+centavos+moneda);
  return cad+' '+centavos;

  //console.log(5555,cad+' '+centavos);



}
function obtenerContribuyente(idC){
  var formData = {"identificador": 'SERVICIO_VALLE-473',"parametros": '{"ycontribuyente_id":'+idC+'}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      //console.log(token,999);
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
           'authorization': 'Bearer '+token,
        },
        success: function(dataContribuyente) {
          dataContribuyente = dataContribuyente[0].xtodo;
          var datosCont = (JSON.parse(dataContribuyente)).contribuyente_data;
          //console.log(datosCont,88);
          var nombreC = datosCont.primer_nombre +' '+datosCont.primer_apellido+' '+datosCont.segundo_apellido;
          htmlListaContribuyente = '';
          htmlListaContribuyente+='<div class="panel panel-info">';
          htmlListaContribuyente+='<div class="panel-heading">DATOS DEL TITULAR</div>';
          htmlListaContribuyente+='<div class="panel-body">';
          htmlListaContribuyente+='<div class="row">';
          htmlListaContribuyente+='<div class="col-md-12">';
          htmlListaContribuyente+='<div class="col-md-2"><b>NOMBRE COMPLETO: </b></div>';
          htmlListaContribuyente+='<div class="col-md-3">';
          htmlListaContribuyente+='<input type="text" id="descContribuyente" value = "'+nombreC+'"class="form-control" name="descContribuyente" readonly>';
          htmlListaContribuyente+='</div>';
          htmlListaContribuyente+='<div class="col-md-2"><b>Nro. DE IDENTIFICACION: </b></div>';
          htmlListaContribuyente+='<div class="col-md-3">';
          htmlListaContribuyente+='<input type="text" id="identificacion" value = "'+datosCont.documento+' '+datosCont.expedido+'" class="form-control" name="identificacion" readonly>';
          htmlListaContribuyente+='</div></div></div></div>';
          $('#idContribuyente').html(htmlListaContribuyente);
        },
        error: function (xhr, status, error) {
          swal( "Error..!", "Error al obtener datos de contribuyente", "error" );
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo listar los datos", "error" );
    },
  }); 
}
function obtenerDatosPuesto(idPuesto)
{
  var formData = {"identificador": 'SERVICIO_VALLE-633',"parametros": '{"xmercado_id":'+idPuesto+'}'};
    $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      //console.log(token,999);
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
           'authorization': 'Bearer '+token,
        },
        success: function(dataActividad) {
          
          var data = JSON.parse(dataActividad[0].xtodo);
          var puesto_mercado_id=data.puesto_mercado_id;
          var mercado=data.mercado_data.nombre;
          var mercaderia=data.mercaderia_data.descripcion;
          var tipoPuesto=data.tipo_puesto_data.descripcion;
          var ancho =data.puesto_mercado_data.datos_tecnicos.ancho;
          var frente =data.puesto_mercado_data.datos_tecnicos.frente;
          //console.log("dataActividad",dataActividad);
          htmlEncabezadoPuesto='';
          htmlEncabezadoPuesto+='<div class="panel panel-info">';
          htmlEncabezadoPuesto+='<div class="panel-heading">DATOS DEL PUESTO MERCADO</div>';
          htmlEncabezadoPuesto+='<div class="panel-body">';
          htmlEncabezadoPuesto+='<div class="row">';         
          htmlEncabezadoPuesto+='<div class="col-md-12">';
          htmlEncabezadoPuesto+='<div class="form-group col-md-5">';
          htmlEncabezadoPuesto+='<h5><b>ID PUESTO:</b> <spam id="act_economica">'+puesto_mercado_id+' </spam></h5>';  
          htmlEncabezadoPuesto+='<h5><b>MERCADO:</b> <spam  id="act_desarrollada">'+mercado+' </spam ></h5>';
          htmlEncabezadoPuesto+='<h5><b>TIPO DE MERCADERIA: </b><spam  id="nit"> '+mercaderia+'</spam ></h5>';
          htmlEncabezadoPuesto+='</div>';
          htmlEncabezadoPuesto+='<div class="form-group col-md-7">';
          htmlEncabezadoPuesto+='<h5><b>TIPO PUESTO : </b><spam  id="inicio"> '+tipoPuesto+'</spam ></h5>';
          htmlEncabezadoPuesto+='<h5><b>ANCHO : </b><spam id="via">'+ancho+'</spam></h5>';
          htmlEncabezadoPuesto+='<h5><b>FRENTE: </b><spam id="distrito">'+frente+'</spam></h5>';
          htmlEncabezadoPuesto+='</div>';
          htmlEncabezadoPuesto+='</div>';
          htmlEncabezadoPuesto+='</div>';
          htmlEncabezadoPuesto+='</div></div></div>';
        
          $('#idPuesto').html(htmlEncabezadoPuesto);
        },
        error: function (xhr, status, error) {
          swal( "Error..!", "Error al obtener datos de contribuyente", "error" );
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo listar los datos", "error" );
    },
  });  
}
function calcularInteres(idPuesto,user){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      var formData = {"identificador": 'SERVICIO_SIERRA-116',"parametros": '{"actividadEconomicaId":'+idPuesto+',"yusrId":'+user+'}'};
      console.log("formData",formData);
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
           'authorization': 'Bearer '+token,
        },
        success: function(datosDeuda) {
          console.log('Se actualizo los datos');
        },
        error: function (xhr, status, error) {
          console.log('Error al actualizar deudas');
        }
      });
    },
    error: function(result) {
      console.log('Error al generar el token');
    },
  }); 
};

function obtenerContribuyenteATM(idC)
{
   var formData = {"identificador": 'SERVICIO_VALLE-765',"parametros": '{"ycontribuyente_id":'+idC+'}'};
    $.ajax({
    type        : 'GET',
    url         : 'prueba/VALLE/public/v0.0/getToken',
    data        : '',
    success: function(token) {
      console.log(token,999);
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
           'authorization': 'Bearer '+token,
        },
        success: function(dataContribuyente) {

        
          var contribuyente_tipo=JSON.parse(dataContribuyente[0].xtodo).contribuyente_tipo;
          var dataContribuyente = dataContribuyente[0].xtodo;
          var datosCont = (JSON.parse(dataContribuyente)).contribuyente_data;
          var nombreC = datosCont.primer_nombre;
          var apellidos =datosCont.primer_apellido+' '+datosCont.segundo_apellido;
          var celular=datosCont.celular;
          var padron=datosCont.padron;
          var direccion=datosCont.zona+' '+datosCont.nro;
          var identificacion=datosCont.documento+' '+datosCont.expedido;
          var empresa=datosCont.empresa;
          var representanteId=datosCont.representanteId;
          if (contribuyente_tipo=='N') {
          htmlEncabezadoContri='';
          htmlEncabezadoContri+='<div class="panel panel-info">';
          htmlEncabezadoContri+='<div class="panel-heading">DATOS DEL CONTRIBUYENTE</div>';
          htmlEncabezadoContri+='<div class="panel-body">';
          htmlEncabezadoContri+='<div class="row">';
          htmlEncabezadoContri+='<div class="col-md-12">';
          htmlEncabezadoContri+='<div class="form-group col-md-4">';
          htmlEncabezadoContri+='<label class="control-spam col-md-4">NOMBRES:</label>';
          htmlEncabezadoContri+='<input type="text" id="nombre" name="tipoContr" class="form-control" disabled value = "'+nombreC+'">';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-6">';
          htmlEncabezadoContri+='<label class="control-spam col-md-4">APELLIDOS:</label>';
          htmlEncabezadoContri+='<input type="text" id="apellidos" name="tipoContr" class="form-control" disabled value = "'+apellidos+'">';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-2">';
          htmlEncabezadoContri+='<label class="control-spam col-md-4">CI:</label>';
          htmlEncabezadoContri+='<input type="text" id="ci" name="tipoContr" class="form-control" disabled value = "'+identificacion+'">';
          htmlEncabezadoContri+='</div>';     
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="row">';         
          htmlEncabezadoContri+='<div class="col-md-12">';
          htmlEncabezadoContri+='<div class="form-group col-md-4">';
          htmlEncabezadoContri+='<h5><b>DIRECCION:</b> <spam id="act_economica">'+direccion+' </spam></h5>';  
          htmlEncabezadoContri+='<h5><b>CELULAR:</b> <spam  id="act_desarrollada">'+celular+' </spam ></h5>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-4">';
          htmlEncabezadoContri+='<h5><b>PADRON: </b><spam  id="nit"> '+padron+'</spam ></h5>';
          htmlEncabezadoContri+='<h5><b></b><spam  id="inicio"></spam ></h5>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-4" id="personaJuridica">';
          htmlEncabezadoContri+='<h5><b></b><spam  id="nit"></spam ></h5>';
          htmlEncabezadoContri+='<h5><b></b><spam  id="inicio"> </spam ></h5>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div></div>';
          htmlEncabezadoContri+='</div>';
      var cedulaIdentidadPub=datosCont.documento+' '+datosCont.expedido;
     datosUsuarioPubli = '{"razonSocial":"'+nombreC+' '+apellidos+'","numeroIdentificacion":"'+cedulaIdentidadPub+'","tipoIdentificacion":"CI."}';
     datosUsuarioPubli = JSON.stringify(datosUsuarioPubli);
          $('#idContribuyente').html(htmlEncabezadoContri);

        }else if (contribuyente_tipo=='J')
        {
          var identificacion=datosCont.documento;
          htmlEncabezadoContri='';
          htmlEncabezadoContri+='<div class="panel panel-info">';
          htmlEncabezadoContri+='<div class="panel-heading">DATOS DEL CONTRIBUYENTE</div>';
          htmlEncabezadoContri+='<div class="panel-body">';
          htmlEncabezadoContri+='<div class="row">';
          htmlEncabezadoContri+='<div class="col-md-12">';
          htmlEncabezadoContri+='<div class="form-group col-md-4">';
          htmlEncabezadoContri+='<label class="control-spam col-md-8">NOMBRE REPRESENTANTE LEGAL:</label>';
          htmlEncabezadoContri+='<input type="text" id="nombre" name="tipoContr" class="form-control" disabled value = "'+representanteId+'">';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-2">';
                    htmlEncabezadoContri+='<h5><b>DIRECCION:</b> <spam id="act_economica">'+direccion+' </spam></h5>';  
          htmlEncabezadoContri+='<h5><b>CELULAR:</b> <spam  id="act_desarrollada">'+celular+' </spam ></h5>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-2">';
                    htmlEncabezadoContri+='<h5><b>PADRON: </b><spam  id="nit"> '+padron+'</spam ></h5>';
          htmlEncabezadoContri+='<h5><b>IDENTIFICAION: </b><spam  id="inicio"> '+identificacion+'</spam ></h5>';
          htmlEncabezadoContri+='</div>';     
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="row">';         
          htmlEncabezadoContri+='<div class="col-md-12">';
          htmlEncabezadoContri+='<div class="form-group col-md-4">';

          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-4">';

          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='<div class="form-group col-md-4" id="personaJuridica">';
          htmlEncabezadoContri+='<h5><b></b><spam  id="nit"></spam ></h5>';
          htmlEncabezadoContri+='<h5><b></b><spam  id="inicio"> </spam ></h5>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div>';
          htmlEncabezadoContri+='</div></div>';
          htmlEncabezadoContri+='</div>';
          var cedulaIdentidadPub=datosCont.documento;
     datosUsuarioPubli = '{"razonSocial":"'+empresa+'","numeroIdentificacion":"'+cedulaIdentidadPub+'","tipoIdentificacion":"NIT."}';
     datosUsuarioPubli = JSON.stringify(datosUsuarioPubli);
           $('#idContribuyente').html(htmlEncabezadoContri);
        }
        },
        error: function (xhr, status, error) {
          swal( "Error..!", "Error al obtener datos de contribuyente", "error" );
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo listar los datos", "error" );
    },
  });  
}




function reemplazaComas(numero,busca){
  while (numero.toString().indexOf(busca) != -1)
      numero = numero.toString().replace(busca,'');
  return numero;
}
//FUNCIONES PARA PAGINACION
//FUNCIONES PARA PAGINACION
var paging_pagenumbe = 1;
function btnAnterior_prin()
{      
paging_pagenumbe--;
buscarPatrimonioPorTipo( paging_pagenumbe);
}
function btnSiguiente_prin()
{
 paging_pagenumbe++;
buscarPatrimonioPorTipo(paging_pagenumbe);
}
function listar_select_pag_prin()
{
paging_pagenumbe=1;
buscarPatrimonioPorTipo(paging_pagenumbe);
}
/******/
function btnAnterior_campo_valor()
{      
paging_pagenumbe--;
buscarPatrimonioTipoCampo( paging_pagenumbe);
}
function btnSiguiente_campo_valor()
{
 paging_pagenumbe++;
buscarPatrimonioTipoCampo(paging_pagenumbe);
}
function listar_select_pag_campo_valor()
{
paging_pagenumbe=1;
buscarPatrimonioTipoCampo(paging_pagenumbe);
}