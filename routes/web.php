<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('frontend.bienvenida');
});

Route::get('/home', function () {
	return view('backend.template.app');
});
Route::post('sesion', [
	'as'   => 'login-post',
	'uses' => 'Auth\AuthController@postLogin',
]);
Route::post('sesion2', [
	'as'   => 'login-post2',
	'uses' => 'Auth\Auth2Controller@postLogin2',
]);
Route::get('sesion', [
	'as'   => 'cerrar',
	'uses' => 'Auth\AuthController@Login',
]);
//------------------ GENERADOR TOKEN ------------------\\ 
Route::get('prueba/VALLE/public/v0.0/getToken', 'Auth\LoginController@getToken');
//--------------------------------------------------------------------------------

//------------------ GENERADOR TOKEN FACTURACION------------------\\ 
Route::get('prueba/VALLE/public/v0.0/getTokenFac', 'Auth\LoginController@getTokenFac');
//--------------------------------------------------------------------------------

Route::group(array('middleware' => 'auth'), function () {

	Route::resource('Acceso', 'admin\gbAccesoController');
	Route::resource('Persona', 'admin\gbPersonaController');
	Route::resource('Usuario', 'admin\gbUsuarioController');
	Route::resource('Rol', 'admin\gbRolController');
	Route::resource('Grupo', 'admin\gbGrupoController');
	Route::resource('Opcion', 'admin\gbOpcionController');
	Route::resource('Asignacion', 'admin\gbAsignacionController');
	Route::resource('RolUsuario', 'admin\gbRolUsuarioController');
	Route::resource('AreaTrabajo', 'admin\gbAreaTrabajoController');
	Route::resource('RolAreaTrabajo', 'admin\gbRolAreTrabajoController');
	Route::get('lenguaje', 'MenuController@lenguaje');
	Route::resource('Acciones', 'admin\accionesController');
	Route::resource('AccionesRoles', 'admin\accionRolController'); 
		//----------------------Puesto via publica--------------------------
	


		//-----------------------------Cementerio------------------------
	


		//-----------------------------Parametricas------------------------
		/*
		 * abm Mercaderia
		 */
		Route::get('Mercaderia', 'parametricas\parametricasController@mercaderia');
		Route::get('MercaderiaAntes', 'parametricas\parametricasController@mercaderiaAntes');

		Route::get('CrudCaracteristicasPublicidad', 'parametricas\parametricasController@caracteristicasPublicidad');

		Route::get('CrudTipoActividad', 'parametricas\parametricasController@tipoactividad');
		Route::get('CrudCambio', 'parametricas\parametricasController@cambio');
		Route::get('CrudTipoMueble', 'parametricas\parametricasController@tipoMueble');
		Route::get('CrudTipoPuesto', 'parametricas\parametricasController@tipoPuesto');

		//-----------------------------Maestras----------------------------
		Route::get('CrudAsociacion', 'maestras\maestrasController@asociacion');
		Route::get('CrudMercado', 'maestras\maestrasController@mercado');
		Route::get('CrudContribuyente', 'maestras\maestrasController@contribuyente');

		//----------------------------Procesos-----------------------------
		Route::get('busquedas', 'procesos\procesosController@buscarContribuyente');
		Route::get('listActividadesEcoPorContri/{id}/{idEspacioTrabajo}', 'maestras\maestrasController@listActividadesEcoPorContri');	
		Route::get('listaPuestos/{id}/{padron}/{primerNombre}/{primerApellido}/{segundoApellido}/{carnetIdentidad}/{expedido}/{nit}/{tipoContr}/{espacioTrab}', 'maestras\maestrasController@listaPuestos');
		Route::get('LiquidacionPorActEconomica/{idAct}/{idEspacioTrabajo}/{idcontri}','maestras\maestrasController@LiquidacionPorActEconomica');
		Route::get('LiquidarDeudaActEconomica/{idAct}/{idCon}/{tipoLiquidacion}/{espaciotrabajo}','procesos\procesosController@LiquidarDeudaActEconomica');
		Route::get('CrudFumgestiones', 'procesos\procesosController@fumgestiones');
		Route::get('planPago/{datos}', 'procesos\procesosController@planPagos');

		
        //----------------------------Patrimonio Cultural----------------------
		Route::get('buscarPatrimonioCultural','patrimonioCultural\patrimonioCulturalController@buscarPatrimonioCultural');
		//--------------------------Espacios Abiertos----------------------//
		Route::get('indexEspacioAbierto', 'patrimonioCultural\patrimonioCulturalController@indexEspacioAbierto');
		Route::get('generarProformaEspacioAbierto','reportesPatrimonioCultural\reporteEspacioAbierto@getGenerar');
		Route::get('generarProformaEspacioAbiertoArchFoto','reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarArchFoto');
		Route::get('generarProformaEspacioAbiertoTodo','reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarFichaTodo');
		//--------------------------Conjuntos----------------------//
		Route::get('indexConjunto', 'patrimonioCultural\patrimonioCulturalController@indexConjunto');
		Route::get('generarProformaConjunto','reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerar');
		Route::get('generarProformaConjuntoArchFoto','reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerarArchFoto');
		Route::get('generarProformaConjuntoTodo','reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerarFichaTodo');
		//--------------------------Escultura----------------------//
		Route::get('indexEscultura', 'patrimonioCultural\patrimonioCulturalController@indexEsculturaMonumento');
		Route::get('generarProformaEscultura','reportesPatrimonioCultural\reporteEscultura@getGenerar');
		Route::get('generarProformaEsculturaArchFoto','reportesPatrimonioCultural\reporteEscultura@getGenerarArchFoto');
		Route::get('generarProformaEsculturaArchTodo','reportesPatrimonioCultural\reporteEscultura@getGenerarFichaTodo');
		//--------------------------Adjuntos patrimonio----------------------//
		Route::get('adjuntosPatrimonio/{casosId}/{numeroficha}','patrimonioCultural\patrimonioCulturalController@documentosAdjuntos');
		Route::get('adjuntosPatri','patrimonioCultural\patrimonioCulturalController@documentosAdjuntos2');
		Route::post('UpdatePersonaImg/','patrimonioCultural\UploadController@UpdatePersonaImg');
		Route::post('UpdatePersonaData/','patrimonioCultural\UploadControllerData@UpdatePersonaData');
		//--------------------------Bienes Arqueologicos----------------------//
		Route::get('indexArqueologicos','patrimonioCultural\patrimonioCulturalController@indexArqueologicos');
		Route::get('generarProformaBienArq','reportesPatrimonioCultural\reporteBienArqueologico@getGenerar');
		Route::get('generarProformaBienArqArchFoto','reportesPatrimonioCultural\reporteBienArqueologico@getGenerarArchFoto');
		Route::get('generarProformaBienArqTodo','reportesPatrimonioCultural\reporteBienArqueologico@getGenerarFichaTodo');
		//--------------------------Bien Inmaterial----------------------//
		Route::get('indexInmaterial','patrimonioCultural\patrimonioCulturalController@indexInmaterial');
		Route::get('generarProformaBienInmaterial','reportesPatrimonioCultural\reporteBienInmaterial@getGenerar');
		Route::get('generarProformaBienInmaterialArchFoto','reportesPatrimonioCultural\reporteBienInmaterial@getGenerarArchFoto');
		Route::get('generarProformaBienInmaterialTodo','reportesPatrimonioCultural\reporteBienInmaterial@getGenerarFichaTodo');
		//--------------------------Bien Inmueble----------------------//
		Route::get('indexInmuebles','patrimonioCultural\patrimonioCulturalController@indexInmuebles');
		Route::get('generarProformaBienInmueble','reportesPatrimonioCultural\reporteBienInmueble@getGenerar');
		Route::get('generarProformaBienInmuebleArchFoto','reportesPatrimonioCultural\reporteBienInmueble@getGenerarArchFoto');
		Route::get('generarProformaBienInmuebleTodo','reportesPatrimonioCultural\reporteBienInmueble@getGenerarFichaTodo');
		//------------------------reporte pivote-------------------------//
		Route::get('reportePivote','reportePivote\reportePivoteController@reportePivote');


        //--------------------------------------------------------------------

		/* atm*/

        //--------------------------------------------------------------------

     

		/** Ruta para el Voucher PDF **/
		//Route::get('Voucher_Normal','pagos\voucherPDFController@getGenerar');
		//Route::get('Voucher_Detallado','pagos\voucherPDFController@getGenera');

		Route::get('inicio', function () {
			echo 'Bienvenido ';

				// Con la función Auth::user() podemos obtener cualquier dato del usuario
				// que este en la sesión, en este caso usamos su correo y su id
				// Esta función esta disponible en cualquier parte del código
				// siempre y cuando haya un usuario con sesión iniciada
			echo 'Bienvenido '.Auth::user()->usr_usuario.', su Id es: '.Auth::user()->usr_id;
		});
		Route::get('close', [
			'as'   => 'cerrar',
			'uses' => 'Auth\AuthController@close',
		]);

		Route::get('codigoQR','admin\accionesController@obtenerCodigoQR');
		Route::post('/codigoQRfum','admin\accionesController@obtenerCodigoQR2');

		// render
		Route::get('render','render\renderController@render');

	});