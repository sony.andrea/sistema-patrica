<?php

namespace gamlp\Http\Controllers\admin;
use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\Auth\AuthController;
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

class accionRolController extends Controller
{
    public function index() {
			    $tk=LoginController::getToken();
			    $idus =  Auth::user()->usr_id;
				return view('backend.administracion.admin.gbAcciones_Rol.index',  compact('tk','idus'));
		
	}

}