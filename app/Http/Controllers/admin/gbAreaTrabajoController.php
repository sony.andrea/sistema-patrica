<?php

namespace gamlp\Http\Controllers\admin;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\Auth\AuthController;
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use gamlp\Modelo\admin\AreaTrabajo;

use Redirect;
use Yajra\Datatables\Datatables;

class gbAreaTrabajoController extends Controller
{
    //
   public function index(){
    	
   		$areaTrabajo = AreaTrabajo::getListar();
		
		//dd($areaTrabajo);
		return view('backend.administracion.admin.gbAreaTrabajo.index', compact('areaTrabajo'));
       	/*$tk=LoginController::getToken();
    	$usrid =  Auth::user()->usr_id;	
   		return view('backend.administracion.admin.gbAreaTrabajo.index', compact('tk','usrid'));*/
   	}
   	public function create() {
		$areaTrabajo = AreaTrabajo::getListar();
		return Datatables::of($areaTrabajo)->addColumn('acciones', function ($areaTrabajo) {
				return '<button value="'.$areaTrabajo->espacio_trabajo_id.'" class="btncirculo btn-xs btn-primary" style="background:#57BC90" onClick="Mostrar(this);" data-toggle="modal" data-target="#myUpdate"><i class="fa fa-pencil-square"></i></button>
            <button value="'.$areaTrabajo->espacio_trabajo_id.'" class="btncirculo btn-xs btn-warning" style="background:#7ACCCE" onClick="Eliminar(this);"><i class="fa fa-trash-o"></i></button>';
			})
			->editColumn('id', 'ID: {{$espacio_trabajo_id}}')
			->make(true);
	}
	public function store(Request $request) {
		AreaTrabajo::create([
				'espacio_trabajo_descripcion'    => $request['espacio_trabajo_descripcion'],
				'espacio_trabajo_usr_id' => 1,
				'espacio_trabajo_estado' => 'A',
			]);
		return redirect()->route('AreaTrabajo.index')->with('success', 'El rol se registro correctamente');
	}

	public function show($id) {

	}

	public function edit($id) {

		$AreaTrabajo = AreaTrabajo::setBuscar($id);
		return response()->json($AreaTrabajo);
	}

	public function update(Request $request, $id) {

		$AreaTrabajo = AreaTrabajo::setBuscar($id);

		$AreaTrabajo->fill($request->all());
		$AreaTrabajo->save();
		return response()->json($AreaTrabajo->toArray());
	}

	public function destroy($id) {
		$AreaTrabajo = AreaTrabajo::getDestroy($id);
		return response()->json($AreaTrabajo);

	}
}
