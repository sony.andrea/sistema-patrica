<?php

namespace gamlp\Http\Controllers\admin;
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

class gbusuarioUnidadRecaudadoraController extends Controller
{
      public function index() {
      	$tk=LoginController::getToken();
      	$idus =  Auth::user()->usr_id;;
		return view('backend.administracion.admin.gbUsuarioUnidadRecaudadora.index', compact ('tk','idus'));
	}

}
