<?php

namespace gamlp\Http\Controllers\admin;

use Auth;
use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;

use gamlp\Modelo\admin\RolAreaTrabajo;
use gamlp\Modelo\admin\AreaTrabajo;
use gamlp\Modelo\admin\Rol;
use gamlp\Modelo\admin\RolUsuario;
use gamlp\Modelo\admin\Usuario;

use Redirect;
use Session;

class gbRolAreTrabajoController extends Controller
{
    //

/*    public function submenus() {

		$ids     = Auth::user()->usr_id;
		$usuario = RolUsuario::getusuarios($ids);
		$rolareatrabajo = RolAreaTrabajo::getListarws($usuario);
		return $rolareatrabajo;
	}*/

    public function index(){
    	$areatrabajo    = AreaTrabajo::getListar();
		$rol        = Rol::getRolUser();
		$rolareatrabajo = RolAreaTrabajo::getListar();
		//dd($rol);
		//dd($areatrabajo);
		//dd($rolareatrabajo);
		return view('backend.administracion.admin.gbRolAreatrabajo.index', compact('areatrabajo', 'rol', 'rolareatrabajo'));
   	}

   	public function create() {

		return $rolUser = AreaTrabajo::getListar();
	}

	public function store(Request $request) {
		$arr_rol = $request['rol'];
		if (isset($_POST['rolatnoasignado'])) {
			$arr_opc = $_POST['rolatnoasignado'];
			for ($i = 0; $i < count($arr_opc); $i++) {
				RolAreaTrabajo::create([
						'espacio_trabajo_detalle_rol_id'          	 => $arr_rol[0],
						'espacio_trabajo_detalle_espacio_trabajo_id' => $arr_opc[$i],
						'espacio_trabajo_detalle_usr_id' 			 => Auth::user()->usr_id,
					]);
			}Session::flash('message', 'Asignación Correcta.');
			//$user = $arr_user[0];
			$rol_id = $arr_rol[0];
			//$rolusuario = AreaTrabajo::getlistar1($arr_user);
			$areatrabajo    = AreaTrabajo::getListar();
			$rol        = Rol::getRolUser();
			$rolareatrabajo = RolAreaTrabajo::getListar1($arr_rol);
			//dd($arr_rol);
			return view('backend.administracion.admin.gbRolAreatrabajo.index', compact('areatrabajo', 'rol', 'rolareatrabajo'));
		} 
		else {
			//dd($_POST['rolasignadoat']);
			if (isset($_POST['rolasignadoat'])) {
				$arr_asig = $_POST['rolasignadoat'];
				//dd($arr_asig);
				for ($i = 0; $i < count($arr_asig); $i++) {
					RolAreaTrabajo::where('espacio_trabajo_detalle_id', $arr_asig[$i])
						->update(['espacio_trabajo_detalle_estado' => 'B']);
				  }
				//Session::flash('message', 'Desasignación Correcta.');
				$areatrabajo    = AreaTrabajo::getListar();
				//dd($areatrabajo);
				$rol        = Rol::getRolUser();
				//dd($rol);
				$rolareatrabajo = RolAreaTrabajo::getListar1($arr_rol);
				//dd($rolareatrabajo);
				return view('backend.administracion.admin.gbRolAreatrabajo.index', compact('areatrabajo', 'rol', 'rolareatrabajo'));
			} else {

			}
		}

	}
    /*public function destroy($id)
    {
        $rolatrab=provincia::getDestroy($id);
    return response()->json(['messaje'=> 'Eliminado Exitosamente']);    
    }*/
}
