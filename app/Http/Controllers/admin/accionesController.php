<?php

namespace gamlp\Http\Controllers\admin;
use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\Auth\AuthController;
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

class accionesController extends Controller
{
    public function index() {
			    $tk=LoginController::getToken();
			    $idus =  Auth::user()->usr_id;
				return view('backend.administracion.admin.gbAcciones.index',  compact('tk','idus'));		
	}
	public function obtenerCodigoQR(){
	    $array = array("Ana", "Maria", "Juan");
		$imagen = QRcode($array);
		return view('codigoQR.codigo', compact('imagen'));
	}
	public static function obtenerCodigoQR2(Request $request){
		$data = array($request['datoFum']);   
		
		$array = array("Ana", "Maria", "Juan");
	    $imagen = QrCode ($data);		
		return $imagen;
	}
	
}