<?php
namespace gamlp\Http\Controllers\admin;

use Auth;
use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\admin\EstadoCivil;
use gamlp\Modelo\admin\Persona;
use gamlp\Modelo\admin\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class gbPersonaController extends Controller {
	public function index() {
		$pers1   = Persona::OrderBy('prs_id', 'desc')->pluck('prs_nombres', 'prs_id');
		$usuario = Usuario::OrderBy('usr_id', 'desc')->pluck('usr_usuario', 'usr_id');
		$data    = EstadoCivil::combo();
		return view('backend.administracion.admin.gbPersonas.index', compact('pers1', 'usuario', 'data'));
	}

	public function create() {
		$personas = Persona::getListar();
		return Datatables::of($personas)->addColumn('acciones', function ($persona) {
				return '<button value="'.$persona->prs_id.'" class="btncirculo btn-xs btn-primary" style="background:#57BC90" onClick="MostrarPersona(this);" data-toggle="modal" data-target="#myUpdate"><i class="fa fa-pencil-square"></i></button>
            <button value="'.$persona->prs_id.'" class="btncirculo btn-xs btn-warning" style="background:#7ACCCE" onClick="Eliminar(this);"><i class="fa fa-trash-o"></i></button>';
			})
			->editColumn('id', 'ID: {{$prs_id}}')	->
		addColumn('nombreCompleto', function ($nombres) {
				return $nombres->prs_nombres.' '.$nombres->prs_paterno.' '.$nombres->prs_materno;
			})
			->editColumn('id', 'ID: {{$prs_id}}')
			->make(true);
	}

	public function store(Request $request) {
		Persona::create([
				'prs_nombres'            => $request['prs_nombres'],
				'prs_paterno'            => $request['prs_paterno'],
				'prs_materno'            => $request['prs_materno'],
				'prs_ci'                 => $request['prs_ci'],
				'prs_direccion'          => $request['prs_direccion'],
				'prs_direccion2'         => $request['prs_direccion2'],
				'prs_telefono'           => $request['prs_telefono'],
				'prs_telefono2'          => $request['prs_telefono2'],
				'prs_celular'            => $request['prs_celular'],
				'prs_empresa_telefonica' => $request['prs_empresa_telefonica'],
				'prs_correo'             => $request['prs_correo'],
				'prs_id_estado_civil'    => $request['prs_id_estado_civil'],
				'prs_sexo'               => $request['prs_sexo'],
				'prs_fec_nacimiento'     => $request['prs_fec_nacimiento'],
				'prs_id_archivo_cv'      => 1,
				'prs_usr_id'             => Auth::user()->usr_id,
			]);

		return response()->json(['Mensaje' => 'Se registro correctamente']);
	}

	public function edit($id) {

		$persona = Persona::find($id);

		$data = \DB::table('_bp_estados_civiles')
			->select('estcivil_id', 'estcivil')
			->where('estcivil_estado', 'A')
			->get();

		return response()->json($persona->toArray());
	}

	public function update(Request $request, $id) {
		$persona = Persona::find($id);
		$persona->fill($request->all());
		$persona->save();
		return response()->json(['mensaje' => 'Se actualizo la persona']);
	}

	public function show($id) {

	}

	public function destroy($id) {
		$persona = Persona::getDestroy($id);
		return response()->json(['mensaje' => 'Se elimino correcta,ente']);
	}

}
