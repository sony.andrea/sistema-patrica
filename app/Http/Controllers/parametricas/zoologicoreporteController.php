<?php
namespace gamlp\Http\Controllers\parametricas;
use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\validacionController;
use PDF;

class zoologicoreporteController extends Controller
{
  
  public function getGenerar(Request $request){    
    $datosD = $request->get('datos');
    //dd($datosD);
    $datosD = json_decode($datosD);
    //$datosD = json_decode($datosD);
    $j = 0;
    $fechaini = $datosD[$j]->{'fechax'};
    $fechafin = $datosD[$j]->{'fechay'};
    $datosReporte = $this->obtieneReporte($fechaini,$fechafin);
   // $datosFum=$datosD->{'datoFum'};
    //$datosUsuario=$datosD->{'datosUsuario'};
    return $this->pdf($datosReporte,$fechaini, $fechafin);
  }

  public static function getToken(){
    $user = 'administrador';
    $pass = '123456';
    $url  = 'http://192.168.6.113/MotorServicio/public/api/apiLogin';
    $data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/json",
            'method'  => 'POST',
            'content' => $data
        )
    );
    $context  = stream_context_create($options);
    $result   = file_get_contents($url, false, $context);
    if ($result === FALSE) {
      dd('error request');
     }
     $tokenArray=explode('"', $result);
      return $tokenArray[3];
  }
  ///
  public function obtieneReporte($fechax,$fechay){

    $token = $this->getToken();
    $url  = 'http://192.168.6.113:80/MotorServicio/public/api/reglaNegocio/ejecutarWeb';
    $parametros = '{"fechax":"'.$fechax.'","fechay":"'.$fechay.'"}';
    $parametros = json_encode($parametros);
    $data = '{"identificador":"SERVICIO_VALLE-466","parametros":'.$parametros.'}';
    $options = array(
        'http' => array(
            'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
            'method'  => 'POST',
            'content' => $data
        )
    );
    $context  = stream_context_create($options);
    $result   = file_get_contents($url, false, $context);
    //dd($result);

    if ($result === FALSE) {
      dd('error request');
    }
    return $result;
  }

  public function pdf($datosBoleto,$fechaini, $fechafin){
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->useOddEven = 1;
    $datosBoleto=json_decode($datosBoleto);
    //dd($datosBoleto);
    $todo = $datosBoleto[0]->{'xtodo'};
    // dd($todo);
    $boleto = json_decode($todo);
    //  dd($boleto);
    $operador =$boleto->{'boleto_data'};
    $operadorNombre =$operador->{'nombreoOperedor'};
    $operadorCi =$operador->{'ci'};
    $html='<table width="100%">
      <tr>
        <td align="right"> 
          <center>Direcci&oacute;n de Desarrollo Organizacional<br>Unidad de Administraci&oacute;n y <br>Desarrollo de Sistemas
          <hr align="right" style="border-top: 3px double #8c8b8b; width: 500px;
            margin: 10px 0 0 100px;"><br>
          <label><h4>REPORTE RECAUDACI&Oacute;N ZOOLOGICO</h4></label>
          <label ><h4 align="right">DE FECHA '.$fechaini.' A '. $fechafin.' </h4></label>
        </td>
        <td>
          <center><h5> GOBIERNO AUT&Oacute;NOMO MUNICIPAL DE LA PAZ </h5> </center>
        </td>
        <div>imagen</div>
      </tr>
      <tr>
        <td colspan=2>
        <div ><hr align="center"></div>
        </td>
      </tr>
      </table>
      <div style="margin: 0px 0px 5px 20px;font-size:12px;font-weight: bold;"><h
    3>DATOS DEL USUARIO MUNICIPAL</h3></div>
      <div class="col-md-12" style="border-radius: 15px; border: 2px solid #696961; width: 1210px; margin: 0px 0 5px 15px; height: 30px;"> <br>
       <table width="100%" style="font-size:12px;">
        <tr>
          <td></td><td></td>
          <td ><label><b> NOMBRE COMPLETO: </b></td>
          <td ><label> '.$operadorNombre.'</td>
          <td><b><label>CI.</b>:</td>
          <td>'.$operadorCi.'</td>
          <td></td>
        </tr>
      </table> <br>
    </div><br>
      <div style="margin: 0px 20px 5px 20px;font-size:12px;font-weight: bold;"><label><h
    3>DETALLE</h3></label></div>
      <div class="col-md-12" style="border-radius: 15px; border: 2px solid #696961; width: 1210px; margin: 5px 20px 5px 15px; height: 50px;"> <br>
        <table width="100%" style="font-size:12px;" border="1">
        <tr>  
          <td align="center" ><b>NRO. </b></td>
         	<td align="center"><b> FECHA</b> </b></td>
         	<td align="center"><b> NOMBRE DEL CIUDADANO</b></td>
         	<td align="center"><b> TOTAL</b> </b></td>      
        </tr>';
        $longitudBoleto=sizeof($datosBoleto);
        $montoTotalrepot =0;
        for ($i = 0; $i < $longitudBoleto; $i++){
            //$monto=number_format($dataTrans[$i]->{'monto'},2);
            $dataTrans = $datosBoleto[$i]->{'xtodo'};
            $dataTrans = json_decode($dataTrans);
            $boleto = $datosBoleto[$i]->{'xtodo'};
            $boleto = json_decode($boleto);
            $boleto= $boleto->{'boleto_data'};
            $ciudadano= $boleto->{'datos_ciudadano'};
            $boletos= $boleto->{'datos_boleto'};
            $tama = sizeof($boletos);
   			    $c=$i+1;
            $html.='<tr>
            <td align="center">'.$c.'</td>
            <td align="center">'.$dataTrans->{'fechax'}.'</td>
				    <td align="center"> <b> '.$ciudadano->{'nombre'}.' '.$ciudadano->{'apellidop'}.' '.$ciudadano->{'apellidom'}.'</b> </td>
                    <table width="100%" style="font-size:12px;" border="1">';
                      $html.=' <tr>
                      <td align="left"><b> ITEMS</b>  </td>
                      <td align="center"><b> PRECIO </b> </td>
                      <td align="center"><b> CANTIDAD</b> </td>
                      <td align="center"><b> MONTO</b> </td></tr>';
                      $j = 0;
                      $montoTotalrepot = $montoTotalrepot + $boleto->{'montoTotal'};
                      for ($j  ; $j < $tama; $j++){
                        $html.=' <tr>
                        <td align="left">'.$boletos[$j]->{'descripcion_ue'}.'</td>
                        <td align="right">'.$boletos[$j]->{'ur_codigo'}.'</td>
                        <td align="right">'.$boletos[$j]->{'ur_codigo'}.'</td>
                        <td align="right">'.$boletos[$j]->{'ur_codigo'}.'</td></tr>';
                      }
                      $html.='</table>
                      <td align="right" > <b><u>'.$boleto->{'montoTotal'}.'.00</u></b></td>
                      </tr>'; 

        } 
        $montoforma=number_format($montoTotalrepot,2);
        $montoTotallite = validacionController::valorEnLetras($montoTotalrepot);
        $html.='</table> 
        <table width="100%" style="font-size:11px;">
        <tr>
          <td colspan=5>
          <div ><hr align="center"   style="border-top: 3px double #8c8b8b; width: 1210px; margin: 0 0 0 100px;"></div>
          </td>
        </tr>
        <tr>
          <td width="30%"><b>MONTO TOTAL DEL REPORTE</td><td  width="40%" colspan=3 align="right"><b><h3> '.$montoforma.' Bs. </h3></b></td></tr>
          <tr>
            <td width="2%"></td>
            <td colspan = 3 width="85%"><h3> '.$montoTotallite.' Bs.<h3></td>
            <td width="3%"></td>
        </tr>
        </table> 
      </div>
      <div style="margin: 0px 0px 0px 15px">';
  $html.='';
  $mpdf->AddPage();
  $mpdf->WriteHTML($html);  
  $mpdf->Output();
  }
}