<?php

namespace gamlp\Http\Controllers\parametricas;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use gamlp\Http\Controllers\Auth\AuthController;
use gamlp\Http\Controllers\Auth\LoginController;


class parametricasController extends Controller
{
	public function mercaderiaAntes()
	{
		$usuarioid =  Auth::user()->usr_id;
		return view('backend.parametricas.mercaderia.index',compact('usuarioid'));
	}
	public function mercaderia()
	{
		$usuarioid =  Auth::user()->usr_id;
		return view('backend.mercados.mercaderia.index',compact('usuarioid'));
	}
	public function tipoactividad() {
		$usid =  Auth::user()->usr_id;;
		//return view('backend.prueba.hola', compact ('tk','usid'));
		return view('backend.parametricas.tipoActividad.index', compact ('tk','usid'));
	}
	public function caracteristicasPublicidad(){
  	$tk=LoginController::getToken();
  	$usrid =  Auth::user()->usr_id;
  	return view('backend.parametricas.caracteristicasPublicidad.index', compact('tk','usrid'));
	}
	public function cambio(){
  $usuarioid =  Auth::user()->usr_id;
  return view('backend.parametricas.cambio.index', compact('usuarioid'));
	}
	public function zoologico() {
		$usuarioid =  Auth::user()->usr_id;
		return view('backend.parametricas.zoologico.index2',compact('usuarioid'));
	}
	public function boleto() {
		$usuarioid =  Auth::user()->usr_id;
		return view('backend.parametricas.zoologico.index',compact('usuarioid'));
	}
	public function reportes() {
		$usuarioid =  Auth::user()->usr_id;
		return view('backend.parametricas.zoologico.reportes',compact('usuarioid'));
	}
	
	public function tipoMueble() {
		$usuarioid =  Auth::user()->usr_id;
		return view('backend.parametricas.tipoMueble.index',compact('usuarioid'));
	}
	
	public function tipoPuesto() {
		$usuarioid =  Auth::user()->usr_id;
		return view('backend.parametricas.tipoPuesto.index',compact('usuarioid'));
	}
}
