<?php

namespace gamlp\Http\Controllers\servicios;
use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\admin\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiAuthController extends Controller {
	//ENVIO POST LOGIN GENERANDO UN TOKEN DE SESSION PARA EL USUARIO

	public function getLogin() {
		return view('home.login');
	}
	public function postLogin(Request $request) {
		try {

		} catch (HttpResponseException $e) {
			return response()->json(['error' =>
					['message'                     => 'auth_invalida',
						'codigo_de_estado'            => IlluminateResponse::HTTP_BAD_REQUEST
					]],
				IlluminateResponse::HTTP_BAD_REQUEST,
				$headers = []
			);
		}
		try {
			if (!$token = JWTAuth::attempt(array('usr_usuario' => Input::get('usr_usuario'), 'usr_clave' => Input::get('usr_clave')))) {
				return response()->json(['error'                  => 'Las credenciales son invalidas!'], 400);
			}
		} catch (JWTException $e) {
			return response()->json(['error' => 'ups hubo un problema!'], 500);
		}
		return response()->json(compact('token'));
	}

	//CERRAR SESION REFRESCANDO EL TOKEN

	public function logout(Request $request) {
		$this->validate($request, [
				'token' => 'required'
			]);
		JWTAuth::invalidate($request->input('token'));
	}

	public function create_user(Request $data) {
		try {
			$user = User::create([
					'usr_usuario' => $data['usuario'],
					'usr_clave'   => bcrypt($data['password']),
					'usr_prs_id'  => $data['ciu_id'],
					'usr_usr_id'  => 1
				]);
			session::flash("message", "Se ha registrado el usuario ".$user->usr_usuario." de manera exitosa!");
			return response()->json(["Respuesta" => $user]);
		} catch (Exception $e) {
			return response()->json(["Respuesta" => "error en el registro"]);
		}
	}

}