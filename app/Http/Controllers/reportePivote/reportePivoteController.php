<?php


namespace gamlp\Http\Controllers\reportePivote;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use Carbon\Carbon;
use gamlp\Http\Controllers\Auth\AuthController; 
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

class reportePivoteController extends Controller
{
    public function reportePivote() {
		$usuarioId =  Auth::user()->usr_id;
		//dd('sasssssssssssss');
		return view('backend.patrimonioCultural.reportePivote.reportePivote',compact('usuarioId'));
	}  
}
