<?php

namespace gamlp\Http\Controllers\patrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use Carbon\Carbon;
use gamlp\Http\Controllers\Auth\AuthController; 
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

class UploadControllerData extends Controller
{
    public function UpdatePersonaData(Request $request)
    {
        //dd($request);
        $usuarioId =  Auth::user()->usr_id;
        $prs_imagen = '';
        $imagen =null;
        $nombre = null;
        $prs_img = null;
        $prs_imgInsert = null;
        if($request->hasFile('aimagen1'))
        {   
            /*$this->validate($request, [
                'aimagen1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);*/
             $imagen = $request->file('aimagen1');           
        }

            $xdoc_cuerpo = $request['atip_data'];  
            $xcg_tipo = $request['aag_tipo'];
            $xdoc_correlativo = 0;
            $xdoc_tipo_documentacion = 0;
      

            if ($xcg_tipo == 'PTR_CON') {

                $parteimage = 'con';
                $xdoc_proceso = '26';
                $doc_ci_nodo = 'Conjuntos';
                $prs_ci_carpeta_pri='CONJUNTOS';                    
                $parteimage = 'con';
                if ( $xdoc_cuerpo == 'Fotografias') {    
                    
                    $xdoc_tipo_documentacion = $request['aposicionconjunto'];
 
                    if ($xdoc_tipo_documentacion == 'Imagen 1 perfil urbano') {
                        $xdoc_correlativo = 1;                        
                    }
                    if ($xdoc_tipo_documentacion == 'Imagen 2 perfil urbano') {
                        $xdoc_correlativo = 2;
                    }
                    if ($xdoc_tipo_documentacion =='Perfil corte esquematico') {
                        $xdoc_correlativo = 3;                         
                    }                       
                    if ($xdoc_tipo_documentacion =='Fotografia 1') {
                        $xdoc_correlativo = 4;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 2') {
                        $xdoc_correlativo = 5;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 3') {
                        $xdoc_correlativo = 6;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 4') {
                        $xdoc_correlativo = 7;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 5') {
                        $xdoc_correlativo = 8;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 6') {
                        $xdoc_correlativo = 9;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Otro') {
                        $xdoc_correlativo = 'Otro';
                        
                    }
                }
            }
           
            if ($xcg_tipo == 'PTR_IME') {
               $parteimage = 'ime';
               $xdoc_proceso = '28';
               $doc_ci_nodo = 'Monumentos y Esculturas';                
               $xdoc_tipo_documentacion = $request['aposicionescultura'];
               //dd($xdoc_tipo_documentacion);
               $prs_ci_carpeta_pri='ESCULTURAS';

                if ( $xdoc_cuerpo == 'Fotografias') {   
 
                     if ($xdoc_tipo_documentacion == 'Fotografia Frente') {
                        $xdoc_correlativo = 1;
                         
                    }
                    if ($xdoc_tipo_documentacion == 'Fotografia Perfil') {
                        $xdoc_correlativo = 2;
                         
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia incluyendo pedestal y placas') {
                        $xdoc_correlativo = 3;
                         
                    }
                     if ($xdoc_tipo_documentacion =='Plano ubicacion') {
                        $xdoc_correlativo = 4;
                         
                    }

                     if ($xdoc_tipo_documentacion =='Fotografia 1') {
                        $xdoc_correlativo = 5;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 2') {
                        $xdoc_correlativo = 6;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 3') {
                        $xdoc_correlativo = 7;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 4') {
                        $xdoc_correlativo = 8;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 5') {
                        $xdoc_correlativo = 9;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 6') {
                        $xdoc_correlativo = 10;
                        
                    }

                    if ($xdoc_tipo_documentacion =='Otro') {
                        $xdoc_correlativo = 'Otro';
                        
                    }
                }
         }
         //dd($xdoc_tipo_documentacion);

            if ($xcg_tipo == 'PTR-ESP') {

                $parteimage = 'esp';
                $xdoc_proceso = '27';
                $doc_ci_nodo = 'Espacio Abierto';
                $xdoc_tipo_documentacion = $request['aposicionespacioabierto'];
                $prs_ci_carpeta_pri='ESPACIOABIERTO';
                if ( $xdoc_cuerpo == 'Fotografias') {   

                     if ($xdoc_tipo_documentacion =='Plano de ubicacion') {
                        $xdoc_correlativo = 1;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Esquema arquitectonico') {
                        $xdoc_correlativo = 2;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Angulo Visual Horizontal') {
                        $xdoc_correlativo = 3;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 1') {
                        $xdoc_correlativo = 4;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 2') {
                        $xdoc_correlativo = 5;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 3') {
                        $xdoc_correlativo = 6;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 4') {
                        $xdoc_correlativo = 7;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 5') {
                        $xdoc_correlativo = 8;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 6') {
                        $xdoc_correlativo = 9;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Otro') {
                        $xdoc_correlativo = 'Otro';
                        
                    }
                }
   
            }
            if ($xcg_tipo == 'PTR_BMA') {
                $xdoc_tipo_documentacion = $request['aposicionesBienArq'];
                 $parteimage = 'bma';
                //dd($xdoc_tipo_documentacion);
                $xdoc_proceso = '29';
                $doc_ci_nodo = 'Bienes Muebles Arqueologicos';
                $prs_ci_carpeta_pri='BIENESARQUEOLOGICOS';

        //dd($xdoc_tipo_documentacion);
            if ( $xdoc_cuerpo == 'Fotografias') {  
 
                    if ($xdoc_tipo_documentacion =='Fotografia del Plano de Ubicacion') {
                        $xdoc_correlativo = 1;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 1') {
                        $xdoc_correlativo = 2;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 2') {
                        $xdoc_correlativo = 3;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 3') {
                        $xdoc_correlativo = 4;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 4') {
                        $xdoc_correlativo = 5;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 5') {
                        $xdoc_correlativo = 6;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 6') {
                        $xdoc_correlativo = 7;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Otro') {
                        $xdoc_correlativo = 'Otro';            
                    }
                }
           
            }
            if ($xcg_tipo == 'PTR-PCI') {
            $parteimage = 'pci';
            $xdoc_tipo_documentacion = $request['aposicionesPatInmaterial'];
            $doc_ci_nodo = 'Patrimonio Inmaterial';
            $xdoc_proceso = '30';
            $prs_ci_carpeta_pri='PATRIMONIOINMATERIAL';

            if ( $xdoc_cuerpo == 'Fotografias') { 

 
                   if ($xdoc_tipo_documentacion =='Fotografia Principal 1') {
                        $xdoc_correlativo = 1;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Fotografia Principal 2') {
                        $xdoc_correlativo = 2;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Fotografia 1') {
                        $xdoc_correlativo = 3;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 2') {
                        $xdoc_correlativo = 4;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 3') {
                        $xdoc_correlativo = 5;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 4') {
                        $xdoc_correlativo = 6;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 5') {
                        $xdoc_correlativo = 7;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 6') {
                        $xdoc_correlativo = 8;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Otro') {
                        $xdoc_correlativo = 'Otro';
                        
                    }
                }
            }
            if ($xcg_tipo == 'PTR_CBI') {
                $parteimage = 'cbi';
                $xdoc_tipo_documentacion = $request['aposicionesBienInmueble'];
                $xdoc_proceso = '31';
                $doc_ci_nodo = 'Bienes Inmuebles';
                $prs_ci_carpeta_pri = 'BIENESINMUEBLES';                
          

             if ( $xdoc_cuerpo == 'Fotografias') {  
 
                     if ($xdoc_tipo_documentacion =='Foto Principal') {
                        $xdoc_correlativo = 1;
                        
                    }
                    if ($xdoc_tipo_documentacion =='Plano de Ubicacion') {
                        $xdoc_correlativo = 2;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Tipologia Arquitectonica') {
                        $xdoc_correlativo = 3;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Perfil de Acera') {
                        $xdoc_correlativo = 4;
                        
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 1') {
                        $xdoc_correlativo = 5;
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 2') {
                        $xdoc_correlativo = 6;
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 3') {
                        $xdoc_correlativo = 7;
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 4') {
                        $xdoc_correlativo = 8;
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 5') {
                        $xdoc_correlativo = 9;
                    }
                     if ($xdoc_tipo_documentacion =='Fotografia 6') {
                        $xdoc_correlativo = 10;
                    }
                    if ($xdoc_tipo_documentacion =='Otro') {
                        $xdoc_correlativo = 'Otro';            
                    }
                }
             
            }

            if ($xdoc_cuerpo == 'Fotografias') {  
                $xdoc_tps_doc_id = 1;
                $carpeta_tipo = 'FOTOS';
            }
            if ( $xdoc_cuerpo == 'Autocad') {  
                $xdoc_tps_doc_id = 2;    
                $carpeta_tipo = 'AUTOCAD';
                $xdoc_correlativo =' ';       
            }
            if ( $xdoc_cuerpo == 'Texto') {  
                $xdoc_tps_doc_id = 4;
                $carpeta_tipo = 'TEXTO';    
                $xdoc_correlativo =' ';       
            }
                
            $numeroficha = $request['anumeroficha'];
            $casosId = $request['acasosId'];
            $xdoc_titulo  = $request['atitulo'];
            $xdoc_datos  = $request['adescripcion'];
            /*$xdoc_usuario = $request['cusuario_avatarad'];*/

            $parteimage = "";
            $idAdjunto = $request['aidAdjunto'];

        if($imagen != null){
            //$ruta = '/adjuntos/'.$prs_ci;
            //$rutaInsert = 'adjuntos/'.$prs_ci;
            $xdoc_tamanio_documento = filesize($imagen);
            $ruta = '/PATRIMONIO/'.$prs_ci_carpeta_pri.'/'.$numeroficha.'/'.$carpeta_tipo;
            //$rutaInsert = 'adjuntos/'.$prs_ci;
            $rutaInsert = 'PATRIMONIO/'.$prs_ci_carpeta_pri.'/'.$numeroficha.'/'.$carpeta_tipo;
            $xdoc_tipo_documento = $imagen->guessExtension();
            $hoy = getdate();
            $year = $hoy['year'];
            $month = $hoy['month'];
            $mday = $hoy['mday'];
            $hours = $hoy['hours'];
            $minutes = $hoy['minutes'];
            $seconds = $hoy['seconds'];
            $nombre = $imagen->getClientOriginalName();
            $nombre = $parteimage.$year.$hours.$minutes.$seconds.$nombre;        
            //$nombre = $year.$month.$mday.$hours.$minutes.$seconds.$nombre;
            $imagen->move(getcwd().$ruta, $nombre);
            $prs_img =$ruta.'/'.$nombre; 
            $prs_imgInsert =$rutaInsert.'/'.$nombre;
            $xdoc_url = $prs_imgInsert;
        }else{
            $ruta = '';
            $rutaInsert = '';
            $nombre = null;
            $xdoc_tipo_documento = null;//**HAY QUE RECUPERAR**///
            $xdoc_url =null; //**HAY QUE RECUPERAR***// 
            $xdoc_tamanio_documento=null;
            $prs_imgInsert=null; //doc_url_logica
        }

               
        $datos = $this->actualizarAdjuntosPatrimonio($idAdjunto,$xdoc_datos,$xdoc_titulo,$xdoc_url,$usuarioId,$xdoc_tipo_documento,$xdoc_tamanio_documento, $nombre, $xdoc_tps_doc_id, $prs_imgInsert, $xdoc_cuerpo, $xdoc_tipo_documentacion, $xdoc_correlativo);        
        //dd($prs_imagen);        
        return view('backend.patrimonioCultural.adjuntosPatrimonio.documentosAdjuntos', compact('casosId','usuarioId','numeroficha'));
    }
    public static function getToken()
    {
        $user = 'administrador';
        $pass = '123456';
        $url  = 'http://172.19.161.3/api/apiLogin';
        $data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/json",
                'method'  => 'POST',
                'content' => $data
            )
        );
        $context  = stream_context_create($options);
        $result   = file_get_contents($url, false, $context);
        if ($result === FALSE) {
            dd('error request');
         }
         $tokenArray=explode('"', $result);
          return $tokenArray[3];
    }
    public function actualizarAdjuntosPatrimonio($idAdjunto,$xdoc_datos,$xdoc_titulo,$xdoc_url,$usuarioId,$xdoc_tipo_documento,$xdoc_tamanio_documento, $nombre, $xdoc_tps_doc_id, $prs_imgInsert, $xdoc_cuerpo, $xdoc_tipo_documentacion, $xdoc_correlativo)
    {   
        //dd($xdoc_tipo_documentacion);
        $token = $this->getToken();
        $url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';

     

        $parametros = '{"xdoc_idadjunto":'.$idAdjunto.',"xdoc_datos":"'.$xdoc_datos .'","xdoc_titulo":"'.$xdoc_titulo.'","xdoc_url":"'.$xdoc_url.'","xdoc_usuario":"'.$usuarioId.'","xdoc_tipo_documento":"'.$xdoc_tipo_documento.'","xdoc_tamanio_documento":"'.$xdoc_tamanio_documento.'", "xdoc_nombre":"'.$nombre.'", "xdoc_tps_doc_id":'.$xdoc_tps_doc_id.', "xprs_imgInsert":"'.$prs_imgInsert.'", "xdoc_cuerpo":"'.$xdoc_cuerpo.'", "xdoc_tipo_documentacion":"'.$xdoc_tipo_documentacion.'", "xdoc_correlativo":"'.$xdoc_correlativo.'"}';
        //dd($parametros);
        $parametros = json_encode($parametros);
        $data = '{"identificador":"SERVICIO_PATRIMONIO-1164","parametros":'.$parametros.'}';
        $options = array(
            'http' => array(
                'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
                'method'  => 'POST',
                'content' => $data
            )
        );
        $context  = stream_context_create($options);
        $result   = file_get_contents($url, false, $context);
        if ($result === FALSE) {
            dd('error request');
        }
     return $result;
    }


}
