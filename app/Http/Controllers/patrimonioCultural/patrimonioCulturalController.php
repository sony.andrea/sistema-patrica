<?php

namespace gamlp\Http\Controllers\patrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\Auth\AuthController;	
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

class patrimonioCulturalController extends Controller
{
    public function buscarPatrimonioCultural() {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.buscarPatrimonioCultural',compact('usuarioId'));
	}
	public function documentosAdjuntos($casosId,$numeroficha) {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.adjuntosPatrimonio.documentosAdjuntos',compact('usuarioId','casosId','numeroficha'));
	}
	public function indexEsculturaMonumento() {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.esculturasMonumentos.indexEsculturasMonumentos',compact('usuarioId'));
	}
	public function indexEspacioAbierto() {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.espaciosAbiertos.indexEspaciosAbiertos',compact('usuarioId'));
	}
	public function indexConjunto() {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.conjuntosPatrimoniales.indexConjuntosPatrimoniales',compact('usuarioId'));
	}
	public function indexInmuebles() {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.bienesInmuebles.index',compact('usuarioId'));
	}
	 public function indexArqueologicos() {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.bienesArqueologicos.index',compact('usuarioId'));
	}
	 public function indexInmaterial() {
    	$usuarioId =  Auth::user()->usr_id;
		return view('backend.patrimonioCultural.patrimonioInmaterial.index',compact('usuarioId'));
	}
}
