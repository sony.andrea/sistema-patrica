<?php

namespace gamlp\Http\Controllers\patrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use Carbon\Carbon;
use gamlp\Http\Controllers\Auth\AuthController; 
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

class UploadController extends Controller
{
    public function UpdatePersonaImg(Request $request)
    {
        //dd($request);
        $usuarioId =  Auth::user()->usr_id;
        $prs_imagen = '';
        if($request->hasFile('imagen'))
        {   
            /*$this->validate($request, [
                'imagen' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            ]);*/
            $imagen = $request->file('imagen');
        }
        
        $xdoc_cuerpo = $request['ctipificacion'];        
        $xcg_tipo = $request['cg_tipo'];
        $xdoc_correlativo = 0;
        $xdoc_tipo_documentacion = 0; 
            

         if ($xcg_tipo == 'PTR_CON') {

            $parteimage = 'con';
            $xdoc_proceso = '26';
            $doc_ci_nodo = 'Conjuntos';
            $prs_ci_carpeta_pri='CONJUNTOS';                    
            $parteimage = 'con';
            if ( $xdoc_cuerpo == 'Fotografias') {              

                $xdoc_tipo_documentacion = $request['cposicionconjunto'];
                if ($xdoc_tipo_documentacion == 'Imagen 1 perfil urbano') {
                    $xdoc_correlativo = 1;                        
                }
                if ($xdoc_tipo_documentacion == 'Imagen 2 perfil urbano') {
                    $xdoc_correlativo = 2;
                }
                if ($xdoc_tipo_documentacion =='Perfil corte esquematico') {
                    $xdoc_correlativo = 3;                        
                }                       
                if ($xdoc_tipo_documentacion =='Fotografia 1') {
                    $xdoc_correlativo = 4;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 2') {
                    $xdoc_correlativo = 5;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 3') {
                    $xdoc_correlativo = 6;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 4') {
                    $xdoc_correlativo = 7;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 5') {
                    $xdoc_correlativo = 8;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 6') {
                    $xdoc_correlativo = 9;
                }
                if ($xdoc_tipo_documentacion =='Otro') {
                    $xdoc_correlativo = 'Otro';

                }
            }
        }
        if ($xcg_tipo == 'PTR_IME') {
            $parteimage = 'ime';                
            $xdoc_proceso = '28';
            $doc_ci_nodo = 'Monumentos y Esculturas';
            $xdoc_tipo_documentacion = $request['cposicionescultura'];
            $prs_ci_carpeta_pri='ESCULTURAS';

            if ( $xdoc_cuerpo == 'Fotografias') {   
                if ($xdoc_tipo_documentacion == 'Fotografia Frente') {
                    $xdoc_correlativo = 1;
                }
                if ($xdoc_tipo_documentacion == 'Fotografia Perfil') {
                    $xdoc_correlativo = 2;
                }
                if ($xdoc_tipo_documentacion =='Fotografia incluyendo pedestal y placas') {
                    $xdoc_correlativo = 3;
                }
                if ($xdoc_tipo_documentacion =='Plano ubicacion') {
                    $xdoc_correlativo = 4;                }

                if ($xdoc_tipo_documentacion =='Fotografia 1') {
                    $xdoc_correlativo = 5;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 2') {
                    $xdoc_correlativo = 6;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 3') {
                    $xdoc_correlativo = 7;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 4') {
                    $xdoc_correlativo = 8;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 5') {
                    $xdoc_correlativo = 9;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 6') {
                    $xdoc_correlativo = 10;
                }
                if ($xdoc_tipo_documentacion =='Otro') {
                    $xdoc_correlativo = 'Otro';
                }
            }
        }
        if ($xcg_tipo == 'PTR-ESP') {

            $parteimage = 'esp';
            $xdoc_proceso = '27';
            $doc_ci_nodo = 'Espacio Abierto';
            $xdoc_tipo_documentacion = $request['cposicionespacioabierto'];
            $prs_ci_carpeta_pri='ESPACIOABIERTO';

            if ( $xdoc_cuerpo == 'Fotografias') {   

                if ($xdoc_tipo_documentacion =='Plano de ubicacion') {
                    $xdoc_correlativo = 1;
                }
                if ($xdoc_tipo_documentacion =='Esquema arquitectonico') {
                    $xdoc_correlativo = 2;
                }
                if ($xdoc_tipo_documentacion =='Angulo Visual Horizontal') {
                    $xdoc_correlativo = 3;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 1') {
                    $xdoc_correlativo = 4;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 2') {
                    $xdoc_correlativo = 5;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 3') {
                    $xdoc_correlativo = 6;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 4') {
                    $xdoc_correlativo = 7;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 5') {
                    $xdoc_correlativo = 8;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 6') {
                    $xdoc_correlativo = 9;
                }
                if ($xdoc_tipo_documentacion =='Otro') {
                    $xdoc_correlativo = 'Otro';

                }
            }

        }
        if ($xcg_tipo == 'PTR_BMA') {
            $parteimage = 'bma';
            $xdoc_tipo_documentacion = $request['cposicionesBienArq'];
            $xdoc_proceso = '29';
            $doc_ci_nodo = 'Bienes Muebles Arqueologicos';
            $prs_ci_carpeta_pri='BIENESARQUEOLOGICOS';

        //dd($xdoc_tipo_documentacion);
            if ( $xdoc_cuerpo == 'Fotografias') {  

                if ($xdoc_tipo_documentacion =='Fotografia del Plano de Ubicacion') {
                    $xdoc_correlativo = 1;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 1') {
                    $xdoc_correlativo = 2;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 2') {
                    $xdoc_correlativo = 3;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 3') {
                    $xdoc_correlativo = 4;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 4') {
                    $xdoc_correlativo = 5;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 5') {
                    $xdoc_correlativo = 6;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 6') {
                    $xdoc_correlativo = 7;
                }
                if ($xdoc_tipo_documentacion =='Otro') {
                    $xdoc_correlativo = 'Otro';            
                }
             }
        }
        if ($xcg_tipo == 'PTR-PCI') {
            $parteimage = 'pci';
            $doc_ci_nodo = 'Patrimonio Inmaterial';
            $xdoc_tipo_documentacion = $request['cposicionesPatInmaterial'];
            $xdoc_proceso = '30';
            $prs_ci_carpeta_pri='PATRIMONIOINMATERIAL';

            if ( $xdoc_cuerpo == 'Fotografias') {  

                if ($xdoc_tipo_documentacion =='Fotografia Principal 1') {
                    $xdoc_correlativo = 1;
                }
                if ($xdoc_tipo_documentacion =='Fotografia Principal 2') {
                    $xdoc_correlativo = 2;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 1') {
                    $xdoc_correlativo = 3;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 2') {
                    $xdoc_correlativo = 4;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 3') {
                    $xdoc_correlativo = 5;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 4') {
                    $xdoc_correlativo = 6;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 5') {
                    $xdoc_correlativo = 7;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 6') {
                    $xdoc_correlativo = 8;
                }
                if ($xdoc_tipo_documentacion =='Otro') {
                    $xdoc_correlativo = 'Otro';

                }
            }
        }
        if ($xcg_tipo == 'PTR_CBI') {
            $parteimage = 'cbi';
            $xdoc_proceso = '31';
            $doc_ci_nodo = 'Bienes Inmuebles';
            $prs_ci_carpeta_pri = 'BIENESINMUEBLES';                
            $xdoc_tipo_documentacion = $request['cposicionesBienInmueble'];

            if ( $xdoc_cuerpo == 'Fotografias') {  

                if ($xdoc_tipo_documentacion =='Foto Principal') {
                    $xdoc_correlativo = 1;
                }
                if ($xdoc_tipo_documentacion =='Plano de Ubicacion') {
                    $xdoc_correlativo = 2;
                }
                if ($xdoc_tipo_documentacion =='Tipologia Arquitectonica') {
                    $xdoc_correlativo = 3;
                }
                if ($xdoc_tipo_documentacion =='Perfil de Acera') {
                    $xdoc_correlativo = 4;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 1') {
                    $xdoc_correlativo = 5;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 2') {
                    $xdoc_correlativo = 6;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 3') {
                    $xdoc_correlativo = 7;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 4') {
                    $xdoc_correlativo = 8;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 5') {
                    $xdoc_correlativo = 9;
                }
                if ($xdoc_tipo_documentacion =='Fotografia 6') {
                    $xdoc_correlativo = 10;
                }
                if ($xdoc_tipo_documentacion =='Otro') {
                    $xdoc_correlativo = 'Otro';            
                }
            }
        }
        if ($xdoc_cuerpo == 'Fotografias') {  
            $xdoc_tps_doc_id = 1;
            $carpeta_tipo = 'FOTOS';
        }
        if ( $xdoc_cuerpo == 'Autocad') {  
            $xdoc_tps_doc_id = 2;    
            $carpeta_tipo = 'AUTOCAD';
            $xdoc_correlativo =' ';       
        }
        if ( $xdoc_cuerpo == 'Texto') {  
            $xdoc_tps_doc_id = 4;
            $carpeta_tipo = 'TEXTO';    
            $xdoc_correlativo =' ';       
        }
        
              
        $numeroficha = $request['cnumeroficha'];
        $casosId = $request['ccasosId'];                 
        $xdoc_titulo  = $request['ctitulo'];
        $xdoc_datos  = $request['cdescripcion'];
        $xdoc_usuario = $request['cusuario_avatarad'];

        $parteimage = "";
        //$ruta = '/adjuntos/'.$prs_ci;
        $xdoc_tamanio_documento = filesize($imagen);
        //$prs_ci='patrimonio';
        $ruta = '/PATRIMONIO/'.$prs_ci_carpeta_pri.'/'.$numeroficha.'/'.$carpeta_tipo;
        //$rutaInsert = 'adjuntos/'.$prs_ci;
        $rutaInsert = 'PATRIMONIO/'.$prs_ci_carpeta_pri.'/'.$numeroficha.'/'.$carpeta_tipo;      
        //$nombre = $prs_ci.sha1(Carbon::now()).'.'.$imagen->guessExtension()2048;
        //$nombre = $prs_ci.'vero'.'.'.$imagen->guessExtension();
        $xdoc_tipo_documento = $imagen->guessExtension();
        $hoy = getdate();
        //print_r($hoy);
        $year = $hoy['year'];
        $month = $hoy['month'];
        $mday = $hoy['mday'];
        $hours = $hoy['hours'];
        $minutes = $hoy['minutes'];
        $seconds = $hoy['seconds'];
        $nombre = $imagen->getClientOriginalName();
        //$nombre = $nombre.$year.$month.$mday.$hours.$minutes.$seconds.'.'.$imagen->guessExtension();
        $nombre = $parteimage.$year.$hours.$minutes.$seconds.$nombre;        
        // dd($nombre);
        $imagen->move(getcwd().$ruta, $nombre);
        $prs_img =$ruta.'/'.$nombre; 
        $prs_imgInsert =$rutaInsert.'/'.$nombre;
        $xdoc_url = $prs_imgInsert;
        
        $datos = $this->insertarAdjuntosPatrimonio($nombre,$prs_imgInsert,$numeroficha,$xdoc_datos,$xdoc_titulo,$xdoc_cuerpo, $xdoc_tipo_documentacion, $xdoc_correlativo, $xdoc_proceso, $doc_ci_nodo, $xdoc_url, $xdoc_usuario, $xdoc_tipo_documento, $xdoc_tamanio_documento, $xdoc_tps_doc_id);      
        return view('backend.patrimonioCultural.adjuntosPatrimonio.documentosAdjuntos', compact('casosId','usuarioId','numeroficha'));
    }
    public static function getToken()
    {
        $user = 'administrador';
        $pass = '123456';
        $url  = 'http://172.19.161.3/api/apiLogin';
        $data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/json",
                'method'  => 'POST',
                'content' => $data
            )
        );
        $context  = stream_context_create($options);
        $result   = file_get_contents($url, false, $context);
        if ($result === FALSE) {
            dd('error request');
         }
         $tokenArray=explode('"', $result);
          return $tokenArray[3];
    }
       public function insertarAdjuntosPatrimonio($xnombre,$xprs_img, $id, $xdoc_datos,$xdoc_titulo,$xdoc_cuerpo, $xdoc_tipo_documentacion, $xdoc_correlativo, $xdoc_proceso ,$doc_ci_nodo, $xdoc_url, $xdoc_usuario, $xdoc_tipo_documento, $xdoc_tamanio_documento, $xdoc_tps_doc_id)
    {   
        $token = $this->getToken();
        $idf_casos=$id;
        $url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
        $parametros = '{"xdoc_idCasos":'.$idf_casos.',"xdoc_nombre":"'.$xnombre.'","xdoc_url_logica":"'.$xprs_img.'","xdoc_datos":"'.$xdoc_datos.'","xdoc_titulo":"'.$xdoc_titulo.'","xdoc_cuerpo":"'.$xdoc_cuerpo.'","xdoc_tipo_documentacion":"'.$xdoc_tipo_documentacion.'","xdoc_correlativo":"'.$xdoc_correlativo.'","xdoc_proceso":"'.$xdoc_proceso.'","xdoc_ci_nodo":"'.$doc_ci_nodo.'","xdoc_url":"'.$xdoc_url.'","xdoc_usuario":"'.$xdoc_usuario.'","xdoc_tipo_documento":"'.$xdoc_tipo_documento.'","xdoc_tamanio_documento":"'.$xdoc_tamanio_documento.'","xdoc_tps_doc_id":'.$xdoc_tps_doc_id.'}';
        $parametros = json_encode($parametros);
        $data = '{"identificador":"SERVICIO_PATRIMONIO-937","parametros":'.$parametros.'}';
        $options = array(
            'http' => array(
                'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
                'method'  => 'POST',
                'content' => $data
            )
        );
        $context  = stream_context_create($options);
        $result   = file_get_contents($url, false, $context);
        if ($result === FALSE) {
            dd('error request');
        }
     return $result;
    }
}


