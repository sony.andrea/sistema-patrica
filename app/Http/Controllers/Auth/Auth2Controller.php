<?php

namespace gamlp\Http\Controllers\Auth;

use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\admin\RolUsuario;
use gamlp\Modelo\admin\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use gamlp\Modelo\admin\RolAreaTrabajo;
use Session;

class Auth2Controller extends Controller 
{

	public function postLogin2() 
	{
		$conect = input::only('rrol');
		foreach ($conect as $conect1) {
		   $roll = intval($conect1);
		   Session::put('ID_ROL', $roll);
	}
	$ids = Auth::user()->usr_id;
	$usuario = RolUsuario::getusuarios($ids);
	$rol = RolUsuario::getListarRol($ids);
	foreach ($usuario as $row) 
	{
		Session::put('USUARIO', $row->vusr_usuario);
		Session::put('AUTENTICADO', true);
		Session::put('NOMBRES', $row->vprs_nombres);
		Session::put('PATERNO', $row->vprs_paterno);
		Session::put('MATERNO', $row->vprs_materno);
		Session::put('ID_USUARIO', $row->vprs_id);
	}
	return Redirect::to('/home');
	}
	
}
