<?php
namespace gamlp\Http\Controllers\Auth;
use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\admin\RolUsuario;
use gamlp\Modelo\admin\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller {
	
	public function login()
	{
		return view('auth.login');
	}

	public function authenticate()
	{
		$data = Input::all();
		config()->set('constants.USER', $data['usr_usuario']);
		config()->set('constants.PASS', $data['usr_clave']);
		$token=$this->postLogin($data);
	}

	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';

		$url  = 'http://172.19.161.3/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/json",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		 }
		 $tokenArray=explode('"', $result);
		  return $tokenArray[3];
	}
	public static function getTokenFac()
	{
		$user = 'carlos.otondo';
		$pass = '6543210';

		$url  = 'http://192.168.5.69/ifacturacion247_pruebas/public/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/json",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		 }
		 $tokenArray=explode('"', $result);
		  return $tokenArray[3];
	}

	public static function postLogin(array $data) 
	{
		try {
		} catch (HttpResponseException $e) {
			return response()->json(['error' =>
					['message'               => 'auth_invalida',
						'codigo_de_estado'   => IlluminateResponse::HTTP_BAD_REQUEST
					]],
				IlluminateResponse::HTTP_BAD_REQUEST,
				$headers = []
			);
		}
		try {
			if (!$token = JWTAuth::attempt(array('usr_usuario' => $data['usr_usuario'], 'usr_clave' => $data['usr_clave']))) {
				return response()->json(['error'=> 'Las credenciales son invalidas!'], 400);
			}
		} catch (JWTException $e) {
			return response()->json(['error' => 'ups hubo un problema!'], 500);
		}
		return $token;
	}

	public function idr(Request $request)
    {
    	dd($request);
    	$idr = $request;
    	return Response::json($idr);
    	//echo $idr;

	}
}