<?php

namespace gamlp\Http\Controllers\atm;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\Auth\AuthController;	
use gamlp\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

use gamlp\Modelo\admin\Usuario;
use gamlp\Modelo\admin\RolUsuario;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use gamlp\Modelo\admin\Rol;

class atmController extends Controller
{
	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';

		$urlRegla  = 'http://192.168.6.113/MotorServicio/public/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/json",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		 }
		 $tokenArray=explode('"', $result);
		  return $tokenArray[3];
	}
        public function sacarFicha() {
    	$usuarioid =  Auth::user()->usr_id;
 		$tk=LoginController::getToken();
		return view('backend.atm.index',compact('tk', 'usuarioid'));
	}
        public function tipoTramite($ciContri, $nitContri) {
    	$usuarioid =  Auth::user()->usr_id;
    	$tk=LoginController::getToken();
		return view('backend.atm.tipoTramite',compact('tk','ciContri','nitContri','usuarioid'));
	}

	        public function listarFicha() {
    	$usuarioid =  Auth::user()->usr_id;
 		$tk=LoginController::getToken();
		return view('backend.atm.listarFicha',compact('tk', 'usuarioid'));
	}

		    public function llamarFicha() {
    	$usuarioid =  Auth::user()->usr_id;
 		$tk=LoginController::getToken();
		return view('backend.atm.llamarFicha',compact('tk', 'usuarioid'));
	}
}