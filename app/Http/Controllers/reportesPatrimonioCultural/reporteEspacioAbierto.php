<?php

namespace gamlp\Http\Controllers\reportesPatrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\admin\accionesController;
use gamlp\Http\Controllers\validacionController;
use PDF;

class reporteEspacioAbierto extends Controller
{
    public function getGenerar(Request $request)
    {  
  	   $id_casos = $request->get('datos');

  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
  	   
       $datos = json_decode($datos);
       $cas_id = $datos[0]->{'xcas_id'};
       	$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'};
	   //dd($g_tipo);
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);
       return $this->pdf($datos, $datosDocumentos);
    }
    public function getGenerarArchFoto(Request $request)
    {  
  	   $id_casos = $request->get('datos');
  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
       $datos = json_decode($datos);
       $cas_id = $datos[0]->{'xcas_id'};
       $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'}; 
	    // dd($g_tipo);      
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);
       return $this->pdfArchFoto($datos, $datosDocumentos);

  	   
    }

    //---------------------------------------------------------------
    //----------------------------------------------------------------
     public function getGenerarFichaTodo(Request $request)
	{ 	$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
		////obtiene imagenes fichas de CATALOGACIÓN
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		////obtiene imagenes ficha fotografica
		$datosDocumentosF = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentosF = json_decode($datosDocumentosF);
		
		return $this->pdfImprimirTodo($datos, $datosDocumentos,$datosDocumentosF);
		
			
	}
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------

    public function obtieneDocumentosPatrimonioCulturalCasos($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.' ,"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-939","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public function obtieneDocumentosPatrimonioCulturalCasosArchFoto($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.',"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-944","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public function caracteresEspecialesActualizar($data) {

		        $data = str_replace("&039;", "'", $data);

				$data = str_replace("&040;", "\"",$data);
				 //dd($data);
				$data = str_replace("&041;", "<", $data);
				$data = str_replace("&042;", ">", $data);
				$data = str_replace("&043;", "\\",$data);
				$data = str_replace("&044;", "{", $data);
				$data = str_replace("&045;", "}", $data);
				$data = str_replace("&046;", "[", $data);
				$data = str_replace("&047;", "]", $data);
				$data = str_replace("&048;", "(", $data);
				$data = str_replace("&049;", ")", $data);
				$data = str_replace("&050;", "/", $data);
				$data = str_replace("&051;", ":", $data);
				$data = str_replace("&052", " ", $data);
				 //dd($data);
		        return $data;
		}
	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';
		$url  = 'http://172.19.161.3/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/json",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		 }
		 $tokenArray=explode('"', $result);
		  return $tokenArray[3];
	}

	public function obtieneDatosPatrimonioCulturalCasos($id_casos)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_casos":'.$id_casos.'}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-935","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
public function pdf($datos, $datosDocumentos)
  	{
	  	 //dd($datos);
	  	  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $cas_id = $datos[0]->{'xcas_id'};
		  $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		  $cas_act_id = $datos[0]->{'xcas_act_id'};
		  $cas_usr_actual_id = $datos[0]->{'xcas_usr_actual_id'};
		  
		  $cas_datos = $datos[0]->{'xcas_datos'};
		  $cas_datos = json_decode($cas_datos);
		  $g_tipo = $cas_datos->{'g_tipo'};
		  $PTR_CREA = $cas_datos->{'PTR_CREA'};
		  $PTR_ACTUALIZA = $cas_datos->{'PTR_ACTUALIZA'};
		  $PTR_COD_CAT = $cas_datos->{'PTR_COD_CAT'};
		  $PTR_GEOREF = $cas_datos->{'PTR_GEOREF'};
		  $PTR_TRAD = $cas_datos->{'PTR_TRAD'};
		  $PTR_ACT = $cas_datos->{'PTR_ACT'};
		  $PTR_DEP = $cas_datos->{'PTR_DEP'};
		  $PTR_CIPO = $cas_datos->{'PTR_CIPO'};
		  $PTR_MUNI = $cas_datos->{'PTR_MUNI'};
		  $PTR_MACRO = $cas_datos->{'PTR_MACRO'};
		  $PTR_BARR = $cas_datos->{'PTR_BARR'};
		  $PTR_DIR = $cas_datos->{'PTR_DIR'};
		  $PTR_ESQ1 = $cas_datos->{'PTR_ESQ1'};
		  $PTR_ESQ2 = $cas_datos->{'PTR_ESQ2'};
		  $PTR_ALT = $cas_datos->{'PTR_ALT'};
		  $PTR_NOMPRO = $cas_datos->{'PTR_NOMPRO'};
		  $PTR_REPRO = $cas_datos->{'PTR_REPRO'};

			$PTR_USO = $cas_datos->{'PTR_USO'};
			$PTR_SERV = $cas_datos->{'PTR_SERV'};
			$PTR_FIGPRO = $cas_datos->{'PTR_FIGPRO'};
			$PTR_OTHER = $cas_datos->{'PTR_OTHER'};
			$PTR_REFHIST = $cas_datos->{'PTR_REFHIST'};
			$PTR_FECCONS = $cas_datos->{'PTR_FECCONS'};
			$PTR_AUTOR = $cas_datos->{'PTR_AUTOR'};
			$PTR_PREEX = $cas_datos->{'PTR_PREEX'};
			$PTR_PREEX = $this->caracteresEspecialesActualizar($PTR_PREEX);
			$PTR_PROP = $cas_datos->{'PTR_PROP'};
			$PTR_USOORG = $cas_datos->{'PTR_USOORG'};
			$PTR_USOORG = $this->caracteresEspecialesActualizar($PTR_USOORG);
			$PTR_HECHIS = $cas_datos->{'PTR_HECHIS'};
			$PTR_HECHIS = $this->caracteresEspecialesActualizar($PTR_HECHIS);
			$PTR_FIEREL = $cas_datos->{'PTR_FIEREL'};
			$PTR_FIEREL = $this->caracteresEspecialesActualizar($PTR_FIEREL);
			$PTR_DATHISC = $cas_datos->{'PTR_DATHISC'};
			$PTR_DATHISC = $this->caracteresEspecialesActualizar($PTR_DATHISC);
			$PTR_FUENTE = $cas_datos->{'PTR_FUENTE'};
			$PTR_FUENTE = $this->caracteresEspecialesActualizar($PTR_FUENTE);
			$PTR_FECCONST = $cas_datos->{'PTR_FECCONST'};
			$PTR_AUTCONST = $cas_datos->{'PTR_AUTCONST'};
			$PTR_PREDIN = $cas_datos->{'PTR_PREDIN'};
			$PTR_PREDIN = $this->caracteresEspecialesActualizar($PTR_PREDIN);
			$PTR_PROPIE = $cas_datos->{'PTR_PROPIE'};
			$PTR_USOPREEX = $cas_datos->{'PTR_USOPREEX'};
			$PTR_USOPREEX = $this->caracteresEspecialesActualizar($PTR_USOPREEX);
			$PTR_HECHISD = $cas_datos->{'PTR_HECHISD'};
			$PTR_HECHISD = $this->caracteresEspecialesActualizar($PTR_HECHISD);
			$PTR_FIERELD = $cas_datos->{'PTR_FIERELD'};
			$PTR_FIERELD = $this->caracteresEspecialesActualizar($PTR_FIERELD);
			$PTR_DATHISTC = $cas_datos->{'PTR_DATHISTC'};
			$PTR_DATHISTC = $this->caracteresEspecialesActualizar($PTR_DATHISTC);
			$PTR_FUENTBIO = $cas_datos->{'PTR_FUENTBIO'};
			$PTR_FUENTBIO = $this->caracteresEspecialesActualizar($PTR_FUENTBIO);
			$PTR_INSCR = $cas_datos->{'PTR_INSCR'};
			$PTR_INSCR = $this->caracteresEspecialesActualizar($PTR_INSCR);
			$PTR_EPOCA = $cas_datos->{'PTR_EPOCA'}; 
			$PTR_ACONS = $cas_datos->{'PTR_ACONS'};
			$PTR_AULTR = $cas_datos->{'PTR_AULTR'};
			$PTR_ESTILO = $cas_datos->{'PTR_ESTILO'};
			$PTR_UBICESP = $cas_datos->{'PTR_UBICESP'};
			$PTR_LINCONS = $cas_datos->{'PTR_LINCONS'};
			$PTR_TIPOLO = $cas_datos->{'PTR_TIPOLO'};
			//--
			$PTR_MURMAT = $cas_datos->{'PTR_MURMAT'};
			
			$PTR_MURMAT_OTROS = $cas_datos->{'PTR_MURMAT_OTROS'};
			$PTR_MUREST = $cas_datos->{'PTR_MUREST'};
			$PTR_MURINTE = $cas_datos->{'PTR_MURINTE'};
			$PTR_REJAS = $cas_datos->{'PTR_REJAS'};
			$PTR_REJAEST = $cas_datos->{'PTR_REJAEST'};
			$PTR_ACAMUR = $cas_datos->{'PTR_ACAMUR'};
			$PTR_ACAEST = $cas_datos->{'PTR_ACAEST'};
			$PTR_PINTURA = $cas_datos->{'PTR_PINTURA'};
			$PTR_PINTEST = $cas_datos->{'PTR_PINTEST'};
			$PTR_PISOS = $cas_datos->{'PTR_PISOS'};
			$PTR_PISOEST = $cas_datos->{'PTR_PISOEST'};
			$PTR_PUERTA = $cas_datos->{'PTR_PUERTA'};
			$PTR_PUERTAEST = $cas_datos->{'PTR_PUERTAEST'};
			$PTR_MOBURB = $cas_datos->{'PTR_MOBURB'};
			$PTR_MOBEST = $cas_datos->{'PTR_MOBEST'};
			$PTR_ESCAL = $cas_datos->{'PTR_ESCAL'};
			$PTR_ESCEST = $cas_datos->{'PTR_ESCEST'};
			$PTR_VEGET = $cas_datos->{'PTR_VEGET'};
			$PTR_VEGEST = $cas_datos->{'PTR_VEGEST'};
			$PTR_DETALLES = $cas_datos->{'PTR_DETALLES'};
			$PTR_DETALLEST = $cas_datos->{'PTR_DETALLEST'};
			$PTR_ESQDES = $cas_datos->{'PTR_ESQDES'};
			$PTR_ESQDES = $this->caracteresEspecialesActualizar($PTR_ESQDES);
			$PTR_ANGOR = $cas_datos->{'PTR_ANGOR'};
			$PTR_ANGGRA = $cas_datos->{'PTR_ANGGRA'};
			//--
			$PTR_VISTAS = $cas_datos->{'PTR_VISTAS'};

			$PTR_ANGOTR = $cas_datos->{'PTR_ANGOTR'};
			$PTR_ELEMVISID = $cas_datos->{'PTR_ELEMVISID'};
			$PTR_ELEMVISID = $this->caracteresEspecialesActualizar($PTR_ELEMVISID);
			$PTR_ELEMVISAL = $cas_datos->{'PTR_ELEMVISAL'};
			$PTR_ELEMVISAL = $this->caracteresEspecialesActualizar($PTR_ELEMVISAL);
			$PTR_ELEMVISAB = $cas_datos->{'PTR_ELEMVISAB'};
			$PTR_ELEMVISAB = $this->caracteresEspecialesActualizar($PTR_ELEMVISAB);
			$PTR_DESENTEI = $cas_datos->{'PTR_DESENTEI'};
			$PTR_DESENTEI = $this->caracteresEspecialesActualizar($PTR_DESENTEI);
			$PTR_DESESPDES = $cas_datos->{'PTR_DESESPDES'};
			$PTR_DESESPDES = $this->caracteresEspecialesActualizar($PTR_DESESPDES);
			$PTR_MEDIDAREA = $cas_datos->{'PTR_MEDIDAREA'};
			$PTR_MEDIDANC = $cas_datos->{'PTR_MEDIDANC'};
			$PTR_MEDIDLAR = $cas_datos->{'PTR_MEDIDLAR'};
			$PTR_MEDIAMC = $cas_datos->{'PTR_MEDIAMC'};
			$PTR_INFADICIONAL = $cas_datos->{'PTR_INFADICIONAL'};
			$PTR_INFOBS = $cas_datos->{'PTR_INFOBS'};
			$PTR_INFOBS = $this->caracteresEspecialesActualizar($PTR_INFOBS);
			//--
			$PTR_PATODAN = $cas_datos->{'PTR_PATODAN'};

			$PTR_PATO_OTROS = $cas_datos->{'PTR_PATO_OTROS'};
			$PTR_PATOOBS = $cas_datos->{'PTR_PATOOBS'};
			$PTR_PATOOBS = $this->caracteresEspecialesActualizar($PTR_PATOOBS);
			//--
			$PTR_PATOCAU = $cas_datos->{'PTR_PATOCAU'};
			
			$PTR_PATOCAU_OTROS = $cas_datos->{'PTR_PATOCAU_OTROS'};
			//--
			$PTR_VALORHIS = $cas_datos->{'PTR_VALORHIS'};
			//--
			$PTR_VALORART = $cas_datos->{'PTR_VALORART'};
			//--
			$PTR_VALORARQ = $cas_datos->{'PTR_VALORARQ'};
			//--
			$PTR_VALORTEC = $cas_datos->{'PTR_VALORTEC'};
			//--
			$PTR_VALORINTG = $cas_datos->{'PTR_VALORINTG'};
			//--
			$PTR_VALORURB = $cas_datos->{'PTR_VALORURB'};
			//--
			$PTR_VALORINT = $cas_datos->{'PTR_VALORINT'};
			//--
			$PTR_VALORSIMB = $cas_datos->{'PTR_VALORSIMB'};

			$PTR_CRITERHIS = $cas_datos->{'PTR_CRITERHIS'};
			$PTR_CRITERHIS = $this->caracteresEspecialesActualizar($PTR_CRITERHIS);
			$PTR_CRITERART = $cas_datos->{'PTR_CRITERART'};
		    $PTR_CRITERART = $this->caracteresEspecialesActualizar($PTR_CRITERART);
			$PTR_CRITERARQ = $cas_datos->{'PTR_CRITERARQ'};
			$PTR_CRITERARQ = $this->caracteresEspecialesActualizar($PTR_CRITERARQ);
			$PTR_CRITERTEC = $cas_datos->{'PTR_CRITERTEC'};
			$PTR_CRITERTEC = $this->caracteresEspecialesActualizar($PTR_CRITERTEC);
			$PTR_CRITERINT = $cas_datos->{'PTR_CRITERINT'};
			$PTR_CRITERINT = $this->caracteresEspecialesActualizar($PTR_CRITERINT);
			$PTR_CRITERURB = $cas_datos->{'PTR_CRITERURB'};
			$PTR_CRITERURB = $this->caracteresEspecialesActualizar($PTR_CRITERURB);
			$PTR_CRITERINTA = $cas_datos->{'PTR_CRITERINTA'};
			$PTR_CRITERINTA = $this->caracteresEspecialesActualizar($PTR_CRITERINTA);
			$PTR_CRITERSIMB = $cas_datos->{'PTR_CRITERSIMB'};
			$PTR_CRITERSIMB = $this->caracteresEspecialesActualizar($PTR_CRITERSIMB);
			$PTR_PERINV = $cas_datos->{'PTR_PERINV'};
			$PTR_PERREV = $cas_datos->{'PTR_PERREV'};
			$PTR_PERDIG = $cas_datos->{'PTR_PERDIG'};

		  
		  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  $cas_estado_paso = $datos[0]->{'xcas_estado_paso'};
		  $cas_fecha_inicio = $datos[0]->{'xcas_fecha_inicio'};
		  $cas_fecha_limite = $datos[0]->{'xcas_fecha_limite'};
		  $cas_nodo_id = $datos[0]->{'xcas_nodo_id'};
		  $cas_registrado = $datos[0]->{'xcas_registrado'};
		  $cas_modificado = $datos[0]->{'xcas_modificado'};
		  $cas_usr_id = $datos[0]->{'xcas_usr_id'};
		  $cas_estado = $datos[0]->{'xcas_estado'};
		  $cas_ws_id = $datos[0]->{'xcas_ws_id'};
		  $cas_asunto = $datos[0]->{'xcas_asunto'};
		  $cas_tipo_hr = $datos[0]->{'xcas_tipo_hr'};
		  $cas_id_padre = $datos[0]->{'xcas_id_padre'};
		  $campo_f = $datos[0]->{'xcampo_f'};
		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter', 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);

		   	$mpdf->SetHTMLHeader('
           <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>FICHA DE CATALOGACIÓN DE ESPACIOS ABIERTOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');
			
			$mpdf->SetHTMLFooter('
				<table width="100%" style="font-size:12px;" >
					<tr>
					<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
					<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
					</tr>
				</table>');

	$html = ' 
	<style>

	#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	}
	table{
		font-size:12px;
		text-align : justify;
	}
	#tdbordeArchFot{
		   
		    	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	 
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }
	</style>
	     ';

	$html.= '
	<table style="font-size:11px;" width="100%">
		<tr>
			<td><b>I. IDENTIFICACI&Oacute;N</b></td>
		</tr>
		<tr>
		    <td><b>1.PLANO DE UBICACIÓN</b></td>
		</tr>
		
    </table>';

			 $html.= '
				
						<table style="font-size:11px;" width="100%">';
							$longitudDoc=sizeof($datosDocumentos);
							//dd($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
							
						if($longitudDoc != 0){

						
							$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};							
							$html.='
							<tr>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
													
							</tr>

							';
						
						}
							$html.='       						
						</table>		
						    ';

	$html.='<br>
	<table style="font-size:11px;" width="100%">
		<tr colspan=4>
		<td><b>2.DATOS GENERALES</b></td>
		</tr>
    </table>
	<table style="font-size:11px;" width="100%"  cellspacing="-0.5">
	<tr>
	<td width="25%" id="tdborde"><b>C&oacute;digo de Catastro:</b></td>
	<td width="25%" id="tdborde">'.$PTR_COD_CAT.'</td>
	<td width="25%" id="tdborde"><b>Georeferencia:</b></td>
	<td width="25%" id="tdborde">'.$PTR_GEOREF.'</td>
	</tr>
	</table >';
	$html.= '<br>
	<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="25%" id="tdborde"><b>Nro.:</b></td>
		<td width="25%" id="tdborde"><b>Punto Colineal</b></td>
	</tr>';
	$figuraPuntosColneales = $cas_datos->{'PTR_DOT_S'};
	for($i = 1; $i < sizeof($figuraPuntosColneales); $i++){
		$html.= '<tr><td  width="25%">'.$figuraPuntosColneales[$i]->{'f01_Nro'}.'</td>
		<td  width="25%">'.$figuraPuntosColneales[$i]->{'f01_Pto_Colineal'}.'</td>
		</tr>';
		}
	$html.= '</table>';
    //--------------------------------------------
	$html.='<br>
	<table style="font-size:11px;" width="100%">
	<tr colspan=4>
	<td ><b>3.DENOMINACIÓN DEL INMUEBLE</b></td>
	</tr>
	</table >
	
  <table style="font-size:11px;" width="100%"  cellspacing="-0.5">
    <tr>
	  <td width="25%" id="tdborde"><b>Tradicional:</b></td>
  	  <td width="25%" id="tdborde">'.$PTR_TRAD.'</td>
	  <td width="25%" id="tdborde"><b>Actual:</b></td>
	  <td width="25%" id="tdborde">'.$PTR_ACT.'</td>
	</tr>				
 </table >
 <br>
 <table style="font-size:11px;" width="100%" >
   <tr colspan=4>
	<td><b>4.LOCALIZACIÓN</b></td>
   </tr>
 </table >

 <table style="font-size:11px;" width="100%"  cellspacing="-0.5">
    <tr>
	  <td width="25%" id="tdborde"><b>Departamento:</b></td>
  	  <td width="25%" id="tdborde">'.$PTR_DEP.'</td>
	  <td width="25%" id="tdborde"><b>Dirección:</b></td>
	  <td width="25%" id="tdborde">'.$PTR_DIR.'</td>
	</tr>
	<tr>
	  <td id="tdborde"><b>Municipio:</b></td>
  	  <td id="tdborde">'.$PTR_MUNI.'</td>
	  <td id="tdborde"><b>Esq. o Entre Calles 1</b></td>
	  <td id="tdborde">'.$PTR_ESQ1.'</td>
	</tr>
	<tr>
	  <td id="tdborde"><b>Ciudad/Poblado:</b></td>
  	  <td id="tdborde">'.$PTR_CIPO.'</td>
	  <td id="tdborde"><b>Esq. o Entre Calles 2</b></td>
	  <td id="tdborde">'.$PTR_ESQ2.'</td>
	</tr>
	<tr>
	  <td id="tdborde"><b>Macro-distrito:</b></td>
  	  <td id="tdborde">'.$PTR_MACRO.'</td>
	  <td id="tdborde"><b>Altitud(m.s.n.m.)</b></td>
	  <td id="tdborde">'.$PTR_ALT.'</td>
	</tr>
	<tr>
	  <td width="20%" id="tdborde"><b>Barrio:</b></td>
  	  <td id="tdborde">'.$PTR_BARR.'</td>
	  <td id="tdborde"><b></b></td>
	  <td id="tdborde"></td>
	</tr>
 </table >
 <br>
 <table style="font-size:11px;" width="100%">
	<tr colspan=4>               
	 <td><b>5.RÉGIMEN DE PROPIEDAD</b></td>	    
	</tr>
 </table >

 <table style="font-size:11px;" width="100%"  align="center" cellspacing="-0.5">
	<tr>
	<td width="25%" id="tdborde"><b>Nombre Propietario:</b></td>
	<td width="25%" id="tdborde">'.$PTR_NOMPRO.'</td>
	<td width="25%" id="tdborde"><b>Propiedad:</b></td>
	<td width="25%" id="tdborde">'.$PTR_REPRO.'</td>
	</tr>
	<tr>
	<td width="25%" id="tdborde"><b>Otros Servicios:</b></td>
	<td width="25%" id="tdborde">';
	$longitud = count($PTR_OTHER);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_OTHER[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_OTHER[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='</td>
	<td width="25%" id="tdborde"><b>Propiedad:</b></td>
	<td width="25%" id="tdborde"></td>
	</tr>
 </table><br>';
$html.='<table style="font-size:11px;" width="100%">
	<tr colspan=4>               
	 <td><b>6.USO DE SUELO</b></td>	    
	</tr>
    </table>';
    //------------------------------------------------------------------------------------
    $html.= '<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="33%" id="tdborde"><b>Descripción</b></td>
		<td width="33%" id="tdborde"><b>Tradicional</b></td>
		<td width="33%" id="tdborde"><b>Actual</b></td>
	</tr>';
	$figuraUsoSuelo = $cas_datos->{'PTR_USO'};
	for($i = 1; $i < sizeof($figuraUsoSuelo); $i++){
		$html.= '<tr><td  width="33%" id="tdborde">'.$figuraUsoSuelo[$i]->{'f01_emision_G_SERV_DESC'}.'</td>
		<td  width="33%" id="tdborde">'.$figuraUsoSuelo[$i]->{'f01_emision_G_TRAD'}.'</td>
		<td  width="33%" id="tdborde">'.$figuraUsoSuelo[$i]->{'f01_emision_G_ACT'}.'</td>
		</tr>';
		}
	$html.= '</table><br>';
	 //----------------------------------------------------------------------------------------
    $html.='<table style="font-size:11px;" width="100%">
	<tr colspan=4>               
	 <td><b>7.SERVICIOS</b></td>	    
	</tr>
    </table>';
	//-----------------------------------------------------------------------------------------
     $html.= '<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="54%" id="tdborde"><b>Descripción</b></td>
		<td width="54%" id="tdborde"><b>Proveedor</b></td>
	</tr>';
	$figuraServicios = $cas_datos->{'PTR_SERV'};
	for($i = 1; $i < sizeof($figuraServicios); $i++){
		$html.= '<tr><td  width="54%" id="tdborde">'.$figuraServicios[$i]->{'f01_emision_G_SERV_DESC'}.'</td>
		<td  width="54%" id="tdborde">'.$figuraServicios[$i]->{'f01_emision_G_SERV_PROV'}.'</td>
		</tr>';
		}
	$html.= '</table>';
	//-----------------------------------------------------------------------------------------
 $html.= '<br>
 <table style="font-size:11px;" width="100%">
 	<tr colspan=4>
 		<td><b>II. MARCO LEGAL</b></td>
 	</tr>
 	<tr colspan=4>
 		<td><b>Figura de Proteccion Legal:</b>'.$PTR_FIGPRO.'</td>
 	</tr>
 </table>
  <br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td id="tdborde">
			<table width="100%">
				<tr>
					<td width="25%"><b>Fecha Construccion:</b></td>
					<td width="25%">'.$PTR_FECCONS.'</td>
					<td width="25%"><b>Autor/Constructor:</b></td>
					<td width="25%">'.$PTR_AUTOR.'</td>
				</tr>
				<tr>
					<td width="25%"><b>Propietarios:</b></td>
					<td width="25%">'.$PTR_PROP.'</td>
					<td width="25%"><b></b></td>
					<td width="25%"></td>
				</tr>				
			</table >
		</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Preexsistencias/Edificaciones/Intevenciones</b></td>
	</tr>
	<tr >
		<td>'.$PTR_PREEX.'</td>
	</tr>

</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Usos Originales</b></td>
	</tr>
	<tr>
		<td>'.$PTR_USOORG.'</td>
	</tr>

</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Hechos Históricos/Personajes Distinguidos</b></td>
	</tr>
	<tr >
		<td align="justify">'.$PTR_HECHIS.'</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Fiestas Religiosas</b></td>
	</tr>
	<tr >
		<td>'.$PTR_FIEREL.'</td>
	</tr>

</table>
 <br>
<table width="100%">
	<tr>
		<td><b>Datos Históricos del Conjunto</b></td>
	</tr>
	<tr>
		<td align="justify">'.$PTR_DATHISC.'</td>
	</tr>
</table>
 <br>
<table width="100%">
	<tr>
		<td><b>Fuentes Bibliográficas</b></td>
	</tr>
	<tr>
		<td>'.$PTR_FUENTE.'</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>V.DATOS DOCUMENTALES</b></td>
	</tr>

</table >
 <br>
<table  width="100%" cellspacing="-0.5" align="center">
	<tr>
		<td id="tdborde">

			<table width="100%" align="center">
				<tr>
					<td width="25%"><b>Fecha Construccion:</b></td>
					<td width="25%">'.$PTR_FECCONST.'</td>
					<td width="25%"><b>Autor/Constructor:</b></td>
					<td width="25%">'.$PTR_AUTCONST.'</td>
				</tr>
				<tr>
					<td width="25%"><b>Propietarios::</b></td>
					<td width="25%">'.$PTR_PROPIE.'</td>
					<td width="25%"><b></b></td>
					<td width="25%"></td>

				</tr>				
			</table >

		</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr >
		<td><b>Preexistencia/ Edificación/ Intervenciones:</b></td>
	</tr>
	<tr>
		<td>'.$PTR_USOPREEX.'</td>
	</tr>
</table>

<table width="100%">
	<tr >
		<td><b>Usos Originales</b></td>
	</tr>
	<tr>
		<td>Espacio Abierto</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>Hechos Históricos/ Personajes Distinguidos</b></td>
	</tr>
	<tr colspan=4>
		<td>'.$PTR_HECHISD.'</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>Fiestas Religiosas</b></td>
	</tr>
	<tr colspan=4>
		<td><b>'.$PTR_FIERELD.'</b></td>
	</tr>
</table>
<br>
<table  width="100%">
	<tr colspan=4>
		<td><b>Datos Históricos del Conjunto</b></td>
	</tr>
	<tr colspan=4>
		<td align="justify">'.$PTR_DATHISTC.'</td>
	</tr>
</table>
 <br>
<table width="100%">
	<tr colspan=4>
		<td><b>Fuentes Bibliográficas</b></td>
	</tr>
	<tr colspan=4>
		<td align="justify">'.$PTR_FUENTBIO.'</td>
	</tr>
</table>
 <br>
<table width="100%">
	<tr colspan=4>
		<td><b>VI. INSCRIPCIONES</b></td>
	</tr>
	<tr colspan=4>
		<td align="justify">'.$PTR_INSCR.'</td>
	</tr>
</table>
<br>
<table  width="100%"  cellspacing="-0.5">
	<tr colspan=4>
		<td><b>VII. EPOCA </b></td>
	</tr>
	<tr>
		<td width="25%" id="tdborde"><b>Época:</b></td>
		<td width="25%" id="tdborde">'.$PTR_EPOCA.'</td>
		<td width="25%" id="tdborde"><b>Año de Construcción:</b></td>
		<td width="25%" id="tdborde">'.$PTR_ACONS.'</td>
	</tr>
	<tr>
		<td width="25%" id="tdborde"><b>Ultima Remo-delación:</b></td>
		<td width="25%" id="tdborde">'.$PTR_AULTR.'</td>
		<td width="25%" id="tdborde"><b></b></td>
		<td width="25%" id="tdborde"></td>
	</tr>


</table >
 <br>
<table width="100%">

	<tr colspan=4>
		<td><b>VIII. ESTILO</b></td>
	</tr>

</table>
 <br>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="25%" id="tdborde"><b>Estilo:</b></td>
		<td width="25%" id="tdborde">'.$PTR_ESTILO.'</td>
		<td width="25%" id="tdborde"><b></b></td>
		<td width="25%" id="tdborde"></td>
	</tr>


</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>VIII. DESCRIPCIÓN DEL ÁREA</b></td>
	</tr>

</table>
 <br>
<table width="100%"  cellspacing="-0.5">
	<tr>
		<td width="25%" id="tdborde"><b>Ubicación del Medianero Espacio en el Area:</b></td>
		<td width="25%" id="tdborde">'.$PTR_UBICESP.'</td>
		<td width="25%" id="tdborde"><b>Linea de Construcción</b></td>
		<td width="25%" id="tdborde">'.$PTR_LINCONS.'</td>
	</tr>
</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>1. TIPOLOGÍA</b></td>
	</tr>

</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="25%" id="tdborde"><b>Tipológia:</b></td>
		<td width="25%" id="tdborde">'.$PTR_TIPOLO.'</td>
		<td width="25%" id="tdborde"><b> </b></td>
		<td width="25%" id="tdborde"></td>
	</tr>


</table >	
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>2. MUROS DE DELIMITACIÓN</b></td>
	</tr>

</table>
<table width="100%"  cellspacing="-0.5">

	<tr>
		<td width="25%" id="tdborde"><b>Materiales:</b></td>
		<td width="25%" id="tdborde">';
		 	      $longitud = count($PTR_MURMAT);
				  for ($j=1; $j < $longitud; $j++) { 
				  	$estado = $PTR_MURMAT[$j]->{'estado'};
					   if ($estado) {				  	
					     $resvalor = $PTR_MURMAT[$j]->{'resvalor'};
					    
					      $html.='
					      <p>'.$resvalor.'</p>
					      ';
					  	}
				  }
		     $html.='
		</td>
		<td width="25%" id="tdborde"><b>Integridad:</b></td>
		<td width="25%" id="tdborde">'.$PTR_MURINTE.' </td>
	</tr>
	<tr>
		<td width="25%" id="tdborde"><b>Estado de Conservación:</b></td>
		<td width="25%" id="tdborde">'.$PTR_MUREST.'</td>
		<td width="25%" id="tdborde"><b> </b></td>
		<td width="25%" id="tdborde"> </td>
	</tr>
</table >
 <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>3. REJAS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_REJAEST.'</td>
	</tr>
</table><br>';

$html.='<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>';
	$figuraRejas = $cas_datos->{'PTR_REJAS'};  
	for($i = 1; $i < sizeof($figuraRejas); $i++){
	 	$tit = $figuraRejas[0]->{'titulos'};
        $titulos = explode ( '|' , $tit , - 1 ); 
    $html.= '<tr><td width="33%" id="tdborde">'.$figuraRejas[$i]->{'f01_reja_DES_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulos); $j++){
	    	$hierrof = $titulos[1];
	    	$tuboi = $titulos[2];
	    	$madera2 = $titulos[3];
	    	$otros = $titulos[4];
	    }
        $hierroH = $figuraRejas[$i]->{'f01_reja_HIERRO_'};
	    $tubo = $figuraRejas[$i]->{'f01_reja_TUBO_'};
	    $madera = $figuraRejas[$i]->{'f01_reja_MAD_'};
	    $otros = $figuraRejas[$i]->{'f01_reja_OTRO_'};

	$html.= '<td width="33%" id="tdborde" VALIGN="TOP">';
		if($hierroH  != "NO" ){
		$html.= '<p>'.$hierrof.'</p>';
		}
		if ($tubo != "NO") 
		{
          $html.= '<p>'.$tuboi.'</p>';
		}
		if ($madera != "NO") 
		{
          $html.= '<p>'.$madera2.'</p>';
		}
        if ($otros != "NO") 
		{
          $html.= '<p>'.$otros.':  '.$figuraRejas[$i]->{'f01_reja_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="33%" id="tdborde">'.$figuraRejas[$i]->{'f01_reja_INT_'}.'</td></tr>';
	}	
$html.= '</table>';

$html.=' <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>4.ACABADOS DE MUROS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_ACAEST.'</td>
	</tr>
</table>';

$html.='<table width="100%"  cellspacing="-0.5">
	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>';
	$figuraAcabadosM = $cas_datos->{'PTR_ACAMUR'};  
	for($i = 1; $i < sizeof($figuraAcabadosM); $i++){
	 	$titAM = $figuraAcabadosM[0]->{'titulos'};
        $titulosAM = explode ( '|' , $titAM , - 1 ); 
      
    $html.= '<tr><td width="33%" id="tdborde">'.$figuraAcabadosM[$i]->{'f01_muro_DES_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulosAM); $j++){
	    	$calC = $titulosAM[1];
	    	$cementoA = $titulosAM[2];
	    	$piedra = $titulosAM[3];
	    	$marmol = $titulosAM[4];
	    	$madera = $titulosAM[5];
	    	$azulejo = $titulosAM[6];
	    	$ladrillo = $titulosAM[7];
	    	$otros = $titulosAM[8];
	    }
        $calCE = $figuraAcabadosM[$i]->{'f01_muro_CAL_'};
	    $cementoAR = $figuraAcabadosM[$i]->{'f01_muro_CEM_'};
	    $piedra1 = $figuraAcabadosM[$i]->{'f01_muro_PIE_'};
	    $marmol1 = $figuraAcabadosM[$i]->{'f01_muro_MAR_'};
        $madera1 = $figuraAcabadosM[$i]->{'f01_muro_MAD_'};
	    $azulejo1 = $figuraAcabadosM[$i]->{'f01_muro_AZU_'};
	    $ladrillo1 = $figuraAcabadosM[$i]->{'f01_muro_LAD_'};
	    $otros1 = $figuraAcabadosM[$i]->{'f01_muro_OTRO_'};
	$html.= '<td width="33%" id="tdborde" VALIGN="TOP">';
		if($calCE  != "NO" ){
		$html.= '<p>'.$calC.'</p>';
		}
		if ($cementoAR != "NO") 
		{
          $html.= '<p>'.$cementoA.'</p>';
		}
		if ($piedra1 != "NO") 
		{
          $html.= '<p>'.$piedra.'</p>';
		}
		if ($marmol1 != "NO") 
		{
          $html.= '<p>'.$marmol.'</p>';
		}
		if ($madera1 != "NO") 
		{
          $html.= '<p>'.$madera.'</p>';
		}
		if ($azulejo1 != "NO") 
		{
          $html.= '<p>'.$azulejo.'</p>';
		}
		if ($ladrillo1 != "NO") 
		{
          $html.= '<p>'.$ladrillo.'</p>';
		}
        if ($otros1 != "NO") 
		{
          $html.= '<p>'.$otros.':  '.$figuraAcabadosM[$i]->{'f01_muro_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="33%" id="tdborde">'.$figuraAcabadosM[$i]->{'f01_muro_INT_'}.'</td></tr>';
	}
$html.= '</table>';

$html.=' <br>		
<table width="100%">
	<tr colspan=4>
		<td><b>5. PINTURAS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_PINTEST.'</td>
	</tr>
</table>';

$html.='<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>';
	$figuraPinturas = $cas_datos->{'PTR_PINTURA'};  
	for($i = 1; $i < sizeof($figuraPinturas); $i++){
	 	$titP = $figuraPinturas[0]->{'titulos'};
        $titulosP = explode ( '|' , $titP , - 1 ); 
    $html.= '<tr><td width="33%" id="tdborde">'.$figuraPinturas[$i]->{'f01_pint_COD_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulosP); $j++){
	    	$cal1 = $titulosP[1];
	    	$acrilica1 = $titulosP[2];
	    	$aceite1 = $titulosP[3];
	    	$otros1 = $titulosP[4];
	    }
        $cal = $figuraPinturas[$i]->{'f01_pint_CAL_'};
	    $acrilica = $figuraPinturas[$i]->{'f01_pint_ACR_'};
	    $aceite = $figuraPinturas[$i]->{'f01_pint_ACE_'};
	    $otros = $figuraPinturas[$i]->{'f01_pint_OTRO_'};

	$html.= '<td width="33%" id="tdborde" VALIGN="TOP">';
		if($cal  != "NO" ){
		 $html.= '<p>'.$cal1.'</p>';
		}
		if ($acrilica != "NO") 
		{
          $html.= '<p>'.$acrilica1.'</p>';
		}
		if ($aceite != "NO") 
		{
          $html.= '<p>'.$aceite1.'</p>';
		}
        if ($otros != "NO") 
		{
          $html.= '<p>'.$otros1.':  '.$figuraPinturas[$i]->{'f01_pint_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="33%" id="tdborde">'.$figuraPinturas[$i]->{'f01_pint_INT_'}.'</td></tr>';
	}
$html.= '</table>';

$html.=' <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>6. PISOS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_PISOEST.'</td>
	</tr>
</table>';

$html.='<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>';
    $figuraPisos = $cas_datos->{'PTR_PISOS'};  
	for($i = 1; $i < sizeof($figuraPisos); $i++){
	 	$titPI = $figuraPisos[0]->{'titulos'};
        $titulosPI = explode ( '|' , $titPI , - 1 ); 
    $html.= '<tr><td width="33%" id="tdborde">'.$figuraPisos[$i]->{'f01_pis_DES_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulosPI); $j++){
	    	$baldosaP = $titulosPI[1];
	    	$mosaicosP = $titulosPI[2];
	    	$porcelanatoP = $titulosPI[3];
	    	$PlajaP = $titulosPI[4];
	    	$PrioP = $titulosPI[5];
	    	$ladrillosP = $titulosPI[6];
	    	$gramaP = $titulosPI[7];
	    	$tierraP = $titulosPI[8];
	    	$concretoP = $titulosPI[9];
	    	$ceramicaP = $titulosPI[10];
	    	$maderaP = $titulosPI[11];
	    	$otrosP = $titulosPI[12];
	    }
        $baldosaPI = $figuraPisos[$i]->{'f01_pis_BAL_'};
	    $mosaicosPI = $figuraPisos[$i]->{'f01_pis_MOS_'};
	    $porcelanatoPI = $figuraPisos[$i]->{'f01_pis_POR_'};
	    $PlajaPI = $figuraPisos[$i]->{'f01_pis_LAJ_'};
	    $PrioPI = $figuraPisos[$i]->{'f01_pis_RIO_'};
	    $ladrillosPI = $figuraPisos[$i]->{'f01_pis_LAD_'};
	    $gramaPI = $figuraPisos[$i]->{'f01_pis_GRA_'};
	    $tierraPI = $figuraPisos[$i]->{'f01_pis_TIE_'};
	    $concretoPI = $figuraPisos[$i]->{'f01_pis_CON_'};
	    $ceramicaPI = $figuraPisos[$i]->{'f01_pis_CER_'};
	    $maderaPI = $figuraPisos[$i]->{'f01_pis_MAD_'};
	    $otrosPI = $figuraPisos[$i]->{'f01_pis_OTRO_'};
	   
	$html.= '<td width="33%" id="tdborde" VALIGN="TOP">';
		if($baldosaPI  != "NO" ){
		$html.= '<p>'.$baldosaP.'</p>';
		}
		if ($mosaicosPI != "NO") 
		{
          $html.= '<p>'.$mosaicosP.'</p>';
		}
		if($porcelanatoPI  != "NO" ){
		$html.= '<p>'.$porcelanatoP.'</p>';
		}
		if ($PlajaPI != "NO") 
		{
          $html.= '<p>'.$PlajaP.'</p>';
		}
		if($PrioPI  != "NO" ){
		$html.= '<p>'.$PrioP.'</p>';
		}
		if ($ladrillosPI != "NO") 
		{
          $html.= '<p>'.$ladrillosP.'</p>';
		}
		if($gramaPI  != "NO" ){
		$html.= '<p>'.$gramaP.'</p>';
		}
		if ($tierraPI != "NO") 
		{
          $html.= '<p>'.$tierraP.'</p>';
		}
		if ($concretoPI != "NO") 
		{
          $html.= '<p>'.$concretoP.'</p>';
		}
		if($ceramicaPI  != "NO" ){
		$html.= '<p>'.$ceramicaP.'</p>';
		}
		if ($maderaPI != "NO") 
		{
          $html.= '<p>'.$maderaP.'</p>';
		}
        if ($otrosPI != "NO") 
		{
          $html.= '<p>'.$otrosP.':  '.$figuraPisos[$i]->{'f01_pis_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="33%" id="tdborde">'.$figuraPisos[$i]->{'f01_pis_INT_'}.'</td></tr>';
	}
$html.= '</table>';

$html.=' <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>7. PUERTAS DE ACCESO</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_PUERTAEST.'</td>
	</tr>
</table>';

$html.='<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>';
$figuraPuertasA = $cas_datos->{'PTR_PUERTA'};  
	for($i = 1; $i < sizeof($figuraPuertasA); $i++){
	 	$titPA = $figuraPuertasA[0]->{'titulos'};
        $titulosPA = explode ( '|' , $titPA , - 1 ); 
    $html.= '<tr><td width="33%" id="tdborde">'.$figuraPuertasA[$i]->{'f01_pue_DES_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulosPA); $j++){
	    	$madera = $titulosPA[1];
	    	$metal = $titulosPA[2];
	    	$otros = $titulosPA[3];
	    }
        $madera1 = $figuraPuertasA[$i]->{'f01_pue_MAD_'};
	    $metal1 = $figuraPuertasA[$i]->{'f01_pue_MET_'};
	    $otros1 = $figuraPuertasA[$i]->{'f01_pue_OTRO_'};
	   
	$html.= '<td width="33%" id="tdborde" VALIGN="TOP">';
		if($madera1  != "NO" ){
		$html.= '<p>'.$madera.'</p>';
		}
		if ($metal1 != "NO") 
		{
          $html.= '<p>'.$metal.'</p>';
		}
        if ($otros1 != "NO") 
		{
          $html.= '<p>'.$otros.':  '.$figuraPuertasA[$i]->{'f01_pue_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="33%" id="tdborde">'.$figuraPuertasA[$i]->{'f01_pue_INT_'}.'</td></tr>';
	}
$html.= '</table>';

$html.=' 
 <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>8. MOBILIARIO URBANO</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación: '.$PTR_MOBEST.'</td>
	</tr>
</table>';

$html.='<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>';
	$figuraMobiliarioU = $cas_datos->{'PTR_MOBURB'};  
	for($i = 1; $i < sizeof($figuraMobiliarioU); $i++){
	 	$titMU = $figuraMobiliarioU[0]->{'titulos'};
        $titulosMU = explode ( '|' , $titMU , - 1 ); 
    $html.= '<tr><td width="33%" id="tdborde">'.$figuraMobiliarioU[$i]->{'f01_mob_DES_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulosMU); $j++){
	    	$madera4 = $titulosMU[1];
	    	$metal4 = $titulosMU[2];
	    	$barro4 = $titulosMU[3];
	    	$cal4 = $titulosMU[4];
	    	$ladrillo4 = $titulosMU[5];
	    	$piedra4 = $titulosMU[6];
	    	$adobe4 = $titulosMU[7];
	    	$concreto4 = $titulosMU[8];
	    	$Fcarbono4 = $titulosMU[9];
	    	$vidrio4 = $titulosMU[10];
	    	$fibrocem4 = $titulosMU[11];
	    	$otros4 = $titulosMU[12];
	    }
        $maderaMU = $figuraMobiliarioU[$i]->{'f01_mob_MAD_'};
	    $metalMU = $figuraMobiliarioU[$i]->{'f01_mob_MET_'};
	    $barroMU = $figuraMobiliarioU[$i]->{'f01_mob_BARR_'};
	    $calMU = $figuraMobiliarioU[$i]->{'f01_mob_CAL_'};
	    $ladrilloMU = $figuraMobiliarioU[$i]->{'f01_mob_LAD_'};
	    $piedraMU = $figuraMobiliarioU[$i]->{'f01_mob_PIE_'};
	    $adobeMU = $figuraMobiliarioU[$i]->{'f01_mob_ADO_'};
	    $concretoMU = $figuraMobiliarioU[$i]->{'f01_mob_CON_'};
	    $FcarbonoMU = $figuraMobiliarioU[$i]->{'f01_mob_CAR_'};
	    $vidrioMU = $figuraMobiliarioU[$i]->{'f01_mob_VID_'};
	    $fibrocemMU = $figuraMobiliarioU[$i]->{'f01_mob_FIBR_'};
	    $otrosMU = $figuraMobiliarioU[$i]->{'f01_mob_OTRO_'};
	$html.= '<td width="33%" id="tdborde" VALIGN="TOP">';
		if($maderaMU  != "NO" ){
		$html.= '<p>'.$madera4.'</p>';
		}
		if ($metalMU != "NO") 
		{
          $html.= '<p>'.$metal4.'</p>';
		}
		if($barroMU  != "NO" ){
		$html.= '<p>'.$barro4.'</p>';
		}
		if ($calMU != "NO") 
		{
          $html.= '<p>'.$cal4.'</p>';
		}
		if($ladrilloMU  != "NO" ){
		$html.= '<p>'.$ladrillo4.'</p>';
		}
		if ($piedraMU != "NO") 
		{
          $html.= '<p>'.$piedra4.'</p>';
		}
		if($adobeMU  != "NO" ){
		$html.= '<p>'.$adobe4.'</p>';
		}
		if ($concretoMU != "NO") 
		{
          $html.= '<p>'.$concreto4.'</p>';
		}
		if($FcarbonoMU  != "NO" ){
		 $html.= '<p>'.$Fcarbono4.'</p>';
		}
		if ($vidrioMU != "NO") 
		{
          $html.= '<p>'.$vidrio4.'</p>';
		}
		if ($fibrocemMU != "NO") 
		{
          $html.= '<p>'.$fibrocem4.'</p>';
		}
        if ($otrosMU != "NO") 
		{
          $html.= '<p>'.$otros4.':  '.$figuraMobiliarioU[$i]->{'f01_mob_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="33%" id="tdborde">'.$figuraMobiliarioU[$i]->{'f01_mob_INT_'}.'</td></tr>';
	}
$html.= '</table>';

$html.='<br>
<table  width="100%">
	<tr colspan=4>
		<td><b>9. ESCALERAS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación: '.$PTR_ESCEST.'</td>
	</tr>
</table>';

$html.='<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><b>Tipo</b></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>';
    $figuraEscalera = $cas_datos->{'PTR_ESCAL'};  
	for($i = 1; $i < sizeof($figuraEscalera); $i++){
	 	$titES = $figuraEscalera[0]->{'titulos'};
        $titulosES = explode ( '|' , $titES , - 1 ); 
    $html.= '<tr><td width="33%" id="tdborde">'.$figuraEscalera[$i]->{'f01_esc_DES_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulosES); $j++){
	    	$madera5 = $titulosES[1];
	    	$metal5 = $titulosES[2];
	    	$concreto5 = $titulosES[3];
	    	$piedra5 = $titulosES[4];
	    	$ladrillo5 = $titulosES[5];
	    	$tierra5 = $titulosES[6];
	    	$otro5 = $titulosES[7];
	    }
        $maderaES = $figuraEscalera[$i]->{'f01_esc_MAD_'};
	    $metalES = $figuraEscalera[$i]->{'f01_esc_MET_'};
	    $concretoES = $figuraEscalera[$i]->{'f01_esc_CON_'};
	    $piedraES = $figuraEscalera[$i]->{'f01_esc_PIE_'};
	    $ladrilloES = $figuraEscalera[$i]->{'f01_esc_LAD_'};
	    $tierraES = $figuraEscalera[$i]->{'f01_esc_TIE_'};
	    $otrosES = $figuraEscalera[$i]->{'f01_esc_OTRO_'};
	$html.= '<td width="33%" id="tdborde" VALIGN="TOP">';
		if($maderaES  != "NO" ){
		$html.= '<p>'.$madera5.'</p>';
		}
		if ($metalES != "NO") 
		{
          $html.= '<p>'.$metal5.'</p>';
		}
		if($concretoES  != "NO" ){
		$html.= '<p>'.$concreto5.'</p>';
		}
		if ($piedraES != "NO") 
		{
          $html.= '<p>'.$piedra5.'</p>';
		}
		if($ladrilloES  != "NO" ){
		$html.= '<p>'.$ladrillo5.'</p>';
		}
		if ($tierraES != "NO") 
		{
          $html.= '<p>'.$tierra5.'</p>';
		}
        if ($otrosES != "NO") 
		{
          $html.= '<p>'.$otro5.':  '.$figuraEscalera[$i]->{'f01_esc_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="33%" id="tdborde">'.$figuraEscalera[$i]->{'f01_esc_INT_'}.'</td></tr>';
	}
$html.= '</table>';

$html.='<br>				
<table  width="100%">
	<tr colspan=4>
		<td><b>10. CARACTERISTICAS DE LA VEGETACIÓN</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_VEGEST.'</td>
	</tr>
</table>';

$html.='<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="25%" id="tdborde"><b>Materiales</b></td>
		<td width="25%" id="tdborde"><b>Tipo</b></td>
		<td width="25%" id="tdborde"><b>Integridad</b></td>
		<td width="25%" id="tdborde"><b>Cantidad</b></td>
	</tr>';
    $figuraCaracteristicaVeg = $cas_datos->{'PTR_VEGET'};  
	for($i = 1; $i < sizeof($figuraCaracteristicaVeg); $i++){
	 	$titCV = $figuraCaracteristicaVeg[0]->{'titulos'};
        $titulosCV = explode ( '|' , $titCV , - 1 ); 
    $html.= '<tr><td width="25%" id="tdborde">'.$figuraCaracteristicaVeg[$i]->{'f01_veg_DES_'}.'</td>';
	
	    for($j = 0; $j < sizeof($titulosCV); $j++){
	    	$Sfloracion = $titulosCV[1];
	    	$caduca = $titulosCV[2];
	    	$perenne = $titulosCV[3];
	    	$otro6 = $titulosCV[4];
	    }
        $SfloracionVG = $figuraCaracteristicaVeg[$i]->{'f01_veg_SFL_'};
	    $caducaVG = $figuraCaracteristicaVeg[$i]->{'f01_veg_CAD_'};
	    $perenneVG = $figuraCaracteristicaVeg[$i]->{'f01_veg_PER_'};
	    $otro6VG = $figuraCaracteristicaVeg[$i]->{'f01_veg_OTRO_'};
	$html.= '<td width="25%" id="tdborde" VALIGN="TOP">';
		if($SfloracionVG  != "NO" ){
		$html.= '<p>'.$Sfloracion.'</p>';
		}
		if ($caducaVG != "NO") 
		{
          $html.= '<p>'.$caduca.'</p>';
		}
		if($perenneVG  != "NO" ){
		$html.= '<p>'.$perenne.'</p>';
		}
        if ($otro6VG != "NO") 
		{
          $html.= '<p>'.$otro6.':  '.$figuraCaracteristicaVeg[$i]->{'f01_veg_OTRODESC_'}.'</p>';
		}
		$html.= '</td>';
		$html.= '<td width="25%" id="tdborde">'.$figuraCaracteristicaVeg[$i]->{'f01_veg_INT_'}.'</td></tr>';
		$html.= '<td width="25%" id="tdborde">'.$figuraCaracteristicaVeg[$i]->{'f01_veg_CAN_'}.'</td></tr>';
	}
$html.= '</table>';

$html.='<br>
<table  width="100%">
	<tr colspan=4>
		<td><b>11. DETALLES ARTISTICOS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_DETALLEST.'</td>
	</tr>
</table>';																								
$html.='<br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="50%" id="tdborde"><b>Tipo</b></td>
		<td width="50%" id="tdborde"><b>Integridad</b></td>
	</tr>';
	 $figuraDetalleArtistico = $cas_datos->{'PTR_DETALLES'};  
		for($i = 1; $i < sizeof($figuraDetalleArtistico); $i++){ 
	    $html.= '<tr><td width="33%" id="tdborde">'.$figuraDetalleArtistico[$i]->{'f01_det_DES_'}.'</td>';
		$html.= '<td width="33%" id="tdborde" VALIGN="TOP">'.$figuraDetalleArtistico[$i]->{'f01_det_INT_'}.'</td></tr>';
	    }
$html.= '</table><br>';

			 $html.= '
					<div style="font-size:11px;">
						<table style="font-size:11px;" width="100%">';
							$longitudDoc=sizeof($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
						    if ($longitudDoc == 2) {
												
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							if ($doc_correlativo1 == 2) {
								$html.='
							<tr>
							<td width="50%" align="center">
								<b>X.'.$doc_titulo1.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>XI. ANGULO VISUAL HORIZONTAL</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								
							</td>
							
							</tr>';
							}
							if ($doc_correlativo1 == 3) {
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>X.ESQUEMA ARQUITECTONICO</b><br>
							</td>
								<td width="50%" align="center">
								<b>XI'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>';
							}
				
						   }
						   if ($longitudDoc == 3) {
												
				
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							
							</tr>';
							
						   }
						 
							$html.='       						
						</table>				
					</div>
			    ';

			    $html.='



<br>
<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%"><b>Vista Panoramica:</b></td>
		<td width="25%">
		';
			$longitud = count($PTR_VISTAS);
			for ($j=1; $j < $longitud; $j++) { 
			$estado = $PTR_VISTAS[$j]->{'estado'};
			$resvalor = $PTR_VISTAS[$j]->{'resvalor'};
			if($resvalor == 'Vista Panorámica')	
				if ($estado) {				  	
				$html.='
				<p>SI</p>
				';
				}
				else{				  	
				$html.='
				<p>NO</p>
				';
				}
			}
			$html.='
		</td>
	</tr>
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%"><b>Vista Parcial:</b></td>
		<td width="25%">
		';
			$longitud = count($PTR_VISTAS);
			for ($j=1; $j < $longitud; $j++) { 
			$estado = $PTR_VISTAS[$j]->{'estado'};
			$resvalor = $PTR_VISTAS[$j]->{'resvalor'};
			
			if($resvalor == 'Vista Parcial  ')	
				if ($estado) {				  	
				$html.='
				<p>SI</p>
				';
				}
				else{				  	
				$html.='
				<p>NO</p>
				';
				}
			}
			$html.='
		</td>
	</tr>
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%">Orientación:</td>
		<td width="25%">'.$PTR_ANGOR.'</td>
	</tr>
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%">Otros:</td>
		<td width="25%">'.$PTR_ANGOTR.'</td>
	</tr>			
</table >
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td ><b>XII. ELEMENTOS VISUALES</b></td>	    
	</tr>
</table>
<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td><b>De izquierda a Derecha</b></td>
	</tr>
	<tr>
		<td >'.$PTR_ELEMVISID.'
		</td>
	</tr>
	<tr>
		<td><b>A lo alto</b></td>
	</tr>
	<tr>
		<td>'.$PTR_ELEMVISAL.'</td>
	</tr>
	<tr>
		<td><b>A lo abajo</b></td>
	</tr>
	<tr>
		<td>'.$PTR_ELEMVISAB.'</td>
	</tr>			
</table >
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td width="50%"><b>XIII. DESCRIPCIÓN DEL ENTORNO INMEDIATO</b></td>	    
	</tr>
	<tr>
		<td width="25%" >'.$PTR_DESENTEI.'</td>
	</tr>	
</table>
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td ><b>XIV. DESCRIPCIÓN DEL ESPACIO</b></td>	    
	</tr>
	<tr>
		<td >'.$PTR_DESESPDES.'</td>
	</tr>	
</table>
<table  width="100%" cellspacing="-0.5" id="tdborde">
	<tr>
		<td width="25%" id="tdborde"><b>Area de Espacio</b></td>
		<td width="25%" id="tdborde">'.$PTR_MEDIDAREA.'</td>
		<td width="25%" id="tdborde"><b>Ancho</b></td>
		<td width="25%" id="tdborde">'.$PTR_MEDIDANC.'</td>
	</tr>	
	<tr>
		<td width="25%" id="tdborde"><b>Largo</b></td>
		<td width="25%" id="tdborde">'.$PTR_MEDIDLAR.'</td>
		<td width="25%" id="tdborde"><b>Ancho de muro de cerco</b></td>                                                 
		<td width="25%" id="tdborde">'.$PTR_MEDIAMC.'</td>
	</tr>					
</table >
<br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="50%"><b>XVI. INFORMACIÓN ADICIONAL</b></td>
	</tr>					
</table >
<table  width="100%" cellspacing="-0.5" id="tdborde">
	<tr>
		<td width="25%" id="tdborde">
			<b>Información <br> Adicional:</b>
		</td>
		<td width="25%" id="tdborde">
			'.$PTR_INFADICIONAL.'
		</td>
		<td width="25%" id="tdborde">
			<b>Observaciones</b>
		</td>
		<td width="25%" id="tdborde">
		   '.$PTR_INFOBS.'
		</td>
	</tr>					
</table >
 <br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="50%"><b>XVII. PATOLOGIAS</b></td>
	</tr>					
</table >
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="25%" id="tdborde"><b>Daños:</b></td>
		<td width="25%" id="tdborde">
		';
			$longitud = count($PTR_PATODAN);
			for ($j=1; $j < $longitud; $j++) { 
			$estado = $PTR_PATODAN[$j]->{'estado'};
				if ($estado) {				  	
				$resvalor = $PTR_PATODAN[$j]->{'resvalor'};

				$html.='
				<p>'.$resvalor.'</p>
				';
				}
			}
			$html.='
		</td>
		<td width="25%" id="tdborde"><b>Causas</b></td>
		<td width="25%" id="tdborde">
		';
	$longitud = count($PTR_PATOCAU);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_PATOCAU[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_PATOCAU[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='


		</td>
	</tr>
	<tr>
		<td width="25%" id="tdborde"><b>Otros Daños:</b></td>
		<td width="25%" id="tdborde">'.$PTR_PATO_OTROS.'</td>
		<td width="25%" id="tdborde"><b>Otras Causas:</b></td>
		<td width="25%" id="tdborde">'.$PTR_PATOCAU_OTROS.'</td>
	</tr>	
	<tr>
		<td width="25%" id="tdborde"><b>Observaciones:</b></td>
		<td width="25%" id="tdborde">'.$PTR_PATOOBS.'</td>
		<td width="25%" id="tdborde"><b></b></td>
		<td width="25%" id="tdborde"></td>
	</tr>						
</table >
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td width="50%"><b>XVIII. VALORACIÓN</b>
		</td>	    
	</tr>
</table>
 <br>

<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td><b>Indicador Histórico Cultural:</b>
		</td>
	</tr>
	<tr>
		<td>
			';
	$longitud = count($PTR_VALORHIS);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORHIS[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORHIS[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Artístico</b>
		</td>
	</tr>
	<tr>
		<td>
					';
	$longitud = count($PTR_VALORART);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORART[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORART[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Arquitectónico</b>
		</td>
	</tr>
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORARQ);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORARQ[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORARQ[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Tecnológico</b>
		</td>
	</tr>
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORTEC);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORTEC[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORTEC[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>	
	<tr>
		<td><br><b>Indicador de Integridad</b>
		</td>
	</tr>
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORINTG);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORINTG[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORINTG[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Urbano</b>
		</td>
	</tr>	
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORURB);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORURB[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORURB[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Intangibilidad</b>
		</td>
	</tr>
	<tr>
		<td>
			';
	$longitud = count($PTR_VALORINT);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORINT[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORINT[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Simbólico</b>
		</td>
	</tr>
	<tr>
		<td>
';
	$longitud = count($PTR_VALORSIMB);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORSIMB[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORSIMB[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='

		</td>
	</tr>
	
</table >

<br>
<table  width="100%">
	<tr colspan=4>           
		<td width="50%"><b>XIX. CRITERIOS DE VALORACIÓN</b>
		</td>	    
	</tr>
	
</table>

<table  width="100%" cellspacing="-0.5">
	<tr>           
		<td><br><b>Indicador Histórico Cultural</b>
		</td>	    
	</tr>
	<tr>
		<td>'.$PTR_CRITERHIS.'
		</td>
	</tr>	
	<tr>
		<td><br><b>Indicador Artístico</b>
		</td>
	</tr>
	<tr>
		<td >'.$PTR_CRITERART.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Arquitectónico</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERARQ.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Tecnológico</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERTEC.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador de Integridad</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERINT.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Urbano</b>
		</td>
	</tr>
		<tr>
		<td>'.$PTR_CRITERURB.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador de Intangibilidad</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERINTA.'
		</td>
	</tr>	
	<tr>
		<td><br><b>Indicador Simbólico</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERSIMB.'
		</td>
	</tr>						
</table >

	';

	
			$mpdf->AddPage();
			$mpdf->WriteHTML($html);   
			$mpdf->Output();				  
	}

	public function pdfArchFoto($datos, $datosDocumentos)
	{
		  //dd($datosDocumentos);
		  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  //-----------Documentos----------------------
	      $longitudDoc = 0;
		

		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter', 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		   
		   $mpdf->SetHTMLHeader('
             <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b><b>ARCHIVO FOTOGRÁFICO DE ESPACIOS ABIERTOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');

		  $mpdf->SetHTMLFooter('
			<table width="100%">
			    <tr>
			        <td width="33%">Para cualquier consulta, comunicarse al Teléfono. 2-2440746</td>
			        <td width="33%" align="center">Página {PAGENO} de {nbpg}</td>
			        
			    </tr>
			</table>');
	$html = ' 
		<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		    	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 50%;
	    	
	    }
	    </style>
	      
	        ';

 $html.= '
					<div style="font-size:11px;">
						<table style="font-size:11px;" width="100%" >';
							$longitudDoc=sizeof($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
							
							 if ($longitudDoc == 1) {
						
							$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};							
							$html.='
							<tr>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							
							</tr>

							';
						   }
						    if ($longitudDoc == 2) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						  	<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>

							';
						   }
						   if ($longitudDoc == 3) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						   if ($longitudDoc == 4) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<br><img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						    if ($longitudDoc == 5) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
							$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo4.'</b><br><br>
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b></b><br><br>
								<img src="" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

						   	    if ($longitudDoc == 6) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
							$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
							$doc_correlativo5 = $datosDocumentos[5]->{'xdoc_correlativo'};
						    $doc_titulo5 = $datosDocumentos[5]->{'xdoc_palabras'};
							$doc_url_logica5 = $datosDocumentos[5]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo4.'</b><br><br>
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo5.'</b><br><br>
								<img src="'.$doc_url_logica5.'" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

							$html.='       						
						</table>				
					</div>
			    ';

			$mpdf->AddPage();
			$mpdf->WriteHTML($html);   
			$mpdf->Output();
	}
public function pdfImprimirTodo($datos , $datosDocumentos ,$datosDocumentosF)
{
	  	 //dd($datos);
	  	  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $cas_id = $datos[0]->{'xcas_id'};
		  $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		  $cas_act_id = $datos[0]->{'xcas_act_id'};
		  $cas_usr_actual_id = $datos[0]->{'xcas_usr_actual_id'};
		  
		  $cas_datos = $datos[0]->{'xcas_datos'};
		  $cas_datos = json_decode($cas_datos);
		  $g_tipo = $cas_datos->{'g_tipo'};
		  $PTR_CREA = $cas_datos->{'PTR_CREA'};
		  $PTR_ACTUALIZA = $cas_datos->{'PTR_ACTUALIZA'};
		  $PTR_COD_CAT = $cas_datos->{'PTR_COD_CAT'};
		  $PTR_GEOREF = $cas_datos->{'PTR_GEOREF'};
		  $PTR_TRAD = $cas_datos->{'PTR_TRAD'};
		  $PTR_ACT = $cas_datos->{'PTR_ACT'};
		  $PTR_DEP = $cas_datos->{'PTR_DEP'};
		  $PTR_CIPO = $cas_datos->{'PTR_CIPO'};
		  $PTR_MUNI = $cas_datos->{'PTR_MUNI'};
		  $PTR_MACRO = $cas_datos->{'PTR_MACRO'};
		  $PTR_BARR = $cas_datos->{'PTR_BARR'};
		  $PTR_DIR = $cas_datos->{'PTR_DIR'};
		  $PTR_ESQ1 = $cas_datos->{'PTR_ESQ1'};
		  $PTR_ESQ2 = $cas_datos->{'PTR_ESQ2'};
		  $PTR_ALT = $cas_datos->{'PTR_ALT'};
		  $PTR_NOMPRO = $cas_datos->{'PTR_NOMPRO'};
		  $PTR_REPRO = $cas_datos->{'PTR_REPRO'};

			$PTR_USO = $cas_datos->{'PTR_USO'};
			$PTR_SERV = $cas_datos->{'PTR_SERV'};
			$PTR_FIGPRO = $cas_datos->{'PTR_FIGPRO'};
			$PTR_OTHER = $cas_datos->{'PTR_OTHER'};
			$PTR_REFHIST = $cas_datos->{'PTR_REFHIST'};
			$PTR_FECCONS = $cas_datos->{'PTR_FECCONS'};
			$PTR_AUTOR = $cas_datos->{'PTR_AUTOR'};
			$PTR_PREEX = $cas_datos->{'PTR_PREEX'};
			$PTR_PREEX = $this->caracteresEspecialesActualizar($PTR_PREEX);
			$PTR_PROP = $cas_datos->{'PTR_PROP'};
			$PTR_USOORG = $cas_datos->{'PTR_USOORG'};
			$PTR_USOORG = $this->caracteresEspecialesActualizar($PTR_USOORG);
			$PTR_HECHIS = $cas_datos->{'PTR_HECHIS'};
			$PTR_HECHIS = $this->caracteresEspecialesActualizar($PTR_HECHIS);
			$PTR_FIEREL = $cas_datos->{'PTR_FIEREL'};
			$PTR_FIEREL = $this->caracteresEspecialesActualizar($PTR_FIEREL);
			$PTR_DATHISC = $cas_datos->{'PTR_DATHISC'};
			$PTR_DATHISC = $this->caracteresEspecialesActualizar($PTR_DATHISC);
			$PTR_FUENTE = $cas_datos->{'PTR_FUENTE'};
			$PTR_FUENTE = $this->caracteresEspecialesActualizar($PTR_FUENTE);
			$PTR_FECCONST = $cas_datos->{'PTR_FECCONST'};
			$PTR_AUTCONST = $cas_datos->{'PTR_AUTCONST'};
			$PTR_PREDIN = $cas_datos->{'PTR_PREDIN'};
			$PTR_PREDIN = $this->caracteresEspecialesActualizar($PTR_PREDIN);
			$PTR_PROPIE = $cas_datos->{'PTR_PROPIE'};
			$PTR_USOPREEX = $cas_datos->{'PTR_USOPREEX'};
			$PTR_USOPREEX = $this->caracteresEspecialesActualizar($PTR_USOPREEX);
			$PTR_HECHISD = $cas_datos->{'PTR_HECHISD'};
			$PTR_HECHISD = $this->caracteresEspecialesActualizar($PTR_HECHISD);
			$PTR_FIERELD = $cas_datos->{'PTR_FIERELD'};
			$PTR_FIERELD = $this->caracteresEspecialesActualizar($PTR_FIERELD);
			$PTR_DATHISTC = $cas_datos->{'PTR_DATHISTC'};
			$PTR_DATHISTC = $this->caracteresEspecialesActualizar($PTR_DATHISTC);
			$PTR_FUENTBIO = $cas_datos->{'PTR_FUENTBIO'};
			$PTR_FUENTBIO = $this->caracteresEspecialesActualizar($PTR_FUENTBIO);
			$PTR_INSCR = $cas_datos->{'PTR_INSCR'};
			$PTR_INSCR = $this->caracteresEspecialesActualizar($PTR_INSCR);
			$PTR_EPOCA = $cas_datos->{'PTR_EPOCA'}; 
			$PTR_ACONS = $cas_datos->{'PTR_ACONS'};
			$PTR_AULTR = $cas_datos->{'PTR_AULTR'};
			$PTR_ESTILO = $cas_datos->{'PTR_ESTILO'};
			$PTR_UBICESP = $cas_datos->{'PTR_UBICESP'};
			$PTR_LINCONS = $cas_datos->{'PTR_LINCONS'};
			$PTR_TIPOLO = $cas_datos->{'PTR_TIPOLO'};
			//--
			$PTR_MURMAT = $cas_datos->{'PTR_MURMAT'};
			
			$PTR_MURMAT_OTROS = $cas_datos->{'PTR_MURMAT_OTROS'};
			$PTR_MUREST = $cas_datos->{'PTR_MUREST'};
			$PTR_MURINTE = $cas_datos->{'PTR_MURINTE'};
			$PTR_REJAS = $cas_datos->{'PTR_REJAS'};
			$PTR_REJAEST = $cas_datos->{'PTR_REJAEST'};
			$PTR_ACAMUR = $cas_datos->{'PTR_ACAMUR'};
			$PTR_ACAEST = $cas_datos->{'PTR_ACAEST'};
			$PTR_PINTURA = $cas_datos->{'PTR_PINTURA'};
			$PTR_PINTEST = $cas_datos->{'PTR_PINTEST'};
			$PTR_PISOS = $cas_datos->{'PTR_PISOS'};
			$PTR_PISOEST = $cas_datos->{'PTR_PISOEST'};
			$PTR_PUERTA = $cas_datos->{'PTR_PUERTA'};
			$PTR_PUERTAEST = $cas_datos->{'PTR_PUERTAEST'};
			$PTR_MOBURB = $cas_datos->{'PTR_MOBURB'};
			$PTR_MOBEST = $cas_datos->{'PTR_MOBEST'};
			$PTR_ESCAL = $cas_datos->{'PTR_ESCAL'};
			$PTR_ESCEST = $cas_datos->{'PTR_ESCEST'};
			$PTR_VEGET = $cas_datos->{'PTR_VEGET'};
			$PTR_VEGEST = $cas_datos->{'PTR_VEGEST'};
			$PTR_DETALLES = $cas_datos->{'PTR_DETALLES'};
			$PTR_DETALLEST = $cas_datos->{'PTR_DETALLEST'};
			$PTR_ESQDES = $cas_datos->{'PTR_ESQDES'};
			$PTR_ESQDES = $this->caracteresEspecialesActualizar($PTR_ESQDES);
			$PTR_ANGOR = $cas_datos->{'PTR_ANGOR'};
			$PTR_ANGGRA = $cas_datos->{'PTR_ANGGRA'};
			//--
			$PTR_VISTAS = $cas_datos->{'PTR_VISTAS'};

			$PTR_ANGOTR = $cas_datos->{'PTR_ANGOTR'};
			$PTR_ELEMVISID = $cas_datos->{'PTR_ELEMVISID'};
			$PTR_ELEMVISID = $this->caracteresEspecialesActualizar($PTR_ELEMVISID);
			$PTR_ELEMVISAL = $cas_datos->{'PTR_ELEMVISAL'};
			$PTR_ELEMVISAL = $this->caracteresEspecialesActualizar($PTR_ELEMVISAL);
			$PTR_ELEMVISAB = $cas_datos->{'PTR_ELEMVISAB'};
			$PTR_ELEMVISAB = $this->caracteresEspecialesActualizar($PTR_ELEMVISAB);
			$PTR_DESENTEI = $cas_datos->{'PTR_DESENTEI'};
			$PTR_DESENTEI = $this->caracteresEspecialesActualizar($PTR_DESENTEI);
			$PTR_DESESPDES = $cas_datos->{'PTR_DESESPDES'};
			$PTR_DESESPDES = $this->caracteresEspecialesActualizar($PTR_DESESPDES);
			$PTR_MEDIDAREA = $cas_datos->{'PTR_MEDIDAREA'};
			$PTR_MEDIDANC = $cas_datos->{'PTR_MEDIDANC'};
			$PTR_MEDIDLAR = $cas_datos->{'PTR_MEDIDLAR'};
			$PTR_MEDIAMC = $cas_datos->{'PTR_MEDIAMC'};
			$PTR_INFADICIONAL = $cas_datos->{'PTR_INFADICIONAL'};
			$PTR_INFOBS = $cas_datos->{'PTR_INFOBS'};
			$PTR_INFOBS = $this->caracteresEspecialesActualizar($PTR_INFOBS);
			//--
			$PTR_PATODAN = $cas_datos->{'PTR_PATODAN'};

			$PTR_PATO_OTROS = $cas_datos->{'PTR_PATO_OTROS'};
			$PTR_PATOOBS = $cas_datos->{'PTR_PATOOBS'};
			$PTR_PATOOBS = $this->caracteresEspecialesActualizar($PTR_PATOOBS);
			//--
			$PTR_PATOCAU = $cas_datos->{'PTR_PATOCAU'};
			
			$PTR_PATOCAU_OTROS = $cas_datos->{'PTR_PATOCAU_OTROS'};
			//--
			$PTR_VALORHIS = $cas_datos->{'PTR_VALORHIS'};
			//--
			$PTR_VALORART = $cas_datos->{'PTR_VALORART'};
			//--
			$PTR_VALORARQ = $cas_datos->{'PTR_VALORARQ'};
			//--
			$PTR_VALORTEC = $cas_datos->{'PTR_VALORTEC'};
			//--
			$PTR_VALORINTG = $cas_datos->{'PTR_VALORINTG'};
			//--
			$PTR_VALORURB = $cas_datos->{'PTR_VALORURB'};
			//--
			$PTR_VALORINT = $cas_datos->{'PTR_VALORINT'};
			//--
			$PTR_VALORSIMB = $cas_datos->{'PTR_VALORSIMB'};

			$PTR_CRITERHIS = $cas_datos->{'PTR_CRITERHIS'};
			$PTR_CRITERHIS = $this->caracteresEspecialesActualizar($PTR_CRITERHIS);
			$PTR_CRITERART = $cas_datos->{'PTR_CRITERART'};
		    $PTR_CRITERART = $this->caracteresEspecialesActualizar($PTR_CRITERART);
			$PTR_CRITERARQ = $cas_datos->{'PTR_CRITERARQ'};
			$PTR_CRITERARQ = $this->caracteresEspecialesActualizar($PTR_CRITERARQ);
			$PTR_CRITERTEC = $cas_datos->{'PTR_CRITERTEC'};
			$PTR_CRITERTEC = $this->caracteresEspecialesActualizar($PTR_CRITERTEC);
			$PTR_CRITERINT = $cas_datos->{'PTR_CRITERINT'};
			$PTR_CRITERINT = $this->caracteresEspecialesActualizar($PTR_CRITERINT);
			$PTR_CRITERURB = $cas_datos->{'PTR_CRITERURB'};
			$PTR_CRITERURB = $this->caracteresEspecialesActualizar($PTR_CRITERURB);
			$PTR_CRITERINTA = $cas_datos->{'PTR_CRITERINTA'};
			$PTR_CRITERINTA = $this->caracteresEspecialesActualizar($PTR_CRITERINTA);
			$PTR_CRITERSIMB = $cas_datos->{'PTR_CRITERSIMB'};
			$PTR_CRITERSIMB = $this->caracteresEspecialesActualizar($PTR_CRITERSIMB);
			$PTR_PERINV = $cas_datos->{'PTR_PERINV'};
			$PTR_PERREV = $cas_datos->{'PTR_PERREV'};
			$PTR_PERDIG = $cas_datos->{'PTR_PERDIG'};

		  
		  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  $cas_estado_paso = $datos[0]->{'xcas_estado_paso'};
		  $cas_fecha_inicio = $datos[0]->{'xcas_fecha_inicio'};
		  $cas_fecha_limite = $datos[0]->{'xcas_fecha_limite'};
		  $cas_nodo_id = $datos[0]->{'xcas_nodo_id'};
		  $cas_registrado = $datos[0]->{'xcas_registrado'};
		  $cas_modificado = $datos[0]->{'xcas_modificado'};
		  $cas_usr_id = $datos[0]->{'xcas_usr_id'};
		  $cas_estado = $datos[0]->{'xcas_estado'};
		  $cas_ws_id = $datos[0]->{'xcas_ws_id'};
		  $cas_asunto = $datos[0]->{'xcas_asunto'};
		  $cas_tipo_hr = $datos[0]->{'xcas_tipo_hr'};
		  $cas_id_padre = $datos[0]->{'xcas_id_padre'};
		  $campo_f = $datos[0]->{'xcampo_f'};
		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter', 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);

		$html = '<htmlpageheader name="myheaderFicha">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE CATALOGACIÓN DE ESPACIOS ABIERTOS
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
		$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';
			
		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');

	$html .= ' 
	<style>

	#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	}
	table{
		font-size:12px;
		text-align : justify;
	}
	#tdbordeArchFot{
		   
		    	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	 
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }
	</style>
	     ';

	$html.= '<br><br><br><br><br><br>
	<table style="font-size:11px;" width="100%">
		<tr>
			<td><b>I. IDENTIFICACI&Oacute;N</b></td>
		</tr>
		<tr>
		    <td><b>1.PLANO DE UBICACIÓN</b></td>
		</tr>
		
    </table>';

			 $html.= '
				
						<table style="font-size:11px;" width="100%">';
							$longitudDoc=sizeof($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
							
						
							$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};							
							$html.='
							<tr>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
													
							</tr>

							';
						

							$html.='       						
						</table>		
						    ';

			    $html.='
	<br>
	<table style="font-size:11px;" width="100%">
		<tr colspan=4>
		<td><b>2.DATOS GENERALES</b></td>
		</tr>
    </table>

	<table style="font-size:11px;" width="100%"  cellspacing="-0.5">
	<tr>
	<td width="25%" id="tdborde"><b>C&oacute;digo de Catastro:</b></td>
	<td width="25%" id="tdborde">'.$PTR_COD_CAT.'</td>
	<td width="25%" id="tdborde"><b>Georeferencia:</b></td>
	<td width="25%" id="tdborde">'.$PTR_GEOREF.'</td>
	</tr>
	</table >

	<br>
	<table style="font-size:11px;" width="100%">
	<tr colspan=4>
	<td ><b>3.DENOMINACIÓN DEL INMUEBLE</b></td>
	</tr>
	</table >
	
  <table style="font-size:11px;" width="100%"  cellspacing="-0.5">
    <tr>
	  <td width="25%" id="tdborde"><b>Tradicional:</b></td>
  	  <td width="25%" id="tdborde">'.$PTR_TRAD.'</td>
	  <td width="25%" id="tdborde"><b>Actual:</b></td>
	  <td width="25%" id="tdborde">'.$PTR_ACT.'</td>
	</tr>				
 </table >
 <br>
 <table style="font-size:11px;" width="100%" >
   <tr colspan=4>
	<td><b>4.LOCALIZACIÓN</b></td>
   </tr>
 </table >

 <table style="font-size:11px;" width="100%"  cellspacing="-0.5">
    <tr>
	  <td width="25%" id="tdborde"><b>Departamento:</b></td>
  	  <td width="25%" id="tdborde">'.$PTR_DEP.'</td>
	  <td width="25%" id="tdborde"><b>Dirección:</b></td>
	  <td width="25%" id="tdborde">'.$PTR_DIR.'</td>
	</tr>
	<tr>
	  <td id="tdborde"><b>Municipio:</b></td>
  	  <td id="tdborde">'.$PTR_MUNI.'</td>
	  <td id="tdborde"><b>Esq. o Entre Calles 1</b></td>
	  <td id="tdborde">'.$PTR_ESQ1.'</td>
	</tr>
	<tr>
	  <td id="tdborde"><b>Ciudad/Poblado:</b></td>
  	  <td id="tdborde">'.$PTR_CIPO.'</td>
	  <td id="tdborde"><b>Esq. o Entre Calles 2</b></td>
	  <td id="tdborde">'.$PTR_ESQ2.'</td>
	</tr>
	<tr>
	  <td id="tdborde"><b>Macro-distrito:</b></td>
  	  <td id="tdborde">'.$PTR_MACRO.'</td>
	  <td id="tdborde"><b>Altitud(m.s.n.m.)</b></td>
	  <td id="tdborde">'.$PTR_ALT.'</td>
	</tr>
	<tr>
	  <td width="20%" id="tdborde"><b>Barrio:</b></td>
  	  <td id="tdborde">'.$PTR_BARR.'</td>
	  <td id="tdborde"><b></b></td>
	  <td id="tdborde"></td>
	</tr>
 </table >
 <br>
 <table style="font-size:11px;" width="100%">
	<tr colspan=4>               
	 <td><b>5.RÉGIMEN DE PROPIEDAD</b></td>	    
	</tr>
 </table >

 <table style="font-size:11px;" width="100%"  align="center" cellspacing="-0.5">
	<tr>
	<td width="25%" id="tdborde"><b>Nombre Propietario:</b></td>
	<td width="25%" id="tdborde">'.$PTR_NOMPRO.'</td>
	<td width="25%" id="tdborde"><b>Propiedad:</b></td>
	<td width="25%" id="tdborde">'.$PTR_REPRO.'</td>
	</tr>
	<tr>
	<td width="25%" id="tdborde"><b>Otros Servicios:</b></td>
	<td width="25%" id="tdborde">

	';
	$longitud = count($PTR_OTHER);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_OTHER[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_OTHER[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='

	</td>
	<td width="25%" id="tdborde"><b>Propiedad:</b></td>
	<td width="25%" id="tdborde"></td>
	</tr>
 </table>
  <br>
 <table style="font-size:11px;" width="100%">
 	<tr colspan=4>
 		<td><b>II. MARCO LEGAL</b></td>
 	</tr>
 	<tr colspan=4>
 		<td><b>Figura de Proteccion Legal:</b>'.$PTR_FIGPRO.'</td>
 	</tr>
 </table>
  <br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td id="tdborde">
			<table width="100%">
				<tr>
					<td width="25%"><b>Fecha Construccion:</b></td>
					<td width="25%">'.$PTR_FECCONS.'</td>
					<td width="25%"><b>Autor/Constructor:</b></td>
					<td width="25%">'.$PTR_AUTOR.'</td>
				</tr>
				<tr>
					<td width="25%"><b>Propietarios:</b></td>
					<td width="25%">'.$PTR_PROP.'</td>
					<td width="25%"><b></b></td>
					<td width="25%"></td>
				</tr>				
			</table >
		</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Preexsistencias/Edificaciones/Intevenciones</b></td>
	</tr>
	<tr >
		<td>'.$PTR_PREEX.'</td>
	</tr>

</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Usos Originales</b></td>
	</tr>
	<tr>
		<td>'.$PTR_USOORG.'</td>
	</tr>

</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Hechos Históricos/Personajes Distinguidos</b></td>
	</tr>
	<tr >
		<td align="justify">'.$PTR_HECHIS.'</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>Fiestas Religiosas</b></td>
	</tr>
	<tr >
		<td>'.$PTR_FIEREL.'</td>
	</tr>

</table>
 <br>
<table width="100%">
	<tr>
		<td><b>Datos Históricos del Conjunto</b></td>
	</tr>
	<tr>
		<td align="justify">'.$PTR_DATHISC.'</td>
	</tr>
</table>
 <br>
<table width="100%">
	<tr>
		<td><b>Fuentes Bibliográficas</b></td>
	</tr>
	<tr>
		<td>'.$PTR_FUENTE.'</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr>
		<td><b>V.DATOS DOCUMENTALES</b></td>
	</tr>

</table >
 <br>
<table  width="100%" cellspacing="-0.5" align="center">
	<tr>
		<td id="tdborde">

			<table width="100%" align="center">
				<tr>
					<td width="25%"><b>Fecha Construccion:</b></td>
					<td width="25%">'.$PTR_FECCONST.'</td>
					<td width="25%"><b>Autor/Constructor:</b></td>
					<td width="25%">'.$PTR_AUTCONST.'</td>
				</tr>
				<tr>
					<td width="25%"><b>Propietarios::</b></td>
					<td width="25%">'.$PTR_PROPIE.'</td>
					<td width="25%"><b></b></td>
					<td width="25%"></td>

				</tr>				
			</table >

		</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr >
		<td><b>Preexistencia/ Edificación/ Intervenciones:</b></td>
	</tr>
	<tr>
		<td>'.$PTR_USOPREEX.'</td>
	</tr>
</table>

<table width="100%">
	<tr >
		<td><b>Usos Originales</b></td>
	</tr>
	<tr>
		<td>Espacio Abierto</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>Hechos Históricos/ Personajes Distinguidos</b></td>
	</tr>
	<tr colspan=4>
		<td>'.$PTR_HECHISD.'</td>
	</tr>
</table>
 <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>Fiestas Religiosas</b></td>
	</tr>
	<tr colspan=4>
		<td><b>'.$PTR_FIERELD.'</b></td>
	</tr>
</table>
<br>
<table  width="100%">
	<tr colspan=4>
		<td><b>Datos Históricos del Conjunto</b></td>
	</tr>
	<tr colspan=4>
		<td align="justify">'.$PTR_DATHISTC.'</td>
	</tr>
</table>
 <br>
<table width="100%">
	<tr colspan=4>
		<td><b>Fuentes Bibliográficas</b></td>
	</tr>
	<tr colspan=4>
		<td align="justify">'.$PTR_FUENTBIO.'</td>
	</tr>
</table>
 <br>
<table width="100%">
	<tr colspan=4>
		<td><b>VI. INSCRIPCIONES</b></td>
	</tr>
	<tr colspan=4>
		<td align="justify">'.$PTR_INSCR.'</td>
	</tr>
</table>
<br>
<table  width="100%"  cellspacing="-0.5">
	<tr colspan=4>
		<td><b>VII. EPOCA </b></td>
	</tr>
	<tr>
		<td width="25%" id="tdborde"><b>Época:</b></td>
		<td width="25%" id="tdborde">'.$PTR_EPOCA.'</td>
		<td width="25%" id="tdborde"><b>Año de Construcción:</b></td>
		<td width="25%" id="tdborde">'.$PTR_ACONS.'</td>
	</tr>
	<tr>
		<td width="25%" id="tdborde"><b>Ultima Remo-delación:</b></td>
		<td width="25%" id="tdborde">'.$PTR_AULTR.'</td>
		<td width="25%" id="tdborde"><b></b></td>
		<td width="25%" id="tdborde"></td>
	</tr>


</table >
 <br>
<table width="100%">

	<tr colspan=4>
		<td><b>VIII. ESTILO</b></td>
	</tr>

</table>
 <br>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="25%" id="tdborde"><b>Estilo:</b></td>
		<td width="25%" id="tdborde">'.$PTR_ESTILO.'</td>
		<td width="25%" id="tdborde"><b></b></td>
		<td width="25%" id="tdborde"></td>
	</tr>


</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>VIII. DESCRIPCIÓN DEL ÁREA</b></td>
	</tr>

</table>
 <br>
<table width="100%"  cellspacing="-0.5">
	<tr>
		<td width="25%" id="tdborde"><b>Ubicación del Medianero Espacio en el Area:</b></td>
		<td width="25%" id="tdborde">'.$PTR_UBICESP.'</td>
		<td width="25%" id="tdborde"><b>Linea de Construcción</b></td>
		<td width="25%" id="tdborde">'.$PTR_LINCONS.'</td>
	</tr>
</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>1. TIPOLOGÍA</b></td>
	</tr>

</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="25%" id="tdborde"><b>Tipológia:</b></td>
		<td width="25%" id="tdborde">'.$PTR_TIPOLO.'</td>
		<td width="25%" id="tdborde"><b> </b></td>
		<td width="25%" id="tdborde"></td>
	</tr>


</table >	
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>2. MUROS DE DELIMITACIÓN</b></td>
	</tr>

</table>
<table width="100%"  cellspacing="-0.5">

	<tr>
		<td width="25%" id="tdborde"><b>Materiales:</b></td>
		<td width="25%" id="tdborde">';
		 	      $longitud = count($PTR_MURMAT);
				  for ($j=1; $j < $longitud; $j++) { 
				  	$estado = $PTR_MURMAT[$j]->{'estado'};
					   if ($estado) {				  	
					     $resvalor = $PTR_MURMAT[$j]->{'resvalor'};
					    
					      $html.='
					      <p>'.$resvalor.'</p>
					      ';
					  	}
				  }
		     $html.='
		</td>
		<td width="25%" id="tdborde"><b>Integridad:</b></td>
		<td width="25%" id="tdborde">'.$PTR_MURINTE.' </td>
	</tr>
	<tr>
		<td width="25%" id="tdborde"><b>Estado de Conservación:</b></td>
		<td width="25%" id="tdborde">'.$PTR_MUREST.'</td>
		<td width="25%" id="tdborde"><b> </b></td>
		<td width="25%" id="tdborde"> </td>
	</tr>


</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>3. REJAS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_REJAEST.'</td>
	</tr>

</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>

	</tr>
</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>4.ACABADOS DE MUROS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_ACAEST.'</td>
	</tr>


</table>
<table width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>

	</tr>
</table >
 <br>	
<table width="100%">

	<tr colspan=4>
		<td><b>5. PINTURAS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_PINTEST.'</td>
	</tr>


</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>

	</tr>
</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>6. PISOS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_PISOEST.'</td>
	</tr>


</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>
	<tr>
		<td width="33%" id="tdborde">Interior</td>
		<td width="33%" id="tdborde">Mosaicos</td>
		<td width="33%" id="tdborde">Nuevo</td>
	</tr>
</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>7. PUERTAS DE ACCESO</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_PUERTAEST.'</td>
	</tr>


</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>

</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>8. MOBILIARIO URBANO</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación: '.$PTR_MOBEST.'</td>
	</tr>


</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>
	<tr>
		<td width="33%" id="tdborde">Basureros</td>
		<td width="33%" id="tdborde">Adobe</td>
		<td width="33%" id="tdborde">Nuevo</td>
	</tr>

</table >
 <br>
<table  width="100%">

	<tr colspan=4>
		<td><b>9. ESCALERAS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación: '.$PTR_ESCEST.'</td>
	</tr>


</table>
<table  width="100%"  cellspacing="-0.5">

	<tr>
		<td width="33%" id="tdborde"><b>Materiales:</b></td>
		<td width="33%" id="tdborde"><B>Tipo</B></td>
		<td width="33%" id="tdborde"><b>Integridad</b></td>
	</tr>
</table >
 <br>		
<table  width="100%">
	<tr colspan=4>
		<td><b>10. CARACTERISTICAS DE LA VEGETACIÓN</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_VEGEST.'</td>
	</tr>

</table>

<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="25%" id="tdborde"><b>Materiales:</b></td>
		<td width="25%" id="tdborde"><B>Tipo</B></td>
		<td width="25%" id="tdborde"><b>Integridad</b></td>
		<td width="25%" id="tdborde"><b>Cantidad</b></td>
	</tr>
	<tr>
		<td width="25%" id="tdborde">Arbustos</td>
		<td width="25%" id="tdborde">Caduca</td>
		<td width="25%" id="tdborde">Modificado</td>
		<td width="25%" id="tdborde"></td>
	</tr>

</table >
 <br>
<table  width="100%">
	<tr colspan=4>
		<td><b>11. DETALLES ARTISTICOS</b></td>
	</tr>
	<tr colspan=4>
		<td> Estado de conservación:'.$PTR_DETALLEST.'</td>
	</tr>

</table>																								

<br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="50%" id="tdborde"><b>Tipo</b></td>
		<td width="50%" id="tdborde"><b>Integridad</b></td>

	</tr>
	<tr>
		<td width="50%" id="tdborde">Esculturas</td>
		<td width="50%" id="tdborde">Original</td>

	</tr>	
	
</table >
<br>

';

			 $html.= '
					<div style="font-size:11px;">
						<table style="font-size:11px;" width="100%">';
							$longitudDoc=sizeof($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
						    if ($longitudDoc == 2) {
												
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							if ($doc_correlativo1 == 2) {
								$html.='
							<tr>
							<td width="50%" align="center">
								<b>X.'.$doc_titulo1.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>XI. ANGULO VISUAL HORIZONTAL</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								
							</td>
							
							</tr>';
							}
							if ($doc_correlativo1 == 3) {
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>X.ESQUEMA ARQUITECTONICO</b><br>
							</td>
								<td width="50%" align="center">
								<b>XI'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>';
							}
				
						   }
						   if ($longitudDoc == 3) {
												
				
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							
							</tr>';
							
						   }
						 
							$html.='       						
						</table>				
					</div>
			    ';

			    $html.='



<br>
<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%"><b>Vista Panoramica:</b></td>
		<td width="25%">
		';
			$longitud = count($PTR_VISTAS);
			for ($j=1; $j < $longitud; $j++) { 
			$estado = $PTR_VISTAS[$j]->{'estado'};
			$resvalor = $PTR_VISTAS[$j]->{'resvalor'};
			if($resvalor == 'Vista Panorámica')	
				if ($estado) {				  	
				$html.='
				<p>SI</p>
				';
				}
				else{				  	
				$html.='
				<p>NO</p>
				';
				}
			}
			$html.='
		</td>
	</tr>
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%"><b>Vista Parcial:</b></td>
		<td width="25%">
		';
			$longitud = count($PTR_VISTAS);
			for ($j=1; $j < $longitud; $j++) { 
			$estado = $PTR_VISTAS[$j]->{'estado'};
			$resvalor = $PTR_VISTAS[$j]->{'resvalor'};
			
			if($resvalor == 'Vista Parcial  ')	
				if ($estado) {				  	
				$html.='
				<p>SI</p>
				';
				}
				else{				  	
				$html.='
				<p>NO</p>
				';
				}
			}
			$html.='
		</td>
	</tr>
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%">Orientación:</td>
		<td width="25%">'.$PTR_ANGOR.'</td>
	</tr>
	<tr>
		<td width="50%"><b></b></td>
		<td width="25%">Otros:</td>
		<td width="25%">'.$PTR_ANGOTR.'</td>
	</tr>			
</table >
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td ><b>XII. ELEMENTOS VISUALES</b></td>	    
	</tr>
</table>
<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td><b>De izquierda a Derecha</b></td>
	</tr>
	<tr>
		<td >'.$PTR_ELEMVISID.'
		</td>
	</tr>
	<tr>
		<td><b>A lo alto</b></td>
	</tr>
	<tr>
		<td>'.$PTR_ELEMVISAL.'</td>
	</tr>
	<tr>
		<td><b>A lo abajo</b></td>
	</tr>
	<tr>
		<td>'.$PTR_ELEMVISAB.'</td>
	</tr>			
</table >
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td width="50%"><b>XIII. DESCRIPCIÓN DEL ENTORNO INMEDIATO</b></td>	    
	</tr>
	<tr>
		<td width="25%" >'.$PTR_DESENTEI.'</td>
	</tr>	
</table>
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td ><b>XIV. DESCRIPCIÓN DEL ESPACIO</b></td>	    
	</tr>
	<tr>
		<td >'.$PTR_DESESPDES.'</td>
	</tr>	
</table>
<table  width="100%" cellspacing="-0.5" id="tdborde">
	<tr>
		<td width="25%" id="tdborde"><b>Area de Espacio</b></td>
		<td width="25%" id="tdborde">'.$PTR_MEDIDAREA.'</td>
		<td width="25%" id="tdborde"><b>Ancho</b></td>
		<td width="25%" id="tdborde">'.$PTR_MEDIDANC.'</td>
	</tr>	
	<tr>
		<td width="25%" id="tdborde"><b>Largo</b></td>
		<td width="25%" id="tdborde">'.$PTR_MEDIDLAR.'</td>
		<td width="25%" id="tdborde"><b>Ancho de muro de cerco</b></td>                                                 
		<td width="25%" id="tdborde">'.$PTR_MEDIAMC.'</td>
	</tr>					
</table >
<br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="50%"><b>XVI. INFORMACIÓN ADICIONAL</b></td>
	</tr>					
</table >
<table  width="100%" cellspacing="-0.5" id="tdborde">
	<tr>
		<td width="25%" id="tdborde">
			<b>Información <br> Adicional:</b>
		</td>
		<td width="25%" id="tdborde">
			'.$PTR_INFADICIONAL.'
		</td>
		<td width="25%" id="tdborde">
			<b>Observaciones</b>
		</td>
		<td width="25%" id="tdborde">
		   '.$PTR_INFOBS.'
		</td>
	</tr>					
</table >
 <br>
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="50%"><b>XVII. PATOLOGIAS</b></td>
	</tr>					
</table >
<table  width="100%" cellspacing="-0.5">
	<tr>
		<td width="25%"><b>Daños:</b></td>
		<td width="25%">
		';
			$longitud = count($PTR_PATODAN);
			for ($j=1; $j < $longitud; $j++) { 
			$estado = $PTR_PATODAN[$j]->{'estado'};
				if ($estado) {				  	
				$resvalor = $PTR_PATODAN[$j]->{'resvalor'};

				$html.='
				<p>'.$resvalor.'</p>
				';
				}
			}
			$html.='
		</td>
		<td width="25%"><b>Causas</b></td>
		<td width="25%">
		';
	$longitud = count($PTR_PATOCAU);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_PATOCAU[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_PATOCAU[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='


		</td>
	</tr>
	<tr>
		<td width="25%" ><b>Otros Daños:</b></td>
		<td width="25%" >'.$PTR_PATO_OTROS.'</td>
		<td width="25%" ><b>Otras Causas:</b></td>
		<td width="25%" >'.$PTR_PATOCAU_OTROS.'</td>
	</tr>	
	<tr>
		<td width="25%" ><b>Observaciones:</b></td>
		<td width="25%" >'.$PTR_PATOOBS.'</td>
		<td width="25%" ><b></b></td>
		<td width="25%" ></td>
	</tr>						
</table >
 <br>
<table  width="100%">
	<tr colspan=4>           
		<td width="50%"><b>XVIII. VALORACIÓN</b>
		</td>	    
	</tr>
</table>
 <br>

<table  width="100%"  cellspacing="-0.5">
	<tr>
		<td><b>Indicador Histórico Cultural:</b>
		</td>
	</tr>
	<tr>
		<td>
			';
	$longitud = count($PTR_VALORHIS);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORHIS[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORHIS[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Artístico</b>
		</td>
	</tr>
	<tr>
		<td>
					';
	$longitud = count($PTR_VALORART);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORART[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORART[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Arquitectónico</b>
		</td>
	</tr>
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORARQ);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORARQ[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORARQ[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Tecnológico</b>
		</td>
	</tr>
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORTEC);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORTEC[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORTEC[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>	
	<tr>
		<td><br><b>Indicador de Integridad</b>
		</td>
	</tr>
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORINTG);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORINTG[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORINTG[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Urbano</b>
		</td>
	</tr>	
	<tr>
		<td>

					';
	$longitud = count($PTR_VALORURB);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORURB[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORURB[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Intangibilidad</b>
		</td>
	</tr>
	<tr>
		<td>
			';
	$longitud = count($PTR_VALORINT);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORINT[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORINT[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Simbólico</b>
		</td>
	</tr>
	<tr>
		<td>
';
	$longitud = count($PTR_VALORSIMB);
	for ($j=1; $j < $longitud; $j++) { 
	$estado = $PTR_VALORSIMB[$j]->{'estado'};
		if ($estado) {				  	
		$resvalor = $PTR_VALORSIMB[$j]->{'resvalor'};

		$html.='
		<p>'.$resvalor.'</p>
		';
		}
	}
	$html.='

		</td>
	</tr>
	
</table >

<br>
<table  width="100%">
	<tr colspan=4>           
		<td width="50%"><b>XIX. CRITERIOS DE VALORACIÓN</b>
		</td>	    
	</tr>
	
</table>

<table  width="100%" cellspacing="-0.5">
	<tr>           
		<td><br><b>Indicador Histórico Cultural</b>
		</td>	    
	</tr>
	<tr>
		<td>'.$PTR_CRITERHIS.'
		</td>
	</tr>	
	<tr>
		<td><br><b>Indicador Artístico</b>
		</td>
	</tr>
	<tr>
		<td >'.$PTR_CRITERART.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Arquitectónico</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERARQ.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Tecnológico</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERTEC.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador de Integridad</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERINT.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador Urbano</b>
		</td>
	</tr>
		<tr>
		<td>'.$PTR_CRITERURB.'
		</td>
	</tr>
	<tr>
		<td><br><b>Indicador de Intangibilidad</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERINTA.'
		</td>
	</tr>	
	<tr>
		<td><br><b>Indicador Simbólico</b>
		</td>
	</tr>
	<tr>
		<td>'.$PTR_CRITERSIMB.'
		</td>
	</tr>						
</table >

	';
//----------------------------------------------------------------

	$html2 = ' 
		<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		    	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 50%;
	    	
	    }
	    </style>
	      
	        ';
	$html2 .= '<htmlpageheader name="myheaderFotografico">
            <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>ARCHIVO FOTOGRÁFICO DE ESPACIOS ABIERTOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
 	</htmlpageheader>';
 	$html2 .= '<sethtmlpageheader name="myheaderFotografico" value="on" show-this-page="1" />';

    $html2 .= '<htmlpageheader name="myheaderFotografico">
        <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>ARCHIVO FOTOGRÁFICO DE ESPACIOS ABIERTOS
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
	$html2 .= '<sethtmlpageheader name="myheaderFotografico" value="on" show-this-page="1" />';

	$html2 .= '<div style="font-size:11px;">
		<table style="font-size:11px;" width="100%" >';
			$longitudDoc=sizeof($datosDocumentosF);
			if ($datosDocumentosF == "[{ }]") {
				$longitudDoc = 0;
			}
			
			 if ($longitudDoc == 1) {
		
			$doc_idd = $datosDocumentosF[0]->{'xdoc_idd'};
		    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};							
			$html2.='
			<tr>
			<td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo.'</b><br><br>
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			
			</tr>

			';
		   }
		    if ($longitudDoc == 2) {
								
		    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
			
			$html2.='
			<tr>
		  	<td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo.'</b><br><br>
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo1.'</b><br><br>
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>

			';
		   }
		   if ($longitudDoc == 3) {
								
		    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
			
			$html2.='
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo.'</b><br><br>
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo1.'</b><br><br>
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo2.'</b><br><br>
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			
			
			</tr>';

		   }
		   if ($longitudDoc == 4) {
								
		    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
			$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
		    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
			$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
			
			$html2.='
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo.'</b><br><br>
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo1.'</b><br><br>
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo2.'</b><br><br>
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo3.'</b><br><br>
				<br><img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>
			
			
			</tr>';

		   }
		    if ($longitudDoc == 5) {
								
		    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
			$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
		    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
			$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
			$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
		    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
			$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
			
			$html2.='
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo.'</b><br><br>
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo1.'</b><br><br>
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo2.'</b><br><br>
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo3.'</b><br><br>
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>
			
			
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo4.'</b><br><br>
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><b></b><br><br>
				<img src="" class="img-responsive">
			</td>
			
			
			</tr>
			';

		   }

		   	    if ($longitudDoc == 6) {
								
		    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
			$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
		    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
			$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
			$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
		    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
			$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
			$doc_correlativo5 = $datosDocumentosF[5]->{'xdoc_correlativo'};
		    $doc_titulo5 = $datosDocumentosF[5]->{'xdoc_palabras'};
			$doc_url_logica5 = $datosDocumentosF[5]->{'xdoc_url_logica'};
			
			$html2.='
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo.'</b><br><br>
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<b>'.$doc_titulo1.'</b><br><br>
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo2.'</b><br><br>
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo3.'</b><br><br>
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>
			
			
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo4.'</b><br><br>
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><b>'.$doc_titulo5.'</b><br><br>
				<img src="'.$doc_url_logica5.'" class="img-responsive">
			</td>
			
			
			</tr>
			';

		   }

	$html2.='       						
		</table>				
		</div>
		';

        $mpdf->AddPage();
		$mpdf->WriteHTML($html); 
		$mpdf->AddPage();
		$mpdf->WriteHTML($html2);     
		$mpdf->Output();
}

}