<?php

namespace gamlp\Http\Controllers\reportesPatrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\admin\accionesController;
use gamlp\Http\Controllers\validacionController;
use PDF;

class reporteConjuntoPatrimonial extends Controller
{
    public function getGenerar(Request $request)
    {  
  	   $id_casos = $request->get('datos');
  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
       $datos = json_decode($datos);
       $cas_id = $datos[0]->{'xcas_id'};
       $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'};
	   //dd($g_tipo);
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);
       return $this->pdf($datos, $datosDocumentos);
    }
       public function getGenerarArchFoto(Request $request)
    {  
  	   $id_casos = $request->get('datos');
  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
       $datos = json_decode($datos);
       $cas_id = $datos[0]->{'xcas_id'};
       	$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'}; 
	     //dd($g_tipo);      
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);
       return $this->pdfArchFoto($datos, $datosDocumentos);

  	   
    }
    //---------------------------------------------------------------
    //----------------------------------------------------------------
     public function getGenerarFichaTodo(Request $request)
	{ 	$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
		////obtiene imagenes fichas de CATALOGACIÓN
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		////obtiene imagenes ficha fotografica
		$datosDocumentosF = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentosF = json_decode($datosDocumentosF);
		
		return $this->pdfImprimirTodo($datos, $datosDocumentos,$datosDocumentosF);
		
			
	}
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    public function obtieneDocumentosPatrimonioCulturalCasos($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.' ,"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-939","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	    public function obtieneDocumentosPatrimonioCulturalCasosArchFoto($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.',"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-944","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
       

		public function caracteresEspecialesActualizar($data) {

	        $data = str_replace("&039;", "'", $data);

			$data = str_replace("&040;", "\"",$data);
			 //dd($data);
			$data = str_replace("&041;", "<", $data);
			$data = str_replace("&042;", ">", $data);
			$data = str_replace("&043;", "\\",$data);
			$data = str_replace("&044;", "{", $data);
			$data = str_replace("&045;", "}", $data);
			$data = str_replace("&046;", "[", $data);
			$data = str_replace("&047;", "]", $data);
			$data = str_replace("&048;", "(", $data);
			$data = str_replace("&049;", ")", $data);
			$data = str_replace("&050;", "/", $data);
			$data = str_replace("&051;", ":", $data);
			$data = str_replace("&052", " ", $data);
			 //dd($data);
	        return $data;
		}
	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';
		$url  = 'http://172.19.161.3/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/json",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		 }
		 $tokenArray=explode('"', $result);
		  return $tokenArray[3];
	}

	public function obtieneDatosPatrimonioCulturalCasos($id_casos)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_casos":'.$id_casos.'}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-935","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}

	public function pdf($datos)
  	{
	  	 //dd($datos);
	  	  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $longitudEA = sizeof($datos);
		  $cas_id = $datos[0]->{'xcas_id'};
		  $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		  $cas_act_id = $datos[0]->{'xcas_act_id'};
		  $cas_usr_actual_id = $datos[0]->{'xcas_usr_actual_id'};
		  
		  $cas_datos = $datos[0]->{'xcas_datos'};
		  $cas_datos = json_decode($cas_datos);
		  $g_tipo = $cas_datos->{'g_tipo'};
		  $PTR_NOM = $cas_datos->{'PTR_NOM'};
		  $PTR_UBIC = $cas_datos->{'PTR_UBIC'};
		  $PTR_TIP_PROT = $cas_datos->{'PTR_TIP_PROT'};
		  $PTR_NRO_PROT = $cas_datos->{'PTR_NRO_PROT'};
		  $PTR_NUM_INM = $cas_datos->{'PTR_NUM_INM'};
		  $PTR_NUM_PAT = $cas_datos->{'PTR_NUM_PAT'};
		  $PTR_IND_URB = $cas_datos->{'PTR_IND_URB'};
		  $PTR_IND_ART = $cas_datos->{'PTR_IND_ART'};
		  $PTR_IND_TIP = $cas_datos->{'PTR_IND_TIP'};
		  $PTR_IND_TEC = $cas_datos->{'PTR_IND_TEC'};
		  $PTR_IND_INT = $cas_datos->{'PTR_IND_INT'};
		  $PTR_IND_HIST = $cas_datos->{'PTR_IND_HIST'};
		  $PTR_IND_SIMB = $cas_datos->{'PTR_IND_SIMB'};
	      $PTR_IND_URB = $this->caracteresEspecialesActualizar( $PTR_IND_URB);
	      $PTR_IND_ART = $this->caracteresEspecialesActualizar( $PTR_IND_ART);
	      $PTR_IND_TIP = $this->caracteresEspecialesActualizar( $PTR_IND_TIP);
	      $PTR_IND_TEC = $this->caracteresEspecialesActualizar( $PTR_IND_TEC);
	      $PTR_IND_INT = $this->caracteresEspecialesActualizar( $PTR_IND_INT);
	      $PTR_IND_HIST = $this->caracteresEspecialesActualizar( $PTR_IND_HIST);
	      $PTR_IND_SIMB = $this->caracteresEspecialesActualizar( $PTR_IND_SIMB);
		  $PTR_NOM_VIA = $cas_datos->{'PTR_NOM_VIA'};
		  $PTR_MAT_VIA = $cas_datos->{'PTR_MAT_VIA'};
		  $PTR_CLASS = $cas_datos->{'PTR_CLASS'};
		  $PTR_ENT_CALL = $cas_datos->{'PTR_ENT_CALL'};

		  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  $cas_estado_paso = $datos[0]->{'xcas_estado_paso'};
		  $cas_fecha_inicio = $datos[0]->{'xcas_fecha_inicio'};
		  $cas_fecha_limite = $datos[0]->{'xcas_fecha_limite'};
		  $cas_nodo_id = $datos[0]->{'xcas_nodo_id'};
		  $cas_registrado = $datos[0]->{'xcas_registrado'};
		  $cas_modificado = $datos[0]->{'xcas_modificado'};
		  $cas_usr_id = $datos[0]->{'xcas_usr_id'};
		  $cas_estado = $datos[0]->{'xcas_estado'};
		  $cas_ws_id = $datos[0]->{'xcas_ws_id'};
		  $cas_asunto = $datos[0]->{'xcas_asunto'};
		  $cas_tipo_hr = $datos[0]->{'xcas_tipo_hr'};
		  $cas_id_padre = $datos[0]->{'xcas_id_padre'};
		  $campo_f = $datos[0]->{'xcampo_f'};
		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter', 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		  
		 $mpdf->SetHTMLHeader('
           <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>FICHA DE CATALOGACIÓN DE MUEBLES ARQUEOLÓGICOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');
	
		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');

	$html = ' 
	<style>

	#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
	table{
		font-size:12px;
	}
	#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }
	</style>
			';

	$html.= '
				
				<table width="100%">
				    <tr>
			  		  <td><b>DATOS GENERALES DEL CONJUNTO</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Nominación:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NOM.'</td>
					</tr>
					<tr>
					<td width="40%" id="tdborde"><b>Ubicación:</b></td>
					<td width="60%" id="tdborde">'.$PTR_UBIC.'</td>
					</tr>					
				</table>
                <br>
                <table width="100%">
				    <tr>
			  		  <td><b>PROTECCIÓN LEGAL</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Protección Legal:</b></td>
					<td width="60%" id="tdborde">'.$PTR_TIP_PROT.'</td>
					</tr>
					<tr> 
					<td width="40%" id="tdborde"><b>Numero de Protección Legal:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NRO_PROT.'</td>
					</tr>					
				</table>
				     <br>
                <table width="100%">
				    <tr>
			  		  <td><b>INFORMACIÓN CUANTITATIVA DEL TRAMO</b></td>
				    </tr>
				</table>
		            <br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Número Total de Inmuebles Tramo:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NUM_INM.'</td>
					</tr>
					<tr> 
					<td width="40%" id="tdborde"><b>Número Total de Inmuebles Patrimoniales:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NUM_PAT.'</td>
					</tr>					
				</table>
				<br>
				<table width="100%">
				    <tr>
			  		  <td><b>INFORMACIÓN CUALITATIVA DEL CONJUNTO</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Indicador Urbano:</b></td>
					<td width="60%" id="tdborde">'.$PTR_IND_URB.'</td>
					</tr>
					<tr> 
					<td width="40%" id="tdborde"><b>Indicador Artístico</b></td>
					<td width="60%" id="tdborde">'.$PTR_IND_ART.'</td>
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Tipológico:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_TIP.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Tecnológico:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_TEC.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador de Integridad:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_INT.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Histórico Cultural:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_HIST.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Simbólico:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_SIMB.'</td>					  
					</tr>					
				</table>
				<br>
				<table width="100%">
				    <tr>
			  		  <td><b>IDENTIFICACIÓN DE TRAMOS</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr>
					  <td width="40%" id="tdborde"><b>Nombre de la Vía:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_NOM_VIA.'</td>					
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Material de la Vía:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_MAT_VIA.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Clasificación:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_CLASS.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Entre Calles:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_ENT_CALL.'</td>
					</tr>				
				</table>

				';

			$mpdf->AddPage();
			$mpdf->WriteHTML($html);   
			$mpdf->Output();				  
	}

	public function pdfArchFoto($datos, $datosDocumentos)
	{
		  //dd($datosDocumentos);
		  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  //-----------Documentos----------------------
	      $longitudDoc = 0;
		

		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter', 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		   
		   $mpdf->SetHTMLHeader('
             <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>ARCHIVO FOTOGŔAFICO DE CATALOGACIÓN DE MUEBLES ARQUEOLÓGICOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');

		  $mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');
		  
	$html = ' 
  
		<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 50%;
	    	
	    }
	    </style>
	        ';

 $html.= '
					<div style="font-size:11px;">
						<table style="font-size:11px;" width="100%" >';
							$longitudDoc=sizeof($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
							 if ($longitudDoc == 1) {
						
							$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};							
							$html.='
							<tr><td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							
							</tr>

							';
						   }
						    if ($longitudDoc == 2) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						  	<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>

							';
						   }
						   if ($longitudDoc == 3) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>';
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						   if ($longitudDoc == 4) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<br><img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						    if ($longitudDoc == 5) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
							$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo4.'</b><br><br>
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b></b><br><br>
								<img src="" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

						   	    if ($longitudDoc == 6) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
							$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
							$doc_correlativo5 = $datosDocumentos[5]->{'xdoc_correlativo'};
						    $doc_titulo5 = $datosDocumentos[5]->{'xdoc_palabras'};
							$doc_url_logica5 = $datosDocumentos[5]->{'xdoc_url_logica'};
							
							$html.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo4.'</b><br><br>
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo5.'</b><br><br>
								<img src="'.$doc_url_logica5.'" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

							$html.='       						
						</table>				
					</div>
			    ';

		$mpdf->AddPage();
			$mpdf->WriteHTML($html);   
			$mpdf->Output();
	}

//-------------------------------
//--------------------------------
public function pdfImprimirTodo($datos , $datosDocumentos ,$datosDocumentosF)
{
	 //dd($datos);
	  	  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $longitudEA = sizeof($datos);
		  $cas_id = $datos[0]->{'xcas_id'};
		  $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		  $cas_act_id = $datos[0]->{'xcas_act_id'};
		  $cas_usr_actual_id = $datos[0]->{'xcas_usr_actual_id'};
		  
		  $cas_datos = $datos[0]->{'xcas_datos'};
		  $cas_datos = json_decode($cas_datos);
		  $g_tipo = $cas_datos->{'g_tipo'};
		  $PTR_NOM = $cas_datos->{'PTR_NOM'};
		  $PTR_UBIC = $cas_datos->{'PTR_UBIC'};
		  $PTR_TIP_PROT = $cas_datos->{'PTR_TIP_PROT'};
		  $PTR_NRO_PROT = $cas_datos->{'PTR_NRO_PROT'};
		  $PTR_NUM_INM = $cas_datos->{'PTR_NUM_INM'};
		  $PTR_NUM_PAT = $cas_datos->{'PTR_NUM_PAT'};
		  $PTR_IND_URB = $cas_datos->{'PTR_IND_URB'};
		  $PTR_IND_ART = $cas_datos->{'PTR_IND_ART'};
		  $PTR_IND_TIP = $cas_datos->{'PTR_IND_TIP'};
		  $PTR_IND_TEC = $cas_datos->{'PTR_IND_TEC'};
		  $PTR_IND_INT = $cas_datos->{'PTR_IND_INT'};
		  $PTR_IND_HIST = $cas_datos->{'PTR_IND_HIST'};
		  $PTR_IND_SIMB = $cas_datos->{'PTR_IND_SIMB'};
	      $PTR_IND_URB = $this->caracteresEspecialesActualizar( $PTR_IND_URB);
	      $PTR_IND_ART = $this->caracteresEspecialesActualizar( $PTR_IND_ART);
	      $PTR_IND_TIP = $this->caracteresEspecialesActualizar( $PTR_IND_TIP);
	      $PTR_IND_TEC = $this->caracteresEspecialesActualizar( $PTR_IND_TEC);
	      $PTR_IND_INT = $this->caracteresEspecialesActualizar( $PTR_IND_INT);
	      $PTR_IND_HIST = $this->caracteresEspecialesActualizar( $PTR_IND_HIST);
	      $PTR_IND_SIMB = $this->caracteresEspecialesActualizar( $PTR_IND_SIMB);
		  $PTR_NOM_VIA = $cas_datos->{'PTR_NOM_VIA'};
		  $PTR_MAT_VIA = $cas_datos->{'PTR_MAT_VIA'};
		  $PTR_CLASS = $cas_datos->{'PTR_CLASS'};
		  $PTR_ENT_CALL = $cas_datos->{'PTR_ENT_CALL'};

		  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  $cas_estado_paso = $datos[0]->{'xcas_estado_paso'};
		  $cas_fecha_inicio = $datos[0]->{'xcas_fecha_inicio'};
		  $cas_fecha_limite = $datos[0]->{'xcas_fecha_limite'};
		  $cas_nodo_id = $datos[0]->{'xcas_nodo_id'};
		  $cas_registrado = $datos[0]->{'xcas_registrado'};
		  $cas_modificado = $datos[0]->{'xcas_modificado'};
		  $cas_usr_id = $datos[0]->{'xcas_usr_id'};
		  $cas_estado = $datos[0]->{'xcas_estado'};
		  $cas_ws_id = $datos[0]->{'xcas_ws_id'};
		  $cas_asunto = $datos[0]->{'xcas_asunto'};
		  $cas_tipo_hr = $datos[0]->{'xcas_tipo_hr'};
		  $cas_id_padre = $datos[0]->{'xcas_id_padre'};
		  $campo_f = $datos[0]->{'xcampo_f'};
		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter', 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		  
		
		 $html = '<htmlpageheader name="myheaderFicha">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE CATALOGACIÓN DE MUEBLES ARQUEOLÓGICOS 
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
		$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';
	
		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');

	$html .= ' 
	<style>

	#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
	table{
		font-size:12px;
	}
	#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }
	</style>
			';

	$html.= '<br><br><br><br><br><br>
				
				<table width="100%">
				    <tr>
			  		  <td><b>DATOS GENERALES DEL CONJUNTO</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Nominación:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NOM.'</td>
					</tr>
					<tr>
					<td width="40%" id="tdborde"><b>Ubicación:</b></td>
					<td width="60%" id="tdborde">'.$PTR_UBIC.'</td>
					</tr>					
				</table>
                <br>
                <table width="100%">
				    <tr>
			  		  <td><b>PROTECCIÓN LEGAL</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Protección Legal:</b></td>
					<td width="60%" id="tdborde">'.$PTR_TIP_PROT.'</td>
					</tr>
					<tr> 
					<td width="40%" id="tdborde"><b>Numero de Protección Legal:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NRO_PROT.'</td>
					</tr>					
				</table>
				     <br>
                <table width="100%">
				    <tr>
			  		  <td><b>INFORMACIÓN CUANTITATIVA DEL TRAMO</b></td>
				    </tr>
				</table>
		            <br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Número Total de Inmuebles Tramo:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NUM_INM.'</td>
					</tr>
					<tr> 
					<td width="40%" id="tdborde"><b>Número Total de Inmuebles Patrimoniales:</b></td>
					<td width="60%" id="tdborde">'.$PTR_NUM_PAT.'</td>
					</tr>					
				</table>
				<br>
				<table width="100%">
				    <tr>
			  		  <td><b>INFORMACIÓN CUALITATIVA DEL CONJUNTO</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr> 
					<td width="40%" id="tdborde"><b>Indicador Urbano:</b></td>
					<td width="60%" id="tdborde">'.$PTR_IND_URB.'</td>
					</tr>
					<tr> 
					<td width="40%" id="tdborde"><b>Indicador Artístico</b></td>
					<td width="60%" id="tdborde">'.$PTR_IND_ART.'</td>
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Tipológico:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_TIP.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Tecnológico:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_TEC.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador de Integridad:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_INT.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Histórico Cultural:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_HIST.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Indicador Simbólico:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_IND_SIMB.'</td>					  
					</tr>					
				</table>
				<br>
				<table width="100%">
				    <tr>
			  		  <td><b>IDENTIFICACIÓN DE TRAMOS</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
					<tr>
					  <td width="40%" id="tdborde"><b>Nombre de la Vía:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_NOM_VIA.'</td>					
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Material de la Vía:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_MAT_VIA.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Clasificación:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_CLASS.'</td>					  
					</tr>
					<tr>
					  <td width="40%" id="tdborde"><b>Entre Calles:</b></td>
				  	  <td width="60%" id="tdborde">'.$PTR_ENT_CALL.'</td>
					</tr>				
				</table>

				';
 //----------------------------------------
$html2 = ' 
  
		<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 50%;
	    	
	    }
	    </style>
	        ';

$html2 .= '<htmlpageheader name="myheaderFotografico">
            <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>ARCHIVO FOTOGRÁFICO DE MUEBLES ARQUEOLÓGICOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
 		</htmlpageheader>';
 $html2 .= '<sethtmlpageheader name="myheaderFotografico" value="on" show-this-page="1" />';

 $html2 .= '
					<div style="font-size:11px;">
						<table style="font-size:11px;" width="100%" >';
							$longitudDoc=sizeof($datosDocumentosF);
							if ($datosDocumentosF == "[{ }]") {
								$longitudDoc = 0;
							}
							 if ($longitudDoc == 1) {
						
							$doc_idd = $datosDocumentosF[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};							
							$html2.='
							<tr><td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							
							</tr>

							';
						   }
						    if ($longitudDoc == 2) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
						  	<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>

							';
						   }
						   if ($longitudDoc == 3) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>';
							$html2.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						   if ($longitudDoc == 4) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<br><img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						    if ($longitudDoc == 5) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
							$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo4.'</b><br><br>
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b></b><br><br>
								<img src="" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

						   	    if ($longitudDoc == 6) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosFF[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
							$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
							$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
							$doc_correlativo5 = $datosDocumentosF[5]->{'xdoc_correlativo'};
						    $doc_titulo5 = $datosDocumentosF[5]->{'xdoc_palabras'};
							$doc_url_logica5 = $datosDocumentosF[5]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo.'</b><br><br>
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<b>'.$doc_titulo1.'</b><br><br>
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo2.'</b><br><br>
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo3.'</b><br><br>
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo4.'</b><br><br>
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<br><b>'.$doc_titulo5.'</b><br><br>
								<img src="'.$doc_url_logica5.'" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

							$html2.='       						
						</table>				
					</div>
			    ';

			$mpdf->AddPage();
			$mpdf->WriteHTML($html); 
			$mpdf->AddPage();
			$mpdf->WriteHTML($html2);     
			$mpdf->Output();
}
	
	


}
