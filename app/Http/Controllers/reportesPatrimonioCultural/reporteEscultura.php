<?php

namespace gamlp\Http\Controllers\reportesPatrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Http\Controllers\admin\accionesController;
use gamlp\Http\Controllers\validacionController;
use PDF;

class reporteEscultura extends Controller
{
    public function getGenerar(Request $request)
    { 
   // dd($request); 
  	   $id_casos = $request->get('datos');
  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
       $datos = json_decode($datos); 
       //dd($datos);                                                           
       $cas_id = $datos[0]->{'xcas_id'};
       $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       //dd($cas_id);
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'};

	   //dd($datos);
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);
       
       return $this->pdf($datos, $datosDocumentos);
    }
      public function getGenerarArchFoto(Request $request)
    {  
  	   $id_casos = $request->get('datos');
  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
       $datos = json_decode($datos);
       $cas_id = $datos[0]->{'xcas_id'};
       $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'}; 
	     //dd($g_tipo);      
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);
       return $this->pdfArchFoto($datos, $datosDocumentos);

  	   
    }

    public function getGenerarFichaTodo(Request $request)
	{ 	$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
		////obtiene imagenes fichas de CATALOGACIÓN
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		////obtiene imagenes ficha fotografica
		$datosDocumentosF = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentosF = json_decode($datosDocumentosF);
		
		return $this->pdfImprimirTodo($datos, $datosDocumentos,$datosDocumentosF);
				
	}
    public function obtieneDocumentosPatrimonioCulturalCasos($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.' ,"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-939","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	    public function obtieneDocumentosPatrimonioCulturalCasosArchFoto($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.',"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-944","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public function caracteresEspecialesActualizar($data) {

		        $data = str_replace("&039;", "'", $data);

				$data = str_replace("&040;", "\"",$data);
				 //dd($data);
				$data = str_replace("&041;", "<", $data);
				$data = str_replace("&042;", ">", $data);
				$data = str_replace("&043;", "\\",$data);
				$data = str_replace("&044;", "{", $data);
				$data = str_replace("&045;", "}", $data);
				$data = str_replace("&046;", "[", $data);
				$data = str_replace("&047;", "]", $data);
				$data = str_replace("&048;", "(", $data);
				$data = str_replace("&049;", ")", $data);
				$data = str_replace("&050;", "/", $data);
				$data = str_replace("&051;", ":", $data);
				$data = str_replace("&053;", "#", $data);
				$data = str_replace("&052", " ", $data);
				
				 //dd($data);

                return $data;
		}
	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';
		$url  = 'http://172.19.161.3/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/json",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		 }
		 $tokenArray=explode('"', $result);
		  return $tokenArray[3];
	}

	public function obtieneDatosPatrimonioCulturalCasos($id_casos)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_casos":'.$id_casos.'}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-935","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}

		public function pdf($datos , $datosDocumentos)
  	{
	  	  //dd($datosDocumentos);
	  	  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $cas_id = $datos[0]->{'xcas_id'};
		  $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		  $cas_act_id = $datos[0]->{'xcas_act_id'};
		  $cas_usr_actual_id = $datos[0]->{'xcas_usr_actual_id'};
		  
		  $cas_datos = $datos[0]->{'xcas_datos'};
		  $cas_datos = json_decode($cas_datos);
		  $g_tipo = $cas_datos->{'g_tipo'};
		  $PTR_NOMBRE = $cas_datos->{'PTR_NOMBRE'};
		  $PTR_EPOCA = $cas_datos->{'PTR_EPOCA'};
		  $PTR_PAIS = $cas_datos->{'PTR_PAIS'};
		  $PTR_DEP = $cas_datos->{'PTR_DEP'};
		  $PTR_MUNI = $cas_datos->{'PTR_MUNI'};
		  $PTR_PROV = $cas_datos->{'PTR_PROV'};
		  $PTR_MACRO = $cas_datos->{'PTR_MACRO'};
		  $PTR_ZONA = $cas_datos->{'PTR_ZONA'};
		  $PTR_COMU = $cas_datos->{'PTR_COMU'};
		  $PTR_DIR = $cas_datos->{'PTR_DIR'};
		  $PTR_AU = $cas_datos->{'PTR_AU'};
		  $PTR_ESP = $cas_datos->{'PTR_ESP'};
		  $PTR_TEC_MAT = $cas_datos->{'PTR_TEC_MAT'};
		  $PTR_CONJ_ESC = $cas_datos->{'PTR_CONJ_ESC'};
		  $PTR_ALTO = $cas_datos->{'PTR_ALTO'};
		  $PTR_ANCHO = $cas_datos->{'PTR_ANCHO'};
		  $PTR_PESO = $cas_datos->{'PTR_PESO'};
		  $PTR_DESC_OBRA = $cas_datos->{'PTR_DESC_OBRA'};
		  $PTR_ORG_PIE = $cas_datos->{'PTR_ORG_PIE'};
		  $PTR_PROP = $cas_datos->{'PTR_PROP'};
		  $PTR_RESP = $cas_datos->{'PTR_RESP'};
		  $PTR_TIPO = $cas_datos->{'PTR_TIPO'};
		  $PTR_LEY_OR = $cas_datos->{'PTR_LEY_OR'};
		  $PTR_DES_PER = $cas_datos->{'PTR_DES_PER'};
		  $PTR_REF_HIST = $cas_datos->{'PTR_REF_HIST'};
		  $PTR_REF_CON = $cas_datos->{'PTR_EST_CON'};
		  $PTR_OBS = $cas_datos->{'PTR_OBS'};
		  $PTR_FUENT_BIBLIO = $cas_datos->{'PTR_FUENT_BIBLIO'};	
		  $PTR_AU = $this->caracteresEspecialesActualizar( $PTR_AU);
	      $PTR_DESC_OBRA = $this->caracteresEspecialesActualizar( $PTR_DESC_OBRA);
	      $PTR_ORG_PIE = $this->caracteresEspecialesActualizar( $PTR_ORG_PIE);
	      $PTR_DES_PER = $this->caracteresEspecialesActualizar( $PTR_DES_PER);
	      $PTR_REF_HIST = $this->caracteresEspecialesActualizar( $PTR_REF_HIST);
	      $PTR_OBS = $this->caracteresEspecialesActualizar( $PTR_OBS);
	      $PTR_FUENT_BIBLIO = $this->caracteresEspecialesActualizar( $PTR_FUENT_BIBLIO);

		  
		  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  $cas_estado_paso = $datos[0]->{'xcas_estado_paso'};
		  $cas_fecha_inicio = $datos[0]->{'xcas_fecha_inicio'};
		  $cas_fecha_limite = $datos[0]->{'xcas_fecha_limite'};
		  $cas_nodo_id = $datos[0]->{'xcas_nodo_id'};
		  $cas_registrado = $datos[0]->{'xcas_registrado'};
		  $cas_modificado = $datos[0]->{'xcas_modificado'};
		  $cas_usr_id = $datos[0]->{'xcas_usr_id'};
		  $cas_estado = $datos[0]->{'xcas_estado'};
		  $cas_ws_id = $datos[0]->{'xcas_ws_id'};
		  $cas_asunto = $datos[0]->{'xcas_asunto'};
		  $cas_tipo_hr = $datos[0]->{'xcas_tipo_hr'};
		  $cas_id_padre = $datos[0]->{'xcas_id_padre'};
		  $campo_f = $datos[0]->{'xcampo_f'};
		  //--------------------dOCU
		  

		 
		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		
		$mpdf->SetHTMLHeader('
           <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>FICHA DE REGISTRO DE ESCULTURAS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');
		
		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');
	
	$html = '
	<style>

		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    }

	    #tdbordeInterlineado{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    line-height: 170%;
	    }

	#interlineado{
	    line-height: 170%;
	 }   
	table{
		font-size:12px;

	}

	#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:10;
	    text-align : justify;
	    font-size:12px;
	  

	    }
	    #tdbordeArchFotEspacio{
		   
		
	    border-collapse: border-bottom-width;
	    padding:10;
	    text-align : justify;
	    font-size:12px;
	  

	    }

	    #tdbordeArchFotDes{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:15;
	    text-align : justify;
	    font-size:18px;
	  

	    }
	    .img-responsive{
	    	height: 45%;
	    	
	    }
	</style>
	 ';

	$html.= '
			<div style="font-size:11px;">
				<table width="100%" >
				    <tr>
			  		  <td width="35%" ><b>DENOMINACIÓN:</b></td>
			          <td width="65%" >'.$PTR_NOMBRE.'</td>
			          <td></td>
			          <td></td>
				    </tr>
				    <tr>
			  		  <td width="35%"><b>ÉPOCA O AÑO DE ELABORACIÓN:</b></td>
			          <td width="65%">'.$PTR_EPOCA.'</td>		   
				    </tr>
				</table>
				<br>
				<table width="100%" cellspacing="2">
				    <tr>
			  		  <td><b>FOTOGRAFÍAS</b></td>
				    </tr>
				</table>
				<br>
				';

			 $html.= '
					<div style="font-size:12px;">
						<table style="font-size:16px;" width="100%" cellpadding="2">';
							$longitudDoc=sizeof($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
							 if ($longitudDoc == 1) {
						
							$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
							</tr>
							<tr>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>													
							</tr>
							<tr>										
							<td width="50%" align="center" id="tdbordeArchFotDes">
							<b>Descripción:</b>
							'.$doc_datos.'
							</td>
							</tr>

							';
						   }
						    if ($longitudDoc == 2) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
							$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive" >
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive" style="position: relative;">
							</td>
							
							</tr>
							<tr>										
							<td width="50%" id="tdbordeArchFotDes" >
							<b>Descripción:</b>
							'.$doc_datos.'
							</td>
							<td id="tdbordeArchFotDes" width="50%">
							<b>Descripción:</b>
							'.$doc_datos1.'
							</td>
							</tr>

							';
						   }
						   if ($longitudDoc == 3) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
							$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
							$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes" >
									<b>Descripción:</b>
									'.$doc_datos.'
								</td>
								<td id="tdbordeArchFotDes" width="50%">
									<b>Descripción:</b>
									'.$doc_datos1.'
								</td>
							</tr>

							';
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>
							</tr>
							<tr>
						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>										
							</tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b>
									'.$doc_datos2.'
								</td>								
							</tr>';

						   }
						   if ($longitudDoc == 4) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'doc_tipo_documentacion'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
							$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
							$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
							$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" id="tdbordeArchFot" VALIGN="TOP" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" VALIGN="TOP" align ="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b>
									'.$doc_datos.'
								</td>
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b>
									'.$doc_datos1.'
								</td>
							</tr>
							<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo3.'</b><br>
							
							</td>
							</tr>

							<tr>
						    <td width="50%" id="tdbordeArchFot" VALIGN="TOP" align="center">
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" VALIGN="TOP" align="center">
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>				
							</tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b>
									'.$doc_datos2.'
								</td>
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b>
									'.$doc_datos3.'
								</td>
							</tr>
							';

						   }
							$html.='       						
						</table>				
					</div>
			    ';

			    $html.='
				<br>
				<table width="100%" id="tdborde">
				    <tr>
			  		  <td><b>I. LOCALIZACIÓN</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
				    <tr> 
			        <td width="30%" id="tdborde"><b>PAÍS:</b></td>
			        <td width="70%" id="tdborde">'.$PTR_PAIS.'</td>
			        </tr>
			        <tr>
			        <td width="30%" id="tdborde"><b>DEPARTAMENTO:</b></td>
			         <td width="70%" id="tdborde">'.$PTR_DEP.'</td>
			        </tr>
			        <tr>
			            <td width="30%" id="tdborde"><b>MUNICIPIO:</b></td>
			              <td width="70%" id="tdborde">'.$PTR_MUNI.'</td>
			          </tr>
			          <tr>
			            <td width="30%" id="tdborde"><b>PROVINCIA:</b></td>
			              <td width="70%" id="tdborde">'.$PTR_PROV.'</td>
			          </tr>
			          <tr>
			            <td width="30%" id="tdborde"><b>MACRODISTRITO:</b></td>
			              <td width="70%" id="tdborde">'.$PTR_MACRO.'</td>
			          </tr>
			          <tr>
			            <td width="30%" id="tdborde"><b>ZONA:</b></td>
			              <td width="70%" id="tdborde">'.$PTR_ZONA.'</td>
			          </tr>
			          <tr>
			            <td width="30%" id="tdborde"><b>COMUNIDAD:</b></td>
			              <td width="70%" id="tdborde">'.$PTR_COMU.'</td>
			          </tr>				
				</table>
                <br>
				<table  width="100%">
				    <tr>
			  		  <td><b>II. DATOS DEL AUTOR</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde">
				    <tr> 
			            <td width="30%" id="tdborde"><b>DATOS DEL AUTOR:</b></td>
			            <td width="70%" id="tdbordeInterlineado">'.$PTR_AU.'</td>
			          </tr>
				</table>
				<br>
                <table width="100%">
				    <tr>
			  		 <tr><td><b>III. DATOS DE LA OBRA</b></td>
				    </tr>
				</table>
				<br>
               <table width="100%" id="tdborde">
				<tr>
		           <td width="30%" id="tdborde"><b>ESPECIALIDAD:</b></td>
		           <td width="70%" id="tdborde">'.$PTR_ESP.'</td>
		           </tr>
		           <tr>
		           <td width="30%" id="tdborde"><b>TÉCNICA Y MATERIAL:</b></td>
		           <td width="70%" id="tdborde">'.$PTR_TEC_MAT.'</td>
 	               </tr>
 	               <tr>
 	               <td width="30%" id="tdborde"><b>PIEZA ESCULTÓRICA:</b></td>
 	               <td width="70%" id="tdborde">
 	               ';
		 	      $longitud = count($PTR_CONJ_ESC);
				  for ($j=1; $j < $longitud; $j++) { 
				  	$estado = $PTR_CONJ_ESC[$j]->{'estado'};
					   if ($estado) {				  	
					     $resvalor = $PTR_CONJ_ESC[$j]->{'resvalor'};
					    
					      $html.='
					      <p>'.$resvalor.'</p>
					      ';
					  	}
				  }
		     $html.='</td></tr>
		     </table>
		     <br>
                <table  width="100%">
				    <tr>
			  		 <tr><td><b>IV. DIMENSIONES DE LA OBRA (SIN PEDESTAL)</b></td>
				    </tr>
				</table>
				<br>
                <table  width="100%" id="tdborde">
				<tr>
		           <td width="30%" id="tdborde"><b>ALTO:</b></td>
		           <td width="70%" id="tdborde">'.$PTR_ALTO.'</td>
		        </tr>
		        <tr>
		           <td width="30%" id="tdborde"><b>ANCHO:</b></td>
		           <td width="70%" id="tdborde">'.$PTR_ANCHO.'</td>
                </tr>
                <tr>
		           <td width="30%" id="tdborde"><b>PESO:</b></td>
		           <td width="70%" id="tdborde">'.$PTR_PESO.'</td>
		        </tr>
		        <tr>
		           <td width="30%" id="tdborde"><b>DESCRIPCIÓN DE LA OBRA:</b></td>
		           <td width="70%" id="tdbordeInterlineado">'.$PTR_DESC_OBRA.'</td>
                </tr>
                <tr>
		           <td width="30%" id="tdborde"><b>ORIGEN DE LA PIEZA:</b></td>
		           <td width="70%" id="tdbordeInterlineado">'.$PTR_ORG_PIE.'</td>
                </tr>
		     </table>
		      <br>
                <table  width="100%">
				    <tr>
			  		 <tr><td><b>V. REGIMEN DE PROPIEDAD</b></td>
				    </tr>
				</table>
				<br>
				 <table width="100%" id="tdborde">
				    <tr>
		            <td width="30%" id="tdborde"><b>NOMBRE DEL PROPIETARIO:</b></td>
		              <td width="70" id="tdborde">'.$PTR_PROP.'</td>
			          </tr>
			          <tr>
		            <td width="30%" id="tdborde"><b>RESPONSABLES:</b></td>
		              <td width="70%" id="tdborde">'.$PTR_RESP.'</td>
		          </tr>
				</table>
				 <br>
                <table  width="100%">
				    <tr>
			  		 <tr><td><b>VI. PROTECCIÓN LEGAL</b></td>
				    </tr>
				</table>
				<br>
				<table  width="100%" id="tdborde">
				     <tr>
			            <td width="30%" id="tdborde"><b>TIPO:</b></td>
			              <td width="70%" id="tdborde">'.$PTR_TIPO.'</td>
			            </tr>
			            <tr>
			            <td width="30%" id="tdborde"><b>NRO. LEY U ORDENANZA MUNICIPAL:</b></td>
			              <td width="70%" id="tdborde">'.$PTR_LEY_OR.'</td>
			          </tr>
				</table>
				<br>
				<table  width="100%">
				    <tr>
			  		 <tr><td><b>VII. REFERENCIAS DE LA OBRA</b></td>
				    </tr>
				</table>
				<br>
				<table  width="100%" id="tdborde" id ="interlineado" >
				     <tr>
		            <td width="30%" id="tdborde"><b>DESCRIPCIÓN DEL PERSONAJE O MOTIVO:</b></td>
		              <td width="70%" id="tdborde">'.$PTR_DES_PER.'</td>
		          </tr>
		          <tr>
		            <td width="30%" id="tdborde"><b>REFERENCIAS HISTÓRICAS DE LA OBRA:</b></td>
		              <td width"70%" id="tdborde">'.$PTR_REF_HIST.'</td>
		          </tr>
				</table>
				<br>
				<table  width="100%">
				    <tr>
			  		 <tr><td><b>VIII. ESTADO DE CONSERVACIÓN</b></td>
				    </tr>
				</table>
				<br>
				<table width="100%" id="tdborde" id ="interlineado">
			      <tr>
		            <td width="30%" id="tdborde"><b>ESTADO DE CONSERVACIÓN:</b></td>
		              <td width="70%" id="tdborde">'.$PTR_REF_CON.'</td>
		          </tr>
		          <tr>
		            <td width="30%" id="tdborde"><b>OBSERVACIONES:</b></td>
		              <td width="70%" id="tdborde">'.$PTR_OBS.'</td>
		          </tr>
		          <tr>
		            <td width="30%" id="tdborde"><b>FUENTES:</b></td>
		              <td width="70%" id="tdborde">'.$PTR_FUENT_BIBLIO.'</td>
		          </tr>
				</table>
			</div>
	';

	
			$mpdf->AddPage();
			$mpdf->WriteHTML($html);   
			$mpdf->Output();				  
	}

	public function pdfArchFoto($datos, $datosDocumentos)
	{
		  //dd($datosDocumentos);
		  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  //-----------Documentos----------------------
	      $longitudDoc = 0;
		

		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter' , 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		  $mpdf->SetHTMLHeader('
             <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>ARCHIVO FOTOGRÁFICO DE ESCULTURAS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');
		 $mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');
	$html = ' 

        
		<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		    	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }

	     #tdbordeArchFotEspacio{
		   
		
	    border-collapse: border-bottom-width;
	    padding:10;
	    text-align : justify;
	    font-size:12px;
	  

	    }

	    #tdbordeArchFotDes{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:15;
	    text-align : justify;
	    font-size:14px;
	  

	    }
	    </style>
	      ';

 $html.= '<div style="font-size:11px;">
			<table style="font-size:11px;" width="100%" >';
				$longitudDoc=sizeof($datosDocumentos);
				if ($datosDocumentos == "[{ }]") {
					$longitudDoc = 0;
				}
				 if ($longitudDoc == 1) {
			
				$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};								
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</tr>
				<tr>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				</tr>
				<tr>										
					<td width="50%" align="center" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos.'
					</td>
				</tr>

				';
			   }
			    if ($longitudDoc == 2) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
				$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			  		<tr><td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%"  align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes" >
						<b>Descripción:</b><br>
						'.$doc_datos.'
					</td>
					<td id="tdbordeArchFotDes" width="50%">
						<b>Descripción:</b><br>
						'.$doc_datos1.'
					</td>
				</tr>

				';
			   }
			   if ($longitudDoc == 3) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
				$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
				$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes" >
						<b>Descripción:</b><br>
						'.$doc_datos.'
					</td>
					<td id="tdbordeArchFotDes" width="50%">
						<b>Descripción:</b><br>
						'.$doc_datos1.'
					</td>
				</tr>
				';
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>								
				</tr>
				<tr>
			    <td width="50%" align="center"  id="tdbordeArchFot">
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>		
				</tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos2.'
					</td>								
				</tr>
				';

			   }
			   if ($longitudDoc == 4) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentos[3]->{'doc_tipo_documentacion'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
				$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
				$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
				$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos.'
					</td>
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos1.'
					</td>
				</tr>
				<tr>
					<td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td>
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<br><img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos2.'
					</td>
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos3.'
					</td>
				</tr>

				';


			   }
			    if ($longitudDoc == 5) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentos[3]->{'doc_tipo_documentacion'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
				$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
			    $doc_titulo4 = $datosDocumentos[4]->{'doc_tipo_documentacion'};
				$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
				$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
				$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
				$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
				$doc_datos3 = $datosDocumentos[4]->{'xdoc_datos'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos.'
					</td>
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos1.'
					</td>
				</tr>
				<tr>
					<td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td>
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos2.'
					</td>
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos3.'
					</td>
				</tr>
				<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo4.'</b><br>
				</td>
					
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica4.'" class="img-responsive">
				</td>
				<td width="50%" align="center">
					<br><b></b><br><br>
					<img src="" class="img-responsive">
				</td>
				
				
				</tr>
				<tr>										
					<td width="50%" id="tdbordeArchFotDes">
						<b>Descripción:</b><br>
						'.$doc_datos4.'
					</td>
				
				</tr>
				';

			   }

			   	    if ($longitudDoc == 6) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentos[3]->{'doc_tipo_documentacion'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
				$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
			    $doc_titulo4 = $datosDocumentos[4]->{'doc_tipo_documentacion'};
				$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
				$doc_correlativo5 = $datosDocumentos[5]->{'xdoc_correlativo'};
			    $doc_titulo5 = $datosDocumentos[5]->{'doc_tipo_documentacion'};
				$doc_url_logica5 = $datosDocumentos[5]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
				$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
				$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
				$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
				$doc_datos3 = $datosDocumentos[4]->{'xdoc_datos'};
				$doc_datos3 = $datosDocumentos[5]->{'xdoc_datos'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>				
				</tr>



	<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos.'
								</td>
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos1.'
								</td>
							</tr>
							<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
							


				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>

	<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos2.'
								</td>
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos3.'
								</td>
							</tr>
							<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
							

					<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo4.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo5.'</b><br>
				
				</td>
				</tr>

				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica4.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica5.'" class="img-responsive">
				</td>
				
				
				</tr>

	<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos4.'
								</td>
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos5.'
								</td>
							</tr>
							<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
							

				';

			   }

							$html.='       						
						</table>				
					</div>
			    ';

		$mpdf->AddPage();
			$mpdf->WriteHTML($html);   
			$mpdf->Output();
	}

	public function pdfImprimirTodo($datos , $datosDocumentos ,$datosDocumentosF)
	{
	 	  //$mpdf = new \Mpdf\Mpdf();
 	  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
      $mpdf->useOddEven = 1;
      $cas_id = $datos[0]->{'xcas_id'};
	  $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
	  $cas_act_id = $datos[0]->{'xcas_act_id'};
	  $cas_usr_actual_id = $datos[0]->{'xcas_usr_actual_id'};
	  
	  $cas_datos = $datos[0]->{'xcas_datos'};
	  $cas_datos = json_decode($cas_datos);
	  $g_tipo = $cas_datos->{'g_tipo'};
	  $PTR_NOMBRE = $cas_datos->{'PTR_NOMBRE'};
	  $PTR_EPOCA = $cas_datos->{'PTR_EPOCA'};
	  $PTR_PAIS = $cas_datos->{'PTR_PAIS'};
	  $PTR_DEP = $cas_datos->{'PTR_DEP'};
	  $PTR_MUNI = $cas_datos->{'PTR_MUNI'};
	  $PTR_PROV = $cas_datos->{'PTR_PROV'};
	  $PTR_MACRO = $cas_datos->{'PTR_MACRO'};
	  $PTR_ZONA = $cas_datos->{'PTR_ZONA'};
	  $PTR_COMU = $cas_datos->{'PTR_COMU'};
	  $PTR_DIR = $cas_datos->{'PTR_DIR'};
	  $PTR_AU = $cas_datos->{'PTR_AU'};
	  $PTR_ESP = $cas_datos->{'PTR_ESP'};
	  $PTR_TEC_MAT = $cas_datos->{'PTR_TEC_MAT'};
	  $PTR_CONJ_ESC = $cas_datos->{'PTR_CONJ_ESC'};
	  $PTR_ALTO = $cas_datos->{'PTR_ALTO'};
	  $PTR_ANCHO = $cas_datos->{'PTR_ANCHO'};
	  $PTR_PESO = $cas_datos->{'PTR_PESO'};
	  $PTR_DESC_OBRA = $cas_datos->{'PTR_DESC_OBRA'};
	  $PTR_ORG_PIE = $cas_datos->{'PTR_ORG_PIE'};
	  $PTR_PROP = $cas_datos->{'PTR_PROP'};
	  $PTR_RESP = $cas_datos->{'PTR_RESP'};
	  $PTR_TIPO = $cas_datos->{'PTR_TIPO'};
	  $PTR_LEY_OR = $cas_datos->{'PTR_LEY_OR'};
	  $PTR_DES_PER = $cas_datos->{'PTR_DES_PER'};
	  $PTR_REF_HIST = $cas_datos->{'PTR_REF_HIST'};
	  $PTR_REF_CON = $cas_datos->{'PTR_EST_CON'};
	  $PTR_OBS = $cas_datos->{'PTR_OBS'};
	  $PTR_FUENT_BIBLIO = $cas_datos->{'PTR_FUENT_BIBLIO'};	
	  $PTR_AU = $this->caracteresEspecialesActualizar( $PTR_AU);
      $PTR_DESC_OBRA = $this->caracteresEspecialesActualizar( $PTR_DESC_OBRA);
      $PTR_ORG_PIE = $this->caracteresEspecialesActualizar( $PTR_ORG_PIE);
      $PTR_DES_PER = $this->caracteresEspecialesActualizar( $PTR_DES_PER);
      $PTR_REF_HIST = $this->caracteresEspecialesActualizar( $PTR_REF_HIST);
      $PTR_OBS = $this->caracteresEspecialesActualizar( $PTR_OBS);
      $PTR_FUENT_BIBLIO = $this->caracteresEspecialesActualizar( $PTR_FUENT_BIBLIO);

	  
	  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
	  $cas_estado_paso = $datos[0]->{'xcas_estado_paso'};
	  $cas_fecha_inicio = $datos[0]->{'xcas_fecha_inicio'};
	  $cas_fecha_limite = $datos[0]->{'xcas_fecha_limite'};
	  $cas_nodo_id = $datos[0]->{'xcas_nodo_id'};
	  $cas_registrado = $datos[0]->{'xcas_registrado'};
	  $cas_modificado = $datos[0]->{'xcas_modificado'};
	  $cas_usr_id = $datos[0]->{'xcas_usr_id'};
	  $cas_estado = $datos[0]->{'xcas_estado'};
	  $cas_ws_id = $datos[0]->{'xcas_ws_id'};
	  $cas_asunto = $datos[0]->{'xcas_asunto'};
	  $cas_tipo_hr = $datos[0]->{'xcas_tipo_hr'};
	  $cas_id_padre = $datos[0]->{'xcas_id_padre'};
	  $campo_f = $datos[0]->{'xcampo_f'};
		  //--------------------dOCU
	 $mpdf->SetHTMLFooter('
		<table width="100%" style="font-size:12px;" >
			<tr>
			<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
			<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
			</tr>
		</table>'); 
	
	$html = '<htmlpageheader name="myheaderFicha">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE REGISTRO DE ESCULTURAS
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
		
	$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />
	<style>

		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    }
	    #tdbordeInterlineado{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    line-height: 170%;
	    }

		#interlineado{
		    line-height: 170%;
		 }   
		table{
			font-size:12px;

		}

		#tdbordeArchFotEspacio{   
		border-collapse: border-bottom-width;
	    padding:10;
	    text-align : justify;
	    font-size:12px;  
	    }

	    #tdbordeArchFotDes{		 
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:15;
	    text-align : justify;
	    font-size:18px;
	  

	    }

		#tdbordeArchFot{
			   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    }
	    .img-responsive{
	    	height: 45%;	
	    }
	</style>
	 ';

	$html.= '<br><br><br><br><br><br>
			<div style="font-size:12px;">
				<table width="100%" >
				    <tr>
			  		  <td width="35%" ><b>DENOMINACIÓN:</b></td>
			          <td width="65%" >'.$PTR_NOMBRE.'</td>
			          <td></td>
			          <td></td>
				    </tr>
				    <tr>
			  		  <td width="35%"><b>ÉPOCA O AÑO DE ELABORACIÓN:</b></td>
			          <td width="65%">'.$PTR_EPOCA.'</td>		   
				    </tr>
				</table>
				<br>
				<table width="100%" cellspacing="2">
				    <tr>
			  		  <td><b>FOTOGRAFÍAS</b></td>
				    </tr>
				</table>
				<br>
				';
	$html.= '
<div style="font-size:12px;">
						<table style="font-size:16px;" width="100%" cellpadding="2">';
							$longitudDoc=sizeof($datosDocumentos);
							if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
							 if ($longitudDoc == 1) {
						
							$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
							</tr>
							<tr>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>													
							</tr>
							<tr>										
							<td width="50%" align="center" id="tdbordeInterlineado">
							<b>Descripción:</b><br>
							'.$doc_datos.'
							</td>
							</tr>

							';
						   }
						    if ($longitudDoc == 2) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
							$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive" >
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive" style="position: relative;">
							</td>
							
							</tr>
							<tr>										
							<td width="50%" id="tdbordeArchFot" >
							<b>Descripción:</b><br>
							'.$doc_datos.'
							</td>
							<td id="tdbordeArchFot" width="50%">
							<b>Descripción:</b><br>
							'.$doc_datos1.'
							</td>
							</tr>

							';
						   }
						   if ($longitudDoc == 3) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
							$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
							$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>

						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes" >
									<b>Descripción:</b><br>
									'.$doc_datos.'
								</td>
								<td id="tdbordeArchFotDes" width="50%">
									<b>Descripción:</b><br>
									'.$doc_datos1.'
								</td>
							</tr>

							';
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>
							</tr>
							<tr>
						    <td width="50%" id="tdbordeArchFot" align="center">
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>										
							</tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos2.'
								</td>								
							</tr>';

						   }
						   if ($longitudDoc == 4) {
												
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentos[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentos[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentos[3]->{'doc_tipo_documentacion'};
							$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
							$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
							$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
							$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
							$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
							
							$html.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" id="tdbordeArchFot" VALIGN="TOP" align="center">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" VALIGN="TOP" align ="center">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos.'
								</td>
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos1.'
								</td>
							</tr>
							<tr><td id="tdbordeArchFotEspacio"></td><td id="tdbordeArchFotEspacio"></td></tr>
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo3.'</b><br>
							
							</td>
							</tr>

							<tr>
						    <td width="50%" id="tdbordeArchFot" VALIGN="TOP" align="center">
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" id="tdbordeArchFot" VALIGN="TOP" align="center">
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>				
							</tr>
							<tr>										
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos2.'
								</td>
								<td width="50%" id="tdbordeArchFotDes">
									<b>Descripción:</b><br>
									'.$doc_datos3.'
								</td>
							</tr>
							';

						   }
							$html.='       						
						</table>				
					</div>
	';

    $html.='
	<br>
	<table width="100%" id="tdborde">
	    <tr>
  		  <td><b>I. LOCALIZACIÓN</b></td>
	    </tr>
	</table>
	<br>
	<table width="100%" id="tdborde">
	    <tr> 
        <td width="30%" id="tdborde"><b>PAÍS:</b></td>
        <td width="70%" id="tdborde">'.$PTR_PAIS.'</td>
        </tr>
        <tr>
        <td width="30%" id="tdborde"><b>DEPARTAMENTO:</b></td>
         <td width="70%" id="tdborde">'.$PTR_DEP.'</td>
        </tr>
        <tr>
            <td width="30%" id="tdborde"><b>MUNICIPIO:</b></td>
              <td width="70%" id="tdborde">'.$PTR_MUNI.'</td>
          </tr>
          <tr>
            <td width="30%" id="tdborde"><b>PROVINCIA:</b></td>
              <td width="70%" id="tdborde">'.$PTR_PROV.'</td>
          </tr>
          <tr>
            <td width="30%" id="tdborde"><b>MACRODISTRITO:</b></td>
              <td width="70%" id="tdborde">'.$PTR_MACRO.'</td>
          </tr>
          <tr>
            <td width="30%" id="tdborde"><b>ZONA:</b></td>
              <td width="70%" id="tdborde">'.$PTR_ZONA.'</td>
          </tr>
          <tr>
            <td width="30%" id="tdborde"><b>COMUNIDAD:</b></td>
              <td width="70%" id="tdborde">'.$PTR_COMU.'</td>
          </tr>				
	</table>
    <br>
	<table  width="100%">
	    <tr>
  		  <td><b>II. DATOS DEL AUTOR</b></td>
	    </tr>
	</table>
	<br>
	<table width="100%" id="tdborde">
	    <tr> 
            <td width="30%" id="tdborde"><b>DATOS DEL AUTOR:</b></td>
            <td width="70%" id="tdbordeInterlineado">'.$PTR_AU.'</td>
          </tr>
	</table>
	<br>
    <table width="100%">
	    <tr>
  		 <tr><td><b>III. DATOS DE LA OBRA</b></td>
	    </tr>
	</table>
	<br>
   <table width="100%" id="tdborde">
	<tr>
       <td width="30%" id="tdborde"><b>ESPECIALIDAD:</b></td>
       <td width="70%" id="tdborde">'.$PTR_ESP.'</td>
       </tr>
       <tr>
       <td width="30%" id="tdborde"><b>TÉCNICA Y MATERIAL:</b></td>
       <td width="70%" id="tdborde">'.$PTR_TEC_MAT.'</td>
           </tr>
           <tr>
           <td width="30%" id="tdborde"><b>PIEZA ESCULTÓRICA:</b></td>
           <td width="70%" id="tdborde">
           ';
	      $longitud = count($PTR_CONJ_ESC);
	  for ($j=1; $j < $longitud; $j++) { 
	  	$estado = $PTR_CONJ_ESC[$j]->{'estado'};
		   if ($estado) {				  	
		     $resvalor = $PTR_CONJ_ESC[$j]->{'resvalor'};
		    
      $html.='
      <p>'.$resvalor.'</p>
      ';
		  	}
	  }

     $html.='</td></tr>
     </table>
     <br>
        <table  width="100%">
		    <tr>
	  		 <tr><td><b>IV. DIMENSIONES DE LA OBRA (SIN PEDESTAL)</b></td>
		    </tr>
		</table>
		<br>
        <table  width="100%" id="tdborde">
		<tr>
           <td width="30%" id="tdborde"><b>ALTO:</b></td>
           <td width="70%" id="tdborde">'.$PTR_ALTO.'</td>
        </tr>
        <tr>
           <td width="30%" id="tdborde"><b>ANCHO:</b></td>
           <td width="70%" id="tdborde">'.$PTR_ANCHO.'</td>
        </tr>
        <tr>
           <td width="30%" id="tdborde"><b>PESO:</b></td>
           <td width="70%" id="tdborde">'.$PTR_PESO.'</td>
        </tr>
        <tr>
           <td width="30%" id="tdborde"><b>DESCRIPCIÓN DE LA OBRA:</b></td>
           <td width="70%" id="tdbordeInterlineado">'.$PTR_DESC_OBRA.'</td>
        </tr>
        <tr>
           <td width="30%" id="tdborde"><b>ORIGEN DE LA PIEZA:</b></td>
           <td width="70%" id="tdbordeInterlineado">'.$PTR_ORG_PIE.'</td>
        </tr>
     </table>
      <br>
        <table  width="100%">
		    <tr>
	  		 <tr><td><b>V. REGIMEN DE PROPIEDAD</b></td>
		    </tr>
		</table>
		<br>
		 <table width="100%" id="tdborde">
		    <tr>
            <td width="30%" id="tdborde"><b>NOMBRE DEL PROPIETARIO:</b></td>
              <td width="70" id="tdborde">'.$PTR_PROP.'</td>
	          </tr>
	          <tr>
            <td width="30%" id="tdborde"><b>RESPONSABLES:</b></td>
              <td width="70%" id="tdborde">'.$PTR_RESP.'</td>
          </tr>
		</table>
		 <br>
        <table  width="100%">
		    <tr>
	  		 <tr><td><b>VI. PROTECCIÓN LEGAL</b></td>
		    </tr>
		</table>
		<br>
		<table  width="100%" id="tdborde">
		     <tr>
	            <td width="30%" id="tdborde"><b>TIPO:</b></td>
	              <td width="70%" id="tdborde">'.$PTR_TIPO.'</td>
	            </tr>
	            <tr>
	            <td width="30%" id="tdborde"><b>NRO. LEY U ORDENANZA MUNICIPAL:</b></td>
	              <td width="70%" id="tdborde">'.$PTR_LEY_OR.'</td>
	          </tr>
		</table>
		<br>
		<table  width="100%">
		    <tr>
	  		 <tr><td><b>VII. REFERENCIAS DE LA OBRA</b></td>
		    </tr>
		</table>
		<br>
		<table  width="100%" id="tdborde" id ="interlineado" >
		     <tr>
            <td width="30%" id="tdborde"><b>DESCRIPCIÓN DEL PERSONAJE O MOTIVO:</b></td>
              <td width="70%" id="tdborde">'.$PTR_DES_PER.'</td>
          </tr>
          <tr>
            <td width="30%" id="tdborde"><b>REFERENCIAS HISTÓRICAS DE LA OBRA:</b></td>
              <td width"70%" id="tdborde">'.$PTR_REF_HIST.'</td>
          </tr>
		</table>
		<br>
		<table  width="100%">
		    <tr>
	  		 <tr><td><b>VIII. ESTADO DE CONSERVACIÓN</b></td>
		    </tr>
		</table>
		<br>
		<table width="100%" id="tdborde" id ="interlineado">
	      <tr>
            <td width="30%" id="tdborde"><b>ESTADO DE CONSERVACIÓN:</b></td>
              <td width="70%" id="tdborde">'.$PTR_REF_CON.'</td>
          </tr>
          <tr>
            <td width="30%" id="tdborde"><b>OBSERVACIONES:</b></td>
              <td width="70%" id="tdborde">'.$PTR_OBS.'</td>
          </tr>
          <tr>
            <td width="30%" id="tdborde"><b>FUENTES:</b></td>
              <td width="70%" id="tdborde">'.$PTR_FUENT_BIBLIO.'</td>
          </tr>
		</table>
	</div>
	';
	////////////////////////BEGIN ARCHIVO FOTO////////////////
	$html2 = '<htmlpageheader name="myheaderFotografico">
            <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>ARCHIVO FOTOGRÁFICO DE ESCULTURAS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
 	</htmlpageheader>';

	$html2 .= ' 
		<sethtmlpageheader name="myheaderFotografico" value="on" show-this-page="1" />
        
		<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	height: 45%;
	    	
	    }
	    </style>
	      ';

 		$html2.= '
					<div style="font-size:11px;">
						<table style="font-size:11px;" width="100%" >';
							$longitudDoc=sizeof($datosDocumentosF);
							if ($datosDocumentosF == "[{ }]") {
								$longitudDoc = 0;
							}
							 if ($longitudDoc == 1) {
						
							$doc_idd = $datosDocumentosF[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};							
							$html2.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</tr>
							<tr>
							<td width="50%" align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							</tr>

							';
						   }
						    if ($longitudDoc == 2) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						  		<tr><td width="50%" align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%"  align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>

							';
						   }
						   if ($longitudDoc == 3) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>';
							$html2.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>								
							</tr>
							<tr>
						    <td width="50%" align="center"  id="tdbordeArchFot">
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						   if ($longitudDoc == 4) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentosF[3]->{'doc_tipo_documentacion'};
							$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo3.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								
								<br><img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>';

						   }
						    if ($longitudDoc == 5) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentosF[3]->{'doc_tipo_documentacion'};
							$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentosF[4]->{'doc_tipo_documentacion'};
							$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo3.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo4.'</b><br>
							</td>
								
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center">
								<br><b></b><br><br>
								<img src="" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

						   	    if ($longitudDoc == 6) {
												
						    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentosF[0]->{'doc_tipo_documentacion'};
							$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
							$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
						    $doc_titulo1 = $datosDocumentosF[1]->{'doc_tipo_documentacion'};
							$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
							$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
						    $doc_titulo2 = $datosDocumentosF[2]->{'doc_tipo_documentacion'};
							$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
							$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
						    $doc_titulo3 = $datosDocumentosF[3]->{'doc_tipo_documentacion'};
							$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
							$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
						    $doc_titulo4 = $datosDocumentosF[4]->{'doc_tipo_documentacion'};
							$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
							$doc_correlativo5 = $datosDocumentosF[5]->{'xdoc_correlativo'};
						    $doc_titulo5 = $datosDocumentosF[5]->{'doc_tipo_documentacion'};
							$doc_url_logica5 = $datosDocumentosF[5]->{'xdoc_url_logica'};
							
							$html2.='
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo1.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica1.'" class="img-responsive">
							</td>
							
							</tr>
							<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo2.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo3.'</b><br>
							
							</td>
							</tr>
							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica2.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								
								<img src="'.$doc_url_logica3.'" class="img-responsive">
							</td>
							
							
							</tr>
								<tr>
							<td width="50%" align="center">
								<b>'.$doc_titulo4.'</b><br>
							</td>
								<td width="50%" align="center">
								<b>'.$doc_titulo5.'</b><br>
							
							</td>
							</tr>

							<tr>
						    <td width="50%" align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica4.'" class="img-responsive">
							</td>
							<td width="50%" align="center" id="tdbordeArchFot">
								<img src="'.$doc_url_logica5.'" class="img-responsive">
							</td>
							
							
							</tr>
							';

						   }

							$html2.='       						
						</table>				
					</div>
			    ';
			    //////FIN ARCHIVO FOTOGRÁFICO/////

	$mpdf->AddPage();
	$mpdf->WriteHTML($html);   
	$mpdf->AddPage();
	$mpdf->WriteHTML($html2);   
	$mpdf->Output();				  	
	}
}
