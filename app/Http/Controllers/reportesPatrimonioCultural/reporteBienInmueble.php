<?php

namespace gamlp\Http\Controllers\reportesPatrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;

class reporteBienInmueble extends Controller
{
	public function getGenerar(Request $request)
	{  
		$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		$rest = substr($cas_nombre_caso,0,7);
		$g_tipo = $rest;
		$cas_datos = json_decode($cas_datos);
	 
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		//dd($datosDocumentos);
		return $this->pdf($datos, $datosDocumentos);
	}
	public function getGenerarArchFoto(Request $request)
	{  
		$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);
		$cas_id = $datos[0]->{'xcas_id'};
	    $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		$rest = substr($cas_nombre_caso,0,7);
		$g_tipo = $rest;    
		$cas_datos = json_decode($cas_datos);
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		$var = 'Revise el listado fotografico';
		$variable = 'Hay posiciones duplicadas en sus imagenes de archivo fotografico!!!';
		if(sizeof($datosDocumentos) < 7){
			return $this->pdfArchFoto($datos, $datosDocumentos);	
		}
		else{
			echo '<script language="javascript">alert("';
			echo $variable;
			echo '");</script>';
			return $var;
		}
	}
		public function getGenerarFichaTodo(Request $request)
	{ 	$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		$rest = substr($cas_nombre_caso,0,7);
		$g_tipo = $rest;
		$cas_datos = json_decode($cas_datos);
		////obtiene imagenes fichas de CATALOGACIÓN
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		////obtiene imagenes ficha fotografica
		$datosDocumentosF = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentosF = json_decode($datosDocumentosF);
		
		return $this->pdfImprimirTodo($datos, $datosDocumentos,$datosDocumentosF);
		
			
	}
	public function obtieneDocumentosPatrimonioCulturalCasos($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.' ,"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-939","parametros":'.$parametros.'}';
		$options = array(
			'http' => array(
				'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public function obtieneDocumentosPatrimonioCulturalCasosArchFoto($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.',"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-944","parametros":'.$parametros.'}';
		$options = array(
			'http' => array(
				'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}

	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';
		$url  = 'http://172.19.161.3/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/json",
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		$tokenArray=explode('"', $result);
		return $tokenArray[3];
	}

	public function obtieneDatosPatrimonioCulturalCasos($id_casos)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_casos":'.$id_casos.'}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-935","parametros":'.$parametros.'}';
		$options = array(
			'http' => array(
				'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public function caracteresEspecialesActualizar($data) {

		$data = str_replace("&039;", "'", $data);
		$data = str_replace("&040;", "\"",$data);
		$data = str_replace("&041;", "<", $data);
		$data = str_replace("&042;", ">", $data);
		$data = str_replace("&043;", "\\",$data);
		$data = str_replace("&044;", "{", $data);
		$data = str_replace("&045;", "}", $data);
		$data = str_replace("&046;", "[", $data);
		$data = str_replace("&047;", "]", $data);
		$data = str_replace("&048;", "(", $data);
		$data = str_replace("&049;", ")", $data);
		$data = str_replace("&050;", "/", $data);
		$data = str_replace("&051;", ":", $data);
		$data = str_replace("&052", " ", $data);
				 //dd($data);
		return $data;
	}
	public function cambiarValorMenos1($valor) {
	if($valor == "N" || $valor == "-1" ){
			$valor = " ";
		}
		return $valor;
	}
	public function pdf($datos , $datosDocumentos)
	{
	  	 //dd($datosDocumentos);
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->useOddEven = 1;
		
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'legal',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		
		$longitudEA = sizeof($datos);
		$cas_id = $datos[0]->{'xcas_id'};
		//$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		

		$valorAct = "";
       if(isset($cas_datos->{'PTR_USO_ACT'})){

		$actual = $cas_datos->{'PTR_USO_ACT'};
		$longitud =  sizeof($actual);
		//$valorAct = "";
		for($i = 1; $i < $longitud; $i++){
			if($actual[$i]->{'estado'}){
				$valorAct = $valorAct.$actual[$i]->{'resvalor'}.',';

			}
		}
		$valorAct = trim($valorAct, ',');
		}
		$valorTradicional = "";
		if(isset($cas_datos->{'PTR_USO_ACT'})){
		$tradicional = $cas_datos->{'PTR_USO_TRAD'};
		
		for($i = 1; $i < sizeof($tradicional); $i++){
			if($tradicional[$i]->{'estado'}){
				$valorTradicional = $valorTradicional.$tradicional[$i]->{'resvalor'}.',';

			}
		}
		$valorTradicional = trim($valorTradicional, ',');
		}

		$PTR_FEC_CREA = ""; 
		if(isset($cas_datos->{'PTR_FEC_CREA'})){
	   	$PTR_FEC_CREA = $cas_datos->{'PTR_FEC_CREA'};
	   }
	    $PTR_COD_CAT = "";
	   if(isset($cas_datos->{'PTR_COD_CAT'})){
	   	$PTR_COD_CAT = $cas_datos->{'PTR_COD_CAT'};
	   }
	   $PTR_ACT = "";
	     if(isset($cas_datos->{'PTR_ACT'})){
	   	$PTR_ACT = $cas_datos->{'PTR_ACT'};
	   }
	   $PTR_MUNI = "";
	     if(isset($cas_datos->{'PTR_MUNI'})){
	   	$PTR_MUNI = $cas_datos->{'PTR_MUNI'};
	   }
	   $PTR_MACRO = "";
	     if(isset($cas_datos->{'PTR_MACRO'})){
	   	$PTR_MACRO = $cas_datos->{'PTR_MACRO'};
	   	$PTR_MACRO = $this->cambiarValorMenos1($PTR_MACRO);
	   }
	   $PTR_ZONA = "";
	     if(isset($cas_datos->{'PTR_ZONA'})){
	   	$PTR_ZONA = $cas_datos->{'PTR_ZONA'};
	   }
	   $PTR_DIRE = "";
	     if(isset($cas_datos->{'PTR_DIRE'})){
	   	$PTR_DIRE = $cas_datos->{'PTR_DIRE'};
	   }
	   $PTR_DEP = "";
	     if(isset($cas_datos->{'PTR_DEP'})){
	   	$PTR_DEP = $cas_datos->{'PTR_DEP'};
	   }
	   $PTR_CIU = "";
	     if(isset($cas_datos->{'PTR_CIU'})){
	   	$PTR_CIU = $cas_datos->{'PTR_CIU'};
	   }
	   $PTR_NUM1 = "";
	     if(isset($cas_datos->{'PTR_NUM1'})){
	   	$PTR_NUM1 = $cas_datos->{'PTR_NUM1'};
	   }
	   $PTR_NUM2= "";
	     if(isset($cas_datos->{'PTR_NUM2'})){
	   	$PTR_NUM2 = $cas_datos->{'PTR_NUM2'};
	   }
	   $PTR_NUM3 = "";
	     if(isset($cas_datos->{'PTR_NUM3'})){
	   	$PTR_NUM3 = $cas_datos->{'PTR_NUM3'};
	   }
	    $PTR_NUM4 = "";
	     if(isset($cas_datos->{'PTR_NUM4'})){
	   	$PTR_NUM4 = $cas_datos->{'PTR_NUM4'};
	   }
  		$PTR_NUM5 = "";
	     if(isset($cas_datos->{'PTR_NUM5'})){
	   	$PTR_NUM5 = $cas_datos->{'PTR_NUM5'};
	   }

  		$PTR_DES_PRED = "";
	     if(isset($cas_datos->{'PTR_DES_PRED'})){
	   	$PTR_DES_PRED = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_DES_PRED'});
	   } 
	   $PTR_TIP_ARQ = "";
	     if(isset($cas_datos->{'PTR_TIP_ARQ'})){
	   	$PTR_TIP_ARQ = $cas_datos->{'PTR_TIP_ARQ'};
	   	$PTR_TIP_ARQ = $this->cambiarValorMenos1($PTR_TIP_ARQ);
	   }
	   $PTR_UBIC_MAN = "";
	     if(isset($cas_datos->{'PTR_UBIC_MAN'})){
	   $PTR_UBIC_MAN = $cas_datos->{'PTR_UBIC_MAN'};
	   $PTR_UBIC_MAN = $this->cambiarValorMenos1($PTR_UBIC_MAN);
	   }
	   $PTR_LIN_CONS = "";
	     if(isset($cas_datos->{'PTR_LIN_CONS'})){
	   	$PTR_LIN_CONS = $cas_datos->{'PTR_LIN_CONS'};
	   	$PTR_LIN_CONS = $this->cambiarValorMenos1($PTR_LIN_CONS);
	   }
	   $PTR_EST_CON = "";
	     if(isset($cas_datos->{'PTR_EST_CON'})){
	   	$PTR_EST_CON = $cas_datos->{'PTR_EST_CON'};
	   	$PTR_EST_CON = $this->cambiarValorMenos1($PTR_EST_CON);
	   }
	   $PTR_TRAZ_MAN = "";
	     if(isset($cas_datos->{'PTR_TRAZ_MAN'})){
	   	$PTR_TRAZ_MAN = $cas_datos->{'PTR_TRAZ_MAN'};
	   	$PTR_TRAZ_MAN = $this->cambiarValorMenos1($PTR_TRAZ_MAN);
	   }
	   $PTR_TIP_VIA = "";
	     if(isset($cas_datos->{'PTR_TIP_VIA'})){
	   	$PTR_TIP_VIA = $cas_datos->{'PTR_TIP_VIA'};
	   	$PTR_TIP_VIA = $this->cambiarValorMenos1($PTR_TIP_VIA);
	   }
	   $PTR_RAN_EPO = "";
	     if(isset($cas_datos->{'PTR_RAN_EPO'})){
	   	$PTR_RAN_EPO = $cas_datos->{'PTR_RAN_EPO'};
	   	$PTR_RAN_EPO = $this->cambiarValorMenos1($PTR_RAN_EPO);
	   }
	   $PTR_MAT_VIA = "";
	     if(isset($cas_datos->{'PTR_MAT_VIA'})){
	   $PTR_MAT_VIA = $cas_datos->{'PTR_MAT_VIA'};
	   $PTR_MAT_VIA = $this->cambiarValorMenos1($PTR_MAT_VIA);
	   }
	   $PTR_EST_GEN = "";
	     if(isset($cas_datos->{'PTR_EST_GEN'})){
	   	$PTR_EST_GEN = $cas_datos->{'PTR_EST_GEN'};
	   	$PTR_EST_GEN = $this->cambiarValorMenos1($PTR_EST_GEN);
	   }
	   $PTR_AUT_CON = "";
	     if(isset($cas_datos->{'PTR_AUT_CON'})){
	   	$PTR_AUT_CON = $cas_datos->{'PTR_AUT_CON'};
	   }
	   $PTR_FECH_CONS = "";
	     if(isset($cas_datos->{'PTR_FECH_CONS'})){
	   	$PTR_FECH_CONS = $cas_datos->{'PTR_FECH_CONS'};
	   }
	   $PTR_AUT_CONS = "";
	     if(isset($cas_datos->{'PTR_AUT_CONS'})){
	   	$PTR_AUT_CONS = $cas_datos->{'PTR_AUT_CONS'};
	   }
	   $PTR_FEC_CONST_DOC = "";
	     if(isset($cas_datos->{'PTR_FEC_CONST_DOC'})){
	   	$PTR_FEC_CONST_DOC = $cas_datos->{'PTR_FEC_CONST_DOC'};
	   }

		// ------------Variables Bloque-----------------------//
	   $PTR_CATEGORIA1 = "";
	     if(isset($cas_datos->{'PTR_CATEGORIA1'})){
	   	$PTR_CATEGORIA1 = $cas_datos->{'PTR_CATEGORIA1'};
	   }

	   $cas_nombre_caso = "";
	     if(isset($datos[0]->{'xcas_nombre_caso'})){
	   $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
	   }
	   $PTR_DES_BLOQ1 = "";
	     if(isset($cas_datos->{'PTR_DES_BLOQ1'})){
	   	$PTR_DES_BLOQ1 = $cas_datos->{'PTR_DES_BLOQ1'};
	   }
	   $PTR_ID_BLOQ1 = "";
	     if(isset($cas_datos->{'PTR_ID_BLOQ1'})){
	   	$PTR_ID_BLOQ1 = $cas_datos->{'PTR_ID_BLOQ1'};
	   }
	   $PTR_EST_BLOQ1 = "";
	     if(isset($cas_datos->{'PTR_EST_BLOQ1'})){
	   	$PTR_EST_BLOQ1 = $cas_datos->{'PTR_EST_BLOQ1'};
	   	$PTR_EST_BLOQ1 = $this->cambiarValorMenos1($PTR_EST_BLOQ1);
	   }
	   $PTR_RANG_EPO1 = "";
	     if(isset($cas_datos->{'PTR_RANG_EPO1'})){
	   $PTR_RANG_EPO1 = $cas_datos->{'PTR_RANG_EPO1'};
	   $PTR_RANG_EPO1 = $this->cambiarValorMenos1($PTR_RANG_EPO1);
	   }
	   $PTR_SIS_CONSTRUC1 = "";
	     if(isset($cas_datos->{'PTR_SIS_CONSTRUC1'})){
	   $PTR_SIS_CONSTRUC1 = $cas_datos->{'PTR_SIS_CONSTRUC1'};
	   $PTR_SIS_CONSTRUC1 = $this->cambiarValorMenos1($PTR_SIS_CONSTRUC1);
	   }
	  
	
		$indicadorHistorico = "";
	     if(isset($cas_datos->{'PTR_VAL_HIS_DESC1'})){
	    $indicadorHistorico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_HIS_DESC1'});
	   }
		$indicadorArtistico = "";
	     if(isset($cas_datos->{'PTR_VAL_ART_DESC1'})){
	  	$indicadorArtistico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_ART_DESC1'});
	   }
	   $indicadorTipologico = "";
	     if(isset($cas_datos->{'PTR_VAL_ARQ_DESC1'})){
	   $indicadorTipologico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_ARQ_DESC1'});
	   }
		$indicadorTecnologico = "";
	     if(isset($cas_datos->{'PTR_VAL_TEC_DESC1'})){
	   $indicadorTecnologico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_TEC_DESC1'});
	   }
		$indicadorIntegridad = "";
	     if(isset($cas_datos->{'PTR_VAL_INTE_DESC1'})){
	   $indicadorIntegridad = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_INTE_DESC1'});
	   }
	   $indicadorUrbano = "";
	     if(isset($cas_datos->{'PTR_VAL_URB_DESC1'})){
	   $indicadorUrbano = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_URB_DESC1'});
	   }
	   $indicadorInmaterial = "";
	     if(isset($cas_datos->{'PTR_VAL_INMAT_DESC1'})){
	   $indicadorInmaterial = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_INMAT_DESC1'});
	   }
	   $indicadorSimbolico = "";
	     if(isset($cas_datos->{'PTR_VAL_SIMB_DESC1'})){
	   $indicadorSimbolico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_SIMB_DESC1'});
	   }
	
		$html = '<htmlpageheader name="myheaderFicha">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE CATALOGACIÓN DE BIENES INMUEBLES
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
		$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';

		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');
		$html .= '
		<style>
		#altoTabla{
		width: 200%;
		height: 200%;

		}
		.altoTabla2{
			width:100%;
			height:200px;
			margin-left: auto;
  			margin-right: auto;
			overflow: auto;
			text-align: center;
		}
		#tablaborde{
		border: 1px solid #696961;
		border-spacing: 2px;
		text-align : justify;
		font-size:11px;
		font-family: Arial;

		}
		#tablaSinBorde{
		border-spacing: 2px;
		text-align : justify;
		font-size:12px;
		font-family: Arial;
		padding-left: 20px;
		line-height: 170%;
	    }
		#tdbordeArchFot{
			padding:0px;
		}
		.img-responsive{
			width: 320px;
			
		} 
		</style>

		<style>
			#tdbordeArchFot{
			border-collapse:collapse;
			border: 1px solid #A4A4A4;
		}
		</style>
		';
		
		$html.= '<div style="font-size:9px;">
		<br><br><br><br><br><br><br><br><br>
		<h2>1. IDENTIFICACIÓN</h2>

		<table width="100%" id="tablaborde">
			<tr>
				<td><b>CÓDIGO CATASTRAL:</b></td>
				<td width="70%">'.$PTR_COD_CAT.'</td>
			</tr>
			<tr>
				<td><b>FECHA DE REGISTRO:</b></td>
				<td width="70%">'.$PTR_FEC_CREA.'</td>
			</tr>
			<tr>
				<td><b>DENOMINACIÓN DEL INMUEBLE:</b></td>
				<td width="70%">'.$PTR_ACT.'</td>
			</tr>
		</table>';
		$html.= '
		<h2>2. LOCALIZACIÓN</h2>
		<table width="100%" id="tablaborde">
			<tr>
				<td><b>MUNICIPIO:</b></td>
				<td width="">'.$PTR_MUNI.'</td>
				<td><b>NUMERO DE CASA:</b></td>
				<td width=""></td>
			</tr>
			<tr>
				<td><b>MACRODISTRITO:</b></td>
				<td width="">'.$PTR_MACRO.'</td>
				<td><b>NUMERO 1:</b></td>
				<td width="">'.$PTR_NUM1.'</td>
			</tr>
			<tr>
				<td><b>ZONA:</b></td>
				<td width="">'.$PTR_ZONA.'</td>
				<td><b>NUMERO 2:</b></td>
				<td width="">'.$PTR_NUM2.'</td>
			</tr>
			<tr>
				<td><b>DIRECCIÓN:</b></td>
				<td width="">'.$PTR_DIRE.'</td>
				<td><b>NUMERO 3:</b></td>
				<td width="">'.$PTR_NUM3.'</td>
			</tr>
			<tr>
				<td><b>DEPARTAMENTO:</b></td>
				<td width="">'.$PTR_DEP.'</td>
				<td><b>NUMERO 4:</b></td>
				<td width="">'.$PTR_NUM4.'</td>
			</tr>
			<tr>
				<td><b>CIUDAD/POBLACIÓN:</b></td>
				<td width="">'.$PTR_CIU.'</td>
				<td><b>NUMERO 5:</b></td>
				<td width="">'.$PTR_NUM5.'</td>
			</tr>
		</table><br>';

		$html.='<br><div class = "altoTabla2" ><table style="margin: 0 auto; text-align: left;"  border="0">';
			$longitudDoc=sizeof($datosDocumentos);
			if ($datosDocumentos == "[{}]") {
			$longitudDoc = 0;
		}

		if ($longitudDoc == 1) {
			$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$f=file_exists($doc_url_logica);	
				if($f){
				$html.='<tr>										
				<td width="50%" align="center" >
					<b>'.$doc_titulo.'</b>
				</td></tr>
				<tr>
				<td align="center">
				<img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
				</tr>';
			    }else{
				$html.='<tr>										
				<td width="50%" align="center" >
					<b>'.$doc_titulo.'</b>
				</td></tr>
				<tr>
				<td align="center">
				<img src="" style="border: 1px solid #a3c2c2;" width="50%" >No existe Imagen</td>
				</tr>';
				}

			}
			if ($longitudDoc == 2) {
				$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_titulo1= $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
				$doc_url_logica1= $datosDocumentos[1]->{'xdoc_url_logica'};
				$f=file_exists($doc_url_logica);
				$f1=file_exists($doc_url_logica1);
				$html.='
				<tr>										
					<td width="50%" align="center" >
						<b>'.$doc_titulo.'</b>
					</td>
					<td width="50%" align="center" >
						<b>'.$doc_titulo1.'</b>
					</td>
				</tr>
				<tr>';										
					if($f){
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
				    }
				    if($f1){
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica1.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
					}	
				$html.='</tr>

				';

			}
			if ($longitudDoc == 3) {
				$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_titulo1= $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
				$doc_url_logica1= $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_titulo2= $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
				$doc_url_logica2= $datosDocumentos[2]->{'xdoc_url_logica'};
				$f=file_exists($doc_url_logica);
				$f1=file_exists($doc_url_logica1);
				$f2=file_exists($doc_url_logica2);
				$html.='
				<tr>										
					<td width="50%" align="center" >
						<b>'.$doc_titulo.'</b>
					</td>
					<td width="50%" align="center" >
						<b>'.$doc_titulo1.'</b>
					</td>
				</tr>
				<tr>';
					if($f){										
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
				     }
					if($f1){
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica1.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
				    }
				$html.='</tr>
				<tr>										
					<td colspan="2" width="50%" align="center" >
						<b>'.$doc_titulo2.'</b>
					</td>

				</tr>
				<tr>';										
				    if($f2){
					$html.='<td colspan="2"  align="center" width="50%">
						<img src="'.$doc_url_logica2.'" class="img-responsive">
					</td>';
					}else{	
					$html.='<td colspan="2"  align="center" width="50%">
						<img src="" class="img-responsive">No existe Imagen
					</td>';	
				    }
				$html.='</tr>

				';

			}
			if ($longitudDoc == 4) {

				$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
				$doc_titulo1= $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};	
				$doc_titulo2 = $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_titulo3 = $datosDocumentos[3]->{'xdoc_tipo_documentacion'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};	
				$f=file_exists($doc_url_logica);
				$f1=file_exists($doc_url_logica1);						
				$f2=file_exists($doc_url_logica2);
				$f3=file_exists($doc_url_logica3);	
				
				if($doc_titulo1 =='Plano de Ubicacion'){
					$doc_titulo1 = 'PLANO DE UBICACIÓN';
				}
				if($doc_titulo2 =='Tipologia Arquitectonica'){
					$doc_titulo2 = 'TIPOLOGÍA ARQUITECTÓNICA';
				}
				if($doc_titulo3 =='Perfil de Acera'){
					$doc_titulo3 = 'PERFIL DE ACERA';
				}
				if($doc_titulo =='Foto Principal'){
					$doc_titulo = 'FOTO PRINCIPAL';
				}
			    $html.='
					<tr>';
						if($f1){										
						$html.='<td align="center">
						    <b style="font-size:12px;">'.$doc_titulo1.'</b><br>
							<img src="'.$doc_url_logica1.'" width="35%">
						</td>';
					   }else{
						$html.='<td align="center">
							<img src="" width="40%">No existe Imagen
						</td>';
						}
						if($f){	
						$html.='<td rowspan = "3" VALIGN="TOP" align="center">
							<b style="font-size:12px;">'.$doc_titulo.'</b><br>
							<img src="'.$doc_url_logica.'" width="55%" >
						</td>';
							}else{
						$html.='<td rowspan = "3" VALIGN="TOP" align="center">
							<img src="" width="50%" >No existe Imagen
						</td>';}

					$html.='</tr>
					<tr>';
						if($f2){											
						$html.='<td align="center">
						    <b style="font-size:12px;">'.$doc_titulo2.'</b><br>
							<img src="'.$doc_url_logica2.'" width="35%">
						</td>';
					    }else{
						$html.='<td align="center">
							<img src="" width="40%">No existe Imagen
						</td>';
						}
						$html.='</tr>
						<tr>';
						if($f3){	
						$html.='<td align="center">
							<b style="font-size:12px;">'.$doc_titulo3.'</b><br>
							<img src="'.$doc_url_logica3.'" width="35%">
						</td>';
							}else{
							$html.='<td align="center">
							<img src="" width="40%">No existe Imagen
						</td>';
							}
						
					$html.='</tr>
					<br><br>
					';
	}
	$html.='</table></div>';

	$html.= '<br><h2>3. DESCRIPCIÓN DEL INMUEBLE</h2>
	<table width="100%" id="tablaSinBorde" id="tablaborde">
		<tr><td>'.$PTR_DES_PRED.'<d></tr>

	</table>';
	$html.= '<br><h2>4. TIPO DE PROPIEDAD</h2>';
	$regimenPropiedad = "";
	if(isset($cas_datos->{'PTR_REG_PROPI'})){
	$regimenPropiedad = $cas_datos->{'PTR_REG_PROPI'};
	$html.= '<table width="100%" id="tablaborde">';
	for($i = 1; $i < sizeof($regimenPropiedad); $i++){
		$tipoPropiedad = $regimenPropiedad[$i]->{'f01_reja_INT_'};
	
		if($tipoPropiedad == "N" || $tipoPropiedad == "null" ){
		$tipoPropiedad = " ";
		}
		$html.= '<tr><td  width="30%"><b>'.$tipoPropiedad.'</b></td>
		<td VALIGN="TOP">'.$regimenPropiedad[$i]->{'f01_nombre_PROP_'}.'</td></tr>';
		}
	$html.= '<tr><td></td></tr>';
	$html.= '</table>';
    }
    else {
    	$html .= '<table width="100%" id="tablaborde">
    	<tr><td></td></tr>
    	</table>
    	';
    }

	$html .= '<br><table width="100%" id="tablaborde">
		<tr>
			<td><b>USO ACTUAL:</b></td>
			<td width="">'.$valorAct.'</td>
			<td><b>USO TRADICIONAL:</b></td>
			<td width="">'.$valorTradicional.'</td>
		</tr>
	</table>';

	$html.= '<br>
	<table width="100%" id="tablaborde">
		<tr>
			<td><b>TIPOLOGIA ARQUITECTÓNICA:</b></td>
			<td width="">'.$PTR_TIP_ARQ.'</td>
			<td><b>UBICACIÓN DE MANZANA:</b></td>
			<td width="">'.$PTR_UBIC_MAN.'</td>
		</tr>
		<tr>
			<td><b>LINEA DE CONSTRUCCIÓN:</b></td>
			<td width="">'.$PTR_LIN_CONS.'</td>
			<td><b>ESTADO DE CONSERVACIÓN:</b></td>
			<td width="">'.$PTR_EST_CON.'</td>
		</tr>
		<tr>
			<td><b>TRAZO DE MANZANA:</b></td>
			<td width="">'.$PTR_TRAZ_MAN.'</td>
			<td><b>TIPO DE VIA:</b></td>
			<td width="">'.$PTR_TIP_VIA.'</td>
		</tr>
		<tr>
			<td><b>RANGO DE EPOCA:</b></td>
			<td width="">'.$PTR_RAN_EPO.'</td>
			<td><b>MATERIAL DE VÍA:</b></td>
			<td width="">'.$PTR_MAT_VIA.'</td>
		</tr>
		<tr>
			<td><b>ESTILO GENERAL:</b></td>
			<td width="">'.$PTR_EST_GEN.'</td>
		</tr>
	</table>';

	$html.= '<br><h2>II DATOS ORALES Y DOCUMENTALES</h2>
	<h2>1. FIGURA DE PROTECCIÓN LEGAL</h2>';
	$figuraProtec = "";
	if(isset($cas_datos->{'PTR_FIG_PROT'})){
	$figuraProtec = $cas_datos->{'PTR_FIG_PROT'};
	
	$html.= '<table width="100%" id="tablaborde">';
	for($i = 1; $i < sizeof($figuraProtec); $i++){
		$tipoFigura = $figuraProtec[$i]->{'f01_FIG_LEG_'};
		if($tipoFigura == "N" || $tipoFigura == "null" ){
			$tipoFigura = " ";
		}
		
		$figuraObservacion = $this->caracteresEspecialesActualizar($figuraProtec[$i]->{'f01_FIG_OBS'});
		$html.= '<tr><td  width="30%"><b>'.$tipoFigura.'</b></td>
		<td VALIGN="TOP">'.$figuraObservacion.'</td></tr>';

		}
	$html.= '<tr><td></td></tr>';
	$html.= '</table>';
    }
    else {
    	$html .= '<table width="100%" id="tablaborde">
    	<tr><td></td></tr>
    	</table>
    	';
    }
	
	$html.= '<br><h2>2. ORALES</h2>
	<table width="100%" id="tablaborde">

	<tr><td ><b>AUTOR O CONSTRUCTOR:</b></td>
	<td  width="70%">'.$PTR_AUT_CON.'</td></tr>

	<tr><td ><b>FECHA DE CONSTRUCCIÓN:</b></td>
	<td  width="70%">'.$PTR_FECH_CONS.'</td></tr>

	</table>';
	$html.= '<br><h2>3. DOCUMENTALES</h2>
	<table width="100%" id="tablaborde">

	<tr><td ><b>AUTOR O CONSTRUCTOR:</b></td>
	<td  width="70%">'.$PTR_AUT_CONS.'</td></tr>

	<tr><td ><b>FECHA DE CONSTRUCCIÓN:</b></td>
	<td  width="70%">'.$PTR_FEC_CONST_DOC.'</td></tr>

	</table>';
	
	$html.= '<br><h2>4. REFERENCIAS HISTORICAS</h2>';
	$figuraRefereciaH = "";
	if(isset($cas_datos->{'PTR_REF_HIST'})){
	$figuraRefereciaH = $cas_datos->{'PTR_REF_HIST'};
	
	$html.= '<table width="100%" id="tablaborde">';
		//dd(sizeof($figuraRefereciaH));
		for($i = 1; $i < sizeof($figuraRefereciaH); $i++){
		$tipoReferencia = $figuraRefereciaH[$i]->{'f01_ref_hist_'};
		if($tipoReferencia == "N" || $tipoReferencia == "null"){
			$tipoReferencia = " ";
		}
		
		$referenciaObservacion = "";
		if(isset($figuraRefereciaH[$i]->{'f01_FIG_OBS'})){
			$referenciaObservacion = $this->caracteresEspecialesActualizar($figuraRefereciaH[$i]->{'f01_FIG_OBS'});

		}
		else if(isset($figuraRefereciaH[$i]->{'f01_Obs_REF_'})) {
			$referenciaObservacion = $this->caracteresEspecialesActualizar($figuraRefereciaH[$i]->{'f01_Obs_REF_'}); 
		}

		//$referenciaObservacion = $this->caracteresEspecialesActualizar($figuraRefereciaH[$i]->{'f01_Obs_REF_'});
		$html.= '<tr><td  width="30%"><b>'.$tipoReferencia.'</b></td>
		<td VALIGN="TOP">'.$referenciaObservacion.'</td>
		</tr>';
		}
	$html.='<tr><td></td></tr>';	
	$html.= '</table>';
	}
	 else {
    	$html .= '<table width="100%" id="tablaborde">
    	<tr><td></td></tr>
    	</table>
    	';
    }
//---------------------Impresion Ficha Bloque------------------------//

	$html2 = '<htmlpageheader name="myheaderFichaBloque">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE BLOQUE BIENES INMUEBLES
			           </td>
			           <td width="35%" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b>CATEGORIA: </b>'.$PTR_CATEGORIA1.'<br><b>NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td>
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
	$html2 .= '<sethtmlpageheader name="myheaderFichaBloque" value="on" show-this-page="1" />';	

	$html2 .= '<div style="font-size:9px;">
	<h2>1. INFORMACIÓN POR BLOQUE:</h2>';

	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td width="35%" ><b>DESCRIPCIÓN DEL BLOQUE:</b></td>
				<td width="65%" >'.$PTR_DES_BLOQ1.'</td>
				</tr>
				<tr>
				<td width="35%" ><b>BLOQUES INVENTARIADOS:</b></td>
				<td width="65%" >1</td>
				</tr>
				<tr>
				<td width="35%" ><b>BLOQUE NÚMERO:</b></td>
				<td width="65%" >'.$PTR_ID_BLOQ1.'</td>
				</tr>
			  </table><br>';

	$html2 .= '<table width="100%" id="tablaborde">
				<tr>
				<td><b>Estilo del Bloque:</b></td>
				<td>'.$PTR_EST_BLOQ1.'</td>
				<td><b>Rango Época Bloque:</b></td>
				<td>'.$PTR_RANG_EPO1.'</td>
				</tr>
				<tr>
				<td><b>Sistema Constructivo</b></td>
				<td>'.$PTR_SIS_CONSTRUC1.'</td>
				</tr>
			  </table>';
	$html2 .= '<h2>2. VALORACIÓN:</h2>';

	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR HISTÓRICO CULTURAL:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorHistorico.'</td>

				</tr>
			  </table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR ARTÍSTICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorArtistico.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR TIPOLÓGICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorTipologico.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR TECNOLÓGICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorTecnologico.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR DE INTEGRIDAD:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorIntegridad.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR URBANO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorUrbano.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR INMATERIAL:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorInmaterial.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR SIMBÓLICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorSimbolico.'</td>
				</tr>
				</table>';

	$html2 .='</div>';

   
//---------------------Fin Impresion Ficha Bloque------------------------//


	$mpdf->AddPage();
	$mpdf->WriteHTML($html); 
	$mpdf->AddPage();
	$mpdf->WriteHTML($html2);   
	$mpdf->Output();				  
}


public function pdfArchFoto($datos, $datosDocumentos)

{
		 // dd($datosDocumentos);
	$mpdf = new \Mpdf\Mpdf();
	$mpdf->useOddEven = 1;
	$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  //-----------Documentos----------------------
	$longitudDoc = 0;


	$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);

	$mpdf->SetHTMLHeader('
           <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>ARCHIVO FOTOGRÁFICO DE BIENES INMUEBLES
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');
	
	$mpdf->SetHTMLFooter('
		<table width="100%">
			<tr>
				<td width="70%">Para cualquier consulta, comunicarse al Teléfono. 2-2440746</td>
				<td width="30%" align="right">Página {PAGENO} de {nbpg}</td>

			</tr>
		</table>');

	$html = ' 

	<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }
	    </style>
	';

	$html.= '
	<div style="font-size:11px;">
		<table style="font-size:11px;" width="100%" >';
			$longitudDoc=sizeof($datosDocumentos);


	if ($datosDocumentos == "[{}]") {
		$longitudDoc = 0;
	}
	if ($longitudDoc == 1) {

		$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
		$doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);							
		$html.='
		<tr>';
		if($f){
		$html.='
			<td width="50%" align="center">
			<b>'.$doc_titulo.'</b><br><br>
			<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
		$html.='
			<td width="50%" align="center">
			<b>'.$doc_titulo.'</b><br><br>
			<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		}
		$html.='
		</tr>';
		$html.='
		<tr>										
		<td width="50%" align="center" id="tdbordeArchFot" >'.$doc_datos.'</td>
		</tr>
	';
	}
	if ($longitudDoc == 2) {

		$doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
		$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
		$doc_titulo1 = $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);	

		$html.='
		<tr>';
		if($f){
		$html.='<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
		$html.='<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		}
		if($f1){
		$html.='<td width="50%"  align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
		$html.='<td width="50%"  align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		}
		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		';
	}
	if ($longitudDoc == 3) {

		$doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
		$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
		$doc_titulo1 = $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
		$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
		$doc_titulo2 = $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);

		$html.='
		<tr>';
			if($f){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		    }else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		    }
		    if($f1){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
			}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
			}
			$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>';
		$html.='
		<tr>';
			if($f2){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
			}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
			}
		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
		</tr>';

	}
	if ($longitudDoc == 4) {

		$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
		$doc_titulo1 = $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
		$doc_titulo2 = $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
		$doc_titulo3 = $datosDocumentos[3]->{'xdoc_tipo_documentacion'};
		$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
		$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);
		$f3=file_exists($doc_url_logica3);
		$html.='
		<tr>';
		if($f){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f1){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		<tr>';
		if($f2){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f3){	
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}


		$html.='	
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos3.'</td>
		</tr>';

	}
	if ($longitudDoc == 5) {
	
		$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
		$doc_titulo1 = $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
		$doc_titulo2 = $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
		$doc_titulo3 = $datosDocumentos[3]->{'xdoc_tipo_documentacion'};
		$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
		$doc_titulo4 = $datosDocumentos[4]->{'xdoc_tipo_documentacion'};
		$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
		$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
		$doc_datos4 = $datosDocumentos[4]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);
		$f3=file_exists($doc_url_logica3);
		$f4=file_exists($doc_url_logica4);

		$html.='
		<tr>';
		if($f){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f1){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		<tr>';
		if($f2){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f3){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html.='	
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos3.'</td>
		</tr>
		<tr>';
		if($f4){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos4.'</td>
		</tr>
		';

	}

	if ($longitudDoc == 6) {

		$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
		$doc_titulo1 = $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
		$doc_titulo2 = $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
		$doc_titulo3 = $datosDocumentos[3]->{'xdoc_tipo_documentacion'};
		$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
		$doc_titulo4 = $datosDocumentos[4]->{'xdoc_tipo_documentacion'};
		$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
		$doc_titulo5 = $datosDocumentos[5]->{'xdoc_tipo_documentacion'};
		$doc_url_logica5 = $datosDocumentos[5]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentos[2]->{'xdoc_datos'};
		$doc_datos3 = $datosDocumentos[3]->{'xdoc_datos'};
		$doc_datos4 = $datosDocumentos[4]->{'xdoc_datos'};
		$doc_datos5 = $datosDocumentos[5]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);
		$f3=file_exists($doc_url_logica3);
		$f4=file_exists($doc_url_logica4);
		$f5=file_exists($doc_url_logica5);

		$html.='
		<tr>';
		if($f){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f1){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}

		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		<tr>';
		if($f2){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
		}else{

			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}

		if($f3){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos3.'</td>
		</tr>
		<tr>';
		if($f4){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f5){
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica5.'" class="img-responsive">
			</td>';
		}else{
			$html.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}

		$html.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos4.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos5.'</td>
		</tr>
		';

	}

	$html.='       						
</table>				
</div>
';

$mpdf->AddPage();
$mpdf->WriteHTML($html);   
$mpdf->Output();
}


public function pdfImprimirTodo($datos , $datosDocumentos ,$datosDocumentosF)
	{

	  $mpdf = new \Mpdf\Mpdf();
		$mpdf->useOddEven = 1;
		
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'legal',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		
		$longitudEA = sizeof($datos);
		$cas_id = $datos[0]->{'xcas_id'};
		//$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		

	$valorAct = "";
       if(isset($cas_datos->{'PTR_USO_ACT'})){

		$actual = $cas_datos->{'PTR_USO_ACT'};
		$longitud =  sizeof($actual);
		//$valorAct = "";
		for($i = 1; $i < $longitud; $i++){
			if($actual[$i]->{'estado'}){
				$valorAct = $valorAct.$actual[$i]->{'resvalor'}.',';

			}
		}
		$valorAct = trim($valorAct, ',');
		}
		$valorTradicional = "";
		if(isset($cas_datos->{'PTR_USO_ACT'})){
		$tradicional = $cas_datos->{'PTR_USO_TRAD'};
		
		for($i = 1; $i < sizeof($tradicional); $i++){
			if($tradicional[$i]->{'estado'}){
				$valorTradicional = $valorTradicional.$tradicional[$i]->{'resvalor'}.',';

			}
		}
		$valorTradicional = trim($valorTradicional, ',');
		}

		$PTR_FEC_CREA = ""; 
		if(isset($cas_datos->{'PTR_FEC_CREA'})){
	   	$PTR_FEC_CREA = $cas_datos->{'PTR_FEC_CREA'};
	   }
	    $PTR_COD_CAT = "";
	   if(isset($cas_datos->{'PTR_COD_CAT'})){
	   	$PTR_COD_CAT = $cas_datos->{'PTR_COD_CAT'};
	   }
	   $PTR_ACT = "";
	     if(isset($cas_datos->{'PTR_ACT'})){
	   	$PTR_ACT = $cas_datos->{'PTR_ACT'};
	   }
	   $PTR_MUNI = "";
	     if(isset($cas_datos->{'PTR_MUNI'})){
	   	$PTR_MUNI = $cas_datos->{'PTR_MUNI'};
	   }
	   $PTR_MACRO = "";
	     if(isset($cas_datos->{'PTR_MACRO'})){
	   	$PTR_MACRO = $cas_datos->{'PTR_MACRO'};
	   	$PTR_MACRO = $this->cambiarValorMenos1($PTR_MACRO);
	   }
	   $PTR_ZONA = "";
	     if(isset($cas_datos->{'PTR_ZONA'})){
	   	$PTR_ZONA = $cas_datos->{'PTR_ZONA'};
	   }
	   $PTR_DIRE = "";
	     if(isset($cas_datos->{'PTR_DIRE'})){
	   	$PTR_DIRE = $cas_datos->{'PTR_DIRE'};
	   }
	   $PTR_DEP = "";
	     if(isset($cas_datos->{'PTR_DEP'})){
	   	$PTR_DEP = $cas_datos->{'PTR_DEP'};
	   }
	   $PTR_CIU = "";
	     if(isset($cas_datos->{'PTR_CIU'})){
	   	$PTR_CIU = $cas_datos->{'PTR_CIU'};
	   }
	   $PTR_NUM1 = "";
	     if(isset($cas_datos->{'PTR_NUM1'})){
	   	$PTR_NUM1 = $cas_datos->{'PTR_NUM1'};
	   }
	   $PTR_NUM2= "";
	     if(isset($cas_datos->{'PTR_NUM2'})){
	   	$PTR_NUM2 = $cas_datos->{'PTR_NUM2'};
	   }
	   $PTR_NUM3 = "";
	     if(isset($cas_datos->{'PTR_NUM3'})){
	   	$PTR_NUM3 = $cas_datos->{'PTR_NUM3'};
	   }
	    $PTR_NUM4 = "";
	     if(isset($cas_datos->{'PTR_NUM4'})){
	   	$PTR_NUM4 = $cas_datos->{'PTR_NUM4'};
	   }
  		$PTR_NUM5 = "";
	     if(isset($cas_datos->{'PTR_NUM5'})){
	   	$PTR_NUM5 = $cas_datos->{'PTR_NUM5'};
	   }

  		$PTR_DES_PRED = "";
	     if(isset($cas_datos->{'PTR_DES_PRED'})){
	   	$PTR_DES_PRED = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_DES_PRED'});
	   } 
	   $PTR_TIP_ARQ = "";
	     if(isset($cas_datos->{'PTR_TIP_ARQ'})){
	   	$PTR_TIP_ARQ = $cas_datos->{'PTR_TIP_ARQ'};
	   	$PTR_TIP_ARQ = $this->cambiarValorMenos1($PTR_TIP_ARQ);
	   }
	   $PTR_UBIC_MAN = "";
	     if(isset($cas_datos->{'PTR_UBIC_MAN'})){
	   $PTR_UBIC_MAN = $cas_datos->{'PTR_UBIC_MAN'};
	   $PTR_UBIC_MAN = $this->cambiarValorMenos1($PTR_UBIC_MAN);
	   }
	   $PTR_LIN_CONS = "";
	     if(isset($cas_datos->{'PTR_LIN_CONS'})){
	   	$PTR_LIN_CONS = $cas_datos->{'PTR_LIN_CONS'};
	   	$PTR_LIN_CONS = $this->cambiarValorMenos1($PTR_LIN_CONS);
	   }
	   $PTR_EST_CON = "";
	     if(isset($cas_datos->{'PTR_EST_CON'})){
	   	$PTR_EST_CON = $cas_datos->{'PTR_EST_CON'};
	   	$PTR_EST_CON = $this->cambiarValorMenos1($PTR_EST_CON);
	   }
	   $PTR_TRAZ_MAN = "";
	     if(isset($cas_datos->{'PTR_TRAZ_MAN'})){
	   	$PTR_TRAZ_MAN = $cas_datos->{'PTR_TRAZ_MAN'};
	   	$PTR_TRAZ_MAN = $this->cambiarValorMenos1($PTR_TRAZ_MAN);
	   }
	   $PTR_TIP_VIA = "";
	     if(isset($cas_datos->{'PTR_TIP_VIA'})){
	   	$PTR_TIP_VIA = $cas_datos->{'PTR_TIP_VIA'};
	   	$PTR_TIP_VIA = $this->cambiarValorMenos1($PTR_TIP_VIA);
	   }
	   $PTR_RAN_EPO = "";
	     if(isset($cas_datos->{'PTR_RAN_EPO'})){
	   	$PTR_RAN_EPO = $cas_datos->{'PTR_RAN_EPO'};
	   	$PTR_RAN_EPO = $this->cambiarValorMenos1($PTR_RAN_EPO);
	   }
	   $PTR_MAT_VIA = "";
	     if(isset($cas_datos->{'PTR_MAT_VIA'})){
	   $PTR_MAT_VIA = $cas_datos->{'PTR_MAT_VIA'};
	   $PTR_MAT_VIA = $this->cambiarValorMenos1($PTR_MAT_VIA);
	   }
	   $PTR_EST_GEN = "";
	     if(isset($cas_datos->{'PTR_EST_GEN'})){
	   	$PTR_EST_GEN = $cas_datos->{'PTR_EST_GEN'};
	   	$PTR_EST_GEN = $this->cambiarValorMenos1($PTR_EST_GEN);
	   }
	   $PTR_AUT_CON = "";
	     if(isset($cas_datos->{'PTR_AUT_CON'})){
	   	$PTR_AUT_CON = $cas_datos->{'PTR_AUT_CON'};
	   }
	   $PTR_FECH_CONS = "";
	     if(isset($cas_datos->{'PTR_FECH_CONS'})){
	   	$PTR_FECH_CONS = $cas_datos->{'PTR_FECH_CONS'};
	   }
	   $PTR_AUT_CONS = "";
	     if(isset($cas_datos->{'PTR_AUT_CONS'})){
	   	$PTR_AUT_CONS = $cas_datos->{'PTR_AUT_CONS'};
	   }
	   $PTR_FEC_CONST_DOC = "";
	     if(isset($cas_datos->{'PTR_FEC_CONST_DOC'})){
	   	$PTR_FEC_CONST_DOC = $cas_datos->{'PTR_FEC_CONST_DOC'};
	   }

		// ------------Variables Bloque-----------------------//
	   $PTR_CATEGORIA1 = "";
	     if(isset($cas_datos->{'PTR_CATEGORIA1'})){
	   	$PTR_CATEGORIA1 = $cas_datos->{'PTR_CATEGORIA1'};
	   }

	   $cas_nombre_caso = "";
	     if(isset($datos[0]->{'xcas_nombre_caso'})){
	   $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
	   }
	   $PTR_DES_BLOQ1 = "";
	     if(isset($cas_datos->{'PTR_DES_BLOQ1'})){
	   	$PTR_DES_BLOQ1 = $cas_datos->{'PTR_DES_BLOQ1'};
	   }
	   $PTR_ID_BLOQ1 = "";
	     if(isset($cas_datos->{'PTR_ID_BLOQ1'})){
	   	$PTR_ID_BLOQ1 = $cas_datos->{'PTR_ID_BLOQ1'};
	   }
	   $PTR_EST_BLOQ1 = "";
	     if(isset($cas_datos->{'PTR_EST_BLOQ1'})){
	   	$PTR_EST_BLOQ1 = $cas_datos->{'PTR_EST_BLOQ1'};
	   	$PTR_EST_BLOQ1 = $this->cambiarValorMenos1($PTR_EST_BLOQ1);
	   }
	   $PTR_RANG_EPO1 = "";
	     if(isset($cas_datos->{'PTR_RANG_EPO1'})){
	   $PTR_RANG_EPO1 = $cas_datos->{'PTR_RANG_EPO1'};
	   $PTR_RANG_EPO1 = $this->cambiarValorMenos1($PTR_RANG_EPO1);
	   }
	   $PTR_SIS_CONSTRUC1 = "";
	     if(isset($cas_datos->{'PTR_SIS_CONSTRUC1'})){
	   $PTR_SIS_CONSTRUC1 = $cas_datos->{'PTR_SIS_CONSTRUC1'};
	   $PTR_SIS_CONSTRUC1 = $this->cambiarValorMenos1($PTR_SIS_CONSTRUC1);
	   }
	  
	
		$indicadorHistorico = "";
	     if(isset($cas_datos->{'PTR_VAL_HIS_DESC1'})){
	    $indicadorHistorico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_HIS_DESC1'});
	   }
		$indicadorArtistico = "";
	     if(isset($cas_datos->{'PTR_VAL_ART_DESC1'})){
	  	$indicadorArtistico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_ART_DESC1'});
	   }
	   $indicadorTipologico = "";
	     if(isset($cas_datos->{'PTR_VAL_ARQ_DESC1'})){
	   $indicadorTipologico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_ARQ_DESC1'});
	   }
		$indicadorTecnologico = "";
	     if(isset($cas_datos->{'PTR_VAL_TEC_DESC1'})){
	   $indicadorTecnologico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_TEC_DESC1'});
	   }
		$indicadorIntegridad = "";
	     if(isset($cas_datos->{'PTR_VAL_INTE_DESC1'})){
	   $indicadorIntegridad = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_INTE_DESC1'});
	   }
	   $indicadorUrbano = "";
	     if(isset($cas_datos->{'PTR_VAL_URB_DESC1'})){
	   $indicadorUrbano = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_URB_DESC1'});
	   }
	   $indicadorInmaterial = "";
	     if(isset($cas_datos->{'PTR_VAL_INMAT_DESC1'})){
	   $indicadorInmaterial = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_INMAT_DESC1'});
	   }
	   $indicadorSimbolico = "";
	     if(isset($cas_datos->{'PTR_VAL_SIMB_DESC1'})){
	   $indicadorSimbolico = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_VAL_SIMB_DESC1'});
	   }
			
		$html = '<htmlpageheader name="myheaderFicha">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE CATALOGACIÓN DE BIENES INMUEBLES
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
		$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';

		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');
		$html .= '
		<style>
		#altoTabla{
		width: 200%;
		height: 200%;

		}
		.altoTabla2{
			width:100%;
			height:200px;
			margin-left: auto;
  			margin-right: auto;
			overflow: auto;
			text-align: center;
		}
		#tablaborde{
		border: 1px solid #696961;
		border-spacing: 2px;
		text-align : justify;
		font-size:11px;
		font-family: Arial;

		}
		#tablaSinBorde{
		border-spacing: 2px;
		text-align : justify;
		font-size:12px;
		font-family: Arial;
		padding-left: 20px;
		line-height: 170%;
	    }
		#tdbordeArchFot{
			padding:0px;
		}
		.img-responsive{
			width: 320px;
			
		} 
		</style>

		<style>
			#tdbordeArchFot{
			border-collapse:collapse;
			border: 1px solid #A4A4A4;
		}
		</style>
		';
		$html.= '<div style="font-size:9px;">
		<br><br><br><br><br><br><br><br><br>
		<h2>1. IDENTIFICACIÓN</h2>

		<table width="100%" id="tablaborde">
			<tr>
				<td><b>CÓDIGO CATASTRAL:</b></td>
				<td width="70%">'.$PTR_COD_CAT.'</td>
			</tr>
			<tr>
				<td><b>FECHA DE REGISTRO:</b></td>
				<td width="70%">'.$PTR_FEC_CREA.'</td>
			</tr>
			<tr>
				<td><b>DENOMINACIÓN DEL INMUEBLE:</b></td>
				<td width="70%">'.$PTR_ACT.'</td>
			</tr>
		</table>';
		$html.= '
		<h2>2. LOCALIZACIÓN</h2>
		<table width="100%" id="tablaborde">
			<tr>
				<td><b>MUNICIPIO:</b></td>
				<td width="">'.$PTR_MUNI.'</td>
				<td><b>NUMERO DE CASA:</b></td>
				<td width=""></td>
			</tr>
			<tr>
				<td><b>MACRODISTRITO:</b></td>
				<td width="">'.$PTR_MACRO.'</td>
				<td><b>NUMERO 1:</b></td>
				<td width="">'.$PTR_NUM1.'</td>
			</tr>
			<tr>
				<td><b>ZONA:</b></td>
				<td width="">'.$PTR_ZONA.'</td>
				<td><b>NUMERO 2:</b></td>
				<td width="">'.$PTR_NUM2.'</td>
			</tr>
			<tr>
				<td><b>DIRECCIÓN:</b></td>
				<td width="">'.$PTR_DIRE.'</td>
				<td><b>NUMERO 3:</b></td>
				<td width="">'.$PTR_NUM3.'</td>
			</tr>
			<tr>
				<td><b>DEPARTAMENTO:</b></td>
				<td width="">'.$PTR_DEP.'</td>
				<td><b>NUMERO 4:</b></td>
				<td width="">'.$PTR_NUM4.'</td>
			</tr>
			<tr>
				<td><b>CIUDAD/POBLACIÓN:</b></td>
				<td width="">'.$PTR_CIU.'</td>
				<td><b>NUMERO 5:</b></td>
				<td width="">'.$PTR_NUM5.'</td>
			</tr>
		</table><br>';

		$html.='<br><div class = "altoTabla2" ><table style="margin: 0 auto; text-align: left;"  border="0">';
			$longitudDoc=sizeof($datosDocumentos);
			if ($datosDocumentos == "[{ }]") {
			$longitudDoc = 0;
		}

		if ($longitudDoc == 1) {
			$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$f=file_exists($doc_url_logica);	
				if($f){
				$html.='<tr>										
				<td width="50%" align="center" >
					<b>'.$doc_titulo.'</b>
				</td></tr>
				<tr>
				<td align="center">
				<img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
				</tr>';
			    }else{
				$html.='<tr>										
				<td width="50%" align="center" >
					<b>'.$doc_titulo.'</b>
				</td></tr>
				<tr>
				<td align="center">
				<img src="" style="border: 1px solid #a3c2c2;" width="50%" >No existe Imagen</td>
				</tr>';
				}

			}
			if ($longitudDoc == 2) {
				$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_titulo1= $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
				$doc_url_logica1= $datosDocumentos[1]->{'xdoc_url_logica'};
				$f=file_exists($doc_url_logica);
				$f1=file_exists($doc_url_logica1);
				$html.='
				<tr>										
					<td width="50%" align="center" >
						<b>'.$doc_titulo.'</b>
					</td>
					<td width="50%" align="center" >
						<b>'.$doc_titulo1.'</b>
					</td>
				</tr>
				<tr>';										
					if($f){
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
				    }
				    if($f1){
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica1.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
					}	
				$html.='</tr>

				';

			}
			if ($longitudDoc == 3) {
				$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_titulo1= $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
				$doc_url_logica1= $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_titulo2= $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
				$doc_url_logica2= $datosDocumentos[2]->{'xdoc_url_logica'};
				$f=file_exists($doc_url_logica);
				$f1=file_exists($doc_url_logica1);
				$f2=file_exists($doc_url_logica2);
				$html.='
				<tr>										
					<td width="50%" align="center" >
						<b>'.$doc_titulo.'</b>
					</td>
					<td width="50%" align="center" >
						<b>'.$doc_titulo1.'</b>
					</td>
				</tr>
				<tr>';
					if($f){										
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
				     }
					if($f1){
					$html.='<td align="center" width="50%" >
						<img src="'.$doc_url_logica1.'" class="img-responsive">
					</td>';
					}else{
					$html.='<td align="center" width="50%" >
						<img src="" class="img-responsive">No existe Imagen
					</td>';
				    }
				$html.='</tr>
				<tr>										
					<td colspan="2" width="50%" align="center" >
						<b>'.$doc_titulo2.'</b>
					</td>

				</tr>
				<tr>';										
				    if($f2){
					$html.='<td colspan="2"  align="center" width="50%">
						<img src="'.$doc_url_logica2.'" class="img-responsive">
					</td>';
					}else{	
					$html.='<td colspan="2"  align="center" width="50%">
						<img src="" class="img-responsive">No existe Imagen
					</td>';	
				    }
				$html.='</tr>

				';

			}
			if ($longitudDoc == 4) {

				$doc_titulo = $datosDocumentos[0]->{'xdoc_tipo_documentacion'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
				$doc_titulo1= $datosDocumentos[1]->{'xdoc_tipo_documentacion'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};	
				$doc_titulo2 = $datosDocumentos[2]->{'xdoc_tipo_documentacion'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_titulo3 = $datosDocumentos[3]->{'xdoc_tipo_documentacion'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};	
				$f=file_exists($doc_url_logica);
				$f1=file_exists($doc_url_logica1);						
				$f2=file_exists($doc_url_logica2);
				$f3=file_exists($doc_url_logica3);	
				
				if($doc_titulo1 =='Plano de Ubicacion'){
					$doc_titulo1 = 'PLANO DE UBICACIÓN';
				}
				if($doc_titulo2 =='Tipologia Arquitectonica'){
					$doc_titulo2 = 'TIPOLOGÍA ARQUITECTÓNICA';
				}
				if($doc_titulo3 =='Perfil de Acera'){
					$doc_titulo3 = 'PERFIL DE ACERA';
				}
				if($doc_titulo =='Foto Principal'){
					$doc_titulo = 'FOTO PRINCIPAL';
				}
			    $html.='
					<tr>';
						if($f1){										
						$html.='<td align="center">
						    <b style="font-size:12px;">'.$doc_titulo1.'</b><br>
							<img src="'.$doc_url_logica1.'" width="35%">
						</td>';
					   }else{
						$html.='<td align="center">
							<img src="" width="40%">No existe Imagen
						</td>';
						}
						if($f){	
						$html.='<td rowspan = "3" VALIGN="TOP" align="center">
							<b style="font-size:12px;">'.$doc_titulo.'</b><br>
							<img src="'.$doc_url_logica.'" width="55%" >
						</td>';
							}else{
						$html.='<td rowspan = "3" VALIGN="TOP" align="center">
							<img src="" width="50%" >No existe Imagen
						</td>';}

					$html.='</tr>
					<tr>';
						if($f2){											
						$html.='<td align="center">
						    <b style="font-size:12px;">'.$doc_titulo2.'</b><br>
							<img src="'.$doc_url_logica2.'" width="35%">
						</td>';
					    }else{
						$html.='<td align="center">
							<img src="" width="40%">No existe Imagen
						</td>';
						}
						$html.='</tr>
						<tr>';
						if($f3){	
						$html.='<td align="center">
							<b style="font-size:12px;">'.$doc_titulo3.'</b><br>
							<img src="'.$doc_url_logica3.'" width="35%">
						</td>';
							}else{
							$html.='<td align="center">
							<img src="" width="40%">No existe Imagen
						</td>';
							}
						
					$html.='</tr>
					<br><br>
					';
	}
	$html.='</table></div>';

	$html.= '<br><h2>3. DESCRIPCIÓN DEL INMUEBLE</h2>
	<table width="100%" id="tablaSinBorde" id="tablaborde">
		<tr><td>'.$PTR_DES_PRED.'<d></tr>

	</table>';
	$html.= '<br><h2>4. TIPO DE PROPIEDAD</h2>';
	$regimenPropiedad = "";
	if(isset($cas_datos->{'PTR_REG_PROPI'})){
	$regimenPropiedad = $cas_datos->{'PTR_REG_PROPI'};
	$html.= '<table width="100%" id="tablaborde">';
	for($i = 1; $i < sizeof($regimenPropiedad); $i++){
		$tipoPropiedad = $regimenPropiedad[$i]->{'f01_reja_INT_'};
	
		if($tipoPropiedad == "N" || $tipoPropiedad == "null" ){
		$tipoPropiedad = " ";
		}
		$html.= '<tr><td  width="30%"><b>'.$tipoPropiedad.'</b></td>
		<td VALIGN="TOP">'.$regimenPropiedad[$i]->{'f01_nombre_PROP_'}.'</td></tr>';
		}
	$html.= '<tr><td></td></tr>';
	$html.= '</table>';
    }
    else {
    	$html .= '<table width="100%" id="tablaborde">
    	<tr><td></td></tr>
    	</table>
    	';
    }

	$html .= '<br><table width="100%" id="tablaborde">
		<tr>
			<td><b>USO ACTUAL:</b></td>
			<td width="">'.$valorAct.'</td>
			<td><b>USO TRADICIONAL:</b></td>
			<td width="">'.$valorTradicional.'</td>
		</tr>
	</table>';

	$html.= '<br>
	<table width="100%" id="tablaborde">
		<tr>
			<td><b>TIPOLOGIA ARQUITECTÓNICA:</b></td>
			<td width="">'.$PTR_TIP_ARQ.'</td>
			<td><b>UBICACIÓN DE MANZANA:</b></td>
			<td width="">'.$PTR_UBIC_MAN.'</td>
		</tr>
		<tr>
			<td><b>LINEA DE CONSTRUCCIÓN:</b></td>
			<td width="">'.$PTR_LIN_CONS.'</td>
			<td><b>ESTADO DE CONSERVACIÓN:</b></td>
			<td width="">'.$PTR_EST_CON.'</td>
		</tr>
		<tr>
			<td><b>TRAZO DE MANZANA:</b></td>
			<td width="">'.$PTR_TRAZ_MAN.'</td>
			<td><b>TIPO DE VIA:</b></td>
			<td width="">'.$PTR_TIP_VIA.'</td>
		</tr>
		<tr>
			<td><b>RANGO DE EPOCA:</b></td>
			<td width="">'.$PTR_RAN_EPO.'</td>
			<td><b>MATERIAL DE VÍA:</b></td>
			<td width="">'.$PTR_MAT_VIA.'</td>
		</tr>
		<tr>
			<td><b>ESTILO GENERAL:</b></td>
			<td width="">'.$PTR_EST_GEN.'</td>
		</tr>
	</table>';

	$html.= '<br><h2>II DATOS ORALES Y DOCUMENTALES</h2>
	<h2>1. FIGURA DE PROTECCIÓN LEGAL</h2>';
	$figuraProtec = "";
	if(isset($cas_datos->{'PTR_FIG_PROT'})){
	$figuraProtec = $cas_datos->{'PTR_FIG_PROT'};
	
	$html.= '<table width="100%" id="tablaborde">';
	for($i = 1; $i < sizeof($figuraProtec); $i++){
		$tipoFigura = $figuraProtec[$i]->{'f01_FIG_LEG_'};
		if($tipoFigura == "N" || $tipoFigura == "null" ){
			$tipoFigura = " ";
		}
		
		$figuraObservacion = $this->caracteresEspecialesActualizar($figuraProtec[$i]->{'f01_FIG_OBS'});
		$html.= '<tr><td  width="30%"><b>'.$tipoFigura.'</b></td>
		<td VALIGN="TOP">'.$figuraObservacion.'</td></tr>';

		}
	$html.= '<tr><td></td></tr>';
	$html.= '</table>';
    }
    else {
    	$html .= '<table width="100%" id="tablaborde">
    	<tr><td></td></tr>
    	</table>
    	';
    }
	
	$html.= '<br><h2>2. ORALES</h2>
	<table width="100%" id="tablaborde">

	<tr><td ><b>AUTOR O CONSTRUCTOR:</b></td>
	<td  width="70%">'.$PTR_AUT_CON.'</td></tr>

	<tr><td ><b>FECHA DE CONSTRUCCIÓN:</b></td>
	<td  width="70%">'.$PTR_FECH_CONS.'</td></tr>

	</table>';
	$html.= '<br><h2>3. DOCUMENTALES</h2>
	<table width="100%" id="tablaborde">

	<tr><td ><b>AUTOR O CONSTRUCTOR:</b></td>
	<td  width="70%">'.$PTR_AUT_CONS.'</td></tr>

	<tr><td ><b>FECHA DE CONSTRUCCIÓN:</b></td>
	<td  width="70%">'.$PTR_FEC_CONST_DOC.'</td></tr>

	</table>';
	
	$html.= '<br><h2>4. REFERENCIAS HISTORICAS</h2>';
	$figuraRefereciaH = "";
	if(isset($cas_datos->{'PTR_REF_HIST'})){
	$figuraRefereciaH = $cas_datos->{'PTR_REF_HIST'};
	
	$html.= '<table width="100%" id="tablaborde">';
		//dd(sizeof($figuraRefereciaH));
		for($i = 1; $i < sizeof($figuraRefereciaH); $i++){
		$tipoReferencia = $figuraRefereciaH[$i]->{'f01_ref_hist_'};
		if($tipoReferencia == "N" || $tipoReferencia == "null"){
			$tipoReferencia = " ";
		}
			$referenciaObservacion = "";
		if(isset($figuraRefereciaH[$i]->{'f01_FIG_OBS'})){
			$referenciaObservacion = $this->caracteresEspecialesActualizar($figuraRefereciaH[$i]->{'f01_FIG_OBS'});

		}
		else if(isset($figuraRefereciaH[$i]->{'f01_Obs_REF_'})) {
			$referenciaObservacion = $this->caracteresEspecialesActualizar($figuraRefereciaH[$i]->{'f01_Obs_REF_'}); 
		}

		//$referenciaObservacion = $this->caracteresEspecialesActualizar($figuraRefereciaH[$i]->{'f01_Obs_REF_'});
		//$referenciaObservacion = $this->caracteresEspecialesActualizar($figuraRefereciaH[$i]->{'f01_Obs_REF_'});
		$html.= '<tr><td  width="30%"><b>'.$tipoReferencia.'</b></td>
		<td VALIGN="TOP">'.$referenciaObservacion.'</td>
		</tr>';
		}
	$html.='<tr><td></td></tr>';	
	$html.= '</table>';
	}
	 else {
    	$html .= '<table width="100%" id="tablaborde">
    	<tr><td></td></tr>
    	</table>
    	';
    }
//---------------------Impresion Ficha Bloque------------------------//

	$html2 = '<htmlpageheader name="myheaderFichaBloque">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE BLOQUE BIENES INMUEBLES
			           </td>
			           <td width="35%" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b>CATEGORIA: </b>'.$PTR_CATEGORIA1.'<br><b>NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td>
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
	$html2 .= '<sethtmlpageheader name="myheaderFichaBloque" value="on" show-this-page="1" />';	

	$html2 .= '<div style="font-size:9px;">
	<h2>1. INFORMACIÓN POR BLOQUE:</h2>';

	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td width="35%" ><b>DESCRIPCIÓN DEL BLOQUE:</b></td>
				<td width="65%" >'.$PTR_DES_BLOQ1.'</td>
				</tr>
				<tr>
				<td width="35%" ><b>BLOQUES INVENTARIADOS:</b></td>
				<td width="65%" >1</td>
				</tr>
				<tr>
				<td width="35%" ><b>BLOQUE NÚMERO:</b></td>
				<td width="65%" >'.$PTR_ID_BLOQ1.'</td>
				</tr>
			  </table><br>';

	$html2 .= '<table width="100%" id="tablaborde">
				<tr>
				<td><b>Estilo del Bloque:</b></td>
				<td>'.$PTR_EST_BLOQ1.'</td>
				<td><b>Rango Época Bloque:</b></td>
				<td>'.$PTR_RANG_EPO1.'</td>
				</tr>
				<tr>
				<td><b>Sistema Constructivo</b></td>
				<td>'.$PTR_SIS_CONSTRUC1.'</td>
				</tr>
			  </table>';
	$html2 .= '<h2>2. VALORACIÓN:</h2>';

	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR HISTÓRICO CULTURAL:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorHistorico.'</td>

				</tr>
			  </table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR ARTÍSTICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorArtistico.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR TIPOLÓGICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorTipologico.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR TECNOLÓGICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorTecnologico.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR DE INTEGRIDAD:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorIntegridad.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR URBANO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorUrbano.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR INMATERIAL:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorInmaterial.'</td>
				</tr>
				</table>';
	$html2 .= '<table width="100%" id="tablaSinBorde">
				<tr>
				<td><b>INDICADOR SIMBÓLICO:</b></td>
				</tr>
				<tr>
				<td>'.$indicadorSimbolico.'</td>
				</tr>
				</table>';

	$html2 .='</div>';
   
//---------------------Fin Impresion Ficha Bloque------------------------//

//// BEGIN FICHA FOTOGRAFICA //////

	$html3 = '<htmlpageheader name="myheaderFotografico">
            <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>ARCHIVO FOTOGRÁFICO DE BIENES INMUEBLES
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
 	</htmlpageheader>';
 	$html3 .= ' 
		<sethtmlpageheader name="myheaderFotografico" value="on" show-this-page="1" />';

	$html3 .= ' 
	<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }
	    </style>
	';

	$html3.= '
	<div style="font-size:11px;">
		<table style="font-size:11px;" width="100%" >';
			$longitudDoc=sizeof($datosDocumentosF);


	if ($datosDocumentosF == "[{}]") {
		$longitudDoc = 0;
	}
	if ($longitudDoc == 1) {

		$doc_idd = $datosDocumentosF[0]->{'xdoc_idd'};
		$doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		$doc_titulo = $datosDocumentosF[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentosF[0]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);							
		$html3.='
		<tr>';
		if($f){
		$html3.='
			<td width="50%" align="center">
			<b>'.$doc_titulo.'</b><br><br>
			<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
		$html3.='
			<td width="50%" align="center">
			<b>'.$doc_titulo.'</b><br><br>
			<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		}
		$html3.='
		</tr>';
		$html3.='
		<tr>										
		<td width="50%" align="center" id="tdbordeArchFot" >'.$doc_datos.'</td>
		</tr>
	';
	}
	if ($longitudDoc == 2) {

		$doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		$doc_titulo = $datosDocumentosF[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
		$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
		$doc_titulo1 = $datosDocumentosF[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentosF[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentosF[1]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);	

		$html3.='
		<tr>';
		if($f){
		$html3.='<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
		$html3.='<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		}
		if($f1){
		$html3.='<td width="50%"  align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
		$html3.='<td width="50%"  align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		}
		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		';
	}
	if ($longitudDoc == 3) {

		$doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
		$doc_titulo = $datosDocumentosF[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
		$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
		$doc_titulo1 = $datosDocumentosF[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
		$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
		$doc_titulo2 = $datosDocumentosF[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentosF[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentosF[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentosF[2]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);

		$html3.='
		<tr>';
			if($f){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		    }else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
		    }
		    if($f1){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
			}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion
			</td>';
			}
			$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>';
		$html3.='
		<tr>';
			if($f2){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
			}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
			}
		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
		</tr>';

	}
	if ($longitudDoc == 4) {

		$doc_titulo = $datosDocumentosF[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
		$doc_titulo1 = $datosDocumentosF[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
		$doc_titulo2 = $datosDocumentosF[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
		$doc_titulo3 = $datosDocumentosF[3]->{'xdoc_tipo_documentacion'};
		$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentosF[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentosF[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentosF[2]->{'xdoc_datos'};
		$doc_datos3 = $datosDocumentosF[3]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);
		$f3=file_exists($doc_url_logica3);
		$html3.='
		<tr>';
		if($f){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f1){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		<tr>';
		if($f2){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f3){	
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<br><img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}


		$html3.='	
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos3.'</td>
		</tr>';

	}
	if ($longitudDoc == 5) {
	
		$doc_titulo = $datosDocumentosF[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
		$doc_titulo1 = $datosDocumentosF[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
		$doc_titulo2 = $datosDocumentosF[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
		$doc_titulo3 = $datosDocumentosF[3]->{'xdoc_tipo_documentacion'};
		$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
		$doc_titulo4 = $datosDocumentosF[4]->{'xdoc_tipo_documentacion'};
		$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentosF[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentosF[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentosF[2]->{'xdoc_datos'};
		$doc_datos3 = $datosDocumentosF[3]->{'xdoc_datos'};
		$doc_datos4 = $datosDocumentosF[4]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);
		$f3=file_exists($doc_url_logica3);
		$f4=file_exists($doc_url_logica4);

		$html3.='
		<tr>';
		if($f){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f1){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		<tr>';
		if($f2){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f3){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html3.='	
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos3.'</td>
		</tr>
		<tr>';
		if($f4){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos4.'</td>
		</tr>
		';

	}

	if ($longitudDoc == 6) {

		$doc_titulo = $datosDocumentosF[0]->{'xdoc_tipo_documentacion'};
		$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
		$doc_titulo1 = $datosDocumentosF[1]->{'xdoc_tipo_documentacion'};
		$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
		$doc_titulo2 = $datosDocumentosF[2]->{'xdoc_tipo_documentacion'};
		$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
		$doc_titulo3 = $datosDocumentosF[3]->{'xdoc_tipo_documentacion'};
		$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
		$doc_titulo4 = $datosDocumentosF[4]->{'xdoc_tipo_documentacion'};
		$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
		$doc_titulo5 = $datosDocumentosF[5]->{'xdoc_tipo_documentacion'};
		$doc_url_logica5 = $datosDocumentosF[5]->{'xdoc_url_logica'};
		$doc_datos = $datosDocumentosF[0]->{'xdoc_datos'};	
		$doc_datos1 = $datosDocumentosF[1]->{'xdoc_datos'};
		$doc_datos2 = $datosDocumentosF[2]->{'xdoc_datos'};
		$doc_datos3 = $datosDocumentosF[3]->{'xdoc_datos'};
		$doc_datos4 = $datosDocumentosF[4]->{'xdoc_datos'};
		$doc_datos5 = $datosDocumentosF[5]->{'xdoc_datos'};
		$f=file_exists($doc_url_logica);
		$f1=file_exists($doc_url_logica1);
		$f2=file_exists($doc_url_logica2);
		$f3=file_exists($doc_url_logica3);
		$f4=file_exists($doc_url_logica4);
		$f5=file_exists($doc_url_logica5);

		$html3.='
		<tr>';
		if($f){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f1){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}

		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos1.'</td>
		</tr>
		<tr>';
		if($f2){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>';
		}else{

			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}

		if($f3){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos2.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos3.'</td>
		</tr>
		<tr>';
		if($f4){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}
		if($f5){
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica5.'" class="img-responsive">
			</td>';
		}else{
			$html3.='
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="" class="img-responsive">No existe imagen solo direccion logica
			</td>';
		}

		$html3.='
		</tr>
		<tr>										
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos4.'</td>
			<td width="50%" id="tdbordeArchFot" >'.$doc_datos5.'</td>
		</tr>
		';

	}

	$html3.='       						
	</table>				
	</div>
	';
///////////////// FIN FICHA FOTOGRAFICA //////

	$mpdf->AddPage();
	$mpdf->WriteHTML($html);
	$mpdf->AddPage();
	$mpdf->WriteHTML($html2);  
	$mpdf->AddPage();
	$mpdf->WriteHTML($html3);   
	$mpdf->Output();	
		  
}

}

