<?php

namespace gamlp\Http\Controllers\reportesPatrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;

class reporteBienInmaterial extends Controller
{
     public function getGenerar(Request $request)
    { 
       //dd($request);
  	   $id_casos = $request->get('datos');
  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
       $datos = json_decode($datos); 
       //dd($datos);
       $cas_id = $datos[0]->{'xcas_id'};
       $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'};
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);
       //dd($datosDocumentos);
       return $this->pdf($datos, $datosDocumentos);
    }
      public function getGenerarArchFoto(Request $request)
    {  
  	   $id_casos = $request->get('datos');
  	   $datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
       $datos = json_decode($datos);
       $cas_id = $datos[0]->{'xcas_id'};
       $cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
       $cas_datos = $datos[0]->{'xcas_datos'};
	   $cas_datos = json_decode($cas_datos);
	   $g_tipo = $cas_datos->{'g_tipo'};       
       $datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
       $datosDocumentos = json_decode($datosDocumentos);

       return $this->pdfArchFoto($datos, $datosDocumentos);

  	   
    }
    
     //---------------------------------------------------------------------
    //---------------------------------------------------------------------
     public function getGenerarFichaTodo(Request $request)
	{ 	$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
		////obtiene imagenes fichas de CATALOGACIÓN
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		////obtiene imagenes ficha fotografica
		$datosDocumentosF = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentosF = json_decode($datosDocumentosF);
		return $this->pdfImprimirTodo($datos, $datosDocumentos,$datosDocumentosF);		
	}
    //------------------------------------------------------------



    public function obtieneDocumentosPatrimonioCulturalCasos($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.' ,"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-939","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	    public function obtieneDocumentosPatrimonioCulturalCasosArchFoto($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.',"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-944","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';
		$url  = 'http://172.19.161.3/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/json",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		 }
		 $tokenArray=explode('"', $result);
		  return $tokenArray[3];
	}

	public function obtieneDatosPatrimonioCulturalCasos($id_casos)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_casos":'.$id_casos.'}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-935","parametros":'.$parametros.'}';
		$options = array(
		    'http' => array(
		        'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}

	public function caracteresEspecialesActualizar($data) {

		        //$data = str_replace("&039;", "'", $data);

				$data = str_replace("&040;", "\"",$data);
				 //dd($data);
				$data = str_replace("&041;", "<", $data);
				$data = str_replace("&042;", ">", $data);
				$data = str_replace("&043;", "\\",$data);
				$data = str_replace("&044;", "{", $data);
				$data = str_replace("&045;", "}", $data);
				$data = str_replace("&046;", "[", $data);
				$data = str_replace("&047;", "]", $data);
				$data = str_replace("&048;", "(", $data);
				$data = str_replace("&049;", ")", $data);
				$data = str_replace("&050;", "/", $data);
				$data = str_replace("&051;", ":", $data);
				$data = str_replace("&052", " ", $data);
				$data = str_replace("&053;", "#", $data);
				 //dd($data);
		        return $data;
		}

	public function pdf($datos, $datosDocumentos)
  	{
	  	 //dd($datos);
	  	  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	       $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
	      $longitudEA = sizeof($datos);
		  $cas_id = $datos[0]->{'xcas_id'};
		  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
				  
		  $cas_datos = $datos[0]->{'xcas_datos'};
		  $cas_datos = json_decode($cas_datos);
		  $g_tipo = $cas_datos->{'g_tipo'};
		  $TIPO_ARQ = $cas_datos->{'PTR_AREAS'};
		  //dd($TIPO_ARQ);
        

		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');

		$html = '
		<style>
		p{
		  	   text-align : justify;
		  	   padding-left: 1cm;
		 }

		  .img-responsive{
	    	height: 50%;
	    } 
	   
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	 

	    }
	    #tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;

	    }
	  
		</style>
		';
		
	  if($TIPO_ARQ=='1'|| $TIPO_ARQ=='2' || $TIPO_ARQ=='3' || $TIPO_ARQ=='4' || $TIPO_ARQ=='5' || $TIPO_ARQ=='7'){
	  	
			$mpdf->SetHTMLHeader('
           <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>AMBITO III USOS SOCIALES, RITUALES Y ACTOS FESTIVOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');
		$nivelProteccion = $cas_datos->{'PTR_NIV_PROTEC'};
		$longitud =  sizeof($nivelProteccion);
		$valorAct = "";
		for($i = 1; $i < $longitud; $i++){
			$estadoNivel = $nivelProteccion[$i]->{'estado'};

			if( $estadoNivel == true){
				$valorAct = $valorAct.', '.$nivelProteccion[$i]->{'resvalor'};

			}
	
		}
		$nivelProteccionF = trim($valorAct, ',');

		$macrodistrito_i = $cas_datos->{'PTR_MACRO'};
		$longitud =  sizeof($macrodistrito_i);
		$valorAct = "";
		for($i = 1; $i < $longitud; $i++){
			$estadoMacrodistrito_i = $macrodistrito_i[$i]->{'estado'};

			if( $estadoMacrodistrito_i == true){
				$valorAct = $valorAct.', '.$macrodistrito_i[$i]->{'resvalor'};

			}
	
		}
		$macrodistrito_iF = trim($valorAct, ',');

		$PTR_EXPR = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_EXPR'});
		//---------
		$PTR_ETIM = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_ETIM'});
		$PTR_CARACT = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_CARACT'});
		$PTR_CONTEX = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_CONTEX'});
		$PTR_NARR_DEN = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_NARR_DEN'});
		$PTR_DAT_HIS = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_DAT_HIS'});
		$PTR_SIG_INTER = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_SIG_INTER'});
		$PTR_DAT_COMP = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_DAT_COMP'});
		$PTR_OTROS = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_OTROS'});
		//--------
		$PTR_DEN = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_DEN'});
		$PTR_PERIODO = $cas_datos->{'PTR_PERIODO'};
		$PTR_PROT_LEGAL = $cas_datos->{'PTR_PROT_LEGAL'};
		$PTR_INSTR_PRLEGAL = $cas_datos->{'PTR_INSTR_PRLEGAL'};

		//dd($PTR_NARR_DEN);

		$html.= '
		<div style = "font-size:12px;">
		<b>DATOS ORALES</b><br><br>	
		<b>1. EXPRESIÓN CULTURAL</b><br>
		<p>'.$PTR_EXPR.'</p>
		<b>2. DENOMINACION</b>
		<p>'.$PTR_DEN.'</p></p>
		<b>3. ETIMOLOGIA</b><br>
		'.$PTR_ETIM.'
		<b>4. EPOCA O AÑO DE ORIGEN</b>
		<p>'.$PTR_PERIODO.'</p>
		<b>5. LOCALIZACION</b>
		<p>LA PAZ - MURILLO ,'.$macrodistrito_iF.'</p>
		<b>6. CARACTERIZACIÓN DE LA EXPRESÓN CULTURAL</b><br>
		'.$PTR_CARACT.'
		<b>7. CONTEXTO SOCIO - CULTURAL</b><br><br>
		'.$PTR_CONTEX.'
		<b>8. NARRACIÓN DE LA DENOMINACIÓN</b><br>
		'.$PTR_NARR_DEN.'
		<b>9. DATOS HISTORICOS</b>
		'.$PTR_DAT_HIS.'<br>
		<b>10. SIGNIFICADO E INTERPRETACIÓN</b><br>
		'.$PTR_SIG_INTER.'
		<b>11. NIVEL DE PROTECCION LEGAL</b><br>	
		<p>'.$nivelProteccionF.'</p>
		<b>12. ÓRGANO EMISOR DE PROTECCIÓN LEGAL</b><br>
		<p>'.$PTR_PROT_LEGAL.'</p>
		<b>13. INSTRUMENTO DE PROTECCIÓN LEGAL</b>
		<p>'.$PTR_INSTR_PRLEGAL.'</p>

		'
		;
		$html.= '<b>14. IMAGENES</b><br>
			<table width="80%" id ="tdborde" align="center">';
				$longitudDoc=sizeof($datosDocumentos);
				if ($datosDocumentos == "[{ }]") {
					$longitudDoc = 0;
				}
				if ($longitudDoc == 1) {
			
				$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
					
				//dd($doc_url_logica);
				$html.='<tr>
				<td id="tdbordeArchFot" align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
				</tr>
				<tr>										
				<td width="50%" align="center" id="tdbordeArchFot">
					<b>14.1. DESCRIPCIÓN</b><br>
					'.$doc_datos.'
				</td>
				</tr>';
				}
				if ($longitudDoc == 2) {
			
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
				$doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
				$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};

				$html.='
				<tr>										
				<td id="tdbordeArchFot" align="center" width="50%" >
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>

				<td id="tdbordeArchFot" align="center" width="50%"  >

				
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				</tr>

				<tr>										
				<td width="50%" id="tdbordeArchFot" >
					<b>14.1. DESCRIPCIÓN</b><br>
					'.$doc_datos.'
				</td>
				<td id="tdbordeArchFot" width="50%">
					<b>14.2. DESCRIPCIÓN</b><br>
					'.$doc_datos1.'
				</td>
				</tr>';
				}
		$html.= '</table>';
							
		$html.='<br>
		<b>15. RESPONSABLE DE LA INFORMACIÓN</b>
		<p>'.$cas_datos->{'PTR_CREADOR'}.'</p>
		<b>16. DATOS COMPLEMENTARIOS</b><br><br>
		'.$PTR_DAT_COMP.'
		<b>17. FUENTES DE INFORMACIÓN</b>
		'.$PTR_OTROS.'

		</div>';
		}

  		if($TIPO_ARQ=='8'){

		$mpdf->SetHTMLHeader('
           <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>FICHA DE CATALOGACIÓN DE PATRIMONIO CULTURAL E INMATERIAL
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
			','ALL');
	
		$html.= '
			
			<div style="font-size:9px;">
			<h2>I. INFORMACION GENERAL</h2>
			<table width="100%" align = "center" cellpadding="2" cellspacing="2"  style="border-collapse:collapse;border: 0px solid ##A4A4A4;font-size:12px;">

			<tr><td id="trrrr"><b>Creador:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_CREADOR'}.'</td>

			</tr>
			</table><br><br>

			<h2>I. CONTEXTO ORIGINAL</h2>
	                <br>
	              	<table width="100%" align = "center" cellpadding="2" cellspacing="2"  style="border-collapse:collapse;border: 0px solid #696961;font-size:12px;">

			<tr>

			<td id="trrrr"><b>Bien cultural:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_DEN'}.'</td>

			</tr>

			<tr><td id="trrrr"><b>Tipo bien cultural:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_TB_CUL'}.'</td>

			</tr>
			<tr><td id="trrrr"><b>Categoria declaratoria:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_PROT_LEGAL'}.'</td>

			</tr>
			<tr><td id="trrrr"><b>Norma legal:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_NIV_PROTEC'}.'</td>

			</tr>
			<tr><td id="trrrr"><b>Numero Legal:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_NUM_PRLEGAL'}.'</td>

			</tr>
			<tr><td id="trrrr"><b>Fecha de programacion:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_FEC_PROG'}.'</td>

			</tr>
			<tr><td id="trrrr"><b>Palabras clave:</b></td>

			<td id="trrrr" width="70%">'.$cas_datos->{'PTR_PALABRA_CLAVE'}.'</td>

			</tr>

			</table>
						</div>

				';
		$html.='<br><h4>III. UBICACION</h4>
	                <table align="center" border="0" width="100%">';

	                $longitudDoc=sizeof($datosDocumentos);
	             	if ($datosDocumentos == "[{ }]") {
								$longitudDoc = 0;
							}
					if ($longitudDoc == 1) {
						
							$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
						    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
							//dd($doc_url_logica);
					$html.='<tr>
						<td align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
					</tr>';
					}
					if ($longitudDoc == 2) {
						
						    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
							$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
							$doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
							$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
					$html.='<tr>
					<td align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
					</tr>';
					$html.='<tr>
						<td align="center"><img src="'.$doc_url_logica1.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
					</tr>';
					}
		$html.='</table>';
	}

			$mpdf->AddPage();
			$mpdf->WriteHTML($html);   
			$mpdf->Output();				  
	}

public function pdfArchFoto($datos, $datosDocumentos)
	{
		  //dd($datosDocumentos);
		  $mpdf = new \Mpdf\Mpdf();
	      $mpdf->useOddEven = 1;
	      $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		  //-----------Documentos----------------------
	      $longitudDoc = 0;
		

		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter' , 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		 
    $mpdf->SetHTMLHeader('
     <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
    <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
            <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                <tr align="center">
                  <td >                               
		            <b>ARCHIVO FOTOGRÁFICO DE PATRIMONIO INMATERIAL
		           </td>
		           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
		           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
		           </td> 
                </tr>                
            </table>
    </div>
	','ALL');
	$mpdf->SetHTMLFooter('
		<table width="100%" style="font-size:12px;" >
			<tr>
			<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
			<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
			</tr>
		</table>');

	$html = '  
		<style>
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:11px;
	    font-family: Arial;

	    }
		#tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	    font-family: Arial;

	    }
	    .img-responsive{
	    	width: 45%;
	    	
	    }
	    </style>
	      ';

 	$html.= '<div style="font-size:11px;">
		<table style="font-size:11px;" width="100%" >';
			$longitudDoc=sizeof($datosDocumentos);
			if ($datosDocumentos == "[{ }]") {
				$longitudDoc = 0;
			}
			 if ($longitudDoc == 1) {
		
			$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
		    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};							
			$html.='
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo.'</b><br>
			</tr>
			<tr>
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			</tr>

			';
		   }
		    if ($longitudDoc == 2) {
								
		    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
			
			$html.='
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo1.'</b><br>
			
			</td>
			</tr>
			<tr>
		  		<tr><td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%"  align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>

			';
		   }
		   if ($longitudDoc == 3) {
								
		    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
			
			$html.='
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo1.'</b><br>
			
			</td>
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>';
			$html.='
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo2.'</b><br>
			</td>								
			</tr>
			<tr>
		    <td width="50%" align="center"  id="tdbordeArchFot">
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			
			
			</tr>';

		   }
		   if ($longitudDoc == 4) {
								
		    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
			$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
		    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
			$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
			
			$html.='
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo1.'</b><br>
			
			</td>
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo2.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo3.'</b><br>
			
			</td>
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				
				<br><img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>
			
			
			</tr>';

		   }
		    if ($longitudDoc == 5) {
								
		    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
			$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
		    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
			$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
			$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
		    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
			$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
			
			$html.='
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo1.'</b><br>
			
			</td>
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo2.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo3.'</b><br>
			
			</td>
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>
			
			
			</tr>
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo4.'</b><br>
			</td>
				
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>
			<td width="50%" align="center">
				<br><b></b><br><br>
				<img src="" class="img-responsive">
			</td>
			
			
			</tr>
			';

		   }

		   	    if ($longitudDoc == 6) {
								
		    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
		    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
			$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
		    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
			$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
			$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
		    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
			$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
			$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
		    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
			$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
			$doc_correlativo5 = $datosDocumentos[5]->{'xdoc_correlativo'};
		    $doc_titulo5 = $datosDocumentos[5]->{'xdoc_palabras'};
			$doc_url_logica5 = $datosDocumentos[5]->{'xdoc_url_logica'};
			
			$html.='
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo1.'</b><br>
			
			</td>
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			
			</tr>
			<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo2.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo3.'</b><br>
			
			</td>
			</tr>
			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica2.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				
				<img src="'.$doc_url_logica3.'" class="img-responsive">
			</td>
			
			
			</tr>
				<tr>
			<td width="50%" align="center">
				<b>'.$doc_titulo4.'</b><br>
			</td>
				<td width="50%" align="center">
				<b>'.$doc_titulo5.'</b><br>
			
			</td>
			</tr>

			<tr>
		    <td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica4.'" class="img-responsive">
			</td>
			<td width="50%" align="center" id="tdbordeArchFot">
				<img src="'.$doc_url_logica5.'" class="img-responsive">
			</td>
			
			
			</tr>
			';

		   }

	$html.='       						
		</table>				
		</div>
		';

		$mpdf->AddPage();
		$mpdf->WriteHTML($html);   
		$mpdf->Output();
	}
	
public function pdfImprimirTodo($datos , $datosDocumentos ,$datosDocumentosF)
{
 //dd($datos);
  	  $mpdf = new \Mpdf\Mpdf();
      $mpdf->useOddEven = 1;
       $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
      $longitudEA = sizeof($datos);
	  $cas_id = $datos[0]->{'xcas_id'};
	  $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
			  
	  $cas_datos = $datos[0]->{'xcas_datos'};
	  $cas_datos = json_decode($cas_datos);
	  $g_tipo = $cas_datos->{'g_tipo'};
	  $TIPO_ARQ = $cas_datos->{'PTR_AREAS'};
        

	  $mpdf->SetHTMLFooter('
		<table width="100%" style="font-size:12px;" >
			<tr>
			<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
			<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
			</tr>
		</table>');

		$html = '
		<style>
		p{
		  	   text-align : justify;
		  	   padding-left: 1cm;
		 }

		  .img-responsive{
	    	width: 280px;
	    } 
	   
		#tdborde{
	   	border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;
	 

	    }
	    #tdbordeArchFot{
		   
		border: 1px solid #E0E0E0;
	    border-collapse: border-bottom-width;
	    padding:5;
	    text-align : justify;
	    font-size:12px;

	    }
	  
		</style>
		';


	  if($TIPO_ARQ=='1'){ 
		
		$html .= '<htmlpageheader name="myheaderFicha">
       <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>AMBITO III USOS SOCIALES, RITUALES Y ACTOS FESTIVOS
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		</htmlpageheader>';
		$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';

		$nivelProteccion = $cas_datos->{'PTR_NIV_PROTEC'};
		$longitud =  sizeof($nivelProteccion);
		$valorAct = "";
		for($i = 1; $i < $longitud; $i++){
			$estadoNivel = $nivelProteccion[$i]->{'estado'};

			if( $estadoNivel == true){
				$valorAct = $valorAct.', '.$nivelProteccion[$i]->{'resvalor'};

			}
	
		}
		$nivelProteccionF = trim($valorAct, ',');
		
		$macrodistrito_i = $cas_datos->{'PTR_MACRO'};
		$longitud =  sizeof($macrodistrito_i);
		$valorAct = "";
		for($i = 1; $i < $longitud; $i++){
			$estadoMacrodistrito_i = $macrodistrito_i[$i]->{'estado'};

			if( $estadoMacrodistrito_i == true){
				$valorAct = $valorAct.', '.$macrodistrito_i[$i]->{'resvalor'};

			}
	
		}
		$macrodistrito_iF = trim($valorAct, ',');


		$PTR_EXPR = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_EXPR'});
		$PTR_ETIM = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_ETIM'});
		$PTR_CARACT = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_CARACT'});
		$PTR_CONTEX = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_CONTEX'});
		$PTR_NARR_DEN = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_NARR_DEN'});
		$PTR_DAT_HIS = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_DAT_HIS'});
		$PTR_SIG_INTER = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_SIG_INTER'});
		$PTR_DAT_COMP = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_DAT_COMP'});
		$PTR_OTROS = $this->caracteresEspecialesActualizar($cas_datos->{'PTR_OTROS'});

		$html.= '
		<div style = "font-size:12px;"><br><br><br><br><br><br><br>
		<b>DATOS ORALES</b><br><br>	
		<b>1. EXPRESIÓN CULTURAL</b><br>
		'.$PTR_EXPR.'
		<b>2. DENOMINACION</b>
		<p>'.$cas_datos->{'PTR_DEN'}.'</p></p>

		<b>3. ETIMOLOGIA</b><br><br>
		'.$PTR_ETIM.'
		<b>4. EPOCA O AÑO DE ORIGEN</b>
		<p>'.$cas_datos->{'PTR_PERIODO'}.'</p>
		<b>5. LOCALIZACION</b>
		<p>LA PAZ - MURILLO ,'.$macrodistrito_iF.'</p>
		<b>6. CARACTERIZACIÓN DE LA EXPRESÓN CULTURAL</b><br>
		'.$PTR_CARACT.'
		<b>7. CONTEXTO SOCIO - CULTURAL</b><br><br>
		'.$PTR_CONTEX.'
		<b>8. NARRACIÓN DE LA DENOMINACIÓN</b><br>
		'.$PTR_NARR_DEN.'
		<b>9. DATOS HISTORICOS</b>
		'.$PTR_DAT_HIS.'<br>
		<b>10. SIGNIFICADO E INTERPRETACIÓN</b><br>
		'.$PTR_SIG_INTER.'
		<b>11. NIVEL DE PROTECCION LEGAL</b><br>	
		<p>'.$nivelProteccionF.'</p>
		<b>12. ÓRGANO EMISOR DE PROTECCIÓN LEGAL</b><br>
		<p>'.$cas_datos->{'PTR_PROT_LEGAL'}.'</p>
		<b>13. INSTRUMENTO DE PROTECCIÓN LEGAL</b>
		<p>'.$cas_datos->{'PTR_INSTR_PRLEGAL'}.'</p>

		'
		;
		$html.= '<b>14. IMAGENES</b><br>
		<table width="80%" id ="tdborde" align="center">';
			$longitudDoc=sizeof($datosDocumentos);
			if ($datosDocumentos == "[{ }]") {
				$longitudDoc = 0;
			}
			if ($longitudDoc == 1) {
		
			$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
		    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
			$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
				
			//dd($doc_url_logica);
			$html.='<tr>
			<td id="tdbordeArchFot" align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
			</tr>
			<tr>										
			<td width="50%" align="center" id="tdbordeArchFot">
				<b>14.1. DESCRIPCIÓN</b><br>
				'.$doc_datos.'
			</td>
			</tr>';
			}
			if ($longitudDoc == 2) {
		
		    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
			$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
			$doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
			$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
			$doc_datos = $datosDocumentos[0]->{'xdoc_datos'};
			$doc_datos1 = $datosDocumentos[1]->{'xdoc_datos'};

			$html.='
			<tr>										
			<td id="tdbordeArchFot" align="center" width="50%" >
				<img src="'.$doc_url_logica.'" class="img-responsive">
			</td>

			<td id="tdbordeArchFot" align="center" width="50%"  >

			
				<img src="'.$doc_url_logica1.'" class="img-responsive">
			</td>
			</tr>

			<tr>										
			<td width="50%" id="tdbordeArchFot" >
				<b>14.1. DESCRIPCIÓN</b><br>
				'.$doc_datos.'
			</td>
			<td id="tdbordeArchFot" width="50%">
				<b>14.2. DESCRIPCIÓN</b><br>
				'.$doc_datos1.'
			</td>
			</tr>';
		}
		$html.= '</table>';
							
		$html.='<br>
		<b>15. RESPONSABLE DE LA INFORMACIÓN</b>
		<p>'.$cas_datos->{'PTR_CREADOR'}.'</p>
		<b>16. DATOS COMPLEMENTARIOS</b><br><br>
		'.$PTR_DAT_COMP.'
		<b>17. FUENTES DE INFORMACIÓN</b>
		'.$PTR_OTROS.'

					</div>';
		}

  if($TIPO_ARQ=='8'){

	$html .= '<htmlpageheader name="myheaderFicha">
   <img  src="img/encabezado_inmaterial.jpeg" style="border: 1px solid #a3c2c2;">           
    <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
            <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                <tr align="center">
                  <td >                               
		            <b>FICHA DE CATALOGACIÓN DE PATRIMONIO CULTURAL E INMATERIAL
		           </td>
		           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
		           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
		           </td> 
                </tr>                
            </table>
    </div>
	</htmlpageheader>';
	$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';
		
	
	$html.= '<div style="font-size:9px;">
			<br><br><br><br><br><br><br><br><br><br>	
		<h2>I. INFORMACION GENERAL</h2>
		<table width="100%" align = "center" cellpadding="2" cellspacing="2"  style="border-collapse:collapse;border: 0px solid ##A4A4A4;font-size:12px;">

		<tr><td id="trrrr"><b>Creador:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_CREADOR'}.'</td>

		</tr>
		</table><br><br>

		<h2>I. CONTEXTO ORIGINAL</h2>
            <br>
          	<table width="100%" align = "center" cellpadding="2" cellspacing="2"  style="border-collapse:collapse;border: 0px solid #696961;font-size:12px;">

		<tr>

		<td id="trrrr"><b>Bien cultural:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_DEN'}.'</td>

		</tr>

		<tr><td id="trrrr"><b>Tipo bien cultural:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_TB_CUL'}.'</td>

		</tr>
		<tr><td id="trrrr"><b>Categoria declaratoria:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_PROT_LEGAL'}.'</td>

		</tr>
		<tr><td id="trrrr"><b>Norma legal:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_NIV_PROTEC'}.'</td>

		</tr>
		<tr><td id="trrrr"><b>Numero Legal:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_NUM_PRLEGAL'}.'</td>

		</tr>
		<tr><td id="trrrr"><b>Fecha de programacion:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_FEC_PROG'}.'</td>

		</tr>
		<tr><td id="trrrr"><b>Palabras clave:</b></td>

		<td id="trrrr" width="70%">'.$cas_datos->{'PTR_PALABRA_CLAVE'}.'</td>

		</tr>

		</table>
			</div>

		';
	$html.='<br><h4>III. UBICACION</h4>
            <table align="center" border="0" width="100%">';

            $longitudDoc=sizeof($datosDocumentos);
         	if ($datosDocumentos == "[{ }]") {
						$longitudDoc = 0;
					}
			if ($longitudDoc == 1) {
				
					$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
				    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
				    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
					$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
					//dd($doc_url_logica);
			$html.='<tr>
				<td align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
			</tr>';
			}
			if ($longitudDoc == 2) {
				
				    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
					$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};	
					$doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
					$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
			$html.='<tr>
			<td align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
			</tr>';
			$html.='<tr>
				<td align="center"><img src="'.$doc_url_logica1.'" style="border: 1px solid #a3c2c2;" width="50%" ></td>
			</tr>';
			}
	$html.='</table>';
}

//--------------------------------------------------------------------------------------------------
	$html2 = ' 
			<style>
			#tdborde{
				border: 1px solid #E0E0E0;
				border-collapse: border-bottom-width;
				padding:5;
				text-align : justify;
				font-size:11px;
				font-family: Arial;

			}
			#tdbordeArchFot{

			border: 1px solid #E0E0E0;
			border-collapse: border-bottom-width;
			padding:5;
			text-align : justify;
			font-size:11px;
			font-family: Arial;

				}
				.img-responsive{
					width: 320px;			
				}
				
			</style>
			';

	$html2 .= '<htmlpageheader name="myheaderFotografico">
	<img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	<div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	        <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	            <tr align="center">
	              <td >                               
		            <b>ARCHIVO FOTOGRÁFICO DE PATRIMONIO INMATERIAL 
		           </td>
		           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
		           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
		           </td> 
	            </tr>                
	        </table>
	</div>
	</htmlpageheader>';

	$html2 .= '<sethtmlpageheader name="myheaderFotografico" value="on" show-this-page="1" />';

	$html2.= '
		<div style="font-size:11px;">
			<table style="font-size:11px;" width="100%" >';
				$longitudDoc=sizeof($datosDocumentosF);
				if ($datosDocumentosF == "[{ }]") {
					$longitudDoc = 0;
				}
				 if ($longitudDoc == 1) {
			
				$doc_idd = $datosDocumentosF[0]->{'xdoc_idd'};
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};							
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</tr>
				<tr>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				</tr>
				';
			   }
			    if ($longitudDoc == 2) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			  		<tr><td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%"  align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>

				';
			   }
			   if ($longitudDoc == 3) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>';
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>								
				</tr>
				<tr>
			    <td width="50%" align="center"  id="tdbordeArchFot">
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				
				
				</tr>';

			   }
			   if ($longitudDoc == 4) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<br><img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>';

			   }
			    if ($longitudDoc == 5) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
				$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
			    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
				$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
				
	$html2.='
		<tr>
		<td width="50%" align="center">
			<b>'.$doc_titulo.'</b><br>
		</td>
			<td width="50%" align="center">
			<b>'.$doc_titulo1.'</b><br>
		
		</td>
		</tr>
		<tr>
	    <td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica.'" class="img-responsive">
		</td>
		<td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica1.'" class="img-responsive">
		</td>
		
		</tr>
		<tr>
		<td width="50%" align="center">
			<b>'.$doc_titulo2.'</b><br>
		</td>
			<td width="50%" align="center">
			<b>'.$doc_titulo3.'</b><br>
		
		</td>
		</tr>
		<tr>
	    <td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica2.'" class="img-responsive">
		</td>
		<td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica3.'" class="img-responsive">
		</td>
		
		
		</tr>
		<tr>
		<td width="50%" align="center">
			<b>'.$doc_titulo4.'</b><br>
		</td>
			
		</tr>
		<tr>
	    <td width="50%" align="center" id="tdbordeArchFot">
			<img src="'.$doc_url_logica4.'" class="img-responsive">
		</td>
		<td width="50%" align="center">
			<br><b></b><br><br>
			<img src="" class="img-responsive">
		</td>
		
		
		</tr>
		';

		 }

	if ($longitudDoc == 6) {
									
	    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
	    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
		$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
		$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
	    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
		$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
		$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
	    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
		$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
		$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
	    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
		$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
		$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
	    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
		$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
		$doc_correlativo5 = $datosDocumentosF[5]->{'xdoc_correlativo'};
	    $doc_titulo5 = $datosDocumentosF[5]->{'xdoc_palabras'};
		$doc_url_logica5 = $datosDocumentosF[5]->{'xdoc_url_logica'};
				
	$html2.='
		<tr>
		<td width="50%" align="center">
			<b>'.$doc_titulo.'</b><br>
		</td>
			<td width="50%" align="center">
			<b>'.$doc_titulo1.'</b><br>
		
		</td>
		</tr>
		<tr>
	    <td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica.'" class="img-responsive">
		</td>
		<td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica1.'" class="img-responsive">
		</td>
		
		</tr>
		<tr>
		<td width="50%" align="center">
			<b>'.$doc_titulo2.'</b><br>
		</td>
			<td width="50%" align="center">
			<b>'.$doc_titulo3.'</b><br>
		
		</td>
		</tr>
		<tr>
	    <td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica2.'" class="img-responsive">
		</td>
		<td width="50%" align="center" id="tdbordeArchFot">
			
			<img src="'.$doc_url_logica3.'" class="img-responsive">
		</td>
		
		
		</tr>
			<tr>
		<td width="50%" align="center">
			<b>'.$doc_titulo4.'</b><br>
		</td>
			<td width="50%" align="center">
			<b>'.$doc_titulo5.'</b><br>
		
		</td>
		</tr>

		<tr>
	    <td width="50%" align="center" id="tdbordeArchFot">
			<img src="'.$doc_url_logica4.'" class="img-responsive">
		</td>
		<td width="50%" align="center" id="tdbordeArchFot">
			<img src="'.$doc_url_logica5.'" class="img-responsive">
		</td>
		
		
		</tr>
	';

	}

	$html2.='       						
		</table>				
		</div>
		';

	$mpdf->AddPage();
	$mpdf->WriteHTML($html);   
	$mpdf->AddPage();
	$mpdf->WriteHTML($html2);   
	$mpdf->Output();

   }
}
