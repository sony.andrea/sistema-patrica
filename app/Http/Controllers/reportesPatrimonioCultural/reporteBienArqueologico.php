<?php

namespace gamlp\Http\Controllers\reportesPatrimonioCultural;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;

class reporteBienArqueologico extends Controller
{
	public function getGenerar(Request $request)
	{  
		$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
	   //dd($g_tipo);
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
      // dd($datos);
		return $this->pdf($datos, $datosDocumentos);
	}
	public function getGenerarArchFoto(Request $request)
	{
		$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};       
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
       //dd($datosDocumentos);
		return $this->pdfArchFoto($datos, $datosDocumentos);
	}

     //---------------------------------------------------------------------
    //---------------------------------------------------------------------
     public function getGenerarFichaTodo(Request $request)
	{ 	$id_casos = $request->get('datos');
		$datos = $this->obtieneDatosPatrimonioCulturalCasos(intval($id_casos));
		$datos = json_decode($datos);                                                           
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nro_caso = $datos[0]->{'xcas_nro_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
		////obtiene imagenes fichas de CATALOGACIÓN
		$datosDocumentos = $this->obtieneDocumentosPatrimonioCulturalCasos(intval($cas_nro_caso), $g_tipo);
		$datosDocumentos = json_decode($datosDocumentos);
		////obtiene imagenes ficha fotografica
		$datosDocumentosF = $this->obtieneDocumentosPatrimonioCulturalCasosArchFoto(intval($cas_nro_caso), $g_tipo);
		$datosDocumentosF = json_decode($datosDocumentosF);
		return $this->pdfImprimirTodo($datos, $datosDocumentos,$datosDocumentosF);		
	}
    //------------------------------------------------------------

	public function obtieneDocumentosPatrimonioCulturalCasos($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.' ,"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-939","parametros":'.$parametros.'}';
		$options = array(
			'http' => array(
				'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public function obtieneDocumentosPatrimonioCulturalCasosArchFoto($id_casos, $g_tipo)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_doccasos":'.$id_casos.',"xg_tipo":"'.$g_tipo.'"}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-944","parametros":'.$parametros.'}';
		$options = array(
			'http' => array(
				'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public static function getToken()
	{
		$user = 'administrador';
		$pass = '123456';
		$url  = 'http://172.19.161.3/api/apiLogin';
		$data = '{"usr_usuario":"'.$user.'", "usr_clave":'.$pass.'}';
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/json",
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		$tokenArray=explode('"', $result);
		return $tokenArray[3];
	}

	public function obtieneDatosPatrimonioCulturalCasos($id_casos)
	{	
		$token = $this->getToken();
		$url  = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
		$parametros = '{"xid_casos":'.$id_casos.'}';
		$parametros = json_encode($parametros);
		$data = '{"identificador":"SERVICIO_PATRIMONIO-935","parametros":'.$parametros.'}';
		$options = array(
			'http' => array(
				'header'  => array("Content-type: application/json","Authorization: Bearer " .$token),
				'method'  => 'POST',
				'content' => $data
				)
			);
		$context  = stream_context_create($options);
		$result   = file_get_contents($url, false, $context);
		if ($result === FALSE) {
			dd('error request');
		}
		return $result;
	}
	public function caracteresEspecialesActualizar($data) {

		        $data = str_replace("&039;", "'", $data);

				$data = str_replace("&040;", "\"",$data);
				 //dd($data);
				$data = str_replace("&041;", "<", $data);
				$data = str_replace("&042;", ">", $data);
				$data = str_replace("&043;", "\\",$data);
				$data = str_replace("&044;", "{", $data);
				$data = str_replace("&045;", "}", $data);
				$data = str_replace("&046;", "[", $data);
				$data = str_replace("&047;", "]", $data);
				$data = str_replace("&048;", "(", $data);
				$data = str_replace("&049;", ")", $data);
				$data = str_replace("&050;", "/", $data);
				$data = str_replace("&051;", ":", $data);
				$data = str_replace("&052", " ", $data);
				 //dd($data);
		        return $data;
		}

	public function pdf($datos, $datosDocumentos)
	{
	  	 //dd($datosDocumentos);
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->useOddEven = 1;
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);

		$longitudEA = sizeof($datos);
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
		$TIPO_ARQ = $cas_datos->{'TIPO_ARQ'};
		

		if($TIPO_ARQ=='1'){

			$PTR_CREADOR = $cas_datos->{'PTR_CREA'};
			$PTR_PROVIENE_C = $cas_datos->{'PTR_PROVIENE_C'};
			$PTR_RELACION = $cas_datos->{'PTR_RELACION'};
			$PTR_UBIC_ACT = $cas_datos->{'PTR_UBIC_ACT'};
			$PTR_DESC_BIEN = $cas_datos->{'PTR_DESC_BIEN'};
			$PTR_DESC_BIEN = $this->caracteresEspecialesActualizar($PTR_DESC_BIEN);
			$PTR_MATERIAL = $cas_datos->{'PTR_MATERIAL'};
			$PTR_MATERIAL = $this->caracteresEspecialesActualizar($PTR_MATERIAL);
			$PTR_TECN = $cas_datos->{'PTR_TECN'};
			$PTR_TECN = $this->caracteresEspecialesActualizar($PTR_TECN);
			$PTR_MANF = $cas_datos->{'PTR_MANF'};
			$PTR_MANF = $this->caracteresEspecialesActualizar($PTR_MANF);
			$PTR_DECOR = $cas_datos->{'PTR_DECOR'};
			$PTR_DECOR = $this->caracteresEspecialesActualizar($PTR_DECOR);
			$PTR_ATRIB = $cas_datos->{'PTR_ATRIB'};
			$PTR_ATRIB = $this->caracteresEspecialesActualizar($PTR_ATRIB);
			$PTR_EST_CONS = $cas_datos->{'PTR_EST_CONS'};
			$PTR_DESC_ESTADO = $cas_datos->{'PTR_DESC_ESTADO'};
			$PTR_DESC_ESTADO = $this->caracteresEspecialesActualizar($PTR_DESC_ESTADO);
			$PTR_OBS = $cas_datos->{'PTR_OBS'};
			$PTR_OBS = $this->caracteresEspecialesActualizar($PTR_OBS);
			$PTR_CRONO = $cas_datos->{'PTR_CRONO'};
			$PTR_FUNC = $cas_datos->{'PTR_FUNC'};
			$PTR_FUNC = $this->caracteresEspecialesActualizar($PTR_FUNC);

		
		$mpdf->SetHTMLHeader('
       <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE CATALOGACIÓN DE BIENES MUEBLES ARQUEOLÓGICOS
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		','ALL');

		$mpdf->SetHTMLFooter('
		<table width="100%" style="font-size:12px;" >
			<tr>
			<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
			<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
			</tr>
		</table>');
		
		$html = '
		<style>
		#tablaborde{
			border: 1px solid #E0E0E0;
		    border-collapse: border-bottom-width;
		    padding:5;
		    text-align : justify;
		    font-size:11px;
		    font-family: Arial;
		}
		#tdborde{
			border: 1px solid #E0E0E0;
		    border-collapse: border-bottom-width;
		    padding:5;
		    text-align : justify;
		    font-size:12px;
		    font-family: Arial;
		}
		.img-responsive{
	    	width: 320px;	
	    } 
		</style>';

		$html.= '
		
		<div>

			<h4>I. INFORMACION GENERAL</h4>
			<table width="100%" id="tablaborde">
				<tr><td id="tdborde"><b>Creador:</b></td>

					<td id="tdborde" width="70%">'.$PTR_CREADOR.'</td>

				</tr>
			</table><br>

			<h4>II. CONTEXTO ORIGINAL</h4>
			<table width="100%" id="tablaborde" >

				<tr>

					<td id="tdborde"><b>Proveniencia del bien Mueble:</b></td>

					<td id="tdborde" width="70%">'.$PTR_PROVIENE_C.'</td>

				</tr>

				<tr><td id="tdborde"><b>Relación de la Pieza con el Yacimiento:</b></td>

					<td id="tdborde" width="70%">'.$PTR_RELACION.'</td>

				</tr>
				<tr><td id="tdborde"><b>Ubicacion Actual:</b></td>

					<td id="tdborde" width="70%">'.$PTR_UBIC_ACT.'</td>

				</tr>
				<tr><td id="tdborde"><b>Descripción del bien Mueble:</b></td>

					<td id="tdborde" width="70%">'.$PTR_DESC_BIEN.'</td>

				</tr>
				<tr><td id="tdborde"><b>Material:</b></td>

					<td id="tdborde" width="70%">'.$PTR_MATERIAL.'</td>

				</tr>
				<tr><td id="tdborde"><b>Técnica:</b></td>

					<td id="tdborde" width="70%">'.$PTR_TECN.'</td>

				</tr>
				<tr><td id="tdborde"><b>Manufactura:</b></td>

					<td id="tdborde" width="70%">'.$PTR_MANF.'</td>

				</tr>
				<tr><td id="tdborde"><b>Decoración:</b></td>

					<td id="tdborde" width="70%">'.$PTR_DECOR.'</td>

				</tr>
				<tr><td id="tdborde"><b>Atributos:</b></td>

					<td id="tdborde" width="70%">'.$PTR_ATRIB.'</td>

				</tr>
				<tr><td id="tdborde"><b>Estado de Conservación:</b></td>

					<td id="tdborde" width="70%">'.$PTR_EST_CONS.'</td>

				</tr>
				<tr><td id="tdborde"><b>Descripción del Estado:</b></td>

					<td id="tdborde" width="70%">'.$PTR_DESC_ESTADO.'</td>

				</tr>
				<tr><td id="tdborde"><b>Observaciones:</b></td>

					<td id="tdborde" width="70%">'.$PTR_OBS.'</td>

				</tr>
				<tr><td id="tdborde"><b>Cronología:</b></td>

					<td id="tdborde" width="70%">'.$PTR_CRONO.'</td>

				</tr>
				<tr><td id="tdborde"><b>Función:</b></td>

					<td id="tdborde" width="70%">'.$PTR_FUNC.'</td>

				</tr>

			</table>
		</div>
		';
		}

		if($TIPO_ARQ=='2'){

		$PTR_CREADOR = $cas_datos->{'PTR_CREA'};
		$PTR_DEN = $cas_datos->{'PTR_DEN'};
		$PTR_MUNI = $cas_datos->{'PTR_MUNI'};
		$PTR_MACRO = $cas_datos->{'PTR_MACRO'};
		$PTR_LUG = $cas_datos->{'PTR_LUG'};
		$PTR_IA_ELEM_DESC = $cas_datos->{'PTR_IA_ELEM_DESC'};
		$PTR_IA_ELEM_DESC = $this->caracteresEspecialesActualizar($PTR_IA_ELEM_DESC);
		$PTR_IA_EST_CONS = $cas_datos->{'PTR_IA_EST_CONS'};
		$PTR_IA_POLIG = $cas_datos->{'PTR_IA_POLIG'};
		$PTR_IA_POLIG = $this->caracteresEspecialesActualizar($PTR_IA_POLIG);
		$PTR_IA_DESC_EST_CONS = $cas_datos->{'PTR_IA_DESC_EST_CONS'};
		$PTR_IA_DESC_EST_CONS = $this->caracteresEspecialesActualizar($PTR_IA_DESC_EST_CONS);
		$PTR_IA_OBS_EST = $cas_datos->{'PTR_IA_OBS_EST'};
		$PTR_IA_OBS_EST = $this->caracteresEspecialesActualizar($PTR_IA_OBS_EST);
		$PTR_IA_TAFON = $cas_datos->{'PTR_IA_TAFON'};
		$PTR_IA_DESC_RUR = $cas_datos->{'PTR_IA_DESC_RUR'};
		$PTR_IA_DESC_RUR = $this->caracteresEspecialesActualizar($PTR_IA_DESC_RUR);
		$PTR_IA_DESC_URB = $cas_datos->{'PTR_IA_DESC_URB'};
		$PTR_IA_DESC_URB = $this->caracteresEspecialesActualizar($PTR_IA_DESC_URB);
		$PTR_IA_TAFON_DESC = $cas_datos->{'PTR_IA_TAFON_DESC'};
		$PTR_IA_TAFON_DESC = $this->caracteresEspecialesActualizar($PTR_IA_TAFON_DESC);
		$PTR_IA_YACIM = $cas_datos->{'PTR_IA_YACIM'};
		$PTR_IA_ELEM_MUEBLE = $cas_datos->{'PTR_IA_ELEM_MUEBLE'};
		$PTR_IA_DESC = $cas_datos->{'PTR_IA_DESC'};
		$PTR_IA_DESC = $this->caracteresEspecialesActualizar($PTR_IA_DESC);
		$PTR_IA_ELEM_MUEB_DESC = $cas_datos->{'PTR_IA_ELEM_MUEB_DESC'};
		$PTR_IA_ELEM_MUEB_DESC = $this->caracteresEspecialesActualizar($PTR_IA_ELEM_MUEB_DESC);

		
		$mpdf->SetHTMLHeader('
       <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
                    <tr align="center">
                      <td >                               
			            <b>FICHA DE CATALOGACIÓN DE SITIOS ARQUEOLÓGICOS
			           </td>
			           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
			           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
			           </td> 
                    </tr>                
                </table>
        </div>
		','ALL');


		$mpdf->SetHTMLFooter('
		<table width="100%" style="font-size:12px;" >
			<tr>
			<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
			<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
			</tr>
		</table>');

		$html = '
		<style>
		#tablaborde{
			border: 1px solid #696961;
		    border-collapse: border-bottom-width;
		    border-spacing: 3px;
		    padding:5;
		    text-align : justify;
		    font-size:11px;     
		}
		#tdborde{
			border-collapse:collapse;
			border: 1px solid #A4A4A4;
			padding:3px;
		}
		</style>';


		$html.= '
		
		<div style="font-size:9px;">
			<h2>INFORMACION GENERAL</h2>

			<table width="100%" id="tablaborde">

				<tr><td id="tdborde"><b>Denominación:</b></td>
					<td id="tdborde" width="70%">'.$PTR_DEN.'</td></tr>

				<tr><td id="tdborde"><b>Creador:</b></td>
						<td id="tdborde" width="70%">'.$PTR_CREADOR.'</td></tr>
					</table>
					<br><h2>UBICACIÓN GEOGRAFICA UTM</h2>

			<table width="100%" id="tablaborde">

				<tr><td id="tdborde"><b>Punto de ubicación en el Municipio:</b></td>

					<td id="tdborde" width="70%">'.$PTR_MUNI.'</td>

				</tr>
				<tr><td id="tdborde"><b>Ubicación Macro-distrito:</b></td>

					<td id="tdborde" width="70%">'.$PTR_MACRO.'</td>

				</tr>
				<tr><td id="tdborde"><b>Ubicación local:</b></td>

					<td id="tdborde" width="70%">'.$PTR_LUG.'</td>

				</tr>

				<tr><td id="tdborde"><b>Ubicación especifica del Polígono:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_POLIG.'</td>

				</tr></table>
			<br><h2>RELACIÓN DEL SITIO O YACIMIENTO CON OTRAS ÁREAS</h2>

			<table width="100%" id="tablaborde">
				<tr><td><b>Descripción del contexto inmediato en area Rural:</b></td>

					<td><b>Descripción del contexto inmediato en area Urbana:</b></td>

				</tr>
				<tr><td id="tdborde">'.$PTR_IA_DESC_RUR.'</td>

					<td id="tdborde">'.$PTR_IA_DESC_URB.'</td>

				</tr>
			</table>
			<br><h2>UBICACIÓN GEOMORFOLÓGICA</h2>
			<table width="100%" id="tablaborde">
				<tr><td id="tdborde"><b>Descripción general del Yacimiento:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_YACIM.'</td>

				</tr>
			</table>
			<h2>EVALUACIÓN SUPERFICIAL</h2>
			<br>
			<table width="100%" id="tablaborde">

				<tr><td id="tdborde"><b>Descripción elementos inmuebles:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_ELEM_DESC.'</td>

				</tr>
				<tr><td id="tdborde"><b>Estado de Conservación:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_EST_CONS.'</td>

				</tr>
				<tr><td id="tdborde"><b>Descripción del Estado:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_DESC_EST_CONS.'</td>

				</tr>
				<tr><td id="tdborde"><b>Observaciones:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_OBS_EST.'</td>

				</tr>
				<tr><td id="tdborde"><b>Tafonomia:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_TAFON.'</td>

				</tr>
				<tr><td id="tdborde"><b>Descripción Tafonomia:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_TAFON_DESC.'</td>

				</tr>

				<tr><td id="tdborde"><b>Presencia de elementos Muebles:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_ELEM_MUEBLE.'</td>

				</tr>
				<tr><td id="tdborde"><b>Descripción:</b></td>

					<td id="tdborde" width="70%">'.$PTR_IA_DESC.'</td>

				</tr>
			</table>

			</div>
		';
		}

		$html.='<br><h4>III. UBICACION</h4>
		<table align="center" border="0" width="100%">';
			$longitudDoc=sizeof($datosDocumentos);
			//dd($datosDocumentos);
			if ($datosDocumentos == "[{ }]") {
					$longitudDoc = 0;
				}
			if ($longitudDoc == 1) {
				$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
				$doc_id = $datosDocumentos[0]->{'xdoc_id'};
				$doc_nombre = $datosDocumentos[0]->{'xdoc_nombre'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};

				$html.='<tr>
				<td align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="80%" ></td>
			</tr>';
		}
		$html.='</table>';


		$mpdf->AddPage();
		$mpdf->WriteHTML($html);   
		$mpdf->Output();				  
		}

	public function pdfArchFoto($datos, $datosDocumentos)
	{
		  //dd($datosDocumentos);
	    $mpdf = new \Mpdf\Mpdf();
        $mpdf->useOddEven = 1;
        $cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};

		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		
		$TIPO_ARQ = $cas_datos->{'TIPO_ARQ'};

		  //-----------Documentos----------------------
	      $longitudDoc = 0;
		

		  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter' , 'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);
		  if($TIPO_ARQ=='1'){
		  $mpdf->SetHTMLHeader('
          		<img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
			        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
			                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
			                    <tr align="center">
			                      <td >                               
						            <b>ARCHIVO FOTOGRÁFICO DE BIENES MUEBLES ARQUEOLÓGICOS
						           </td>
						           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
						           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
						           </td> 
			                    </tr>                
			                </table>
			        </div>
			','ALL');
		}
		  if($TIPO_ARQ=='2'){
		   $mpdf->SetHTMLHeader('
          		<img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
			        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
			                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
			                    <tr align="center">
			                      <td >                               
						            <b>ARCHIVO FOTOGRÁFICO DE BIENES SITIOS ARQUEOLÓGICOS
						           </td>
						           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
						           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
						           </td> 
			                    </tr>                
			                </table>
			        </div>
			','ALL');	
		  }
 
		 $mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');
		$html = ' 

	        
			<style>
			#tdborde{
		   	border: 1px solid #E0E0E0;
		    border-collapse: border-bottom-width;
		    padding:5;
		    text-align : justify;
		    font-size:11px;
		    font-family: Arial;

		    }
			#tdbordeArchFot{
			   
			    	border: 1px solid #E0E0E0;
		    border-collapse: border-bottom-width;
		    padding:5;
		    text-align : justify;
		    font-size:12px;
		    font-family: Arial;

		    }
		    .img-responsive{
		    	width: 45%;
		    	
		    }
		    </style>
		      ';

		 $html.= '
		<div style="font-size:11px;">
			<table style="font-size:11px;" width="100%" >';
				$longitudDoc=sizeof($datosDocumentos);
				if ($datosDocumentos == "[{ }]") {
					$longitudDoc = 0;
				}
				 if ($longitudDoc == 1) {
			
				$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};							
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</tr>
				<tr>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				</tr>

				';
			   }
			    if ($longitudDoc == 2) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			  		<tr><td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%"  align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>

				';
			   }
			   if ($longitudDoc == 3) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>';
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>								
				</tr>
				<tr>
			    <td width="50%" align="center"  id="tdbordeArchFot">
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				
				
				</tr>';

			   }
			   if ($longitudDoc == 4) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<br><img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>';

			   }
			    if ($longitudDoc == 5) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
				$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
			    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
				$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo4.'</b><br>
				</td>
					
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica4.'" class="img-responsive">
				</td>
				<td width="50%" align="center">
					<br><b></b><br><br>
					<img src="" class="img-responsive">
				</td>
				
				
				</tr>
				';

			   }

			   	    if ($longitudDoc == 6) {
									
			    $doc_correlativo = $datosDocumentos[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentos[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentos[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentos[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentos[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentos[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentos[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentos[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentos[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentos[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentos[3]->{'xdoc_url_logica'};
				$doc_correlativo4 = $datosDocumentos[4]->{'xdoc_correlativo'};
			    $doc_titulo4 = $datosDocumentos[4]->{'xdoc_palabras'};
				$doc_url_logica4 = $datosDocumentos[4]->{'xdoc_url_logica'};
				$doc_correlativo5 = $datosDocumentos[5]->{'xdoc_correlativo'};
			    $doc_titulo5 = $datosDocumentos[5]->{'xdoc_palabras'};
				$doc_url_logica5 = $datosDocumentos[5]->{'xdoc_url_logica'};
				
				$html.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>
					<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo4.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo5.'</b><br>
				
				</td>
				</tr>

				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica4.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica5.'" class="img-responsive">
				</td>
				
				
				</tr>
				';

			   }

		$html.='       						
			</table>				
			</div>
    		';

		$mpdf->AddPage();
		$mpdf->WriteHTML($html);   
		$mpdf->Output();
	}
	public function pdfImprimirTodo($datos , $datosDocumentos ,$datosDocumentosF)
	{    //dd($datosDocumentos);
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->useOddEven = 1;
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'letter',  'setAutoTopMargin' => 'stretch', 'autoMarginPadding' => 4]);

		$longitudEA = sizeof($datos);
		$cas_id = $datos[0]->{'xcas_id'};
		$cas_nombre_caso = $datos[0]->{'xcas_nombre_caso'};
		$cas_datos = $datos[0]->{'xcas_datos'};
		$cas_datos = json_decode($cas_datos);
		$g_tipo = $cas_datos->{'g_tipo'};
		$TIPO_ARQ = $cas_datos->{'TIPO_ARQ'};

		if($TIPO_ARQ=='1'){
			$PTR_CREADOR = $cas_datos->{'PTR_CREA'};
			$PTR_PROVIENE_C = $cas_datos->{'PTR_PROVIENE_C'};
			$PTR_RELACION = $cas_datos->{'PTR_RELACION'};
			$PTR_UBIC_ACT = $cas_datos->{'PTR_UBIC_ACT'};
			$PTR_DESC_BIEN = $cas_datos->{'PTR_DESC_BIEN'};
			$PTR_DESC_BIEN = $this->caracteresEspecialesActualizar($PTR_DESC_BIEN);
			$PTR_MATERIAL = $cas_datos->{'PTR_MATERIAL'};
			$PTR_MATERIAL = $this->caracteresEspecialesActualizar($PTR_MATERIAL);
			$PTR_TECN = $cas_datos->{'PTR_TECN'};
			$PTR_TECN = $this->caracteresEspecialesActualizar($PTR_TECN);
			$PTR_MANF = $cas_datos->{'PTR_MANF'};
			$PTR_MANF = $this->caracteresEspecialesActualizar($PTR_MANF);
			$PTR_DECOR = $cas_datos->{'PTR_DECOR'};
			$PTR_DECOR = $this->caracteresEspecialesActualizar($PTR_DECOR);
			$PTR_ATRIB = $cas_datos->{'PTR_ATRIB'};
			$PTR_ATRIB = $this->caracteresEspecialesActualizar($PTR_ATRIB);
			$PTR_EST_CONS = $cas_datos->{'PTR_EST_CONS'};
			$PTR_DESC_ESTADO = $cas_datos->{'PTR_DESC_ESTADO'};
			$PTR_DESC_ESTADO = $this->caracteresEspecialesActualizar($PTR_DESC_ESTADO);
			$PTR_OBS = $cas_datos->{'PTR_OBS'};
			$PTR_OBS = $this->caracteresEspecialesActualizar($PTR_OBS);
			$PTR_CRONO = $cas_datos->{'PTR_CRONO'};
			$PTR_FUNC = $cas_datos->{'PTR_FUNC'};
			$PTR_FUNC = $this->caracteresEspecialesActualizar($PTR_FUNC);

		$html = '<htmlpageheader name="myheaderFicha">
	    	<img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>FICHA DE CATALOGACIÓN DE BIENES MUEBLES ARQUEOLÓGICOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
		</htmlpageheader>';
		$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';

		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:12px;" >
				<tr>
				<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
				<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
				</tr>
			</table>');
		$html .= '
			<style>
			#tablaborde{
				border: 1px solid #E0E0E0;
			    border-collapse: border-bottom-width;
			    padding:5;
			    text-align : justify;
			    font-size:11px;
			    font-family: Arial;
			}
			#tdborde{
				border: 1px solid #E0E0E0;
			    border-collapse: border-bottom-width;
			    padding:5;
			    text-align : justify;
			    font-size:12px;
			    font-family: Arial;
			}
			.img-responsive{
		    	width: 320px;	
		    } 
			</style>';
		$html .= '<br><br><br><br><br>
			<div>
				<h4>I. INFORMACION GENERAL</h4>
				<table width="100%" id="tablaborde">
					<tr><td id="tdborde"><b>Creador:</b></td>

						<td id="tdborde" width="70%">'.$PTR_CREADOR.'</td>
					</tr>
				</table><br>
				<h4>II. CONTEXTO ORIGINAL</h4>
				<table width="100%" id="tablaborde" >
					<tr>
						<td id="tdborde"><b>Proveniencia del bien Mueble:</b></td>
						<td id="tdborde" width="70%">'.$PTR_PROVIENE_C.'</td>
					</tr>
					<tr><td id="tdborde"><b>Relación de la Pieza con el Yacimiento:</b></td>
						<td id="tdborde" width="70%">'.$PTR_RELACION.'</td>
					</tr>
					<tr><td id="tdborde"><b>Ubicacion Actual:</b></td>
						<td id="tdborde" width="70%">'.$PTR_UBIC_ACT.'</td>
					</tr>
					<tr><td id="tdborde"><b>Descripción del bien Mueble:</b></td>
						<td id="tdborde" width="70%">'.$PTR_DESC_BIEN.'</td>
					</tr>
					<tr><td id="tdborde"><b>Material:</b></td>
						<td id="tdborde" width="70%">'.$PTR_MATERIAL.'</td>
					</tr>
					<tr><td id="tdborde"><b>Técnica:</b></td>
						<td id="tdborde" width="70%">'.$PTR_TECN.'</td>
					</tr>
					<tr><td id="tdborde"><b>Manufactura:</b></td>
						<td id="tdborde" width="70%">'.$PTR_MANF.'</td>
					</tr>
					<tr><td id="tdborde"><b>Decoración:</b></td>
						<td id="tdborde" width="70%">'.$PTR_DECOR.'</td>
					</tr>
					<tr><td id="tdborde"><b>Atributos:</b></td>
						<td id="tdborde" width="70%">'.$PTR_ATRIB.'</td>
					</tr>
					<tr><td id="tdborde"><b>Estado de Conservación:</b></td>
						<td id="tdborde" width="70%">'.$PTR_EST_CONS.'</td>
					</tr>
					<tr><td id="tdborde"><b>Descripción del Estado:</b></td>
						<td id="tdborde" width="70%">'.$PTR_DESC_ESTADO.'</td>
					</tr>
					<tr><td id="tdborde"><b>Observaciones:</b></td>
						<td id="tdborde" width="70%">'.$PTR_OBS.'</td>
					</tr>
					<tr><td id="tdborde"><b>Cronología:</b></td>
						<td id="tdborde" width="70%">'.$PTR_CRONO.'</td>
					</tr>
					<tr><td id="tdborde"><b>Función:</b></td>
						<td id="tdborde" width="70%">'.$PTR_FUNC.'</td>
					</tr>
				</table>
			</div>
			';
		}
		if($TIPO_ARQ=='2'){
			$PTR_CREADOR = $cas_datos->{'PTR_CREA'};
			$PTR_DEN = $cas_datos->{'PTR_DEN'};
			$PTR_MUNI = $cas_datos->{'PTR_MUNI'};
			$PTR_MACRO = $cas_datos->{'PTR_MACRO'};
			$PTR_LUG = $cas_datos->{'PTR_LUG'};
			$PTR_IA_ELEM_DESC = $cas_datos->{'PTR_IA_ELEM_DESC'};
			$PTR_IA_ELEM_DESC = $this->caracteresEspecialesActualizar($PTR_IA_ELEM_DESC);
			$PTR_IA_EST_CONS = $cas_datos->{'PTR_IA_EST_CONS'};
			$PTR_IA_POLIG = $cas_datos->{'PTR_IA_POLIG'};
			$PTR_IA_POLIG = $this->caracteresEspecialesActualizar($PTR_IA_POLIG);
			$PTR_IA_DESC_EST_CONS = $cas_datos->{'PTR_IA_DESC_EST_CONS'};
			$PTR_IA_DESC_EST_CONS = $this->caracteresEspecialesActualizar($PTR_IA_DESC_EST_CONS);
			$PTR_IA_OBS_EST = $cas_datos->{'PTR_IA_OBS_EST'};
			$PTR_IA_OBS_EST = $this->caracteresEspecialesActualizar($PTR_IA_OBS_EST);
			$PTR_IA_TAFON = $cas_datos->{'PTR_IA_TAFON'};
			$PTR_IA_DESC_RUR = $cas_datos->{'PTR_IA_DESC_RUR'};
			$PTR_IA_DESC_RUR = $this->caracteresEspecialesActualizar($PTR_IA_DESC_RUR);
			$PTR_IA_DESC_URB = $cas_datos->{'PTR_IA_DESC_URB'};
			$PTR_IA_DESC_URB = $this->caracteresEspecialesActualizar($PTR_IA_DESC_URB);
			$PTR_IA_TAFON_DESC = $cas_datos->{'PTR_IA_TAFON_DESC'};
			$PTR_IA_TAFON_DESC = $this->caracteresEspecialesActualizar($PTR_IA_TAFON_DESC);
			$PTR_IA_YACIM = $cas_datos->{'PTR_IA_YACIM'};
			$PTR_IA_ELEM_MUEBLE = $cas_datos->{'PTR_IA_ELEM_MUEBLE'};
			$PTR_IA_DESC = $cas_datos->{'PTR_IA_DESC'};
			$PTR_IA_DESC = $this->caracteresEspecialesActualizar($PTR_IA_DESC);
			$PTR_IA_ELEM_MUEB_DESC = $cas_datos->{'PTR_IA_ELEM_MUEB_DESC'};
			$PTR_IA_ELEM_MUEB_DESC = $this->caracteresEspecialesActualizar($PTR_IA_ELEM_MUEB_DESC);


		$html = '<htmlpageheader name="myheaderFicha">
	    	<img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
	        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
	                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
	                    <tr align="center">
	                      <td >                               
				            <b>FICHA DE CATALOGACIÓN DE SITIOS ARQUEOLÓGICOS
				           </td>
				           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
				           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
				           </td> 
	                    </tr>                
	                </table>
	        </div>
		</htmlpageheader>';
		$html .= '<sethtmlpageheader name="myheaderFicha" value="on" show-this-page="1" />';

		$mpdf->SetHTMLFooter('
		<table width="100%" style="font-size:12px;" >
			<tr>
			<td width="80%" align ="center" >Para cualquier consulta, por favor comunicarse al Teléfono. 2-2440746</td>
			<td width="20%" align="right">Página {PAGENO} de {nbpg}</td>
			</tr>
		</table>');

		$html .= '
			<style>
			#tablaborde{
				border: 1px solid #696961;
			    border-collapse: border-bottom-width;
			    border-spacing: 3px;
			    padding:5;
			    text-align : justify;
			    font-size:11px;     
			}
			#tdborde{
				border-collapse:collapse;
				border: 1px solid #A4A4A4;
				padding:3px;
			}
			</style>';
		$html .= '<br><br><br><br><br><br>
			<div style="font-size:9px;">
				<h2>INFORMACION GENERAL</h2>
				<table width="100%" id="tablaborde">
					<tr><td id="tdborde"><b>Denominación:</b></td>
						<td id="tdborde" width="70%">'.$PTR_DEN.'</td></tr>
						<tr><td id="tdborde"><b>Creador:</b></td>
							<td id="tdborde" width="70%">'.$PTR_CREADOR.'</td></tr>
						</table>
						<br><h2>UBICACIÓN GEOGRAFICA UTM</h2>
						<table width="100%" id="tablaborde">
							<tr><td id="tdborde"><b>Punto de ubicación en el Municipio:</b></td>
								<td id="tdborde" width="70%">'.$PTR_MUNI.'</td>
							</tr>
							<tr><td id="tdborde"><b>Ubicación Macro-distrito:</b></td>
								<td id="tdborde" width="70%">'.$PTR_MACRO.'</td>
							</tr>
							<tr><td id="tdborde"><b>Ubicación local:</b></td>
								<td id="tdborde" width="70%">'.$PTR_LUG.'</td>
							</tr>
							<tr><td id="tdborde"><b>Ubicación especifica del Polígono:</b></td>
								<td id="tdborde" width="70%">'.$PTR_IA_POLIG.'</td>
							</tr></table>
							<br><h2>RELACIÓN DEL SITIO O YACIMIENTO CON OTRAS ÁREAS</h2>
							<table width="100%" id="tablaborde">
								<tr><td><b>Descripción del contexto inmediato en area Rural:</b></td>
									<td><b>Descripción del contexto inmediato en area Urbana:</b></td>
								</tr>
								<tr><td id="tdborde">'.$PTR_IA_DESC_RUR.'</td>
									<td id="tdborde">'.$PTR_IA_DESC_URB.'</td>
								</tr>
							</table>
							<br><h2>UBICACIÓN GEOMORFOLÓGICA</h2>
							<table width="100%" id="tablaborde">
								<tr><td id="tdborde"><b>Descripción general del Yacimiento:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_YACIM.'</td>
								</tr>
							</table>
							<h2>EVALUACIÓN SUPERFICIAL</h2>
							<br>
							<table width="100%" id="tablaborde">
								<tr><td id="tdborde"><b>Descripción elementos inmuebles:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_ELEM_DESC.'</td>
								</tr>
								<tr><td id="tdborde"><b>Estado de Conservación:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_EST_CONS.'</td>
								</tr>
								<tr><td id="tdborde"><b>Descripción del Estado:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_DESC_EST_CONS.'</td>
								</tr>
								<tr><td id="tdborde"><b>Observaciones:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_OBS_EST.'</td>
								</tr>
								<tr><td id="tdborde"><b>Tafonomia:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_TAFON.'</td>
								</tr>
								<tr><td id="tdborde"><b>Descripción Tafonomia:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_TAFON_DESC.'</td>
								</tr>
								<tr><td id="tdborde"><b>Presencia de elementos Muebles:</b></td>
									<td id="tdborde" width="70%">'.$PTR_IA_ELEM_MUEBLE.'</td>
								</tr>
								<tr><td id="tdborde"><b>Descripción:</b></td>

									<td id="tdborde" width="70%">'.$PTR_IA_DESC.'</td>
								</tr>
							</table>
						</div>
						';
					}
		$html .= '<br><h4>III. UBICACION</h4>
			<table align="center" border="0" width="100%">';
				$longitudDoc=sizeof($datosDocumentos);
				//dd($datosDocumentos);
				if ($datosDocumentos == "[{ }]") {
						$longitudDoc = 0;
					}
				if ($longitudDoc == 1) {
					$doc_idd = $datosDocumentos[0]->{'xdoc_idd'};
					$doc_id = $datosDocumentos[0]->{'xdoc_id'};
					$doc_nombre = $datosDocumentos[0]->{'xdoc_nombre'};
					$doc_url_logica = $datosDocumentos[0]->{'xdoc_url_logica'};

		$html.='<tr>
					<td align="center"><img src="'.$doc_url_logica.'" style="border: 1px solid #a3c2c2;" width="80%" ></td>
				</tr>';
			}
		$html.='</table>';

//-------------------------------------------------
		$html2 = ' 
		<style>
		#tdborde{
			border: 1px solid #E0E0E0;
			border-collapse: border-bottom-width;
			padding:5;
			text-align : justify;
			font-size:11px;
			font-family: Arial;
			border-spacing: 3px;

		}
		#tdbordeArchFot{

			border: 1px solid #E0E0E0;
			border-collapse: border-bottom-width;
			padding:5;
			text-align : justify;
			font-size:11px;
			font-family: Arial;
			padding:3px;
		}
		.img-responsive{
			width: 50%;	
		}
		</style>
			';
		if($TIPO_ARQ=='1'){
		$html2 .='<htmlpageheader name="myheaderFotografico">
		            <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
			        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
			                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
			                    <tr align="center">
			                      <td >                               
						            <b>ARCHIVO FOTOGRÁFICO DE BIENES MUEBLES ARQUEOLÓGICOS
						           </td>
						           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
						           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
						           </td> 
			                    </tr>                
			                </table>
			        </div>
		 	</htmlpageheader>';
		 }
		if($TIPO_ARQ=='2'){
		$html2 .='<htmlpageheader name="myheaderFotografico">
		            <img  src="img/encabezado_material.jpeg" style="border: 1px solid #a3c2c2;">           
			        <div style="border: 0px solid #a3c2c2; background-color:#f0f5f5; height: 40px; ">
			                <table width="100%" style="font-size:12px;" cellpadding="6" cellspacing="6">
			                    <tr align="center">
			                      <td >                               
						            <b>ARCHIVO FOTOGRÁFICO DE SITIOS ARQUEOLÓGICOS
						           </td>
						           <td align="center" style="border-width: 1px;border: solid; border-color: #696969;">
						           <b> NRO.FICHA: </b>'.$cas_nombre_caso.'
						           </td> 
			                    </tr>                
			                </table>
			        </div>
		 	</htmlpageheader>';
		 }

		 $html2 .= '<sethtmlpageheader name="myheaderFotografico" value="on" show-this-page="1" />';
		 $html2 .= '
		<div style="font-size:11px;">
			<table style="font-size:11px;" width="100%" >';
				$longitudDoc=sizeof($datosDocumentosF);
				if ($datosDocumentosF == "[{ }]") {
					$longitudDoc = 0;
				}
				 if ($longitudDoc == 1) {
			
				$doc_idd = $datosDocumentosF[0]->{'xdoc_idd'};
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};							
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</tr>
				<tr>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				</tr>

				';
			   }
			    if ($longitudDoc == 2) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			  		<tr><td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%"  align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>

				';
			   }
			   if ($longitudDoc == 3) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>';
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>								
				</tr>
				<tr>
			    <td width="50%" align="center"  id="tdbordeArchFot">
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				
				
				</tr>';

			   }
			   if ($longitudDoc == 4) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<br><img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>';

			   }
			    if ($longitudDoc == 5) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
				$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
			    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
				$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo4.'</b><br>
				</td>
					
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica4.'" class="img-responsive">
				</td>
				<td width="50%" align="center">
					<br><b></b><br><br>
					<img src="" class="img-responsive">
				</td>
				
				
				</tr>
				';

			   }

			   	    if ($longitudDoc == 6) {
									
			    $doc_correlativo = $datosDocumentosF[0]->{'xdoc_correlativo'};
			    $doc_titulo = $datosDocumentosF[0]->{'xdoc_palabras'};
				$doc_url_logica = $datosDocumentosF[0]->{'xdoc_url_logica'};
				$doc_correlativo1 = $datosDocumentosF[1]->{'xdoc_correlativo'};
			    $doc_titulo1 = $datosDocumentosF[1]->{'xdoc_palabras'};
				$doc_url_logica1 = $datosDocumentosF[1]->{'xdoc_url_logica'};
				$doc_correlativo2 = $datosDocumentosF[2]->{'xdoc_correlativo'};
			    $doc_titulo2 = $datosDocumentosF[2]->{'xdoc_palabras'};
				$doc_url_logica2 = $datosDocumentosF[2]->{'xdoc_url_logica'};
				$doc_correlativo3 = $datosDocumentosF[3]->{'xdoc_correlativo'};
			    $doc_titulo3 = $datosDocumentosF[3]->{'xdoc_palabras'};
				$doc_url_logica3 = $datosDocumentosF[3]->{'xdoc_url_logica'};
				$doc_correlativo4 = $datosDocumentosF[4]->{'xdoc_correlativo'};
			    $doc_titulo4 = $datosDocumentosF[4]->{'xdoc_palabras'};
				$doc_url_logica4 = $datosDocumentosF[4]->{'xdoc_url_logica'};
				$doc_correlativo5 = $datosDocumentosF[5]->{'xdoc_correlativo'};
			    $doc_titulo5 = $datosDocumentosF[5]->{'xdoc_palabras'};
				$doc_url_logica5 = $datosDocumentosF[5]->{'xdoc_url_logica'};
				
				$html2.='
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo1.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica1.'" class="img-responsive">
				</td>
				
				</tr>
				<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo2.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo3.'</b><br>
				
				</td>
				</tr>
				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica2.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					
					<img src="'.$doc_url_logica3.'" class="img-responsive">
				</td>
				
				
				</tr>
					<tr>
				<td width="50%" align="center">
					<b>'.$doc_titulo4.'</b><br>
				</td>
					<td width="50%" align="center">
					<b>'.$doc_titulo5.'</b><br>
				
				</td>
				</tr>

				<tr>
			    <td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica4.'" class="img-responsive">
				</td>
				<td width="50%" align="center" id="tdbordeArchFot">
					<img src="'.$doc_url_logica5.'" class="img-responsive">
				</td>
				
				
				</tr>
				';

			   }

		$html2.='       						
			</table>				
			</div>
		    ';
	 	$mpdf->AddPage();
		$mpdf->WriteHTML($html);   
		$mpdf->AddPage();
		$mpdf->WriteHTML($html2);   
		$mpdf->Output();
	}

}
