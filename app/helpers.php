<?php

use SimpleSoftwareIO\QrCode\Facades\QrCode;

function QRcode($aContenido){
	$sContenido = implode("\n", $aContenido);
	ob_start();
	$imagen = base64_encode(QrCode::format('png')->size(200)->generate($sContenido));
	return $imagen;
}
