<?php

namespace gamlp\Modelo\admin;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
	protected $fillable = array('usr_id', 'usr_prs_id', 'usr_usuario', 'usr_clave', 'usr_controlar_ip', 'usr_registrado', 'usr_modificado', 'usr_estado', 'password', 'usr_usr_id', 'usr_oid');
	protected $hidden   = array('usr_clave');

	protected $table      = "_bp_usuarios";
	protected $primaryKey = "usr_id";
	public $timestamps    = false;
	public static $login  = array(
		'usr_clave'   => 'required',
		'usr_usuario' => 'required',
	);

	public function getAuthIdentifier() {
		return $this->getKey();
	}

	public function getAuthPassword() {
		return $this->usr_clave;
	}

	public function getReminderEmail() {
		return $this->usr_usuario;
	}

	public static function validate($data) {
		return Validator::make($data, static ::$login);
	}

}