<?php

namespace gamlp\Modelo\admin;

use Auth;
use Illuminate\Database\Eloquent\Model;

class AreaTrabajo extends Model
{
	protected $table      = '_bp_espacio_trabajo';
	protected $fillable   = ['espacio_trabajo_id', 'espacio_trabajo_descripcion', 'espacio_trabajo_tipo', 'espacio_trabajo_registrado', 'espacio_trabajo_modificado', 'espacio_trabajo_usr_id','espacio_trabajo_estado'];
	protected $primaryKey = 'espacio_trabajo_id';
	public $timestamps    = false;

	public function Usuario()
	{
		return HasMany('gamlp\AreaTrabajo');
	}

	protected static function getListar()
	{
		$AreaTrabajo = AreaTrabajo::where('espacio_trabajo_estado', 'A')->get();
		return $AreaTrabajo;
	}

	protected static function setBuscar($id)
    {
        $AreaTrabajo = AreaTrabajo::where('espacio_trabajo_id', $id)->first();
        return $AreaTrabajo;
    }
    protected static function getDestroy($id)
    {
        $AreaTrabajo = AreaTrabajo::where('espacio_trabajo_id', $id)->update(['espacio_trabajo_estado' => 'B']);
        return $AreaTrabajo;
    }
}


  
   
  
  
  
  
  