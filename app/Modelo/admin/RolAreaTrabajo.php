<?php

namespace gamlp\Modelo\admin;

use DB;

use gamlp\Modelo\admin\RolAreaTrabajo;

use Illuminate\Database\Eloquent\Model;

class RolAreaTrabajo extends Model
{
    //
    protected $table      = '_bp_espacio_trabajo_detalle';
	protected $fillable   = ['espacio_trabajo_detalle_id', 'espacio_trabajo_detalle_rol_id', 'espacio_trabajo_detalle_espacio_trabajo_id', 'espacio_trabajo_detalle_registrado', 'espacio_trabajo_detalle_modificado', 'espacio_trabajo_detalle_usr_id', 'espacio_trabajo_detalle_estado'];
	protected $primaryKey = 'espacio_trabajo_detalle_id';
	public $timestamps    = false;


	protected static function getListar() {
		$rolareatrabajo = RolAreaTrabajo::join('_bp_roles as r', 'r.rls_id', '=', '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_rol_id')
			->join('_bp_espacio_trabajo as et', 'et.espacio_trabajo_id', '=', '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_espacio_trabajo_id')
			->select( '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_id' , '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_espacio_trabajo_id', 'r.rls_id', 'r.rls_rol', 'et.espacio_trabajo_id', 'et.espacio_trabajo_descripcion')
			->where('r.rls_estado', 'A')
			->where('et.espacio_trabajo_estado', 'A')
			->where('_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_estado', 'A')
			->OrderBy('_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_id', 'ASC')
			->get();
		$aretrabajo   = collect($rolareatrabajo);
		$aretrabajo->values()->all();
		return $aretrabajo;
	}
	protected static function getListar1($row) {
		$rolareatrabajo = RolAreaTrabajo::join('_bp_roles as r', 'r.rls_id', '=', '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_rol_id')
			->join('_bp_espacio_trabajo as et', 'et.espacio_trabajo_id', '=', '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_espacio_trabajo_id')
			->select( '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_id' , '_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_espacio_trabajo_id', 'r.rls_id', 'r.rls_rol', 'et.espacio_trabajo_id', 'et.espacio_trabajo_descripcion')
			->where('r.rls_estado', '<>', 'B')
			->where('et.espacio_trabajo_estado', '<>', 'B')
			->where('r.rls_id', '=', $row[0])
			->OrderBy('_bp_espacio_trabajo_detalle.espacio_trabajo_detalle_id', 'ASC')
			->get();
		$aretrabajo   = collect($rolareatrabajo);
		$aretrabajo->values()->all();
		return $aretrabajo;
	}

	protected static function getListarws($id) {

		$rolareatrabajo = DB::select('select eptr. espacio_trabajo_id,eptr.espacio_trabajo_descripcion
			from _bp_espacio_trabajo_detalle etd
			inner join _bp_espacio_trabajo eptr on eptr.espacio_trabajo_id = etd.espacio_trabajo_detalle_espacio_trabajo_id
			where etd.espacio_trabajo_detalle_rol_id = ' . $id);

		return $rolareatrabajo;
	}

	protected static function getDestroy($id) {
        $rolareatrabajo2 = Opcion::where('espacio_trabajo_detalle_id', $id)->update(['espacio_trabajo_detalle_estado' => 'B']);
        return $rolareatrabajo2;
    }
    protected static function getListarAreaTrab($idrol) {
		$getesptrab = DB::select('select * from sp_obtiene_espacio_trabajo(?)', array($idrol));
		
		return $getesptrab;
	}  

}



