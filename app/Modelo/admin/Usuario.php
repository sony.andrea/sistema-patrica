<?php

namespace gamlp\Modelo\admin;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table      = '_bp_usuarios';
    protected $fillable   = ['usr_id', 'usr_usuario', 'usr_prs_id', 'usr_estado', 'usr_clave', 'usr_usr_id'];
    protected $primaryKey = 'usr_id';
    public $timestamps    = false;
    protected static function getListar()
    {
        $usr = Usuario::join('_bp_personas', '_bp_usuarios.usr_prs_id', '=', '_bp_personas.prs_id')
            ->where('_bp_usuarios.usr_estado', 'A')
            ->get();
        return $usr;
    }
    protected static function setBuscar($id)
    {
        $respuesta = Usuario::where('usr_id', $id)->where('usr_estado', 'A')->first();
        return $respuesta;
    }
    protected static function getEstado()
    {
        $usuario=Usuario::where('_bp_usuarios.usr_estado', 'A')
            ->get();
        return $usuario;
    }
    protected static function setDestroy($id)
    {
        $usuario = Usuario::where('usr_id', $id)
            ->update(['usr_estado' => 'B']);
        return $usuario;
    }
}
