<?php

namespace gamlp\Modelo\parametricas;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';

    protected $fillable = [
        'prs_id',
        'prs_nombres',
        'prs_paterno',
        'prs_materno',
        'prs_ci',
        'prs_estado',
        'prs_usr_id'
    ];
    protected $primaryKey = 'prs_id';

    public $timestamps = false;

    public function Usuario()
    {
        return HasMany('gamlp\Persona');
    }
    protected static function getListar()
    {
        $persona = Persona::join('_bp_usuarios', 'personas.prs_usr_id', '=', '_bp_usuarios.usr_id')
            //->join('_bp_estados_civiles', 'personas.prs_id_estado_civil', '=', '_bp_estados_civiles.estcivil_id')
            ->select(
                'personas.prs_id',
                'personas.prs_nombres',
                'personas.prs_paterno',
                'personas.prs_materno',
                'personas.prs_ci',
                //'_bp_estados_civiles.estcivil',
                '_bp_usuarios.usr_usuario'
            )
            ->where('personas.prs_estado', 'A')
            ->get();
        return $persona;
    }
    protected static function getDestroy($id)
    {
        $persona = Persona::where('prs_id', $id)->update(['prs_estado' => 'B']);
        return $persona;
    }

}
