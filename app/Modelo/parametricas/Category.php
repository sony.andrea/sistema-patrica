<?php

namespace gamlp\Modelo\parametricas;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	public $fililable = ['title', 'parent_id'];

	protected $table      = 'cementerio.categories';
	protected $fillable   = ['id', 'title', 'parent_id', 'datos', 'created_at', 'updated_at'];
	protected $primaryKey = 'id';
	public function childs() {
		return $this->hasMany('gamlp\Modelo\parametricas\Category', 'parent_id', 'id');
	}

}
