<?php

namespace gamlp\Modelo\parametricas;

use Illuminate\Database\Eloquent\Model;

class Macrodistrito extends Model {
	protected $table      = '_prq_macrodistrito';
	protected $fillable   = ['mac_id', 'mac_ciu_id', 'mac_descripcion', 'mac_registro', 'mac_modificado', 'mac_estado'];
	protected $primaryKey = 'mac_id';
	public $timestamps    = false;
}
