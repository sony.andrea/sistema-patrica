{!! Html::script('js/jquery-3.1.0.min.js') !!}
{!! Html::script('js/loadingoverlay.js')!!}

{!! Html::script('js/bootstrap.min.js') !!}

{!! Html::script('js/app.min.js') !!}

{!! Html::script('js/bootstrapValidator.js') !!}

{!! Html::script('js/sweetalert.min.js') !!}
{!! Html::script('js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('js/datatable/dataTables.fixedHeader.min.js') !!}
{!! Html::script('js/datatable/dataTables.responsive.min.js') !!}


{!! Html::script('js/datatable/dataTables.buttons.min.js') !!}
{!! Html::script('js/datatable/buttons.html5.min.js')!!}


{!! Html::script('js/bootstrap-select/bootstrap-select.min.js') !!}
{!! Html::script('js/treeview.js') !!}\

{!! Html::script('js/jquery-ui.min.js') !!}
{!! Html::script('js/jqueryui-touch-punch.min.js') !!}
{!! Html::script('js/pivot.js') !!}
{!! Html::style('css/pivot.css')!!}



@stack('scripts')

<script>
	/*
	 * limpia todos los inputs
	 */
	 $(".reset-inputs").click(function(){
	 	$("input").val("");
	 	$("select").val("");
	 });

	 $(".reset-input2").click(function(){
$("input").val("");
	 	$("select").val("");
	 	
	
	 });
 	/*
 	 *Convierte a mayusculas
 	 */
 	 $(".to-upper-case").on('focus keyup keypress blur change',function() {
 	 	this.value = this.value.toLocaleUpperCase();
 	 });


  /*
	 *Valida que no se acepte numeros negativos
	 */
	 $(".not-negative").on('focus keyup keypress blur change', function() {
	 	var des= parseInt($(".not-negative").val());
	 	if(!isNaN(des)){
	 		$(".not-negative").val('');
	 	} 
	 });

	 /**/

	 $(".clean-form").click(function(event){
	 	$( 'form' ).data('bootstrapValidator').resetForm();

	 });

	 $(".reset-cerrar-c").click(function(event) {
	 	$( 'form' ).data('bootstrapValidator').resetForm();
	 	$( "#myCreate" ).modal('toggle');

	 });


	 $(".reset-cerrar-update").click(function(event) {
	 	$( 'form' ).data('bootstrapValidator').resetForm();
	 	$( "#myUpdate" ).modal('toggle');


	 });


@stack('scripts')
