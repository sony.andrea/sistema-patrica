@include('backend.template.partials.sidebar')
@inject('menuws','gamlp\Http\Controllers\Auth\AuthController')
@inject('menuws2','gamlp\Http\Controllers\Auth\AuthController')
<style type="text/css">
#event
{
    font-size: 16px;
    height: 25px;
    width: 120px;
    color: #2a8d1;
    background-color: transparent;
    border: none !important;
}

#rol
{
    color: #ffffff;
    display:inline;
    min-height:10px;
    margin:0 0 10px;
}

#rol2
{
    color: #ffffff;
    display:inline;
    margin: 15px 13px 0 0;
}
</style>
<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SP</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>PATRIMONIO</b> </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu" id="rol2"> 
                        <form type= "hidden" action="{{ route('login-post2') }}" method="post"  >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="rol">
                                Seleccionar Rol <span class="caret"></span>
                            </a>
                              <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                 @foreach ($menuws2->submenus3() as $link03)
                                <li><a href="#" name="{{ $link03->xrls_rol }}" id="rol-{{ $link03->xrls_id }}" onclick="info2(this)"><button type="submit"  id= "event" >{{ $link03->xrls_rol }}</button></a></li>
                                @endforeach
                              </ul>
                              <input type="hidden" id= "rrol" name="rrol"/>
                              <input type="hidden" class="dropdown-toggle" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </li>
                    <li class="dropdown user user-menu">     
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="workspace">
                            Espacio de Trabajo <span class="caret"></span>
                        </a>
                          <ul class="dropdown-menu" role="menu" >
                             @foreach ($menuws->submenus2() as $link02)
                            <li><a href="#" name="{{ $link02->xespacio_trabajo_descripcion }}" id="workspace-{{ $link02->xespacio_trabajo_id }}" onclick="info(this)">{{ $link02->xespacio_trabajo_descripcion }}</a></li>
                            @endforeach
                          </ul>
                    </li>
                     <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('/img/puntocinco041.png')}}" class="user-image" alt="User Image"/>
                            <span class="hidden-xs">{{ Auth::user()->usr_usuario}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="{{asset('/img/puntocinco041.png')}}" class="img-circle" alt="User Image" />
                                <p>
                                    <small>{{ Session::get('NOMBRES') }} {{ Session::get('PATERNO') }} {{ Session::get('MATERNO') }}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-5 text-center">
                                    Rol:&nbsp;&nbsp;{{ Session::get('ROL') }}&nbsp;&nbsp;&nbsp;&nbsp;Usuario:&nbsp;&nbsp;{{ Session::get('USUARIO') }}
                                </div>
                                <div class="col-xs-4 text-center">
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('cerrar') }}" class="btn btn-default btn-flat">Salir</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                <!-- Control Sidebar Toggle Button
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>
<script>
  function info2(dato)
  { 
    var rrooll = [];
    var cont = 0;
    datorol = document.getElementById(dato.id).innerHTML;
    document.getElementById("rol").innerHTML = datorol;
    var idrol = dato.id.split('-');
    esprol = idrol[1];
    var nA = new Array(esprol);
    console.log(nA,'id_rol');
    $('#rrol').val(nA);
  }
</script>