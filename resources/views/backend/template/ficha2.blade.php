<!DOCTYPE html>
<html lang="en">
    @section('htmlheader')
        @include('backend.template.partials.htmlheader')
    @show
    <body class="skin-green sidebar-mini">
        @section('scripts')
            @include('backend.template.partials.scripts')
        @show
    </body>
</html>