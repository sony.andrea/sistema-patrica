@extends('backend.template.app')
@section('htmlheader_title')
@endsection
@section('main-content')
{!! Form::open(array('route' => 'RolAreaTrabajo.store','method'=>'POST','class'=>'')) !!}
@if(Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		{{Session::get('message')}}
		</div>
 @endif
 <br>
            <h2>Rol Espacio de Trabajo</h2>
<div class="row">
<div class="col-md-5">
 <div id="no-more-tables">
	<table class="col-md-12 table-striped table-condensed cf" id="lts-usuario" border="1" bordercolor="#999">
		<thead class="cf">
			<br>
			<tr>
				<th colspan="4"> Rol Usuarios </th>
   			</tr>
	 			<tr>
				<th><i class="fa fa-check-square-o fa-2x"></i></th>
				<th>ID</th>
				<th>Rol</th>
				</tr>
		</thead>
			@foreach($rol as $r)
				<tr>
					<td data-title="Seleccionar">
					 <input tabindex="1" type="radio" name="rol[]" id="{{ $r->rls_id }}" value="{{ $r->rls_id }}">
					</td>
					<td data-title="ID">{{ $r->rls_id }}</td>
					<td data-title="rol">{{ $r->rls_rol }}</td>
				</tr>
			@endforeach
	</table>
  </div>
</div>
<div class="col-md-6" >
  <div id="no-more-tables">
	 <table class="col-md-12 table-striped table-condensed cf" id="rolasignadoat" border="1" bordercolor="#999">
		<thead class="cf">
				<br>
					<tr>
						<th colspan="4"> Roles Asignados </th>
					</tr>
					<tr>
						<th><i class="fa fa-check-square-o fa-2x"></i></th>
						<th>ID</th>
						<th>Usuario</th>
						<th>Rol</th>
					</tr>
				</thead>
				@foreach($rolareatrabajo as $ar)
					<tr>
	        			<td data-title="Seleccionar">
	        			<input type="checkbox" name="rolasignadoat[]" id="{{ $ar->espacio_trabajo_detalle_id }}" value=" {{ $ar->espacio_trabajo_detalle_id }}">
	        			</td>
	        			<td data-title="ID">{{ $ar->espacio_trabajo_detalle_id }}</td>
						<td data-title="area trabajo">{{ $ar->espacio_trabajo_descripcion}}</td>
						<td data-title="Rol">{{ $ar->rls_rol}}</td>
					</tr>
				@endforeach
	   </table>
	   	&nbsp;
	</div>
    <div  align="center">
	       <button type="submit" name="Asignar" class="btn btn-default" style="background:#61BC8C" ><span class="fa fa-plus" ></span>Asignar</button>&nbsp;&nbsp;
	       <button type="submit" name="Desasignar" class="btn btn-default" style="background:#61BC8C"><span class="fa fa-close" ></span>Desasignar</button>
	</div>
	&nbsp;
	<div class="col-lg-12" >
		<div id="no-more-tables">
			<table class="col-md-12 table-striped table-condensed cf" id="rolatnoasignado"  border="1" bordercolor="#999">
				<thead class="cf">
					<tr>
						<th colspan="4"> Roles No Asignados </th>
					</tr>
					<tr>
					<th><i class="fa fa-check-square-o fa-2x"></i></th>
						<th>ID</th>
						<th>Usuario</th>
					</tr>
				</thead>
				@foreach($areatrabajo as $u)
					<tr>
	        			<td data-title="Seleccionar">
	        			<input tabindex="1" type="checkbox" name="rolatnoasignado[]" id="{{ $u->espacio_trabajo_id }}" value=" {{ $u->espacio_trabajo_id }}">
	        			</td>
	        			<td data-title="ID">{{ $u->espacio_trabajo_id }}</td>
						<td data-title="Nombre">{{ $u->espacio_trabajo_descripcion }}</td>
					</tr>
				@endforeach
				</table>
		</div>
	</div>
</div>
</div>
@endsection
