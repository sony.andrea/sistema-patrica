<div class="modal fade modal-default" id="myCreate" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 container-fluit">
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h4>
									Registrar Área de Trabajo
								</h4>
							</div>
							<div class="panel-body">

								<input type="hidden" name="csrf-token" value="{{ csrf_token() }}" id="token">

								<div class="form-group">
									{!!Form::label('areatrabajo','Nueva Area de travajo: ')!!}
									{!! Form::text('areatravajo', null, array('placeholder' => 'Nombre de Area de Trabajo','class' => 'form-control','id'=>'areatrabajo')) !!}
									{{-- {!!Form::label('areatrabajotipo','Nuevo Tipo: ')!!}
									{!! Form::text('areatrabajotipo', null, array('placeholder' => 'Tipo','class' => 'form-control','id'=>'areatrabajotipo')) !!} --}}
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<a type = "button" class = "btn btn-primary resetBtn"  id="registrar">Registrar</a> 
			</div>

		</div>
	</div>
</div>

