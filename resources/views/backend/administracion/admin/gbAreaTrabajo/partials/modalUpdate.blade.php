<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myUpdate" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
              <div class="row">
                <div class="col-xs-12 container-fluit">
                 <div class="panel panel-warning">
                     <div class="panel-heading">
                       <h4>
                           Actualizar Área de Trabajo
                       </h4>
                   </div>
                   <div class="panel-body">
                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                    <input id="id" type="hidden">
                    
                    <div class="form-group">
                        {!!Form::label('areatrabajou', 'Área de Trabajo:')!!}
                        {!! Form::text('areatrabajou', null, ['placeholder' => 'Ingrese el nombre del Área de Trabajo','class'=>'form-control','id'=>'areatrabajou']) !!}
                        {{-- {!!Form::label('areatrabajotipou','Tipo: ')!!}
                  {!! Form::text('areatrabajotipou', null, array('placeholder' => 'Tipo','class' => 'form-control','id'=>'areatrabajotipou')) !!} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-default" data-dismiss="modal" type="button">
        Cerrar
    </button>
    <a type = "button" class = "btn btn-primary resetBtn"  id="actualizar">Actualizar</a> 
</div>
</div>
</div>
</div>