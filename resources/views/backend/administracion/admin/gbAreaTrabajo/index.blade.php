@extends('backend.template.app')
@section('main-content')
@include('backend.administracion.admin.gbAreaTrabajo.partials.modalUpdate')
@include('backend.administracion.admin.gbAreaTrabajo.partials.modalCreate')
<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title">
         <h3>
            Area de trabajo
            <small>
                <button class="btn btn-warning   fa fa-plus-square pull-right" data-target="#myCreate" data-toggle="modal">&nbsp;Nuevo</button>
            </small>
         </h3>
       </div>
      </section>
   </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
            </div>
            <div class="box-body">
            <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-rol">
            <thead class="cf">
            <tr>
                <th>
                    Acciones
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Fecha registro
                </th>
                <th>
                    Fecha modificado
                </th>
                <th>
                    Estado
                </th>
            </tr>
        </thead>
        <tr>
        </tr>
    </table>
</div>

</div>
@endsection
@push('scripts')
<script>
	$('#lts-rol').DataTable( {

         "processing": true,
            "serverSide": true,
            "ajax": "AreaTrabajo/create",
            "columns":[
                {data: 'acciones',orderable: false, searchable: false},
                {data: 'espacio_trabajo_descripcion'},
                {data: 'espacio_trabajo_registrado'},
                {data: 'espacio_trabajo_modificado'},
                {data: 'espacio_trabajo_estado'},
        ],
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

    });
	function Mostrar(btn){
    var route = "/AreaTrabajo/"+btn.value+"/edit";
    $.get(route, function(res){
        $("#areatrabajou").val(res.espacio_trabajo_descripcion);
        $("#id").val(res.espacio_trabajo_id);
    });
    }
    $("#registrar").click(function(){
        var route="AreaTrabajo";
         var token =$("#token").val();
        $.ajax({
            url: route,
             headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            data: {'espacio_trabajo_descripcion':$("#areatrabajo").val()},
                success: function(data){
                    //$('#myCreate').fadeIn(1000).html(data);
                    $("#myCreate").modal('toggle');
                    swal("El Area de Trabajo!", "Se registrado correctamente!", "success");
                    $('#lts-rol').DataTable().ajax.reload();

                },
                error: function(result) {
                        swal("Opss..!", "Succedio un problema al registrar inserte bien los datos!", "error");
                }
        });
    });
    $("#actualizar").click(function(){
            var value =$("#id").val();
            var route="AreaTrabajo/"+value+"";
            var token =$("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data: {'espacio_trabajo_descripcion':$("#areatrabajou").val()},
                success: function(data){
                    $("#myUpdate").modal('toggle');
                    swal("El Area de Trabajo!", "Fue actualizado correctamente!", "success");
                    $('#lts-rol').DataTable().ajax.reload();


                },  error: function(result) {
                      console.log(result);
                     swal("Opss..!", "El Area de Trabajo no se puedo actualizar intente de nuevo!", "error")
                }
            });
        });
    function Eliminar(btn){
    var route="AreaTrabajo/"+btn.value+"";
    var token =$("#token").val();
    swal({   title: "Esta seguro de eliminar el Rol?",
          text: "Presione ok para eliminar el registro de la base de datos!",
          type: "warning",   showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Eliminar!",
          closeOnConfirm: false
        }, function(){
         $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'DELETE',
            dataType: 'json',

            success: function(data){
                $('#lts-rol').DataTable().ajax.reload();
                swal("El Area de Trabajo!", "Fue eliminado correctamente!", "success");
            },
            error: function(result) {
                swal("Opss..!", "El Grupo tiene registros en otras tablas!", "error")
            }
        });
    });
    }
</script>
@endpush


