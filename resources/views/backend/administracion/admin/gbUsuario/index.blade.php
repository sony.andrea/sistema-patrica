@extends('backend.template.app')
@section('main-content')
@include('backend.administracion.admin.gbUsuario.partials.modalregUsuario')
@include('backend.administracion.admin.gbUsuario.partials.modalupUsuario')
<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title">
         <h3>
            Usuario
            <small>
                <button class="btn btn-warning fa fa-plus-square pull-right" data-target="#myUserCreate" data-toggle="modal">&nbsp;Nuevo</button>
            </small>
         </h3>
       </div>
      </section>
   </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
            </div>
            <div class="box-body">
<div id="no-more-tables">
    <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-usuario">
        <thead class="cf">
            <tr>
                <th>
                    Acciones
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Paterno
                </th>
                <th>
                    Usuario
                </th>
                <th>
                    Clave
                </th>
                <th>
                    Estado
                </th>
                <th>
                    Registro
                </th>
                <th>
                    Modificado
                </th>
            </tr>
        </thead>
    </table>
</div>
@endsection

@push('scripts')
<script>
    $('#lts-usuario').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": "Usuario/create",
                    "columns":[
                    		{data: 'acciones',orderable: false, searchable: false},
                    		{data: 'prs_nombres'},
                    		{data: 'prs_paterno'},
                    		{data: 'usr_usuario'},
                    		{data: 'usr_clave'},
                    		{data: 'usr_estado'},
                    		{data: 'usr_registrado'},
                    		{data: 'usr_modificado'},
                    ],
                      "language": {
                        "url": "/lenguaje"
                    },
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
 	});
 	$("#registroUsuario").click(function()
    {
        var route="Usuario";
         var token =$("#token").val();
        $.ajax({
            url: route,
             headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            dataType: 'json',
            data: {'usr_usuario':$("#usuario").val(),'usr_clave':$("#clave").val(),'usr_prs_id':$("#usr_prs_id").val()},
                success: function(data){
                    //$('#myCreate').fadeIn(1000).html(data);
                    $("#myUserCreate").modal('toggle');
                    swal("El Usuario!", "Fue registrado correctamente!", "success");
                    $('#lts-usuario').DataTable().ajax.reload();

                },
                error: function(result) {
                        swal("Opss..!", "Sucedio un problema al registrar inserte bien los datos!", "error");
                }
        });
    });
 	function MostrarUsuario(btn)
 	{
    	var route = "Usuario/"+btn.value+"/edit";
    	$.get(route, function(res){
            $("#usr_id").val(res.usr_id);
        	$("#usuario1").val(res.usr_usuario);
        	$("#clave1").val(res.usr_clave);
        	$("#usr_prs_id1").val(res.usr_prs_id);
    	});
    }
 	$("#actualizar").click(function()
 	{
        var value =$("#usr_id").val();
        var route="Usuario/"+value+"";
        var token =$("#token").val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            data: {'usr_usuario':$("#usr_usuario1").val(),'usr_clave':$("#usr_clave1").val(),'usr_prs_id':$("#usr_prs_id1").val()},
            success: function(data){
                $("#myUserUpdate").modal('toggle');
                swal("El Usuario!", "Fue actualizado correctamente!", "success");
                $('#lts-usuario').DataTable().ajax.reload();
            },  error: function(result) {
                  console.log(result);
                 swal("Opss..!", "El Grupo no se puedo actualizar intente de nuevo!", "error")
            }
        });
    });
    function EliminarUsuario(btn){
        var route="Usuario/"+btn.value+"";
        var token =$("#token").val();
        swal({   title: "Esta seguro de eliminar al usuario?",
        text: "Presione ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Eliminar!",
        closeOnConfirm: false
        }, function(){
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',

                success: function(data){
                    $('#lts-usuario').DataTable().ajax.reload();
                    swal("Usuario!", "Fue eliminado correctamente!", "success");
                },
                    error: function(result) {
                        swal("Opss..!", "El Usuario tiene registros en otras tablas!", "error")
                }
            });
         });
    }
</script>
@endpush

