@extends('backend.template.app')
@section('main-content')
@include('backend.administracion.admin.gbUsuarioUnidadRecaudadora.partials.modalCreate')
@include('backend.administracion.admin.gbUsuarioUnidadRecaudadora.partials.modalUpdate')
<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title">
         <h3>
            Usuario Unidad Recaudadora
             
            <small>
                <button class="btn btn-primary fa fa-plus pull-right" data-target="#myCreate" onClick="llenarCombos()" data-toggle="modal">&nbsp;Nuevo</button>
            </small>
         </h3>
                  <div class="row">
            <div class="col-md-6">
              <!---->
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Mostrar Registros:</label>
                    <select class="form-control" id="reg" name="reg" onclick="listar_select()">
                     
                      <option value="10" selected="selected"> 10</option>
                       <option value="30">30</option>
                       <option value="40">40</option>
                       <option value="50">50</option>
                       <option value="60">60</option>
                       <option value="70">70</option>
                       <option value="80">80</option>
                       <option value="90">90</option>
                       <option value="100">100</option>
                     </select>
                  </div>
                </div>
              <div class="col-md-6">
                 <label class="col-md-6">Listar por:</label> 
                   <select class="form-control" id="opcion" name="opcion" onclick="listar_select()">
                    <option value="altas" >ALTAS</option>
                    <option value="bajas">BAJAS</option>
                    <option value="todo">TODO</option>
                   </select>
                 </div>

              </div>
            </div>
          </div>
       </div>
      </section>
   </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                </div>
            <div class="table-responsive">
            <div id="listtipopuesto">
            </div>  
            </div>
@endsection
@push('scripts')
<script>
  var urlEjecutaRegla = 'http://192.168.6.113:80/MotorServicio/public/api/reglaNegocio/ejecutarWeb';
  var idu = {{$idus}};
  var datosUsuario = [];
  var datosCodigo = [];
function listar(paging_pagenumber){

           htmlUnidadRecaudadora = '';
           htmlUnidadRecaudadora+='<div class="row">';
           htmlUnidadRecaudadora+='<div class="col-md-12" >' ;
           htmlUnidadRecaudadora+='<table id="lts-via" class="table table-striped" cellspacing="1" width="100%" >';

           htmlUnidadRecaudadora+='<thead><tr>';
           htmlUnidadRecaudadora+='<th align="center">Nro.</th><th align="center" class="form-nobajas-cabecera">Opciones</th><th align="left">Usuario</th><th align="left">Unidad Recaudadora</th><th align="left">Ingreso</th></tr></thead>';
           var lista1 = $( "#opcion" ).val();
           var paging_pagesize = $( "#reg" ).val();
           var formData ={"identificador": "SERVICIO_SIERRA-406","parametros": '{"tipolista":"'+lista1+'","paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumber+'}'};
           
           $.ajax({
             type     : 'GET',
             url       :'/v0.0/getToken',
             data      :'',

             success: function(token){
              console.log('tok : ' +token);
              $.ajax({
               type        : 'POST',            
               url         : urlEjecutaRegla,
               data        : formData,
               dataType    : 'json',
               crossDomain : true,
               headers: {
                 'authorization': 'Bearer' + token,
               },
               success: function(dataIN) {
                console.log(dataIN);
                var datos = dataIN;
                var tam = datos.length;
                if(datos == "[{ }]"){
                  tam = 0;
                };
                for (var i = 0; i < tam; i++) {  
                 htmlUnidadRecaudadora +='<tr><td align="left">'+(i+1)+'</td>';
                 
                 if(JSON.parse(datos[i].data).usuario_unidad_recaudadora_estado!='B') {
                  htmlUnidadRecaudadora+='<td class="form-nobajas"><button class="btncirculo btn-xs btn-primary"  fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar(' + JSON.parse(dataIN[i].data).usuario_unidad_recaudadora_id + ', \''+ JSON.parse(dataIN[i].data).usr_usuario +'\',\''+JSON.parse(datos[i].data).ur_descripcion+'\',\''+JSON.parse(datos[i].data).usuario_unidad_recaudadora_registra_ingreso+'\');" ><i class="glyphicon glyphicon-pencil"></i></button>  <button value="" class="btncirculo btn-xs btn-danger" style="background:#FA8072" fa fa-plus-square pull-right" data-target="#myDelete" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + JSON.parse(dataIN[i].data).usuario_unidad_recaudadora_id + ');" ><i class="glyphicon glyphicon-trash"></i></button></td>';
                }else{
                  if(opcion=='todo'){
                   htmlUnidadRecaudadora+='<td>Dado de Baja</td>';   

                 }
                 
               }
               
              htmlUnidadRecaudadora+= '<td align="left">'+ JSON.parse(datos[i].data).usr_usuario+'</td>'+
                          '<td align="left">'+ JSON.parse(datos[i].data).ur_descripcion+'</td>'+
                          '<td align="left">'+ JSON.parse(datos[i].data).usuario_unidad_recaudadora_registra_ingreso+'</td>'+
                          '</tr>';
              
            }
            htmlUnidadRecaudadora +='</table></div></div';
            htmlUnidadRecaudadora +='<div>';
            htmlUnidadRecaudadora +='<ul class="pager">';
            htmlUnidadRecaudadora +='<li><a href="#" style="color:blue;" onClick="btnAnterior()">Anterior</a></li>';
            htmlUnidadRecaudadora +='<li><a href="#" style="color:blue;" onClick="btnSiguiente()">Siguiente</a></li>';
            htmlUnidadRecaudadora +='</ul>';
            htmlUnidadRecaudadora +='</div>';
            htmlUnidadRecaudadora +='</div>';
            $('#listtipopuesto').html(htmlUnidadRecaudadora);
            if (opcion=="bajas") {
              $(".form-nobajas-cabecera").hide();
              $(".form-nobajas").hide();
            }
          },
          error: function (xhr, status, error) {
          }
        });
            },
            error: function(result) {
              swal( "Error..!", "No se puedo guardar los datos", "error" );
            },
          });  
         };


function limpiar()
{
    var id1 = $( "#id1" ).val('');
    var idusuario1 = $( "#idusuario1" ).val('');
    var codigo1 = $( "#codigo1" ).val('');
    var ingreso1 = $( "#ingreso1" ).val('');
}

$( "#registrar" ).click(function() 
{
    var idu1 = idu; 
    var idusuario1 = $( "#idusuario1" ).val();
    var codigo1 = $( "#codigo1" ).val();
    var ingreso1 = $( "#ingreso1" ).val();
    $.ajax({
      type        : 'GET',
      url         :  '/v0.0/getToken',
      data        : '',
      success: function(token) {

      var formData = {"identificador": "SERVICIO_SIERRA-407","parametros": '{"usuario_id":'+idusuario1+', "ur_codigo":'+codigo1+',"registra_ingreso":"'+ingreso1+'","usr_id":'+idu1+'}'}; 
    $.ajax({
          type        : 'POST',            
          url         :  urlEjecutaRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
            'authorization': 'Bearer ' + token,
          },
           success: function(dataIN) {
            console.log('data',dataIN);
              swal( "Exito..!", "Se guardo correctamente los datos!", "success" );
              limpiar();
               listar(1);
           },
           error: function(result) {
            swal( "Error..!", "No se puedo guardar los datos", "error" );
           }
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });
   
});


function editar(id ,usuario_nombre ,codigo,registra_ingreso){
$( "#id2" ).val(id);
          var  tam = datosUsuario.length;
              if(datosUsuario == "[{ }]"){
                tam = 0;
              };
              for(var j=0;j<tam;j++){ 
                var a =  datosUsuario[j].usr_usuario.trim() ;
                  if(usuario_nombre.trim() !=a){   
                  document.getElementById("idusuario2").innerHTML += "<option value='"+datosUsuario[j].usr_id+"'>"+datosUsuario[j].usr_usuario +"</option>";
                }else{
                  document.getElementById("idusuario2").innerHTML += "<option value='"+datosUsuario[j].usr_id+"' selected>"+datosUsuario[j].usr_usuario +"</option>";  
                };
              };
              tam = datosCodigo.length;
              if(datosCodigo == "[{ }]"){
                tam = 0;
              };
              for(var j=0;j<tam;j++){
                 var b =  datosCodigo[j].ur_descripcion.trim() ;
                if( codigo.trim()== b){
                  document.getElementById("codigo2").innerHTML += "<option value='"+datosCodigo[j].ur_codigo+"' selected>"+datosCodigo[j].ur_descripcion+"</option>";
                }else{
                  document.getElementById("codigo2").innerHTML += "<option value='"+datosCodigo[j].ur_codigo+"'>"+datosCodigo[j].ur_descripcion+"</option>";
                };
              };
            $( "#ingreso2" ).val(registra_ingreso);
};



// LISTA EL NOMBRE DEL USUARIO EN EL COMBO

function listar_Combos(){
  $.ajax({
    type        : 'GET',
    url         :  '/v0.0/getToken',
    data        : '',
    success: function(token) {
      var formData = {"identificador": "SERVICIO_SIERRA-426","parametros": '{}'};
      $.ajax({
        type        : 'POST',            
        url         : urlEjecutaRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer ' + token,
        },
        success: function(dataIN) {
          datosUsuario = dataIN;
          console.log(datosUsuario);
        },
        error: function (xhr, status, error) {
        }
      });
      formData = {"identificador": "SERVICIO_SIERRA-430","parametros": '{}'};
      $.ajax({
        type        : 'POST',            
        url         : urlEjecutaRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer ' + token,
        },
        success: function(dataIN) {
        
          datosCodigo = dataIN;
          console.log('Codigo',datosCodigo);
        },
        error: function (xhr, status, error) {
        }
      });
     
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
};
//------------------------------------------

function llenarCombos(){
  var tam = datosUsuario.length;
  if(datosUsuario == "[{ }]"){
    tam = 0;
  };
  for(var i=0;i<tam;i++){ 
    document.getElementById("idusuario1").innerHTML += "<option value='"+datosUsuario[i].usr_id+"'>"+datosUsuario[i].usr_usuario +"</option>";
  };

 tam = datosCodigo.length;
  if(datosCodigo == "[{ }]"){
    tam = 0;
  };
  for(var i=0;i<tam;i++){ 
    document.getElementById("codigo1").innerHTML += "<option value='"+datosCodigo[i].ur_codigo+"'>"+datosCodigo[i].ur_descripcion+"</option>";
  };
}



$( "#actualizar" ).click(function() {
     
    var idu1 = idu; 
    var id2 = $( "#id2" ).val();
    var idusuario2 = $( "#idusuario2" ).val();
    var codigo2 = $( "#codigo2" ).val();
    var ingreso2 = $( "#ingreso2" ).val();
    
     $.ajax({
      type        : 'GET',
      url         :  '/v0.0/getToken',
      data        : '',
      success: function(token) {
     var formData = {"identificador": "SERVICIO_SIERRA-408","parametros": '{"id":'+id2+',"usuario_id":'+idusuario2+', "ur_codigo":'+codigo2+',"registra_ingreso":"'+ingreso2+'","usr_id":'+ idu1 +'}'};

      $.ajax({
         type        : 'POST',            
         url         :  urlEjecutaRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
             'authorization': 'Bearer ' + token,
         },
         success: function(dataIN) {
            swal( "Exito..!", "Se modifico correctamente los datos!", "success" );
         },
         error: function(result) {
          swal( "Error..!", "No se puedo mofigicar los datos", "error" );
         }
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });
    listar(1);
});



function darBaja(id)
{
  console.log(id);
  var idu1 = idu;
  console.log(idu1),
  swal({   title: "Esta seguro de eliminar el Tipo Puesto?",
      text: "Presione ok para eliminar el registro de la base de datos!",
      type: "warning",   showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si, Eliminar!",

      closeOnConfirm: false
    }, function(){
    $.ajax({
    type        : 'GET',
    url         :  '/v0.0/getToken',
    data        : '',
    success: function(token) { 

    var formData = {"identificador": "SERVICIO_SIERRA-409","parametros": '{"id":'+id+', "usr_id":'+idu1+'}'};
    console.log(formData);
        $.ajax({
         type        : 'POST',            
         url         :  urlEjecutaRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
             'authorization': 'Bearer ' + token,
         },
         success: function(dataIN) {
          console.log(dataIN);
            swal( "Exito..!", "Se elimino correctamente los datos!", "success" );
         },
         error: function(result) {
          swal( "Error..!", "No se puede eliminar los datos", "error" );
         }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
  listar(1);
  });
};

$(document).ready(function (){
  listar(1);
  listar_Combos();
}); 

</script>

<script type="text/javascript">
 $(document).ready(function(){
     $('#uuRecaudadoraform').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
            
            idusuario1: {
                        validators: {
                        notEmpty: { // <=== Use notEmpty instead of Callback validator
                        message: ' Ingrese Usuario .'
                        }
                    }
                },
            codigo1: {
                        validators: {
                        notEmpty: { // <=== Use notEmpty instead of Callback validator
                        message: ' Ingrese Codigo Usuario .'
                        }
                    }
                },
            ingreso1: {
                        validators: {
                        notEmpty: { // <=== Use notEmpty instead of Callback validator
                        message: ' Ingrese Ingreso .'
                        }
                    }
                },    
        }
      }).on('error.field.bv', function(e, data) {
        if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
      }).on('success.field.fv', function(e, data) {
        if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
      });
      $('#uuRecaudadoraform2').bootstrapValidator({
            message: 'Este valor no es válido',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
           
            idusuario2: {
                        validators: {
                        notEmpty: { // <=== Use notEmpty instead of Callback validator
                        message: ' Ingrese Usuario .'
                        }
                    }
                },
            codigo2: {
                        validators: {
                        notEmpty: { // <=== Use notEmpty instead of Callback validator
                        message: ' Ingrese Codigo Usuario .'
                        }
                    }
                },
            ingreso2: {
                        validators: {
                        notEmpty: { // <=== Use notEmpty instead of Callback validator
                        message: ' Ingrese Ingreso .'
                        }
                    }
                },
              
        }
      })
       }).on('error.field.bv', function(e, data) {
        if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
      }).on('success.field.fv', function(e, data) {
        if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
      });
</script>

<script>
var paging_pagesize = $( "#reg" ).val();
 var paging_pagenumbe = 1;

 function btnAnterior(){
  
     paging_pagenumbe--;

   listar( paging_pagenumbe);
 }

 function btnSiguiente(){

  paging_pagenumbe++;
  listar(paging_pagenumbe);
  }

  function listar_select(){
   
   paging_pagenumbe=1;
   
   listar(paging_pagenumbe);
  };

</script>

@endpush
