
<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myUpdate" tabindex="-5">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 container-fluit">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>
                    Modificar Usuario Unidad Recaudadora
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h4>
              </div>
              <div class="panel-body">
                <div class="caption">
                    {!! Form::open(['id' => 'uuRecaudadoraform2'])!!}
                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                     <input id="id2" name="id2" type="hidden" value="">
                             <div class="row">
                                <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>
                                            Usuario:
                                        </label>
                                        <span class="block input-icon input-icon-right">
                                            <select class=" form-control" placeholder ="Seleccionar Nombre de Usuario" id ="idusuario2" name="idusuario2">
                                                <option value="-1">Seleccionar Nombre de Usuario</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>
                                            Unidad Recaudadora:
                                        </label>
                                        <span class="block input-icon input-icon-right">
                                            <select class=" form-control" placeholder ="Seleccionar Nombre de Usuario" id ="codigo2" name="codigo2">
                                                <option value="-1">Seleccionar Unidad Recaudadora</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>
                                                Ingreso:
                                            </label>
                                            {!! Form::text('ingreso2', null, array('placeholder' => 'Ingrese Número ','maxlength'=>'20','class' => 'form-control','id'=>'ingreso2')) !!}
                                        </div>
                                    </div>
                                </div>
                            </div> 
                              
                           
                        </input>
                    </input>
                    {!! Form::close() !!}
                </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary" id="actualizar" data-dismiss="modal" type="button">
                   <i class="glyphicon glyphicon-pencil"></i> Modificar
                </button>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 

