<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myCreate" tabindex="-5">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 container-fluit">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>
                    Registrar Acciones
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h4>
              </div>
              <div class="panel-body">
                <div class="caption">
                    {!! Form::open(['id1' => 'accionesform'])!!}
                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                        <input id="id1" name="id1" type="hidden" value="">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>
                                                Acción:
                                            </label>
                                            {!! Form::text('accion1', null, array('placeholder' => 'Ingrese Acción ','maxlength'=>'20','class' => 'form-control','id'=>'accion1')) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>
                                                Descripción:
                                            </label>
                                            {!! Form::text('descripçion1', null, array('placeholder' => 'Ingrese Desacripción','maxlength'=>'50','class' => 'form-control','id'=>'descripción1')) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                               
                        </input>
                    </input>
                    {!! Form::close() !!}
                </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-default "  type="button" onClick="Limpiar()">
                    <i class="fa fa-eraser"></i> Limpiar
                </button>
                <button class="btn btn-primary" id="registrar" data-dismiss="modal" type="button" >
                    <i class="fa fa-save"></i> Guardar
                </button>    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 

