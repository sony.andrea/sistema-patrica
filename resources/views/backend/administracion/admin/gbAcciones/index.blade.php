@extends('backend.template.app')
@section('main-content')
@include('backend.administracion.admin.gbAcciones.partials.modalCreate')
@include('backend.administracion.admin.gbAcciones.partials.modalUpdate')
<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title">
         <h3>
            Acciones
            <small>
                <button class="btn btn-primary fa fa-plus pull-right" data-target="#myCreate"  data-toggle="modal">&nbsp;Nuevo</button>
            </small>
         </h3>
       </div>
      </section>
   </div>
</div>
<div class="row">
  <div class="col-md-8">
  </div>
  <div class="col-md-2">
   <label >Mostrar Registros:</label> 
   <select class="form-control" id="reg" name="reg" onclick="listarSelect()">
    <option value="10">10</option>
    <option value="15">15</option>
    <option value="20">20</option>
    <option value="30">30</option>
    <option value="40">40</option>
    <option value="50">50</option>
    <option value="60">60</option>
    <option value="70">70</option>
    <option value="80">80</option>
    <option value="90">90</option>
    <option value="100">100</option>
   </select>
  </div>
<div class="col-md-2">
  <label >Listar por:</label> 
  <select class="form-control" id="opcion" name="opcion" onclick="listarSelect()">
    <option value="altas" selected="selected" > ALTAS</option>
    <option value="bajas">BAJAS</option>
    <option value="todo">TODO</option>
  </select>
  </div>
</div>
<hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                </div>
            <div class="table-responsive">
            <div id="listtipopuesto">
            </div>  
            </div>
@endsection
@push('scripts')
<script>
  var idu = {{$idus}};
  var datosUsuario = [];
  var datosCodigo = [];

function listar(paging_pagenumber){

     htmlAcciones = '';
     htmlAcciones+='<div class="row">';
     htmlAcciones+='<div class="col-md-12" >' ;
     htmlAcciones+='<table id="lts-via" class="table table-striped" cellspacing="1" width="100%" >';
     htmlAcciones+='<thead><tr>';
     htmlAcciones+='<th align="center">Nro.</th><th align="center" class="form-nobajas-cabecera">Opciones</th><th align="left">Acción</th><th align="left">Desacripción</th></tr></thead>';
     var lista1 = $( "#opcion" ).val();
     var paging_pagesize = $( "#reg" ).val();
     var formData ={"identificador": "SERVICIO_SIERRA-431","parametros": '{"tipolista":"'+lista1+'","pagesize":'+paging_pagesize+',"pagenumber":'+paging_pagenumber+'}'};
     $.ajax({
       type     : 'GET',
       url       :'/v0.0/getToken',
       data      :'',
       success: function(token){
        $.ajax({
         type        : 'POST',            
         url         : urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
           'authorization': 'Bearer' + token,
         },
         success: function(dataIN) {
          var datos = dataIN;
          var tam = datos.length;
          if(datos == "[{ }]"){
            tam = 0;
          };
          for (var i = 0; i < tam; i++) {  
           htmlAcciones +='<tr><td align="left">'+(i+1)+'</td>';
           if(JSON.parse(datos[i].data).acciones_estado!='B') {
            htmlAcciones+='<td class="form-nobajas"><button class="btncirculo btn-xs btn-primary"  fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar(' + JSON.parse(dataIN[i].data).acciones_id + ', \''+ JSON.parse(dataIN[i].data).acciones_accion +'\',\''+JSON.parse(datos[i].data).acciones_descripcion+'\');" ><i class="glyphicon glyphicon-pencil"></i></button>  <button value="' + JSON.parse(dataIN[i].data).acciones_id + '" class="btncirculo btn-xs btn-danger" style="background:#FA8072" fa fa-plus-square pull-right" data-target="#myDelete" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + JSON.parse(dataIN[i].data).acciones_id + ');" ><i class="glyphicon glyphicon-trash"></i></button></td>';
          }else{
            if(opcion=='todo'){
             htmlAcciones+='<td>Dado de Baja</td>';   
           } 
         }
        htmlAcciones+= '<td align="left">'+ JSON.parse(datos[i].data).acciones_accion+'</td>'+
                    '<td align="left">'+ JSON.parse(datos[i].data).acciones_descripcion+'</td>'+
                    '</tr>'; 
      }
      htmlAcciones +='</table></div></div';
      htmlAcciones +='<div>';
      htmlAcciones +='<ul class="pager">';
      htmlAcciones +='<li><a href="#" style="color:blue;" onClick="btnAnterior()">Anterior</a></li>';
      htmlAcciones +='<li><a href="#" style="color:blue;" onClick="btnSiguiente()">Siguiente</a></li>';
      htmlAcciones +='</ul>';
      htmlAcciones +='</div>';
      htmlAcciones +='</div>';
      $('#listtipopuesto').html(htmlAcciones);
      if (opcion=="bajas") {
        $(".form-nobajas-cabecera").hide();
        $(".form-nobajas").hide();
      }
    },
    error: function (xhr, status, error) {
    }
  });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });  
   };


function limpiar()
{
    var id1 = $( "#id1" ).val('');
    var accion1 = $( "#accion1" ).val('');
    var descripción1 = $( "#descripción1" ).val('');
}

$( "#registrar" ).click(function() 
{
    var idu1 = idu; 
    var accion1 = $( "#accion1" ).val();
    var descripción1 = $( "#descripción1" ).val();
    $.ajax({
      type        : 'GET',
      url         :  '/v0.0/getToken',
      data        : '',
      success: function(token) {
      var formData = {"identificador": "SERVICIO_SIERRA-432","parametros": '{"xacciones_accion":"'+accion1+'", "xacciones_descripcion":"'+descripción1+'","xacciones_usuario_id":'+idu1+'}'};
      console.log(formData);
    $.ajax({
          type        : 'POST',            
          url         :  urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
            'authorization': 'Bearer ' + token,
          },
           success: function(dataIN) {
              swal( "Exito..!", "Se guardo correctamente los datos!", "success" );
              limpiar();
               listar(1);
           },
           error: function(result) {
            swal( "Error..!", "No se puedo guardar los datos", "error" );
           }
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });
});


function editar(id, accion,descripcion){
    $( "#id2" ).val(id);
    $( "#accion2" ).val(accion);
    $( "#descripción2" ).val(descripcion);
};

$( "#actualizar" ).click(function() {
    var idu1 = idu; 
    var id2 = $( "#id2" ).val();
    var accion2 = $( "#accion2" ).val();
    var descripción2 = $( "#descripción2" ).val();
     $.ajax({
      type        : 'GET',
      url         :  '/v0.0/getToken',
      data        : '',
      success: function(token) {
     var formData = {"identificador": "SERVICIO_SIERRA-433","parametros": '{"xacciones_id":'+id2+',"xacciones_accion":"'+accion2+'", "xacciones_descripcion":"'+descripción2+'","xacciones_usuario_id":'+ idu1 +'}'};
      $.ajax({
         type        : 'POST',            
         url         :  urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
             'authorization': 'Bearer ' + token,
         },
         success: function(dataIN) {
            swal( "Exito..!", "Se modifico correctamente los datos!", "success" );
         },
         error: function(result) {
          swal( "Error..!", "No se puedo mofigicar los datos", "error" );
         }
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });
    listar(1);
});


function darBaja(id)
{
  var idu1 = idu;
  swal({   title: "Esta seguro de eliminar el Tipo Puesto?",
      text: "Presione ok para eliminar el registro de la base de datos!",
      type: "warning",   showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si, Eliminar!",
      closeOnConfirm: false
    }, function(){
    $.ajax({
    type        : 'GET',
    url         :  '/v0.0/getToken',
    data        : '',
    success: function(token) {  
    var formData = {"identificador": "SERVICIO_SIERA-434","parametros": '{"xacciones_id":'+id+', "xacciones_usuario_id":'+idu1+'}'};
        $.ajax({
         type        : 'POST',            
         url         :  urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
             'authorization': 'Bearer ' + token,
         },
         success: function(dataIN) {
            swal( "Exito..!", "Se elimino correctamente los datos!", "success" );
         },
         error: function(result) {
          swal( "Error..!", "No se puede eliminar los datos", "error" );
         }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
  listar(1);
  });
};



$(document).ready(function (){
  listar(1);
}); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
                $('#uuRecaudadoraform').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
              idusuario1: {
                  message: 'La descripcion no es valida',
                  validators: {
                      notEmpty: {
                          message: 'Descripcion es requerido'
                      },
                  }
                  },
            codigo1: {
                message: 'El tipo de categoria  no es valido',
                    validators: {
                        notEmpty: {
                             message: 'El tipo de categoria  es requerido'
                                  }
                                }
                           },
            ingreso1: {
                message: 'El tipo de categoria  no es valido',
                    validators: {
                        notEmpty: {
                             message: 'El tipo de categoria  es requerido'
                                  }
                                }
                           },
                }
            }).on('error.field.bv', function(e, data) {
              if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
              }).on('success.field.fv', function(e, data) {
               if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
            });
            $('#uuRecaudadoraform2').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
             idusuario2: {
                            message: 'La descripcion no es valida',
                            validators: {
                                notEmpty: {
                                    message: 'Descripcion es requerido'
                                },
                            }
                            },
                      codigo2: {
                          message: 'El tipo de categoria  no es valido',
                              validators: {
                                  notEmpty: {
                                       message: 'El tipo de categoria  es requerido'
                                            }
                                          }
                                     },
                      ingreso2: {
                          message: 'El tipo de categoria  no es valido',
                              validators: {
                                  notEmpty: {
                                       message: 'El tipo de categoria  es requerido'
                                            }
                                          }
                                     },

                
                     }
                });
            });
</script>

<script>
var paging_pagesize = $( "#reg" ).val();
 var paging_pagenumbe = 1;

 function btnAnterior(){
  
     paging_pagenumbe--;

   listar( paging_pagenumbe);
 }

 function btnSiguiente(){

  paging_pagenumbe++;
  listar(paging_pagenumbe);
  }

  function listarSelect(){
   
   paging_pagenumbe=1;
   
   listar(paging_pagenumbe);
  };

</script>


@endpush
