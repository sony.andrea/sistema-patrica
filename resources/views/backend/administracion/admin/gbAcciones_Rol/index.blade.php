@extends('backend.template.app')
@section('main-content')
<div class="row">
   <div class="col-md-12">
      <section class="content-header">
         <h3>  
            <small>
                
            </small>
         </h3>
            <div class="row">
            <div class="col-md-6">
            </div>
          </div>
      </section>
   </div>
</div>
  <div class="row">
  <div class="col-md-12">
    <div class="col-md-3">
      <div class="header_title">
        <h4> LISTADO DE ROLES</h4>
      </div>
      <div class="box table-responsive">
      <div id="listaroles">
      
      </div>
      </div>
  </div>
<div class="col-md-4">
      <div class="header_title">
        <h4> LISTADO DE ACCIONES</h4>
      </div>
      <div class="box table-responsive">
      <div id="listaacciones">
      </div>
      </div>
  </div>  
   <div class="col-md-5">
   <div class="header_title">
        <h4> LISTADO DE ROLES - ACCIONES</h4>
      </div>
      <div class="box table-responsive">
          <div class="row">
                <div class="col-md-3">
                 <div class="form-group">
                    <label class="control-label">Mostrar Registros:</label>
                    <select class="form-control" id="reg" name="reg" onclick="listar_select()"> 
                      <option value="10" selected="selected"> 10</option>
                       <option value="30">30</option>
                       <option value="40">40</option>
                       <option value="50">50</option>
                     </select>
                  </div>
                </div>
              <div class="col-md-3">
                 <label class="control-label">Listar por:</label> 
                   <select class="form-control" id="opcion" name="opcion" onclick="listar_select()">
                    <option value="altas" >ALTAS</option>
                    <option value="bajas">BAJAS</option>
                    <option value="todo">TODO</option>
                   </select>
                 </div>
          </div>
      <div id="listaaccionesrol">
      </div>
      </div>
  </div>
<div class="row">
  <div class="col-md-12">
  <div class="col-md-3">
  </div>
  <div class="col-md-4">
    <a class="boton_personalizado" onClick="buscarAccionRol()"> < Asignar </a>
 <div class="col-md-5">
 </div>
</div>
</div>

<style type="text/css">
  .boton_personalizado{
    text-decoration: none;
    padding: 10px;
    font-weight: 600;
    font-size: 20px;
    color: #ffffff;
    background-color: #1883ba;
    border-radius: 6px;
    border: 2px solid #0016b0;
  }
  .boton_personalizado:hover{
    color: #1883ba;
    background-color: #ffffff;
  }
</style> 

@endsection
@push('scripts')
<script>
  var urlEjecutaRegla = 'http://192.168.6.113:80/MotorServicio/public/api/reglaNegocio/ejecutarWeb';
  var idu = {{$idus}};
  var asignar = [];
  var prueba=[];
  var rol1 = 0;
  var rol2 = 0;
  var cont = 0;
 
function listarRol(){ 
    htmlRoles = '';
    htmlRoles+='<div class="row">';
    htmlRoles+='<div class="col-md-6" >' ;
    htmlRoles+='<table id="lts-rol" class="table table-striped" cellspacing="0" width="100%" >';
    htmlRoles+='<th align="left">SELECCIONAR</th>';
    htmlRoles+='<th align="left">ID</th>';
    htmlRoles+='<th align="left">NOMBRE</th>';
    htmlRoles+='</thead>';
    $.ajax({
      type        : 'GET',
      url         :  '/v0.0/getToken',
      data        : '',
      success: function(token) {
        var formData = {"identificador": "SERVICIO_SIERRA-436","parametros": '{}'}; 
        $.ajax({
          type        : 'POST',            
          url         : urlEjecutaRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
          'authorization': 'Bearer '+token,
          },
        success: function(dataIN) {
                if (dataIN!="[{ }]") {
                     var tam=dataIN.length;
                  }
                     
                for (var i = 0; i < tam; i++) 
                    
                  {
                    htmlRoles+='<tr><td align="left"><input class="form-check-input" type="radio" value="'+dataIN[i].rls_id+'"  id="seleccionar" name="seleccionar" onclick="seleccionarRol()"></td>';
                    htmlRoles+='<td align="left">'+ dataIN[i].rls_id+'</td>';
                    htmlRoles+='<td align="left">'+ dataIN[i].rls_rol+'</td>';          
                    
                   htmlRoles+='</tr>';
                   
                   }
                   htmlRoles +='</table></div></div>';             
                   $('#listaroles').html(htmlRoles);
                      },
        error: function (xhr, status, error) {
        }
      });
},
error: function(result) {
  swal( "Error..!", "No se puedo listar los datos", "error" );
},
});  
};

function listarAcciones(){ 
    htmlaccion = '';
    htmlaccion+='<div class="row">';
    htmlaccion+='<div class="col-md-6" >' ;
    htmlaccion+='<table id="lts-acciones" class="table table-striped" cellspacing="0" width="100%" >';
    htmlaccion+='<th align="left">SELECCIONAR</th>';
    htmlaccion+='<th align="left">ID</th>';
    htmlaccion+='<th align="left">ACCION</th>';
    htmlaccion+='<th align="left">DESCRIPCION</th>';
    
    htmlaccion+='</thead>';
    $.ajax({
      type        : 'GET',
      url         :  '/v0.0/getToken',
      data        : '',
      success: function(token) { 
        var formData = {"identificador": "SERVICIO_SIERRA-437","parametros": '{}'};
        $.ajax({
          type        : 'POST',            
          url         : urlEjecutaRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
          'authorization': 'Bearer '+token,
          },
        success: function(dataIN) {
                if (dataIN!="[{ }]") {
                     var tam=dataIN.length;
                  }
                     
                for (var i = 0; i < tam; i++) 
                    
                  { 
                    
                    htmlaccion+='<tr><td align="left"><input class="form-check-input" type="checkbox" value="'+dataIN[i].acciones_id+'" id="seleccionar1" name="seleccionar1" onclick="incluirAccion()"></td>';

                    htmlaccion+='<td align="left">'+ dataIN[i].acciones_id +'</td>';
                     htmlaccion+='<td align="left">'+ dataIN[i].acciones_accion+'</td>';
                    htmlaccion+='<td align="left">'+ dataIN[i].acciones_descripcion+'</td>';          
                    
                   htmlaccion+='</tr>';
                   
                   }
                   htmlaccion +='</table></div></div>';             
                   $('#listaacciones').html(htmlaccion);
                      },
        error: function (xhr, status, error) {
        }
      });
},
error: function(result) {
  swal( "Error..!", "No se puedo listar los datos", "error" );
},
});  
};

//----FUNCION SELECIONA EL ROL CON UN RADIO BUTTON

function seleccionarRol(){
  var prueba1 = 0;
  $("input[name=seleccionar]").each(function (index) {  
    if($(this).is(':checked')){
      prueba1=$(this).val();
      rol1 = prueba1;
      rol2 = prueba1; 
    }
  });
 prueba1=0;
};
//-----FUNCION PARA ASIGNAR LAS ACCIONES A UN ROL
function incluirAccion(){
  cont=0;
  $("input[name=seleccionar1]").each(function (index) {  
    if($(this).is(':checked')){
      var acciones = $(this).val();
      prueba[cont]=$(this).val(); 
      cont++;
      asignar = prueba;
    }
  });
  prueba =[];
};

//------------------------------

// LISTADO DE ACCIONES-ROLES----
function listarAccionesRoles(paging_pagenumber){

     htmlARoles = '';
     htmlARoles+='<div class="row">';
     htmlARoles+='<div class="col-md-12" >' ;
     htmlARoles+='<table id="lts-via" class="table table-striped" cellspacing="1" width="100%" >';
     htmlARoles+='<thead><tr>';
     htmlARoles+='<th align="center">Nro.</th><th align="center" class="form-nobajas-cabecera">Opciones</th><th align="left">Rol</th><th align="left">Acciones</th></tr></thead>';
     var lista1 = $( "#opcion" ).val();
     var paging_pagesize = $( "#reg" ).val();
     var formData ={"identificador": "SERVICIO_SIERRA-439","parametros": '{"xtipolista":"'+lista1+'","paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumber+'}'};

     $.ajax({
       type     : 'GET',
       url       :'/v0.0/getToken',
       data      :'',
       success: function(token){
        console.log('tok : ' +token);
        $.ajax({
         type        : 'POST',            
         url         : urlEjecutaRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
           'authorization': 'Bearer' + token,
         },
         success: function(dataIN) {
          console.log(dataIN);
          var datos = dataIN;
          var tam = datos.length;
          if(datos == "[{ }]"){
            tam = 0;
          };
          for (var i = 0; i < tam; i++) {  
          htmlARoles +='<tr><td align="left">'+(i+1)+'</td>';
          if(JSON.parse(datos[i].data).acciones_rol_estado!='B') {
            htmlARoles+='<td align="left">';
            htmlARoles += '<button class="btn btn-info btn-sm" id = "cuota"  data-toggle="modal" data-target="#detallecuota" onclick="darBaja(' + JSON.parse(dataIN[i].data).acciones_rol_id + ')" > <i class="glyphicon glyphicon-th-list"></i> Desasignar </button>';
           
                }
              htmlARoles+= '<td align="left">'+ JSON.parse(datos[i].data).rls_rol+'</td>'+
                          '<td align="left">'+ JSON.parse(datos[i].data).acciones_accion+'</td>'+
                          '</tr>';
            }
            htmlARoles +='</table></div></div';
            htmlARoles +='<div>';
            htmlARoles +='<ul class="pager">';
            htmlARoles +='<li><a href="#" style="color:blue;" onClick="btnAnterior()">Anterior</a></li>';
            htmlARoles +='<li><a href="#" style="color:blue;" onClick="btnSiguiente()">Siguiente</a></li>';
            htmlARoles +='</ul>';
            htmlARoles +='</div>';
            htmlARoles +='</div>';
            $('#listaaccionesrol').html(htmlARoles);
            if (opcion=="bajas") {
              $(".form-nobajas-cabecera").hide();
              $(".form-nobajas").hide();
            }

          },
        error: function (xhr, status, error) {
          }
        });
      },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },

  });  
};

//--FUNCUION BUSCAR ROL Y ACCIONES 
function buscarAccionRol(){ 
   $.ajax({
            type        : 'GET',
            url         :  '/v0.0/getToken',
            data        : '',
            success: function(token) {
              var idrol = rol2;
              var idaccion = asignar;
              var  tam1 = idaccion.length;
              for(var j=0; j<tam1; j++){
               var idaccion2 = idaccion[j];
              var formData = {"identificador": 'SERVICIO_SIERRA-455',"parametros": '{"rol_id":'+idrol+', "accion_id":'+idaccion2+'}'};
                 $.ajax({
                      type        : 'POST',            
                      url         : urlEjecutaRegla,
                      data        : formData,
                      dataType    : 'json',
                      crossDomain : true,
                      headers: {
                      'authorization': 'Bearer '+token,
                      },
                       success: function( data ){
                       console.log(data,'valor devuelto de buscar');
                       if (data[0].sp_buscar_rol_existente == false){
                         InsertarAccionRol();
                       }else{
                          swal('la acción ya ha sido asignado al rol');
                       }

                       },
                       error: function( result ) {                               
                       }
                   }); 
                } 
               listar()
               },
            error: function(result) {
            swal( "Error..!", "No se puedo guardar los datos", "error" );
            },
      });    
}

// FUNCION LISTAR EN UN FOR DE LISTAR ACCIONES ROLES
function listar()
{
  $.ajax({
    type     : 'GET',
    url       :'/v0.0/getToken',
    data      :'',
    success: function(token){
    var idrol1 = rol1;
    console.log(rol1,'ververveer');
    var asignar2 = asignar;
    var  tam = asignar2.length;
    for(var i=0; i<tam; i++){
    listarAccionesRoles(1);
          $.ajax({
            type        : 'POST',            
            url         : urlEjecutaRegla,
            data        : formData,
            dataType    : 'json',
            crossDomain : true,
            headers: {
            'authorization': 'Bearer' + token,
            },
            success: function(data){
            },
            error: function(result) {
              console.log(result);
            }
          });
        }
      },
   });
}


//--FUNCION INSERTAR--INSERTA A UN ROL VARIAS ACCIONES--
function InsertarAccionRol(){   
  $.ajax({
    type     : 'GET',
    url       :'/v0.0/getToken',
    data      :'',
    success: function(token){
    var idrol1 = rol1;
    console.log(rol1,'ververveer');
    var asignar2 = asignar;
    var  tam = asignar2.length;
    for(var i=0; i<tam; i++){
    var asignar3 = asignar2[i];
    var formData = {"identificador": 'SERVICIO_SIERRA-440',"parametros": '{"xacciones_rol_rls_id":'+idrol1+',"xacciones_rol_acciones_id":'+asignar3+',"xacciones_rol_usuario_id":'+idu+'}'};
          $.ajax({
            type        : 'POST',            
            url         : urlEjecutaRegla,
            data        : formData,
            dataType    : 'json',
            crossDomain : true,
            headers: {
            'authorization': 'Bearer' + token,
            },
            success: function(data){
              
              swal( "Exito..!", "Se Registro correctamente el contribuyente...!", "success" );
              asignar3 = 0;
              rol1 = 0; 
            },
            error: function(result) {
              console.log(result);
            }
          });
        }
      },
   });
};

// FUNCION DAR BAJA ACCION
function darBaja(id)
{
  var idu1 = idu;
  swal({   title: "Esta seguro de desasignar la acción?",
      text: "Presione ok para eliminar el registro de la base de datos!",
      type: "warning",   showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si, Eliminar!",
      closeOnConfirm: false
    }, function(){
    $.ajax({
    type        : 'GET',
    url         :  '/v0.0/getToken',
    data        : '',
    success: function(token) {  
    var formData = {"identificador": "SERVICIO_SIERRA-442","parametros": '{"xacciones_rol_id":'+id+', "xacciones_rol_usuario_id":'+idu1+'}'};
        $.ajax({
         type        : 'POST',            
         url         :  urlEjecutaRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
             'authorization': 'Bearer ' + token,
         },
         success: function(dataIN) {
            swal( "Exito..!", "Se elimino correctamente los datos!", "success" );
         },
         error: function(result) {
          swal( "Error..!", "No se puede eliminar los datos", "error" );
         }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
  listarAccionesRoles(1);
  });
};



//--------------------------- 
$(document).ready(function (){
  listarRol();
  listarAcciones();
  listarAccionesRoles(1);
  
}); 
</script>
<script>
var paging_pagesize = $( "#reg" ).val();
 var paging_pagenumbe = 1;

 function btnAnterior(){
  
     paging_pagenumbe--;

   listarAccionesRoles( paging_pagenumbe);
 }

 function btnSiguiente(){

  paging_pagenumbe++;
  listarAccionesRoles(paging_pagenumbe);
  }

  function listar_select(){
   
   paging_pagenumbe=1;
   
   listarAccionesRoles(paging_pagenumbe);
  };

</script>


@endpush
