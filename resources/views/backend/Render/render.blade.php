@extends('backend.template.app')
@section('main-content')
<!DOCTYPE html>
<!-- To run the current sample code in your own environment, copy this to an html page. -->

<html>
<head>
  <link href="samples.css" rel="stylesheet" />
</head>
<body>
 <div id="panelopcion"  > 
        <div class="panel panel-info col-md-12">
           <center><h3> GENERADOR DE VISTAS </h3>            
           <div class="row">          
              <label class="control-spam col-md-4">META DATA:</label>
              <div class="control-spam col-md-4">                        
                  <textarea class="form-control" rows="5" id="metaData"></textarea>
              </div> 
           </div><br>
             <div class="row">          
              <label class="control-spam col-md-4">DATA:</label>
              <div class="control-spam col-md-4">                        
                  <textarea class="form-control" rows="5" id="data"></textarea>
              </div> 
           </div>
          <div class="row"> <br>         
              <label class="control-spam col-md-4">TIPO VISTA:</label>
              <div class="control-spam col-md-4">                        
                  <select class="form-control" id="tipoVista" >
                  <option value="1" selected="selected" > LISTAR</option>
                  <option value="2">ADICIONAR</option>                          
                  </select>
              </div> 
              <div class="control-spam col-md-4">                        
                <button class="btn btn-primary" class="btn btn-primary" onclick="ejecutaRender()">Crear</button>
              </div> 
           </div>
           </center> 
       </div> 
</div>
<div  class="panel panel-info" id="listar" style="display: none;">
          <br>
          <div class="row">
            <div class="col-md-12">
             <div class="form-group col-md-12">
               <label class="control-spam col-md-12">RESULTADO </label>
              </div>  
              <div class="form-group col-md-12">
                <div class="form-group col-md-6">
                  <table>
                    <thead id="movieListc"></thead>
                    <tbody id="movieList"></tbody>
                  </table>
                  <div id="registrar">
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <textarea class="form-control" rows="15" id="resul"></textarea>
                </div>
                 <div class="control-spam col-md-4">                        
                <button class="btn btn-primary"  onclick="refrescar()">Volver</button>
              </div>
              </div>            
                             </div>
         </div>
</div>
<div class="panel panel-info"  id="adicionar" style="display: none;">
          <br>
          <div class="row">
            <div class="col-md-12">
             <div class="form-group col-md-12">
               <label class="control-spam col-md-12">RESULTADO </label>
              </div>  
              <div class="form-group col-md-12">
                <div class="form-group col-md-6">
                  <table>
                    <thead id="movieListc"></thead>
                    <tbody id="movieList"></tbody>
                  </table>
                  <div id="registrar1">
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <textarea class="form-control" rows="15" id="resul1"></textarea>
                </div>
                  <div class="control-spam col-md-4">                        
                <button class="btn btn-primary"  onclick="refrescar()">Volver</button>
              </div>
              </div>            
               </div>
         </div>
</div>


<script id="cabecera" type="text/x-jsrender">
  <tr>
    <td>[%:espacio1%]</td> <td>[%:espacio2%]</td>
  </tr>
</script>

<script id="movieTemplate" type="text/x-jsrender">
  <tr>
    <td> <b>[%:acciones_id%]</b></td>
    <td>
      [%:acciones_accion_data%]
    </td>
  </tr>
</script>

<script id="columnTemplate" type="text/x-jsrender">
  <div>
   [%:espacio1%]
  </div>
</script>

<script id="rowTemplate" type="text/x-jsrender">
  <span>
    <b>[%:name%]</b>
  </span>
</script>

<script id="conditionalTemplate" type="text/x-jsrender">
  [%if name.charAt(0)==='E' tmpl='rowTemplate'%]
  [%else tmpl='columnTemplate'%]
  [%/if%]
</script>

<script id="formulario" type="text/x-jsrender">
  <form action="">
    [%for datos %]
      [%if tipo==='texto' tmpl='texto'%]
      [%else%]
        [%if tipo==='numero' tmpl='numero'%]
        [%else%]
          [%if tipo==='password' tmpl='password'%]
          [%else%]
            [%if tipo==='checkbox' tmpl='checkbox'%]
            [%else%]
              [%if tipo==='combo' tmpl='combo'%]
              [%else%]
                [%if tipo==='checkboxMultiple' tmpl='checkboxMultiple'%]
                [%else%]
                  [%if tipo==='textarea' tmpl='textarea'%]
                  [%else tmpl='ninguna'%]
                  [%/if%]
                [%/if%]
              [%/if%]
            [%/if%]
          [%/if%]
        [%/if%]
      [%/if%]
    [%/for%]
    <br>
    <button type="submit" class="btn btn-success btn-sm">Registrar</button>
  </form>
</script>

<script id="password" type="text/x-jsrender">
  <label for="[%:descripcion%]">[%:descripcion%]</label><br>
  <input type="password" class="form-control" id="[%:descripcion%]" aria-describedby="emailHelp" placeholder="[%:descripcion%]">
</script>

<script id="checkbox" type="text/x-jsrender">
  <label for="[%:descripcion%]">[%:descripcion%]</label><br>
  <label><input data-link="{toBool:gender:toString}" type="checkbox" /> [%:descripcion%]</label>
  <br>
</script>

<script id="texto" type="text/x-jsrender">
  <label for="[%:descripcion%]">[%:descripcion%]</label><br>
  <input type="text" class="form-control" id="[%:descripcion%]" aria-describedby="emailHelp" placeholder="[%:descripcion%]">
</script>

<script id="numero" type="text/x-jsrender">
  <label for="[%:descripcion%]">[%:descripcion%]</label><br>
  <input type="number" class="form-control" id="[%:descripcion%]" aria-describedby="emailHelp" placeholder="[%:descripcion%]">
</script>

<script id="combo" type="text/x-jsrender">
  <label for="[%:descripcion%]">[%:descripcion%]</label><br>
  <select class="form-control" id="[%:descripcion%]">
    [%for data%]
      <option value="[%:valor%]">[%:dato%]</option>
    [%/for%]
  </select>
  <br>
</script>

<script id="checkboxMultiple" type="text/x-jsrender">
  <label for="[%:descripcion%]">[%:descripcion%]</label><br>
  [%for data%]
    <label><input id="[%:clave%]" type="checkbox" /> [%:dato%]</label>
  [%/for%]
  <br>
</script>

<script id="textarea" type="text/x-jsrender">
  <label for="exampleTextarea">[%:descripcion%]</label>
  <textarea class="form-control" id="[%:descripcion%]" rows="3"></textarea>
  <br>
</script>

<script id="ninguna" type="text/x-jsrender">
  <br>
</script>


<script type="text/javascript">
  $.views.settings.delimiters("[%", "%]");
  var movieTemplate11  = $.templates( "movieTemplate11", { markup: "#movieTemplate"  });
  var movieTemplate12  = $.templates( "cabecera", { markup: "#cabecera" });
  var form  = $.templates( "formulario", {
    markup: "#formulario",
    templates: { texto: "#texto", numero: "#numero", checkbox: "#checkbox", password: "#password", combo: "#combo", checkboxMultiple: "#checkboxMultiple", textarea: "#textarea" }
  });
  var checkbox  = $.templates( "checkbox", { markup: "#checkbox"  });
  var numero  = $.templates( "numero", { markup: "#numero" });
  function ejecutaRender(){  
    var cabeceraData = $("#data").val();
    cabeceraData = JSON.parse(cabeceraData);
    console.log(999, cabeceraData);
    var movies1 =  $("#metaData").val();  
    movies1 = JSON.parse(movies1);
    var tex = [{"tipo":"texto","descripcion":"Nombre"}];
    var tipo = $("#tipoVista").val();
    console.log(tipo,233);
      switch (tipo) {
        case '1':
          document.getElementById("panelopcion").style="display:none;";
          document.getElementById("listar").style="display:block;";
          var c = movieTemplate12.render(cabeceraData);
          var h = movieTemplate11.render(movies1);
          $( "#movieListc" ).html(c);
          $( "#movieList" ).html(h);
          $("#resul").val(h);
          break;
        case '2':
          document.getElementById("panelopcion").style="display:none;";
          document.getElementById("listar").style="display:block;";      
          var i = form.render(movies1);
          $( "#registrar" ).html(i);
          $("#resul").val(i);
          break;
        default:
     console.log("Sorry, we are out of ");
  }
}
function refrescar(){  
location.reload();
}
</script>   
</body>
</html>
@endsection