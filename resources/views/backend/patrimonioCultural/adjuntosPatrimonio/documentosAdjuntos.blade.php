@extends('backend.template.app')
@section('main-content')
@include('backend.patrimonioCultural.adjuntosPatrimonio.partial.modalCreateAdjuntos')
@include('backend.patrimonioCultural.adjuntosPatrimonio.partial.modalVerImagen')
@include('backend.patrimonioCultural.adjuntosPatrimonio.partial.modalVerPdf')
@include('backend.patrimonioCultural.adjuntosPatrimonio.partial.modalUpdate')
<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>Documentos Adjuntos4</b></div>
    <div class="panel-body">     
      <br>
      <div class="form-group">
        <label for="exampleInputEmail1" class="col-sm-2 control-label">FICHA</label>
        <div class="col-sm-4">
          <span class="block input-icon input-icon-right">
            <input type="text" id="ficha" name="ficha" class="form-control" disabled>
            <input type="text" id="g_tipo" name="g_tipo" class="form-control" disabled >
          </span>
        </div>
        <label for="exampleInputEmail1" class="col-sm-2 control-label">PROCESO</label>
        <div class="col-sm-4">
          <span class="block input-icon input-icon-right">
            <input type="text" id="proceso" name="proceso" class="form-control" disabled>
          </span>
        </div>
      </div>
    </div>
  </div>
  <div class="row" id="adjuntosEsculturas">
    <div class="col-md-12">
      <div class="col-md-4">
      </div>
      <div class="col-md-2">     
        <a class= "btn btn-primary" data-target= "#myCreateAdjuntos" data-toggle= "modal"><i class="glyphicon glyphicon-plus"></i> Nuevo Adjunto</a>
      </div>
      <div class="col-md-2">       
        <a type = "submit" class = "btn btn-primary" data-dismiss="modal" id="botonImprimirFicha" onclick="imprimirFicha()"> <i class="glyphicon glyphicon-print"></i> Imprimir Ficha </a>  
      </div> 
      <div class="col-md-2">
        <a type = "submit" class = "btn btn-primary" data-dismiss="modal" id="botonArchivoFotografico" onclick="imprimirArchivoFotografico()"> <i class="glyphicon glyphicon-print"></i> Archivo Fotografico </a>  
      </div>
      <div class="col-md-2">
        <a type = "submit" class = "btn btn-primary" data-dismiss="modal" id="botonTodo" onclick="imprimirFichaTodo()"> <i class="glyphicon glyphicon-print"></i> Imprimir Todo </a>  
      </div>  
    </div>  
  </div>          
  <div class= "row">
    <div class= "col-md-12">
      <div class= "box">
        <div class= "box-body">
          <div id="listAdjuntos">
          </div>  
        </div>
      </div>
    </div>
 </div>
  <div class="row" id="atrasEscultura">
    <div class="col-md-12">
      <a type="button" href="{{ url('/indexEscultura') }}"><i class="fa fa-arrow-left"></i>VOLVER ATRAS</a>
    </div>
  </div>
    <div class="row" id="atrasInmueble">
    <div class="col-md-12">
      <a type="button" href="{{ url('/indexInmuebles') }}"><i class="fa fa-arrow-left"></i>VOLVER ATRAS</a>
    </div>
  </div>
    <div class="row" id="atrasConjunto">
    <div class="col-md-12">
      <a type="button" href="{{ url('/indexConjunto') }}"><i class="fa fa-arrow-left"></i>VOLVER ATRAS</a>
    </div>
  </div>
    <div class="row" id="atraspatrInmaterial">
    <div class="col-md-12">
      <a type="button" href="{{ url('/indexInmaterial') }}"><i class="fa fa-arrow-left"></i>VOLVER ATRAS</a>
    </div>
  </div>
    <div class="row" id="atrasEspacioAbierto">
    <div class="col-md-12">
      <a type="button" href="{{ url('/indexEspacioAbierto') }}"><i class="fa fa-arrow-left"></i>VOLVER ATRAS</a>
    </div>
  </div>
    <div class="row" id="atraPatrArque">
    <div class="col-md-12">
      <a type="button" href="{{ url('/indexArqueologicos') }}"><i class="fa fa-arrow-left"></i>VOLVER ATRAS</a>
    </div>
  </div>
</section>

@endsection
@push('scripts')
<script>
var _usrid = {{$usuarioId}};
var casosId = {{$casosId}};
var numeroficha = {{$numeroficha}};
$("#cnumeroficha").val(numeroficha);
$("#ccasosId").val(casosId);
$("#anumeroficha").val(numeroficha);
$("#aidcasosesc22").val(numeroficha);

function imprimirFicha(){
  setTimeout(function(){

    var g_tipo = $("#g_tipo").val();

    if (g_tipo == 'PTR-ESP') {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerar',['datos'=>'d1']) }}";
    }
    if (g_tipo == 'PTR-CON') {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerar',['datos'=>'d1']) }}";
    }
    if ( g_tipo == 'PTR_IME' ) {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerar',['datos'=>'d1']) }}"; 
    } 
    if ( g_tipo == 'PTR_BMA' ) {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerar',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR-PCI' ) {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerar',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR_CBI' ) {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerar',['datos'=>'d1']) }}";
    }  
    urlPdf = urlPdf.replace('d1', casosId);
    window.open(urlPdf); 
  }, 500);
}

function irAtras()
{   
  setTimeout(function(){
  var g_tipo = $("#g_tipo").val();
  if (g_tipo == 'PTR-ESP') {
    document.getElementById("atrasEspacioAbierto").style = "display:block;";
  }
  if (g_tipo == 'PTR-CON') {
    document.getElementById("atrasConjunto").style = "display:block;";
  }
  if ( g_tipo == 'PTR_IME' ) {
    document.getElementById("atrasEscultura").style = "display:block;";
  } 
  if ( g_tipo == 'PTR_BMA' ) {
    document.getElementById("atraPatrArque").style = "display:block;";
  }
  if ( g_tipo == 'PTR-PCI' ) {
    document.getElementById("atraspatrInmaterial").style = "display:block;";
  }
  if ( g_tipo == 'PTR_CBI' ) {
    document.getElementById("atrasInmueble").style = "display:block;";
  }  
}, 500);
}

function imprimirArchivoFotografico(){
  setTimeout(function(){

    var g_tipo =$("#g_tipo").val();
    if (g_tipo == 'PTR-ESP') {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarArchFoto',['datos'=>'d1']) }}";
    }
    if (g_tipo == 'PTR-CON') {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerarArchFoto',['datos'=>'d1']) }}";
    }
    if ( g_tipo == 'PTR_IME' ) {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerarArchFoto',['datos'=>'d1']) }}"; 
    }  
    if ( g_tipo == 'PTR_BMA' ) {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerarArchFoto',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR-PCI' ) {
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerarArchFoto',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR_CBI' ) {      
      revisarImagenesDuplicadas(numeroficha,g_tipo); 
    } 
  
    urlPdf = urlPdf.replace('d1', casosId);
    window.open(urlPdf); 
  }, 500);
}
function imprimirFichaTodo(){
  setTimeout(function(){

    var g_tipo = $("#g_tipo").val();

    if (g_tipo == 'PTR-ESP') {
      var urlTodo = "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarFichaTodo',['datos'=>'d1']) }}";
    }
    if ( g_tipo == 'PTR_CBI' ) {
      var urlTodo = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR_IME' ) {
      var urlTodo = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR-CON' ) {
      var urlTodo = "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR_BMA' ) {
      var urlTodo = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
    }
    if ( g_tipo == 'PTR-PCI' ) {
      var urlTodo = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
    }
    urlTodo = urlTodo.replace('d1', casosId);

    window.open(urlTodo); 
  }, 500);
}

function revisarImagenesDuplicadas(numeroficha, g_tipo){
   $.ajax(
  {
    type        : 'GET',
    url         : '/prueba/VALLE/public/v0.0/getToken',
    data        : '',
    success: function(token)
    {
      var formData = {"identificador": "SERVICIO_PATRIMONIO-944","parametros": '{"xid_doccasos":'+numeroficha+',"xg_tipo":"'+g_tipo+'"}'};          
      $.ajax(
      {
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: 
        {
          'authorization': 'Bearer '+token,
        },
        success: function( dataIN )
        {           
         var tam = dataIN.length;
         var posicion = [5,6,7,8,9,10];
         var sw = 0;
         var cont = 0;
         for (var i = 0; i < posicion.length; i++){
           for (var j = 0; j < dataIN.length; j++){
            if(posicion[i] == parseInt(dataIN[j].xdoc_correlativo)){
              cont++;              
            }
          }
          if(cont > 1){
            sw = 1;
            break; 
           }
          cont = 0;
         }
         if(sw == 1){
          swal( "Revisar..!", "Hay posiciones duplicadas", "warning" );
         }
         else{
           var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerarArchFoto',['datos'=>'d1']) }}";
           urlPdf = urlPdf.replace('d1', casosId);
          window.open(urlPdf); 
         }
        },
        error: function( result ) 
        {
          console.log("Error al recuperar registro de Imagenes");
        }
      });

    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo recuperar los datos", "error" );
    },
  });
}

function listFrCasos(casosId)
{
  $.ajax(
  {
    type        : 'GET',
    url         : '/prueba/VALLE/public/v0.0/getToken',
    data        : '',
    success: function(token)
    {
      var formData = {"identificador": "SERVICIO_PATRIMONIO-935","parametros": '{"xid_casos":'+casosId+'}'};
      $.ajax(
      {
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: 
        {
          'authorization': 'Bearer '+token,
        },
        success: function( dataIN )
        {         
          
          var codigo_ficha = dataIN[0].xcas_nombre_caso;
          var g_tipo = codigo_ficha.substring(0, 7);
          //Muestra en ficha
          $("#ficha").val(codigo_ficha);
          $("#g_tipo").val(g_tipo);
          //Para registrar
          $("#cg_tipo").val(g_tipo);
          //Para actualizar
          $("#aag_tipo").val(g_tipo);

          if (g_tipo == 'PTR-CON') {
            $("#proceso").val("CONJUNTOS PATRIMONIALES");
          
          }
          if (g_tipo == 'PTR_IME') {
            $("#proceso").val("ESCULTURAS"); 
          
          }
          if (g_tipo == 'PTR-ESP') {
            $("#proceso").val("ESPACIOS ABIERTOS");
          
          }
          if (g_tipo == 'PTR-PCI') {
            $("#proceso").val("PATRIMONIO INMATERIAL");
          
          }
          if (g_tipo == 'PTR_BMA') {
            $("#proceso").val("BIENES ARQUEOLOGICOS");
          
          }
          if (g_tipo == 'PTR_CBI') {
            $("#proceso").val("BIENES INMUEBLES");
            
          }
        },
        error: function( result ) 
        {
          console.log("Error al recuperar registro de fr casos");
        }
      });

    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo recuperar los datos", "error" );
    },
  });  
};

function listAdjuntos(){
  setTimeout(function(){

    var g_tipo = $("#g_tipo").val();  
    if (g_tipo == 'PTR-CON') {
      var doc_proceso = 26;
    }
    if (g_tipo == 'PTR_IME') {
      var doc_proceso = 28;
    }
    if (g_tipo == 'PTR-ESP') {
      var doc_proceso = 27;
    }
    if (g_tipo == 'PTR-PCI') {
      var doc_proceso = 30;
    }
    if (g_tipo == 'PTR_BMA') {
      var doc_proceso = 29;
    }
    if (g_tipo == 'PTR_CBI') {
      var doc_proceso = 31;
    }

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-adjuntos" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th width="12%" align="center">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Titulo</th>';
    htmlListaBus += '<th align="left" class="form-natural">Descripcion</th>';
    htmlListaBus += '<th align="left" class="form-natural">Posición</th>';
    htmlListaBus += '<th align="left" class="form-natural">Imagen</th>';
    htmlListaBus += '</thead>'; 
    $.ajax({
      type        : 'GET',
      url         :  '/prueba/VALLE/public/v0.0/getToken',
      data        : '',
      success: function(token) {
        var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1046',"parametros": '{"xid_doccasos":' +numeroficha+ ', "xdoc_proceso":' +doc_proceso+ '}'};  
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
            'authorization': 'Bearer '+token,
          },
          success: function(dataIN) {
            var datos = dataIN;            
            var tam = datos.length;
            if(dataIN == "[{ }]"){
              tam = 0;
            }; 
            for (var i = 0; i < tam; i++){ 
              var doc_idd = dataIN[i].xdoc_idd;
              var doc_descripcion = dataIN[i].xdoc_datos;
              var doc_titulo = dataIN[i].xdoc_titulo;
              var doc_url = dataIN[i].xdoc_url; 
              var doc_tipo_documento = dataIN[i].xdoc_tipo_documento;//f
              var doc_url_logica = dataIN[i].xdoc_url_logica;
              var doc_tps_doc_id = dataIN[i].xdoc_tps_doc_id;//f
              var doc_cuerpo = dataIN[i].xdoc_cuerpo;
              var doc_tipo_documentacion = dataIN[i].xdoc_palabras;
              var doc_nombre = dataIN[i].xdoc_nombre;
              var long_nombre = doc_nombre.length;
              var tipo_ext = doc_nombre.substring(long_nombre-3, long_nombre);               
              var doc_url = '/'+doc_url_logica;
              var doc_url_pdf = '/img/adobe.jpg';
              var doc_url_doc = '/img/images_doc.png';
              var doc_url_dwg = '/img/dwg-icon.jpg';
              var url_dwg_docx = '/'+doc_url_logica;             
              htmlListaBus += '<tr>';
              htmlListaBus += '<td align="left">'+(i+1)+'</td>';
              htmlListaBus += '<td align="left">';          
              htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#UpdateAdjunto" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar(\''+g_tipo+'\','+ doc_idd +','+ casosId +',\''+doc_url_logica+'\',\''+ doc_cuerpo +'\',\''+ doc_tipo_documentacion +'\',\''+ doc_titulo +'\',\''+ doc_descripcion +'\',\''+ tipo_ext +'\');" ><i class="glyphicon glyphicon-pencil"></i></button> ';       
              htmlListaBus+='<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + doc_idd +');">';
              htmlListaBus+='<i class="glyphicon glyphicon-trash"></i></button>';
              if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ) {
              htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myModalVerImagen" data-toggle="modal" data-placement="top" title="Ver Imagen" type="button" onClick="verImagen(' + doc_idd +',\''+doc_url_logica+'\',\''+doc_titulo+'\',\''+doc_descripcion+'\')" ><i class="glyphicon glyphicon-new-window"></i></button>';
              }
              if (tipo_ext == 'pdf' || tipo_ext == 'PDF') {
              htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myModalVerPdf" data-toggle="modal" data-placement="top" title="Ver Pdf" type="button" onClick="verPdf(' + doc_idd +',\''+doc_url_logica+'\',\''+doc_titulo+'\',\''+doc_descripcion+'\')" ><i class="glyphicon glyphicon-new-window"></i></button>';
              }
              if (tipo_ext == 'dwg' || tipo_ext == 'DWG' || tipo_ext == 'doc' || tipo_ext == 'DOC' || tipo_ext == 'ocx' || tipo_ext == 'OCX') {
               htmlListaBus+='<a href="'+url_dwg_docx+'" id="doc_url_a" name="doc_url_a"><button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-placement="top" title="Descargar Documento" type="button"><i class="glyphicon glyphicon-new-window"></i></button></a>';
              }

              htmlListaBus += '</td>';  
              htmlListaBus += '<td align="left" class="form-natural">'+doc_titulo+'</td>'; 
              htmlListaBus += '<td align="left" class="form-natural">'+doc_descripcion+'</td>';
              htmlListaBus += '<td align="left" class="form-natural">'+doc_tipo_documentacion+'</td>';
              if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ) {
              htmlListaBus += '<td align="left" class="form-natural"><img src="'+doc_url+'"  width="50" heigth="50"></td>';
              }
              if (tipo_ext == 'dwg' || tipo_ext == 'DWG') {    

               htmlListaBus += '<td align="left" class="form-natural"><img src="'+doc_url_dwg+'"  width="50" heigth="50"></td>'; 
              }
              if (tipo_ext == 'pdf' || tipo_ext == 'PDF') {
                htmlListaBus += '<td align="left" class="form-natural"><img src="'+doc_url_pdf+'"  width="50" heigth="50"></td>';
              }
              if (tipo_ext == 'doc' || tipo_ext == 'DOC' || tipo_ext == 'ocx' || tipo_ext == 'OCX') {
                htmlListaBus += '<td align="left" class="form-natural"><img src="'+doc_url_doc+'"  width="50" heigth="50"></td>';
              }
              htmlListaBus += '</tr>';
            } 
            htmlListaBus += '</table></div></div>';
            $( '#listAdjuntos' ).html(htmlListaBus);
            $( '#lts-adjuntos' ).DataTable(
            {
              "language": {"url": "lenguaje"},
              "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            });

          },
          error: function(result) {
            swal( "Error..!", "Error....", "error" );
          }
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });
  }, 500);
};

function verPdf(doc_idd, doc_url_logica, doc_titulo, doc_descripcion){

  
  $("#pdf_url").attr("src",'/'+doc_url_logica);
  $("#titulo_pdf").html(doc_titulo);
  

}

function verImagen(doc_idd, doc_url_logica, doc_titulo, doc_descripcion){

  $("#idAdjunto").val(doc_idd);
  $("#doc_url_logica").attr("src",'/'+doc_url_logica);
  $("#tituloImagen").html(doc_titulo);
  $("#descripcionImagen").html(doc_descripcion);

}

function darBaja(id)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  '/prueba/VALLE/public/v0.0/getToken',
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-940  ","parametros": '{"ydoc_idd":'+id+', "ydoc_usuario":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {

            listAdjuntos(); 
            swal("Patrimonio!", "Fue eliminado correctamente!", "success");

          },
          error: function( result ) 
          {
            swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
          }
        });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  

};

function editar(g_tipo,doc_idd,casosId,doc_url_logica,doc_cuerpo,doc_tipo_documentacion,doc_titulo,doc_descripcion,tipo_ext){  
  var valorx = $( "#aag_tipo" ).val();
  var xposicion; 
  if (valorx == 'PTR-CON') {  

     if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ) {

      $("#aposicionconjunto").val(doc_tipo_documentacion);
      document.getElementById("apanelFotografiaConjunto").style="display:block;";
      document.getElementById("apanelFotografiaEscultura").style="display:none;";
      document.getElementById("apanelFotografiaEspacioAbierto").style="display:none;";
      document.getElementById("apanelFotografiaBienArq").style="display:none;";
      document.getElementById("apanelFotografiaPatInmaterial").style="display:none;";
      document.getElementById("apanelFotografiaBienInmueble").style="display:none;";
    }       
}

if(valorx == 'PTR_IME')
{
    if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ) {     
    $("#aposicionescultura").val(doc_tipo_documentacion);
    document.getElementById("apanelFotografiaEscultura").style="display:block;";
    document.getElementById("apanelFotografiaEspacioAbierto").style="display:none;";
    document.getElementById("apanelFotografiaConjunto").style="display:none;";
    document.getElementById("apanelFotografiaBienArq").style="display:none;";
    document.getElementById("apanelFotografiaPatInmaterial").style="display:none;";
    document.getElementById("apanelFotografiaBienInmueble").style="display:none;";
  }
}
if(valorx == 'PTR-ESP')
{

 if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ) {

  $("#aposicionespacioabierto").val(doc_tipo_documentacion);
  document.getElementById("apanelFotografiaEscultura").style="display:none;";
  document.getElementById("apanelFotografiaEspacioAbierto").style="display:block;";
  document.getElementById("apanelFotografiaConjunto").style="display:none;";
  document.getElementById("apanelFotografiaBienArq").style="display:none;";
  document.getElementById("apanelFotografiaPatInmaterial").style="display:none;";
  document.getElementById("apanelFotografiaBienInmueble").style="display:none;";
}

}
if (valorx == 'PTR_BMA')
{
  if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ) {
    $("#aposicionesBienArq").val(doc_tipo_documentacion);
    document.getElementById("apanelFotografiaEscultura").style="display:none;";
    document.getElementById("apanelFotografiaEspacioAbierto").style="display:none;";
    document.getElementById("apanelFotografiaConjunto").style="display:none;";
    document.getElementById("apanelFotografiaBienArq").style="display:block;";
    document.getElementById("apanelFotografiaPatInmaterial").style="display:none;";
    document.getElementById("apanelFotografiaBienInmueble").style="display:none;";
  }
}
if (valorx == 'PTR-PCI') {

   if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ) {

    $("#aposicionesPatInmaterial").val(doc_tipo_documentacion);
    document.getElementById("apanelFotografiaEscultura").style="display:none;";
    document.getElementById("apanelFotografiaEspacioAbierto").style="display:none;";
    document.getElementById("apanelFotografiaConjunto").style="display:none;";
    document.getElementById("apanelFotografiaBienArq").style="display:none;";
    document.getElementById("apanelFotografiaPatInmaterial").style="display:block;";
    document.getElementById("apanelFotografiaBienInmueble").style="display:none;";
  }

}

if (valorx == 'PTR_CBI') {

 if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ){

  $("#aposicionesBienInmueble").val(doc_tipo_documentacion);
  document.getElementById("apanelFotografiaBienInmueble").style="display:block;";
  document.getElementById("apanelFotografiaEscultura").style="display:none;";
  document.getElementById("apanelFotografiaEspacioAbierto").style="display:none;";
  document.getElementById("apanelFotografiaConjunto").style="display:none;";
  document.getElementById("apanelFotografiaBienArq").style="display:none;";
  document.getElementById("apanelFotografiaPatInmaterial").style="display:none;";
 }
}

  if (tipo_ext == 'jpg' || tipo_ext == 'peg' || tipo_ext == 'JPG' || tipo_ext == 'PEG' ){
   
    var doc_cuerpo='Fotografias';
    $("#atipificacion").val(doc_cuerpo);
    $("#atip_data").val(doc_cuerpo);
  }

  if (tipo_ext == 'dwg' || tipo_ext == 'DWG'){
    
    var doc_cuerpo='Autocad';
    $("#atipificacion").val(doc_cuerpo);
    $("#atip_data").val(doc_cuerpo);
    var otros= 'Otro';
    $("#aposicionesBienInmueble").val(otros);
  }

  if ( tipo_ext == 'doc' || tipo_ext == 'DOC' || tipo_ext == 'ocx' || tipo_ext == 'OCX' || tipo_ext == 'pdf' || tipo_ext == 'PDF'){
    var doc_cuerpo='Texto';
    $("#atipificacion").val(doc_cuerpo);
    $("#atip_data").val(doc_cuerpo);
    var otros= 'Otro';
    $("#aposicionesBienInmueble").val(otros);
  }
$("#aidAdjunto").val(doc_idd);
$("#acasosId").val(casosId);
$("#atitulo").val(doc_titulo);
$("#adescripcion").val(doc_descripcion);
}
  function refrescarPantalla(){
  location.reload();
  }

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         : '/prueba/VALLE/public/v0.0/getToken',
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SERVICIO_PATRIMONIO-1023","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
          var usuario_avatar = dataIN[0].vusr_usuario;
       $("#cusuario_avatarad").val(usuario_avatar);
     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};

$(document).ready(function (){  
  listFrCasos(casosId);
  listAdjuntos();
  cargarDatosUsuario();

  document.getElementById("panelTitulo").style = "display:none;";
  document.getElementById("panelFotografiaEscultura").style = "display:none;";
  document.getElementById("panelFotografiaEspacioAbierto").style = "display:none;";
  document.getElementById("panelFotografiaConjunto").style = "display:none;";
  document.getElementById("panelFotografiaBienArq").style = "display:none;";
  document.getElementById("panelFotografiaPatInmaterial").style = "display:none;";
  document.getElementById("panelFotografiaBienInmueble").style = "display:none;";
  document.getElementById("paneltipodescripcion").style = "display:none;";
  document.getElementById("apanelFotografiaEscultura").style = "display:none;";
  document.getElementById("apanelFotografiaEspacioAbierto").style = "display:none;";
  document.getElementById("apanelFotografiaConjunto").style = "display:none;";
  document.getElementById("apanelFotografiaBienArq").style = "display:none;";
  document.getElementById("apanelFotografiaPatInmaterial").style = "display:none;";
  document.getElementById("apanelFotografiaBienInmueble").style = "display:none;";
  //-----Atras

  document.getElementById("atrasEscultura").style = "display:none;";
  document.getElementById("atrasInmueble").style = "display:none;";
  document.getElementById("atrasConjunto").style = "display:none;";
  document.getElementById("atraspatrInmaterial").style = "display:none;";
  document.getElementById("atrasEspacioAbierto").style = "display:none;";
  document.getElementById("atraPatrArque").style = "display:none;";
  irAtras();
});
</script>
@endpush