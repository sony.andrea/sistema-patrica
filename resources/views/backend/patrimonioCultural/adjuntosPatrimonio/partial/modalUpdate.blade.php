<style type="text/css">
  #tdborde{
      border: 1px solid #E0E0E0;
      border-collapse: border-bottom-width;
      padding:5;
      text-align : justify;
  }  
</style>

<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="UpdateAdjunto" tabindex="-5">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <form  class="form-horizontal" role="form" method="POST" action="/UpdatePersonaData"  enctype="multipart/form-data" id="frmActualizar" name="frmActualizar" onsubmit='return validarup();'>
        <!--Inicia content-->
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-12 container-fluit">
              <div class="panel panel-info">
                <div class="panel-heading">
                  <h3>
                   Actualizar Adjuntos
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h3>
              </div>
              <div class="panel-body">
                <div class="caption">
                  <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
                  <input type="hidden" name="aidcasosesc22" id="aidcasosesc22">
                  <input type="hidden" name="aag_tipo" id="aag_tipo">
                  <input type="hidden" name="aidAdjunto" id="aidAdjunto">
                  <input type="hidden" name="acasosId" id="acasosId">
                  <input type="hidden" name="anumeroficha" id="anumeroficha">
                  <div class="col-md-12">
                    <div class="col-md-12 form-group">
                      <label class="control-label">Documento Adjunto</label>
                      <input type="file" class="form-control" name="aimagen1" id="aimagen1">             
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-12 form-group">
                    <label class="control-label">Tipificación:</label>                      
                    <select  id="atipificacion" name="atipificacion" class="form-control" onclick="tipoAdjuntoup(this)" disabled="true">
                       <option value="">Seleccionar</option>
                       <option value="Fotografias">Fotografias</option>
                       <option value="Autocad">Autocad</option>
                       <option value="Texto">Texto</option>                                         
                    </select>
                  <input type="hidden" name="atip_data" id="atip_data">
                </div>
              </div>
              <!--Begin Fotografias-->
              <div id="apanelTitulo">
                <div class="col-md-12">
                  <div class="panel-heading">
                    <label class="control-label" style="color:#275c26";>
                      OPCIONES:
                    </label>
                  </div>
                </div>
              </div>
              <div id="apanelFotografiaEscultura" >
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Posición:
                    </label>
                    <select class="form-control" id="aposicionescultura" name="aposicionescultura" onchange="buscarAdjuntosup(this.value)">
                          <option value="N"> Selecionar...</option>
                          <option value="Fotografia Frente">Fotografia Frente</option>
                          <option value="Fotografia Perfil">Fotografia Perfil</option>
                          <option value="Fotografia incluyendo pedestal y placas">Fotografia incluyendo pedestal y placas</option>
                          <option value="Plano de Ubicacion">Plano ubicacion</option>
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro"> Otro</option> 
                    </select>
                  </div>               
                </div>
              </div>
              <div id="apanelFotografiaEspacioAbierto" >
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Posición:
                    </label>
                    <select class="form-control" id="aposicionespacioabierto" name="aposicionespacioabierto" onchange="buscarAdjuntosup(this.value)">
                      <option value="N"> Selecionar...</option> 
                      <option value="Plano de ubicacion">Plano de Ubicacion</option>
                      <option value="Esquema arquitectonico">Esquema Arquitectonico</option>
                      <option value="Angulo Visual Horizontal">Angulo Visual Horizontal</option>
                      <option value="Fotografia 1">Fotografia 1</option>
                      <option value="Fotografia 2">Fotografia 2</option>
                      <option value="Fotografia 3">Fotografia 3</option>
                      <option value="Fotografia 4">Fotografia 4</option>
                      <option value="Fotografia 5">Fotografia 5</option>
                      <option value="Fotografia 6">Fotografia 6</option>
                      <option value="Otro">Otro</option>  
                    </select> 
                  </div>               
                </div>
              </div>
              <div id="apanelFotografiaConjunto">
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Posición:
                    </label>
                    <select class="form-control" id="aposicionconjunto" name="aposicionconjunto" onchange="buscarAdjuntosup(this.value)">
                          <option value="N"> Selecionar...</option> 
                          <option value="Imagen 1 perfil urbano">Imagen 1 Perfil Urbano</option>
                          <option value="Imagen 2 perfil urbano">Imagen 2 Perfil Urbano</option>
                          <option value="Perfil corte esquematico">Perfil Corte Esqematico</option>
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro"> Otro</option>                   
                    </select> 
                  </div>               
                </div>
              </div>
              <div id="apanelFotografiaBienArq" >
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Posición:
                    </label>
                    <select class="form-control" id="aposicionesBienArq" name="aposicionesBienArq" onchange="buscarAdjuntosup(this.value)">
                          <option value="N"> Selecionar...</option> 
                          <option value="Fotografia del Plano de Ubicacion">Fotografia del Plano de Ubicación</option>
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro">Otro</option> 
                    </select> 
                  </div>               
                </div>
              </div>
              <div id="apanelFotografiaPatInmaterial" >
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Posición:
                    </label>
                    <select class="form-control" id="aposicionesPatInmaterial" name="aposicionesPatInmaterial" onchange="buscarAdjuntosup(this.value)">
                          <option value="N"> Selecionar...</option> 
                          <option value="Fotografia Principal 1">Fotografia Principal 1</option>
                          <option value="Fotografia Principal 2">Fotografia Principal 2</option>                   
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro">Otro</option>
                    </select> 
                  </div>               
                </div>
              </div>
              <div id="apanelFotografiaBienInmueble" >
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Posición:
                    </label>
                    <select class="form-control" id="aposicionesBienInmueble" name="aposicionesBienInmueble" onchange="buscarAdjuntosup(this.value)">
                          <option value="N"> Selecionar...</option> 
                          <option value="Foto Principal">Foto Principal</option>
                          <option value="Plano de Ubicacion">Plano de Ubicacion</option>
                          <option value="Tipologia Arquitectonica">Tipologia Arquitectonica</option>
                          <option value="Perfil de Acera">Perfil de Acera</option>                      
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro">Otro</option>  
                    </select> 
                  </div>               
                </div>
              </div>
              <div id="apaneltipodescripcion">
                <div class="col-md-12">
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Título:
                    </label>
                    <input id="atitulo" name="atitulo" type="text" class="form-control"  placeholder="Título">
                  </div>
                  <div class="form-group col-md-12">
                    <label class="control-label">
                      Descripción:
                    </label>
                    <input id="adescripcion" name="adescripcion" type="text" class="form-control"  placeholder="Descripción">
                  </div>
                </div>
              </div> 
              <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                      <i class="fa fa-close"></i> Cancelar</button>  
                <button type="submit" class = "btn btn-primary"><i class="fa fa-save"></i>Modificar</button>                      
                </div>                                                   
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end content--> 
  </form>
</div>
</div>
</div>

<script type="text/javascript">

  function validarup(){
    console.log();

    var g_tipo = $('#aag_tipo').val();
    var todo_correcto = true;   
    var casosId;

    if (g_tipo == 'PTR_CON') {
      if(document.getElementById('aposicionconjunto').value == ''){
        todo_correcto = false;
      }
    }
    if (g_tipo == 'PTR_IME') {

      if(document.getElementById('aposicionescultura').value == ''){
        todo_correcto = false;
      }
    }
    if (g_tipo == 'PTR-ESP') {

      if(document.getElementById('aposicionespacioabierto').value == ''){
        todo_correcto = false;
      }
    }
    if (g_tipo == 'PTR_BMA') {

      if(document.getElementById('aposicionesBienArq').value == ''){
        todo_correcto = false;
      }
    }
    if (g_tipo == 'PTR-PCI') {

      if(document.getElementById('aposicionesPatInmaterial').value == ''){
        todo_correcto = false;
      }
    }
    if (g_tipo == 'PTR_CBI') {
      if(document.getElementById('aposicionesBienInmueble').value == ''){
        todo_correcto = false;
      }
    }
    if(!todo_correcto){
      alert('Seleccione un elemento de la lista de Posición');
    }
    return todo_correcto;
  }

  function buscarAdjuntosup(val){
    casosId =$("#aidcasosesc22").val();
    var g_tipo = $('#aag_tipo').val();
    var xdoc_correlativo=0;
    if (g_tipo == 'PTR_CON') {         
      if (val == 'Imagen 1 perfil urbano') {
        xdoc_correlativo = 1;             
      }
      if (val == 'Imagen 2 perfil urbano') {
        xdoc_correlativo = 2;
      }
      if (val =='Perfil corte esquematico') {
        xdoc_correlativo = 3;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 4;                       
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 5;                        
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 6;                        
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 7;                        
      }
      if (val =='Fotografia 5') {                        
        xdoc_correlativo = 8;                        
      }
      if ( val =='Fotografia 6') {
        xdoc_correlativo = 9;                        
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';
      }

    }
    if (g_tipo == 'PTR_IME') {
      if (val == 'Fotografia de frente') {
        xdoc_correlativo = 1;            
      }
      if (val == 'Fotografia de Perfil') {
        xdoc_correlativo = 2;
      }
      if (val =='Fotografia Incluyendo el Pedestal y Placas') {
        xdoc_correlativo = 3;
      }
      if (val =='Plano ubicacion') {
        xdoc_correlativo = 4;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 5;
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 6;
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 7;
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 8;
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 9;
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 10;
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';                       
      }
    }
    if (g_tipo == 'PTR-ESP') {
      if (val =='Plano de ubicacion') {
        xdoc_correlativo = 1;             
      }
      if (val =='Esquema arquitectonico') {
        xdoc_correlativo = 2;
      }
      if (val =='Angulo Visual Horizontal') {
        xdoc_correlativo = 3;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 4;

      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 5;                     
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 6;                                                
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 7;                        
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 8;                        
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 9;                        
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';                       
      }

    }
    if (g_tipo == 'PTR_BMA') {
      if (val =='Plano de ubicacion') {
        xdoc_correlativo = 1;            
      }

      if (val =='Fotografia 1') {
        xdoc_correlativo = 2;                        
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 3;

      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 4;                        
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 5;                        
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 6;                        
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 7;                        
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';            
      }
    }
    if (g_tipo == 'PTR-PCI') {
      if (val =='Fotografia Principal 1') {
        xdoc_correlativo = 1;          
      }
      if (val =='Fotografia Principal 2') {
        xdoc_correlativo = 2;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 3;                        
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 4;                        
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 5;

      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 6;

      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 7;

      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 8;

      }
      if (val =='Otro') {
        xdoc_correlativo = 9;

      }
    }
    if (g_tipo == 'PTR_CBI') {
      if (val =='Foto Principal') {
        xdoc_correlativo = 1;
      }
      if (val =='Plano de ubicacion') {
        xdoc_correlativo = 2;
      }
      if (val =='Tipologia Arquitectonica') {
        xdoc_correlativo = 3;
      }
      if (val =='Perfil de Acera') {
        xdoc_correlativo = 4;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 5;
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 6;
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 7;
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 8;
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 9;
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 10;
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';            
      }
    }
    if ( xdoc_correlativo != 'Otro') {
    $.ajax({
      type        : 'GET',
      url         :  '/prueba/VALLE/public/v0.0/getToken',
      data        : '',
      success: function(token) {
        console.log('************************',casosId);
         var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1160',"parametros": '{"yid_doccasos":' +casosId+ ',"ycorrelativo":"' +xdoc_correlativo+ '"}'}; 
        console.log(formData);
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
            'authorization': 'Bearer '+token,
          },
          success: function(dataIN) {
            var datos = dataIN;
            console.log(dataIN,'buscar posicion xdoc_correlativo');
            var tam = datos.length;
            if(dataIN != "[{}]"){
              swal( "Alerta..!", "El tipo de fotografía para esta Posición ya fue registrada", "warning" );
              document.getElementById("aposicionescultura").value='';
              document.getElementById("aposicionespacioabierto").value='';
              document.getElementById("aposicionconjunto").value='';
              document.getElementById("aposicionesBienArq").value='';
              document.getElementById("aposicionesPatInmaterial").value='';
              document.getElementById("aposicionesBienInmueble").value='';
            }
          },      
          error: function(result) {
           swal( "Error..!", "Error ....", "error" );
          },
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });
  }
  };


  function tipoAdjuntoup(v){    
   var g_tipo = $('#aag_tipo').val();
   if (g_tipo == 'PTR_CON') {
    if (v.value=='Fotografias') {
      document.getElementById("apanelTitulo").style="display:block;";
      document.getElementById("apanelFotografiaConjunto").style="display:block;";
      document.getElementById("apaneltipodescripcion").style="display:block;";

    }
    if (v.value=='Mapas o Planos') {
      document.getElementById("apanelTitulo").style="display:block;";
      document.getElementById("apanelFotografiaConjunto").style="display:none;";
      document.getElementById("apaneltipodescripcion").style="display:block;";
    } 


  }
  if (g_tipo == 'PTR_IME') {

    if (v.value=='Fotografias') {
      document.getElementById("apanelTitulo").style="display:block;";
      document.getElementById("apanelFotografiaEscultura").style="display:block;";
      document.getElementById("apaneltipodescripcion").style="display:block;";

    }
    if (v.value=='Mapas o Planos') {
      document.getElementById("apanelTitulo").style="display:block;";
      document.getElementById("apanelFotografiaEscultura").style="display:none;";
      document.getElementById("apaneltipodescripcion").style="display:block;";
    } 

  }
  if (g_tipo == 'PTR-ESP') {
    if (v.value=='Fotografias') {
      document.getElementById("apanelTitulo").style="display:block;";
      document.getElementById("apanelFotografiaEspacioAbierto").style="display:block;";
      document.getElementById("apaneltipodescripcion").style="display:block;";
    }
    if (v.value=='Mapas o Planos') {
      document.getElementById("apanelTitulo").style="display:block;";
      document.getElementById("apanelFotografiaEspacioAbierto").style="display:none;";
      document.getElementById("apaneltipodescripcion").style="display:block;";
    } 

  }
  if (g_tipo == 'PTR_BMA') {
   if (v.value=='Fotografias') {
    document.getElementById("apanelTitulo").style="display:block;";
    document.getElementById("apanelFotografiaBienArq").style="display:block;";
    document.getElementById("apaneltipodescripcion").style="display:block;";
  }
  if (v.value=='Mapas o Planos') {
    document.getElementById("apanelTitulo").style="display:block;";
    document.getElementById("apanelFotografiaBienArq").style="display:none;";
    document.getElementById("apaneltipodescripcion").style="display:block;";
  } 

}
if (g_tipo == 'PTR-PCI') {
  if (v.value=='Fotografias') {
    document.getElementById("apanelTitulo").style="display:block;";
    document.getElementById("apanelFotografiaPatInmaterial").style="display:block;";
    document.getElementById("apaneltipodescripcion").style="display:block;";
  }
  if (v.value=='Mapas o Planos') {
    document.getElementById("apanelTitulo").style="display:block;";
    document.getElementById("apanelFotografiaPatInmaterial").style="display:none;";
    document.getElementById("apaneltipodescripcion").style="display:block;";
  }

}
if (g_tipo == 'PTR_CBI') {
  if (v.value=='Fotografias') {
    document.getElementById("apanelTitulo").style="display:block;";
    document.getElementById("apanelFotografiaBienInmueble").style="display:block;";
    document.getElementById("apaneltipodescripcion").style="display:block;";
  }
  if (v.value=='Mapas o Planos') {
    document.getElementById("apanelTitulo").style="display:block;";
    document.getElementById("apanelFotografiaBienInmueble").style="display:none;";
    document.getElementById("apaneltipodescripcion").style="display:block;";
  }
}
}
</script>