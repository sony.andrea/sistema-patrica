<style type="text/css">
  #tdborde{
      border: 1px solid #E0E0E0;
      border-collapse: border-bottom-width;
      padding:5;
      text-align : justify;
  }  
</style>
<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myModalVerImagen" tabindex="-5">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical verImagen-validate','id'=>'formVer_imagen'])!!}    
      <!--Inicia content-->
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 container-fluit">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h3>
                  Imagen
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h3>
              </div>
              <div class="panel-body">
                <div class="caption">
                  <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
                  <input type="hidden" name="idAdjunto" id="idAdjunto">
                  <div class="row"> 
                  <div class="col-xs-12">
                      <center>
                        <table width="" id="tdborde">
                         <tr>
                           <td>
                             <br>
                           </td>
                         </tr>
                          <tr>
                            <td>
                              <img src="" id="doc_url_logica" name="doc_url_logica" class="img-responsive col-xs-12">                                       
                            </td>
                          </tr>
                          <tr>
                            <td align="center"><br>
                              <h4><b><div id="tituloImagen"></div></b></h4>
                              <br>
                            </td>
                          </tr>
                        </table>
                      </center>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                      <i class="fa fa-close"></i> Cancelar</button>                     
                    </div>                                                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--end content--> 
        {!! Form::close() !!}
      </div>
   </div>
</div>
