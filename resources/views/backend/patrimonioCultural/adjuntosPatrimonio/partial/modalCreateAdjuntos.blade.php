<style type="text/css">
  form#pago [required]{  
  }  
</style>
<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myCreateAdjuntos" tabindex="-5">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <form  class="form-horizontal" role="form" method="POST" action="/UpdatePersonaImg"  enctype="multipart/form-data" id="frmAnadir" name="frmAnadir" onsubmit='return validar()'>
      <!--Inicia content-->
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 container-fluit">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h3>
                 Adjuntar Documentos
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h3>
              </div>
              <div class="panel-body">
                <div class="caption">
                  <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
                  <input type="hidden" name="cnumeroficha" id="cnumeroficha">
                  <input type="hidden" name="ccasosId" id="ccasosId">
                  <input type="hidden" name="cg_tipo" id="cg_tipo">
                  <input type="hidden" name="cusuario_avatarad" id="cusuario_avatarad">
                  <div class="col-md-12">
                    <div class="col-md-12 form-group">
                      <label class="control-label">Documento Adjunto</label>
                      <input type="file" id="file" class="form-control" name="imagen" id="imagen" accept="image/jpeg, .jpg ,application/pdf, .dwg, .doc, .docx" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-12 form-group">
                      <label class="control-label">Tipificación:</label>                      
                      <select  id="ctipificacion" name="ctipificacion" class="form-control" onclick="tipoAdjunto(this)" required/>
                        <option selected value="">Seleccione...</option>
                        <option value="Fotografias">Fotografias</option>
                        <option value="Autocad">Autocad</option>
                        <option value="Texto">Texto</option>                        
                      </select>
                    </div>
                  </div>
                  <!--Begin Fotografias-->
                  <div id="panelTitulo">
                    <div class="col-md-12">
                      <div class="panel-heading">
                        <label class="control-label" style="color:#275c26";>
                          OPCIONES:
                        </label>
                      </div>
                    </div>
                  </div>
                  <div id="panelFotografiaEscultura" style="display: none;">
                    <div class="col-md-12">
                      <div class="form-group col-md-12">
                        <label class="control-label">
                          Posición:
                        </label>
                        <select class="form-control" id="cposicionescultura" name="cposicionescultura" onchange="buscarAdjuntos(this.value)">
                          <option value=""> Selecionar...</option>
                          <option value="Fotografia Frente">Fotografia Frente</option>
                          <option value="Fotografia Perfil">Fotografia Perfil</option>
                          <option value="Fotografia incluyendo pedestal y placas">Fotografia incluyendo pedestal y placas</option>
                          <option value="Plano de Ubicacion">Plano ubicacion</option>
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro"> Otro</option> 
                        </select> 
                      </div>               
                    </div>
                    </div>
                    <div id="panelFotografiaEspacioAbierto" style="display: none;">
                    <div class="col-md-12">
                      <div class="form-group col-md-12">
                        <label class="control-label">
                          Posición:
                        </label>
                        <select class="form-control" id="cposicionespacioabierto" name="cposicionespacioabierto" onchange="buscarAdjuntos(this.value)">
                          <option value=""> Selecionar...</option> 
                          <option value="Plano de ubicacion">Plano de Ubicacion</option>
                          <option value="Esquema arquitectonico">Esquema Arquitectonico</option>
                          <option value="Angulo Visual Horizontal">Angulo Visual Horizontal</option>
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro">Otro</option> 
                        </select> 
                      </div>               
                    </div>
                    </div>
                    <div id="panelFotografiaConjunto" style="display: none;">
                    <div class="col-md-12">
                      <div class="form-group col-md-12">
                        <label class="control-label">
                          Posición:
                        </label>
                        <select class="form-control" id="cposicionconjunto" name="cposicionconjunto" onchange="buscarAdjuntos(this.value)">
                          <option value=""> Selecionar...</option> 
                          <option value="Imagen 1 perfil urbano">Imagen 1 Perfil Urbano</option>
                          <option value="Imagen 2 perfil urbano">Imagen 2 Perfil Urbano</option>
                          <option value="Perfil corte esquematico">Perfil Corte Esqematico</option>
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro"> Otro</option>                   
                        </select> 
                      </div>               
                    </div>
                    </div>
                    <div id="panelFotografiaBienArq" style="display: none;">
                    <div class="col-md-12">
                      <div class="form-group col-md-12">
                        <label class="control-label">
                          Posición:
                        </label>
                        <select class="form-control" id="cposicionesBienArq" name="cposicionesBienArq" onchange="buscarAdjuntos(this.value)">
                          <option value=""> Selecionar...</option> 
                          <option value="Fotografia del Plano de Ubicacion">Fotografia del Plano de Ubicación</option>
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro">Otro</option> 
                        </select> 
                      </div>               
                    </div>
                    </div>
                     <div id="panelFotografiaPatInmaterial" style="display: none;">
                    <div class="col-md-12">
                      <div class="form-group col-md-12">
                        <label class="control-label">
                          Posición:
                        </label>
                        <select class="form-control" id="cposicionesPatInmaterial" name="cposicionesPatInmaterial" onchange="buscarAdjuntos(this.value)">
                          <option value=""> Selecionar...</option> 
                          <option value="Fotografia Principal 1">Fotografia Principal 1</option>
                          <option value="Fotografia Principal 2">Fotografia Principal 2</option>                   
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro">Otro</option> 
                        </select> 
                      </div>               
                    </div>
                    </div>
                     <div id="panelFotografiaBienInmueble" style="display: none;">
                    <div class="col-md-12">
                      <div class="form-group col-md-12">
                        <label class="control-label">
                          Posición:
                        </label>
                        <select class="form-control" id="cposicionesBienInmueble" name="cposicionesBienInmueble" onchange="buscarAdjuntos(this.value)">
                          <option value=""> Selecionar...</option> 
                          <option value="Foto Principal">Foto Principal</option>
                          <option value="Plano de Ubicacion">Plano de Ubicacion</option>
                          <option value="Tipologia Arquitectonica">Tipologia Arquitectonica</option>
                          <option value="Perfil de Acera">Perfil de Acera</option>                      
                          <option value="Fotografia 1">Fotografia 1</option>
                          <option value="Fotografia 2">Fotografia 2</option>
                          <option value="Fotografia 3">Fotografia 3</option>
                          <option value="Fotografia 4">Fotografia 4</option>
                          <option value="Fotografia 5">Fotografia 5</option>
                          <option value="Fotografia 6">Fotografia 6</option>
                          <option value="Otro">Otro</option> 
                        </select> 
                      </div>               
                    </div>
                    </div>
                    <div id="paneltipodescripcion">
                      <div class="col-md-12">
                      <div class="form-group col-md-12">
                        <label class="control-label">
                          Título:
                        </label>
                        <input id="ctitulo" name="ctitulo" type="text" class="form-control"  placeholder="Título">
                      </div>
                       <div class="form-group col-md-12">
                        <label class="control-label">
                          Descripción:
                        </label>
                        <input id="cdescripcion" name="cdescripcion" type="text" class="form-control"  placeholder="Descripción">
                      </div>
                      </div>
                    </div>
                    
                    <div class="modal-footer">
                      <button class="btn btn-primary" data-dismiss="modal" type="button">
                        <i class="fa fa-close"></i> Cancelar</button>
                        <button class="btn btn-default "  type="button" onclick="limpiarFoto()">
                          <i class="fa fa-eraser"></i> Limpiar
                        </button>
                        <button type="submit" class = "btn btn-primary"><i class="fa fa-save"></i>Guardar</button>                        
                      </div>                                                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--end content--> 
          </form>
        </div>
   </div>
</div>
<script type="text/javascript">



  function validar(){
   

    var g_tipo = $('#cg_tipo').val();

    var todo_correcto = true;   

    if (g_tipo == 'PTR_CON') {
      if(document.getElementById('ctipificacion').value == 'Fotografias')
      {
        if(document.getElementById('cposicionconjunto').value == ''){
          todo_correcto = false;
        }
      }

    }
    if (g_tipo == 'PTR_IME') {
      if(document.getElementById('ctipificacion').value == 'Fotografias')
      {
        if(document.getElementById('cposicionescultura').value == ''){
          todo_correcto = false;
        }
      }

    }
    if (g_tipo == 'PTR-ESP') {
      if(document.getElementById('ctipificacion').value == 'Fotografias')
      {
        if(document.getElementById('cposicionespacioabierto').value == ''){
          todo_correcto = false;
        }
      }
    }
    if (g_tipo == 'PTR_BMA') {
      if(document.getElementById('ctipificacion').value == 'Fotografias')
      {
        if(document.getElementById('cposicionesBienArq').value == ''){
          todo_correcto = false;
        }
      }
    }
    if (g_tipo == 'PTR-PCI') {
      if(document.getElementById('ctipificacion').value == 'Fotografias')
      {
        if(document.getElementById('cposicionesPatInmaterial').value == ''){
          todo_correcto = false;
        }
      }
    }
    if (g_tipo == 'PTR_CBI') {
      if(document.getElementById('ctipificacion').value == 'Fotografias')
      {
        if(document.getElementById('cposicionesBienInmueble').value == ''){
          todo_correcto = false;
        }
        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(.jpg|.jpeg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Por favor subir archivos con extension .jpeg/.jpg ');
            fileInput.value = '';
            return false;
        }      
      }
      if (document.getElementById('ctipificacion').value == 'Autocad') {
        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(.dwg)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Por favor subir archivos con extension .dwg');
            fileInput.value = '';
            return false;
        }      
      }
       if (document.getElementById('ctipificacion').value == 'Texto') {
        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(.pdf|.doc |.docx)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Por favor subir archivos con extension .pdf/.doc/.docx ');
            fileInput.value = '';
            return false;
        }      
      }

    }
    
    if(!todo_correcto){
      alert('Seleccione un elemento de la lista de Posición');
    }

    return todo_correcto;
  }

  function buscarAdjuntos(val){
    var casosId =$("#cnumeroficha").val();
    var g_tipo = $('#cg_tipo').val();
    var xdoc_correlativo=0;
    if (g_tipo == 'PTR_CON') {         
      if (val == 'Imagen 1 perfil urbano') {
        xdoc_correlativo = 1;             
      }
      if (val == 'Imagen 2 perfil urbano') {
        xdoc_correlativo = 2;
      }
      if (val =='Perfil corte esquematico') {
        xdoc_correlativo = 3;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 4;                       
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 5;                        
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 6;                        
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 7;                        
      }
      if (val =='Fotografia 5') {                        
        xdoc_correlativo = 8;                        
      }
      if ( val =='Fotografia 6') {
        xdoc_correlativo = 9;                        
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';
      }

    }
    if (g_tipo == 'PTR_IME') {
      if (val == 'Fotografia de frente') {
        xdoc_correlativo = 1;            
      }
      if (val == 'Fotografia de Perfil') {
        xdoc_correlativo = 2;
      }
      if (val =='Fotografia Incluyendo el Pedestal y Placas') {
        xdoc_correlativo = 3;
      }
      if (val =='Plano ubicacion') {
        xdoc_correlativo = 4;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 5;
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 6;
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 7;
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 8;
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 9;
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 10;
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';                       
      }
    }
    if (g_tipo == 'PTR-ESP') {
      if (val =='Plano de ubicacion') {
        xdoc_correlativo = 1;             
      }
      if (val =='Esquema arquitectonico') {
        xdoc_correlativo = 2;
      }
      if (val =='Angulo Visual Horizontal') {
        xdoc_correlativo = 3;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 4;

      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 5;                     
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 6;                                                
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 7;                        
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 8;                        
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 9;                        
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';                       
      }

    }
    if (g_tipo == 'PTR_BMA') {
      if (val =='Plano de ubicacion') {
        xdoc_correlativo = 1;            
      }

      if (val =='Fotografia 1') {
        xdoc_correlativo = 2;                        
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 3;

      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 4;                        
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 5;                        
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 6;                        
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 7;                        
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';            
      }
    }
    if (g_tipo == 'PTR-PCI') {
      if (val =='Fotografia Principal 1') {
        xdoc_correlativo = 1;          
      }
      if (val =='Fotografia Principal 2') {
        xdoc_correlativo = 2;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 3;                        
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 4;                        
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 5;

      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 6;

      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 7;

      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 8;

      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';

      }
    }
    if (g_tipo == 'PTR_CBI') {
      if (val =='Foto Principal') {
        xdoc_correlativo = 1;
      }
      if (val =='Plano de ubicacion') {
        xdoc_correlativo = 2;
      }
      if (val =='Tipologia Arquitectonica') {
        xdoc_correlativo = 3;
      }
      if (val =='Perfil de Acera') {
        xdoc_correlativo = 4;
      }
      if (val =='Fotografia 1') {
        xdoc_correlativo = 5;
      }
      if (val =='Fotografia 2') {
        xdoc_correlativo = 6;
      }
      if (val =='Fotografia 3') {
        xdoc_correlativo = 7;
      }
      if (val =='Fotografia 4') {
        xdoc_correlativo = 8;
      }
      if (val =='Fotografia 5') {
        xdoc_correlativo = 9;
      }
      if (val =='Fotografia 6') {
        xdoc_correlativo = 10;
      }
      if (val =='Otro') {
        xdoc_correlativo = 'Otro';            
      }
    }

    if ( xdoc_correlativo != 'Otro') {
          $.ajax({
            type        : 'GET',
            url         :  '/prueba/VALLE/public/v0.0/getToken',
            data        : '',
            success: function(token) {
              var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1160',"parametros": '{"yid_doccasos":' +casosId+ ',"ycorrelativo":"' +xdoc_correlativo+ '"}'}; 
              console.log(formData);
              $.ajax({
                type        : 'POST',            
                url         : urlRegla,
                data        : formData,
                dataType    : 'json',
                crossDomain : true,
                headers: {
                  'authorization': 'Bearer '+token,
                },
                success: function(dataIN) {
                  var datos = dataIN;
                  console.log(dataIN,'buscar posicion xdoc_correlativo');
                  var tam = datos.length;
                  if(dataIN != "[{}]"){
                    swal( "Alerta..!", "El tipo de fotografía para esta Posición ya fue registrada", "warning" );
                    document.getElementById("cposicionescultura").value='';
                    document.getElementById("cposicionespacioabierto").value='';
                    document.getElementById("cposicionconjunto").value='';
                    document.getElementById("cposicionesBienArq").value='';
                    document.getElementById("cposicionesPatInmaterial").value='';
                    document.getElementById("cposicionesBienInmueble").value='';
                    
                  }
                },      
                error: function(result) {
                  swal( "Error..!", "Error....", "error" );
                },
              });
            },
            error: function(result) {
              swal( "Error..!", "No se puedo guardar los datos", "error" );
            },
          });
       }
  };



  function tipoAdjunto(v){    
     var g_tipo = $('#cg_tipo').val();
         if (g_tipo == 'PTR_CON') {
              if (v.value=='Fotografias') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaConjunto").style="display:block;";
                document.getElementById("paneltipodescripcion").style="display:block;";

              }
              if (v.value=='Autocad' || v.value=='Texto') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaConjunto").style="display:none;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              } 
        }
            if (g_tipo == 'PTR_IME') {
                   
              if (v.value=='Fotografias') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaEscultura").style="display:block;";
                document.getElementById("paneltipodescripcion").style="display:block;";

              }
              if (v.value=='Autocad' || v.value=='Texto') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaEscultura").style="display:none;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }

            }
            if (g_tipo == 'PTR-ESP') {
              if (v.value=='Fotografias') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaEspacioAbierto").style="display:block;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }
              if (v.value=='Autocad' || v.value=='Texto') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaEspacioAbierto").style="display:none;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }     
            }
            if (g_tipo == 'PTR_BMA') {
               if (v.value=='Fotografias') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaBienArq").style="display:block;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }
              if (v.value=='Autocad' || v.value=='Texto') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaBienArq").style="display:none;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              } 
           
            }
            if (g_tipo == 'PTR-PCI') {
              if (v.value=='Fotografias') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaPatInmaterial").style="display:block;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }
              if (v.value=='Autocad' || v.value=='Texto') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaPatInmaterial").style="display:none;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }
         
            }
            if (g_tipo == 'PTR_CBI') {
              if (v.value=='Fotografias') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaBienInmueble").style="display:block;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }
               if (v.value=='Autocad' || v.value=='Texto') {
                document.getElementById("panelTitulo").style="display:block;";
                document.getElementById("panelFotografiaBienInmueble").style="display:none;";
                document.getElementById("paneltipodescripcion").style="display:block;";
              }
             
            }
  }
</script>
