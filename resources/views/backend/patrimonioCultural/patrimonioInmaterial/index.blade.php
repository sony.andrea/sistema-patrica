@extends('backend.template.app')
@section('main-content')

@include('backend.patrimonioCultural.patrimonioInmaterial.partial.modalCreate')
@include('backend.patrimonioCultural.patrimonioInmaterial.partial.modalUpdateP2')
@include('backend.patrimonioCultural.patrimonioInmaterial.partial.modalUpdateP4')

<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>BUSQUEDA DE FICHAS</b></div>
    <div class="panel-body">     
      <br>
      <div class="col-md-12">
       <div class="form-group col-md-4">
        <label class="control-label">
          Tipo Patrimonio Cultural:
        </label>
       <input id="tipoPatriCultural" type="text" class="form-control" value="PATRIMONIO INMATERIAL" disabled>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Campo:
        </label>
        <select class="form-control" name="camposPorPatrimonio" id="camposPorPatrimonio">
          <option value="-1">Seleccionar...</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Valor:
        </label>
        <input type="text" id="valorPatrimonio" class="form-control" name="valorPatrimonio">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="container">
        <div class="col-md-6"></div>
        <div class="col-md-6"> 
          <button type ="button" class = "btn btn-primary" onclick="buscarPatrimonioTipoCampo(1)"> <i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-primary" onclick="limpiarPrincipal()"> <i class="fa fa fa-eraser"></i> Limpiar</button>
          <button type="button" class="btn btn-primary" data-target="#myCreatePatriInmaterial" 
          data-toggle="modal"><i class="fa fa-plus"></i> Nueva Ficha</button>
       
     </div>
   </div>
 </div>
</div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-body">
        <div id="listPatrimonio">
        </div>  
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@push('scripts')
<script>

  
  var _usrid={{$usuarioId}};
  var correlativog="";

  function buscarPatrimonioPorTipo(paging_pagenumbe)
  { 
    document.getElementById("listado_campo_valor").style = "display:none;";
    document.getElementById("listado_principal").style = "display:block;";
    var paging_pagesize = $( "#reg" ).val(); 
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1209',"parametros": '{"paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'}; 
    var band = 'list_principal';
    buscar(formData, band);   
  }
 function buscarPatrimonioTipoCampo(paging_pagenumbe){
     document.getElementById("listado_campo_valor").style = "display:block;";
    document.getElementById("listado_principal").style = "display:none;";
    var paging_pagesize = $( "#reg_campo" ).val(); 
    var campoPatrimonio = $( '#camposPorPatrimonio' ).val();
    var valorPatrimonio = $( '#valorPatrimonio' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1210',"parametros": '{"xcampo_id":' +campoPatrimonio+ ', "xcampo_valor":"' +valorPatrimonio+ '","xpaging_pagesize":'+paging_pagesize+',"xpaging_pagenumber":'+paging_pagenumbe+'}'};  
    var band = 'list_por_campo'; 
    buscar(formData, band,444444444);  
  }
    function limpiarPrincipal()
  {
    $( '#camposPorPatrimonio' ).val("-1");
    $( '#valorPatrimonio' ).val("");
  }

  /* function listarPatrimonioCultural(g_tipo){
    var tipo = 2;
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-933',"parametros": '{"xtipo_id":' +tipo+ '}'}; 
    buscar(formData);  
  }*/


function selectCampos()
{  
  var tipoCampoPatrimonio = 2;
  document.getElementById("camposPorPatrimonio").length=1;
  //////////console.log(tipoCampoPatrimonio,333333333333);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-931',"parametros": '{"ytipoid":'+tipoCampoPatrimonio+'}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        //////////console.log("listadoCampos",dataIN);
        for(var i = 0; i < dataIN.length; i++){
         var campo_id = dataIN[i].xcampo_id;
         var campo_tipo = JSON.parse(dataIN[i].xcampo_data).campoTipo;   
         document.getElementById("camposPorPatrimonio").innerHTML += '<option value=' +campo_id+ '>' +campo_tipo+ '</option>';            
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
}


function obtenercorrelativo(tipoPatrimonio)
{
 $.ajax({
  type        : 'GET',
  url         : urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": 'SERVICIO_PATRIMONIO-936',"parametros":'{"xtipoPat":"'+tipoPatrimonio+'"}'};
    //////////console.log("formdataaa",formData);
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
      var data=dataIN;
      correlativog=dataIN[0].sp_correlativo_patrimonio4;       
    //////////console.log("correl99999999999999999ativog",correlativog);

  },     
  error: function (xhr, status, error) { }
});
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

}

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SERVICIO_PATRIMONIO-1023","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
       ////////console.log(dataIN);
       var primerNombre = dataIN[0].vprs_nombres;
       var primerApellido = dataIN[0].vprs_paterno;
       var segundoApellido = dataIN[0].vprs_materno;
       var cedulaIdentidad = dataIN[0].vprs_ci;
       var razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
       
       $("#ccreador_p").val(primerNombre+' '+primerApellido+' '+segundoApellido);

     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};

function darBaja(id, g_tipo)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-934","parametros": '{"xcas_id":'+id+', "xcas_usr_id":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
           buscarPatrimonioPorTipo(1); 
           swal("Patrimonio!", "Fue eliminado correctamente!", "success");

         },
         error: function( result ) 
         {
          swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

  function buscar(formData){

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-patrimonio" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Nro de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Denominación</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Fecha</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Adjuntos</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Impresiones</th></tr>' ;
    htmlListaBus += '</thead>'; 
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          
          var datos = dataIN;
          var tam = datos.length;
        if(dataIN == "[{ }]"){
          tam = 0;
        }; 

        for (var i = 0; i < tam; i++){ 
         var cas_id = JSON.parse(dataIN[i].data).cas_id;
         var numero_ficha = JSON.parse(dataIN[i].data).cas_nro_caso;
         var dataP= JSON.parse(dataIN[i].data);
         var g_tipo='PTR-PCI'; 
         var g_tipoArq = dataP.PTR_AREAS; 
         var nroFicha=dataP.cas_nro_caso;
         var denominacion = JSON.parse(dataIN[i].data).ptr_den;
         var codigo_ficha = JSON.parse(dataIN[i].data).cas_nombre_caso;
         var codigo_catastral = '';
         var fecha_registro = JSON.parse(dataIN[i].data).cas_registrado;
         fecha_registro = fecha_registro.split('T');
         fecha_registro = fecha_registro[0];
 
         if ( numero_ficha == null) {
           numero_ficha = '';
         }
         if ( denominacion == null) {
           denominacion = '';                  
         }
         if ( codigo_ficha == null) {
           codigo_ficha = '';
         }
       
         if ( denominacion == 'PTR_DEN_ACT') {
           denominacion = '';                  
         }
         htmlListaBus += '<tr>';
         htmlListaBus += '<td align="left">'+(i+1)+'</td>';
         htmlListaBus += '<td align="center">';
         htmlListaBus+='<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + cas_id +',\''+ g_tipo +'\');">';
         htmlListaBus+='<i class="glyphicon glyphicon-trash"></i></button>';

         if(g_tipoArq =='1' || g_tipoArq =='2' || g_tipoArq =='3' || g_tipoArq =='4' || g_tipoArq =='5' || g_tipoArq =='7'){
          
            htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdatePatInmaterialP4" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick="editarRegistroPatrimonio('+cas_id+');"><i class="glyphicon glyphicon-pencil"></i></button>';
          }

          if(g_tipoArq =='8'){
           htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdatePatInmaterialP2" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick="editarRegistroPatrimonio('+cas_id+');"><i class="glyphicon glyphicon-pencil"></i></button>';
         }
      htmlListaBus += '</td>';
      htmlListaBus += '<td align="left" class="form-natural">'+numero_ficha+'</td>'; 
      htmlListaBus += '<td align="left" class="form-natural">'+codigo_ficha+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+denominacion+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+fecha_registro+'</td>';
      var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
      url = url.replace('casosId1', cas_id);
      url = url.replace('numeroficha1', numero_ficha);

      htmlListaBus+='<td style="text-align: center;"><a href="'+url+'" class="btncirculo btn-xs btn-default" fa fa-plus-square pull-right"  > <i class="fa fa-folder"></i></a>';
      htmlListaBus+='</td>';
      htmlListaBus+='<td align="center">';
      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Ficha" type="button" onClick= "generarProforma(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-print"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Archivo Fotografico" type="button" onClick= "imprimirArchivoFotografico(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-camera"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Todo" type="button" onClick= "imprimirFichaTodo(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-list"></i></button>';

      htmlListaBus+='</td>';

      htmlListaBus += '</tr>';
    } 
    htmlListaBus += '</table></div></div>';
    $( '#listPatrimonio' ).html(htmlListaBus);
    $( '#lts-patrimonio' ).DataTable(
    {
      "language": {"url": "lenguaje"},
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    });

  },
  error: function(result) {
    swal( "Error..!", "Error....", "error" );
  }
});
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});
};

 function editarRegistroPatrimonio(cas_id){

    var formData = {"identificador": "SERVICIO_PATRIMONIO-1016", "parametros":'{"id_caso":'+cas_id+'}'};

    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          var idCaso = JSON.parse(dataIN[0].data).cas_id;
          var nroFicha = JSON.parse(dataIN[0].data).cas_nro_caso;
          var dataP= JSON.parse(dataIN[0].data);
          var g_tipoArq=dataP.cas_datos.PTR_AREAS; 

              if(g_tipoArq =='1'){  
               var PTR_ETIM = dataP.cas_datos.PTR_ETIM;
               var PTR_CARACT = dataP.cas_datos.PTR_CARACT;
               var PTR_CONTEX = dataP.cas_datos.PTR_CONTEX;
               var PTR_NARR_DEN = dataP.cas_datos.PTR_NARR_DEN;
               var PTR_DAT_HIS = dataP.cas_datos.PTR_DAT_HIS;
               var PTR_SIG_INTER = dataP.cas_datos.PTR_SIG_INTER;
               var PTR_DAT_COMP = dataP.cas_datos.PTR_DAT_COMP;
               var PTR_OTROS = dataP.cas_datos.PTR_OTROS;
               //console.log("PTR_ETIM",PTR_ETIM);
               
               var PTR_ETIM = caracteresEspecialesActualizar(PTR_ETIM);
               var PTR_CARACT = caracteresEspecialesActualizar(PTR_CARACT);
               var PTR_CONTEX = caracteresEspecialesActualizar(PTR_CONTEX);
               var PTR_NARR_DEN = caracteresEspecialesActualizar(PTR_NARR_DEN);
               var PTR_DAT_HIS = caracteresEspecialesActualizar(PTR_DAT_HIS);
               var PTR_SIG_INTER = caracteresEspecialesActualizar(PTR_SIG_INTER);
               var PTR_DAT_COMP = caracteresEspecialesActualizar(PTR_DAT_COMP);
               var PTR_OTROS = caracteresEspecialesActualizar(PTR_OTROS);

               $( "#acasoId_i" ).val(idCaso);
               $( "#anroFicha_i" ).val(nroFicha);
               $( "#acreador_in" ).val(dataP.cas_datos.PTR_CREADOR);
               $( "#aexpreCultural_i" ).val(dataP.cas_datos.PTR_EXPR);
               $( "#adenominacion_i" ).val(dataP.cas_datos.PTR_DEN);
               $( "#aetimologia_i" ).val(PTR_ETIM);
               $( "#aano_i" ).val(dataP.cas_datos.PTR_PERIODO);
               //$( "#amacrodistrito_i" ).val(macroI);//
               $( "#azona_i" ).val(dataP.cas_datos.PTR_ZONA);
               $( "#adireccion_i" ).val(dataP.cas_datos.PTR_DIR);
               $( "#acaracterExpresion_i" ).val(PTR_CARACT);
               $( "#acontexto_socio_i" ).val(PTR_CONTEX);
               $( "#adescripDenomina_i" ).val(PTR_NARR_DEN);
               $( "#adatosHisto_i" ).val(PTR_DAT_HIS);
               $( "#aSignificado_i" ).val(PTR_SIG_INTER);
                //--------------------------------------------
                var nivelProteccion = dataP.cas_datos.PTR_NIV_PROTEC;

                var macrodistrito_i = dataP.cas_datos.PTR_MACRO;
                document.getElementById("anivel_proteccion1").checked = nivelProteccion[1].estado;
                document.getElementById("anivel_proteccion2").checked = nivelProteccion[2].estado;
                document.getElementById("anivel_proteccion3").checked = nivelProteccion[3].estado;
                document.getElementById("anivel_proteccion4").checked = nivelProteccion[4].estado;
                document.getElementById("anivel_proteccion5").checked = nivelProteccion[5].estado;

                document.getElementById("amacrodistrito_i1").checked = macrodistrito_i[1].estado;
                document.getElementById("amacrodistrito_i2").checked = macrodistrito_i[2].estado;
                document.getElementById("amacrodistrito_i3").checked = macrodistrito_i[3].estado;
                document.getElementById("amacrodistrito_i4").checked = macrodistrito_i[4].estado;
                document.getElementById("amacrodistrito_i5").checked = macrodistrito_i[5].estado;
                document.getElementById("amacrodistrito_i6").checked = macrodistrito_i[6].estado;
                document.getElementById("amacrodistrito_i7").checked = macrodistrito_i[7].estado;
                document.getElementById("amacrodistrito_i8").checked = macrodistrito_i[8].estado;
                document.getElementById("amacrodistrito_i9").checked = macrodistrito_i[9].estado;
             
                $( "#aorganoEmisor_i" ).val(dataP.cas_datos.PTR_PROT_LEGAL);
                $( "#ainstrumento_i" ).val(dataP.cas_datos.PTR_INSTR_PRLEGAL);
                $( "#anrolegal_i" ).val(dataP.cas_datos.PTR_NUM_LEGAL);
                $( "#acomplementarios_i" ).val(PTR_DAT_COMP);
                $( "#afuentes_i" ).val(PTR_OTROS);
                $( "#apalabra_i" ).val(dataP.cas_datos.PTR_PALABRA_CLAVE);
              }

              if(g_tipoArq =='8'){
               $( "#acasoId_p" ).val(idCaso);
               $( "#anroFicha_p" ).val(nroFicha);
               $( "#abien_cultural_p" ).val(dataP.cas_datos.PTR_DEN); 
               $( "#atipo_cultural_p" ).val(dataP.cas_datos.PTR_TB_CUL);
               $( "#acategoria_declaratoria_p" ).val(dataP.cas_datos.PTR_PROT_LEGAL);
               $( "#anorma_legal_p" ).val(dataP.cas_datos.PTR_NIV_PROTEC);
               $( "#anumero_legal_p" ).val(dataP.cas_datos.PTR_NUM_PRLEGAL);
               $( "#afecha_programacion_p" ).val(dataP.cas_datos.PTR_FEC_PROG);
               $( "#apalabra_clave_p" ).val(dataP.cas_datos.PTR_PALABRA_CLAVE);
             }
            
           },
        error: function(result) {
          swal( "Error..!", "Error....", "error" );
        }
      });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
      });
};


$( "#registrarPatriCultural").click(function() 
{
  obtenercorrelativo('PTR-PCI');
  setTimeout(function(){
    var ccreador_p = $("#ccreador_p").val();  
    console.log("ccreador_p",ccreador_p); 
    var ccodPatrimonio_p = 'PTR_PCI'+correlativog+'/2018';
    var cbien_cultural_p = $("#cbien_cultural_p").val();
    var ctipo_cultural_p = $("#ctipo_cultural_p").val();
    var ccategoria_declaratoria_p = document.getElementById('ccategoria_declaratoria_p').value;
    var cnorma_legal_p = document.getElementById('cnorma_legal_p').value;
    var cnumero_legal_p = $("#cnumero_legal_p").val();
    var cfecha_programacion_p = $("#cfecha_programacion_p").val();
    var cpalabra_clave_p = $("#cpalabra_clave_p").val();
    var g_tipo = 'PTR-PCI';

    var cas_data_p ='{"g_tipo":"'+g_tipo+'","PTR_CREADOR":"'+ccreador_p+'","PTR_AREAS":"8","PTR_DEN":"'+cbien_cultural_p+'","PTR_TB_CUL":"'+ctipo_cultural_p+'","PTR_PROT_LEGAL":"'+ccategoria_declaratoria_p+'","PTR_NIV_PROTEC":"'+cnorma_legal_p+'","PTR_NUM_PRLEGAL":"'+cnumero_legal_p+'","PTR_FEC_PROG":"'+cfecha_programacion_p+'","PTR_PALABRA_CLAVE":"'+cpalabra_clave_p+'"}';

    var jdata_p = JSON.stringify(cas_data_p);

  var xcas_nro_caso = correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = '';
  var xcas_tipo_hr = '';
  var xcas_id_padre = 1;
  var xcampo_f = ''; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_p+',"xcas_nombre_caso":"'+ccodPatrimonio_p+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

      //////////console.log("dataaaa",data);
      buscarPatrimonioPorTipo(1);
      swal( "Exíto..!", "Se registro correctamente el Patrimonio Inmaterial...!", "success" );
      //$( "#formInmaterial_create").data('bootstrapValidator').resetForm();
      //$( "#myCreatePatriInmaterial" ).modal('toggle');
      $( "#cbien_cultural_p" ).val(""); 
      $( "#ctipo_cultural_p" ).val(""); 
      $( "#ccategoria_declaratoria_p" ).val(""); 
      $( "#cnorma_legal_p" ).val(""); 
      $( "#cnumero_legal_p" ).val(""); 
      $( "#cfecha_programacion_p" ).val(""); 
      $( "#cpalabra_clave_p" ).val(""); 

      var idCasos = data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', xcas_nro_caso);
        location.href = url;
      }, 1000);
    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  

}, 500);
});

$( "#registrarAmbito4").click(function() 
{
  obtenercorrelativo('PTR-PCI');
  setTimeout(function(){
  var ccreador_p = $("#ccreador_p").val();     
  var ccodPatrimonio_i = 'PTR_PCI'+correlativog+'/2018';
  var cexpreCultural_i = $("#cexpreCultural_i").val();
  //////////console.log("cexpreCultural_i",cexpreCultural_i);
  var cdenominacion_i = $("#cdenominacion_i").val();
  //////////console.log("cdenominacion_i",cdenominacion_i);
  var cetimologia_i = $("#cetimologia_i").val();
  //console.log("cetimologia_i",cetimologia_i);
  var cano_i = $("#cano_i").val();
  var cdepartamento_i = $("#cdepartamento_i").val();
  var cmunicipio_i = $("#cmunicipio_i").val();
  var cprovincia_i = $("#cprovincia_i").val();
  //var cmacrodistrito_i = document.getElementById('cmacrodistrito_i').value;
  var cmacrodistrito_i1 = document.getElementById("cmacrodistrito_i1").checked;
  var cmacrodistrito_i2 = document.getElementById("cmacrodistrito_i2").checked;
  var cmacrodistrito_i3 = document.getElementById("cmacrodistrito_i3").checked;
  var cmacrodistrito_i4 = document.getElementById("cmacrodistrito_i4").checked;
  var cmacrodistrito_i5 = document.getElementById("cmacrodistrito_i5").checked;
  var cmacrodistrito_i6 = document.getElementById("cmacrodistrito_i6").checked;
  var cmacrodistrito_i7 = document.getElementById("cmacrodistrito_i7").checked;
  var cmacrodistrito_i8 = document.getElementById("cmacrodistrito_i8").checked;
  var cmacrodistrito_i9 = document.getElementById("cmacrodistrito_i9").checked;

  var czona_i = $("#czona_i").val();
  var cdireccion_i = $("#cdireccion_i").val();
  var ccaracterExpresion_i = $("#ccaracterExpresion_i").val();
  var ccontexto_socio_i = $("#ccontexto_socio_i").val();
  var cdescripDenomina_i = $("#cdescripDenomina_i").val();
  var cdatosHisto_i = $("#cdatosHisto_i").val();
  var cSignificado_i = $("#cSignificado_i").val();
  
  var cnivel_proteccion1 = document.getElementById("cnivel_proteccion1").checked;
  var cnivel_proteccion2 = document.getElementById("cnivel_proteccion2").checked;
  var cnivel_proteccion3 = document.getElementById("cnivel_proteccion3").checked;
  var cnivel_proteccion4 = document.getElementById("cnivel_proteccion4").checked;
  var cnivel_proteccion5 = document.getElementById("cnivel_proteccion5").checked;

  var corganoEmisor_i = document.getElementById('corganoEmisor_i').value;
  var cinstrumento_i = document.getElementById('cinstrumento_i').value;
  var cnrolegal_i = $("#cnrolegal_i").val();
  var ccomplementarios_i = $("#ccomplementarios_i").val();
  var cfuentes_i = $("#cfuentes_i").val();
  var cpalabra_i = $("#cpalabra_i").val();
  var g_tipo = 'PTR-PCI';
////////console.log("cexpreCultural_i",cexpreCultural_i);
  var cexpreCultural_i = caracteresEspecialesRegistrar(cexpreCultural_i);
  var cetimologia_i = caracteresEspecialesRegistrar(cetimologia_i);

  //console.log("cetimologia_icConvertido",cetimologia_i);
  var ccaracterExpresion_i = caracteresEspecialesRegistrar(ccaracterExpresion_i);
  var ccontexto_socio_i = caracteresEspecialesRegistrar(ccontexto_socio_i);
  var cdescripDenomina_i = caracteresEspecialesRegistrar(cdescripDenomina_i);
  var cdatosHisto_i = caracteresEspecialesRegistrar(cdatosHisto_i);
  var cSignificado_i = caracteresEspecialesRegistrar(cSignificado_i);
  var ccomplementarios_i = caracteresEspecialesRegistrar(ccomplementarios_i);
  var cfuentes_i = caracteresEspecialesRegistrar(cfuentes_i);

  var cnivelprotect_check = '[{"tipo":"CHKM"},{"resid": 277, "estado": '+cnivel_proteccion1+', "resvalor": "Ninguno"},{"resid": 285, "estado": '+cnivel_proteccion2+', "resvalor": "Patrimonio de la Humanidad"},{"resid": 280, "estado": '+cnivel_proteccion3+', "resvalor": "Patrimonio Departamental"},{"resid": 283, "estado": '+cnivel_proteccion4+', "resvalor": "Patrimonio Municipal"},{"resid": 282, "estado": '+cnivel_proteccion5+', "resvalor": "Patrimonio Nacional"}]';

  var cmacrodistrito_i_check = '[{"tipo":"CHKM"},{"resid": 304, "estado": '+cmacrodistrito_i1+', "resvalor": "MACRODISTRITO 1 COTAHUMA"},{"resid": 305, "estado": '+cmacrodistrito_i2+', "resvalor": "MACRODISTRITO 2 MAXIMILIANO PAREDES"},{"resid": 306, "estado": '+cmacrodistrito_i3+', "resvalor": "MACRODISTRITO 3 PERIFERICA"},{"resid": 307, "estado": '+cmacrodistrito_i4+', "resvalor": "MACRODISTRITO 4 SAN ANTONIO"},{"resid": 308, "estado": '+cmacrodistrito_i5+', "resvalor": "MACRODISTRITO 5 SUR"},{"resid": 309, "estado": '+cmacrodistrito_i6+', "resvalor": "MACRODISTRITO 6 MALLASA"},{"resid": 310, "estado": '+cmacrodistrito_i7+', "resvalor": "MACRODISTRITO 7 CENTRO"},{"resid": 311, "estado": '+cmacrodistrito_i8+', "resvalor": "MACRODISTRITO 8 HAMPATURI"},{"resid": 312, "estado": '+cmacrodistrito_i9+', "resvalor": "MACRODISTRITO 9 ZONGO"}]';

    ////console.log("nivelprotect_check",cnivelprotect_check);

    var cas_data_i ='{"g_tipo":"'+g_tipo+'","PTR_AREAS":"1","PTR_CREADOR":"'+ccreador_p+'","PTR_EXPR":"'+cexpreCultural_i+'","PTR_DEN":"'+cdenominacion_i+'","PTR_ETIM":"'+cetimologia_i+'","PTR_PERIODO":"'+cano_i+'","PTR_DEP":"'+cdepartamento_i+'","PTR_MUNI":"'+cmunicipio_i+'","PTR_PROV":"'+cprovincia_i+'","PTR_MACRO":'+cmacrodistrito_i_check+',"PTR_ZONA":"'+czona_i+'","PTR_DIR":"'+cdireccion_i+'","PTR_CARACT":"'+ccaracterExpresion_i+'","PTR_CONTEX":"'+ccontexto_socio_i+'","PTR_NARR_DEN":"'+cdescripDenomina_i+'","PTR_DAT_HIS":"'+cdatosHisto_i+'","PTR_SIG_INTER":"'+cSignificado_i+'","PTR_NIV_PROTEC":'+cnivelprotect_check+',"PTR_PROT_LEGAL":"'+corganoEmisor_i+'","PTR_INSTR_PRLEGAL":"'+cinstrumento_i+'","PTR_NUM_LEGAL":"'+cnrolegal_i+'","PTR_DAT_COMP":"'+ccomplementarios_i+'","PTR_OTROS":"'+cfuentes_i+'","PTR_PALABRA_CLAVE":"'+cpalabra_i+'"}';

    //console.log("cas_data_i",cas_data_i);

    var jdata_i = JSON.stringify(cas_data_i);
  ////console.log("jdataaaa",jdata_i);
    var xcas_nro_caso = correlativog;
    var xcas_act_id = 137;
    var xcas_usr_actual_id = 1155;
    var xcas_estado_paso = 'Recibido';
    var xcas_nodo_id = 1;
    var xcas_usr_id = 1;
    var xcas_ws_id = 1;
    var xcas_asunto = 'asunto';
    var xcas_tipo_hr = 'hora';
    var xcas_id_padre = 1;
    var xcampo_f = 'campo'; 

    var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_i+',"xcas_nombre_caso":"'+ccodPatrimonio_i+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
    
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

      //console.log("dataaaaInmaterial",data);
      buscarPatrimonioPorTipo(1);
      swal( "Exíto..!","Se registro correctamente el Patrimonio Inmaterial...!", "success" );
      //$( "#formInmaterial_create").data('bootstrapValidator').resetForm();
      //$( "#myCreatePatriInmaterial" ).modal('toggle');
      $( "#cexpreCultural_i" ).val(""); 
      $( "#cdenominacion_i" ).val(""); 
      $( "#cetimologia_i" ).val(""); 
      $( "#cano_i" ).val(""); 
      $( "#cdepartamento_i" ).val("");
      $( "#cmunicipio_i" ).val("");
      $( "#cprovincia_i" ).val("");
      //$( "#cmacrodistrito_i" ).val("");
      $( "#czona_i" ).val("");
      $( "#cdireccion_i").val("");
      $( "#ccaracterExpresion_i" ).val("");
      $( "#ccontexto_socio_i" ).val("");
      $( "#cdescripDenomina_i" ).val("");
      $( "#cdatosHisto_i" ).val("");
      $( "#cSignificado_i" ).val("");
      //-----------------------------
      $( "#cnivel_proteccion1" ).val("");
      $( "#cnivel_proteccion2" ).val("");
      $( "#cnivel_proteccion3" ).val("");
      $( "#cnivel_proteccion4" ).val("");
      $( "#cnivel_proteccion5" ).val("");
      //-----------------------------
      $( "#corganoEmisor_i" ).val("");
      $( "#cinstrumento_i" ).val("");
      $( "#cnrolegal_i" ).val("");
      $( "#ccomplementarios_i" ).val("");
      $( "#cfuentes_i" ).val("");
      $( "#cpalabra_i" ).val("");

      var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', xcas_nro_caso);
        //console.log("url",url);
        location.href = url;
      }, 1000);

    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  

}, 500);
});

function limpiarA2(){
  $( "#cbien_cultural_p" ).val(""); 
  $( "#ctipo_cultural_p" ).val(""); 
  $( "#ccategoria_declaratoria_p" ).val(""); 
  $( "#cnorma_legal_p" ).val(""); 
  $( "#cnumero_legal_p" ).val(""); 
  $( "#cfecha_programacion_p" ).val(""); 
  $( "#cpalabra_clave_p" ).val(""); 

}
function limpiarA4(){
  $( "#cexpreCultural_i" ).val(""); 
  $( "#cdenominacion_i" ).val(""); 
  $( "#cetimologia_i" ).val(""); 
  $( "#cano_i" ).val(""); 
  $( "#cdepartamento_i" ).val("");
  $( "#cmunicipio_i" ).val("");
  $( "#cprovincia_i" ).val("");
 // $( "#cmacrodistrito_i" ).val("");
  $( "#czona_i" ).val("");
  $( "#cdireccion_i").val("");
  $( "#ccaracterExpresion_i" ).val("");
  $( "#ccontexto_socio_i" ).val("");
  $( "#cdescripDenomina_i" ).val("");
  $( "#cdatosHisto_i" ).val("");
  $( "#cSignificado_i" ).val("");
  //-----------------------------
  $( "#cnivel_proteccion1" ).val("");
  $( "#cnivel_proteccion2" ).val("");
  $( "#cnivel_proteccion3" ).val("");
  $( "#cnivel_proteccion4" ).val("");
  $( "#cnivel_proteccion5" ).val("");
  //-----------------------------
  $( "#corganoEmisor_i" ).val("");
  $( "#cinstrumento_i" ).val("");
  $( "#cnrolegal_i" ).val("");
  $( "#ccomplementarios_i" ).val("");
  $( "#cfuentes_i" ).val("");
  $( "#cpalabra_i" ).val("");
}



$( "#actualizarInmaterialA4" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

      var acasoId_i = $( "#acasoId_i" ).val();
      var anroFicha_i = $( "#anroFicha_i" ).val(); 
      var acreador_in = $( "#acreador_in" ).val(); 

      var aTipo_i = $( "#aTipo_i" ).val(); 
      var aTipoInmaterial = $( "#aTipoInmaterial" ).val(); 
      var acodPatrimonio_i = 'PTR_PCI'+(anroFicha_i)+'/2018';
      var aexpreCultural_i = $("#aexpreCultural_i").val();
      var adenominacion_i = $("#adenominacion_i").val();
      var aetimologia_i = $("#aetimologia_i").val();
      var aano_i = $("#aano_i").val();
      var adepartamento_i = $("#adepartamento_i").val();
      var amunicipio_i = $("#amunicipio_i").val();
      var aprovincia_i = $("#aprovincia_i").val();
      //var amacrodistrito_i = document.getElementById('amacrodistrito_i').value;
      var amacrodistrito_i1 = document.getElementById("amacrodistrito_i1").checked;
      var amacrodistrito_i2 = document.getElementById("amacrodistrito_i2").checked;
      var amacrodistrito_i3 = document.getElementById("amacrodistrito_i3").checked;
      var amacrodistrito_i4 = document.getElementById("amacrodistrito_i4").checked;
      var amacrodistrito_i5 = document.getElementById("amacrodistrito_i5").checked;
      var amacrodistrito_i6 = document.getElementById("amacrodistrito_i6").checked;
      var amacrodistrito_i7 = document.getElementById("amacrodistrito_i7").checked;
      var amacrodistrito_i8 = document.getElementById("amacrodistrito_i8").checked;
      var amacrodistrito_i9 = document.getElementById("amacrodistrito_i9").checked;

      var azona_i = $("#azona_i").val();
      var adireccion_i = $("#adireccion_i").val();
      var acaracterExpresion_i = $("#acaracterExpresion_i").val();
      var acontexto_socio_i = $("#acontexto_socio_i").val();
      var adescripDenomina_i = $("#adescripDenomina_i").val();
      var adatosHisto_i = $("#adatosHisto_i").val();
      var aSignificado_i = $("#aSignificado_i").val();

      var anivel_proteccion_legal1 = document.getElementById("anivel_proteccion1").checked;
      var anivel_proteccion_legal2 = document.getElementById("anivel_proteccion2").checked;
      var anivel_proteccion_legal3 = document.getElementById("anivel_proteccion3").checked;
      var anivel_proteccion_legal4 = document.getElementById("anivel_proteccion4").checked;
      var anivel_proteccion_legal5 = document.getElementById("anivel_proteccion5").checked;

      var aorganoEmisor_i = document.getElementById('aorganoEmisor_i').value;
      var ainstrumento_i = document.getElementById('ainstrumento_i').value;
      var anrolegal_i = $("#anrolegal_i").val();
      var acomplementarios_i = $("#acomplementarios_i").val();
      var afuentes_i = $("#afuentes_i").val();
      //console.log("afuentes_i",afuentes_i);
      var apalabra_i = $("#apalabra_i").val();
      var g_tipo = 'PTR-PCI';
      var aexpreCultural_i = caracteresEspecialesRegistrar(aexpreCultural_i);
      var aetimologia_i = caracteresEspecialesRegistrar(aetimologia_i);
      var acaracterExpresion_i = caracteresEspecialesRegistrar(acaracterExpresion_i);
      var acontexto_socio_i = caracteresEspecialesRegistrar(acontexto_socio_i);
      var adescripDenomina_i = caracteresEspecialesRegistrar(adescripDenomina_i);
      //console.log("adatosHisto_iAntes",adatosHisto_i);
      var adatosHisto_i = caracteresEspecialesRegistrar(adatosHisto_i);
      //console.log("adatosHisto_i",adatosHisto_i);

      var aSignificado_i = caracteresEspecialesRegistrar(aSignificado_i);
      var acomplementarios_i = caracteresEspecialesRegistrar(acomplementarios_i);
      var afuentes_i = caracteresEspecialesRegistrar(afuentes_i);

      var anivelprotect_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+anivel_proteccion_legal1+', "resvalor": "Ninguno"},{"resid": 285, "estado": '+anivel_proteccion_legal2+', "resvalor": "Patrimonio de la Humanidad"},{"resid": 280, "estado": '+anivel_proteccion_legal3+', "resvalor": "Patrimonio Departamental"},{"resid": 283, "estado": '+anivel_proteccion_legal4+', "resvalor": "Patrimonio Municipal"},{"resid": 282, "estado": '+anivel_proteccion_legal5+', "resvalor": "Patrimonio Nacional"}]';

      var amacrodistrito_i_check = '[{"tipo":"CHKM"},{"resid": 304, "estado": '+amacrodistrito_i1+', "resvalor": "MACRODISTRITO 1 COTAHUMA"},{"resid": 305, "estado": '+amacrodistrito_i2+', "resvalor": "MACRODISTRITO 2 MAXIMILIANO PAREDES"},{"resid": 306, "estado": '+amacrodistrito_i3+', "resvalor": "MACRODISTRITO 3 PERIFERICA"},{"resid": 307, "estado": '+amacrodistrito_i4+', "resvalor": "MACRODISTRITO 4 SAN ANTONIO"},{"resid": 308, "estado": '+amacrodistrito_i5+', "resvalor": "MACRODISTRITO 5 SUR"},{"resid": 309, "estado": '+amacrodistrito_i6+', "resvalor": "MACRODISTRITO 6 MALLASA"},{"resid": 310, "estado": '+amacrodistrito_i7+', "resvalor": "MACRODISTRITO 7 CENTRO"},{"resid": 311, "estado": '+amacrodistrito_i8+', "resvalor": "MACRODISTRITO 8 HAMPATURI"},{"resid": 312, "estado": '+amacrodistrito_i9+', "resvalor": "MACRODISTRITO 9 ZONGO"}]';
 ////console.log("nivelprotect_check",anivelprotect_check);

      var cas_data_i ='{"g_tipo":"'+g_tipo+'","PTR_AREAS":"1","PTR_CREADOR":"'+acreador_in+'","PTR_EXPR":"'+aexpreCultural_i+'","PTR_DEN":"'+adenominacion_i+'","PTR_ETIM":"'+aetimologia_i+'","PTR_PERIODO":"'+aano_i+'","PTR_DEP":"'+adepartamento_i+'","PTR_MUNI":"'+amunicipio_i+'","PTR_PROV":"'+aprovincia_i+'","PTR_MACRO":'+amacrodistrito_i_check+',"PTR_ZONA":"'+azona_i+'","PTR_DIR":"'+adireccion_i+'","PTR_CARACT":"'+acaracterExpresion_i+'","PTR_CONTEX":"'+acontexto_socio_i+'","PTR_NARR_DEN":"'+adescripDenomina_i+'","PTR_DAT_HIS":"'+adatosHisto_i+'","PTR_SIG_INTER":"'+aSignificado_i+'","PTR_NIV_PROTEC":'+anivelprotect_check+',"PTR_PROT_LEGAL":"'+aorganoEmisor_i+'","PTR_INSTR_PRLEGAL":"'+ainstrumento_i+'","PTR_NUM_LEGAL":"'+anrolegal_i+'","PTR_DAT_COMP":"'+acomplementarios_i+'","PTR_OTROS":"'+afuentes_i+'","PTR_PALABRA_CLAVE":"'+apalabra_i+'"}';
      
       var jdata_i = JSON.stringify(cas_data_i);
      
       var xcas_act_id = 137;
       var xcas_usr_actual_id = 1155;
       var xcas_estado_paso = 'Recibido';
       var xcas_nodo_id = 1;
       var xcas_usr_id = 1;
       var xcas_ws_id = 1;
       var xcas_asunto = '';
       var xcas_tipo_hr = '';
       var xcas_id_padre = 1;
       var xcampo_f =''; 

       var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_i+',"xcas_nro_caso":'+anroFicha_i+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_i+',"xcas_nombre_caso":"'+acodPatrimonio_i+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
       console.log("formData",formData);

       $.ajax({
         type        : 'POST',            
         url         : urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: { 
           'authorization': 'Bearer' + token,
         },
         success: function(data){
          //console.log("dataaaaaaInmaterialActualizar",data);
          buscarPatrimonioPorTipo(1);
          swal( "Exíto..!", "Se actualizo correctamente el registro del Patrimonio Inmaterial...!", "success" );

        },
        error: function(result) {
          swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
        }
      });         
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
      });  
      });

$( "#actualizarInmaterialA2" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      var acasoId_p = $( "#acasoId_p" ).val();
      var anroFicha_p = $( "#anroFicha_p" ).val(); 
      var acodPatrimonio_p = 'PTR_PCI'+(anroFicha_p)+'/2018';
      var abien_cultural_p = $("#abien_cultural_p").val();
      var atipo_cultural_p = $("#atipo_cultural_p").val();
      var acategoria_declaratoria_p = document.getElementById('acategoria_declaratoria_p').value;
      var anorma_legal_p = document.getElementById('anorma_legal_p').value;
      var anumero_legal_p = $("#anumero_legal_p").val();
      var afecha_programacion_p = $("#afecha_programacion_p").val();
      var apalabra_clave_p = $("#apalabra_clave_p").val();
      var g_tipo = 'PTR-PCI';

      var cas_data_p ='{"g_tipo":"'+g_tipo+'","PTR_AREAS":"8","PTR_DEN":"'+abien_cultural_p+'","PTR_TB_CUL":"'+atipo_cultural_p+'","PTR_PROT_LEGAL":"'+acategoria_declaratoria_p+'","PTR_NIV_PROTEC":"'+anorma_legal_p+'","PTR_NUM_PRLEGAL":"'+anumero_legal_p+'","PTR_FEC_PROG":"'+afecha_programacion_p+'","PTR_PALABRA_CLAVE":"'+apalabra_clave_p+'"}';

      var jdata_p = JSON.stringify(cas_data_p);
  //////////console.log("jdataaaa",jdata_p);

  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = '';
  var xcas_tipo_hr = '';
  var xcas_id_padre = 1;
  var xcampo_f =''; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_p+',"xcas_nro_caso":'+anroFicha_p+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_p+',"xcas_nombre_caso":"'+acodPatrimonio_p+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

      //////////console.log("formData",formData);
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: { 
         'authorization': 'Bearer' + token,
       },
       success: function(data){
        //////////console.log("dataaaaaa",data);
        buscarPatrimonioPorTipo(1);
        swal( "Exíto..!", "Se actualizo correctamente el registro del Patrimonio Inmaterial...!", "success" );

      },
      error: function(result) {
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });         
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
});

function generarProforma(id_cas, g_tipo){
    setTimeout(function(){
    if ( g_tipo == 'PTR-PCI' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerar',['datos'=>'d1']) }}"; 
     }
     urlPdf = urlPdf.replace('d1', id_cas);
     //console.log("urlPdf",urlPdf);
     window.open(urlPdf);
   }, 500);
  }
function imprimirArchivoFotografico(id_cas, g_tipo){
    setTimeout(function(){
 
     if ( g_tipo == 'PTR-PCI' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerarArchFoto',['datos'=>'d1']) }}"; 
     }    
     urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

function imprimirFichaTodo(id_cas, g_tipo){
    setTimeout(function(){
     if ( g_tipo == 'PTR-PCI' ) {
       var urlFicha = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     }
     urlFicha = urlFicha.replace('d1', id_cas);
    ////console.log("urlFicha",urlFicha);
    window.open(urlFicha); 
  }, 500);
  }


/*-----------------END PATRIMONIO INMATERIAL-----------------------*/

$(document).ready(function (){

 buscarPatrimonioPorTipo(1);
 selectCampos();
 cargarDatosUsuario();
});
</script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script >

  $('textarea').ckeditor();


</script>

@endpush