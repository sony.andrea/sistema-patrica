<div class="modal fade modal-default" id="myUpdatePatInmaterialP4" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     {!! Form::open(['class'=>'form-vertical','id'=>'formInmaterialP4_update'])!!}
     <div class="modal-header">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="row">
           <div class="col-md-12">
            <section class="content-header">
             <div class="header_title">
              <h3>
                Editar datos de Patrimonio Inmaterial
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </h3>
            </div>
          </section>
        </div>   
      </div>
    </div>
    <div  style="padding:20px">
      <input type="hidden" name="csrf-token" value=" {{ csrf_token(
      ) }}" id="token">   
      <input type="hidden" name="acasoId_i" id="acasoId_i">
      <input type="hidden" name="anroFicha_i" id="anroFicha_i">
      <input type="hidden" name="acreador_in" id="acreador_in">
      
      <div class="col-md-12">
        <div class="panel-heading">
          <label class="control-label" style="color:#275c26";>
            DATOS GENERALES:
          </label>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group col-md-12">
          <label class="control-label">
            Expresión Cultural:
          </label>
          <input id="aexpreCultural_i" type="text" class="form-control"  placeholder="Expresión Cultural"> 
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group col-md-6">
          <label class="control-label">
            Denominación:
          </label>
          <input id="adenominacion_i" type="text" class="form-control"  placeholder="Denominación"> 
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group col-md-12">
          <label class="control-label">
            Etimologia:
          </label>
          <textarea name="textarea" rows="4" cols="50" class="form-control " id="aetimologia_i"></textarea> 
        </div>
      </div>
       <div class="col-md-12">
        <div class="form-group col-md-6">
          <label class="control-label">
            Epoca o año de origen:
          </label>
          <!--<input id="aano_i" type="text" class="form-control"  placeholder="Epoca o año de origen">-->
          <select class="form-control" id="aano_i" >
            <option value="-1"> Selecionar...</option> 
            <option value="Prehispánico">Prehispánico</option>
            <option value="Colonial">Colonial</option>
            <option value="Repúblicano">Repúblicano</option> 
            <option value="Moderna">Moderna</option>                          
          </select>  
        </div>
        <div class="form-group col-md-6">
          <label class="control-label">
            Departamento:
          </label>
          <input id="adepartamento_i" type="text" value="LA PAZ" class="form-control"  placeholder="Ubicación" disabled> 
        </div>
       </div>
      <div class="col-md-12">
        <div class="form-group col-md-6">
          <label class="control-label">
            Municipio:
          </label>
          <input id="amunicipio_i" type="text" value="LA PAZ" class="form-control"  placeholder="Ubicación" disabled> 
        </div>
       <div class="form-group col-md-6">
        <label class="control-label">
          Provincia:
        </label>
        <input id="aprovincia_i" type="text" value="MURRILLO" class="form-control"  placeholder="Ubicación" disabled> 
      </div>
      </div>
      <div class="col-md-12">
      <div class="form-group col-md-6">
        <label class="control-label">
          Macrodistrito:
        </label>
      
          <br>                                    
            <input type="checkbox" id="amacrodistrito_i1">MACRODISTRITO 1 COTAHUMA<br> 
            <input type="checkbox" id="amacrodistrito_i2">MACRODISTRITO 2 MAXIMILIANO PAREDES<br>
            <input type="checkbox" id="amacrodistrito_i3">MACRODISTRITO 3 PERIFERICA<br>
            <input type="checkbox" id="amacrodistrito_i4">MACRODISTRITO 4 SAN ANTONIO<br>  
            <input type="checkbox" id="amacrodistrito_i5">MACRODISTRITO 5 SUR<br>
            <input type="checkbox" id="amacrodistrito_i6">MACRODISTRITO 6 MALLASA<br>
            <input type="checkbox" id="amacrodistrito_i7">MACRODISTRITO 7 CENTRO<br>
            <input type="checkbox" id="amacrodistrito_i8">MACRODISTRITO 8 HAMPATURI<br>
            <input type="checkbox" id="amacrodistrito_i9">MACRODISTRITO 9 ZONGO<br>  

      </div>
     <div class="form-group col-md-6">
      <label class="control-label">
        Zona:
      </label>
      <input id="azona_i" type="text" class="form-control"  placeholder="Zona">
    </div>
    </div>
    <div class="col-md-12">
    <div class="form-group col-md-6">
      <label class="control-label">
        Dirección:
      </label>
      <input id="adireccion_i" type="text" class="form-control"  placeholder="Dirección">
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        Caracterización de la Expresión Cultural::
      </label>
      <textarea name="textarea" rows="4" cols="50" class="form-control " id="acaracterExpresion_i"></textarea> 
    </div>
  </div>
   <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        Contexto Socio - Cultural:
      </label>
      <textarea name="textarea" rows="4" cols="50" class="form-control " id="acontexto_socio_i"></textarea> 
    </div>
  </div>
  <div class="col-md-12">
   <div class="form-group col-md-12">
    <label class="control-label">
      Descripción de la Denominación:
    </label>
    <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripDenomina_i"></textarea> 
  </div>
</div>
  <div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Datos Históricos:
    </label>
    <textarea name="textarea" rows="4" cols="50" class="form-control " id="adatosHisto_i"></textarea> 
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
    Significado e Interpretación:
  </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="aSignificado_i"></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
    <label class="control-label">
       Nivel Protección Legal:
    </label>
    <br>
    <input type="checkbox" id="anivel_proteccion1"> Ninguno<br>
    <input type="checkbox" id="anivel_proteccion4"> Patrimonio Municipal<br>
    <input type="checkbox" id="anivel_proteccion3"> Patrimonio Departamental<br>
    <input type="checkbox" id="anivel_proteccion5"> Patrimonio Nacional<br>
    <input type="checkbox" id="anivel_proteccion2"> Patrimonio de la Humanidad<br>
 </div>
  <div class="form-group col-md-6">
    <label class="control-label">
      Órgano Emisor de Protección Legal:
    </label>
    <select class="form-control" id="aorganoEmisor_i" >
      <option value=""> Selecionar</option> 
      <option value="Estado Plurinacional de Bolivia"> Estado Plurinacional de Bolivia </option>
      <option value="Gobernación"> Gobernación</option>
      <option value="Municipio"> Municipio</option> 
      <option value="Ninguno"> Ninguno</option>
      <option value="Unesco"> Unesco</option> 
    </select> 
  </div>
  </div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      Instrumento de Protección Legal:
    </label>
    <select class="form-control" id="ainstrumento_i" >
      <option value=""> Selecionar</option> 
      <option value="Decreto Departamental">Decreto Departamental</option>
      <option value="Ley Decreto Supremo"> Ley Decreto Supremo</option>
      <option value="ley u Ordenanza Municipal">ley u Ordenanza Municipal</option>  
      <option value="Ninguno">Ninguno</option> 
      <option value="Resolución del Estado Plurinacional">Resolución del Estado Plurinacional</option> 
      <option value="Titulo de Patrimonio Inmaterial de la Humanidad">Titulo de Patrimonio Inmaterial de la Humanidad</option>
    </select> 
  </div>
  <div class="form-group col-md-6">
    <label class="control-label">
     Nro Instrumento Legal:
   </label>
   <input id="anrolegal_i" type="text" class="form-control"  placeholder="Nro Instrumento Legal">
 </div>
 </div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
    Datos Complementarios:
  </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="acomplementarios_i"></textarea> 
</div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Fuentes:
    </label>
    <textarea name="textarea" rows="4" cols="50" class="form-control " id="afuentes_i"></textarea> 
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Palabras Clave:
    </label>
    <input id="apalabra_i" type="text" class="form-control"  placeholder="Palabras Clave">
  </div>
</div>
<div class="modal-footer">
 <button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
 <button class="btn btn-primary" id="actualizarInmaterialA4" data-dismiss="modal" type="button">
  <i class="glyphicon glyphicon-pencil"></i> Modificar
</button>
</div> 
{!! Form::close() !!} 

</div>
</div>
</div>
</div>
</div>
</div>
