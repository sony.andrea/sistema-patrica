<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myCreatePatriInmaterial" tabindex="-5">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			{!! Form::open(['class'=>'form-vertical inmaterial-validate','id'=>'formInmaterial_create'])!!}
			<!--Inicia content-->
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 container-fluit">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3>
									Nueva Ficha Patrimonio Inmaterial
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</h3>
							</div>
							<div class="panel-body">
								<div class="caption">
									<input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
									<input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">
									<div class="col-md-12">
										<div class="col-md-6 form-group">
											<label class="control-label">Tipo de Ambito:</label>
											<!--<label class="control-label col-md-4">Tipo de contribuyente:</label>-->
											<select  id="tipo-patrimonio" class="form-control" onclick="tipoPatrimonioInmaterial(this)">
												<option selected value=" ">Seleccione...</option>
												<option value="1">PATRIMONIO CULTURAL INMATERIAL  DEL MUNICIPIO DE LA PAZ</option>
												<option value="2">ÁMBITO I TRADICIONES Y EXPRESIONES ORALES</option>
												<option value="3">ÁMBITO II ARTES DEL ESPECTÁCULO</option>
												<option value="4">ÁMBITO III USOS SOCIALES, RITUALES Y ACTOS FESTIVOS</option>
												<option value="5">ÁMBITO IV CONOCIMIENTOS Y USOS RELACIONADOS CON LA NATURALEZA Y EL UNIVERSO</option>
												<option value="6">ÁMBITO V TÉCNICAS ARTESANALES TRADICIONALES</option>
												<option value="7">PERSONAJES RELACIONADOS A LAS TRADICIONES ORALES</option>
											</select>
										</div>
										<div class="col-md-6 form-group">
												<label class="control-label">
													Creador:
												</label>
										<input id="ccreador_p" type="text" class="form-control" disabled>
										</div>

									</div>
									<!--inicio div mueble-->
									<div id="panelPatrimonioCultural" style="display: none;">
										<div class="col-md-12">
											<div class="panel-heading">
												<label class="control-label" style="color:#275c26";>
													CONTEXTO ORIGINAL:
												</label>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Bien cultural:
												</label>
												<input id="cbien_cultural_p" type="text" class="form-control"  placeholder="Bien cultural">
											</div>

											<div class="form-group col-md-6">
												<label class="control-label">
													Tipo bien cultural:
												</label>
												<input id="ctipo_cultural_p" type="text" class="form-control"  placeholder="Tipo bien cultural">
											</div> 

										</div> 
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Categoria declaratoria:
												</label>

												<select class="form-control" id="ccategoria_declaratoria_p" >
													<option value=""> Selecionar</option> 
													<option value="1"> Estado Plurinacional de Bolivia </option>
													<option value="2"> Gobernacion</option>
													<option value="3"> Municipio</option>
													<option value="3"> Unesco</option> 
													<option value="3"> Ninguno</option>  
												</select> 
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">
													Norma legal:
												</label>

												<select class="form-control" id="cnorma_legal_p" >
													<option value=""> Selecionar</option> 
													<option value="1"> Patrimonio Nacional </option>
													<option value="2"> Patrimonio Municipal</option>
													<option value="3"> Patrimonio Departamental</option>
													<option value="3"> Patrimonio de la humanidad</option> 
													<option value="3"> Ninguno</option>  
												</select> 
											</div> 
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Numero Legal:
												</label>
												<input id="cnumero_legal_p"  type="text" class="form-control"  placeholder="Numero legal">
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">
													Fecha de programacion:
												</label>
												<input id="cfecha_programacion_p" name="cfecha_programacion" type="date" class="form-control"  placeholder="fecha de programacion">
											</div> 
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Palabras clave:
												</label>
												<input id="cpalabra_clave_p" type="text" class="form-control"  placeholder="Palabra_clave"> 
											</div>
										</div>

										<div class="modal-footer">
										<button class="btn btn-primary" data-dismiss="modal" type="button">
            								<i class="fa fa-close"></i> Cancelar</button>
											<button class="btn btn-default "  type="button" onClick="limpiarA2()">
												<i class="fa fa-eraser"></i> Limpiar
											</button>
											<a type = "button" class = "btn btn-primary" id="registrarPatriCultural" data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
										</div>  
									</div>
									<!--fin div mueble-->
									<!--inicio div yacimiento-->
									<div id="panelAmbito" style="display: none;">
										<div class="col-md-12">
											<div class="panel-heading">
												<label class="control-label" style="color:#275c26";>
													DATOS GENERALES:
												</label>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Expresión Cultural:
												</label>
												<input id="cexpreCultural_i" type="text" class="form-control"  placeholder="Expresión Cultural">  
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Denominación:
												</label>
												<input id="cdenominacion_i" type="text" class="form-control"  placeholder="Denominación"> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Etimologia:
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="cetimologia_i"></textarea> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Epoca o año de origen:
												</label>
												<!--<input id="cano_i" type="text" class="form-control"  placeholder="Epoca o año de origen">-->
												<select class="form-control" id="cano_i" >
													<option value="-1"> Selecionar</option> 
													<option value="Prehispánico">Prehispánico</option>
													<option value="Colonial">Colonial</option>
													<option value="Repúblicano">Repúblicano</option> 
													<option value="Moderna">Moderna</option>													
												</select> 

											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Departamento:
												</label>
												<input id="cdepartamento_i" type="text" value="LA PAZ" class="form-control"  placeholder="Ubicación" disabled> 
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">
													Municipio:
												</label>
												<input id="cmunicipio_i" type="text" value="LA PAZ" class="form-control"  placeholder="Ubicación" disabled> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Provincia:
												</label>
												<input id="cprovincia_i" type="text" value="MURRILLO" class="form-control"  placeholder="Ubicación" disabled> 
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">
													Macrodistrito:
												</label><br>                                                  
						                          <input type="checkbox" id="cmacrodistrito_i1">MACRODISTRITO 1 COTAHUMA<br> 
						                          <input type="checkbox" id="cmacrodistrito_i2">MACRODISTRITO 2 MAXIMILIANO PAREDES<br>
						                          <input type="checkbox" id="cmacrodistrito_i3">MACRODISTRITO 3 PERIFERICA<br>
						                          <input type="checkbox" id="cmacrodistrito_i4">MACRODISTRITO 4 SAN ANTONIO<br>  
						                          <input type="checkbox" id="cmacrodistrito_i5">MACRODISTRITO 5 SUR<br>
						                          <input type="checkbox" id="cmacrodistrito_i6">MACRODISTRITO 6 MALLASA<br>
						                          <input type="checkbox" id="cmacrodistrito_i7">MACRODISTRITO 7 CENTRO<br>
						                          <input type="checkbox" id="cmacrodistrito_i8">MACRODISTRITO 8 HAMPATURI<br>
						                          <input type="checkbox" id="cmacrodistrito_i9">MACRODISTRITO 9 ZONGO<br>  
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Zona:
												</label>
												<input id="czona_i" type="text" class="form-control"  placeholder="Zona">
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">
													Dirección:
												</label>
												<input id="cdireccion_i" type="text" class="form-control"  placeholder="Dirección">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Caracterización de la Expresión Cultural::
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="ccaracterExpresion_i"></textarea> 
											</div>
										</div>
											<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Contexto Socio - Cultural:
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="ccontexto_socio_i"></textarea> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Descripción de la Denominación:
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripDenomina_i"></textarea> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Datos Históricos:
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="cdatosHisto_i"></textarea> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Significado e Interpretación:
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="cSignificado_i"></textarea> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
												   Nivel Protección Legal:
												</label>
												<br>
												<input type="checkbox" id="cnivel_proteccion1"> Ninguno<br>
												<input type="checkbox" id="cnivel_proteccion4"> Patrimonio Municipal<br>
												<input type="checkbox" id="cnivel_proteccion3"> Patrimonio Departamental<br>
												<input type="checkbox" id="cnivel_proteccion5"> Patrimonio Nacional<br>
												<input type="checkbox" id="cnivel_proteccion2"> Patrimonio de la Humanidad<br>
												</div>
											<div class="form-group col-md-6">
												<label class="control-label">
													Órgano Emisor de Protección Legal:
												</label>

												<select class="form-control" id="corganoEmisor_i" >
													<option value=""> Selecionar</option> 
													<option value="Estado Plurinacional de Bolivia"> Estado Plurinacional de Bolivia </option>
													<option value="Gobernación"> Gobernación</option>
													<option value="Municipio"> Municipio</option> 
													<option value="Ninguno"> Ninguno</option>
													<option value="Unesco"> Unesco</option> 
												</select> 
											</div>
											</div>
											<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Instrumento de Protección Legal:
												</label>
												<select class="form-control" id="cinstrumento_i" >
													<option value=""> Selecionar</option> 
													<option value="Decreto Departamental">Decreto Departamental</option>
													<option value="Ley Decreto Supremo"> Ley Decreto Supremo</option>
													<option value="Ley u Ordenanza Municipal">Ley u Ordenanza Municipal</option>  
													<option value="Ninguno">Ninguno</option> 
													<option value="Resolución del Estado Plurinacional">Resolución del Estado Plurinacional</option> 
													<option value="Titulo de Patrimonio Inmaterial de la Humanidad">Titulo de Patrimonio Inmaterial de la Humanidad</option>
												</select> 
											</div>
									
											<div class="form-group col-md-6">
												<label class="control-label">
													Nro Instrumento Legal:
												</label>
												<input id="cnrolegal_i" type="text" class="form-control"  placeholder="Nro Instrumento Legal">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Datos Complementarios:
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="ccomplementarios_i"></textarea> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-12">
												<label class="control-label">
													Fuentes:
												</label>
												<textarea name="textarea" rows="4" cols="50" class="form-control " id="cfuentes_i"></textarea> 
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-6">
												<label class="control-label">
													Palabras Clave:
												</label>
												<input id="cpalabra_i" type="text" class="form-control"  placeholder="Palabras Clave">
											</div>
										</div>
										<div class="modal-footer">
										<button class="btn btn-primary" data-dismiss="modal" type="button">
            								<i class="fa fa-close"></i> Cancelar</button>
											<button class="btn btn-default "  type="button" onClick="limpiarA4()">
												<i class="fa fa-eraser"></i> Limpiar
											</button>
											<a type = "button" class = "btn btn-primary" id="registrarAmbito4" data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
										</div>

									</div>
									<!--fin div yacimiento-->

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end content--> 
			{!! Form::close() !!} 
		</div>
	</div>
</div>

<script type="text/javascript">
function tipoPatrimonioInmaterial(v){
   if (v.value==1) {
     document.getElementById("panelPatrimonioCultural").style="display:block;";
     document.getElementById("panelAmbito").style="display:none;"; 
   }
   if (v.value==2) {
     document.getElementById("panelPatrimonioCultural").style="display:none;";
     document.getElementById("panelAmbito").style="display:block;";
   } 
   if (v.value==3) {
     document.getElementById("panelPatrimonioCultural").style="display:none;";
     document.getElementById("panelAmbito").style="display:block;";
   } 
   if (v.value==4) {
     document.getElementById("panelPatrimonioCultural").style="display:none;";
     document.getElementById("panelAmbito").style="display:block;";
   } 
   if (v.value==5) {
     document.getElementById("panelPatrimonioCultural").style="display:none;";
     document.getElementById("panelAmbito").style="display:block;";
   } 
   if (v.value==6) {
     document.getElementById("panelPatrimonioCultural").style="display:none;";
     document.getElementById("panelAmbito").style="display:block;";
   } 
   if (v.value==7) {
     document.getElementById("panelPatrimonioCultural").style="display:none;";
     document.getElementById("panelAmbito").style="display:block;";
   } 


 }
</script>

