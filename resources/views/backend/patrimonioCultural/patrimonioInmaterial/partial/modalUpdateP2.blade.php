<div class="modal fade modal-default" id="myUpdatePatInmaterialP2" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     {!! Form::open(['class'=>'form-vertical','id'=>'formInmaterialP4_update'])!!}
     <div class="modal-header">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="row">
           <div class="col-md-12">
            <section class="content-header">
             <div class="header_title">
              <h3>
                EDITAR DATOS DE PATRIMONIO CULTURAL INMATERIAL  DEL MUNICIPIO DE LA PAZ
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </h3>
            </div>
          </section>
        </div>   
      </div>
    </div>
    <div  style="padding:20px">
      <input type="hidden" name="csrf-token" value=" {{ csrf_token(
      ) }}" id="token">   
      <input type="hidden" name="acasoId_p" id="acasoId_p">
      <input type="hidden" name="anroFicha_p" id="anroFicha_p">
    <div class="col-md-12">
      <div class="panel-heading">
        <label class="control-label" style="color:#275c26";>
          CONTEXTO ORIGINAL:
        </label>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group col-md-6">
        <label class="control-label">
          Bien cultural:
        </label>
        <input id="abien_cultural_p" type="text" class="form-control"  placeholder="Bien cultural">
      </div>

      <div class="form-group col-md-6">
        <label class="control-label">
          Tipo bien cultural:
        </label>
        <input id="atipo_cultural_p" type="text" class="form-control"  placeholder="Tipo bien cultural">
      </div> 

    </div> 
    <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
        Categoria declaratoria:
      </label>

      <select class="form-control" id="acategoria_declaratoria_p" >
        <option value=""> Selecionar</option> 
        <option value="1"> Estado Plurinacional de Bolivia </option>
        <option value="2"> Gobernacion</option>
        <option value="3"> Municipio</option>
        <option value="3"> Unesco</option> 
        <option value="3"> Ninguno</option>  
      </select> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Norma legal::
      </label>

      <select class="form-control" id="anorma_legal_p" >
        <option value=""> Selecionar</option> 
        <option value="1"> Patrimonio Nacional </option>
        <option value="2"> Patrimonio Municipal</option>
        <option value="3"> Patrimonio Departamental</option>
        <option value="3"> Patrimonio de la humanidad</option> 
        <option value="3"> Ninguno</option>  
      </select> 
    </div> 
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-6">
      <label class="control-label">
        Numero Legal:
      </label>
      <input id="anumero_legal_p"  type="text" class="form-control"  placeholder="Numero legal">
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Fecha de programacion:
      </label>
      <input id="afecha_programacion_p" name="cfecha_programacion" type="date" class="form-control"  placeholder="fecha de programacion">
    </div> 
  </div>
  <div class="col-md-12">
   <div class="form-group col-md-6">
    <label class="control-label">
     Palabras clave:
   </label>
   <input id="apalabra_clave_p" type="text" class="form-control"  placeholder="Palabra_clave"> 
 </div>
</div>

<div class="modal-footer">
 <button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
 <button class="btn btn-primary" id="actualizarInmaterialA2" data-dismiss="modal" type="button">
  <i class="glyphicon glyphicon-pencil"></i> Modificar
</button>
</div> 
{!! Form::close() !!} 

</div>
</div>
</div>
</div>
</div>
</div>
