@extends('backend.template.app')
@section('main-content')
@include('backend.patrimonioCultural.esculturasMonumentos.partial.modalCreate')
@include('backend.patrimonioCultural.esculturasMonumentos.partial.modalUpdate')

<input type="hidden" name="ccreador_es" id="ccreador_es">
<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>OPCIONES DE BUSQUEDA - ESCULTURAS</b></div>
    <div class="panel-body">     
      <br>
      <div class="col-md-12">
       <div class="form-group col-md-4">
        <label class="control-label">
          Tipo Patrimonio Cultural:
        </label>
        <input type="hidden" name="tipoPatriCultural" id="tipoPatriCultural" value="1">
        <select class="form-control" name="tipoPatriCulturalMuestra" id="tipoPatriCulturalMuestra" disabled="">
          <option selected="">ESCULTURAS</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Campo:
        </label>
        <select class="form-control" name="camposPorPatrimonio" id="camposPorPatrimonio">
          <option value="-1">Seleccionar...</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Valor:
        </label>
        <input type="text" id="valorPatrimonio" class="form-control" name="valorPatrimonio">
        <input class="form-control" type="hidden" id="usuario_avatar" name="usuario_avatar"></input>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="container">
        <div class="col-md-6"></div>
        <div class="col-md-6"> 
          <button type ="button" class = "btn btn-primary" onclick="buscarPatrimonioTipoCampo(1)"> <i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-primary" onclick="limpiarPrincipal()"> <i class="fa fa fa-eraser"></i> Limpiar</button>
          <a class= "btn btn-primary" data-target= "#myCreateMonumentoEscultura" data-toggle= "modal"><i class="glyphicon glyphicon-plus"></i> Nueva Ficha</a>
     </div>
   </div>
 </div>
</div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-body">
        <div id="listPatrimonio">
        </div>  
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@push('scripts')
<script>


  var i = 20;
  var _usrid={{$usuarioId}};
  var macrodistrito_descripcion='0';
  var contespaciosabiertos = 1;
  var contconjuntos = 1;
  var contesculturas = 1;
  var correlativog="";

  function buscarPatrimonioPorTipo(paging_pagenumbe)
  { 
    document.getElementById("listado_campo_valor").style = "display:none;";
    document.getElementById("listado_principal").style = "display:block;";
    var paging_pagesize = $( "#reg" ).val(); 
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1201',"parametros": '{"paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'}; 
    var band = 'list_principal';
    buscar(formData, band); 
  }
  function buscarPatrimonioTipoCampo(paging_pagenumbe){
      document.getElementById("listado_campo_valor").style = "display:block;";
    document.getElementById("listado_principal").style = "display:none;";
    var paging_pagesize = $( "#reg_campo" ).val(); 
    var campoPatrimonio = $( '#camposPorPatrimonio' ).val();
    var valorPatrimonio = $( '#valorPatrimonio' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1202',"parametros": '{"xcampo_id":' +campoPatrimonio+ ', "xcampo_valor":"' +valorPatrimonio+ '","xpaging_pagesize":'+paging_pagesize+',"xpaging_pagenumber":'+paging_pagenumbe+'}'};  
    var band = 'list_por_campo'; 
    buscar(formData, band,444444444);  
  }

  function limpiarPrincipal()
  {
    $( '#tipoPatriCultural' ).val("");
    $( '#camposPorPatrimonio' ).val("");
    $( '#valorPatrimonio' ).val("");
  }

  /*function listarPatrimonioCultural(g_tipo){
    var tipo = 1;
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-933',"parametros": '{"xtipo_id":' +tipo+ '}'}; 
    buscar(formData);  
  }*/

  function generarProforma(id_cas, g_tipo){
    setTimeout(function(){
     
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerar',['datos'=>'d1']) }}"; 
      urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirArchivoFotografico(id_cas, g_tipo){
    setTimeout(function(){
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerarArchFoto',['datos'=>'d1']) }}"; 
       urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirFichaTodo(id_cas, g_tipo){
    setTimeout(function(){
      var urlFicha = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
      urlFicha = urlFicha.replace('d1', id_cas);
    ////console.log("urlFicha",urlFicha);
    window.open(urlFicha); 
  }, 500);
  }


  function buscar(formData, band){

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-patrimonio" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Nro de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código Catastral</th>';
    htmlListaBus += '<th align="left" class="form-natural">Denominación</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Fecha</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Adjuntos</th>' ;
    htmlListaBus += '<th width="15%" align="center" class="form-natural">Impresiones</th></tr>' ;
    htmlListaBus += '</thead>'; 
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          var datos = dataIN;
          console.log('busquedaaaaa',dataIN);
          var tam = datos.length;
        //////console.log('tam',tam);
        if(dataIN == "[{ }]"){
          tam = 0;
        }; 
        var tipoContribuyenteEnvio = $( "#tipo-contribuyente" ).val();

        for (var i = 0; i < tam; i++){ 
          var cas_id = JSON.parse(dataIN[i].data).cas_id;
          var dataP= JSON.parse(dataIN[i].data);
          var g_tipo='PTR_IME'; 
          var numero_ficha = JSON.parse(dataIN[i].data).cas_nro_caso;
           var codigo_ficha = JSON.parse(dataIN[i].data).cas_nombre_caso;
          var codigo_catastral = '';
          var fecha_registro = JSON.parse(dataIN[i].data).cas_registrado;
         fecha_registro = fecha_registro.split('T');
         fecha_registro = fecha_registro[0];
          var denominacion = JSON.parse(dataIN[i].data).ptr_nombre;
         
         htmlListaBus += '<tr>';
         htmlListaBus += '<td align="left">'+(i+1)+'</td>';
         htmlListaBus += '<td align="center">';
         htmlListaBus+='<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + cas_id +',\''+ g_tipo +'\');">';
         htmlListaBus+='<i class="glyphicon glyphicon-trash"></i></button>';
         htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateMonumentoEscultura" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarMonumentoEscultura('+ cas_id +');"><i class="glyphicon glyphicon-pencil"></i></button>';       
        htmlListaBus += '</td>';
        htmlListaBus += '<td align="left" class="form-natural">'+numero_ficha+'</td>'; 
        htmlListaBus += '<td align="left" class="form-natural">'+codigo_ficha+'</td>';                           
        htmlListaBus += '<td align="left" class="form-natural">'+codigo_catastral+'</td>'; 
        htmlListaBus += '<td align="left" class="form-natural">'+denominacion+'</td>';                           
        htmlListaBus += '<td align="left" class="form-natural">'+fecha_registro+'</td>';
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', cas_id);
        url = url.replace('numeroficha1', numero_ficha);

        htmlListaBus+='<td style="text-align: center;"><a href="'+url+'" class="btncirculo btn-xs btn-default" fa fa-plus-square pull-right"  > <i class="fa fa-folder"></i></a>';
        htmlListaBus+='</td>';
        htmlListaBus+='<td align="center">';
        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Ficha" type="button" onClick= "generarProforma(' + cas_id +',\''+ g_tipo +'\' );">';
        htmlListaBus+='<i class="glyphicon glyphicon-print"></i></button> &nbsp;';

        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Archivo Fotografico" type="button" onClick= "imprimirArchivoFotografico(' + cas_id +',\''+ g_tipo +'\' );">';
        htmlListaBus+='<i class="glyphicon glyphicon-camera"></i></button> &nbsp;';

        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Todo" type="button" onClick= "imprimirFichaTodo(' + cas_id +',\''+ g_tipo +'\' );">';
        htmlListaBus+='<i class="glyphicon glyphicon-list"></i></button>';

        htmlListaBus+='</td>';

        htmlListaBus += '</tr>';
      } 
      htmlListaBus += '</table></div></div>';
      htmlListaBus +='<div>';
      htmlListaBus +='<ul class="pager">';
      if(band =='list_principal'){
      htmlListaBus +='<li><a href="#" onClick="btnAnterior_prin()">Previous</a></li>';
      htmlListaBus +='<li><a href="#" onClick="btnSiguiente_prin()">Next</a></li>';
      }else{
      htmlListaBus +='<li><a href="#" onClick="btnAnterior_campo_valor()">Previous</a></li>';
      htmlListaBus +='<li><a href="#" onClick="btnSiguiente_campo_valor()">Next</a></li>';  
      }
      htmlListaBus+='</ul>';
      htmlListaBus +='</div>';

      $( '#listPatrimonio' ).html(htmlListaBus);
    },
  error: function(result) {
    swal( "Error..!", "Error....", "error" );
  }
});
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});
};
function getBool (val){
  return !!JSON.parse(String(val).toLowerCase());
}

function editarMonumentoEscultura(cas_id){
    var formData = {"identificador": "SERVICIO_PATRIMONIO-1016", "parametros":'{"id_caso":'+cas_id+'}'};
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {
       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
         var cas_id = JSON.parse(dataIN[0].data).cas_id;
         var numero_ficha = JSON.parse(dataIN[0].data).cas_nro_caso;
         //console.log(numero_ficha,'numero_ficha');
         var g_tipo = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.g_tipo !== 'undefined'){
            g_tipo = JSON.parse(dataIN[0].data).cas_datos.g_tipo;
         }
         var codigo_ficha = JSON.parse(dataIN[0].data).cas_nombre_caso;      
         var denominacion = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_NOMBRE !== 'undefined'){
            denominacion = JSON.parse(dataIN[0].data).cas_datos.PTR_NOMBRE;
         }
         var epocaelaboracion = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_EPOCA !== 'undefined'){
            epocaelaboracion = JSON.parse(dataIN[0].data).cas_datos.PTR_EPOCA;
         }
         var pais = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_PAIS !== 'undefined'){
            pais = JSON.parse(dataIN[0].data).cas_datos.PTR_PAIS;
          }
         var departamento = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_DEP !== 'undefined'){
            departamento = JSON.parse(dataIN[0].data).cas_datos.PTR_DEP;
          }
         var municipio = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_MUNI !== 'undefined'){
            municipio = JSON.parse(dataIN[0].data).cas_datos.PTR_MUNI;
          }
         var provincia = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_PROV !== 'undefined'){
            provincia = JSON.parse(dataIN[0].data).cas_datos.PTR_PROV;
          }
         var macrodistrito = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_MACRO !== 'undefined'){
            macrodistrito = JSON.parse(dataIN[0].data).cas_datos.PTR_MACRO;
          }
         var zona = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_ZONA !== 'undefined'){
            zona = JSON.parse(dataIN[0].data).cas_datos.PTR_ZONA;
          }
         var comunidad = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_COMU !== 'undefined'){
            comunidad = JSON.parse(dataIN[0].data).cas_datos.PTR_COMU;
          }
         var direccion = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_DIR !== 'undefined'){
            direccion = JSON.parse(dataIN[0].data).cas_datos.PTR_DIR;
          }
         var datosautor = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_AU !== 'undefined'){
            datosautor = JSON.parse(dataIN[0].data).cas_datos.PTR_AU;
          }
         var especialidad = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_ESP !== 'undefined'){
            especialidad = JSON.parse(dataIN[0].data).cas_datos.PTR_ESP;
          }
         var tecnicamaterial = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_TEC_MAT !== 'undefined'){
            tecnicamaterial = JSON.parse(dataIN[0].data).cas_datos.PTR_TEC_MAT;
          }
         var piezaesculturica = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_CONJ_ESC !== 'undefined'){
            piezaesculturica = JSON.parse(dataIN[0].data).cas_datos.PTR_CONJ_ESC;
          }
         var alto = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_ALTO !== 'undefined'){
            alto = JSON.parse(dataIN[0].data).cas_datos.PTR_ALTO;
          }
         var ancho = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_ANCHO !== 'undefined'){
            ancho = JSON.parse(dataIN[0].data).cas_datos.PTR_ANCHO;
          }
         var peso = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_PESO !== 'undefined'){
            peso = JSON.parse(dataIN[0].data).cas_datos.PTR_PESO;
          }
         var descripcionobra = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_DESC_OBRA !== 'undefined'){
            descripcionobra = JSON.parse(dataIN[0].data).cas_datos.PTR_DESC_OBRA;
          }
         var origenpieza = "";
         if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_ORG_PIE !== 'undefined'){
          origenpieza = JSON.parse(dataIN[0].data).cas_datos.PTR_ORG_PIE;
         }
         var nombrepropietario = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_PROP !== 'undefined'){
            nombrepropietario = JSON.parse(dataIN[0].data).cas_datos.PTR_PROP;
          }
         var responsable = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_RESP !== 'undefined'){
            responsable = JSON.parse(dataIN[0].data).cas_datos.PTR_RESP;
          }
         var tipo = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_TIPO !== 'undefined'){
            tipo = JSON.parse(dataIN[0].data).cas_datos.PTR_TIPO;
          }
         var ordenanzamunicipal = "";
         if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_LEY_OR !== 'undefined'){
          ordenanzamunicipal = JSON.parse(dataIN[0].data).cas_datos.PTR_LEY_OR;
         }
         var descripcionpersonaje = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_DES_PER !== 'undefined'){
            descripcionpersonaje = JSON.parse(dataIN[0].data).cas_datos.PTR_DES_PER;
          }
         var referenciahistorica = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_REF_HIST !== 'undefined'){
            referenciahistorica = JSON.parse(dataIN[0].data).cas_datos.PTR_REF_HIST;
          }
         var estadoconservacion = "";//PTR_REF_CON
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_EST_CON !== 'undefined'){
            estadoconservacion = JSON.parse(dataIN[0].data).cas_datos.PTR_EST_CON;
          }
         var observaciones = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_OBS !== 'undefined'){
            observaciones = JSON.parse(dataIN[0].data).cas_datos.PTR_OBS;
          }
         var fuentesbibliograficos = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_FUENT_BIBLIO !== 'undefined'){
            fuentesbibliograficos = JSON.parse(dataIN[0].data).cas_datos.PTR_FUENT_BIBLIO;
           }
          //Campos faltantes
          var PTR_INV = JSON.parse(dataIN[0].data).cas_datos.PTR_INV;
          var PTR_REV = JSON.parse(dataIN[0].data).cas_datos.PTR_REV;
          var AE_NRO_CASO = JSON.parse(dataIN[0].data).cas_datos.AE_NRO_CASO;
          var PTR_FEC_INV = JSON.parse(dataIN[0].data).cas_datos.PTR_FEC_INV;
          var PTR_FEC_REV = JSON.parse(dataIN[0].data).cas_datos.PTR_FEC_REV;
          var PTR_NRO_FIC = JSON.parse(dataIN[0].data).cas_datos.PTR_NRO_FIC;
          var g_fecha = JSON.parse(dataIN[0].data).cas_datos.g_fecha;
          var g_usuario = JSON.parse(dataIN[0].data).cas_datos.g_usuario;
          var g_datos_solicitante = JSON.parse(dataIN[0].data).cas_datos.g_datos_solicitante;     
        var datosautor = caracteresEspecialesActualizar(datosautor);
        var descripcionobra = caracteresEspecialesActualizar(descripcionobra);
        var origenpieza = caracteresEspecialesActualizar(origenpieza);
        var descripcionpersonaje = caracteresEspecialesActualizar(descripcionpersonaje);
        var referenciahistorica = caracteresEspecialesActualizar(referenciahistorica);
        var observaciones = caracteresEspecialesActualizar(observaciones);
        var fuentesbibliograficos = caracteresEspecialesActualizar(fuentesbibliograficos);
        $( "#acas_id" ).val(cas_id);
        $( "#aescnumero_ficha" ).val(numero_ficha);
        $( "#aesccodigo_ficha" ).val(codigo_ficha);
        $( "#ag_tipo" ).val(g_tipo);
        $( "#anumeroficha" ).val(codigo_ficha);
        $( "#aescdenominacion" ).val(denominacion);
        $( "#aepocaelaboracion").val(epocaelaboracion);
        $( "#apais" ).val(pais);
        $( "#adepartamento" ).val(departamento);
        $( "#amunicipio" ).val(municipio);
        $( "#aprovincia" ).val(provincia);
        $( "#amacrodistrito" ).val(macrodistrito);
        document.getElementById("amacrodistritoescultura").length=0;
        document.getElementById("cmacrodistritoescultura").length=1;
        listaMacrodistritoescultura(macrodistrito);
        $( "#azona" ).val(zona);
        $( "#acomunidad").val(comunidad);
        $( "#adireccion" ).val(direccion);
        $( "#adatosautor" ).val(datosautor);
        $( "#aespecialidad" ).val(especialidad);
        $( "#atecnicamaterial" ).val(tecnicamaterial);
        //$( "#apiezaesculturica" ).val(piezaesculturica);
        document.getElementById("apiezaesculturica1").checked = piezaesculturica[1].estado;
        document.getElementById("apiezaesculturica2").checked = piezaesculturica[2].estado;

        $( "#aalto" ).val(alto);
        $( "#aancho" ).val(ancho);
        $( "#apeso" ).val(peso);
        $( "#adescripcionobra" ).val(descripcionobra);
        $( "#aorigenpieza" ).val(origenpieza);
        $( "#anombrepropietarioescultura" ).val(nombrepropietario);
        $( "#aresponsable" ).val(responsable);
        $( "#atipo" ).val(tipo);
        $( "#aordenanzamunicipal" ).val(ordenanzamunicipal);
        $( "#adescripcionpersonaje" ).val(descripcionpersonaje);
        $( "#areferenciahistorica" ).val(referenciahistorica);
        $( "#aestadoconservacion" ).val(estadoconservacion);
        $( "#aobservaciones" ).val(observaciones);
        $( "#afuentesbibliograficos" ).val(fuentesbibliograficos); 
        //Datos Faltantes
        $( "#ptrinv").val(PTR_INV);
        $( "#ptrrev").val(PTR_REV);
        $( "#aenrocaso").val(AE_NRO_CASO);
        $( "#ptrfecinv").val(PTR_FEC_INV);
        $( "#ptrfecrev").val(PTR_FEC_REV);
        $( "#ptrnrofic").val(PTR_NRO_FIC);
        $( "#gfecha").val(g_fecha);
        $( "#gusuario").val(g_usuario);
        $( "#gdatossolicitante").val(g_datos_solicitante);
      
           },
        error: function(result) {
          swal( "Error..!", "Error....", "error" );
        }
      });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
      });
};


function selectCampos()
{  
  var tipoCampoPatrimonio = $('#tipoPatriCultural').val();
  document.getElementById("camposPorPatrimonio").length=1;
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-931',"parametros": '{"ytipoid":'+tipoCampoPatrimonio+'}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        //////////console.log("listadoCampos",dataIN);
        for(var i = 0; i < dataIN.length; i++){
         var campo_id = dataIN[i].xcampo_id;
         var campo_tipo = JSON.parse(dataIN[i].xcampo_data).campoTipo;   
         document.getElementById("camposPorPatrimonio").innerHTML += '<option value=' +campo_id+ '>' +campo_tipo+ '</option>';            
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
}

function obtenercorrelativo(tipoPatrimonio)
{

 $.ajax({
  type        : 'GET',
  url         : urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": 'SERVICIO_PATRIMONIO-936',"parametros":'{"xtipoPat":"'+tipoPatrimonio+'"}'};
    //////////console.log("formdataaa",formData);
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
      var data=dataIN;
      correlativog=dataIN[0].sp_correlativo_patrimonio4;       
    //////////console.log("correl99999999999999999ativog",correlativog);


  },     
  error: function (xhr, status, error) { }
});
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

}

/*-----------------BEGIN ESCULTURAS Y MONUMENTOS---------------------*/

$("#guardarEsculturas").click(function(){
  obtenercorrelativo('PTR_IME');
  setTimeout(function(){
  var PTR_NOMBRE = $( "#cescdenominacion" ).val();
  var PTR_EPOCA = $("#cepocaelaboracion").val();
  var PTR_PAIS = $( "#cpais" ).val();
  var PTR_DEP = $( "#cdepartamento" ).val();
  var PTR_MUNI = $( "#cmunicipio" ).val();
  var PTR_PROV = $( "#cprovincia" ).val();
  var macrodistritoTxt = document.getElementById("cmacrodistritoescultura");
  var PTR_MACRO = macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
  var PTR_ZONA = $( "#czona" ).val();
  var PTR_COMU = $( "#ccomunidad").val();
  var PTR_DIR = $( "#cdireccion" ).val();
  var PTR_AU = $( "#cdatosautor" ).val(); //duda
  var PTR_ESP = $( "#cespecialidad" ).val();
  var PTR_TEC_MAT = $( "#ctecnicamaterial" ).val(); 
  //var PTR_CONJ_ESC = $( "#cpiezaesculturica" ).val();
  var PTR_ALTO = $( "#calto" ).val();
  var PTR_ANCHO = $( "#cancho" ).val();
  var PTR_PESO = $( "#cpeso" ).val();
  var PTR_DESC_OBRA = $( "#cdescripcionobra" ).val();
  var PTR_ORG_PIE = $( "#corigenpieza" ).val();
  var PTR_PROP = $( "#cnombrepropietarioescultura" ).val();
  var PTR_RESP = $( "#cresponsable" ).val();
  var PTR_TIPO = $( "#ctipo" ).val();
  var PTR_LEY_OR = $( "#cordenanzamunicipal" ).val();
  var PTR_DES_PER = $( "#cdescripcionpersonaje" ).val();
  var PTR_REF_HIST = $( "#creferenciahistorica" ).val();
  var PTR_EST_CON = $( "#cestadoconservacion" ).val(); //PTR_REF_CON
  var PTR_OBS = $( "#cobservaciones" ).val();
  var PTR_FUENT_BIBLIO = $( "#cfuentesbibliograficos" ).val(); 
  var g_tipo = 'PTR_IME';
  var PTR_AU = caracteresEspecialesRegistrar(PTR_AU);
  var PTR_DESC_OBRA = caracteresEspecialesRegistrar(PTR_DESC_OBRA);
  var PTR_ORG_PIE = caracteresEspecialesRegistrar(PTR_ORG_PIE);
  var PTR_DES_PER = caracteresEspecialesRegistrar(PTR_DES_PER);
  var PTR_REF_HIST = caracteresEspecialesRegistrar(PTR_REF_HIST);
  var PTR_OBS = caracteresEspecialesRegistrar(PTR_OBS);
  var PTR_FUENT_BIBLIO = caracteresEspecialesRegistrar(PTR_FUENT_BIBLIO);
  var cpiezaesculturica1 = document.getElementById("cpiezaesculturica1").checked;
  var cpiezaesculturica2 = document.getElementById("cpiezaesculturica2").checked;
  var cpiezaesculturica_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cpiezaesculturica1+', "resvalor": "Conjunto Escultórico"},{"resid": 285, "estado": '+cpiezaesculturica2+', "resvalor": "Una sola pieza"}]';
  //CAMPOS FALTANTES
  var PTR_INV = $( "#ccreador_es" ).val();
  var PTR_REV = "";
  var cas_nombre_caso = 'PTR_IME'+correlativog+'/2018';
  var AE_NRO_CASO = cas_nombre_caso;
  var PTR_FEC_INV = "";
  var PTR_FEC_REV = "";
  var PTR_NRO_FIC = cas_nombre_caso;
  var fecha1 = new Date();
  var fecha = fecha1.getDate() + "-" +fecha1.getMonth()+ "-" + fecha1.getFullYear();
  //var gestiones = fecha.getFullYear();
  var g_fecha = fecha;
  var g_usuario = $("#usuario_avatar").val();
  var g_datos_solicitante ='[]';
  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOMBRE":"'+PTR_NOMBRE+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_PAIS":"'+PTR_PAIS+'","PTR_DEP":"'+PTR_DEP+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_PROV":"'+PTR_PROV+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_ZONA":"'+PTR_ZONA+'","PTR_COMU":"'+PTR_COMU+'","PTR_DIR":"'+PTR_DIR+'","PTR_AU":"'+PTR_AU+'","PTR_ESP":"'+PTR_ESP+'","PTR_TEC_MAT":"'+PTR_TEC_MAT+'","PTR_CONJ_ESC":'+cpiezaesculturica_check+',"PTR_ALTO":"'+PTR_ALTO+'","PTR_ANCHO":"'+PTR_ANCHO+'","PTR_PESO":"'+PTR_PESO+'","PTR_DESC_OBRA":"'+PTR_DESC_OBRA+'","PTR_ORG_PIE":"'+PTR_ORG_PIE+'","PTR_PROP":"'+PTR_PROP+'","PTR_RESP":"'+PTR_RESP+'","PTR_TIPO":"'+PTR_TIPO+'","PTR_LEY_OR":"'+PTR_LEY_OR+'","PTR_DES_PER":"'+PTR_DES_PER+'","PTR_REF_HIST":"'+PTR_REF_HIST+'","PTR_EST_CON":"'+PTR_EST_CON+'","PTR_OBS":"'+PTR_OBS+'","PTR_FUENT_BIBLIO":"'+PTR_FUENT_BIBLIO+'","PTR_INV":"'+PTR_INV+'","PTR_REV":"'+PTR_REV+'","AE_NRO_CASO":"'+AE_NRO_CASO+'","PTR_FEC_INV":"'+PTR_FEC_INV+'","PTR_FEC_REV":"'+PTR_FEC_REV+'","PTR_NRO_FIC":"'+PTR_NRO_FIC+'","g_fecha":"'+g_fecha+'","g_usuario":"'+g_usuario+'","g_datos_solicitante":"'+g_datos_solicitante+'"}';

  var data = JSON.stringify(data);
 ////console.log("datadataESCULTURA",data);
 var cas_nro_caso = correlativog ;
 var cas_act_id = 137 ;
 var cas_usr_actual_id = 1155 ;
 var PTR_NRO_FIC = $( "#cnumeroficha" ).val();
      
 var cas_estado_paso = 'RECIBIDO';
 var cas_nodo_id = 1; 
 var cas_usr_id = 1;
 var cas_ws_id = 1;  
 var cas_asunto = 'asunto';
 var cas_tipo_hr = 'hora';
 var cas_id_padre = 1;
 var campo_f = 'campo';

 contesculturas = contesculturas+1; 
 var formData = {"identificador":"SERVICIO_PATRIMONIO-941","parametros":'{"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");

  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 


        //console.log("dataaEEEEEa",data);
        buscarPatrimonioPorTipo(1); 
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );
        var numeroficha='PTR_IME'+contesculturas+'/2018';
        $( "#cnumeroficha" ).val(numeroficha); 

        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
        setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', cas_nro_caso);
          console.log("url",url);
          location.href = url;
        }, 1000);           
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 800);
});



$("#actualizaresculturas").click(function(){
  var cas_id = $( "#acas_id" ).val();
  var cas_nro_caso = $( "#aescnumero_ficha" ).val();
  var PTR_NOMBRE = $( "#aescdenominacion" ).val();
  var PTR_EPOCA = $("#aepocaelaboracion").val();
  var PTR_PAIS = $( "#apais" ).val();
  var PTR_DEP = $( "#adepartamento" ).val();
  var PTR_MUNI = $( "#amunicipio" ).val();
  var PTR_PROV = $( "#aprovincia" ).val();
  var macrodistritoTxt=document.getElementById("amacrodistritoescultura");
  var PTR_MACRO=macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
  var PTR_ZONA = $( "#azona" ).val();
  var PTR_COMU = $( "#acomunidad").val();
  var PTR_DIR = $( "#adireccion" ).val();
  var PTR_AU = $( "#adatosautor" ).val(); //duda
  var PTR_ESP = $( "#aespecialidad" ).val();
  var PTR_TEC_MAT = $( "#atecnicamaterial" ).val();
  //var PTR_CONJ_ESC = $( "#apiezaesculturica" ).val();
  var PTR_ALTO = $( "#aalto" ).val();
  var PTR_ANCHO = $( "#aancho" ).val();
  var PTR_PESO = $( "#apeso" ).val();
  var PTR_DESC_OBRA = $( "#adescripcionobra" ).val();
  var PTR_ORG_PIE = $( "#aorigenpieza" ).val();
  var PTR_PROP = $( "#anombrepropietarioescultura" ).val();
  var PTR_RESP = $( "#aresponsable" ).val();
  var PTR_TIPO = $( "#atipo" ).val();
  var PTR_LEY_OR = $( "#aordenanzamunicipal" ).val();
  var PTR_DES_PER = $( "#adescripcionpersonaje" ).val();
  var PTR_REF_HIST = $( "#areferenciahistorica" ).val();
  var PTR_EST_CON = $( "#aestadoconservacion" ).val(); //PTR_REF_CON
  var PTR_OBS = $( "#aobservaciones" ).val();
  var PTR_FUENT_BIBLIO = $( "#afuentesbibliograficos" ).val(); 
  var g_tipo = 'PTR_IME';

  var PTR_AU = caracteresEspecialesRegistrar(PTR_AU);
  var PTR_DESC_OBRA = caracteresEspecialesRegistrar(PTR_DESC_OBRA);
  var PTR_ORG_PIE = caracteresEspecialesRegistrar(PTR_ORG_PIE);
  var PTR_DES_PER = caracteresEspecialesRegistrar(PTR_DES_PER);
  var PTR_REF_HIST = caracteresEspecialesRegistrar(PTR_REF_HIST);
  var PTR_OBS = caracteresEspecialesRegistrar(PTR_OBS);
  var PTR_FUENT_BIBLIO = caracteresEspecialesRegistrar(PTR_FUENT_BIBLIO);

  var apiezaesculturica1 = document.getElementById("apiezaesculturica1").checked;
  var apiezaesculturica2 = document.getElementById("apiezaesculturica2").checked;
  var apiezaesculturica_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+apiezaesculturica1+', "resvalor": "Conjunto Escultórico"},{"resid": 285, "estado": '+apiezaesculturica2+', "resvalor": "Una sola pieza"}]';
  //datos faltantes
  var PTR_INV = $( "#ptrinv").val();
  var PTR_REV = $( "#ptrrev").val();
  var AE_NRO_CASO = $( "#aenrocaso").val();
  var PTR_FEC_INV = $( "#ptrfecinv").val();
  var PTR_FEC_REV = $( "#ptrfecrev").val();
  var PTR_NRO_FIC = $( "#ptrnrofic").val();
  var g_fecha = $( "#gfecha").val();
  var g_usuario = $( "#gusuario").val();
  var g_datos_solicitante = $( "#gdatossolicitante").val();

  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOMBRE":"'+PTR_NOMBRE+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_PAIS":"'+PTR_PAIS+'","PTR_DEP":"'+PTR_DEP+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_PROV":"'+PTR_PROV+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_ZONA":"'+PTR_ZONA+'","PTR_COMU":"'+PTR_COMU+'","PTR_DIR":"'+PTR_DIR+'","PTR_AU":"'+PTR_AU+'","PTR_ESP":"'+PTR_ESP+'","PTR_TEC_MAT":"'+PTR_TEC_MAT+'","PTR_CONJ_ESC":'+apiezaesculturica_check+',"PTR_ALTO":"'+PTR_ALTO+'","PTR_ANCHO":"'+PTR_ANCHO+'","PTR_PESO":"'+PTR_PESO+'","PTR_DESC_OBRA":"'+PTR_DESC_OBRA+'","PTR_ORG_PIE":"'+PTR_ORG_PIE+'","PTR_PROP":"'+PTR_PROP+'","PTR_RESP":"'+PTR_RESP+'","PTR_TIPO":"'+PTR_TIPO+'","PTR_LEY_OR":"'+PTR_LEY_OR+'","PTR_DES_PER":"'+PTR_DES_PER+'","PTR_REF_HIST":"'+PTR_REF_HIST+'","PTR_EST_CON":"'+PTR_EST_CON+'","PTR_OBS":"'+PTR_OBS+'","PTR_FUENT_BIBLIO":"'+PTR_FUENT_BIBLIO+'","PTR_INV":"'+PTR_INV+'","PTR_REV":"'+PTR_REV+'","AE_NRO_CASO":"'+AE_NRO_CASO+'","PTR_FEC_INV":"'+PTR_FEC_INV+'","PTR_FEC_REV":"'+PTR_FEC_REV+'","PTR_NRO_FIC":"'+PTR_NRO_FIC+'","g_fecha":"'+g_fecha+'","g_usuario":"'+g_usuario+'","g_datos_solicitante":"'+g_datos_solicitante+'"}';

  var data = JSON.stringify(data);
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  var PTR_NRO_FIC = $( "#anumeroficha" ).val();
  var cas_nombre_caso = PTR_NRO_FIC;  
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-943","parametros":'{"xcas_id":'+cas_id+',"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        buscarPatrimonioPorTipo(1); 
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );

      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 
});

function listaMacrodistritoescultura(macrodistrito_descripcion)
{ 
  $.ajax(
  {
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) 
    {         
      var formData = {"identificador": 'SISTEMA_VALLE-616',"parametros": '{}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) 
       {  
        var datos = dataIN;
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          document.getElementById("cmacrodistritoescultura").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+ '</option>';
        }
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          if (macrodistrito == macrodistrito_descripcion) 
          {       
            document.getElementById("amacrodistritoescultura").innerHTML += '<option value='+macrodistrito+' selected>'+macrodistrito+'</option>';
          }
          else
          {
            document.getElementById("amacrodistritoescultura").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+'</option>';
          } 
        }      
      },     
      error: function (xhr, status, error) { }
    });
    },
    error: function(result) {
      swal( "Error..!", "No se puede recuperar los datos", "error" );
    },
  });
};


/*-----------------END ESCULTURAS Y MONUMENTOS-----------------------*/
function darBaja(id, g_tipo)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-934","parametros": '{"xcas_id":'+id+', "xcas_usr_id":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
           buscarPatrimonioPorTipo(1); 
           swal("Patrimonio!", "Fue eliminado correctamente!", "success");

         },
         error: function( result ) 
         {
          swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SERVICIO_PATRIMONIO-1023","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
       ////////console.log(dataIN);
       var primerNombre = dataIN[0].vprs_nombres;
       var primerApellido = dataIN[0].vprs_paterno;
       var segundoApellido = dataIN[0].vprs_materno;
       var cedulaIdentidad = dataIN[0].vprs_ci;
       var razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
       var usuario_avatar = dataIN[0].vusr_usuario;
       
       $("#ccreador_es").val(primerNombre+' '+primerApellido+' '+segundoApellido);
     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};



$(document).ready(function (){
 
 cargarDatosUsuario();
 selectCampos();
 buscarPatrimonioPorTipo(1);
 listaMacrodistritoescultura(macrodistrito_descripcion);
 var numeroficha='PTR_IME'+contesculturas+'/2018';
 $( "#cnumeroficha" ).val(numeroficha);

 document.getElementById("listado_campo_valor").style = "display:none;";

});
</script>

<script  src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script >

  $('textarea').ckeditor();


</script>
@endpush