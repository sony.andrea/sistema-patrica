<div class="modal fade modal-default" id="myCreateMonumentoEscultura" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h3>
              Nueva Ficha Esculturas y Monumentos
              <small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h3>
          </div>
        </section>
      </div>   
    </div>
</div>
<div  style="padding:20px">
<input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
<input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">
<input type="hidden" name="cpiezaesculturicacheck" id="cpiezaesculturicacheck">

<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       I. IDENTIFICACIÓN DE LA FICHA:
      </label>
    </div>
</div>    
<div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Nro de Ficha:
      </label>
      <input id="cnumeroficha" type="text" class="form-control"  placeholder="Nro de Ficha" disabled> 
    </div>
{!! Form::open(['class'=>'form-vertical puestoMercado-validate','id'=>'formNuevaFicha_create'])!!}
    <div class="form-group col-md-6">
      <label class="control-label">
       Denominación:
     </label>
     <input id="cescdenominacion" type="text" class="form-control"  placeholder="Denominación"> 
   </div>
</div>
<div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Época o año de elaboración de la obra:
      </label>
      <input id="cepocaelaboracion" type="text" class="form-control"  placeholder="Época o año de elaboración de la obra"> 
    </div>
    <div class="form-group col-md-6">
    </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       II. LOCALIZACIÓN:
      </label>
    </div>
 </div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        País:
      </label>
      <input id="cpais" type="text" class="form-control"  placeholder="País" value="Bolivia" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Departamento:
     </label>
     <input id="cdepartamento" type="text" class="form-control"  placeholder="Departamento" value="La Paz" disabled> 
   </div>
</div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Municipio:
      </label>
      <input id="cmunicipio" type="text" class="form-control"  placeholder="Municipio" value="La Paz" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Provincia:
     </label>
     <input id="cprovincia" type="text" class="form-control"  placeholder="Provincia" value="Murillo" disabled> 
   </div>
</div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        MacroDistrito:
      </label>
      
    <select id="cmacrodistritoescultura" name="cmacrodistritoescultura" class="form-control">
      <option value="-1">Seleccionar...</option>
    </select>
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Zona:
     </label>
     <input id="czona" type="text" class="form-control"  placeholder="Zona"> 
   </div>
</div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Comunidad:
      </label>
      <input id="ccomunidad" type="text" class="form-control"  placeholder="Comunidad"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Dirección:
     </label>
     <input id="cdireccion" type="text" class="form-control"  placeholder="Dirección"> 
   </div>
</div>

<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       III. IDENTIFICACIÓN:
      </label>
    </div>
</div>
<div class="col-md-12">
   <div class="form-group col-md-12">
      <label class="control-label" for="cdatosautor">
        1. Datos del autor:
      </label>
        <textarea id="cdatosautor" class="form-control "  placeholder="Descripción de la Obra(abierto)"></textarea>  
    </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       2.Datos de la Obra:
      </label>
    </div>
</div>
<div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Especialidad:
      </label>
      <input id="cespecialidad" type="text" class="form-control"  placeholder="Especialidad"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Técnica y Material:
     </label>
     <input id="ctecnicamaterial" type="text" class="form-control"  placeholder="Técnica y Material"> 
   </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
    Piezas Escultóricas: 
    </label>
    <br>
    <span class="block input-icon input-icon-right">
      <label class="checkbox-inline">
         <input type="checkbox" id="cpiezaesculturica1">Conjunto Escultórico
      </label>
      <label class="checkbox-inline">
         <input type="checkbox" id="cpiezaesculturica2">Una sola pieza
      </label>
    </span>
  </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       3. Dimensiones de la Obra:
      </label>
    </div>
 </div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Alto(m):
  </label>
 <input id="calto" type="text" class="form-control"  placeholder="Alto(m)"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Ancho(m):
  </label>
  <input id="cancho" type="text" class="form-control"  placeholder="Ancho(m)s"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Peso:
  </label>
 <input id="cpeso" type="text" class="form-control"  placeholder="Peso"> 
</div>
 <!--<div class="form-group col-md-6">
  <label class="control-label">
  Profundidad:
  </label>
 <input id="cprofun" type="text" class="form-control"  placeholder="Profundidad"> 
</div>-->
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label" for="cdescripcionobra">
  Descripción de la Obra(abierto):
  </label>
 <textarea id="cdescripcionobra" class="form-control "  placeholder="Descripción de la Obra(abierto)"></textarea>
</div>
</div>
<div class="form-group col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
  Origen de la Pieza:
  </label>
 <textarea id="corigenpieza" class="form-control " placeholder="Origen de la Pieza"></textarea>
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       IV. RÉGIMEN DE PROPIEDAD:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Nombre del Propietario:
 </label>
 <input id="cnombrepropietarioescultura" type="text" class="form-control"  placeholder="Nombre del Propietario"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Responsables:
  </label>
  <input id="cresponsable" type="text" class="form-control"  placeholder="Responsables"> 
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       V. PROTECCIÓN LEGAL:
      </label>
    </div>
</div>

<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Tipo:
 </label>
 <input id="ctipo" type="text" class="form-control"  placeholder="Tipo"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
   Nro. Ley u Ordenanza Municipal:
  </label>
  <input id="cordenanzamunicipal" type="text" class="form-control"  placeholder="Nro. Ley u Ordenanza Municipal"> 
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       VI. REFERENCIAS DE LA OBRA:
      </label>
    </div>
</div>

<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Descripción del personaje o motivo:
 </label>
  <textarea id="cdescripcionpersonaje" class="form-control " placeholder="Descripción del personaje o motivo"></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
    Referencias históricas de la obra:
  </label>
  <textarea id="creferenciahistorica" class="form-control " placeholder="Referencias históricas de la obra"></textarea>
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       VII. ESTADO DE CONSERVACIÓN:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Estado de conservación:
 </label>
 <select id="cestadoconservacion" class="form-control">
  <option value="-1">Seleccione</option> 
  <option value="BUENO">BUENO</option> 
  <option value="MALO">MALO</option> 
  <option value="RGULAR">REGULAR</option> 
 </select>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
<label class="control-label">
  Observaciones:
 </label>
 <textarea id="cobservaciones" class="form-control "  placeholder="Observaciones"> </textarea>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Fuentes/Bibliograficas:
 </label>
 <textarea id="cfuentesbibliograficos" class="form-control "  placeholder="Fuentes/Bibliograficas"></textarea>
</div>
</div>
<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
  <button class="btn btn-default "  type="reset">
    <i class="fa fa-eraser"></i> Limpiar
  </button>
  <a type = "button" class = "btn btn-primary"  id="guardarEsculturas"  data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>
         
</div> 
</div>
{!! Form::close() !!}
</div>
</div>
</div>
