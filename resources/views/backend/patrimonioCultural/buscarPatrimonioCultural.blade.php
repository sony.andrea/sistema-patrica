@extends('backend.template.app')
@section('main-content')

@include('backend.patrimonioCultural.bienesInmuebles.partial.modalCreate')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalCreate1')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalCreate2')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalUpdate')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalUpdate1')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalUpdate2')

@include('backend.patrimonioCultural.bienesArqueologicos.partial.modalCreate')
@include('backend.patrimonioCultural.bienesArqueologicos.partial.modalUpdateM')
@include('backend.patrimonioCultural.bienesArqueologicos.partial.modalUpdateS')
@include('backend.patrimonioCultural.espaciosAbiertos.partial.modalCreate')
@include('backend.patrimonioCultural.espaciosAbiertos.partial.modalUpdate')
@include('backend.patrimonioCultural.conjuntosPatrimoniales.partial.modalCreate')
@include('backend.patrimonioCultural.conjuntosPatrimoniales.partial.modalUpdate')
@include('backend.patrimonioCultural.esculturasMonumentos.partial.modalCreate')
@include('backend.patrimonioCultural.esculturasMonumentos.partial.modalUpdate')
@include('backend.patrimonioCultural.patrimonioInmaterial.partial.modalCreate')
@include('backend.patrimonioCultural.patrimonioInmaterial.partial.modalUpdateP2')
@include('backend.patrimonioCultural.patrimonioInmaterial.partial.modalUpdateP4')

<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>BUSQUEDA DE FICHAS DE PATRIMONIO</b></div>
    <div class="panel-body">     
      <br>
      <div class="col-md-12">
       <div class="form-group col-md-6">
        <label class="control-label">
          Tipo Patrimonio Cultural:
        </label>
        <select class="form-control" name="tipoPatriCultural" id="tipoPatriCultural" onchange="buscarPatrimonioPorTipo(),selectCampos()">
          <option>Seleccionar...</option>
        </select>
      </div>
      <div class="form-group col-md-3">
        <label class="control-label">
          Campo:
        </label>
        <select class="form-control" name="camposPorPatrimonio" id="camposPorPatrimonio">
          <option value="-1">Seleccionar...</option>
        </select>
      </div>
      <div class="form-group col-md-3">
        <label class="control-label">
          Valor:
        </label>
        <input type="text" id="valorPatrimonio" class="form-control" name="valorPatrimonio">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="container">
        <div class="col-md-6"></div>
        <div class="col-md-6"> 
          <button type ="button" class = "btn btn-primary" onclick="buscarPatrimonioTipoCampo()"> <i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-primary" onclick="limpiarPrincipal()"> <i class="fa fa fa-eraser"></i> Limpiar</button>
          <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle"
            data-toggle="dropdown">
            Nueva Ficha <span class="caret"></span>
          </button>

          <ul class="dropdown-menu" role="menu">
           <li><a href="#" data-target="#myCreateMonumentoEscultura" data-toggle="modal">Esculturas</a></li>
           <li><a href="#" data-target="#myCreatePatriInmaterial" data-toggle="modal">Patrimonio Inmaterial</a></li>
           <li><a href="#" data-target="#myCreateBienesArq" data-toggle="modal">Bienes Arqueologicos</a></li>
           <li><a href="#" data-target="#myCreateEspacioAbierto" data-toggle="modal">Espacios Abiertos</a></li>
           <li><a href="#" data-target="#myCreateConjuntoPatrimonial" data-toggle="modal">Conjuntos Patrimoniales</a></li>
           <li><a href="#" data-target="#myCreateBienesInmueble" data-toggle="modal">Bienes Inmuebles</a></li>

           <li class="divider"></li>
         </ul>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-body">
        <div id="listPatrimonio">
        </div>  
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@push('scripts')
<script>


  var i = 20;
  var _usrid={{$usuarioId}};
  var macrodistrito_descripcion='0';
  var contespaciosabiertos = 1;
  var contconjuntos = 1;
  var contesculturas = 1;
  var correlativog="";

  function buscarPatrimonioPorTipo()
  { 
    var tipoPatrimonio = $( '#tipoPatriCultural' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-933',"parametros": '{"xtipo_id":' +tipoPatrimonio+ '}'}; 
    buscar(formData);  
  }
  function buscarPatrimonioTipoCampo(){
    var tipoPatrimonio = $( '#tipoPatriCultural' ).val();
    var campoPatrimonio = $( '#camposPorPatrimonio' ).val();
    var valorPatrimonio = $( '#valorPatrimonio' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-932',"parametros": '{"xtipo_id":' +tipoPatrimonio+ ', "xcampo_id":' +campoPatrimonio+ ', "xcampo_valor":"' +valorPatrimonio+ '"}'};   
    buscar(formData);  
  }

  function limpiarPrincipal()
  {
    $( '#tipoPatriCultural' ).val("");
    $( '#camposPorPatrimonio' ).val("");
    $( '#valorPatrimonio' ).val("");
  }

  function listarPatrimonioCultural(g_tipo){
    if (g_tipo == 'PTR-ESP') {
      var tipo = 4;
    }
    if (g_tipo == 'PTR_CON') {
      var tipo = 5;
    }
    if (g_tipo == 'PTR-IME') {
      var tipo = 1;
    }
    if (g_tipo == 'PTR-PCI') {
      var tipo = 2;
    }
    if (g_tipo == 'PTR_BMA') {
      var tipo = 3;
    }
    if (g_tipo == 'PTR_CBI') {
      var tipo = 6;
    }
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-933',"parametros": '{"xtipo_id":' +tipo+ '}'}; 
    buscar(formData);  
  }

  function generarProforma(id_cas, g_tipo){
    setTimeout(function(){
      if (g_tipo == 'PTR-ESP') {
        var urlPdf= "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerar',['datos'=>'d1']) }}";
      }
      if (g_tipo == 'PTR_CON') {
        var urlPdf = "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerar',['datos'=>'d1']) }}";
      }

      if ( g_tipo == 'PTR-IME' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerar',['datos'=>'d1']) }}"; 
     }  
     if ( g_tipo == 'PTR_BMA' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerar',['datos'=>'d1']) }}"; 
     }
     if ( g_tipo == 'PTR-PCI' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerar',['datos'=>'d1']) }}"; 
     }
     if ( g_tipo == 'PTR_CBI' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerar',['datos'=>'d1']) }}"; 
     } 
     urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirArchivoFotografico(id_cas, g_tipo){
    setTimeout(function(){

      if (g_tipo == 'PTR-ESP') {
        var urlPdf= "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarArchFoto',['datos'=>'d1']) }}";
      }
      if (g_tipo == 'PTR_CON') {
       var urlPdf= "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerarArchFoto',['datos'=>'d1']) }}";
     }

     if ( g_tipo == 'PTR-IME' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerarArchFoto',['datos'=>'d1']) }}"; 
     } 
     if ( g_tipo == 'PTR_BMA' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerarArchFoto',['datos'=>'d1']) }}"; 
     }  
     if ( g_tipo == 'PTR-PCI' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerarArchFoto',['datos'=>'d1']) }}"; 
     }  
     if ( g_tipo == 'PTR_CBI' ) {
       var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerarArchFoto',['datos'=>'d1']) }}"; 
     }  
     urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirFichaTodo(id_cas, g_tipo){
    setTimeout(function(){

      if (g_tipo == 'PTR-ESP') {
        var urlFicha= "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarFichaTodo',['datos'=>'d1']) }}";
      }
      if ( g_tipo == 'PTR_CBI' ) {
        var urlFicha = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
      }
      if ( g_tipo == 'PTR-IME' ) {
       var urlFicha = "{{ action('reportesPatrimonioCultural\reporteEscultura@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     }
     if ( g_tipo == 'PTR_CON' ) {
       var urlFicha = "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     }
     if ( g_tipo == 'PTR_BMA' ) {
       var urlFicha = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     }
     if ( g_tipo == 'PTR-PCI' ) {
       var urlFicha = "{{ action('reportesPatrimonioCultural\reporteBienInmaterial@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     }
     urlFicha = urlFicha.replace('d1', id_cas);
    ////console.log("urlFicha",urlFicha);
    window.open(urlFicha); 
  }, 500);
  }


  function buscar(formData){

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-patrimonio" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Nro de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código Catastral</th>';
    htmlListaBus += '<th align="left" class="form-natural">Denominación</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Fecha</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Adjuntos</th>' ;
    htmlListaBus += '<th width="15%" align="center" class="form-natural">Impresiones</th></tr>' ;
    htmlListaBus += '</thead>'; 
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          var datos = dataIN;
          //console.log('busquedaaaaa',dataIN);
          var tam = datos.length;
        //////console.log('tam',tam);
        if(dataIN == "[{ }]"){
          tam = 0;
        }; 
        var tipoContribuyenteEnvio = $( "#tipo-contribuyente" ).val();

        for (var i = 0; i < tam; i++){ 
          var cas_id = JSON.parse(dataIN[i].data).cas_id;
          var numero_ficha = JSON.parse(dataIN[i].data).cas_nro_caso;
          //////console.log("numero_ficha",numero_ficha);
          var dataP= JSON.parse(dataIN[i].data);
          var g_tipo=dataP.cas_datos.g_tipo; 
          var g_tipoArq=dataP.cas_datos.TIPO_ARQ; 
          var nroFicha=dataP.cas_nro_caso;
          var denominacion = JSON.parse(dataIN[i].data).cas_datos.PTR_DEN;
          if (g_tipo == 'PTR_CBI') {
           denominacion = JSON.parse(dataIN[i].data).cas_datos.PTR_ACT;                  
         }
         var codigo_ficha = JSON.parse(dataIN[i].data).cas_nombre_caso;
         var codigo_catastral = JSON.parse(dataIN[i].data).cas_datos.PTR_COD_CAT;
         var fecha_registro = JSON.parse(dataIN[i].data).cas_registrado;
         fecha_registro = fecha_registro.split('T');
         fecha_registro = fecha_registro[0];


          //////////console.log("nroFicha",nroFicha); 
          if ( numero_ficha == null) {
           numero_ficha = '';
         }
         if ( denominacion == null) {
           denominacion = '';                  
         }
         if ( codigo_ficha == null) {
           codigo_ficha = '';
         }
         if ( codigo_catastral == null) {
           codigo_catastral = '';
         }
         if ( denominacion == 'PTR_DEN_ACT') {
           denominacion = '';                  
         }
         htmlListaBus += '<tr>';
         htmlListaBus += '<td align="left">'+(i+1)+'</td>';
         htmlListaBus += '<td align="center">';
         htmlListaBus+='<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + cas_id +',\''+ g_tipo +'\');">';
         htmlListaBus+='<i class="glyphicon glyphicon-trash"></i></button>';


         if(g_tipo == 'PTR-ESP'){
           var creador = JSON.parse(dataIN[i].data).cas_datos.PTR_CREA;
           var autorizador = JSON.parse(dataIN[i].data).cas_datos.PTR_ACTUALIZA;
           var codigocatastral = JSON.parse(dataIN[i].data).cas_datos.PTR_COD_CAT;
           var georeferencia = JSON.parse(dataIN[i].data).cas_datos.PTR_GEOREF;
           var tradicional = JSON.parse(dataIN[i].data).cas_datos.PTR_TRAD;
           var actual = JSON.parse(dataIN[i].data).cas_datos.PTR_ACT;
           var departamento = JSON.parse(dataIN[i].data).cas_datos.PTR_DEP;
           var provincia = JSON.parse(dataIN[i].data).cas_datos.PTR_CIPO;
           var municipio = JSON.parse(dataIN[i].data).cas_datos.PTR_MUNI;
           var macrodistrito = JSON.parse(dataIN[i].data).cas_datos.PTR_MACRO;
           var barrio = JSON.parse(dataIN[i].data).cas_datos.PTR_BARR;
           var direccioncalle = JSON.parse(dataIN[i].data).cas_datos.PTR_DIR;
           var esquina1 = JSON.parse(dataIN[i].data).cas_datos.PTR_ESQ1;
           var esquina2 = JSON.parse(dataIN[i].data).cas_datos.PTR_ESQ2;
           var altitud = JSON.parse(dataIN[i].data).cas_datos.PTR_ALT;
           var nombrepropietario = JSON.parse(dataIN[i].data).cas_datos.PTR_NOMPRO;
           var tiporegpropietario = JSON.parse(dataIN[i].data).cas_datos.PTR_REPRO;
           /**/
           var regprousosuelo=JSON.parse(dataIN[i].data).cas_datos.PTR_USO;
           var regproservicio=JSON.parse(dataIN[i].data).cas_datos.PTR_SERV;
           var marlegfiguraproyeccion=JSON.parse(dataIN[i].data).cas_datos.PTR_FIGPRO;
           var regprootrosservicios=JSON.parse(dataIN[i].data).cas_datos.PTR_OTHER;        
           var regprootrosservicios_check = "";
           for (var j = 1; j < regprootrosservicios.length; j++){
            var trad = regprootrosservicios[j].estado;
            regprootrosservicios_check = regprootrosservicios_check + trad + ',';
          }
          regprootrosservicios = regprootrosservicios_check;
          var crefhisreferenciahistorica=JSON.parse(dataIN[i].data).cas_datos.PTR_REFHIST;  
          //var crefhisreferenciahistorica=JSON.parse(dataIN[i].data).cas_datos.PTR_REFHIST;
          //////console.log(crefhisreferenciahistorica,66666666666999999999);
          var cdatorafechaconstruccion=JSON.parse(dataIN[i].data).cas_datos.PTR_FECCONS;       
          var cdatorautorconstructor=JSON.parse(dataIN[i].data).cas_datos.PTR_AUTOR;          
          var cdatorapreexistenciaedificacion=JSON.parse(dataIN[i].data).cas_datos.PTR_PREEX;         
          var cdatorapropietario=JSON.parse(dataIN[i].data).cas_datos.PTR_PROP;          
          var cdatorausosoriginales=JSON.parse(dataIN[i].data).cas_datos.PTR_USOORG;        
          var cdatorahechoshistoricosdist=JSON.parse(dataIN[i].data).cas_datos.PTR_HECHIS;       
          var cdatorafiestasreligiosas=JSON.parse(dataIN[i].data).cas_datos.PTR_FIEREL;       
          var cdatoradatoshistoricos=JSON.parse(dataIN[i].data).cas_datos.PTR_DATHISC;
          var cdatorafuentebibliografia=JSON.parse(dataIN[i].data).cas_datos.PTR_FUENTE;        
          var cdatdocfechadeconstruccion=JSON.parse(dataIN[i].data).cas_datos.PTR_FECCONST;
          var cdatdocautoroconstructor=JSON.parse(dataIN[i].data).cas_datos.PTR_AUTCONST;      
          var cdatdocpreexistenciainvestigacion=JSON.parse(dataIN[i].data).cas_datos.PTR_PREDIN;       
          var cdatdocpropietario=JSON.parse(dataIN[i].data).cas_datos.PTR_PROPIE;        
          var cdatdocusospreexistentes=JSON.parse(dataIN[i].data).cas_datos.PTR_USOPREEX;
          var cdatdochechoshistoricospers=JSON.parse(dataIN[i].data).cas_datos.PTR_HECHISD;     
          var cdatdocfiestasreli=JSON.parse(dataIN[i].data).cas_datos.PTR_FIERELD;       
          var cdatdocdatoshistoricosconj=JSON.parse(dataIN[i].data).cas_datos.PTR_DATHISTC;
          var cdatdocfuentebiblio=JSON.parse(dataIN[i].data).cas_datos.PTR_FUENTBIO;      
          var cinsinscripciones=JSON.parse(dataIN[i].data).cas_datos.PTR_INSCR;     
          var cepoepoca=JSON.parse(dataIN[i].data).cas_datos.PTR_EPOCA;         
          var cepoanoconstruccion=JSON.parse(dataIN[i].data).cas_datos.PTR_ACONS;         
          var cepoanoremodelacion=JSON.parse(dataIN[i].data).cas_datos.PTR_AULTR;         
          var cestestilo=JSON.parse(dataIN[i].data).cas_datos.PTR_ESTILO;        
          var cdesareubicacionespacio=JSON.parse(dataIN[i].data).cas_datos.PTR_UBICESP;       
          var cdesarelineaconstruccion=JSON.parse(dataIN[i].data).cas_datos.PTR_LINCONS;     
          var cdesaretipologia=JSON.parse(dataIN[i].data).cas_datos.PTR_TIPOLO;      
          var cdesarematerial=JSON.parse(dataIN[i].data).cas_datos.PTR_MURMAT; 
          
          var cdesarematerial_check = ""; 
          for (var j = 1; j < cdesarematerial.length; j++){
            var trad = cdesarematerial[j].estado;
            cdesarematerial_check = cdesarematerial_check + trad + ',';
          }
          cdesarematerial=cdesarematerial_check;
          //var cdesareotros=JSON.parse(dataIN[i].data).cas_datos.PTR_MURMAT_OTROS;
          //////console.log(cdesareotros,'PTR_MURMAT_OTROS');
          var cdesareestado=JSON.parse(dataIN[i].data).cas_datos.PTR_MUREST;      
          var cdesareintegridad=JSON.parse(dataIN[i].data).cas_datos.PTR_MURINTE;
          var rejasgrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_REJAS;
          var cdesareestadorejas=JSON.parse(dataIN[i].data).cas_datos.PTR_REJAEST;
          var acabadomurosgrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_ACAMUR;
          var cdesareestadoacabadomur=JSON.parse(dataIN[i].data).cas_datos.PTR_ACAEST;
          var pinturagrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_PINTURA;
          var cdesareestadopint=JSON.parse(dataIN[i].data).cas_datos.PTR_PINTEST;
          var pisosgrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_PISOS;
          var cdesareestadopisos=JSON.parse(dataIN[i].data).cas_datos.PTR_PISOEST;
          var puertadeaccesogrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_PUERTA;
          var cdesareestadopuertasacc=JSON.parse(dataIN[i].data).cas_datos.PTR_PUERTAEST;
          var mobiliariogrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_MOBURB;
          var cdesareestadomoviliariourb=JSON.parse(dataIN[i].data).cas_datos.PTR_MOBEST;
          var escaleragrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_ESCAL;
          var cdesareestadoescaleras=JSON.parse(dataIN[i].data).cas_datos.PTR_ESCEST;
          var caracterivegetagrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_VEGET;
          var cdesareestadocaracteristicasvege=JSON.parse(dataIN[i].data).cas_datos.PTR_VEGEST;
          var detalleartisticogrilla=JSON.parse(dataIN[i].data).cas_datos.PTR_DETALLES;
          var cdesareestadodetalleartistico=JSON.parse(dataIN[i].data).cas_datos.PTR_DETALLEST;
          var cesqesquemas =JSON.parse(dataIN[i].data).cas_datos.PTR_ESQDES;
          var cangvisorientacion =JSON.parse(dataIN[i].data).cas_datos.PTR_ANGOR;
          var cangvisgrados =JSON.parse(dataIN[i].data).cas_datos.PTR_ANGGRA;
          var cangvisvistas=JSON.parse(dataIN[i].data).cas_datos.PTR_VISTAS;
          var cangvisvistas_check = ""; 
          for (var j = 1; j < cangvisvistas.length; j++){
            var trad = cangvisvistas[j].estado;
            cangvisvistas_check = cangvisvistas_check + trad + ',';
          }
          cangvisvistas = cangvisvistas_check;
          var cangvisotros =JSON.parse(dataIN[i].data).cas_datos.PTR_ANGOTR;
          var celevisizquierda_derecha=JSON.parse(dataIN[i].data).cas_datos.PTR_ELEMVISID;
          var celivisalto =JSON.parse(dataIN[i].data).cas_datos.PTR_ELEMVISAL;
          var celivisbajo =JSON.parse(dataIN[i].data).cas_datos.PTR_ELEMVISAB;
          var cdesententorno_inmediato=JSON.parse(dataIN[i].data).cas_datos.PTR_DESENTEI;      
          var cdesespdescrpcion_espacio=JSON.parse(dataIN[i].data).cas_datos.PTR_DESESPDES;
          var cmedarea_total=JSON.parse(dataIN[i].data).cas_datos.PTR_MEDIDAREA;
          var cmedancho =JSON.parse(dataIN[i].data).cas_datos.PTR_MEDIDANC;
          var cmedlargo =JSON.parse(dataIN[i].data).cas_datos.PTR_MEDIDLAR;
          var cmedancho_muro=JSON.parse(dataIN[i].data).cas_datos.PTR_MEDIAMC;
          var cinformacion_adicional=JSON.parse(dataIN[i].data).cas_datos.PTR_INFADICIONAL;
          var cinfadicobservaciones=JSON.parse(dataIN[i].data).cas_datos.PTR_INFOBS;
          var cpatdanos =JSON.parse(dataIN[i].data).cas_datos.PTR_PATODAN;
          var cpatdanos_check = ""; 
          for (var j = 1; j < cpatdanos.length; j++){
            var trad = cpatdanos[j].estado;
            cpatdanos_check = cpatdanos_check + trad + ',';
          }
          cpatdanos = cpatdanos_check;
          var cpatotros=JSON.parse(dataIN[i].data).cas_datos.PTR_PATO_OTROS;
          var cpatobservaciones=JSON.parse(dataIN[i].data).cas_datos.PTR_PATOOBS;
          var cpatcausas =JSON.parse(dataIN[i].data).cas_datos.PTR_PATOCAU;
          var cpatcausas_check = ""; 
          for (var j = 1; j < cpatcausas.length; j++){
            var trad = cpatcausas[j].estado;
            cpatcausas_check = cpatcausas_check + trad + ',';
          }
          cpatcausas = cpatcausas_check;

          var cpatotros=JSON.parse(dataIN[i].data).cas_datos.PTR_PATOCAU_OTROS;
          var cvalhistorico=JSON.parse(dataIN[i].data).cas_datos.PTR_VALORHIS;
          var cvalhistorico_check = ""; 
          for (var j = 1; j < cvalhistorico.length; j++){
            var trad = cvalhistorico[j].estado;
            cvalhistorico_check = cvalhistorico_check + trad + ',';
          }
          cvalhistorico  = cvalhistorico_check;

          var cvalartistico =JSON.parse(dataIN[i].data).cas_datos.PTR_VALORART;
          var cvalartistico_check = ""; 
          for (var j = 1; j < cvalartistico.length; j++){
            var trad = cvalartistico[j].estado;
            cvalartistico_check = cvalartistico_check + trad + ',';
          }
          cvalartistico  = cvalartistico_check;

          var cvalarquitectonico =JSON.parse(dataIN[i].data).cas_datos.PTR_VALORARQ;
          var cvalarquitectonico_check = "";
          for (var j = 1; j < cvalarquitectonico.length; j++){
            var trad = cvalarquitectonico[j].estado;
            cvalarquitectonico_check = cvalarquitectonico_check + trad + ',';
          }
          cvalarquitectonico =  cvalarquitectonico_check;
          var cvaltecnologico =JSON.parse(dataIN[i].data).cas_datos.PTR_VALORTEC;
          var cvaltecnologico_check = "";
          for (var j = 1; j < cvaltecnologico.length; j++){
            var trad = cvaltecnologico[j].estado;
            cvaltecnologico_check = cvaltecnologico_check + trad + ',';
          }
          cvaltecnologico =  cvaltecnologico_check;

          var cvalintegridad =JSON.parse(dataIN[i].data).cas_datos.PTR_VALORINTG; 
          var cvalintegridad_check = "";
          for (var j = 1; j < cvalintegridad.length; j++){
            var trad = cvalintegridad[j].estado;
            cvalintegridad_check = cvalintegridad_check + trad + ',';
          }
          cvalintegridad = cvalintegridad_check;

          var cvalurbano=JSON.parse(dataIN[i].data).cas_datos.PTR_VALORURB;
          var cvalurbano_check = "";
          for (var j = 1; j < cvalurbano.length; j++){
            var trad = cvalurbano[j].estado;
            cvalurbano_check = cvalurbano_check + trad + ',';
          }
          cvalurbano = cvalurbano_check;

          var cvalintangible =JSON.parse(dataIN[i].data).cas_datos.PTR_VALORINT;
          var cvalintangible_check = "";
          for (var j = 1; j < cvalintangible.length; j++){
            var trad = cvalintangible[j].estado;
            cvalintangible_check = cvalintangible_check + trad + ',';
          }
          cvalintangible = cvalintangible_check;

          var cvalsimbolico =JSON.parse(dataIN[i].data).cas_datos.PTR_VALORSIMB; 
          var cvalsimbolico_check = "";
          for (var j = 1; j < cvalsimbolico.length; j++){
            var trad = cvalsimbolico[j].estado;
            cvalsimbolico_check = cvalsimbolico_check + trad + ',';
          }
          cvalsimbolico = cvalsimbolico_check;

          var ccrivalhistorico=JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERHIS;    
          var ccrivalartistico =JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERART;    
          var ccrivalarquitectonico =JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERARQ;
          var ccrivaltecnologicos=JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERTEC;
          var ccrivalintegridad =JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERINT;
          var ccrivalurbano =JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERURB;
          var ccrivalintangible =JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERINTA;
          var ccrivalsimbolico =JSON.parse(dataIN[i].data).cas_datos.PTR_CRITERSIMB;
          var cperencinventariador=JSON.parse(dataIN[i].data).cas_datos.PTR_PERINV;
          var cperrenrevisado=JSON.parse(dataIN[i].data).cas_datos.PTR_PERREV;
          var cperencdigitalizado=JSON.parse(dataIN[i].data).cas_datos.PTR_PERDIG;




          htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateEspacioAbierto" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarEspacioAbierto('+ cas_id +','+ numero_ficha +',\''+ codigo_ficha +'\',\''+ g_tipo +'\',\''+ creador +'\',\''+ autorizador +'\',\''+ codigocatastral +'\',\''+ georeferencia +'\',\''+ tradicional +'\',\''+ actual +'\',\''+ departamento +'\',\''+ provincia +'\',\''+ municipio +'\',\''+ macrodistrito +'\',\''+ barrio +'\',\''+ direccioncalle +'\',\''+ esquina1 +'\',\''+ esquina2 +'\',\''+ altitud +'\',\''+ nombrepropietario +'\',\''+ tiporegpropietario +'\',\''+ regprousosuelo+'\',\''+regproservicio+'\',\''+marlegfiguraproyeccion+'\',\''+regprootrosservicios+'\',\''+crefhisreferenciahistorica+'\',\''+cdatorafechaconstruccion+'\',\''+cdatorautorconstructor+'\',\''+cdatorapreexistenciaedificacion+'\',\''+ cdatorapropietario+'\',\''+cdatorausosoriginales+'\',\''+cdatorahechoshistoricosdist+'\',\''+cdatorafiestasreligiosas+'\',\''+cdatoradatoshistoricos+'\',\''+cdatorafuentebibliografia+'\',\''+ cdatdocfechadeconstruccion+'\',\''+cdatdocautoroconstructor+'\',\''+cdatdocpreexistenciainvestigacion+'\',\''+cdatdocpropietario+'\', \''+cdatdocusospreexistentes+'\',\''+cdatdochechoshistoricospers+'\',\''+ cdatdocfiestasreli+'\',\''+cdatdocdatoshistoricosconj+'\',\''+cdatdocfuentebiblio+'\',\''+cinsinscripciones+'\',\''+cepoepoca+'\', \''+cepoanoconstruccion+'\',\''+cepoanoremodelacion+'\',\''+cestestilo+'\',\''+cdesareubicacionespacio+'\',\''+cdesarelineaconstruccion+'\',\''+cdesaretipologia+'\',\''+cdesarematerial+'\',\''+cdesareotros+'\',\''+cdesareestado+'\',\''+cdesareintegridad+'\',\''+rejasgrilla+'\',\''+cdesareestadorejas+'\',\''+acabadomurosgrilla+'\',\''+cdesareestadoacabadomur+'\',\''+pinturagrilla+'\',\''+cdesareestadopint+'\',\''+pisosgrilla+'\',\''+cdesareestadopisos+'\',\''+puertadeaccesogrilla+'\',\''+cdesareestadopuertasacc+'\',\''+mobiliariogrilla+'\',\''+ cdesareestadomoviliariourb+'\',\''+escaleragrilla+'\',\''+cdesareestadoescaleras+'\',\''+caracterivegetagrilla+'\',\''+cdesareestadocaracteristicasvege+'\',\''+detalleartisticogrilla+'\',\''+cdesareestadodetalleartistico+'\',\''+cesqesquemas+'\',\''+cangvisorientacion+'\',\''+cangvisgrados+'\',\''+cangvisvistas+'\',\''+cangvisotros+'\',\''+celevisizquierda_derecha+'\',\''+celivisalto+'\',\''+celivisbajo+'\',\''+ cdesententorno_inmediato+'\',\''+cdesespdescrpcion_espacio+'\',\''+cmedarea_total+'\',\''+cmedancho+'\',\''+cmedlargo+'\',\''+cmedancho_muro+'\',\''+cinformacion_adicional+'\',\''+cinfadicobservaciones+'\',\''+cpatdanos+'\',\''+cpatotros+'\',\''+cpatobservaciones+'\',\''+cpatcausas+'\',\''+cpatotros+'\',\''+cvalhistorico+'\',\''+cvalartistico+'\',\''+ cvalarquitectonico+'\',\''+cvaltecnologico+'\',\''+cvalintegridad+'\',\''+cvalurbano+'\',\''+cvalintangible+'\',\''+cvalsimbolico+'\',\''+ccrivalhistorico+'\',\''+ccrivalartistico+'\',\''+ccrivalarquitectonico+'\',\''+ccrivaltecnologicos+'\',\''+ccrivalintegridad+'\',\''+ccrivalurbano+'\',\''+ccrivalintangible+'\',\''+ccrivalsimbolico+'\',\''+cperencinventariador+'\',\''+cperrenrevisado+'\',\''+cperencdigitalizado+'\');"><i class="glyphicon glyphicon-pencil"></i></button>';

        }


        if(g_tipo=='PTR_CON'){
          var nominacion = JSON.parse(dataIN[i].data).cas_datos.PTR_NOM;
          var ubicacion = JSON.parse(dataIN[i].data).cas_datos.PTR_UBIC;
          var tipoproteccion = JSON.parse(dataIN[i].data).cas_datos.PTR_TIP_PROT;
          var nroproteccion = JSON.parse(dataIN[i].data).cas_datos.PTR_NRO_PROT;
          var nrototalinmuebletramo = JSON.parse(dataIN[i].data).cas_datos.PTR_NUM_INM;
          var nrototalinmueblepatri = JSON.parse(dataIN[i].data).cas_datos.PTR_NUM_PAT;
          var indicadorurbano = JSON.parse(dataIN[i].data).cas_datos.PTR_IND_URB;
          var indicadorartistico = JSON.parse(dataIN[i].data).cas_datos.PTR_IND_ART;
          var indicadortipologico = JSON.parse(dataIN[i].data).cas_datos.PTR_IND_TIP;
          var indicadortecnologico = JSON.parse(dataIN[i].data).cas_datos.PTR_IND_TEC;
          var indicadorintegridad = JSON.parse(dataIN[i].data).cas_datos.PTR_IND_INT;
          var indicadorhistcult = JSON.parse(dataIN[i].data).cas_datos.PTR_IND_HIST;
          var indicadorsimbolico = JSON.parse(dataIN[i].data).cas_datos.PTR_IND_SIMB;
          var nombrevia = JSON.parse(dataIN[i].data).cas_datos.PTR_NOM_VIA;
          var materiavia = JSON.parse(dataIN[i].data).cas_datos.PTR_MAT_VIA;
          var clasificacion = JSON.parse(dataIN[i].data).cas_datos.PTR_CLASS;
          var entrecalles = JSON.parse(dataIN[i].data).cas_datos.PTR_ENT_CALL;


          htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateConjuntoPatrimonial" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarConjuntoPatrimonial('+ cas_id +','+ numero_ficha +',\''+ codigo_ficha +'\',\''+ g_tipo +'\',\''+ nominacion +'\',\''+ ubicacion +'\',\''+ tipoproteccion +'\',\''+ nroproteccion +'\',\''+ nrototalinmuebletramo +'\',\''+ nrototalinmueblepatri +'\',\''+ indicadorurbano +'\',\''+ indicadorartistico +'\',\''+ indicadortipologico +'\',\''+  indicadortecnologico +'\',\''+ indicadorintegridad +'\',\''+ indicadorhistcult +'\',\''+ indicadorsimbolico +'\',\''+ nombrevia +'\',\''+ materiavia +'\',\''+ clasificacion +'\',\''+ entrecalles +'\');"><i class="glyphicon glyphicon-pencil"></i></button>';

        }

        if(g_tipo=='PTR-IME'){

         var codigo_ficha = JSON.parse(dataIN[i].data).cas_nombre_caso;
         var denominacion = JSON.parse(dataIN[i].data).cas_datos.PTR_NOMBRE;
         var epocaelaboracion = JSON.parse(dataIN[i].data).cas_datos.PTR_EPOCA;
         var pais = JSON.parse(dataIN[i].data).cas_datos.PTR_PAIS;
         var departamento = JSON.parse(dataIN[i].data).cas_datos.PTR_DEP;
         var municipio = JSON.parse(dataIN[i].data).cas_datos.PTR_MUNI;
         var provincia = JSON.parse(dataIN[i].data).cas_datos.PTR_PROV;
         var macrodistrito = JSON.parse(dataIN[i].data).cas_datos.PTR_MACRO;
         var zona = JSON.parse(dataIN[i].data).cas_datos.PTR_ZONA;
         var comunidad = JSON.parse(dataIN[i].data).cas_datos.PTR_COMU;
         var direccion = JSON.parse(dataIN[i].data).cas_datos.PTR_DIR;
         var datosautor = JSON.parse(dataIN[i].data).cas_datos.PTR_AU;
         var especialidad = JSON.parse(dataIN[i].data).cas_datos.PTR_ESP;
         var tecnicamaterial = JSON.parse(dataIN[i].data).cas_datos.PTR_TEC_MAT;
         var piezaesculturica = JSON.parse(dataIN[i].data).cas_datos.PTR_CONJ_ESC;
         var alto = JSON.parse(dataIN[i].data).cas_datos.PTR_ALTO;
         var ancho = JSON.parse(dataIN[i].data).cas_datos.PTR_ANCHO;
         var peso = JSON.parse(dataIN[i].data).cas_datos.PTR_PESO;
         var descripcionobra = JSON.parse(dataIN[i].data).cas_datos.PTR_DESC_OBRA;
         var origenpieza = JSON.parse(dataIN[i].data).cas_datos.PTR_ORG_PIE;
         var nombrepropietario = JSON.parse(dataIN[i].data).cas_datos.PTR_PROP;
         var responsable = JSON.parse(dataIN[i].data).cas_datos.PTR_RESP;
         var tipo = JSON.parse(dataIN[i].data).cas_datos.PTR_TIPO;
         var ordenanzamunicipal = JSON.parse(dataIN[i].data).cas_datos.PTR_LEY_OR;
         var descripcionpersonaje = JSON.parse(dataIN[i].data).cas_datos.PTR_DES_PER;
         var referenciahistorica = JSON.parse(dataIN[i].data).cas_datos.PTR_REF_HIST;
         var estadoconservacion = JSON.parse(dataIN[i].data).cas_datos.PTR_REF_CON;
         var observaciones = JSON.parse(dataIN[i].data).cas_datos.PTR_OBS;
         var fuentesbibliograficos = JSON.parse(dataIN[i].data).cas_datos.PTR_FUENT_BIBLIO;

         var longitud  = piezaesculturica.length;
         var check_tradicional = "";
         for (var j = 1; j < longitud; j++){
          var trad = piezaesculturica[j].estado;
          check_tradicional = check_tradicional + trad + ',';
        }
          ////console.log(check_tradicional, longitud,'check_tradicionalcheck_tradicional');

          htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateMonumentoEscultura" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarMonumentoEscultura('+ cas_id +','+ numero_ficha +',\''+ g_tipo +'\',\''+ codigo_ficha +'\',\''+ denominacion +'\',\''+ epocaelaboracion +'\',\''+ pais +'\',\''+ departamento +'\',\''+ municipio +'\',\''+ provincia +'\',\''+ macrodistrito +'\',\''+ zona +'\',\''+ comunidad +'\',\''+ direccion +'\',\''+ datosautor +'\',\''+ especialidad +'\',\''+ tecnicamaterial +'\',\''+ check_tradicional +'\',\''+ check_tradicional +'\',\''+ alto +'\',\''+ ancho +'\',\''+ peso +'\',\''+ descripcionobra +'\',\''+ origenpieza +'\',\''+ nombrepropietario +'\',\''+ responsable +'\',\''+ tipo +'\',\''+ ordenanzamunicipal +'\',\''+ descripcionpersonaje +'\',\''+ referenciahistorica +'\',\''+ estadoconservacion +'\',\''+ observaciones +'\',\''+ fuentesbibliograficos +'\');"><i class="glyphicon glyphicon-pencil"></i></button>';


        }
        if ( g_tipo == 'PTR-PCI') {


          if(g_tipoArq =='P4'){

            var nivelProteccion = dataP.cas_datos.PTR_NIV_PROTEC;
            var check_nivelProteccion = "";
            for (var j = 1; j < nivelProteccion.length; j++){
              var nivelP = nivelProteccion[j].estado;
              check_nivelProteccion = check_nivelProteccion + nivelP + ',';
            }

            var macroditrito_i = dataP.cas_datos.PTR_MACRO;
            var chek_macroditrito_i = "";
            for (var j = 1; j < macroditrito_i.length; j++){
              var nivelM = macroditrito_i[j].estado;
              chek_macroditrito_i = chek_macroditrito_i + nivelM + ',';
            }

            //console.log("check_nivelProteccion",check_nivelProteccion);
            htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdatePatInmaterialP4" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick="editarAmbito4('+cas_id+','+nroFicha+',\''+dataP.cas_datos.PTR_CREADOR+'\',\''+dataP.cas_datos.PTR_EXPR+'\',\''+dataP.cas_datos.PTR_DEN+'\',\''+dataP.cas_datos.PTR_ETIM+'\',\''+dataP.cas_datos.PTR_PERIODO+'\',\''+chek_macroditrito_i+'\',\''+dataP.cas_datos.PTR_ZONA+'\',\''+dataP.cas_datos.PTR_DIR+'\',\''+dataP.cas_datos.PTR_CARACT+'\',\''+dataP.cas_datos.PTR_CONTEX+'\',\''+dataP.cas_datos.PTR_NARR_DEN+'\',\''+dataP.cas_datos.PTR_DAT_HIS+'\',\''+dataP.cas_datos.PTR_SIG_INTER+'\',\''+check_nivelProteccion+'\',\''+dataP.cas_datos.PTR_PROT_LEGAL+'\',\''+dataP.cas_datos.PTR_INSTR_PRLEGAL+'\',\''+dataP.cas_datos.PTR_NUM_LEGAL+'\',\''+dataP.cas_datos.PTR_DAT_COMP+'\',\''+dataP.cas_datos.PTR_OTROS+'\',\''+dataP.cas_datos.PTR_PALABRA_CLAVE+'\');"><i class="glyphicon glyphicon-pencil"></i></button>';

          }
          
          if(g_tipoArq =='P2'){
           htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdatePatInmaterialP2" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick="editarAmbito2('+cas_id+','+nroFicha+',\''+dataP.cas_datos.PTR_DEN+'\',\''+dataP.cas_datos.PTR_TB_CUL+'\',\''+dataP.cas_datos.PTR_PROT_LEGAL+'\',\''+dataP.cas_datos.PTR_NIV_PROTEC+'\',\''+dataP.cas_datos.PTR_NUM_PRLEGAL+'\',\''+dataP.cas_datos.PTR_FEC_PROG+'\',\''+dataP.cas_datos.PTR_PALABRA_CLAVE+'\');"><i class="glyphicon glyphicon-pencil"></i></button>';

         }

       }
        if ( g_tipo == 'PTR_BMA') {
        if ( g_tipoArq == '1') {
         htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateBienesArqM" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarBienesArqMuebles('+cas_id+','+nroFicha+',\''+dataP.cas_datos.PTR_PROVIENE_C+'\',\''+dataP.cas_datos.PTR_RELACION+'\',\''+dataP.cas_datos.PTR_UBIC_ACT+'\',\''+dataP.cas_datos.PTR_DESC_BIEN+'\',\''+dataP.cas_datos.PTR_MATERIAL+'\',\''+dataP.cas_datos.PTR_TECN+'\',\''+dataP.cas_datos.PTR_MANF+'\',\''+dataP.cas_datos.PTR_DECOR+'\',\''+dataP.cas_datos.PTR_ATRIB+'\',\''+dataP.cas_datos.PTR_EST_CONS+'\',\''+dataP.cas_datos.PTR_DESC_ESTADO+'\',\''+dataP.cas_datos.PTR_OBS+'\',\''+dataP.cas_datos.PTR_CRONO+'\',\''+dataP.cas_datos.PTR_FUNC+'\',\''+dataP.cas_datos.PTR_CREA+'\');"><i class="glyphicon glyphicon-pencil"></i></button>'; 
       }

       if ( g_tipoArq == '2') {

        var PTR_CRONO =JSON.stringify(dataP.cas_datos.PTR_CRONO);
        var PTR_CRONO = reemComillasPorBarras(PTR_CRONO);
        var PTR_IA_FUNC = JSON.stringify(dataP.cas_datos.PTR_IA_FUNC);
        var PTR_IA_FUNC = reemComillasPorBarras(PTR_IA_FUNC);
        var PTR_IA_TIP_INTER = JSON.stringify(dataP.cas_datos.PTR_IA_TIP_INTER);
        var PTR_IA_TIP_INTER = reemComillasPorBarras(PTR_IA_TIP_INTER);
        var PTR_IA_ELEM_INM = JSON.stringify(dataP.cas_datos.PTR_IA_ELEM_INM);
        var PTR_IA_ELEM_INM = reemComillasPorBarras(PTR_IA_ELEM_INM);
        var PTR_IA_ELEM_MUEB = JSON.stringify(dataP.cas_datos.PTR_IA_ELEM_MUEB);
        var PTR_IA_ELEM_MUEB = reemComillasPorBarras(PTR_IA_ELEM_MUEB);

        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateBienesArqS" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarBienesArqSitios('+cas_id+','+nroFicha+',\''+dataP.cas_datos.PTR_DEN+'\',\''+dataP.cas_datos.PTR_MUNI+'\',\''+dataP.cas_datos.PTR_MACRO+'\',\''+dataP.cas_datos.PTR_LUG+'\',\''+dataP.cas_datos.PTR_IA_ELEM_DESC+'\',\''+dataP.cas_datos.PTR_IA_OBS_EST+'\',\''+dataP.cas_datos.PTR_IA_EST_CONS+'\',\''+dataP.cas_datos.PTR_IA_POLIG+'\',\''+dataP.cas_datos.PTR_IA_DESC_EST_CONS+'\',\''+dataP.cas_datos.PTR_IA_OBS_EST+'\',\''+dataP.cas_datos.PTR_IA_TAFON+'\',\''+dataP.cas_datos.PTR_IA_DESC_RUR+'\',\''+dataP.cas_datos.PTR_IA_DESC_URB+'\',\''+dataP.cas_datos.PTR_IA_TAFON_DESC+'\',\''+dataP.cas_datos.PTR_IA_YACIM+'\',\''+dataP.cas_datos.PTR_IA_ELEM_MUEBLE+'\',\''+dataP.cas_datos.PTR_IA_DESC+'\',\''+dataP.cas_datos.PTR_IA_ELEM_MUEB_DESC+'\',\''+dataP.cas_datos.PTR_CREA+'\',\''+dataP.cas_datos.PTR_IA_DESC_ELEM_INM+'\',\''+dataP.cas_datos.PTR_CRONO_DESC+'\',\''+PTR_CRONO+'\',\''+PTR_IA_FUNC+'\',\''+dataP.cas_datos.PTR_IA_TAM_YACIM+'\',\''+PTR_IA_TIP_INTER+'\',\''+dataP.cas_datos.PTR_IA_NUM_POZ+'\',\''+dataP.cas_datos.PTR_IA_AR_INTER+'\',\''+dataP.cas_datos.PTR_IA_PROF_MIN+'\',\''+dataP.cas_datos.PTR_IA_PROF_MAX+'\',\''+PTR_IA_ELEM_INM+'\',\''+PTR_IA_ELEM_MUEB+'\',\''+dataP.cas_datos.PTR_IA_ELEM_INM_DESC+'\',\''+dataP.cas_datos.PTR_IA_ELEM_MUEB_DESC+'\',\''+dataP.cas_datos.PTR_IA_OBS+'\');"><i class="glyphicon glyphicon-pencil"></i></button>';
      }
      
    }
    if ( g_tipo == 'PTR_CBI') {

      var tradicional = dataP.cas_datos.PTR_USO_TRAD;
      var actual = dataP.cas_datos.PTR_USO_ACT;
      var tendencia = dataP.cas_datos.PTR_USO_TEN;


      var check_tradicional = "";
      var check_actual = "";
      var check_tendencia = "";
      for (var j = 1; j < tradicional.length; j++){
        var trad = tradicional[j].estado;
        var act = actual[j].estado;
        var ten = tendencia[j].estado;
        check_tradicional = check_tradicional + trad + ',';
        check_actual = check_actual + act + ',';
        check_tendencia = check_tendencia + ten + ',';
      }
        //-------------------------------------------------

         var epoca_estilo = dataP.cas_datos.PTR_EPO_EST1;
         var epocaestilo_check = epoca_estilo[1].valor;
         

         var sistema_constructivo = dataP.cas_datos.PTR_SIS_CONST1;
         var sistema_constructivo_check = sistema_constructivo[1].valor;          
  
          var acapites = dataP.cas_datos.PTR_ACAP1;
          //console.log('ACAPITES',acapites);
          var acapi_check = "";
          for (var g = 1; g < acapites.length; g++){
          var acapi = acapites[g].estado;
          acapi_check = acapi_check + acapi + ',';        
          }
           //console.log('acapi_check',acapi_check);
              
          var ihistorico = dataP.cas_datos.PTR_VAL_HIST1;
          //console.log('INDICADOR HISTORICO',ihistorico);
          var ihistorico_check = "";
          for (var g = 1; g < ihistorico.length; g++){
            var ihist = ihistorico[g].estado;
            ihistorico_check = ihistorico_check + ihist + ',';
          }
          
          var iartistico= dataP.cas_datos.PTR_VAL_ART1;  
          //console.log('INDICADOR ATISTICO',iartistico);
          var iartistico_check = "";
          for (var g = 1; g < iartistico.length; g++){
            var iart = iartistico[g].estado;
            iartistico_check = iartistico_check + iart + ',';
          }
          
          var iarquitectonico= dataP.cas_datos.PTR_VAL_ARQ1;
          //console.log('INDICADOR ARQUITECTONICO',iarquitectonico);
          var iarquitectonico_check = "";
          for (var g = 1; g < iarquitectonico.length; g++){
            var iarqui = iarquitectonico[g].estado;
            iarquitectonico_check = iarquitectonico_check + iarqui + ',';
          }
          
          var itecnologico= dataP.cas_datos.PTR_VAL_TEC1; 
          //console.log('INDICADOR TECNOLÓGICO',itecnologico);
          var itecnologico_check = "";
          for (var g = 1; g < itecnologico.length; g++){
            var itec = itecnologico[g].estado;
            itecnologico_check = itecnologico_check + itec + ',';
          }

          var iintegridad= dataP.cas_datos.PTR_VAL_INTE1; 
          //console.log('INDICADOR INTEGRIDAD',iintegridad);
          var iintegridad_check = "";
          for (var g = 1; g < iintegridad.length; g++){
            var itec = iintegridad[g].estado;
            iintegridad_check = iintegridad_check + itec + ',';
          }

          var iurbano= dataP.cas_datos.PTR_VAL_URB1; 
          //console.log('INDICADOR URBANO',iurbano);
          var iurbano_check = "";
          for (var g = 1; g < iurbano.length; g++){
            var iurb = iurbano[g].estado;
            iurbano_check = iurbano_check + iurb + ',';
          }

          var iinmaterial= dataP.cas_datos.PTR_VALORES_1; 
          //console.log('INDICADOR INMATERIAL',iinmaterial);
          var iinmaterial_check = "";
          for (var g = 1; g < iinmaterial.length; g++){
            var iinm = iinmaterial[g].estado;
            iinmaterial_check = iinmaterial_check + iinm + ',';
          }

          var PTR_FIG_PROT= dataP.cas_datos.PTR_FIG_PROT;
          var PTR_FIG_PROT= JSON.stringify(PTR_FIG_PROT);
          ////console.log("antesdeReem",PTR_FIG_PROT);
          var PTR_FIG_PROT = reemComillasPorBarras(PTR_FIG_PROT);
           ////console.log("BarrasPTR_FIG_PROT",PTR_FIG_PROT);
          
          var PTR_REF_HIST= dataP.cas_datos.PTR_REF_HIST;
          var PTR_REF_HIST= JSON.stringify(PTR_REF_HIST);
          ////console.log("antesdeReemPTR_REF_HIST",PTR_REF_HIST);
          var PTR_REF_HIST = reemComillasPorBarras(PTR_REF_HIST);
          ////console.log("BarrasPTR_REF_HIST",PTR_REF_HIST);

       htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateBienesInmueble" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick="editarBienesInmuebles('+cas_id+','+nroFicha+',\''+dataP.cas_datos.PTR_COD_CAT+'\',\''+dataP.cas_datos.PTR_COD_CAT_HIS1+'\',\''+dataP.cas_datos.PTR_OR+'\',\''+dataP.cas_datos.PTR_TRAD+'\',\''+dataP.cas_datos.PTR_ACT+'\',\''+dataP.cas_datos.PTR_MACRO+'\',\''+dataP.cas_datos.PTR_TIP_VIAS+'\',\''+dataP.cas_datos.PTR_CALL_ESQ+'\',\''+dataP.cas_datos.PTR_DIRE+'\',\''+dataP.cas_datos.PTR_ZONA+'\',\''+dataP.cas_datos.PTR_NUM1+'\',\''+dataP.cas_datos.PTR_REG_PROPI+'\',\''+dataP.cas_datos.PTR_REG_TIPO_PROPI+'\',\''+dataP.cas_datos.PTR_ARE_PRED+'\',\''+dataP.cas_datos.PTR_ANCH_FREN+'\',\''+dataP.cas_datos.PTR_FOND_PRED+'\',\''+dataP.cas_datos.PTR_ANCH_MUR+'\',\''+dataP.cas_datos.PTR_ALT_PROM+'\',\''+dataP.cas_datos.PTR_ALT_FACH+'\',\''+dataP.cas_datos.PTR_ALT_EDIF+'\',\''+check_tradicional+'\',\''+check_actual+'\',\''+check_tendencia+'\',\''+dataP.cas_datos.PTR_DES_PRED+'\',\''+dataP.cas_datos.PTR_UBIC_MAN+'\',\''+dataP.cas_datos.PTR_LIN_CONS+'\',\''+dataP.cas_datos.PTR_TRAZ_MAN+'\',\''+dataP.cas_datos.PTR_TIP_ARQ+'\',\''+dataP.cas_datos.PTR_AER_EDIF+'\',\''+dataP.cas_datos.PTR_MAT_VIA+'\',\''+dataP.cas_datos.PTR_TIP_VIA+'\',\''+dataP.cas_datos.PTR_BLOQ_INV+'\',\''+dataP.cas_datos.PTR_HOG_REAL+'\',\''+dataP.cas_datos.PTR_HOG_INV+'\',\''+dataP.cas_datos.PTR_HOG_REAL+'\',\''+dataP.cas_datos.PTR_EST_GEN+'\',\''+dataP.cas_datos.PTR_RAN_EPO+'\',\''+dataP.cas_datos.PTR_EST_CON+'\',\''+dataP.cas_datos.PTR_FECH_CONS+'\',\''+dataP.cas_datos.PTR_AUT_CON+'\',\''+dataP.cas_datos.PTR_PREEX_EDIF+'\',\''+dataP.cas_datos.PTR_PROP_ANT+'\',\''+dataP.cas_datos.PTR_USO_ORG+'\',\''+dataP.cas_datos.PTR_HEC_HIST+'\',\''+dataP.cas_datos.PTR_FIE_REL+'\',\''+dataP.cas_datos.PTR_DAT_HIST_CONJ+'\',\''+dataP.cas_datos.PTR_FUENT_DOC+'\',\''+dataP.cas_datos.PTR_FEC_CONST_DOC+'\',\''+dataP.cas_datos.PTR_AUT_CONS+'\',\''+dataP.cas_datos.PTR_PREEX_INTER+'\',\''+dataP.cas_datos.PTR_PROP_ANT_DOC+'\',\''+dataP.cas_datos.PTR_USOS_ORG_DOC+'\',\''+dataP.cas_datos.PTR_HEC_HIS_DOC+'\',\''+dataP.cas_datos.PTR_FIE_REL_DOC+'\',\''+dataP.cas_datos.PTR_DAT_HIS_DOC+'\',\''+dataP.cas_datos.PTR_FUENTE_DOC+'\',\''+dataP.cas_datos.PTR_INSCRIP+'\',\''+dataP.cas_datos.PTR_EXIST+'\',\''+dataP.cas_datos.PTR_EXIST_AR+'\',\''+dataP.cas_datos.PTR_EXIST_ELEM+'\',\''+dataP.cas_datos.PTR_EXIST_RITO+'\',\''+dataP.cas_datos.PTR_CONJUNTO+'\',\''+dataP.cas_datos.PTR_EST_VERF+'\',\''+dataP.cas_datos.PTR_VAL_CAT+'\',\''+dataP.cas_datos.PTR_ID_BLOQ1+'\',\''+dataP.cas_datos.PTR_DES_BLOQ1+'\',\''+dataP.cas_datos.PTR_RANG_EPO1+'\',\''+dataP.cas_datos.PTR_EST_BLOQ1+'\',\''+epocaestilo_check+'\',\''+sistema_constructivo_check+'\',\''+dataP.cas_datos.PTR_SIS_CONSTRUC1+'\',\''+dataP.cas_datos.PTR_NRO_VIV1+'\',\''+dataP.cas_datos.PTR_NRO_HOG1+'\',\''+acapi_check+'\',\''+ihistorico_check+'\',\''+iartistico_check+'\',\''+iarquitectonico_check+'\',\''+itecnologico_check+'\',\''+iintegridad_check+'\',\''+iurbano_check+'\',\''+iinmaterial_check+'\',\''+dataP.cas_datos.PTR_VAL_HIS_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_ART_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_ARQ_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_TEC_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_INTE_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_URB_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_INMAT_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_SIMB_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_INT_DESC1+'\',\''+dataP.cas_datos.PTR_VAL_EXT_DESC1+'\',\''+dataP.cas_datos.PTR_CATEGORIA1+'\',\''+dataP.cas_datos.PTR_PUNTO+'\',\''+dataP.cas_datos.PTR_ID_BLOQ1+'\',\''+PTR_FIG_PROT+'\',\''+PTR_REF_HIST+'\');"><i class="glyphicon glyphicon-pencil"></i></button>';


      }


      htmlListaBus += '</td>';
      htmlListaBus += '<td align="left" class="form-natural">'+numero_ficha+'</td>'; 
      htmlListaBus += '<td align="left" class="form-natural">'+codigo_ficha+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+codigo_catastral+'</td>'; 
      htmlListaBus += '<td align="left" class="form-natural">'+denominacion+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+fecha_registro+'</td>';
      var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
      url = url.replace('casosId1', cas_id);

      htmlListaBus+='<td style="text-align: center;"><a href="'+url+'" class="btncirculo btn-xs btn-default" fa fa-plus-square pull-right"  > <i class="fa fa-folder"></i></a>';
      htmlListaBus+='</td>';
      htmlListaBus+='<td align="center">';
      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Ficha" type="button" onClick= "generarProforma(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-print"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Archivo Fotografico" type="button" onClick= "imprimirArchivoFotografico(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-camera"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Todo" type="button" onClick= "imprimirFichaTodo(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-list"></i></button>';

      htmlListaBus+='</td>';

      htmlListaBus += '</tr>';
    } 
    htmlListaBus += '</table></div></div>';
    $( '#listPatrimonio' ).html(htmlListaBus);
    $( '#lts-patrimonio' ).DataTable(
    {
      "language": {"url": "lenguaje"},
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    });

  },
  error: function(result) {
    swal( "Error..!", "Error....", "error" );
  }
});
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});
};
function getBool (val){
  return !!JSON.parse(String(val).toLowerCase());
}

function listarTipoPatrimonioCombo(){ 
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-942',"parametros": '{}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        //console.log("listadoCommbooooooooooooooo",dataIN);
        for(var i = 0; i < dataIN.length; i++){
         var id_tipo = dataIN[i].tipo_id;
         var tipoPatrimonio = JSON.parse(dataIN[i].tipo_data).tipoPatrimonio;   
         document.getElementById("tipoPatriCultural").innerHTML += '<option value=' +id_tipo+ '>' +tipoPatrimonio+ '</option>';         
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
};

function selectCampos()
{  
  var tipoCampoPatrimonio = $('#tipoPatriCultural').val();
  document.getElementById("camposPorPatrimonio").length=1;
  //////////console.log(tipoCampoPatrimonio,333333333333);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-931',"parametros": '{"ytipoid":'+tipoCampoPatrimonio+'}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        //////////console.log("listadoCampos",dataIN);
        for(var i = 0; i < dataIN.length; i++){
         var campo_id = dataIN[i].xcampo_id;
         var campo_tipo = JSON.parse(dataIN[i].xcampo_data).campoTipo;   
         document.getElementById("camposPorPatrimonio").innerHTML += '<option value=' +campo_id+ '>' +campo_tipo+ '</option>';            
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
}

function obtenercorrelativo(tipoPatrimonio)
{
 $.ajax({
  type        : 'GET',
  url         : urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": 'SERVICIO_PATRIMONIO-936',"parametros":'{"xtipoPat":"'+tipoPatrimonio+'"}'};
    //////////console.log("formdataaa",formData);
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
      var data=dataIN;
      correlativog=dataIN[0].sp_correlativo_patrimonio4;       
    //////////console.log("correl99999999999999999ativog",correlativog);

  },     
  error: function (xhr, status, error) { }
});
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

}

/*-----------------BIENES ARQUEOLOGICOS-----------------------*/

$( "#registrarMuebleArqueologico" ).click(function() 
{
  obtenercorrelativo('PTR_BMA');
  setTimeout(function(){

  var ccodPatrimonio_m = 'PTR_BMA'+correlativog+'/2018';
  var ccreador_arq = $("#ccreadorM_arq").val();
  ////////console.log("ccreador_arq",ccreador_arq);
  var cprovenienciaMueble_m = document.getElementById('cprovenienciaMueble_m').value;
  var crelacionPieza_m = $("#crelacionPieza_m").val();
  var cubicacionActual_m = $("#cubicacionActual_m").val();
  var cDescripcionMueble_m = $("#cDescripcionMueble_m").val();
  var cmaterial_m = $("#cmaterial_m").val();
  var ctecnica_m = $("#ctecnica_m").val();
  var cmanufactura_m = $("#cmanufactura_m").val();
  var cdecoracion_m = $("#cdecoracion_m").val();
  var catributos_m = $("#catributos_m").val();
  var cestadoConserva_m = document.getElementById('cestadoConserva_m').value;
  var cdescripEstado_m = $("#cdescripEstado_m").val();
  var cobservacion_m = $("#cobservacion_m").val();
  var ccronologia_m = $("#ccronologia_m").val();
  var cfuncion_m = $("#cfuncion_m").val();
  var g_tipo = 'PTR_BMA';

  //////console.log("cDescripcionMueble_m",cDescripcionMueble_m);

  var cDescripcionMueble_m = caracteresEspecialesRegistrar(cDescripcionMueble_m);
  var cmaterial_m = caracteresEspecialesRegistrar(cmaterial_m);
  var ctecnica_m = caracteresEspecialesRegistrar(ctecnica_m);
  var cmanufactura_m = caracteresEspecialesRegistrar(cmanufactura_m);
  var cdecoracion_m = caracteresEspecialesRegistrar(cdecoracion_m);
  var catributos_m = caracteresEspecialesRegistrar(catributos_m);
  var cdescripEstado_m = caracteresEspecialesRegistrar(cdescripEstado_m);
  var cobservacion_m = caracteresEspecialesRegistrar(cobservacion_m);
  var cfuncion_m = caracteresEspecialesRegistrar(cfuncion_m);
 //////console.log("cDescripcionMueble_m",cDescripcionMueble_m);

//console.log("ccreador_arq",ccreador_arq);
 var cas_data_m ='{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"1","PTR_CREA":"'+ccreador_arq+'","PTR_PROVIENE_C":"'+cprovenienciaMueble_m+'","PTR_RELACION":"'+crelacionPieza_m+'","PTR_UBIC_ACT":"'+cubicacionActual_m+'","PTR_DESC_BIEN":"'+cDescripcionMueble_m+'","PTR_MATERIAL":"'+cmaterial_m+'","PTR_TECN":"'+ctecnica_m+'","PTR_MANF":"'+cmanufactura_m+'","PTR_DECOR":"'+cdecoracion_m+'","PTR_ATRIB":"'+catributos_m+'","PTR_EST_CONS":"'+cestadoConserva_m+'","PTR_DESC_ESTADO":"'+cdescripEstado_m+'","PTR_OBS":"'+cobservacion_m+'","PTR_CRONO":"'+ccronologia_m+'","PTR_FUNC":"'+cfuncion_m+'"}';
 var jdata_m = JSON.stringify(cas_data_m);
  //////console.log("jdataaaa",jdata_m);

  var xcas_nro_caso = correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = '';
  var xcas_tipo_hr = '';
  var xcas_id_padre = 1;
  var xcampo_f = ''; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_m+',"xcas_nombre_caso":"'+ccodPatrimonio_m+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
  //////console.log("formData",formData);

  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

    //var ccodPatrimonio = $("#ccodPatrimonio").val();
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },                  
     success: function(data){ 

      //////console.log("dataaaa",data);
      listarPatrimonioCultural(g_tipo);
      swal( "Exíto..!", "Se registro correctamente los bienes arqueologicos...!", "success" );
      //$( "#formBienesArqM_create").data('bootstrapValidator').resetForm();
      //$( "#myCreateBienesArqMueble" ).modal('toggle');
      $( "#cprovenienciaMueble_m" ).val(""); 
      $( "#crelacionPieza_m" ).val(""); 
      $( "#cubicacionActual_m" ).val(""); 
      $( "#cDescripcionMueble_m" ).val(""); 
      $( "#cmaterial_m" ).val("");
      $( "#ctecnica_m" ).val("");
      $( "#cmanufactura_m" ).val("");
      $( "#cdecoracion_m" ).val("");
      $( "#catributos_m" ).val("");
      $( "#cestadoConserva_m").val("");
      $( "#cdescripEstado_m" ).val("");
      $( "#cobservacion_m" ).val("");
      $( "#ccronologia_m" ).val("");
      $( "#cfuncion_m" ).val("");

      var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
        url = url.replace('casosId1', idCasos);
        //console.log("url",url);
        location.href = url;
      }, 1000);
    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500); 
});

$( "#registrarSitioArqueologico" ).click(function() 
{
  obtenercorrelativo('PTR_BMA');
  setTimeout(function(){
    var ccreador_arq = $("#ccreador_arq").val();   
    var cdenominacion = $( "#cdenominacion" ).val();
    var ccodPatrimonio = 'PTR_BMA'+correlativog+'/2018';
    var cubiMunicipio = $( "#cubiMunicipio" ).val();
    var cubiMacrodistrito = $("#cubiMacrodistrito").val();
    var cubiLocal = $("#cubiLocal").val();
    var cdescripInmuebles = $("#cdescripInmuebles").val();
    var cestadoConserva = document.getElementById('cestadoConserva').value;
  //////////console.log("estdoconserva",cestadoConserva);
  var cubicacionPoligono = $("#cubicacionPoligono").val();
  var cdescripEstad_s = $("#cdescripEstado_s").val();
  ////////console.log(cdescripEstad_s);
  var cobservacion = $("#cobservacion").val();
  var ctafonomia = $("#ctafonomia").val();
  var cdescripContextoUrbana = $("#cdescripContextoUrbana").val();
  var cdescripContextoRural = $("#cdescripContextoRural").val();
  var cdescripTafonomia = $("#cdescripTafonomia").val();
  var cdescripYacimiento = $("#cdescripYacimiento").val();
  var celementos = $("#celementos").val();
  var cdescrip = $("#cdescrip").val();
  var cdescripcionMueble = $("#cdescripcionMueble").val();
  var g_tipo = 'PTR_BMA';

  var cdescripInmuebles = caracteresEspecialesRegistrar(cdescripInmuebles);
  var cubicacionPoligono = caracteresEspecialesRegistrar(cubicacionPoligono);
  var cdescripEstad_s = caracteresEspecialesRegistrar(cdescripEstad_s);
  var cobservacion = caracteresEspecialesRegistrar(cobservacion);
  var cdescripContextoUrbana = caracteresEspecialesRegistrar(cdescripContextoUrbana);
  var cdescripContextoRural = caracteresEspecialesRegistrar(cdescripContextoRural);
  var cdescripTafonomia = caracteresEspecialesRegistrar(cdescripTafonomia);
  var cdescrip = caracteresEspecialesRegistrar(cdescrip);
  var cdescripcionMueble = caracteresEspecialesRegistrar(cdescripcionMueble);

  ////////////////////////formulario 2/////////////////////

  var PTR_IA_DESC_ELEM_INM = $("#cdescripcionElem_ar").val();
  var PTR_CRONO_DESC = $("#ccronologiaDesc_ar").val();
  var PTR_IA_TAM_YACIM = $("#ctamañoSitio_ar").val();
  var PTR_IA_NUM_POZ = $("#cnumeroPozo_ar").val();
  var PTR_IA_AR_INTER = $("#careaInter_ar").val();
  var PTR_IA_PROF_MIN = $("#cprofundidad_ar").val();
  var PTR_IA_PROF_MAX = $("#cprofundidadMax_ar").val();
  var PTR_IA_ELEM_INM_DESC = $("#cdescripcionMue_ar").val();
  var PTR_IA_ELEM_MUEB_DESC = $("#cdescripcionMue_ar").val();
  var PTR_IA_OBS = $("#cobservaciones_ar").val();

  var PTR_CRONO_DESC = caracteresEspecialesRegistrar(PTR_CRONO_DESC);
  var PTR_IA_TAM_YACIM = caracteresEspecialesRegistrar(PTR_IA_TAM_YACIM);
  var PTR_IA_ELEM_INM_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_INM_DESC);
  var PTR_IA_ELEM_MUEB_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_MUEB_DESC);
  var PTR_IA_OBS = caracteresEspecialesRegistrar(PTR_IA_OBS);

  var ccronologia_ar1 = document.getElementById("ccronologia_ar1").checked;
  var ccronologia_ar2 = document.getElementById("ccronologia_ar2").checked;
  var ccronologia_ar3 = document.getElementById("ccronologia_ar3").checked;
  var ccronologia_ar4 = document.getElementById("ccronologia_ar4").checked;
  var ccronologia_ar5 = document.getElementById("ccronologia_ar5").checked;
  var ccronologia_ar6 = document.getElementById("ccronologia_ar6").checked;
  var ccronologia_ar7 = document.getElementById("ccronologia_ar7").checked;
  var ccronologia_ar8 = document.getElementById("ccronologia_ar8").checked;
  var ccronologia_ar9 = document.getElementById("ccronologia_ar9").checked;

   var PTR_CRONO = '[{"tipo":"CHKM"},{"resid":215,"estado":'+ccronologia_ar1+',"resvalor":"Arcaico tardío"},{"resid":214,"estado":'+ccronologia_ar2+',"resvalor":"Arcaico temprano-medio"},{"resid":221,"estado":'+ccronologia_ar3+',"resvalor":"Colonial"},{"resid":217,"estado":'+ccronologia_ar4+',"resvalor":"Formativo tardío"},{"resid":216,"estado":'+ccronologia_ar5+',"resvalor":"Formativo temprano-medio"},{"resid":220,"estado":'+ccronologia_ar6+',"resvalor":"Inca"},{"resid":219,"estado":'+ccronologia_ar7+',"resvalor":"Intermedio tardío"},{"resid":222,"estado":'+ccronologia_ar8+',"resvalor":"Republicano"},{"resid":218,"estado":'+ccronologia_ar9+',"resvalor":"Tiwanaku"}]';

   //console.log("PTR_CRONOOOOO",PTR_CRONO);

  var cfuncionalidad_ar1 = document.getElementById("cfuncionalidad_ar1").checked;
  var cfuncionalidad_ar2 = document.getElementById("cfuncionalidad_ar2").checked;
  var cfuncionalidad_ar3 = document.getElementById("cfuncionalidad_ar3").checked;
  var cfuncionalidad_ar4 = document.getElementById("cfuncionalidad_ar4").checked;
  var cfuncionalidad_ar5 = document.getElementById("cfuncionalidad_ar5").checked;
  var cfuncionalidad_ar6 = document.getElementById("cfuncionalidad_ar6").checked;
  var cfuncionalidad_ar7 = document.getElementById("cfuncionalidad_ar7").checked;
  var cfuncionalidad_ar8 = document.getElementById("cfuncionalidad_ar8").checked;

  var PTR_IA_FUNC = '[{"tipo":"CHKM"},{"resid":230,"estado":'+cfuncionalidad_ar1+',"resvalor":"Administrativa"},{"resid":229,"estado":'+cfuncionalidad_ar2+',"resvalor":"Caminera"},{"resid":228,"estado":'+cfuncionalidad_ar3+',"resvalor":"Ceremonial"},{"resid":226,"estado":'+cfuncionalidad_ar4+',"resvalor":"Defensiva"},{"resid":223,"estado":'+cfuncionalidad_ar5+',"resvalor":"Doméstica"},{"resid":227,"estado":'+cfuncionalidad_ar6+',"resvalor":"Funeraria"},{"resid":225,"estado":'+cfuncionalidad_ar7+',"resvalor":"Manufactura"},{"resid":224,"estado":'+cfuncionalidad_ar8+',"resvalor":"Productiva"}]';

  var cintervencion_ar1 = document.getElementById("cintervencion_ar1").checked;
  var cintervencion_ar2 = document.getElementById("cintervencion_ar2").checked;
  var cintervencion_ar3 = document.getElementById("cintervencion_ar3").checked;
  var cintervencion_ar4 = document.getElementById("cintervencion_ar4").checked;
  var cintervencion_ar5 = document.getElementById("cintervencion_ar5").checked;

  var PTR_IA_TIP_INTER ='[{"tipo":"CHKM"},{"resid":235,"estado":'+cintervencion_ar1+',"resvalor":"Apertura de Área"},{"resid":233,"estado":'+cintervencion_ar2+',"resvalor":"Calas Auger"},{"resid":232,"estado":'+cintervencion_ar3+',"resvalor":"Examen de perfiles"},{"resid":231,"estado":'+cintervencion_ar4+',"resvalor":"Hallazgo Accidental"},{"resid":234,"estado":'+cintervencion_ar5+',"resvalor":"Sondeos"}]';

  var cinmuebles_ar1 = document.getElementById("cinmuebles_ar1").checked; 
  var cinmuebles_ar2 = document.getElementById("cinmuebles_ar2").checked;
  var cinmuebles_ar3 = document.getElementById("cinmuebles_ar3").checked;
  var cinmuebles_ar4 = document.getElementById("cinmuebles_ar4").checked;
  var cinmuebles_ar5 = document.getElementById("cinmuebles_ar5").checked;
  var cinmuebles_ar6 = document.getElementById("cinmuebles_ar6").checked;
  var cinmuebles_ar7 = document.getElementById("cinmuebles_ar7").checked;
  var cinmuebles_ar8 = document.getElementById("cinmuebles_ar8").checked;
  var cinmuebles_ar9 = document.getElementById("cinmuebles_ar9").checked;
  var cinmuebles_ar10 = document.getElementById("cinmuebles_ar10").checked;
  var cinmuebles_ar11 = document.getElementById("cinmuebles_ar11").checked;
  
  var PTR_IA_ELEM_INM ='[{"tipo":"CHKM"},{"resid":164,"estado":'+cinmuebles_ar1+',"resvalor":"Almacenes"},{"resid":162,"estado":'+cinmuebles_ar2+',"resvalor":"Arquitectura Ceremonial"},{"resid":171,"estado":'+cinmuebles_ar3+',"resvalor":"Arte Repestre"},{"resid":165,"estado":'+cinmuebles_ar4+',"resvalor":"Caminos"},{"resid":167,"estado":'+cinmuebles_ar5+',"resvalor":"Entierros"},{"resid":166,"estado":'+cinmuebles_ar6+',"resvalor":"Habitaciones"},{"resid":172,"estado":'+cinmuebles_ar7+',"resvalor":"Obras hidráulicas"},{"resid":169,"estado":'+cinmuebles_ar8+',"resvalor":"Socavones"},{"resid":170,"estado":'+cinmuebles_ar9+',"resvalor":"Talleres"},{"resid":168,"estado":'+cinmuebles_ar10+',"resvalor":"Terraceado"},{"resid":163,"estado":'+cinmuebles_ar11+',"resvalor":"Torres Funerarias"}]';

   var cmuebles_ar1 = document.getElementById("cmuebles_ar1").checked;
   var cmuebles_ar2 = document.getElementById("cmuebles_ar2").checked;
   var cmuebles_ar3 = document.getElementById("cmuebles_ar3").checked;
   var cmuebles_ar4 = document.getElementById("cmuebles_ar4").checked;
   var cmuebles_ar5 = document.getElementById("cmuebles_ar5").checked;
 
  var PTR_IA_ELEM_MUEB = '[{"tipo":"CHKM"},{"resid":209,"estado":'+cmuebles_ar1+',"resvalor":"Cerámica"},{"resid":210,"estado":'+cmuebles_ar2+',"resvalor":"Líticos"},{"resid":211,"estado":'+cmuebles_ar3+',"resvalor":"Metales"},{"resid":213,"estado":'+cmuebles_ar4+',"resvalor":"Óseos Animales"},{"resid":212,"estado":'+cmuebles_ar5+',"resvalor":"Óseos Humanos"}]';

  var cas_data = '{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"2","PTR_CREA":"'+ccreador_arq+'","PTR_DEN":"'+cdenominacion+'","PTR_MUNI":"'+cubiMunicipio+'","PTR_MACRO":"'+cubiMacrodistrito+'","PTR_LUG":"'+cubiLocal+'","PTR_IA_ELEM_DESC":"'+cdescripInmuebles+'", "PTR_IA_EST_CONS":"'+cestadoConserva+'","PTR_IA_POLIG":"'+cubicacionPoligono+'", "PTR_IA_DESC_EST_CONS":"'+cdescripEstad_s+'","PTR_IA_OBS_EST":"'+cobservacion+'", "PTR_IA_TAFON":"'+ctafonomia+'","PTR_IA_DESC_RUR":"'+cdescripContextoRural+'", "PTR_IA_DESC_URB":"'+cdescripContextoUrbana+'","PTR_IA_TAFON_DESC":"'+cdescripTafonomia+'","PTR_IA_YACIM":"'+cdescripYacimiento+'","PTR_IA_ELEM_MUEBLE":"'+celementos+'","PTR_IA_DESC":"'+cdescrip+'","PTR_IA_ELEM_MUEB_DESC":"'+cdescripcionMueble+'",';

  var cas_data2 = '"PTR_IA_DESC_ELEM_INM":"'+PTR_IA_DESC_ELEM_INM+'","PTR_CRONO_DESC":"'+PTR_CRONO_DESC+'","PTR_CRONO":'+PTR_CRONO+',"PTR_IA_FUNC":'+PTR_IA_FUNC+',"PTR_IA_TAM_YACIM":"'+PTR_IA_TAM_YACIM+'","PTR_IA_TIP_INTER":'+PTR_IA_TIP_INTER+',"PTR_IA_NUM_POZ":"'+PTR_IA_NUM_POZ+'","PTR_IA_AR_INTER":"'+PTR_IA_AR_INTER+'","PTR_IA_PROF_MIN":"'+PTR_IA_PROF_MIN+'","PTR_IA_PROF_MAX":"'+PTR_IA_PROF_MAX+'","PTR_IA_ELEM_INM":'+PTR_IA_ELEM_INM+',"PTR_IA_ELEM_MUEB":'+PTR_IA_ELEM_MUEB+',"PTR_IA_ELEM_INM_DESC":"'+PTR_IA_ELEM_INM_DESC+'","PTR_IA_ELEM_MUEB_DESC":"'+PTR_IA_ELEM_MUEB_DESC+'","PTR_IA_OBS":"'+PTR_IA_OBS+'"}';

  var cas_data_total  = cas_data + cas_data2;
    //console.log("cas_data_total",cas_data_total);
  var jdata = JSON.stringify(cas_data_total);
  //console.log("stringyfi",cas_data_total);

  var xcas_nro_caso = correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = 'asunto';
  var xcas_tipo_hr = 'hora';
  var xcas_id_padre = 1;
  var xcampo_f ='campo'; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata+',"xcas_nombre_caso":"'+ccodPatrimonio+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
  //////////console.log("formDataaaa",formData);
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //console.log("dataaaaaaaaaa",data);
        listarPatrimonioCultural(g_tipo);
        swal( "Exíto..!", "Se registro correctamente los bienes arqueologicos...!", "success" );
        $( "#cdenominacion" ).val(""); 
        $( "#cubiMunicipio" ).val(""); 
        $( "#cubiMacrodistrito" ).val(""); 
        $( "#cubiLocal" ).val(""); 
        $( "#cdescripInmuebles" ).val(""); 
        $( "#cestadoConserva" ).val(""); 
        $( "#cubicacionPoligono" ).val(""); 
        $( "#cdescripEstado" ).val(""); 
        $( "#cobservacion" ).val(""); 
        $( "#ctafonomia" ).val(""); 
        $( "#cdescripContextoUrbana" ).val(""); 
        $( "#cdescripContextoRural" ).val(""); 
        $( "#cdescripTafonomia" ).val(""); 
        $( "#cdescripYacimiento" ).val(""); 
        $( "#celementos" ).val("");
        $( "#cdescrip" ).val(""); 
        $( "#cdescripcionMueble" ).val("");

        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
        setTimeout(function(){
          var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
          url = url.replace('casosId1', idCasos);
          //console.log("url",url);
          location.href = url;
        }, 1000);
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  

}, 500);
});

function limpiarM(){
  $( "#cprovenienciaMueble_m" ).val(""); 
  $( "#crelacionPieza_m" ).val(""); 
  $( "#cubicacionActual_m" ).val(""); 
  $( "#cDescripcionMueble_m" ).val(""); 
  $( "#cmaterial_m" ).val("");
  $( "#ctecnica_m" ).val("");
  $( "#cmanufactura_m" ).val("");
  $( "#cdecoracion_m" ).val("");
  $( "#catributos_m" ).val("");
  $( "#cestadoConserva_m").val("");
  $( "#cdescripEstado_m" ).val("");
  $( "#cobservacion_m" ).val("");
  $( "#ccronologia_m" ).val("");
  $( "#cfuncion_m" ).val("");

}
function limpiarS(){
  $( "#cdenominacion" ).val(""); 
  $( "#cubiMunicipio" ).val(""); 
  $( "#cubiMacrodistrito" ).val(""); 
  $( "#cubiLocal" ).val(""); 
  $( "#cdescripInmuebles" ).val(""); 
  $( "#cestadoConserva" ).val(""); 
  $( "#cubicacionPoligono" ).val(""); 
  $( "#cdescripEstado" ).val(""); 
  $( "#cobservacion" ).val(""); 
  $( "#ctafonomia" ).val(""); 
  $( "#cdescripContextoUrbana" ).val(""); 
  $( "#cdescripContextoRural" ).val(""); 
  $( "#cdescripTafonomia" ).val(""); 
  $( "#cdescripYacimiento" ).val(""); 
  $( "#celementos" ).val("");
  $( "#cdescrip" ).val(""); 
  $( "#cdescripcionMueble" ).val(""); 

}

function editarBienesArqMuebles(idCaso,nroFicha,proviene, relacion,ubiAct, descBien, material, tecnico, manofactura, decoracion,atributos,estadoConserva,descripEstado,observacion,crono,funcion,creador)
{
  //console.log("creador",creador);
  var descBien = caracteresEspecialesActualizar(descBien);
  var material = caracteresEspecialesActualizar(material);
  var tecnico = caracteresEspecialesActualizar(tecnico);
  var manofactura = caracteresEspecialesActualizar(manofactura);
  var decoracion = caracteresEspecialesActualizar(decoracion);
  var atributos = caracteresEspecialesActualizar(atributos);
  var descripEstado = caracteresEspecialesActualizar(descripEstado);
  var observacion = caracteresEspecialesActualizar(observacion);
  var funcion = caracteresEspecialesActualizar(funcion);

  $("#acasoId_m").val(idCaso);
  $("#acreador_arq").val(creador);
  $("#anroFicha_m").val(nroFicha);
  $("#aprovenienciaMueble_m").val(proviene);
  $("#arelacionPieza_m").val(relacion);
  $("#aubicacionActual_m").val(ubiAct);
  $("#aDescripcionMueble_m").val(descBien);
  $("#amaterial_m").val(material);
  $("#atecnica_m").val(tecnico);
  $("#amanufactura_m").val(manofactura);
  $("#adecoracion_m").val(decoracion);
  $("#aatributos_m").val(atributos);
  $("#aestadoConserva_m").val(estadoConserva);
  $("#adescripEstado_m").val(descripEstado);
  $("#aobservacion_m").val(observacion);
  $("#acronologia_m").val(crono);
  $("#afuncion_m").val(funcion);

};

function editarBienesArqSitios(idCaso,nroFicha,denominacion, municipio,macro, lugar, elementoDesc, obsEstado,estadoConservacion, poligono,descripEstado,obs,tafon, descRural,descUrbana,descTafon,yacimiento,elemMueble,desc,descMueble,creadorS,DescElementoInmuebles,cronologiaDesc,cronologiaArq,funcionSitio,tamanoYacim,tipoIntervencion,nroPozos,areaIntervencion,profundidadMinima,profundidadMaxima,elemInmuebles,elemMuebles,descripElemInmuebles,descElemMuebles,obserArq)

{

  //console.log("DescElementoInmuebles",DescElementoInmuebles);
  var cronologia = reemBarrasPorComillas(cronologiaArq);
  var cronologia = JSON.parse(cronologia);
  var funcionSitio = reemBarrasPorComillas(funcionSitio);
  var funcionSitio = JSON.parse(funcionSitio);
  var tipoIntervencion = reemBarrasPorComillas(tipoIntervencion);
  var tipoIntervencion = JSON.parse(tipoIntervencion);
  var elemInmuebles = reemBarrasPorComillas(elemInmuebles);
  var elemInmuebles = JSON.parse(elemInmuebles);
  var elemMuebles = reemBarrasPorComillas(elemMuebles);
  var elemMuebles = JSON.parse(elemMuebles);
  
  document.getElementById("acronologia_ar1").checked = cronologia[1].estado;
  document.getElementById("acronologia_ar2").checked = cronologia[2].estado;
  document.getElementById("acronologia_ar3").checked = cronologia[3].estado;
  document.getElementById("acronologia_ar4").checked = cronologia[4].estado;
  document.getElementById("acronologia_ar5").checked = cronologia[5].estado;
  document.getElementById("acronologia_ar6").checked = cronologia[6].estado;
  document.getElementById("acronologia_ar7").checked = cronologia[7].estado;
  document.getElementById("acronologia_ar8").checked = cronologia[8].estado;
  document.getElementById("acronologia_ar9").checked = cronologia[9].estado;

  document.getElementById("afuncionalidad_ar1").checked = funcionSitio[1].estado;
  document.getElementById("afuncionalidad_ar2").checked = funcionSitio[2].estado;
  document.getElementById("afuncionalidad_ar3").checked = funcionSitio[3].estado;
  document.getElementById("afuncionalidad_ar4").checked = funcionSitio[4].estado;
  document.getElementById("afuncionalidad_ar5").checked = funcionSitio[5].estado;
  document.getElementById("afuncionalidad_ar6").checked = funcionSitio[6].estado;
  document.getElementById("afuncionalidad_ar7").checked = funcionSitio[7].estado;
  document.getElementById("afuncionalidad_ar8").checked = funcionSitio[8].estado;

  document.getElementById("aintervencion_ar1").checked = tipoIntervencion[1].estado;
  document.getElementById("aintervencion_ar2").checked = tipoIntervencion[2].estado;
  document.getElementById("aintervencion_ar3").checked = tipoIntervencion[3].estado;
  document.getElementById("aintervencion_ar4").checked = tipoIntervencion[4].estado;
  document.getElementById("aintervencion_ar5").checked = tipoIntervencion[5].estado;

  document.getElementById("ainmuebles_ar1").checked = elemInmuebles[1].estado;
  document.getElementById("ainmuebles_ar2").checked = elemInmuebles[2].estado;
  document.getElementById("ainmuebles_ar3").checked = elemInmuebles[3].estado;
  document.getElementById("ainmuebles_ar4").checked = elemInmuebles[4].estado;
  document.getElementById("ainmuebles_ar5").checked = elemInmuebles[5].estado;
  document.getElementById("ainmuebles_ar6").checked = elemInmuebles[6].estado;
  document.getElementById("ainmuebles_ar7").checked = elemInmuebles[7].estado;
  document.getElementById("ainmuebles_ar8").checked = elemInmuebles[8].estado;
  document.getElementById("ainmuebles_ar9").checked = elemInmuebles[9].estado;
  document.getElementById("ainmuebles_ar10").checked = elemInmuebles[10].estado;
  document.getElementById("ainmuebles_ar11").checked = elemInmuebles[11].estado;

  document.getElementById("amuebles_ar1").checked = elemMuebles[1].estado;
  document.getElementById("amuebles_ar2").checked = elemMuebles[2].estado;
  document.getElementById("amuebles_ar3").checked = elemMuebles[3].estado;
  document.getElementById("amuebles_ar4").checked = elemMuebles[4].estado;
  document.getElementById("amuebles_ar5").checked = elemMuebles[5].estado;

  var elementoDesc = caracteresEspecialesActualizar(elementoDesc);
  var poligono = caracteresEspecialesActualizar(poligono);
  var descripEstado = caracteresEspecialesActualizar(descripEstado);
  var obs = caracteresEspecialesActualizar(obs);
  var descRural = caracteresEspecialesActualizar(descRural);
  var descUrbana = caracteresEspecialesActualizar(descUrbana);
  var descTafon = caracteresEspecialesActualizar(descTafon);
  var desc = caracteresEspecialesActualizar(desc);
  var descMueble = caracteresEspecialesActualizar(descMueble);
////////formulario 2/////////
  var cronologiaDesc = caracteresEspecialesActualizar(cronologiaDesc);
  var tamanoYacim = caracteresEspecialesActualizar(tamanoYacim);
  var descripElemInmuebles = caracteresEspecialesActualizar(descripElemInmuebles);
  var descElemMuebles = caracteresEspecialesActualizar(descElemMuebles);
  var obserArq = caracteresEspecialesActualizar(obserArq);

  $("#acasoId_s").val(idCaso);
  $("#anroFicha").val(nroFicha);
  $("#acreador_arq").val(creadorS);
  $("#adenominacion").val(denominacion);
  $("#aubiMunicipio").val(municipio);
  $("#aubiMacrodistrito").val(macro);
  $("#aubiLocal").val(lugar);
  $("#adescripInmuebles").val(elementoDesc);
  $("#aestadoConserva").val(estadoConservacion);
  $("#aubicacionPoligono").val(poligono);
  $("#adescripEstado").val(descripEstado);
  $("#aobservacion").val(obs);
  $("#atafonomia").val(tafon);
  $("#adescripContextoUrbana").val(descUrbana);
  $("#adescripContextoRural").val(descRural);
  $("#adescripTafonomia").val(descTafon);
  $("#adescripYacimiento").val(yacimiento);
  $("#aelementos").val(elemMueble);
  $("#adescrip").val(desc);
  $("#adescripcionMueble").val(descMueble);


  $("#adescripcionElem_ar").val(DescElementoInmuebles);
  $("#acronologiaDesc_ar").val(cronologiaDesc);
  $("#atamañoSitio_ar").val(tamanoYacim);
  $("#anumeroPozo_ar").val(nroPozos);
  $("#aareaInter_ar").val(areaIntervencion);
  $("#aprofundidadMin_ar").val(profundidadMinima);
  $("#aprofundidadMax_ar").val(profundidadMaxima);
  $("#adescripcionInmu_ar").val(descripElemInmuebles);
  $("#adescripcionMue_ar").val(descElemMuebles);
  $("#aobservaciones_ar").val(obserArq);


};

$( "#actualizarMuebleArqueologico" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      //////////console.log("aqui registro muebles");
      var acasoId_m = $( "#acasoId_m" ).val();
      var anroFicha_m = $( "#anroFicha_m" ).val();
      var acodPatrimonio = 'PTR_BMA'+anroFicha_m+'/2018';
      var acreador_arq = $( "#acreador_arq").val();
      var aprovenienciaMueble = document.getElementById('aprovenienciaMueble_m').value;
      var arelacionPieza = $( "#arelacionPieza_m" ).val();
      //////////console.log("aprovenienciaMueble",aprovenienciaMueble);
      //////////console.log("arelacionPieza",arelacionPieza);
      var aubicacionActual = $("#aubicacionActual_m").val();
      var aDescripcionMueble_m = $("#aDescripcionMueble_m").val();
      var amaterial_m = $("#amaterial_m").val();
      var atecnica_m =$("#atecnica_m").val();
      var amanufactura_m = $("#amanufactura_m").val();
      var adecoracion_m = $("#adecoracion_m").val();
      var aatributos_m = $("#aatributos_m").val();
      var aestadoConserva = document.getElementById('aestadoConserva_m').value;
      var adescripEstado_m = $("#adescripEstado_m").val();
      var aobservacion_m = $("#aobservacion_m").val();
      var acronologia = $("#acronologia_m").val();
      var afuncion_m = $("#afuncion_m").val();
      var g_tipo = 'PTR_BMA';

      var aDescripcionMueble_m = caracteresEspecialesRegistrar(aDescripcionMueble_m);
      var amaterial_m = caracteresEspecialesRegistrar(amaterial_m);
      var atecnica_m = caracteresEspecialesRegistrar(atecnica_m);
      var amanufactura_m = caracteresEspecialesRegistrar(amanufactura_m);
      var adecoracion_m = caracteresEspecialesRegistrar(adecoracion_m);
      var aatributos_m = caracteresEspecialesRegistrar(aatributos_m);
      var adescripEstado_m = caracteresEspecialesRegistrar(adescripEstado_m);
      var aobservacion_m = caracteresEspecialesRegistrar(aobservacion_m);
      var afuncion_m = caracteresEspecialesRegistrar(afuncion_m);

      var cas_data = '{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"1","PTR_CREA":"'+acreador_arq+'","PTR_PROVIENE_C":"'+aprovenienciaMueble+'","PTR_RELACION":"'+arelacionPieza+'","PTR_UBIC_ACT":"'+aubicacionActual+'","PTR_DESC_BIEN":"'+aDescripcionMueble_m+'","PTR_MATERIAL":"'+amaterial_m+'","PTR_TECN":"'+atecnica_m+'","PTR_MANF":"'+amanufactura_m+'","PTR_DECOR":"'+adecoracion_m+'","PTR_ATRIB":"'+aatributos_m+'","PTR_EST_CONS":"'+aestadoConserva+'","PTR_DESC_ESTADO":"'+adescripEstado_m+'","PTR_OBS":"'+aobservacion_m+'","PTR_CRONO":"'+acronologia+'","PTR_FUNC":"'+afuncion_m+'"}';
      var jdata = JSON.stringify(cas_data);
      //////////console.log("jdataaaa",jdata);

      var xcas_act_id = 137;
      var xcas_usr_actual_id = 1155;
      var xcas_estado_paso = 'Recibido';
      var xcas_nodo_id = 1;
      var xcas_usr_id = 1;
      var xcas_ws_id = 1;
      var xcas_asunto = 'asunto';
      var xcas_tipo_hr = 'hora';
      var xcas_id_padre = 1;
      var xcampo_f ='campo'; 

      var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_m+',"xcas_nro_caso":'+anroFicha_m+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata+',"xcas_nombre_caso":"'+acodPatrimonio+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
      ////////console.log("formData",formData);

      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: { 
         'authorization': 'Bearer' + token,
       },
       success: function(data){
        //////////console.log("dataaaaaa",data);
        listarPatrimonioCultural(g_tipo);
        swal( "Exíto..!", "Se actualizo correctamente el registro del Bienes Arqueologicos...!", "success" );

      },
      error: function(result) {
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });         
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
});

$( "#actualizarSitioArqueologico" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      //////////console.log("aqui registro sitios");
      var acasoId_m = $( "#acasoId_s" ).val();
      var anroFicha = $( "#anroFicha" ).val();
      var acreador_arq = $( "#acreador_arq" ).val();
      var adenominacion =$( "#adenominacion" ).val();
      var acodPatrimonio = 'PTR_BMA'+(anroFicha)+'/2018';
      var aprovenienciaMueble2 = 'aoleccion';
      var aubiMunicipio = $( "#aubiMunicipio" ).val();
      var aubiMacrodistrito =$("#aubiMacrodistrito").val();
      var aubiLocal = $("#aubiLocal").val();
      var adescripInmuebles = $("#adescripInmuebles").val();
      var aestadoConserva = $("#aestadoConserva").val();
      var aubicacionPoligono =$("#aubicacionPoligono").val();
      var adescripEstado = $("#adescripEstado").val();
      var aobservacion = $("#aobservacion").val();
      var atafonomia = $("#atafonomia").val();
      var adescripContextoUrbana = $("#adescripContextoUrbana").val();
      var adescripContextoRural = $("#adescripContextoRural").val();
      var adescripTafonomia = $("#adescripTafonomia").val();
      var adescripYacimiento = $("#adescripYacimiento").val();
      var aelementos = $("#aelementos").val();
      var adescrip = $("#adescrip").val();
      var adescripcionMueble = $("#adescripcionMueble").val();
      var g_tipo = 'PTR_BMA';

      var adescripInmuebles = caracteresEspecialesRegistrar(adescripInmuebles);
      var aubicacionPoligono = caracteresEspecialesRegistrar(aubicacionPoligono);
      var adescripEstado = caracteresEspecialesRegistrar(adescripEstado);
      var aobservacion = caracteresEspecialesRegistrar(aobservacion);
      var adescripContextoUrbana = caracteresEspecialesRegistrar(adescripContextoUrbana);
      var adescripContextoRural = caracteresEspecialesRegistrar(adescripContextoRural);
      var adescripTafonomia = caracteresEspecialesRegistrar(adescripTafonomia);
      var adescrip = caracteresEspecialesRegistrar(adescrip);
      var adescripcionMueble = caracteresEspecialesRegistrar(adescripcionMueble);

     
  ////////////////////////formulario 2/////////////////////

  var PTR_IA_DESC_ELEM_INM = $("#adescripcionElem_ar").val();
  var PTR_CRONO_DESC = $("#acronologiaDesc_ar").val();
  var PTR_IA_TAM_YACIM = $("#atamañoSitio_ar").val();
  var PTR_IA_DESC_ELEM_INM = $("#anumeroPozo_ar").val();
  var PTR_IA_AR_INTER = $("#aareaInter_ar").val();
  var PTR_IA_PROF_MIN = $("#aprofundidad_ar").val();
  var PTR_IA_PROF_MAX = $("#aprofundidadMax_ar").val();
  var PTR_IA_ELEM_INM_DESC = $("#adescripcionMue_ar").val();
  var PTR_IA_ELEM_MUEB_DESC = $("#adescripcionMue_ar").val();
  var PTR_IA_OBS = $("#aobservaciones_ar").val();

  var PTR_CRONO_DESC = caracteresEspecialesRegistrar(PTR_CRONO_DESC);
  var PTR_IA_TAM_YACIM = caracteresEspecialesRegistrar(PTR_IA_TAM_YACIM);
  var PTR_IA_ELEM_INM_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_INM_DESC);
  var PTR_IA_ELEM_MUEB_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_MUEB_DESC);
  var PTR_IA_OBS = caracteresEspecialesRegistrar(PTR_IA_OBS);

  var ccronologia_ar1 = document.getElementById("acronologia_ar1").checked;
  var ccronologia_ar2 = document.getElementById("acronologia_ar2").checked;
  var ccronologia_ar3 = document.getElementById("acronologia_ar3").checked;
  var ccronologia_ar4 = document.getElementById("acronologia_ar4").checked;
  var ccronologia_ar5 = document.getElementById("acronologia_ar5").checked;
  var ccronologia_ar6 = document.getElementById("acronologia_ar6").checked;
  var ccronologia_ar7 = document.getElementById("acronologia_ar7").checked;
  var ccronologia_ar8 = document.getElementById("acronologia_ar8").checked;
  var ccronologia_ar9 = document.getElementById("acronologia_ar9").checked;

  var PTR_CRONO = '[{"tipo":"CHKM"},{"resid":215,"estado":'+ccronologia_ar1+',"resvalor":"Arcaico tardío"},{"resid":214,"estado":'+ccronologia_ar2+',"resvalor":"Arcaico temprano-medio"},{"resid":221,"estado":'+ccronologia_ar3+',"resvalor":"Colonial"},{"resid":217,"estado":'+ccronologia_ar4+',"resvalor":"Formativo tardío"},{"resid":216,"estado":'+ccronologia_ar5+',"resvalor":"Formativo temprano-medio"},{"resid":220,"estado":'+ccronologia_ar6+',"resvalor":"Inca"},{"resid":219,"estado":'+ccronologia_ar7+',"resvalor":"Intermedio tardío"},{"resid":222,"estado":'+ccronologia_ar8+',"resvalor":"Republicano"},{"resid":218,"estado":'+ccronologia_ar9+',"resvalor":"Tiwanaku"}]';

  var cfuncionalidad_ar1 = document.getElementById("afuncionalidad_ar1").checked;
  var cfuncionalidad_ar2 = document.getElementById("afuncionalidad_ar2").checked;
  var cfuncionalidad_ar3 = document.getElementById("afuncionalidad_ar3").checked;
  var cfuncionalidad_ar4 = document.getElementById("afuncionalidad_ar4").checked;
  var cfuncionalidad_ar5 = document.getElementById("afuncionalidad_ar5").checked;
  var cfuncionalidad_ar6 = document.getElementById("afuncionalidad_ar6").checked;
  var cfuncionalidad_ar7 = document.getElementById("afuncionalidad_ar7").checked;
  var cfuncionalidad_ar8 = document.getElementById("afuncionalidad_ar8").checked;

  var PTR_IA_FUNC = '[{"tipo":"CHKM"},{"resid":230,"estado":'+cfuncionalidad_ar1+',"resvalor":"Administrativa"},{"resid":229,"estado":'+cfuncionalidad_ar2+',"resvalor":"Caminera"},{"resid":228,"estado":'+cfuncionalidad_ar3+',"resvalor":"Ceremonial"},{"resid":226,"estado":'+cfuncionalidad_ar4+',"resvalor":"Defensiva"},{"resid":223,"estado":'+cfuncionalidad_ar5+',"resvalor":"Doméstica"},{"resid":227,"estado":'+cfuncionalidad_ar6+',"resvalor":"Funeraria"},{"resid":225,"estado":'+cfuncionalidad_ar7+',"resvalor":"Manufactura"},{"resid":224,"estado":'+cfuncionalidad_ar8+',"resvalor":"Productiva"}]';

  var cintervencion_ar1 = document.getElementById("aintervencion_ar1").checked;
  var cintervencion_ar2 = document.getElementById("aintervencion_ar2").checked;
  var cintervencion_ar3 = document.getElementById("aintervencion_ar3").checked;
  var cintervencion_ar4 = document.getElementById("aintervencion_ar4").checked;
  var cintervencion_ar5 = document.getElementById("aintervencion_ar5").checked;

 var PTR_IA_TIP_INTER ='[{"tipo":"CHKM"},{"resid":235,"estado":'+cintervencion_ar1+',"resvalor":"Apertura de Área"},{"resid":233,"estado":'+cintervencion_ar2+',"resvalor":"Calas Auger"},{"resid":232,"estado":'+cintervencion_ar3+',"resvalor":"Examen de perfiles"},{"resid":231,"estado":'+cintervencion_ar4+',"resvalor":"Hallazgo Accidental"},{"resid":234,"estado":'+cintervencion_ar5+',"resvalor":"Sondeos"}]';

  var cinmuebles_ar1 = document.getElementById("ainmuebles_ar1").checked; 
  //console.log("cinmuebles_ar1",cinmuebles_ar1);
  var cinmuebles_ar2 = document.getElementById("ainmuebles_ar2").checked;
  //console.log("cinmuebles_ar2",cinmuebles_ar2);
  var cinmuebles_ar3 = document.getElementById("ainmuebles_ar3").checked;
  var cinmuebles_ar4 = document.getElementById("ainmuebles_ar4").checked;
  var cinmuebles_ar5 = document.getElementById("ainmuebles_ar5").checked;
  var cinmuebles_ar6 = document.getElementById("ainmuebles_ar6").checked;
  var cinmuebles_ar7 = document.getElementById("ainmuebles_ar7").checked;
  var cinmuebles_ar8 = document.getElementById("ainmuebles_ar8").checked;
  var cinmuebles_ar9 = document.getElementById("ainmuebles_ar9").checked;
  var cinmuebles_ar10 = document.getElementById("ainmuebles_ar10").checked;
  var cinmuebles_ar11 = document.getElementById("ainmuebles_ar11").checked;
  
  var PTR_IA_ELEM_INM ='[{"tipo":"CHKM"},{"resid":164,"estado":'+cinmuebles_ar1+',"resvalor":"Almacenes"},{"resid":162,"estado":'+cinmuebles_ar2+',"resvalor":"Arquitectura Ceremonial"},{"resid":171,"estado":'+cinmuebles_ar3+',"resvalor":"Arte Repestre"},{"resid":165,"estado":'+cinmuebles_ar4+',"resvalor":"Caminos"},{"resid":167,"estado":'+cinmuebles_ar5+',"resvalor":"Entierros"},{"resid":166,"estado":'+cinmuebles_ar6+',"resvalor":"Habitaciones"},{"resid":172,"estado":'+cinmuebles_ar7+',"resvalor":"Obras hidráulicas"},{"resid":169,"estado":'+cinmuebles_ar8+',"resvalor":"Socavones"},{"resid":170,"estado":'+cinmuebles_ar9+',"resvalor":"Talleres"},{"resid":168,"estado":'+cinmuebles_ar10+',"resvalor":"Terraceado"},{"resid":163,"estado":'+cinmuebles_ar11+',"resvalor":"Torres Funerarias"}]';

   var cmuebles_ar1 = document.getElementById("amuebles_ar1").checked;
   var cmuebles_ar2 = document.getElementById("amuebles_ar2").checked;
   var cmuebles_ar3 = document.getElementById("amuebles_ar3").checked;
   var cmuebles_ar4 = document.getElementById("amuebles_ar4").checked;
   var cmuebles_ar5 = document.getElementById("amuebles_ar5").checked;
 
  var PTR_IA_ELEM_MUEB = '[{"tipo":"CHKM"},{"resid":209,"estado":'+cmuebles_ar1+',"resvalor":"Cerámica"},{"resid":210,"estado":'+cmuebles_ar2+',"resvalor":"Líticos"},{"resid":211,"estado":'+cmuebles_ar3+',"resvalor":"Metales"},{"resid":213,"estado":'+cmuebles_ar4+',"resvalor":"Óseos Animales"},{"resid":212,"estado":'+cmuebles_ar5+',"resvalor":"Óseos Humanos"}]';
  
  var cas_data = '{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"2","PTR_DEN":"'+adenominacion+'","PTR_MUNI":"'+aubiMunicipio+'","PTR_MACRO":"'+aubiMacrodistrito+'","PTR_LUG":"'+aubiLocal+'","PTR_IA_ELEM_DESC":"'+adescripInmuebles+'", "PTR_IA_EST_CONS":"'+aestadoConserva+'","PTR_IA_POLIG":"'+aubicacionPoligono+'", "PTR_IA_DESC_EST_CONS":"'+adescripEstado+'","PTR_IA_OBS_EST":"'+aobservacion+'", "PTR_IA_TAFON":"'+atafonomia+'","PTR_IA_DESC_RUR":"'+adescripContextoRural+'", "PTR_IA_DESC_URB":"'+adescripContextoUrbana+'","PTR_IA_TAFON_DESC":"'+adescripTafonomia+'","PTR_IA_YACIM":"'+adescripYacimiento+'","PTR_IA_ELEM_MUEBLE":"'+aelementos+'","PTR_IA_DESC":"'+adescrip+'","PTR_IA_ELEM_MUEB_DESC":"'+adescripcionMueble+'",';

   var cas_data2 = '"PTR_IA_DESC_ELEM_INM":"'+PTR_IA_DESC_ELEM_INM+'","PTR_CRONO_DESC":"'+PTR_CRONO_DESC+'","PTR_CRONO":'+PTR_CRONO+',"PTR_IA_FUNC":'+PTR_IA_FUNC+',"PTR_IA_TAM_YACIM":"'+PTR_IA_TAM_YACIM+'","PTR_IA_TIP_INTER":'+PTR_IA_TIP_INTER+',"PTR_IA_NUM_POZ":"'+PTR_IA_NUM_POZ+'","PTR_IA_AR_INTER":"'+PTR_IA_AR_INTER+'","PTR_IA_PROF_MIN":"'+PTR_IA_PROF_MIN+'","PTR_IA_PROF_MAX":"'+PTR_IA_PROF_MAX+'","PTR_IA_ELEM_INM":'+PTR_IA_ELEM_INM+',"PTR_IA_ELEM_MUEB":'+PTR_IA_ELEM_MUEB+',"PTR_IA_ELEM_INM_DESC":"'+PTR_IA_ELEM_INM_DESC+'","PTR_IA_ELEM_MUEB_DESC":"'+PTR_IA_ELEM_MUEB_DESC+'","PTR_IA_OBS":"'+PTR_IA_OBS+'"}';

  var cas_data_total  = cas_data + cas_data2;
    //console.log("cas_data_total",cas_data_total);
  var jdata = JSON.stringify(cas_data_total);
  //console.log("stringyfi",cas_data_total);

    //var xcas_nro_caso_m=11;
    var xcas_act_id = 137;
    var xcas_usr_actual_id = 1155;
    //var xcas_nombre_caso='PTR_BMA77/2018' ;
    var xcas_estado_paso = 'Recibido';
    var xcas_nodo_id = 1;
    var xcas_usr_id = 1;
    var xcas_ws_id = 1;
    var xcas_asunto = 'asunto';
    var xcas_tipo_hr = 'hora';
    var xcas_id_padre = 1;
    var xcampo_f ='campo'; 

    var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_m+',"xcas_nro_caso":'+anroFicha+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata+',"xcas_nombre_caso":"'+acodPatrimonio+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
    //////////console.log("formData",formData);
    
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: { 
       'authorization': 'Bearer' + token,
     },
     success: function(data){
   //console.log("dataaaaaaActualiza",data);
      listarPatrimonioCultural(g_tipo);
      swal( "Exíto..!", "Se actualizo correctamente el registro del Bienes Arqueologicos...!", "success" );

    },
    error: function(result) {
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });         
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});  
});

/*-----------------END BIENES ARQUEOLOGICOS-----------------------*/

/*----------------- BIENES INMUEBLES-----------------------*/

$( "#registrarBienesInmuebles" ).click(function() 
{
  obtenercorrelativo('PTR_CBI');
  setTimeout(function(){

  var ccodPatrimonio_in = 'PTR_CBI'+correlativog+'/2018';
  var ccodCatastral_in = $("#ccodCatastral_in").val();
  var ccodCatastral1_in = $("#ccodCatastral1_in").val();
  var coriginal_in = $("#coriginal_in").val();
  var ctradicional_in = $("#ctradicional_in").val();
  var cActual_in = $("#cActual_in").val();
  var cMacrodistrito_in = document.getElementById('cMacrodistrito_in').value;
  var ctipoVia_in = document.getElementById('ctipoVia_in').value;
  var ccalleEsquina_in = $("#ccalleEsquina_in").val();
  var cdireccion_in = $("#cdireccion_in").val();
  var czona_in = $("#czona_in").val();
  var cnumero_in = $("#cnumero_in").val();
  //no se encuentrar estos campo
  var cnombrePropietario_in = 'gdhf';
  var tipoPropiedad_in = 'fgdghd';
  //
  var careaPredio_in = $("#careaPredio_in").val();
  var canchoFrente_in = $("#canchoFrente_in").val();
  var cfondoPredio_in = $("#cfondoPredio_in").val();
  var canchoMuro_in = $("#canchoMuro_in").val();
  var calturaInterior_in = $("#calturaInterior_in").val();
  var calturaFachada_in = $("#calturaFachada_in").val();
  var calturaMaxima_in = $("#calturaMaxima_in").val();
  //var ctradicional_check = $("#ctradicional_check").val();
  //var cactual_in = $("#cactual_in").val();
  //var ctendencia_in = $("#ctendencia_in").val();
  var cdescripcion_in = $("#cdescripcion_in").val();
  var cubicacionManzana_in = document.getElementById('cubicacionManzana_in').value;
  var clineaConstruccion_in = document.getElementById('clineaConstruccion_in').value;
  var ctrazoManzana_in = document.getElementById('ctrazoManzana_in').value;
  var ctipologiaArqui_in = document.getElementById('ctipologiaArqui_in').value;
  var careaEdificable_in = document.getElementById('careaEdificable_in').value;
  var cmaterialVia_in = document.getElementById('cmaterialVia_in').value;
  var ctipoVia_in = document.getElementById('ctipoVia_in').value;
  //var cbloqueInventario_in = $("#cbloqueInventario_in").val();
  var cbloqueInventario_in = document.getElementById('cbloqueInventario_in').value;
  var cbloquesReales_in = $("#cbloquesReales_in").val();
  var chogaresInventario_in = $("#chogaresInventario_in").val();
  var chogaresReales_in = $("#chogaresReales_in").val();
  var cestiloGeneral_in = document.getElementById('cestiloGeneral_in').value;
  var crangoEpoca_in = document.getElementById('crangoEpoca_in').value;
  var cestadoConserva_in = document.getElementById('cestadoConserva_in').value;

  /////GRILLA FIGURA DE PROTECCION /////////
  var cont=0;
  arrDibujo.forEach(function (item, index, array) 
  {
    arrCodigo[cont].arg_valor = $("#valor"+item.id+"").val();
    arrCodigo[cont].arg_tipo = $("#tipoarg"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"f01_FIG_LEG_|f01_FIG_OBS","titulos":"Figura de Proteccion Legal|Observaciones"}';
  for (var i = 0; i<arrCodigo.length;i++)
  {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigura = '{"f01_FIG_OBS":"'+arrCodigo[i].arg_valor+'","f01_FIG_LEG_":"'+arrCodigo[i].arg_tipo+'","f01_FIG_LEG__valor":"'+arrCodigo[i].arg_tipo+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaFiguraProtec = bloqueFiguraProtec; 

/////FIN GRILLA FIGURA DE PROTECCION  /////

/////GRILLA ARTISTICOS COMPOSITIVOS /////////

  var cont=0;
   arrDibujoTIC.forEach(function (item, index, array) 
        {
          arrCodigoTIC[cont].tipologicos1 = $("#tipologicos1"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos2 = $("#tipologicos2"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos3 = $("#tipologicos3"+item.id+"").val();
          cont++;
         
        });
   //console.log("arrCodigoTIC",arrCodigoTIC);
    var bloqueFiguraProtecTIC = '[{"tipo":"GRD","campos":"elem_art_estado|elem_art_inte|elem_art_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
    for (var i = 0; i<arrCodigoTIC.length;i++)
    {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraTIC = '{"elem_art_estado":"'+arrCodigoTIC[i].tipologicos1+'", "elem_art_estado_valor":"'+arrCodigoTIC[i].tipologicos1+'", "elem_art_inte":"'+arrCodigoTIC[i].tipologicos2+'", "elem_art_inte_valor":"'+arrCodigoTIC[i].tipologicos2+'", "elem_art_tipo":"'+arrCodigoTIC[i].tipologicos3+'", "elem_art_tipo_valor":"'+arrCodigoTIC[i].tipologicos3+'"}';

    bloqueFiguraProtecTIC = (bloqueFiguraProtecTIC.concat(',').concat(grillaFiguraTIC));
     
    }
    bloqueFiguraProtecTIC = bloqueFiguraProtecTIC.concat(']');
    var cgrillaFiguraProtecTIC = bloqueFiguraProtecTIC
    //console.log(cgrillaFiguraProtecTIC, "cgrillaFiguraProtecTIC");


    /////GRILLA ARTISTICOS ORNAMENTALES /////////
  var cont=0;
   arrDibujoOR.forEach(function (item, index, array) 
        {
          arrCodigoOR[cont].ornamentales1 = $("#ornamentales1"+item.id+"").val();
          arrCodigoOR[cont].ornamentales2 = $("#ornamentales2"+item.id+"").val();
          arrCodigoOR[cont].ornamentales3 = $("#ornamentales3"+item.id+"").val();
          cont++;         
        });
    //console.log("arrCodigoOR",arrCodigoOR);
    var bloqueFiguraProtecOR = '[{"tipo":"GRD","campos":"elem_orn_estado|elem_orn_inte|elem_orn_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
    for (var i = 0; i<arrCodigoOR.length;i++)
    {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraOR = '{"elem_orn_estado":"'+arrCodigoOR[i].tipologicos1+'", "elem_orn_estado_valor":"'+arrCodigoOR[i].tipologicos1+'", "elem_orn_inte":"'+arrCodigoOR[i].tipologicos2+'", "elem_orn_inte_valor":"'+arrCodigoOR[i].tipologicos2+'", "elem_orn_tipo":"'+arrCodigoOR[i].tipologicos3+'", "elem_orn_tipo_valor":"'+arrCodigoOR[i].tipologicos3+'"}';

    bloqueFiguraProtecOR = (bloqueFiguraProtecOR.concat(',').concat(grillaFiguraOR));
     
    }
    bloqueFiguraProtecOR = bloqueFiguraProtecOR.concat(']');
    var cgrillaFiguraProtecOR = bloqueFiguraProtecOR
    //console.log(cgrillaFiguraProtecOR, "cgrillaFiguraProtecOR");


    /////GRILLA ARTISTICOS TIPOLOGICOS /////////
  var cont=0;
   arrDibujoAR.forEach(function (item, index, array) 
        {
          arrCodigoAR[cont].artisticosC1 = $("#artisticosC1"+item.id+"").val();
          arrCodigoAR[cont].artisticosC2 = $("#artisticosC2"+item.id+"").val();
          arrCodigoAR[cont].artisticosC3 = $("#artisticosC3"+item.id+"").val();
          cont++;
         
        });
      //console.log("arrCodigoAR",arrCodigoAR);
    var bloqueFiguraProtecAR = '[{"tipo":"GRD","campos":"elem_orn_estado|elem_orn_inte|elem_orn_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
    for (var i = 0; i<arrCodigoAR.length;i++)
    {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraAR = '{"elem_orn_estado":"'+arrCodigoAR[i].tipologicos1+'", "elem_orn_estado_valor":"'+arrCodigoAR[i].tipologicos1+'", "elem_orn_inte":"'+arrCodigoAR[i].tipologicos2+'", "elem_orn_inte_valor":"'+arrCodigoAR[i].tipologicos2+'", "elem_orn_tipo":"'+arrCodigoAR[i].tipologicos3+'", "elem_orn_tipo_valor":"'+arrCodigoAR[i].tipologicos3+'"}';

    bloqueFiguraProtecAR = (bloqueFiguraProtecAR.concat(',').concat(grillaFiguraAR));
     
    }
    bloqueFiguraProtecAR = bloqueFiguraProtecAR.concat(']');
    var cgrillaFiguraProtecAR = bloqueFiguraProtecAR
     //console.log(cgrillaFiguraProtecAR, "cgrillaFiguraProtecAR");

    /////GRILLA HOGAR /////////
  var cont=0;
   arrDibujoHogar.forEach(function (item, index, array) 
        {
          arrCodigoHogar[cont].hogar1 = $("#hogar1"+item.id+"").val();
          arrCodigoHogar[cont].hogar2 = $("#hogar2"+item.id+"").val();
          arrCodigoHogar[cont].hogar3 = document.getElementById("hogar3"+item.id+"").checked;
          arrCodigoHogar[cont].hogar4 = document.getElementById("hogar4"+item.id+"").checked;
          arrCodigoHogar[cont].hogar5 = $("#hogar5"+item.id+"").val();
          arrCodigoHogar[cont].hogar6 = $("#hogar6"+item.id+"").val();
          cont++;         
        });
   //console.log(arrCodigoHogar, "arrCodigoHogar");
    var bloqueFiguraProtecHogar = '[{"tipo":"GRD","campos":"hog_desc|edad_hog|hom_hog|muj_hog|ten_hog|der_hog","titulos":"Hogar Des.|Edad|Hombre|Mujer|Tendencia|Derecho Propietario"}';
    for (var i = 0; i<arrCodigoHogar.length;i++)
    {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraHogar = '{"hog_desc":"'+arrCodigoHogar[i].hogar1+'", "edad_hog":"'+arrCodigoHogar[i].hogar2+'", "edad_hog_valor":"'+arrCodigoHogar[i].hogar2+'", "hom_hog":"'+arrCodigoHogar[i].hogar3+'", "muj_hog":"'+arrCodigoHogar[i].hogar4+'", "ten_hog":"'+arrCodigoHogar[i].hogar5+'", "ten_hog_valor":"'+arrCodigoHogar[i].hogar5+'", "der_hog":"'+arrCodigoHogar[i].hogar6+'", "der_hog_valor":"'+arrCodigoHogar[i].hogar6+'"}';

    bloqueFiguraProtecHogar = (bloqueFiguraProtecHogar.concat(',').concat(grillaFiguraHogar));
     
    }
    bloqueFiguraProtecHogar = bloqueFiguraProtecHogar.concat(']');
    var cgrillaFiguraProtecHogar = bloqueFiguraProtecHogar
    //console.log(cgrillaFiguraProtecHogar, "cgrillaFiguraProtecHogar");

//------------------- GRILLA REGIMEN PROPIEDA ------------

var cont=0;
reg_proDibujo.forEach(function (item, index, array) 
{
  reg_proCodigo[cont].arg_propietario = $("#cnombrePropietario_in"+item.id+"").val();
  reg_proCodigo[cont].arg_tipoP = $("#tipoPropiedad_in"+item.id+"").val();
  cont++;

});
var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"f01_nombre_PROP_|f01_reja_INT","titulos":"Nombre de Propietario|Tipo de Propiedad"}';
for (var i = 0; i<reg_proDibujo.length;i++)
{
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigura = '{"f01_nombre_PROP_":"'+reg_proCodigo[i].arg_propietario+'","f01_reja_INT":"'+reg_proCodigo[i].arg_tipoP+'","f01_reja_INT_valor":"'+reg_proCodigo[i].arg_tipoP+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaRegimenPropietario = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------

  //------------------- GRILLA MARCO LEGAL ------------

  var cont=0;
  ref_hisDibujo.forEach(function (item, index, array) 
  {
    ref_hisCodigo[cont].arg_tipoRef = $("#tipoRefrencia"+item.id+"").val();
    ref_hisCodigo[cont].arg_observacion = $("#observacion"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"f01_ref_hist_|f01_Obs_REF_","titulos":"Referencias Históricas|Observación"}';
  for (var i = 0; i<ref_hisDibujo.length;i++)
  {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigura = '{"f01_ref_hist_":"'+ref_hisCodigo[i].arg_tipoRef+'","f01_ref_hist__valor":"'+ref_hisCodigo[i].arg_tipoRef+'","f01_Obs_REF_":"'+ref_hisCodigo[i].arg_observacion+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaRefHist = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------
  //------------------- BLOQUE PELIGORS POTENCIALES-------------

  var contPelPot=0;
  arrDibujoPP.forEach(function (item, index, array) 
  {
    arrPP[contPelPot].val_pe = $("#peligropPP"+item.id+"").val();
    arrPP[contPelPot].val_pel = $("#peligropPPO"+item.id+"").val();
    arrPP[contPelPot].val_cau = $("#causasPP"+item.id+"").val();
    contPelPot++;
  });
  var bloqueFiguraProtecpp = '[{"tipo":"GRD","campos":"f01_peligro_DES_|f01_reja_DES_|f01_reja_INT_","titulos":"Peligros Potenciales|Peligros Potenciales|Nivel de Peligro"}';
  for (var i = 0; i<arrDibujoPP.length;i++)
  {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigurapp = '{"f01_peligro_DES_":"'+arrPP[i].val_pe+'","f01_peligro_DES_valor":"'+arrPP[i].val_pe+'","f01_reja_DES_":"'+arrPP[i].val_pel+'","f01_reja_DES_valor":"'+arrPP[i].val_pel+'","f01_reja_INT_":"'+arrPP[i].val_cau+'","f01_reja_INT_valor":"'+arrPP[i].val_cau+'"}';
    bloqueFiguraProtecpp = (bloqueFiguraProtecpp.concat(',').concat(grillaFigurapp));

  }
  PTR_PEL_POT1 = bloqueFiguraProtecpp.concat(']');
  //console.log(PTR_PEL_POT1,'INSERSION DE GRILLAS PELIGROS');

  //------------------- Patologías Edificación---------------------

  var contPE=0;
  arrDibujoPE.forEach(function (item, index, array) 
  {
    arrPE[contPE].val_daños = $("#dañosPE"+item.id+"").val();
    arrPE[contPE].val_causas = $("#causasPE"+item.id+"").val();
    contPE++;
  });
  var bloqueFiguraProtecPE = '[{"tipo":"GRD","campos":"f01_reja_DES_|f01_causa_DES_","titulos":"Daños|Causas"}';
  for (var i = 0; i<arrDibujoPE.length;i++)
  {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraPE = '{"f01_reja_DES_":"'+arrPE[i].val_daños+'","f01_reja_DES_valor":"'+arrPE[i].val_daños+'","f01_causa_DES_":"'+arrPE[i].val_causas+'","f01_causa_DES_valor":"'+arrPE[i].val_causas+'"}';
    bloqueFiguraProtecPE = (bloqueFiguraProtecPE.concat(',').concat(grillaFiguraPE));

  }
  PTR_PAT_EDIF1 = bloqueFiguraProtecPE.concat(']');
  //console.log(PTR_PAT_EDIF1,'PATOLOGIAS EDIFICACION');

   //------------------- Instalaciones de Estado---------------------

   var contINE=0;

   arrDibujoINE.forEach(function (item, index, array) 
   {
    arrINE[contINE].var_inst_est = $("#instalacionINE"+item.id+"").val();
    arrINE[contINE].var_pisose = $("#pisoservINE"+item.id+"").val();
    arrINE[contINE].var_est = $("#estadoINE"+item.id+"").val();
    arrINE[contINE].var_int = $("#integridadINE"+item.id+"").val();
    cont++;
  });
   var bloqueFiguraProtecINE = '[{"tipo":"GRD","campos":"f01_Instalaciones_DESC|f01_servidumbre_DESC|f01_estado_G_SERV_PROV|f01_integridad_G_SERV_PROV","titulos":"Instalaciones Estado|Paso de Servidumbre|Estado|Integridad"}';




   for (var i = 0; i<arrDibujoINE.length;i++)
   {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraINE = '{"f01_Instalaciones_DESC":"'+arrINE[i].var_inst_est+'","f01_Instalaciones_DESC_valor":"'+arrINE[i].var_inst_est+'","f01_servidumbre_DESC":"'+arrINE[i].var_pisose+'","f01_servidumbre_DESC_valor":"'+arrINE[i].var_pisose+'","f01_estado_G_SERV_PROV":"'+arrINE[i].var_est+'","f01_estado_G_SERV_PROV_valor":"'+arrINE[i].var_est+'","f01_integridad_G_SERV_PROV":"'+arrINE[i].var_int+'","f01_integridad_G_SERV_PROV_valor":"'+arrINE[i].var_int+'"}';
    bloqueFiguraProtecINE = (bloqueFiguraProtecINE.concat(',').concat(grillaFiguraINE));

  }
  PTR_INST_EST1 = bloqueFiguraProtecINE.concat(']');
  //console.log(PTR_INST_EST1,'INSTALACIONES DE ESTADO');

   //------------------- Servicios ---------------------

   var contSERV=0;
   arrDibujoSERV.forEach(function (item, index, array) 
   {
    arrSERV[contSERV].val_desc = $("#descripcionSERV"+item.id+"").val();
    arrSERV[contSERV].val_prov = $("#proveedorSERV"+item.id+"").val();
    contSERV++;
  });
   var bloqueFiguraProtecSERV = '[{"tipo":"GRD","campos":"f01_emision_G_SERV_DESC|f01_emision_G_SERV_PROV","titulos":"Descripción|Proveedor"}';

   for (var i = 0; i<arrDibujoSERV.length;i++)
   {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraSERV = '{"f01_emision_G_SERV_DESC":"'+arrSERV[i].val_desc+'","f01_emision_G_SERV_DESC_valor":"'+arrSERV[i].val_desc+'","f01_emision_G_SERV_PROV":"'+arrSERV[i].val_prov+'","f01_emision_G_SERV_PROV_valor":"'+arrSERV[i].val_prov+'"}';
    bloqueFiguraProtecSERV = (bloqueFiguraProtecSERV.concat(',').concat(grillaFiguraSERV));

  }
  PTR_SERVICIOS1 = bloqueFiguraProtecSERV.concat(']');
  //console.log(PTR_SERVICIOS1,'SERVICIOS');
  //------------------- FINAL--------------------------------------

  //------------------- GRILLA MURO EXTERIOR ------------  
  var cont=0;
  muro_extDibujo.forEach(function (item, index, array) 
  {
    muro_extCodigo[cont].arg_estado = $("#estado"+item.id+"").val();
    muro_extCodigo[cont].arg_integridad = $("#integridad"+item.id+"").val();
    muro_extCodigo[cont].arg_tipo = $("#tipo"+item.id+"").val();
    muro_extCodigo[cont].arg_material = $("#material"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"pint_ext_estado|pint_ext_inte|pint_ext_tipo|pint_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<muro_extDibujo.length;i++)
  {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigura = '{"pint_ext_estado":"'+muro_extCodigo[i].arg_estado+'","pint_ext_estado_valor":"'+muro_extCodigo[i].arg_estado+'","pint_ext_inte":"'+muro_extCodigo[i].arg_integridad+'","pint_ext_inte_valor":"'+muro_extCodigo[i].arg_integridad+'","pint_ext_tipo":"'+muro_extCodigo[i].arg_tipo+'","pint_ext_tipo_valor":"'+muro_extCodigo[i].arg_tipo+'","pint_ext_mat":"'+muro_extCodigo[i].arg_material+'","pint_ext_mat_valor":"'+muro_extCodigo[i].arg_material+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaMuroExt = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------



  //------------------- GRILLA MURO INTERIOR ------------  
  var cont=0;
  muro_intDibujo.forEach(function (item, index, array) 
  {
    muro_intCodigo[cont].arg_estadoInt = $("#estadoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_integridadInt = $("#integridadInt"+item.id+"").val();
    muro_intCodigo[cont].arg_tipoInt = $("#tipoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_materialInt = $("#materialInt"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"pint_int_estado|pint_int_inte|pint_int_tipo|pint_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<muro_intDibujo.length;i++)
  {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigura = '{"pint_int_estado":"'+muro_intCodigo[i].arg_estadoInt+'","pint_int_estado_valor":"'+muro_intCodigo[i].arg_estadoInt+'","pint_int_inte":"'+muro_intCodigo[i].arg_integridadInt+'","pint_int_inte_valor":"'+muro_intCodigo[i].arg_integridadInt+'","pint_int_tipo":"'+muro_intCodigo[i].arg_tipoInt+'","pint_int_tipo_valor":"'+muro_intCodigo[i].arg_tipoInt+'","pint_int_mat":"'+muro_intCodigo[i].arg_materialInt+'","pint_int_mat_valor":"'+muro_intCodigo[i].arg_materialInt+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaMuroInt = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------

  //------------------- GRILLA ACABADO EXTERIOR ------------  
  var cont=0;
  aca_extDibujo.forEach(function (item, index, array) 
  {
    aca_extCodigo[cont].arg_estadoAcaExt = $("#estadoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_integridadAcaExt = $("#integridadAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_tipoAcaExt = $("#tipoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_materialAcaExt = $("#materialAcaExt"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"aca_ext_estado|aca_ext_inte|aca_ext_tipo|aca_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<aca_extDibujo.length;i++)
  {
  ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
  var grillaFigura = '{"aca_ext_estado":"'+aca_extCodigo[i].arg_estadoAcaExt+'","aca_ext_estado_valor":"'+aca_extCodigo[i].arg_estadoAcaExt+'","aca_ext_inte":"'+aca_extCodigo[i].arg_integridadAcaExt+'","aca_ext_inte_valor":"'+aca_extCodigo[i].arg_integridadAcaExt+'","aca_ext_tipo":"'+aca_extCodigo[i].arg_tipoAcaExt+'","aca_ext_tipo_valor":"'+aca_extCodigo[i].arg_tipoAcaExt+'","aca_ext_mat":"'+aca_extCodigo[i].arg_materialAcaExt+'","aca_ext_mat_valor":"'+aca_extCodigo[i].arg_materialAcaExt+'"}';
  bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

}
bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
var cgrillaAcabadoEx = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------

  //------------------- GRILLA ACABADO INTERIOR ------------  
  var cont=0;
  aca_intDibujo.forEach(function (item, index, array) 
  {
    aca_intCodigo[cont].arg_estadoAcaInt = $("#estadoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_integridadAcaInt = $("#integridadAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_tipoAcaExt = $("#tipoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_materialAcaExt = $("#materialAcaInt"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"aca_int_estado|aca_int_inte|aca_int_tipo|aca_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<aca_intDibujo.length;i++)
  {
  ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
  var grillaFigura = '{"aca_int_estado":"'+aca_intCodigo[i].arg_estadoAcaInt+'","aca_int_estado_valor":"'+aca_intCodigo[i].arg_estadoAcaInt+'","aca_int_inte":"'+aca_intCodigo[i].arg_integridadAcaInt+'","aca_int_inte_valor":"'+aca_intCodigo[i].arg_integridadAcaInt+'","aca_int_tipo":"'+aca_intCodigo[i].arg_tipoAcaInt+'","aca_int_tipo_valor":"'+aca_intCodigo[i].arg_tipoAcaInt+'","aca_int_mat":"'+aca_intCodigo[i].arg_materialAcaInt+'","aca_int_mat_valor":"'+aca_intCodigo[i].arg_materialAcaInt+'"}';
  bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

}
bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
var cgrillaAcabadoInt = bloqueFiguraProtec; 


  //--------------------------------GISEL-------------------------------------------//
  //-----------------GRILLA PAREDES EXTERIORES--------------------------------------//

  var cont=0;
  arrDibujoPEX.forEach(function (item, index, array) 
  {
    arrPEX[cont].val_est_con = $("#estadocPEX"+item.id+"").val();
    arrPEX[cont].val_int = $("#integridadPEX"+item.id+"").val();
    arrPEX[cont].val_tipo = $("#tipoPEX"+item.id+"").val();
    arrPEX[cont].val_mat = $("#materialPEX"+item.id+"").val();
    cont++;
  });
  var bloqueParedExterna = '[{"tipo": "GRD","campos": "par_estado|par_inte|par_tipo|par_mat", "titulos": "EstadodeConservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';

  for (var i = 0; i<arrPEX.length;i++)
  {
    var grillaParedExterna = '{"par_mat": "'+arrPEX[i].val_mat+'","par_inte": "'+arrPEX[i].val_int+'","par_tipo": "'+arrPEX[i].val_tipo+'","par_estado": "'+arrPEX[i].val_est_con+'","par_mat_valor": 1,"par_inte_valor": 1,"par_tipo_valor": 1,"par_estado_valor": 2}';
    bloqueParedExterna = (bloqueParedExterna.concat(',').concat(grillaParedExterna));
     
  }
  bloqueParedExterna = bloqueParedExterna.concat(']');
  var cgrillaParedExterna = bloqueParedExterna; 

   //-------------FIN GRILLA PAREDES EXTERIORES-------------------//

   //---------------GRILLA COBERTURA  DEL TECHO-------------------//
  var cont=0;
  arrDibujoCT.forEach(function (item, index, array) 
  {
      arrCT[cont].val_est_con = $("#estadocCT"+item.id+"").val();
      arrCT[cont].val_inte = $("#integridadCT"+item.id+"").val();
      arrCT[cont].val_tipo = $("#tipoCT"+item.id+"").val();
      arrCT[cont].val_mate = $("#materialCT"+item.id+"").val();
      cont++;
    });
  var bloqueCoberturaTecho = '[ {"tipo": "GRD","campos": "cob_estado|cob_inte|cob_tipo|par_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCT.length;i++)
  {
    var grillaCoberturaTecho = '{"par_mat": "'+arrCT[i].val_mate+'","cob_inte": "'+arrCT[i].val_inte+'","cob_tipo": "'+arrCT[i].val_tipo+'","cob_estado": "'+arrCT[i].val_est_con+'","par_mat_valor": 1,"cob_inte_valor": 1,"cob_tipo_valor": 1,"cob_estado_valor": 1}';
    bloqueCoberturaTecho = (bloqueCoberturaTecho.concat(',').concat(grillaCoberturaTecho));    
  }

  bloqueCoberturaTecho = bloqueCoberturaTecho.concat(']');
  var cgrillaCoberturaTecho = bloqueCoberturaTecho; 

   //---------------GRILLA PAREDES INTERIORES -------------------//

   var cont=0;
        arrDibujoPI.forEach(function (item, index, array) 
        {
          arrPI[cont].val_est_conser = $("#estadocPI"+item.id+"").val();
          arrPI[cont].val_int = $("#integridadPI"+item.id+"").val();
          arrPI[cont].val_tipo = $("#tipoPI"+item.id+"").val();
          arrPI[cont].val_mat = $("#materialPI"+item.id+"").val();
          cont++;
        });
  var bloqueParedeInterior = '[ {"tipo": "GRD","campos": "pari_estado|pari_inte|pari_tipo|pari_mat","titulos": "Estado Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrPI.length;i++)
  {
    var grillaParedInterior = '{"pari_mat": "'+arrPI[i].val_mat+'","pari_inte": "'+arrPI[i].val_int+'","pari_tipo": "'+arrPI[i].val_tipo+'","pari_estado": "'+arrPI[i].val_est_conser+'","pari_mat_valor": 1,"pari_inte_valor": 1,"pari_tipo_valor": 2,"pari_estado_valor": 1}';
    bloqueParedeInterior = (bloqueParedeInterior.concat(',').concat(grillaParedInterior));    
  }

  bloqueParedeInterior = bloqueParedeInterior.concat(']');
  var cgrillaParedInterior = bloqueParedeInterior; 

  //---------------FIN  PAREDES INTERIORES--------------------------//

//---------------GRILLA ENTRE PISOS PTR_ENT_PIS-------------------//

     var cont=0;
        arrDibujoEPI.forEach(function (item, index, array) 
        {
          arrEPI[cont].val_esta = $("#estadocEPI"+item.id+"").val();
          arrEPI[cont].val_int = $("#integridadEPI"+item.id+"").val();
          arrEPI[cont].val_tipo = $("#tipoEPI"+item.id+"").val();
          arrEPI[cont].val_mat = $("#materialEPI"+item.id+"").val();
          cont++;
        });
  var bloqueEntrePisos = '[{"tipo": "GRD","campos": "ent_estado|ent_inte|ent_tipo|ent_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrEPI.length;i++)
  {

    var grillaEntrePisos = '{"ent_mat": "'+arrEPI[i].val_mat+'","ent_inte": "'+arrEPI[i].val_int+'","ent_tipo": "'+arrEPI[i].val_tipo+'","ent_estado": "'+arrEPI[i].val_esta+'","ent_mat_valor": 1,"ent_inte_valor": 1,"ent_tipo_valor": 1,"ent_estado_valor": 1}';
    bloqueEntrePisos = (bloqueEntrePisos.concat(',').concat(grillaEntrePisos));    
  }
  bloqueEntrePisos = bloqueEntrePisos.concat(']');
  var cgrillaEntrePisos = bloqueEntrePisos; 


 //---------------FIN GRILLA ENTRE PISOS----------------------------//

 //-----------------GRILLA ESCALERAS  -------------------------------//
    var cont=0;
        arrDibujoES.forEach(function (item, index, array) 
        {
          arrES[cont].val_estado = $("#estadocES"+item.id+"").val();
          arrES[cont].val_int = $("#integridadES"+item.id+"").val();
          arrES[cont].val_tipo = $("#tipoES"+item.id+"").val();
          arrES[cont].val_mat = $("#materialES"+item.id+"").val();
          cont++;
        });
  var bloqueEscalera = '[{"tipo": "GRD","campos": "esc_estado|esc_inte|esc_tipo|esc_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrES.length;i++)
  {

    var grillaEscalera = '{"esc_mat": "'+arrES[i].val_mat+'","esc_inte": "'+arrES[i].val_int+'","esc_tipo": "'+arrES[i].val_tipo+'","esc_estado": "'+arrES[i].val_estado+'","esc_mat_valor": 2,"esc_inte_valor": 1,"esc_tipo_valor": 4,"esc_estado_valor": 2}';
    bloqueEscalera = (bloqueEscalera.concat(',').concat(grillaEscalera));    
  }
  bloqueEscalera = bloqueEscalera.concat(']');
  var cgrillaEscalera = bloqueEscalera; 

 //---------------FIN GRILLA ESCALERAS----------------------------//

 //---------------GRILLA CIELOS ------------------//

     var cont=0;
      arrDibujoCIE.forEach(function (item, index, array) 
      {
          arrCIE[cont].val_est = $("#estadocCIE"+item.id+"").val();
          arrCIE[cont].val_int = $("#integridadCIE"+item.id+"").val();
          arrCIE[cont].val_tipo = $("#tipoCIE"+item.id+"").val();
          arrCIE[cont].val_mat = $("#materialCIE"+item.id+"").val();
          cont++;
      });
  var bloqueCielo = '[{"tipo": "GRD","campos": "cie_estado|cie_inte|cie_tipo|cie_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCIE.length;i++)
  {

    var grillaCielo = '{"cie_mat": "'+arrCIE[i].val_mat+'","cie_inte": "'+arrCIE[i].val_int+'","cie_tipo": "'+arrCIE[i].val_tipo+'","cie_estado": "'+arrCIE[i].val_est+'","cie_mat_valor": 2,"cie_inte_valor": 1,"cie_tipo_valor": 4,"cie_estado_valor": 2}';
    bloqueCielo = (bloqueCielo.concat(',').concat(grillaCielo));    
  }
  bloqueCielo = bloqueCielo.concat(']');
  var cgrillaCielo = bloqueCielo; 
 //---------------FIN CIELOS----------------------------//

 //---------------GRILLA BALCONES ------------------//

   var cont=0;
        arrDibujoBAL.forEach(function (item, index, array) 
        {
          arrBAL[cont].val_est = $("#estadocBAL"+item.id+"").val();
          arrBAL[cont].val_inte = $("#integridadBAL"+item.id+"").val();
          arrBAL[cont].val_tipo = $("#tipoBAL"+item.id+"").val();
          arrBAL[cont].val_mat = $("#materialBAL"+item.id+"").val();
          cont++;
        });

  var bloqueBalcon = '[ {"tipo": "GRD","campos": "bal_estado|bal_inte|bal_tipo|bal_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrBAL.length;i++)
  {
    var grillaBalcon = '{"bal_mat": "'+arrBAL[i].val_mat+'","bal_inte": "'+arrBAL[i].val_inte+'","bal_tipo": "'+arrBAL[i].val_tipo+'","bal_estado": "'+arrBAL[i].val_est+'","bal_mat_valor": 1,"bal_inte_valor": 2,"bal_tipo_valor": 1,"bal_estado_valor": 3}';
    bloqueBalcon = (bloqueBalcon.concat(',').concat(grillaBalcon));    
  }

  bloqueBalcon = bloqueBalcon.concat(']');
  var cgrillaBalcon = bloqueBalcon; 

 //---------------FIN GRILLA BALCONES----------------------------//

 //---------------GRILLA ESTRUCTURA CUBIERTA ------------------//

 var cont=0;
        arrDibujoEC.forEach(function (item, index, array) 
        {
          arrEC[cont].val_est = $("#estadocEC"+item.id+"").val();
          arrEC[cont].val_int = $("#integridadEC"+item.id+"").val();
          arrEC[cont].val_tipo = $("#tipoEC"+item.id+"").val();
          arrEC[cont].val_mat = $("#materialEC"+item.id+"").val();
          cont++;
        });
  var bloqueEstructuraCubierta = '[{"tipo": "GRD","campos": "est_cub_estado|est_cub_inte|est_cub_tipo|est_cub_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrEC.length;i++)
  {

    var grillaEstructuraCubierta = '{"est_cub_mat": "'+arrEC[i].val_mat+'","est_cub_inte": "'+arrEC[i].val_int+'","est_cub_tipo": "'+arrEC[i].val_tipo+'","est_cub_estado": "'+arrEC[i].val_est+'","est_cub_mat_valor": 2,"est_cub_inte_valor": 3,"est_cub_tipo_valor": 2,"est_cub_estado_valor": 1}';
    bloqueEstructuraCubierta = (bloqueEstructuraCubierta.concat(',').concat(grillaEstructuraCubierta));    
  }
  bloqueEstructuraCubierta = bloqueEstructuraCubierta.concat(']');
  var cgrillaEstructuraCubierta = bloqueEstructuraCubierta; 

 //---------------FIN GRILLA ESTRUCTURA CUBIERTA-------------------//

//------------------------GRILLA PISOS---------------------------//
        
      var cont=0;
      arrDibujoP.forEach(function (item, index, array) 
        {
          arrP[cont].val_est = $("#estadocP"+item.id+"").val();
          arrP[cont].val_int = $("#integridadP"+item.id+"").val();
          arrP[cont].val_tipo = $("#tipoP"+item.id+"").val();
          arrP[cont].val_mat = $("#materialP"+item.id+"").val();
          cont++;
      });

  var bloquePiso = '[{"tipo": "GRD","campos": "piso_estado|piso_inte|piso_tipo|piso_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrP.length;i++)
  {

    var grillaPiso = '{"piso_mat": "'+arrP[i].val_mat+'","piso_inte": "'+arrP[i].val_int+'","piso_tipo": "'+arrP[i].val_tipo+'","piso_estado": "'+arrP[i].val_est+'","piso_mat_valor": 1,"piso_inte_valor": 1,"piso_tipo_valor": 1,"piso_estado_valor": 2}';
    bloquePiso = (bloquePiso.concat(',').concat(grillaPiso));    
  }
  bloquePiso = bloquePiso.concat(']');
  var cgrillaPiso = bloquePiso; 

//JSON TANYA
 /////GRILLA Vanos Ventanas Interiores /////////
   var contvvi=0;
   arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
        {
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_estado = $("#vent_int_estado"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_inte = $("#vent_int_inte"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_tipo = $("#vent_int_tipo"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_mat = $("#vent_int_mat"+item.id+"").val();
          contvvi++;
         
        });
    //console.log('vector', arrCodigoEleComVanosVenInteriores);
   //console.log('elemento', arrCodigoEleComVanosVenInteriores[0].arg_vent_int_estado);
    var bloqueFiguraProtecvvi= '[{"tipo":"GRD","campos":"vent_int_estado|vent_int_inte|vent_int_tipo|vent_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    //console.log('titulos',bloqueFiguraProtecvvi);
    var grillaFiguravvi = '';   
    for (var i = 0; i<arrCodigoEleComVanosVenInteriores.length;i++)
    {
    ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    grillaFiguravvi = '{"vent_int_estado":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_estado +'","vent_int_inte":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_inte +'","vent_int_tipo":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_tipo +'","vent_int_mat":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_mat +'"}';
    //var grillaFiguravvi = '{"vent_int_estado":"1","vent_int_inte":"1","vent_int_tipo":"1","vent_int_mat":"1"}';
    //console.log('grillaaa', grillaFiguravvi );


    bloqueFiguraProtecvvi = (bloqueFiguraProtecvvi.concat(',').concat(grillaFiguravvi));
     
    }
    bloqueFiguraProtecvvi = bloqueFiguraProtecvvi.concat(']');
    //alert(bloqueFiguraProtecvvi);


    ///GRILLA2
    var contdvi=0;
    arrDibujovent_inte_es_in.forEach(function (item, index, array) 
        {
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_estado = $("#vent_inte_estado"+item.id+"").val();
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_inte = $("#vent_inte_inte"+item.id+"").val();
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_tipo = $("#vent_inte_tipo"+item.id+"").val();
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_mat = $("#vent_inte_mat"+item.id+"").val();
          contdvi++;
         
        });
    var bloqueFiguraProtecdvi= '[{"tipo":"GRD","campos":"vent_inte_estado|vent_inte_inte|vent_inte_tipo|vent_inte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    //console.log('titulos',bloqueFiguraProtecdvi);
    var grillaFiguradvi = '';   
    for (var i = 0; i<arrCodigovent_inte_es_in.length;i++)
    {
    grillaFiguradvi = '{"vent_inte_estado":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_estado +'","vent_inte_inte":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_inte +'","vent_inte_tipo":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_tipo +'","vent_inte_mat":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_mat +'"}';
    bloqueFiguraProtecdvi = (bloqueFiguraProtecdvi.concat(',').concat(grillaFiguradvi));
     
    }
    bloqueFiguraProtecdvi = bloqueFiguraProtecdvi.concat(']');
    //alert(bloqueFiguraProtecdvi);
    

    ///GRILLA3
    var contdvve=0;
    arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
        {
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_estado = $("#vent_ext_estado"+item.id+"").val();
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_inte = $("#vent_ext_inte"+item.id+"").val();
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_mat = $("#vent_ext_mat"+item.id+"").val();
          contdvve++;
         
        });
    var bloqueFiguraProtecdvve= '[{"tipo":"GRD","campos":"vent_ext_estado|vent_ext_inte|vent_ext_tipo|vent_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    //console.log('titulos',bloqueFiguraProtecdvve);
    var grillaFiguradvve = '';   
    for (var i = 0; i<arrCodigoVanVenExt_es_int.length;i++)
    {
    grillaFiguradvve = '{"vent_ext_estado":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_estado +'","vent_ext_inte":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_inte +'","vent_ext_tipo":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_tipo +'","vent_ext_mat":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_mat +'"}';
    bloqueFiguraProtecdvve = (bloqueFiguraProtecdvve.concat(',').concat(grillaFiguradvve));
     
    }
    bloqueFiguraProtecdvve = bloqueFiguraProtecdvve.concat(']');
    //(bloqueFiguraProtecdvve);
    ///GRILLA3

    ///GRILLA4 
    var contdve=0;
    arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_estado = $("#vent_exte_estado"+item.id+"").val();
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_inte = $("#vent_exte_inte"+item.id+"").val();
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_tipo = $("#vent_exte_tipo"+item.id+"").val();
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_mat = $("#vent_exte_mat"+item.id+"").val();
          contdve++;
         
        });
    var bloqueFiguraProtecdve= '[{"tipo":"GRD","campos":"vent_exte_estado|vent_exte_inte|vent_exte_tipo|vent_exte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    //console.log('titulos',bloqueFiguraProtecdve);
    var grillaFiguradve = '';   
    for (var i = 0; i<arrCodigo_ptr_vent_ext.length;i++)
    {
    grillaFiguradve = '{"vent_exte_estado":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_estado +'","vent_exte_inte":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_inte +'","vent_exte_tipo":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_tipo +'","vent_exte_mat":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_mat +'"}';
    bloqueFiguraProtecdve = (bloqueFiguraProtecdve.concat(',').concat(grillaFiguradve));
     
    }
    bloqueFiguraProtecdve = bloqueFiguraProtecdve.concat(']');
    //alert(bloqueFiguraProtecdve);
    ///GRILLA4 
    ///GRILLA5
    var contdvpi=0;
    arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_estado = $("#pue_int_estado"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_inte = $("#pue_int_inte"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_tipo = $("#pue_int_tipo"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_mat = $("#pue_int_mat"+item.id+"").val();
          contdvpi++;         
        });
    var bloqueFiguraProtecdvpi= '[{"tipo":"GRD","campos":"pue_int_estado|pue_int_inte|pue_int_tipo|pue_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradvpi = '';   
    for (var i = 0; i<arrCodigo_ptr_van_pue_int.length;i++)
    {
    grillaFiguradvpi = '{"pue_int_estado":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_estado +'","pue_int_inte":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_inte +'","pue_int_tipo":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_tipo +'","pue_int_mat":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_mat +'"}';
    bloqueFiguraProtecdvpi = (bloqueFiguraProtecdvpi.concat(',').concat(grillaFiguradvpi));
    }
    bloqueFiguraProtecdvpi = bloqueFiguraProtecdvpi.concat(']');
    //alert(bloqueFiguraProtecdvpi);
    ///GRILLA5
    ///GRILLA6
    var contdvpe=0;
    arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_estado = $("#pue_ext_estado"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_inte = $("#pue_ext_inte"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_tipo = $("#pue_ext_tipo"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_mat = $("#pue_ext_mat"+item.id+"").val();
          contdvpe++;         
        });
    var bloqueFiguraProtecdvpe= '[{"tipo":"GRD","campos":"pue_ext_estado|pue_ext_inte|pue_ext_tipo|pue_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradvpe = '';   
    for (var i = 0; i<arrCodigo_ptr_van_pue_ext.length;i++)
    {
    grillaFiguradvpe = '{"pue_ext_estado":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_estado +'","pue_ext_inte":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_inte +'","pue_ext_tipo":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_tipo +'","pue_ext_mat":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_mat +'"}';
    bloqueFiguraProtecdvpe = (bloqueFiguraProtecdvpe.concat(',').concat(grillaFiguradvpe));
    }
    bloqueFiguraProtecdvpe = bloqueFiguraProtecdvpe.concat(']');
    //alert(bloqueFiguraProtecdvpe);
    ///GRILLA6
    ///GRILLA7
    var contdpi=0;
    arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_estado = $("#pue_inte_estado"+item.id+"").val();
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_inte = $("#pue_inte_inte"+item.id+"").val();
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_tipo = $("#pue_inte_tipo"+item.id+"").val();
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_mat = $("#pue_inte_mat"+item.id+"").val();
          contdpi++;         
        });
    var bloqueFiguraProtecdpi= '[{"tipo":"GRD","campos":"pue_inte_estado|pue_inte_inte|pue_inte_tipo|pue_inte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradpi = '';   
    for (var i = 0; i<arrCodigo_ptr_pue_int.length;i++)
    {
    grillaFiguradpi = '{"pue_inte_estado":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_estado +'","pue_inte_inte":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_inte +'","pue_inte_tipo":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_tipo +'","pue_inte_mat":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_mat +'"}';
    bloqueFiguraProtecdpi = (bloqueFiguraProtecdpi.concat(',').concat(grillaFiguradpi));
    }
    bloqueFiguraProtecdpi = bloqueFiguraProtecdpi.concat(']');
    //alert(bloqueFiguraProtecdpi);
    ///GRILLA7  
    ///GRILLA8
    var contdpe=0;
    arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_estado = $("#pue_exte_estado"+item.id+"").val();
          arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_inte = $("#pue_exte_inte"+item.id+"").val();
          arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_tipo = $("#pue_exte_tipo"+item.id+"").val();
          arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_mat = $("#pue_exte_mat"+item.id+"").val();
          contdpe++;         
        });
    var bloqueFiguraProtecdpe= '[{"tipo":"GRD","campos":"pue_exte_estado|pue_exte_inte|pue_exte_tipo|pue_exte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradpe = '';   
    for (var i = 0; i<arrCodigo_ptr_pue_ext.length;i++)
    {
    grillaFiguradpe = '{"pue_exte_estado":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_estado +'","pue_exte_inte":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_inte +'","pue_exte_tipo":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_tipo +'","pue_exte_mat":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_mat +'"}';
    bloqueFiguraProtecdpe = (bloqueFiguraProtecdpe.concat(',').concat(grillaFiguradpe));
    }
    bloqueFiguraProtecdpe = bloqueFiguraProtecdpe.concat(']');
    //alert(bloqueFiguraProtecdpe);
    ///GRILLA8
    ///GRILLA9
    var contdr=0;
    arrDibujo_ptr_reja.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_reja[contdr].arg_reja_estado = $("#reja_estado"+item.id+"").val();
          arrCodigo_ptr_reja[contdr].arg_reja_inte = $("#reja_inte"+item.id+"").val();
          arrCodigo_ptr_reja[contdr].arg_reja_tipo = $("#reja_tipo"+item.id+"").val();
          arrCodigo_ptr_reja[contdr].arg_reja_mat = $("#reja_mat"+item.id+"").val();
          contdr++;         
        });
    var bloqueFiguraProtecdr= '[{"tipo":"GRD","campos":"reja_estado|reja_inte|reja_tipo|reja_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradr = '';   
    for (var i = 0; i<arrCodigo_ptr_reja.length;i++)
    {
    grillaFiguradr = '{"reja_estado":"'+ arrCodigo_ptr_reja[i].arg_reja_estado +'","reja_inte":"'+ arrCodigo_ptr_reja[i].arg_reja_inte +'","reja_tipo":"'+ arrCodigo_ptr_reja[i].arg_reja_tipo +'","reja_mat":"'+ arrCodigo_ptr_reja[i].arg_reja_mat +'"}';
    bloqueFiguraProtecdr = (bloqueFiguraProtecdr.concat(',').concat(grillaFiguradr));
    }
    bloqueFiguraProtecdr = bloqueFiguraProtecdr.concat(']');
    //alert(bloqueFiguraProtecdr);
    ///GRILLA9


    //FIN JSON TANYA

  //------------------- FIN GRILLA ----------------------------

  var cfechaConstruccion_in = $("#cfechaConstruccion_in").val();
  var cautor_in = $("#cautor_in").val();
  var cpreexistencia_in = $("#cpreexistencia_in").val();
  var cpropietarios_in = $("#cpropietarios_in").val();
  var cusoOriginal_in = $("#cusoOriginal_in").val();
  var chechoHistorico_in = $("#chechoHistorico_in").val();
  var cfiestaReligiosa_in = $("#cfiestaReligiosa_in").val();
  var cdatoHistorico_in = $("#cdatoHistorico_in").val();
  var cfuente_in = $("#cfuente_in").val();
  var cfechaConstruccion_in3 = $("#cfechaConstruccion_in3").val();
  var cautor_in3 = $("#cautor_in3").val();
  var cpreexistencia_in3 = $("#cpreexistencia_in3").val();
  var cpropietarios_in3 = $("#cpropietarios_in3").val();
  var cusoOriginal_in3 = $("#cusoOriginal_in3").val();
  var chechoHistorico_in3 = $("#chechoHistorico_in3").val();
  var cfiestaReligiosa_in3 = $("#cfiestaReligiosa_in3").val();
  var cdatoHistorico_in3 = $("#cdatoHistorico_in3").val();
  var cfuente_in3 = $("#cfuente_in3").val();
  var cinscripcion_in= $("#cinscripcion_in").val();
  var cexistePatrimonio_in = $("#cexistePatrimonio_in").val();
  var cexisteResto_in = $("#cexisteResto_in").val();
  var cexisteElemento_in = $("#cexisteElemento_in").val();
  var critosMitos_in = $("#critosMitos_in").val();
  var cconjunto_in = document.getElementById('cconjunto_in').value;
  var cestadoVerifica_in = document.getElementById('cestadoVerifica_in').value;
  var cpuntuacion_in = $("#cpuntuacion_in").val();
  var g_tipo = 'PTR_CBI';

  var cdescripcion_in = caracteresEspecialesRegistrar(cdescripcion_in);
  var cpreexistencia_in = caracteresEspecialesRegistrar(cpreexistencia_in);
  var cpropietarios_in = caracteresEspecialesRegistrar(cpropietarios_in);
  var cusoOriginal_in = caracteresEspecialesRegistrar(cusoOriginal_in);
  var chechoHistorico_in = caracteresEspecialesRegistrar(chechoHistorico_in);
  var cfiestaReligiosa_in = caracteresEspecialesRegistrar(cfiestaReligiosa_in);
  var cdatoHistorico_in = caracteresEspecialesRegistrar(cdatoHistorico_in);
  var cfuente_in = caracteresEspecialesRegistrar(cfuente_in);
  var cpreexistencia_in3 = caracteresEspecialesRegistrar(cpreexistencia_in3);
  var cpropietarios_in3 = caracteresEspecialesRegistrar(cpropietarios_in3);
  var cusoOriginal_in3 = caracteresEspecialesRegistrar(cusoOriginal_in3);
  var chechoHistorico_in3 = caracteresEspecialesRegistrar(chechoHistorico_in3);
  var cfiestaReligiosa_in3 = caracteresEspecialesRegistrar(cfiestaReligiosa_in3);
  var cdatoHistorico_in3 = caracteresEspecialesRegistrar(cdatoHistorico_in3);
  var cfuente_in3 = caracteresEspecialesRegistrar(cfuente_in3);
  var cinscripcion_in = caracteresEspecialesRegistrar(cinscripcion_in);
  var cexistePatrimonio_in = caracteresEspecialesRegistrar(cexistePatrimonio_in);
  var cexisteResto_in = caracteresEspecialesRegistrar(cexisteResto_in);
  var cexisteElemento_in = caracteresEspecialesRegistrar(cexisteElemento_in);
  var critosMitos_in = caracteresEspecialesRegistrar(critosMitos_in);

  var tradicional1 = document.getElementById("tradicional1").checked;
  var tradicional2 = document.getElementById("tradicional2").checked;
  var tradicional3 = document.getElementById("tradicional3").checked;
  var tradicional4 = document.getElementById("tradicional4").checked;
  var tradicional5 = document.getElementById("tradicional5").checked;
  var tradicional6 = document.getElementById("tradicional6").checked;
  var tradicional7 = document.getElementById("tradicional7").checked;
  var tradicional8 = document.getElementById("tradicional8").checked;
  var tradicional9 = document.getElementById("tradicional9").checked;
  var tradicional10 = document.getElementById("tradicional10").checked;
  var tradicional11= document.getElementById("tradicional11").checked;
  var tradicional12 = document.getElementById("tradicional12").checked;

  var actual1 = document.getElementById("actual1").checked;
  var actual2 = document.getElementById("actual2").checked;
  var actual3 = document.getElementById("actual3").checked;
  var actual4 = document.getElementById("actual4").checked;
  var actual5 = document.getElementById("actual5").checked;
  var actual6 = document.getElementById("actual6").checked;
  var actual7 = document.getElementById("actual7").checked;
  var actual8 = document.getElementById("actual8").checked;
  var actual9 = document.getElementById("actual9").checked;
  var actual10 = document.getElementById("actual10").checked;
  var actual11 = document.getElementById("actual11").checked;
  var actual12 = document.getElementById("actual12").checked;

  var tendencia1 = document.getElementById("tendencia1").checked;
  var tendencia2 = document.getElementById("tendencia2").checked;
  var tendencia3 = document.getElementById("tendencia3").checked;
  var tendencia4 = document.getElementById("tendencia4").checked;
  var tendencia5 = document.getElementById("tendencia5").checked;
  var tendencia6 = document.getElementById("tendencia6").checked;
  var tendencia7 = document.getElementById("tendencia7").checked;
  var tendencia8 = document.getElementById("tendencia8").checked;
  var tendencia9 = document.getElementById("tendencia9").checked;
  var tendencia10 = document.getElementById("tendencia10").checked;
  var tendencia11 = document.getElementById("tendencia11").checked;
  var tendencia12 = document.getElementById("tendencia12").checked;

  var ctradicional_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+tradicional1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+tradicional2+', "resvalor": "Culto"},{"resid": 280, "estado": '+tradicional3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+tradicional4+', "resvalor": "Industria"},{"resid": 282, "estado": '+tradicional5+', "resvalor": "Militar"},{"resid": 281, "estado": '+tradicional6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+tradicional7+', "resvalor": "Salud"},{"resid": 278, "estado": '+tradicional8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+tradicional9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+tradicional10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+tradicional11+', "resvalor": "Taller"},{"resid": 276, "estado": '+tradicional12+', "resvalor": "Vivienda"}]';

  var cactual_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+actual1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+actual2+', "resvalor": "Culto"},{"resid": 280, "estado": '+actual3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+actual4+', "resvalor": "Industria"},{"resid": 282, "estado": '+actual5+', "resvalor": "Militar"},{"resid": 281, "estado": '+actual6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+actual7+', "resvalor": "Salud"},{"resid": 278, "estado": '+actual8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+actual9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+actual10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+actual11+', "resvalor": "Taller"},{"resid": 276, "estado": '+actual12+', "resvalor": "Vivienda"}]';

  var ctendencia_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+tendencia1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+tendencia2+', "resvalor": "Culto"},{"resid": 280, "estado": '+tendencia3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+tendencia4+', "resvalor": "Industria"},{"resid": 282, "estado": '+tendencia5+', "resvalor": "Militar"},{"resid": 281, "estado": '+tendencia6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+tendencia7+', "resvalor": "Salud"},{"resid": 278, "estado": '+tendencia8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+tendencia9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+tendencia10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+tendencia11+', "resvalor": "Taller"},{"resid": 276, "estado": '+tendencia12+', "resvalor": "Vivienda"}]'; 

////////////////FORMULARIO BLOQUE////////////////////////////////
   var PTR_ID_BLOQ1 = '1';
   var PTR_DES_BLOQ1 = $("#cdescripcion").val();
   //var PTR_COD_CAT = '12DM';
   var PTR_RANG_EPO1 = document.getElementById('crango_epoca').value;
   var PTR_EST_BLOQ1 = document.getElementById('cestilo_bloque').value;
   var cepocaestilo = document.getElementById("cepocaestilocheck").checked;
   var csistemaconstructivo = document.getElementById("csistemaconstructivo").checked;
   var PTR_SIS_CONSTRUC1 = document.getElementById('csistema_constructivo_insertar').value; 
   var PTR_NRO_VIV1 = $("#cnro_viviendas").val(); 
   var PTR_NRO_HOG1 = $("#cnro_hogares").val(); 
   
   var cacapites1 = document.getElementById("cacapites1").checked;
   var cacapites2 = document.getElementById("cacapites2").checked;
   var cacapites3 = document.getElementById("cacapites3").checked;
   var cacapites4 = document.getElementById("cacapites4").checked;
   var cacapites5 = document.getElementById("cacapites5").checked;
   var cacapites6 = document.getElementById("cacapites6").checked;
   //-----------4 check Indicador Histórico (10% / 20%)------------------------------
   var cindicador_historico1 = document.getElementById("cindicador_historico1").checked;
   var cindicador_historico2 = document.getElementById("cindicador_historico2").checked;
   var cindicador_historico3 = document.getElementById("cindicador_historico3").checked;
   var cindicador_historico4 = document.getElementById("cindicador_historico4").checked;
   var cindicador_historico5 = document.getElementById("cindicador_historico5").checked;
   //----------- 5 check Indicador Artístico (6% / 15%)------------------------------
   var cindicador_artistico1 = document.getElementById("cindicador_artistico1").checked;
   var cindicador_artistico2 = document.getElementById("cindicador_artistico2").checked;
   var cindicador_artistico3 = document.getElementById("cindicador_artistico3").checked;
   //----------- 6 check Indicador Arquitectónico(4% / 15%)------------------------------
   var cindicador_arquitectonico1 = document.getElementById("cindicador_arquitectonico1").checked;
   var cindicador_arquitectonico2 = document.getElementById("cindicador_arquitectonico2").checked;
   //----------- 7 check Indicador Tecnológico (6% / 13%)------------------------------
   var cindicador_teclogico1 = document.getElementById("cindicador_teclogico1").checked;
   var cindicador_teclogico2 = document.getElementById("cindicador_teclogico2").checked;
   var cindicador_teclogico3 = document.getElementById("cindicador_teclogico3").checked;
   //----------- 8 check Indicador Integridad (4% / 15%)------------------------------
   var cindicador_integridad1 = document.getElementById("cindicador_integridad1").checked;
   var cindicador_integridad2 = document.getElementById("cindicador_integridad2").checked;
   var cindicador_integridad3 = document.getElementById("cindicador_integridad3").checked;
  //----------- 9 check Indicador Urbano  (9% / 15%)------------------------------
  var cindicador_urbano1 = document.getElementById("cindicador_urbano1").checked;
  var cindicador_urbano2 = document.getElementById("cindicador_urbano2").checked;
  var cindicador_urbano3 = document.getElementById("cindicador_urbano3").checked;
   //----------- 10 check Indicador Inmaterial------------------------------
   var cindicador_inmaterial1 = document.getElementById("cindicador_inmaterial1").checked;
   var cindicador_inmaterial2 = document.getElementById("cindicador_inmaterial2").checked;

   var PTR_EPO_EST1 = '[{"tipo": "CHK"},{"valor": '+cepocaestilo+'}]';
   //console.log('EPOCA Y ESTILO',PTR_EPO_EST1);

   var PTR_SIS_CONST1 = '[{"tipo": "CHK"},{"valor": '+csistemaconstructivo+'}]';
   //console.log('SISTEMA CONSTRUCTIVO',PTR_SIS_CONST1);

   var PTR_ACAP1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cacapites1+', "resvalor": "Elementos Estructurales (8%)"},{"resid": 285, "estado": '+cacapites2+', "resvalor": "Elementos Compositivos (6%)"},{"resid": 280, "estado": '+cacapites3+', "resvalor": "Acabados (3%)"},{"resid": 283, "estado": '+cacapites4+', "resvalor": "Elementos Artisiticos Compositivos (3%)"},{"resid": 282, "estado": '+cacapites5+', "resvalor": "Elementos Ornamentales (4%)"},{"resid": 281, "estado": '+cacapites6+', "resvalor": "Elementos Tipologicos Compositivos (9%)"}]';
   //console.log('ACAPITES',PTR_ACAP1);

   var PTR_VAL_HIST1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_historico1+', "resvalor": "Inmueble que posee un valor testimonial y documental, que ilustra el desarrollo político, social, religioso, cultural, económico y la forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 285, "estado": '+cindicador_historico2+', "resvalor": "Inmueble que desde su creación se constituye en un hito en la memoria historica"},{"resid": 280, "estado": '+cindicador_historico3+', "resvalor": "Inmueble que en el transcurso del tiempo se constituyo en hito en la memoria historica, al ser escenario relacionado con personas o eventos importantes, dentro del proceso Histórico,social, cultural, económico de las comunidades"},{"resid": 283, "estado": '+cindicador_historico4+', "resvalor": "Inmueble que forma parte de un conjunto Histórico"},{"resid": 282, "estado": '+cindicador_historico5+', "resvalor": "Inmueble que posee elementos arqueológicos o testimonios arquitectonicos de construcciones preexistentes que documentan su evolución y desarrollo"}]';
   //console.log('INDICADOR HISTÓRICO',PTR_VAL_HIST1);

   var PTR_VAL_ART1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_artistico1+', "resvalor": "Inmueble que se constituye en un ejemplo sobresaliente por su singularidad arquitectónica/artística"},{"resid": 285, "estado": '+cindicador_artistico2+', "resvalor": "Inmueble que conserva elementos arquitectónicos y tradicionales de interés"},{"resid": 280, "estado": '+cindicador_artistico3+', "resvalor": "Inmueble poseedor de expresiones artísticas y decorativas de interés"}]';
   //console.log('INDICADOR ARTÍSTICO',PTR_VAL_ART1);


   var PTR_VAL_ARQ1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_arquitectonico1+', "resvalor": "Inmueble poseedor de características Tipológicas representativas de estilos arquitectónicos"},{"resid": 285, "estado": '+cindicador_arquitectonico2+', "resvalor": "Inmueble con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalles"}]';
   //console.log('INDICADOR ARQUITECTÓNICO',PTR_VAL_ARQ1);


   var PTR_VAL_TEC1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_teclogico1+', "resvalor": "Inmueble que se constituye en un exponente de las técnicas constructivas y uso de materiales característicos de una época o región determinada"},{"resid": 285, "estado": '+cindicador_teclogico2+', "resvalor": "Inmueble donde la aplicación de técnicas constructivas son singulares o de especial interés"},{"resid": 280, "estado": '+cindicador_teclogico3+', "resvalor":"Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
   //console.log('INDICADOR TECNOLÓGICO',PTR_VAL_TEC1);

   var PTR_VAL_INTE1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_integridad1+', "resvalor": "Inmueble que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 285, "estado": '+cindicador_integridad2+', "resvalor": "Inmueble o espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"},{"resid": 280, "estado": '+cindicador_integridad3+', "resvalor": "Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
   //console.log('INDICADOR ITEGRIDAD',PTR_VAL_INTE1);

   var PTR_VAL_URB1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_urbano1+', "resvalor": "Inmueble que contribuye a definir un entorno de valor por su configuración y calidad en su estructura urbanística, el paisaje y/o el espacio público"},{"resid": 285, "estado": '+cindicador_urbano2+', "resvalor": "Inmueble que forma un perfil homogéneo y/ó armónico"},{"resid": 280, "estado": '+cindicador_urbano3+', "resvalor": "Inmueble considerado hito de referencia por su emplazamiento"}]';
   //console.log('INDICADOR URBANO',PTR_VAL_URB1);

   var PTR_VALORES_1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_inmaterial1+', "resvalor": "INDICADOR INMATERIAL (2% / 7%) Inmueble relacionado con la organización social, forma de vida, usos, representaciones, expresiones, conocimientos y técnicas que las comunidades y grupos sociales reconocen como parte de patrimonio"},{"resid": 285, "estado": '+cindicador_inmaterial2+', "resvalor": "INDICADOR SIMBÓLICO (1% / 5%) Inmueble o espacio abierto único que expresa una significación cultural, representa identidad y pertenencia para el colectivo"}]';
   //console.log('INDICADOR INMATERIAL',PTR_VALORES_1);

  //----------------------------------------------------------------------
  var cindicador_historico3 = $("#cindicador_historico").val();
  var PTR_VAL_HIS_DESC1 = caracteresEspecialesRegistrar(cindicador_historico3);
  var cindicador_atistico3 = $("#cindicador_atistico").val();
  var PTR_VAL_ART_DESC1 = caracteresEspecialesRegistrar(cindicador_atistico3);
  var cindicador_arquitectonico3 = $("#cindicador_arquitectonico").val();
  var PTR_VAL_ARQ_DESC1 = caracteresEspecialesRegistrar(cindicador_arquitectonico3);
  var cindicador_teclogico3 = $("#cindicador_teclogico").val();
  var PTR_VAL_TEC_DESC1 = caracteresEspecialesRegistrar(cindicador_teclogico3);
  var cindicador_integrid3 = $("#cindicador_integrid").val();
  var PTR_VAL_INTE_DESC1 = caracteresEspecialesRegistrar(cindicador_integrid3);
  var cindicador_urbano3 = $("#cindicador_urbano").val();
  var PTR_VAL_URB_DESC1 = caracteresEspecialesRegistrar(cindicador_urbano3);
  var cindicador_inmaterial3 = $("#cindicador_inmaterial").val();
  var PTR_VAL_INMAT_DESC1 = caracteresEspecialesRegistrar(cindicador_inmaterial3);
  var cindicador_simbolico3 = $("#cindicador_simbolico").val();
  var PTR_VAL_SIMB_DESC1 = caracteresEspecialesRegistrar(cindicador_simbolico3);
  var cindicador_interior3 = $("#cindicador_interior").val();
  var PTR_VAL_INT_DESC1 = caracteresEspecialesRegistrar(cindicador_interior3);
  var cdescripcion_exterior3 = $("#cdescripcion_exterior").val();
  var PTR_VAL_EXT_DESC1 = caracteresEspecialesRegistrar(cdescripcion_exterior3);
  var PTR_CATEGORIA1 = $("#ccategoria_inmueble").val();
  var PTR_PUNTO = $("#cpuntuacion").val();
  var PTR_ID_BLOQ1 = $("#cid_bloque").val();

  var cas_data_in ='{"g_tipo":"'+g_tipo+'","PTR_COD_CAT":"'+ccodCatastral_in+'","PTR_COD_CAT_HIS1":"'+ccodCatastral1_in+'","PTR_OR":"'+coriginal_in+'","PTR_TRAD":"'+ctradicional_in+'","PTR_ACT":"'+cActual_in+'","PTR_MACRO":"'+cMacrodistrito_in+'","PTR_TIP_VIAS":"'+ctipoVia_in+'","PTR_CALL_ESQ":"'+ccalleEsquina_in+'","PTR_DIRE":"'+cdireccion_in+'","PTR_ZONA":"'+czona_in+'","PTR_NUM1":"'+cnumero_in+'","PTR_REG_PROPI":"'+cnombrePropietario_in+'","PTR_REG_TIPO_PROPI":"'+tipoPropiedad_in+'","PTR_ARE_PRED":"'+careaPredio_in+'","PTR_ANCH_FREN":"'+canchoFrente_in+'","PTR_FOND_PRED":"'+cfondoPredio_in+'","PTR_ANCH_MUR":"'+canchoMuro_in+'","PTR_ALT_PROM":"'+calturaInterior_in+'","PTR_ALT_FACH":"'+calturaFachada_in+'","PTR_ALT_EDIF":"'+calturaMaxima_in+'","PTR_USO_TRAD":'+ctradicional_check+',"PTR_USO_ACT":'+cactual_check+',"PTR_USO_TEN":'+ctendencia_check+',"PTR_DES_PRED":"'+cdescripcion_in+'","PTR_UBIC_MAN":"'+cubicacionManzana_in+'","PTR_LIN_CONS":"'+clineaConstruccion_in+'","PTR_TRAZ_MAN":"'+ctrazoManzana_in+'","PTR_TIP_ARQ":"'+ctipologiaArqui_in+'","PTR_AER_EDIF":"'+careaEdificable_in+'","PTR_MAT_VIA":"'+cmaterialVia_in+'","PTR_TIP_VIA":"'+ctipoVia_in+'","PTR_BLOQ_INV":"'+cbloqueInventario_in+'","PTR_HOG_REAL":"'+cbloquesReales_in+'","PTR_HOG_INV":"'+chogaresInventario_in+'","PTR_HOG_REAL":"'+chogaresReales_in+'","PTR_EST_GEN":"'+cestiloGeneral_in+'","PTR_RAN_EPO":"'+crangoEpoca_in+'","PTR_EST_CON":"'+cestadoConserva_in+'","PTR_FIG_PROT":'+cgrillaFiguraProtec+',"PTR_FECH_CONS":"'+cfechaConstruccion_in+'","PTR_AUT_CON":"'+cautor_in+'","PTR_PREEX_EDIF":"'+cpreexistencia_in+'","PTR_PROP_ANT":"'+cpropietarios_in+'","PTR_USO_ORG":"'+cusoOriginal_in+'","PTR_HEC_HIST":"'+chechoHistorico_in+'","PTR_FIE_REL":"'+cfiestaReligiosa_in+'","PTR_DAT_HIST_CONJ":"'+cdatoHistorico_in+'","PTR_FUENT_DOC":"'+cfuente_in+'","PTR_FEC_CONST_DOC":"'+cfechaConstruccion_in3+'","PTR_AUT_CONS":"'+cautor_in3+'","PTR_PREEX_INTER":"'+cpreexistencia_in3+'","PTR_PROP_ANT_DOC":"'+cpropietarios_in3+'","PTR_USOS_ORG_DOC":"'+cusoOriginal_in3+'","PTR_HEC_HIS_DOC":"'+chechoHistorico_in3+'","PTR_FIE_REL_DOC":"'+cfiestaReligiosa_in3+'","PTR_DAT_HIS_DOC":"'+cdatoHistorico_in3+'","PTR_FUENTE_DOC":"'+cfuente_in3+'","PTR_INSCRIP":"'+cinscripcion_in+'","PTR_EXIST":"'+cexistePatrimonio_in+'","PTR_EXIST_AR":"'+cexisteResto_in+'","PTR_EXIST_ELEM":"'+cexisteElemento_in+'","PTR_EXIST_RITO":"'+critosMitos_in+'","PTR_CONJUNTO":"'+cconjunto_in+'","PTR_EST_VERF":"'+cestadoVerifica_in+'","PTR_VAL_CAT":"'+cpuntuacion_in+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'","PTR_DES_BLOQ1":"'+PTR_DES_BLOQ1+'","PTR_RANG_EPO1":"'+PTR_RANG_EPO1+'","PTR_EST_BLOQ1":"'+PTR_EST_BLOQ1+'","PTR_EPO_EST1":'+PTR_EPO_EST1+',"PTR_SIS_CONST1":'+PTR_SIS_CONST1+',"PTR_SIS_CONSTRUC1":"'+PTR_SIS_CONSTRUC1+'","PTR_NRO_VIV1":"'+PTR_NRO_VIV1+'","PTR_NRO_HOG1":"'+PTR_NRO_HOG1+'","PTR_ACAP1":'+PTR_ACAP1+',"PTR_VAL_HIST1":'+PTR_VAL_HIST1+',"PTR_VAL_ART1":'+PTR_VAL_ART1+',"PTR_VAL_ARQ1":'+PTR_VAL_ARQ1+',"PTR_VAL_TEC1":'+PTR_VAL_TEC1+',"PTR_VAL_INTE1":'+PTR_VAL_INTE1+',"PTR_VAL_URB1":'+PTR_VAL_URB1+',"PTR_VALORES_1":'+PTR_VALORES_1+',"PTR_VAL_HIS_DESC1":"'+PTR_VAL_HIS_DESC1+'","PTR_VAL_ART_DESC1":"'+PTR_VAL_ART_DESC1+'","PTR_VAL_ARQ_DESC1":"'+PTR_VAL_ARQ_DESC1+'","PTR_VAL_TEC_DESC1":"'+PTR_VAL_TEC_DESC1+'","PTR_VAL_INTE_DESC1":"'+PTR_VAL_INTE_DESC1+'","PTR_VAL_URB_DESC1":"'+PTR_VAL_URB_DESC1+'","PTR_VAL_INMAT_DESC1":"'+PTR_VAL_INMAT_DESC1+'","PTR_VAL_SIMB_DESC1":"'+PTR_VAL_SIMB_DESC1+'","PTR_VAL_INT_DESC1":"'+PTR_VAL_INT_DESC1+'","PTR_VAL_EXT_DESC1":"'+PTR_VAL_EXT_DESC1+'","PTR_REG_PROPI":'+cgrillaRegimenPropietario+',"PTR_REF_HIST":'+cgrillaRefHist+',"PTR_PINT_MUR_EXT":'+cgrillaMuroExt+',"PTR_PINT_MUR_INT":'+cgrillaMuroInt+',"PTR_ACA_MUR_EXT":'+cgrillaAcabadoEx+',"PTR_ACA_MUR_INT":'+cgrillaAcabadoInt+',"PTR_PEL_POT1":'+PTR_PEL_POT1+',"PTR_PAT_EDIF1":'+PTR_PAT_EDIF1+',"PTR_INST_EST1":'+PTR_INST_EST1+',"PTR_SERVICIOS1":'+PTR_SERVICIOS1+', "PTR_ELEM_ART_COMP":'+cgrillaFiguraProtecTIC+', "PTR_ELEM_ORN_DEC":'+cgrillaFiguraProtecOR+', "PTR_ELEM_TIP_COMP":'+cgrillaFiguraProtecAR+', "PTR_AG_HOGARES":'+cgrillaFiguraProtecHogar+',"PTR_CATEGORIA1":"'+PTR_CATEGORIA1+'","PTR_PUNTO":"'+PTR_PUNTO+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'"}';



  var jdata_m = JSON.stringify(cas_data_in);
  ////////console.log("jdataaaa",jdata_m);
  var xcas_nro_caso =correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = 'asunto';
  var xcas_tipo_hr = 'hora';
  var xcas_id_padre = 1;
  var xcampo_f = 'campo'; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_m+',"xcas_nombre_caso":"'+ccodPatrimonio_in+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

  $.ajax({
    type        : 'GET',
    url         :  '/prueba/VALLE/public/v0.0/getToken',
    data        : '',
    success: function(token) {

    //var ccodPatrimonio = $("#ccodPatrimonio").val();
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },                  
     success: function(data){ 

      ////////console.log("dataaaaINmuebles",data);
      listarPatrimonioCultural(g_tipo);
      swal( "Exíto..!", "Se registro correctamente los datos de Bienes Inmuebles...!", "success" );
      //$( "#formBienesArqM_create").data('bootstrapValidator').resetForm();
      //$( "#myCreateBienesArqMueble" ).modal('toggle');
      var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
        url = url.replace('casosId1', idCasos);
        //console.log("url",url);
        location.href = url;
      }, 1000); 
      
    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500); 
});

function editarBienesInmuebles(idCaso,nroFicha,codCatastral, codCatastral1,original, tradicionalDenominacion, actualDenominacion, macro,tipoVia, entreCalles,direccion,zona,numero, nombPropietario,tipoPropietario,area,anchoFrente,fondo,anchoMuro,alturaInterior,alturaFachada,alturaMaximaEdif,tradicionalSuelo,actualSuelo, tendencia,decsPredio,ubicaManzana,lineaConst,trazoManzan,tipologia,areaEdificable,materialVia,tipoVia,bloInventario,bloReal,hogarInventario,hogarReal,estiloGeneral,rangoEpoca,estadoconser,fechaCD,autor,preexistencia,propietarioA,usoOriginal,hechoHisto,fiestaReligiosa,datoConjunto,fuente,fechaCD3,autor3,preexistencia3,propietarioA3,usoOriginal3,hechoHisto3,fiestaReligiosa3,datoConjunto3,fuente3,inscrip,existePatrimonio,existeRestos,existeElemento,ritosMitos,conjunto,estadoVerifi,puntuacion, IDbloeuque, descripcion, rangoepoca, estilobloque, epocaestilocheck, sistemaconstructivocheck, sistemaconstructivo2, nrovivienda, nrohogares, aacapites, aidicadorhistorico, aiartistico, aiarquitectonico, aitecnologico, aiintegridad, iurbano, iinmaterial, indicadorhistorico2, indicadorartistico2,indicadorarquitectonico1, indicadortecnologico1, indicadorintegridad1, indicadorurbano1, indicadorinmaterial1, indicadorsimbolico1, descripcioninterior, descripcioexterior,categoriaBloque,categoriaPunto,nroBloque,grillaFiguraProtec,grillaReferenciaHistorica)
{
  var decsPredio = caracteresEspecialesActualizar(decsPredio);
  var preexistencia = caracteresEspecialesActualizar(preexistencia);
  var propietarioA = caracteresEspecialesActualizar(propietarioA);
  var usoOriginal = caracteresEspecialesActualizar(usoOriginal);
  var hechoHisto = caracteresEspecialesActualizar(hechoHisto);
  var fiestaReligiosa = caracteresEspecialesActualizar(fiestaReligiosa);
  var datoConjunto = caracteresEspecialesActualizar(datoConjunto);
  var fuente = caracteresEspecialesActualizar(fuente);
  var preexistencia3 = caracteresEspecialesActualizar(preexistencia3);
  var propietarioA3 = caracteresEspecialesActualizar(propietarioA3);
  var usoOriginal3 = caracteresEspecialesActualizar(usoOriginal3);
  var hechoHisto3 = caracteresEspecialesActualizar(hechoHisto3);
  var fiestaReligiosa3 = caracteresEspecialesActualizar(fiestaReligiosa3);
  var datoConjunto3 = caracteresEspecialesActualizar(datoConjunto3);
  var fuente3 = caracteresEspecialesActualizar(fuente3);
  var inscrip = caracteresEspecialesActualizar(inscrip);
  var existePatrimonio = caracteresEspecialesActualizar(existePatrimonio);
  var existeRestos = caracteresEspecialesActualizar(existeRestos);
  var existeElemento = caracteresEspecialesActualizar(existeElemento);
  var ritosMitos = caracteresEspecialesActualizar(ritosMitos);

  $("#acasoId_in").val(idCaso);
  $("#anroficha_in").val(nroFicha);
  $("#acodCatastral_in").val(codCatastral);
  $("#acodCatastral1_in").val(codCatastral1);
  $("#aoriginal_in").val(original);

  $("#atradicional_in1").val(tradicionalDenominacion);
  $("#aActual_in").val(actualDenominacion);
  $("#aMacrodistrito_in").val(macro);
  $("#atipoVia_in").val(tipoVia);
  $("#acalleEsquina_in").val(entreCalles);
  $("#adireccion_in").val(direccion);
  $("#azona_in").val(zona);
  $("#anumero_in").val(numero);
  $("#anombrePropietario_in").val(nombPropietario);
  $("#atipoPropiedad_in").val(tipoPropietario);
  $("#aareaPredio_in").val(area);
  $("#aanchoFrente_in").val(anchoFrente);
  $("#afondoPredio_in").val(fondo);
  $("#aanchoMuro_in").val(anchoMuro);
  $("#aalturaInterior_in").val(alturaInterior);
  $("#aalturaFachada_in").val(alturaFachada);
  $("#aalturaMaxima_in").val(alturaMaximaEdif);
  $("#afiguraProtec").val(grillaFiguraProtec);
  $("#areferenciaHistorica").val(grillaReferenciaHistorica);

  var tradicionalAct = tradicionalSuelo.split(',');
  //////console.log("tradicionalAct",tradicionalAct);

  document.getElementById("atradicional1").checked = getBool(tradicionalAct[0]);
  document.getElementById("atradicional2").checked = getBool(tradicionalAct[1]);
  document.getElementById("atradicional3").checked = getBool(tradicionalAct[2]);
  document.getElementById("atradicional4").checked = getBool(tradicionalAct[3]);
  document.getElementById("atradicional5").checked = getBool(tradicionalAct[4]);
  document.getElementById("atradicional6").checked = getBool(tradicionalAct[5]);
  document.getElementById("atradicional7").checked = getBool(tradicionalAct[6]);
  document.getElementById("atradicional8").checked = getBool(tradicionalAct[7]);
  document.getElementById("atradicional9").checked = getBool(tradicionalAct[8]);
  document.getElementById("atradicional10").checked = getBool(tradicionalAct[9]);
  document.getElementById("atradicional11").checked = getBool(tradicionalAct[10]);
  document.getElementById("atradicional12").checked = getBool(tradicionalAct[11]);

  var actualAct = actualSuelo.split(',');
    //////console.log("actualAct",actualAct);
    document.getElementById("aactual1").checked = getBool(actualAct[0]);
    document.getElementById("aactual2").checked = getBool(actualAct[1]);
    document.getElementById("aactual3").checked = getBool(actualAct[2]);
    document.getElementById("aactual4").checked = getBool(actualAct[3]);
    document.getElementById("aactual5").checked = getBool(actualAct[4]);
    document.getElementById("aactual6").checked = getBool(actualAct[5]);
    document.getElementById("aactual7").checked = getBool(actualAct[6]);
    document.getElementById("aactual8").checked = getBool(actualAct[7]);
    document.getElementById("aactual9").checked = getBool(actualAct[8]);
    document.getElementById("aactual10").checked = getBool(actualAct[9]);
    document.getElementById("aactual11").checked = getBool(actualAct[10]);
    document.getElementById("aactual12").checked = getBool(actualAct[11]);

    var tendenciaAct = tendencia.split(',');
    //////console.log("tendenciaAct",tendenciaAct);
    document.getElementById("atendencia1").checked = getBool(tendenciaAct[0]);
    document.getElementById("atendencia2").checked = getBool(tendenciaAct[1]);
    document.getElementById("atendencia3").checked = getBool(tendenciaAct[2]);
    document.getElementById("atendencia4").checked = getBool(tendenciaAct[3]);
    document.getElementById("atendencia5").checked = getBool(tendenciaAct[4]);
    document.getElementById("atendencia6").checked = getBool(tendenciaAct[5]);
    document.getElementById("atendencia7").checked = getBool(tendenciaAct[6]);
    document.getElementById("atendencia8").checked = getBool(tendenciaAct[7]);
    document.getElementById("atendencia9").checked = getBool(tendenciaAct[8]);
    document.getElementById("atendencia10").checked = getBool(tendenciaAct[9]);
    document.getElementById("atendencia11").checked = getBool(tendenciaAct[10]);
    document.getElementById("atendencia12").checked = getBool(tendenciaAct[11]);

  //$("#aactualSuelo_in").val(actualSuelo);
  //$("#atendencia_in").val(tendencia);
  $("#adescripcion_in").val(decsPredio);
  $("#aubicacionManzana_in").val(ubicaManzana);
  $("#alineaConstruccion_in").val(lineaConst);
  $("#atrazoManzana_in").val(trazoManzan);
  $("#atipologiaArqui_in").val(tipologia);
  $("#aareaEdificable_in").val(areaEdificable);
  $("#amaterialVia_in").val(materialVia);
  $("#atipoVia_in").val(tipoVia);
  $("#abloqueInventario_in").val(bloInventario);
  $("#abloquesReales_in").val(bloReal);
  $("#ahogaresInventario_in").val(hogarInventario);
  $("#ahogaresReales_in").val(hogarReal);
  $("#aestiloGeneral_in").val(estiloGeneral);
  $("#arangoEpoca_in").val(rangoEpoca);
  $("#aestadoConserva_in").val(estadoconser);
  $("#afechaConstruccion_in").val(fechaCD);
  $("#aautor_in").val(autor);
  $("#apreexistencia_in").val(preexistencia);
  $("#apropietarios_in").val(propietarioA);
  $("#ausoOriginal_in").val(usoOriginal);
  $("#ahechoHistorico_in").val(hechoHisto);
  $("#afiestaReligiosa_in").val(fiestaReligiosa);
  $("#adatoHistorico_in").val(datoConjunto);
  $("#afuente_in").val(fuente);
  $("#afechaConstruccion_in3").val(fechaCD3);
  $("#aautor_in3").val(autor3);
  $("#apreexistencia_in3").val(preexistencia3);
  $("#apropietarios_in3").val(propietarioA3);
  $("#ausoOriginal_in3").val(usoOriginal3);
  $("#ahechoHistorico_in3").val(hechoHisto3);
  $("#afiestaReligiosa_in3").val(fiestaReligiosa3);
  $("#adatoHistorico_in3").val(datoConjunto3);
  $("#afuente_in3").val(fuente3);
  $("#ainscripcion_in").val(inscrip);
  $("#aexistePatrimonio_in").val(existePatrimonio);
  $("#aexisteResto_in").val(existeRestos);
  $("#aexisteElemento_in").val(existeElemento);
  $("#aritosMitos_in").val(ritosMitos);
  $("#aconjunto_in").val(conjunto);
  $("#aestadoVerifica_in").val(estadoVerifi);
  $("#apuntuacion_in").val(puntuacion);
  //----------------------------------------------
  $("#aid_bloque").val(IDbloeuque);
  $("#adescripcion").val(descripcion);
  //$("#acodigo_catastral").val(codCatastral);
  $("#arango_epoca").val(rangoepoca);
  $("#aestilo_bloque").val(estilobloque);
  document.getElementById("aepocaestilocheck").checked = epocaestilocheck;//var PTR_EPO_EST1
  document.getElementById("asistemaconstructivocheck").checked = sistemaconstructivocheck;//var PTR_SIS_CONST1
  $("#asistema_constructivo").val(sistemaconstructivo2);
  //console.log(sistemaconstructivo2,'sistemaconstructivo2 sistemaconstructivo2');
  $("#anro_viviendas").val(nrovivienda);
  $("#anro_hogares").val(nrohogares);

  var aacapites = aacapites.split(',');
  //console.log('ACAPITES EDITAR',aacapites);
  document.getElementById("aacapites1").checked = getBool(aacapites[0]);
  document.getElementById("aacapites2").checked = getBool(aacapites[1]);
  document.getElementById("aacapites3").checked = getBool(aacapites[2]);
  document.getElementById("aacapites4").checked = getBool(aacapites[3]);
  document.getElementById("aacapites5").checked = getBool(aacapites[4]);
  document.getElementById("aacapites6").checked = getBool(aacapites[5]);

  var aidicadorhistorico = aidicadorhistorico.split(',');
  document.getElementById("aindicador_historico1").checked = getBool(aidicadorhistorico[0]);
  document.getElementById("aindicador_historico2").checked = getBool(aidicadorhistorico[1]);
  document.getElementById("aindicador_historico3").checked = getBool(aidicadorhistorico[2]);
  document.getElementById("aindicador_historico4").checked = getBool(aidicadorhistorico[3]);
  document.getElementById("aindicador_historico5").checked = getBool(aidicadorhistorico[4]);

  var aiartistico = aiartistico.split(',');
  document.getElementById("aindicador_artistico1").checked = getBool(aiartistico[0]);
  document.getElementById("aindicador_artistico2").checked = getBool(aiartistico[1]);
  document.getElementById("aindicador_artistico3").checked = getBool(aiartistico[2]);

  var aiarquitectonico = aiarquitectonico.split(',');
  document.getElementById("aindicador_arquitectonico1").checked = getBool(aiarquitectonico[0]);
  document.getElementById("aindicador_arquitectonico2").checked = getBool(aiarquitectonico[1]);
  
  var aitecnologico = aitecnologico.split(',');
  document.getElementById("aindicador_teclogico1").checked = getBool(aitecnologico[0]);
  document.getElementById("aindicador_teclogico2").checked = getBool(aitecnologico[1]);
  document.getElementById("aindicador_teclogico3").checked = getBool(aitecnologico[2]);
  
  var aiintegridad = aiintegridad.split(',');
  document.getElementById("aindicador_integridad1").checked = getBool(aiintegridad[0]);
  document.getElementById("aindicador_integridad2").checked = getBool(aiintegridad[1]);
  document.getElementById("aindicador_integridad3").checked = getBool(aiintegridad[2]);
  
  var iurbano = iurbano.split(',');
  document.getElementById("aindicador_urbano1").checked = getBool(iurbano[0]);
  document.getElementById("aindicador_urbano2").checked = getBool(iurbano[1]);
  document.getElementById("aindicador_urbano3").checked = getBool(iurbano[2]);

  var iinmaterial = iinmaterial.split(',');
  document.getElementById("aindicador_inmaterial1").checked = getBool(iinmaterial[0]);
  document.getElementById("aindicador_inmaterial1").checked = getBool(iinmaterial[1]);

  var indicadorhistorico2 = caracteresEspecialesActualizar(indicadorhistorico2);
  var indicadorartistico2 = caracteresEspecialesActualizar(indicadorartistico2);
  var indicadorarquitectonico1 = caracteresEspecialesActualizar(indicadorarquitectonico1);
  var indicadortecnologico1 = caracteresEspecialesActualizar(indicadortecnologico1);
  var indicadorintegridad1 = caracteresEspecialesActualizar(indicadorintegridad1);
  var indicadorurbano1 = caracteresEspecialesActualizar(indicadorurbano1);
  var indicadorinmaterial1 = caracteresEspecialesActualizar(indicadorinmaterial1);
  var indicadorsimbolico1 = caracteresEspecialesActualizar(indicadorsimbolico1);
  var descripcioninterior = caracteresEspecialesActualizar(descripcioninterior);
  var descripcioexterior = caracteresEspecialesActualizar(descripcioexterior);

  $("#aindicador_historico").val(indicadorhistorico2);
  $("#aindicador_atistico").val(indicadorartistico2);
  $("#aindicador_arquitectonico").val(indicadorarquitectonico1);
  $("#aindicador_teclogico").val(indicadortecnologico1);
  $("#aindicador_integrid").val(indicadorintegridad1);
  $("#aindicador_urbano").val(indicadorurbano1);
  $("#aindicador_inmaterial").val(indicadorinmaterial1);
  $("#aindicador_simbolico").val(indicadorsimbolico1);
  $("#aindicador_interior").val(descripcioninterior);
  $("#adescripcion_exterior").val(descripcioexterior);

  $("#acategoria_inmueble").val(categoriaBloque); 
  $("#apuntuacion").val(categoriaPunto);
  $("#aid_bloque").val(nroBloque);
  $("#acodigo_catastral").val(codCatastral);  
  
};


$( "#actualizarBienesInmuebles" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

  var acasoId_in = $( "#acasoId_in" ).val();
  var anroficha_in = $( "#anroficha_in" ).val();  
  var acodPatrimonio_in = 'PTR_CBI'+(anroficha_in)+'/2018';
  var acodCatastral_in = $("#acodCatastral_in").val();
  console.log("acodCatastral_in",acodCatastral_in);
  var acodCatastral1_in = $("#acodCatastral1_in").val();
  var aoriginal_in = $("#aoriginal_in").val();
  var atradicional_in1 = $("#atradicional_in1").val();
  var aActual_in = $("#aActual_in").val();
  var aMacrodistrito_in = document.getElementById('aMacrodistrito_in').value;
  var atipoVia_in = document.getElementById('atipoVia_in').value;
  var acalleEsquina_in = $("#acalleEsquina_in").val();
  var adireccion_in = $("#adireccion_in").val();
  var azona_in = $("#azona_in").val();
  var anumero_in = $("#anumero_in").val();
  var anombrePropietario_in = $("#anombrePropietario_in").val();
  var atipoPropiedad_in = document.getElementById('atipoPropiedad_in').value;
  var aareaPredio_in = $("#aareaPredio_in").val();
  var aanchoFrente_in = $("#aanchoFrente_in").val();
  var afondoPredio_in = $("#afondoPredio_in").val();
  var aanchoMuro_in = $("#aanchoMuro_in").val();
  var aalturaInterior_in = $("#aalturaInterior_in").val();
  var aalturaFachada_in = $("#aalturaFachada_in").val();
  var aalturaMaxima_in = $("#aalturaMaxima_in").val();
  var adescripcion_in = $("#adescripcion_in").val();
  var aubicacionManzana_in = document.getElementById('aubicacionManzana_in').value;
  var alineaConstruccion_in = document.getElementById('alineaConstruccion_in').value;
  var atrazoManzana_in = document.getElementById('atrazoManzana_in').value;
  var atipologiaArqui_in = document.getElementById('atipologiaArqui_in').value;
  var aareaEdificable_in = document.getElementById('aareaEdificable_in').value;
  var amaterialVia_in = document.getElementById('amaterialVia_in').value;
  var atipoVia_in = document.getElementById('atipoVia_in').value;
  //var abloqueInventario_in = $("#abloqueInventario_in").val();
  var abloqueInventario_in = document.getElementById('abloqueInventario_in').value;
  var abloquesReales_in = $("#abloquesReales_in").val();
  var ahogaresInventario_in = $("#ahogaresInventario_in").val();
  var ahogaresReales_in = $("#ahogaresReales_in").val();
  var aestiloGeneral_in = document.getElementById('aestiloGeneral_in').value;
  var arangoEpoca_in = document.getElementById('arangoEpoca_in').value;
  var aestadoConserva_in = document.getElementById('aestadoConserva_in').value;
  var afechaConstruccion_in = $("#afechaConstruccion_in").val();
  var aautor_in = $("#aautor_in").val();
  var apreexistencia_in = $("#apreexistencia_in").val();
  var apropietarios_in = $("#apropietarios_in").val();
  var ausoOriginal_in = $("#ausoOriginal_in").val();
  var ahechoHistorico_in = $("#ahechoHistorico_in").val();
  var afiestaReligiosa_in = $("#afiestaReligiosa_in").val();
  var adatoHistorico_in = $("#adatoHistorico_in").val();
  var afuente_in = $("#afuente_in").val();
  var afechaConstruccion_in3 = $("#afechaConstruccion_in3").val();
  var aautor_in3 = $("#aautor_in3").val();
  var apreexistencia_in3 = $("#apreexistencia_in3").val();
  var apropietarios_in3 = $("#apropietarios_in3").val();
  var ausoOriginal_in3 = $("#ausoOriginal_in3").val();
  var ahechoHistorico_in3 = $("#ahechoHistorico_in3").val();
  var afiestaReligiosa_in3 = $("#afiestaReligiosa_in3").val();
  var adatoHistorico_in3 = $("#adatoHistorico_in3").val();
  var afuente_in3 = $("#afuente_in3").val();
  var ainscripcion_in= $("#ainscripcion_in").val();
  var aexistePatrimonio_in = $("#aexistePatrimonio_in").val();
  var aexisteResto_in = $("#aexisteResto_in").val();
  var aexisteElemento_in = $("#aexisteElemento_in").val();
  var aritosMitos_in = $("#aritosMitos_in").val();
  var aconjunto_in = document.getElementById('aconjunto_in').value;
  var aestadoVerifica_in = document.getElementById('aestadoVerifica_in').value;
  var apuntuacion_in = $("#apuntuacion_in").val();
  var g_tipo = 'PTR_CBI';

  var afiguraProtec = $("#afiguraProtec").val();
  //console.log("ANTESfiguraProtec",afiguraProtec);
  var afiguraProtec = reemBarrasPorComillas(afiguraProtec);
  //var afiguraProtec = JSON.parse(afiguraProtec);
  //console.log("afiguraProtec",afiguraProtec);

  var areferenciaHistorica = $("#areferenciaHistorica").val();
  //console.log("ANTESreferenciaHistorica",areferenciaHistorica);
  var areferenciaHistorica = reemBarrasPorComillas(areferenciaHistorica);
  //var areferenciaHistorica = JSON.parse(areferenciaHistorica);
  
  //console.log("areferenciaHistorica",areferenciaHistorica);

   var adescripcion_in = caracteresEspecialesRegistrar(adescripcion_in);
   var apreexistencia_in = caracteresEspecialesRegistrar(apreexistencia_in);
   var apropietarios_in = caracteresEspecialesRegistrar(apropietarios_in);
   var ausoOriginal_in = caracteresEspecialesRegistrar(ausoOriginal_in);
   var ahechoHistorico_in = caracteresEspecialesRegistrar(ahechoHistorico_in);
   var afiestaReligiosa_in = caracteresEspecialesRegistrar(afiestaReligiosa_in);
   var adatoHistorico_in = caracteresEspecialesRegistrar(adatoHistorico_in);
   var afuente_in = caracteresEspecialesRegistrar(afuente_in);
   var apreexistencia_in3 = caracteresEspecialesRegistrar(apreexistencia_in3);
   var apropietarios_in3 = caracteresEspecialesRegistrar(apropietarios_in3);
   var ausoOriginal_in3 = caracteresEspecialesRegistrar(ausoOriginal_in3);
   var ahechoHistorico_in3 = caracteresEspecialesRegistrar(ahechoHistorico_in3);
   var afiestaReligiosa_in3 = caracteresEspecialesRegistrar(afiestaReligiosa_in3);
   var adatoHistorico_in3 = caracteresEspecialesRegistrar(adatoHistorico_in3);
   var afuente_in3 = caracteresEspecialesRegistrar(afuente_in3);
   var ainscripcion_in = caracteresEspecialesRegistrar(ainscripcion_in);
   var aexistePatrimonio_in = caracteresEspecialesRegistrar(aexistePatrimonio_in);
   var aexisteResto_in = caracteresEspecialesRegistrar(aexisteResto_in);
   var aexisteElemento_in = caracteresEspecialesRegistrar(aexisteElemento_in);
   var aritosMitos_in = caracteresEspecialesRegistrar(aritosMitos_in);

   var atradicional1 = document.getElementById("atradicional1").checked;
   var atradicional2 = document.getElementById("atradicional2").checked;
   var atradicional3 = document.getElementById("atradicional3").checked;
   var atradicional4 = document.getElementById("atradicional4").checked;
   var atradicional5 = document.getElementById("atradicional5").checked;
   var atradicional6 = document.getElementById("atradicional6").checked;
   var atradicional7 = document.getElementById("atradicional7").checked;
   var atradicional8 = document.getElementById("atradicional8").checked;
   var atradicional9 = document.getElementById("atradicional9").checked;
   var atradicional10 = document.getElementById("atradicional10").checked;
   var atradicional11= document.getElementById("atradicional11").checked;
   var atradicional12 = document.getElementById("atradicional12").checked;

   var aactual1 = document.getElementById("aactual1").checked;
   var aactual2 = document.getElementById("aactual2").checked;
   var aactual3 = document.getElementById("aactual3").checked;
   var aactual4 = document.getElementById("aactual4").checked;
   var aactual5 = document.getElementById("aactual5").checked;
   var aactual6 = document.getElementById("aactual6").checked;
   var aactual7 = document.getElementById("aactual7").checked;
   var aactual8 = document.getElementById("aactual8").checked;
   var aactual9 = document.getElementById("aactual9").checked;
   var aactual10 = document.getElementById("aactual10").checked;
   var aactual11 = document.getElementById("aactual11").checked;
   var aactual12 = document.getElementById("aactual12").checked;

   var atendencia1 = document.getElementById("atendencia1").checked;
   var atendencia2 = document.getElementById("atendencia2").checked;
   var atendencia3 = document.getElementById("atendencia3").checked;
   var atendencia4 = document.getElementById("atendencia4").checked;
   var atendencia5 = document.getElementById("atendencia5").checked;
   var atendencia6 = document.getElementById("atendencia6").checked;
   var atendencia7 = document.getElementById("atendencia7").checked;
   var atendencia8 = document.getElementById("atendencia8").checked;
   var atendencia9 = document.getElementById("atendencia9").checked;
   var atendencia10 = document.getElementById("atendencia10").checked;
   var atendencia11 = document.getElementById("atendencia11").checked;
   var atendencia12 = document.getElementById("atendencia12").checked;

   var atradicional_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+atradicional1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+atradicional2+', "resvalor": "Culto"},{"resid": 280, "estado": '+atradicional3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+atradicional4+', "resvalor": "Industria"},{"resid": 282, "estado": '+atradicional5+', "resvalor": "Militar"},{"resid": 281, "estado": '+atradicional6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+atradicional7+', "resvalor": "Salud"},{"resid": 278, "estado": '+atradicional8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+atradicional9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+atradicional10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+atradicional11+', "resvalor": "Taller"},{"resid": 276, "estado": '+atradicional12+', "resvalor": "Vivienda"}]';

    var aactual_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aactual1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+aactual2+', "resvalor": "Culto"},{"resid": 280, "estado": '+aactual3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+aactual4+', "resvalor": "Industria"},{"resid": 282, "estado": '+aactual5+', "resvalor": "Militar"},{"resid": 281, "estado": '+aactual6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+aactual7+', "resvalor": "Salud"},{"resid": 278, "estado": '+aactual8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+aactual9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+aactual10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+aactual11+', "resvalor": "Taller"},{"resid": 276, "estado": '+aactual12+', "resvalor": "Vivienda"}]';

   var atendencia_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+atendencia1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+atendencia2+', "resvalor": "Culto"},{"resid": 280, "estado": '+atendencia3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+atendencia4+', "resvalor": "Industria"},{"resid": 282, "estado": '+atendencia5+', "resvalor": "Militar"},{"resid": 281, "estado": '+atendencia6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+atendencia7+', "resvalor": "Salud"},{"resid": 278, "estado": '+atendencia8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+atendencia9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+atendencia10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+atendencia11+', "resvalor": "Taller"},{"resid": 276, "estado": '+atendencia12+', "resvalor": "Vivienda"}]'; 
   //------------------------------------------------------------------------------------------------
      var PTR_ID_BLOQ1 = '1';
      var PTR_DES_BLOQ1 = $("#adescripcion").val();
     // var PTR_COD_CAT = '12DM';
      var PTR_RANG_EPO1 = document.getElementById('arango_epoca').value;
      var PTR_EST_BLOQ1 = document.getElementById('aestilo_bloque').value;
      var aepocaestilo2 = document.getElementById("aepocaestilocheck").checked;//var PTR_EPO_EST1
      var asistemaconstructivo2 = document.getElementById("asistemaconstructivocheck").checked;//var PTR_SIS_CONST1
      var PTR_EPO_EST1 = '[{"tipo": "CHK"},{"valor": '+aepocaestilo2+'}]';
      var PTR_SIS_CONST1 = '[{"tipo": "CHK"},{"valor": '+asistemaconstructivo2+'}]';
      var PTR_SIS_CONSTRUC1 = document.getElementById('asistema_constructivo').value;

     
      var PTR_NRO_VIV1 = $("#anro_viviendas").val();
      var PTR_NRO_HOG1 = $("#anro_hogares").val();
      
      var aacapites1 = document.getElementById("aacapites1").checked;
      var aacapites2 = document.getElementById("aacapites2").checked;
      var aacapites3 = document.getElementById("aacapites3").checked;
      var aacapites4 = document.getElementById("aacapites4").checked;
      var aacapites5 = document.getElementById("aacapites5").checked;
      var aacapites6 = document.getElementById("aacapites6").checked;

      var PTR_ACAP1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aacapites1+', "resvalor": "Elementos Estructurales (8%)"},{"resid": 285, "estado": '+aacapites2+', "resvalor": "Elementos Compositivos (6%)"},{"resid": 280, "estado": '+aacapites3+', "resvalor": "Acabados (3%)"},{"resid": 283, "estado": '+aacapites4+', "resvalor": "Elementos Artisiticos Compositivos (3%)"},{"resid": 282, "estado": '+aacapites5+', "resvalor": "Elementos Ornamentales (4%)"},{"resid": 281, "estado": '+aacapites6+', "resvalor": "Elementos Tipologicos Compositivos (9%)"}]';
         //console.log('ACAPITES',PTR_ACAP1);

      var aindicador_historico1 = document.getElementById("aindicador_historico1").checked;
      var aindicador_historico2 = document.getElementById("aindicador_historico2").checked;
      var aindicador_historico3 = document.getElementById("aindicador_historico3").checked;
      var aindicador_historico4 = document.getElementById("aindicador_historico4").checked;
      var aindicador_historico5 = document.getElementById("aindicador_historico5").checked;

      var PTR_VAL_HIST1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_historico1+', "resvalor": "Inmueble que posee un valor testimonial y documental, que ilustra el desarrollo político, social, religioso, cultural, económico y la forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 285, "estado": '+aindicador_historico2+', "resvalor": "Inmueble que desde su creación se constituye en un hito en la memoria historica"},{"resid": 280, "estado": '+aindicador_historico3+', "resvalor": "Inmueble que en el transcurso del tiempo se constituyo en hito en la memoria historica, al ser escenario relacionado con personas o eventos importantes, dentro del proceso Histórico,social, cultural, económico de las comunidades"},{"resid": 283, "estado": '+aindicador_historico4+', "resvalor": "Inmueble que forma parte de un conjunto Histórico"},{"resid": 282, "estado": '+aindicador_historico5+', "resvalor": "Inmueble que posee elementos arqueológicos o testimonios arquitectonicos de construcciones preexistentes que documentan su evolución y desarrollo"}]';
        //console.log('INDICADOR HISTÓRICO',PTR_VAL_HIST1);

      var aindicador_artistico1 = document.getElementById("aindicador_artistico1").checked;
      var aindicador_artistico2 = document.getElementById("aindicador_artistico2").checked;
      var aindicador_artistico3 = document.getElementById("aindicador_artistico3").checked; 

      var PTR_VAL_ART1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_artistico1+', "resvalor": "Inmueble que se constituye en un ejemplo sobresaliente por su singularidad arquitectónica/artística"},{"resid": 285, "estado": '+aindicador_artistico2+', "resvalor": "Inmueble que conserva elementos arquitectónicos y tradicionales de interés"},{"resid": 280, "estado": '+aindicador_artistico3+', "resvalor": "Inmueble poseedor de expresiones artísticas y decorativas de interés"}]';
        //console.log('INDICADOR ARTÍSTICO',PTR_VAL_ART1);

      var aindicador_arquitectonico1 = document.getElementById("aindicador_arquitectonico1").checked;
      var aindicador_arquitectonico2 = document.getElementById("aindicador_arquitectonico2").checked;

      var PTR_VAL_ARQ1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_arquitectonico1+', "resvalor": "Inmueble poseedor de características Tipológicas representativas de estilos arquitectónicos"},{"resid": 285, "estado": '+aindicador_arquitectonico2+', "resvalor": "Inmueble con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalles"}]';
        //console.log('INDICADOR ARQUITECTÓNICO',PTR_VAL_ARQ1);

      var aindicador_teclogico1 = document.getElementById("aindicador_teclogico1").checked;
      var aindicador_teclogico2 = document.getElementById("aindicador_teclogico2").checked;
      var aindicador_teclogico3 = document.getElementById("aindicador_teclogico3").checked;

      var PTR_VAL_TEC1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_teclogico1+', "resvalor": "Inmueble que se constituye en un exponente de las técnicas constructivas y uso de materiales característicos de una época o región determinada"},{"resid": 285, "estado": '+aindicador_teclogico2+', "resvalor": "Inmueble donde la aplicación de técnicas constructivas son singulares o de especial interés"},{"resid": 280, "estado": '+aindicador_teclogico3+', "resvalor":"Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
        //console.log('INDICADOR TECNOLÓGICO',PTR_VAL_TEC1);

      var aindicador_integridad1 = document.getElementById("aindicador_integridad1").checked;
      var aindicador_integridad2 = document.getElementById("aindicador_integridad2").checked;
      var aindicador_integridad3 = document.getElementById("aindicador_integridad3").checked;
      
      var PTR_VAL_INTE1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_integridad1+', "resvalor": "Inmueble que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 285, "estado": '+aindicador_integridad2+', "resvalor": "Inmueble o espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"},{"resid": 280, "estado": '+aindicador_integridad3+', "resvalor": "Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
        //console.log('INDICADOR ITEGRIDAD',PTR_VAL_INTE1);
      
      var aindicador_urbano1 = document.getElementById("aindicador_urbano1").checked;
      var aindicador_urbano2 = document.getElementById("aindicador_urbano2").checked;
      var aindicador_urbano3 = document.getElementById("aindicador_urbano3").checked;

        
      var PTR_VAL_URB1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_urbano1+', "resvalor": "Inmueble que contribuye a definir un entorno de valor por su configuración y calidad en su estructura urbanística, el paisaje y/o el espacio público"},{"resid": 285, "estado": '+aindicador_urbano2+', "resvalor": "Inmueble que forma un perfil homogéneo y/ó armónico"},{"resid": 280, "estado": '+aindicador_urbano3+', "resvalor": "Inmueble considerado hito de referencia por su emplazamiento"}]';
        //console.log('INDICADOR URBANO',PTR_VAL_URB1);
      
      var aindicador_inmaterial1 = document.getElementById("aindicador_inmaterial1").checked;
      var aindicador_inmaterial2 = document.getElementById("aindicador_inmaterial2").checked;

      var PTR_VALORES_1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_inmaterial1+', "resvalor": "INDICADOR INMATERIAL (2% / 7%) Inmueble relacionado con la organización social, forma de vida, usos, representaciones, expresiones, conocimientos y técnicas que las comunidades y grupos sociales reconocen como parte de patrimonio"},{"resid": 285, "estado": '+aindicador_inmaterial2+', "resvalor": "INDICADOR SIMBÓLICO (1% / 5%) Inmueble o espacio abierto único que expresa una significación cultural, representa identidad y pertenencia para el colectivo"}]';

      var PTR_VAL_HIS_DESC1 = $("#aindicador_historico").val();
      var PTR_VAL_HIS_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_HIS_DESC1);
      var PTR_VAL_ART_DESC1 = $("#aindicador_atistico").val();
      var PTR_VAL_ART_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_ART_DESC1);
      var PTR_VAL_ARQ_DESC1 = $("#aindicador_arquitectonico").val();
      var PTR_VAL_ARQ_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_ARQ_DESC1);
      var PTR_VAL_TEC_DESC1 = $("#aindicador_teclogico").val();
      var PTR_VAL_TEC_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_TEC_DESC1);
      var PTR_VAL_INTE_DESC1 = $("#aindicador_integrid").val();
      var PTR_VAL_INTE_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_INTE_DESC1);
      var PTR_VAL_URB_DESC1 = $("#aindicador_urbano").val();
      var PTR_VAL_URB_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_URB_DESC1);
      var PTR_VAL_INMAT_DESC1 = $("#aindicador_inmaterial").val();
      var PTR_VAL_INMAT_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_INMAT_DESC1);
      var PTR_VAL_SIMB_DESC1 = $("#aindicador_simbolico").val();
      var PTR_VAL_SIMB_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_SIMB_DESC1);
      var PTR_VAL_INT_DESC1 = $("#aindicador_interior").val();
      var PTR_VAL_INT_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_INT_DESC1);
      var PTR_VAL_EXT_DESC1 = $("#adescripcion_exterior").val();
      var PTR_VAL_EXT_DESC1 = caracteresEspecialesRegistrar(PTR_VAL_EXT_DESC1);
      var PTR_CATEGORIA1 = $("#acategoria_inmueble").val();
      var PTR_PUNTO = $("#apuntuacion").val();
      var PTR_ID_BLOQ1 = $("#aid_bloque").val();
 
  var cas_data_in ='{"g_tipo":"'+g_tipo+'","PTR_COD_CAT":"'+acodCatastral_in+'","PTR_COD_CAT_HIS1":"'+acodCatastral1_in+'","PTR_OR":"'+aoriginal_in+'","PTR_TRAD":"'+atradicional_in1+'","PTR_ACT":"'+aActual_in+'","PTR_MACRO":"'+aMacrodistrito_in+'","PTR_TIP_VIAS":"'+atipoVia_in+'","PTR_CALL_ESQ":"'+acalleEsquina_in+'","PTR_DIRE":"'+adireccion_in+'","PTR_ZONA":"'+azona_in+'","PTR_NUM1":"'+anumero_in+'","PTR_REG_PROPI":"'+anombrePropietario_in+'","PTR_REG_TIPO_PROPI":"'+atipoPropiedad_in+'","PTR_ARE_PRED":"'+aareaPredio_in+'","PTR_ANCH_FREN":"'+aanchoFrente_in+'","PTR_FOND_PRED":"'+afondoPredio_in+'","PTR_ANCH_MUR":"'+aanchoMuro_in+'","PTR_ALT_PROM":"'+aalturaInterior_in+'","PTR_ALT_FACH":"'+aalturaFachada_in+'","PTR_ALT_EDIF":"'+aalturaMaxima_in+'","PTR_USO_TRAD":'+atradicional_check+',"PTR_USO_ACT":'+aactual_check+',"PTR_USO_TEN":'+atendencia_check+',"PTR_DES_PRED":"'+adescripcion_in+'","PTR_UBIC_MAN":"'+aubicacionManzana_in+'","PTR_LIN_CONS":"'+alineaConstruccion_in+'","PTR_TRAZ_MAN":"'+atrazoManzana_in+'","PTR_TIP_ARQ":"'+atipologiaArqui_in+'","PTR_AER_EDIF":"'+aareaEdificable_in+'","PTR_MAT_VIA":"'+amaterialVia_in+'","PTR_TIP_VIA":"'+atipoVia_in+'","PTR_BLOQ_INV":"'+abloqueInventario_in+'","PTR_HOG_REAL":"'+abloquesReales_in+'","PTR_HOG_INV":"'+ahogaresInventario_in+'","PTR_HOG_REAL":"'+ahogaresReales_in+'","PTR_EST_GEN":"'+aestiloGeneral_in+'","PTR_RAN_EPO":"'+arangoEpoca_in+'","PTR_EST_CON":"'+aestadoConserva_in+'","PTR_FECH_CONS":"'+afechaConstruccion_in+'","PTR_AUT_CON":"'+aautor_in+'","PTR_PREEX_EDIF":"'+apreexistencia_in+'","PTR_PROP_ANT":"'+apropietarios_in+'","PTR_USO_ORG":"'+ausoOriginal_in+'","PTR_HEC_HIST":"'+ahechoHistorico_in+'","PTR_FIE_REL":"'+afiestaReligiosa_in+'","PTR_DAT_HIST_CONJ":"'+adatoHistorico_in+'","PTR_FUENT_DOC":"'+afuente_in+'","PTR_FEC_CONST_DOC":"'+afechaConstruccion_in3+'","PTR_AUT_CONS":"'+aautor_in3+'","PTR_PREEX_INTER":"'+apreexistencia_in3+'","PTR_PROP_ANT_DOC":"'+apropietarios_in3+'","PTR_USOS_ORG_DOC":"'+ausoOriginal_in3+'","PTR_HEC_HIS_DOC":"'+ahechoHistorico_in3+'","PTR_FIE_REL_DOC":"'+afiestaReligiosa_in3+'","PTR_DAT_HIS_DOC":"'+adatoHistorico_in3+'","PTR_FUENTE_DOC":"'+afuente_in3+'","PTR_INSCRIP":"'+ainscripcion_in+'","PTR_EXIST":"'+aexistePatrimonio_in+'","PTR_EXIST_AR":"'+aexisteResto_in+'","PTR_EXIST_ELEM":"'+aexisteElemento_in+'","PTR_EXIST_RITO":"'+aritosMitos_in+'","PTR_CONJUNTO":"'+aconjunto_in+'","PTR_EST_VERF":"'+aestadoVerifica_in+'","PTR_VAL_CAT":"'+apuntuacion_in+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'","PTR_DES_BLOQ1":"'+PTR_DES_BLOQ1+'","PTR_RANG_EPO1":"'+PTR_RANG_EPO1+'","PTR_EST_BLOQ1":"'+PTR_EST_BLOQ1+'","PTR_EPO_EST1":'+PTR_EPO_EST1+',"PTR_SIS_CONST1":'+PTR_SIS_CONST1+',"PTR_SIS_CONSTRUC1":"'+PTR_SIS_CONSTRUC1+'","PTR_NRO_VIV1":"'+PTR_NRO_VIV1+'","PTR_NRO_HOG1":"'+PTR_NRO_HOG1+'","PTR_ACAP1":'+PTR_ACAP1+',"PTR_VAL_HIST1":'+PTR_VAL_HIST1+',"PTR_VAL_ART1":'+PTR_VAL_ART1+', "PTR_VAL_ARQ1":'+PTR_VAL_ARQ1+',"PTR_VAL_TEC1":'+PTR_VAL_TEC1+',"PTR_VAL_INTE1":'+PTR_VAL_INTE1+',"PTR_VAL_URB1":'+PTR_VAL_URB1+',"PTR_VALORES_1":'+PTR_VALORES_1+',"PTR_VAL_HIS_DESC1":"'+PTR_VAL_HIS_DESC1+'","PTR_VAL_ART_DESC1":"'+PTR_VAL_ART_DESC1+'","PTR_VAL_ARQ_DESC1":"'+PTR_VAL_ARQ_DESC1+'","PTR_VAL_TEC_DESC1":"'+PTR_VAL_TEC_DESC1+'","PTR_VAL_INTE_DESC1":"'+PTR_VAL_INTE_DESC1+'","PTR_VAL_URB_DESC1":"'+PTR_VAL_URB_DESC1+'","PTR_VAL_INMAT_DESC1":"'+PTR_VAL_INMAT_DESC1+'","PTR_VAL_SIMB_DESC1":"'+PTR_VAL_SIMB_DESC1+'","PTR_VAL_INT_DESC1":"'+PTR_VAL_INT_DESC1+'","PTR_VAL_EXT_DESC1":"'+PTR_VAL_EXT_DESC1+'","PTR_CATEGORIA1":"'+PTR_CATEGORIA1+'","PTR_PUNTO":"'+PTR_PUNTO+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'","PTR_FIG_PROT":'+afiguraProtec+',"PTR_REF_HIST":'+areferenciaHistorica+'}';
  console.log("cas_data_in",cas_data_in);

    var jdata_in = JSON.stringify(cas_data_in);
    var xcas_act_id = 137;
    var xcas_usr_actual_id = 1155;
    var xcas_estado_paso = 'Recibido';
    var xcas_nodo_id = 1;
    var xcas_usr_id = 1;
    var xcas_ws_id = 1;
    var xcas_asunto = 'asunto';
    var xcas_tipo_hr = 'hora';
    var xcas_id_padre = 1;
    var xcampo_f ='campo'; 

    var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_in+',"xcas_nro_caso":'+anroficha_in+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_in+',"xcas_nombre_caso":"'+acodPatrimonio_in+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
   
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: { 
       'authorization': 'Bearer' + token,
     },
     success: function(data){
      console.log("dataaaaaa",data);
      listarPatrimonioCultural(g_tipo);
      swal( "Exíto..!", "Se actualizo correctamente el registro del Bienes Arqueologicos...!", "success" );

    },
    error: function(result) {
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });         
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});  
});

/*-----------------END BIENES INMUEBLES-----------------------*/

/*-----------------PATRIMONIO INMATERIAL-----------------------*/

$( "#registrarPatriCultural").click(function() 
{
  obtenercorrelativo('PTR-PCI');
  setTimeout(function(){
    var ccreador_in = $("#ccreador_in").val();   
    var ccodPatrimonio_p = 'PTR_PCI'+correlativog+'/2018';
    var cbien_cultural_p = $("#cbien_cultural_p").val();
    var ctipo_cultural_p = $("#ctipo_cultural_p").val();
    var ccategoria_declaratoria_p = document.getElementById('ccategoria_declaratoria_p').value;
    var cnorma_legal_p = document.getElementById('cnorma_legal_p').value;
    var cnumero_legal_p = $("#cnumero_legal_p").val();
    var cfecha_programacion_p = $("#cfecha_programacion_p").val();
    var cpalabra_clave_p = $("#cpalabra_clave_p").val();
    var g_tipo = 'PTR-PCI';

    var cas_data_p ='{"g_tipo":"'+g_tipo+'","PTR_CREADOR":"'+ccreador_in+'","TIPO_ARQ":"P2","PTR_DEN":"'+cbien_cultural_p+'","PTR_TB_CUL":"'+ctipo_cultural_p+'","PTR_PROT_LEGAL":"'+ccategoria_declaratoria_p+'","PTR_NIV_PROTEC":"'+cnorma_legal_p+'","PTR_NUM_PRLEGAL":"'+cnumero_legal_p+'","PTR_FEC_PROG":"'+cfecha_programacion_p+'","PTR_PALABRA_CLAVE":"'+cpalabra_clave_p+'"}';

    var jdata_p = JSON.stringify(cas_data_p);
  //////////console.log("jdataaaa",jdata_p);

  var xcas_nro_caso = correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = '';
  var xcas_tipo_hr = '';
  var xcas_id_padre = 1;
  var xcampo_f = ''; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_p+',"xcas_nombre_caso":"'+ccodPatrimonio_p+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

      //////////console.log("dataaaa",data);
      listarPatrimonioCultural(g_tipo);
      swal( "Exíto..!", "Se registro correctamente el Patrimonio Inmaterial...!", "success" );
      $( "#formInmaterial_create").data('bootstrapValidator').resetForm();
      $( "#myCreatePatriInmaterial" ).modal('toggle');
      $( "#cbien_cultural_p" ).val(""); 
      $( "#ctipo_cultural_p" ).val(""); 
      $( "#ccategoria_declaratoria_p" ).val(""); 
      $( "#cnorma_legal_p" ).val(""); 
      $( "#cnumero_legal_p" ).val(""); 
      $( "#cfecha_programacion_p" ).val(""); 
      $( "#cpalabra_clave_p" ).val(""); 
      var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
        url = url.replace('casosId1', idCasos);
        //console.log("url",url);
        location.href = url;
      }, 1000);
    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  

}, 500);
});

$( "#registrarAmbito4").click(function() 
{
  obtenercorrelativo('PTR-PCI');
  setTimeout(function(){
    var ccreador_in = $("#ccreador_in").val();     
    var ccodPatrimonio_i = 'PTR_PCI'+correlativog+'/2018';
    var cexpreCultural_i = $("#cexpreCultural_i").val();
  //////////console.log("cexpreCultural_i",cexpreCultural_i);
  var cdenominacion_i = $("#cdenominacion_i").val();
  //////////console.log("cdenominacion_i",cdenominacion_i);
  var cetimologia_i = $("#cetimologia_i").val();
  //////////console.log("cetimologia_i",cetimologia_i);
  var cano_i = $("#cano_i").val();
  var cdepartamento_i = $("#cdepartamento_i").val();
  var cmunicipio_i = $("#cmunicipio_i").val();
  var cprovincia_i = $("#cprovincia_i").val();
  //var cmacrodistrito_i = document.getElementById('cmacrodistrito_i').value;
  var cmacrodistrito_i1 = document.getElementById("cmacrodistrito_i1").checked;
  var cmacrodistrito_i2 = document.getElementById("cmacrodistrito_i2").checked;
  var cmacrodistrito_i3 = document.getElementById("cmacrodistrito_i3").checked;
  var cmacrodistrito_i4 = document.getElementById("cmacrodistrito_i4").checked;
  var cmacrodistrito_i5 = document.getElementById("cmacrodistrito_i5").checked;
  var cmacrodistrito_i6 = document.getElementById("cmacrodistrito_i6").checked;
  var cmacrodistrito_i7 = document.getElementById("cmacrodistrito_i7").checked;
  var cmacrodistrito_i8 = document.getElementById("cmacrodistrito_i8").checked;
  var cmacrodistrito_i9 = document.getElementById("cmacrodistrito_i9").checked;

  var czona_i = $("#czona_i").val();
  var cdireccion_i = $("#cdireccion_i").val();
  var ccaracterExpresion_i = $("#ccaracterExpresion_i").val();
  var ccontexto_socio_i = $("#ccontexto_socio_i").val();
  var cdescripDenomina_i = $("#cdescripDenomina_i").val();
  var cdatosHisto_i = $("#cdatosHisto_i").val();
  var cSignificado_i = $("#cSignificado_i").val();
  
  var cnivel_proteccion1 = document.getElementById("cnivel_proteccion1").checked;
  var cnivel_proteccion2 = document.getElementById("cnivel_proteccion2").checked;
  var cnivel_proteccion3 = document.getElementById("cnivel_proteccion3").checked;
  var cnivel_proteccion4 = document.getElementById("cnivel_proteccion4").checked;
  var cnivel_proteccion5 = document.getElementById("cnivel_proteccion5").checked;

  var corganoEmisor_i = document.getElementById('corganoEmisor_i').value;
  var cinstrumento_i = document.getElementById('cinstrumento_i').value;
  var cnrolegal_i = $("#cnrolegal_i").val();
  var ccomplementarios_i = $("#ccomplementarios_i").val();
  var cfuentes_i = $("#cfuentes_i").val();
  var cpalabra_i = $("#cpalabra_i").val();
  var g_tipo = 'PTR-PCI';
////////console.log("cexpreCultural_i",cexpreCultural_i);
var cexpreCultural_i = caracteresEspecialesRegistrar(cexpreCultural_i);
var cetimologia_i = caracteresEspecialesRegistrar(cetimologia_i);

//console.log("cetimologia_iaaaaaa",cetimologia_i);
var ccaracterExpresion_i = caracteresEspecialesRegistrar(ccaracterExpresion_i);
var ccontexto_socio_i = caracteresEspecialesRegistrar(ccontexto_socio_i);
var cdescripDenomina_i = caracteresEspecialesRegistrar(cdescripDenomina_i);
var cdatosHisto_i = caracteresEspecialesRegistrar(cdatosHisto_i);
var cSignificado_i = caracteresEspecialesRegistrar(cSignificado_i);
var ccomplementarios_i = caracteresEspecialesRegistrar(ccomplementarios_i);
var cfuentes_i = caracteresEspecialesRegistrar(cfuentes_i);

var cnivelprotect_check = '[{"tipo":"CHKM"},{"resid": 277, "estado": '+cnivel_proteccion1+', "resvalor": "Ninguno"},{"resid": 285, "estado": '+cnivel_proteccion2+', "resvalor": "Patrimonio de la Humanidad"},{"resid": 280, "estado": '+cnivel_proteccion3+', "resvalor": "Patrimonio Departamental"},{"resid": 283, "estado": '+cnivel_proteccion4+', "resvalor": "Patrimonio Municipal"},{"resid": 282, "estado": '+cnivel_proteccion5+', "resvalor": "Patrimonio Nacional"}]';

var cmacrodistrito_i_check = '[{"tipo":"CHKM"},{"resid": 304, "estado": '+cmacrodistrito_i1+', "resvalor": "MACRODISTRITO 1 COTAHUMA"},{"resid": 305, "estado": '+cmacrodistrito_i2+', "resvalor": "MACRODISTRITO 2 MAXIMILIANO PAREDES"},{"resid": 306, "estado": '+cmacrodistrito_i3+', "resvalor": "MACRODISTRITO 3 PERIFERICA"},{"resid": 307, "estado": '+cmacrodistrito_i4+', "resvalor": "MACRODISTRITO 4 SAN ANTONIO"},{"resid": 308, "estado": '+cmacrodistrito_i5+', "resvalor": "MACRODISTRITO 5 SUR"},{"resid": 309, "estado": '+cmacrodistrito_i6+', "resvalor": "MACRODISTRITO 6 MALLASA"},{"resid": 310, "estado": '+cmacrodistrito_i7+', "resvalor": "MACRODISTRITO 7 CENTRO"},{"resid": 311, "estado": '+cmacrodistrito_i8+', "resvalor": "MACRODISTRITO 8 HAMPATURI"},{"resid": 312, "estado": '+cmacrodistrito_i9+', "resvalor": "MACRODISTRITO 9 ZONGO"}]';

    ////console.log("nivelprotect_check",cnivelprotect_check);

    var cas_data_i ='{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"P4","PTR_CREADOR":"'+ccreador_in+'","PTR_EXPR":"'+cexpreCultural_i+'","PTR_DEN":"'+cdenominacion_i+'","PTR_ETIM":"'+cetimologia_i+'","PTR_PERIODO":"'+cano_i+'","PTR_DEP":"'+cdepartamento_i+'","PTR_MUNI":"'+cmunicipio_i+'","PTR_PROV":"'+cprovincia_i+'","PTR_MACRO":'+cmacrodistrito_i_check+',"PTR_ZONA":"'+czona_i+'","PTR_DIR":"'+cdireccion_i+'","PTR_CARACT":"'+ccaracterExpresion_i+'","PTR_CONTEX":"'+ccontexto_socio_i+'","PTR_NARR_DEN":"'+cdescripDenomina_i+'","PTR_DAT_HIS":"'+cdatosHisto_i+'","PTR_SIG_INTER":"'+cSignificado_i+'","PTR_NIV_PROTEC":'+cnivelprotect_check+',"PTR_PROT_LEGAL":"'+corganoEmisor_i+'","PTR_INSTR_PRLEGAL":"'+cinstrumento_i+'","PTR_NUM_LEGAL":"'+cnrolegal_i+'","PTR_DAT_COMP":"'+ccomplementarios_i+'","PTR_OTROS":"'+cfuentes_i+'","PTR_PALABRA_CLAVE":"'+cpalabra_i+'"}';

    var jdata_i = JSON.stringify(cas_data_i);
  ////console.log("jdataaaa",jdata_i);
  var xcas_nro_caso = correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = 'asunto';
  var xcas_tipo_hr = 'hora';
  var xcas_id_padre = 1;
  var xcampo_f = 'campo'; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_i+',"xcas_nombre_caso":"'+ccodPatrimonio_i+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
    
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

      //console.log("dataaaaInmaterial",data);
      listarPatrimonioCultural(g_tipo);
      swal( "Exíto..!","Se registro correctamente el Patrimonio Inmaterial...!", "success" );
      //$( "#formInmaterial_create").data('bootstrapValidator').resetForm();
      //$( "#myCreatePatriInmaterial" ).modal('toggle');
      $( "#cexpreCultural_i" ).val(""); 
      $( "#cdenominacion_i" ).val(""); 
      $( "#cetimologia_i" ).val(""); 
      $( "#cano_i" ).val(""); 
      $( "#cdepartamento_i" ).val("");
      $( "#cmunicipio_i" ).val("");
      $( "#cprovincia_i" ).val("");
      //$( "#cmacrodistrito_i" ).val("");
      $( "#czona_i" ).val("");
      $( "#cdireccion_i").val("");
      $( "#ccaracterExpresion_i" ).val("");
      $( "#ccontexto_socio_i" ).val("");
      $( "#cdescripDenomina_i" ).val("");
      $( "#cdatosHisto_i" ).val("");
      $( "#cSignificado_i" ).val("");
      //-----------------------------
      $( "#cnivel_proteccion1" ).val("");
      $( "#cnivel_proteccion2" ).val("");
      $( "#cnivel_proteccion3" ).val("");
      $( "#cnivel_proteccion4" ).val("");
      $( "#cnivel_proteccion5" ).val("");
      //-----------------------------
      $( "#corganoEmisor_i" ).val("");
      $( "#cinstrumento_i" ).val("");
      $( "#cnrolegal_i" ).val("");
      $( "#ccomplementarios_i" ).val("");
      $( "#cfuentes_i" ).val("");
      $( "#cpalabra_i" ).val("");

      var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
        url = url.replace('casosId1', idCasos);
        //console.log("url",url);
        location.href = url;
      }, 1000);

    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  

}, 500);
});

function limpiarA2(){
  $( "#cbien_cultural_p" ).val(""); 
  $( "#ctipo_cultural_p" ).val(""); 
  $( "#ccategoria_declaratoria_p" ).val(""); 
  $( "#cnorma_legal_p" ).val(""); 
  $( "#cnumero_legal_p" ).val(""); 
  $( "#cfecha_programacion_p" ).val(""); 
  $( "#cpalabra_clave_p" ).val(""); 

}
function limpiarA4(){
  $( "#cexpreCultural_i" ).val(""); 
  $( "#cdenominacion_i" ).val(""); 
  $( "#cetimologia_i" ).val(""); 
  $( "#cano_i" ).val(""); 
  $( "#cdepartamento_i" ).val("");
  $( "#cmunicipio_i" ).val("");
  $( "#cprovincia_i" ).val("");
 // $( "#cmacrodistrito_i" ).val("");
  $( "#czona_i" ).val("");
  $( "#cdireccion_i").val("");
  $( "#ccaracterExpresion_i" ).val("");
  $( "#ccontexto_socio_i" ).val("");
  $( "#cdescripDenomina_i" ).val("");
  $( "#cdatosHisto_i" ).val("");
  $( "#cSignificado_i" ).val("");
  //-----------------------------
  $( "#cnivel_proteccion1" ).val("");
  $( "#cnivel_proteccion2" ).val("");
  $( "#cnivel_proteccion3" ).val("");
  $( "#cnivel_proteccion4" ).val("");
  $( "#cnivel_proteccion5" ).val("");
  //-----------------------------
  $( "#corganoEmisor_i" ).val("");
  $( "#cinstrumento_i" ).val("");
  $( "#cnrolegal_i" ).val("");
  $( "#ccomplementarios_i" ).val("");
  $( "#cfuentes_i" ).val("");
  $( "#cpalabra_i" ).val("");
}

function editarAmbito4(idCaso,nroFicha,creador,expresionCul,denominacionI,etimologiaI,epocaI,macroI,zonaI,dirI,caracExpresion,contexCultural,descripDeno,datoHis,sigInterpretar,nivelProteccion,organoProtec,instrumentoProtec,nroInstrumento,datosComple,fuente,palabraClave){

 var expresionCul = caracteresEspecialesActualizar(expresionCul);
 var etimologiaI = caracteresEspecialesActualizar(etimologiaI);
 var caracExpresion = caracteresEspecialesActualizar(caracExpresion);
 var contexCultural = caracteresEspecialesActualizar(contexCultural);
 var descripDeno = caracteresEspecialesActualizar(descripDeno);
 var datoHis = caracteresEspecialesActualizar(datoHis);
 var sigInterpretar = caracteresEspecialesActualizar(sigInterpretar);
 var datosComple = caracteresEspecialesActualizar(datosComple);
 var fuente = caracteresEspecialesActualizar(fuente);




 $( "#acasoId_i" ).val(idCaso);
 $( "#anroFicha_i" ).val(nroFicha);
 $( "#acreador_in" ).val(creador);
 $( "#aexpreCultural_i" ).val(expresionCul);
 $( "#adenominacion_i" ).val(denominacionI);
 $( "#aetimologia_i" ).val(etimologiaI);
 $( "#aano_i" ).val(epocaI);
 $( "#amacrodistrito_i" ).val(macroI);
 $( "#azona_i" ).val(zonaI);
 $( "#adireccion_i" ).val(dirI);
 $( "#acaracterExpresion_i" ).val(caracExpresion);
 $( "#acontexto_socio_i" ).val(contexCultural);
 $( "#adescripDenomina_i" ).val(descripDeno);
 $( "#adatosHisto_i" ).val(datoHis);
 $( "#aSignificado_i" ).val(sigInterpretar);
  //--------------------------------------------

  //console.log("nivelProteccionEditarrrr",nivelProteccion);

  var nivelprotect = nivelProteccion.split(',');
  document.getElementById("anivel_proteccion1").checked = getBool(nivelprotect[0]);
  ////console.log("nivelprotect[0]",nivelprotect[0]);
  document.getElementById("anivel_proteccion2").checked = getBool(nivelprotect[1]);
  document.getElementById("anivel_proteccion3").checked = getBool(nivelprotect[2]);
  document.getElementById("anivel_proteccion4").checked = getBool(nivelprotect[3]);
  document.getElementById("anivel_proteccion5").checked = getBool(nivelprotect[3]);                       
  //------------------------------------------

  var macrodistrito_i = macroI.split(',');
  document.getElementById("amacrodistrito_i1").checked = getBool(macrodistrito_i[0]);
  document.getElementById("amacrodistrito_i2").checked = getBool(macrodistrito_i[1]);
  document.getElementById("amacrodistrito_i3").checked = getBool(macrodistrito_i[2]);
  document.getElementById("amacrodistrito_i4").checked = getBool(macrodistrito_i[3]);
  document.getElementById("amacrodistrito_i5").checked = getBool(macrodistrito_i[4]);
  document.getElementById("amacrodistrito_i6").checked = getBool(macrodistrito_i[5]);
  document.getElementById("amacrodistrito_i7").checked = getBool(macrodistrito_i[6]);
  document.getElementById("amacrodistrito_i8").checked = getBool(macrodistrito_i[7]);
  document.getElementById("amacrodistrito_i9").checked = getBool(macrodistrito_i[8]);


  $( "#aorganoEmisor_i" ).val(organoProtec);
  $( "#ainstrumento_i" ).val(instrumentoProtec);
  $( "#anrolegal_i" ).val(nroInstrumento);
  $( "#acomplementarios_i" ).val(datosComple);
  $( "#afuentes_i" ).val(fuente);
  $( "#apalabra_i" ).val(palabraClave);
}

function editarAmbito2(idCaso,nroFicha,bienCultural,tipoBien,catDeclaratoria,normaLegal,nroLegal,fechaProg,palabraClave){
 $( "#acasoId_p" ).val(idCaso);
 $( "#anroFicha_p" ).val(nroFicha);
 $( "#abien_cultural_p" ).val(bienCultural); 
 $( "#atipo_cultural_p" ).val(tipoBien);
 $( "#acategoria_declaratoria_p" ).val(catDeclaratoria);
 $( "#anorma_legal_p" ).val(normaLegal);
 $( "#anumero_legal_p" ).val(nroLegal);
 $( "#afecha_programacion_p" ).val(fechaProg);
 $( "#apalabra_clave_p" ).val(palabraClave);
}

$( "#actualizarInmaterialA4" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

      var acasoId_i = $( "#acasoId_i" ).val();
      var anroFicha_i = $( "#anroFicha_i" ).val(); 
      var acreador_in = $( "#acreador_in" ).val(); 

      var aTipo_i = $( "#aTipo_i" ).val(); 
      var aTipoInmaterial = $( "#aTipoInmaterial" ).val(); 
      var acodPatrimonio_i = 'PTR_PCI'+(anroFicha_i)+'/2018';
      var aexpreCultural_i = $("#aexpreCultural_i").val();
      var adenominacion_i = $("#adenominacion_i").val();
      var aetimologia_i = $("#aetimologia_i").val();
      var aano_i = $("#aano_i").val();
      var adepartamento_i = $("#adepartamento_i").val();
      var amunicipio_i = $("#amunicipio_i").val();
      var aprovincia_i = $("#aprovincia_i").val();
      //var amacrodistrito_i = document.getElementById('amacrodistrito_i').value;
      var amacrodistrito_i1 = document.getElementById("amacrodistrito_i1").checked;
      var amacrodistrito_i2 = document.getElementById("amacrodistrito_i2").checked;
      var amacrodistrito_i3 = document.getElementById("amacrodistrito_i3").checked;
      var amacrodistrito_i4 = document.getElementById("amacrodistrito_i4").checked;
      var amacrodistrito_i5 = document.getElementById("amacrodistrito_i5").checked;
      var amacrodistrito_i6 = document.getElementById("amacrodistrito_i6").checked;
      var amacrodistrito_i7 = document.getElementById("amacrodistrito_i7").checked;
      var amacrodistrito_i8 = document.getElementById("amacrodistrito_i8").checked;
      var amacrodistrito_i9 = document.getElementById("amacrodistrito_i9").checked;

      var azona_i = $("#azona_i").val();
      var adireccion_i = $("#adireccion_i").val();
      var acaracterExpresion_i = $("#acaracterExpresion_i").val();
      var acontexto_socio_i = $("#acontexto_socio_i").val();
      var adescripDenomina_i = $("#adescripDenomina_i").val();
      var adatosHisto_i = $("#adatosHisto_i").val();
      var aSignificado_i = $("#aSignificado_i").val();

      var anivel_proteccion_legal1 = document.getElementById("anivel_proteccion1").checked;
      var anivel_proteccion_legal2 = document.getElementById("anivel_proteccion2").checked;
      var anivel_proteccion_legal3 = document.getElementById("anivel_proteccion3").checked;
      var anivel_proteccion_legal4 = document.getElementById("anivel_proteccion4").checked;
      var anivel_proteccion_legal5 = document.getElementById("anivel_proteccion5").checked;

      var aorganoEmisor_i = document.getElementById('aorganoEmisor_i').value;
      var ainstrumento_i = document.getElementById('ainstrumento_i').value;
      var anrolegal_i = $("#anrolegal_i").val();
      var acomplementarios_i = $("#acomplementarios_i").val();
      var afuentes_i = $("#afuentes_i").val();
      //console.log("afuentes_i",afuentes_i);
      var apalabra_i = $("#apalabra_i").val();
      var g_tipo = 'PTR-PCI';
      var aexpreCultural_i = caracteresEspecialesRegistrar(aexpreCultural_i);
      var aetimologia_i = caracteresEspecialesRegistrar(aetimologia_i);
      var acaracterExpresion_i = caracteresEspecialesRegistrar(acaracterExpresion_i);
      var acontexto_socio_i = caracteresEspecialesRegistrar(acontexto_socio_i);
      var adescripDenomina_i = caracteresEspecialesRegistrar(adescripDenomina_i);
      //console.log("adatosHisto_iAntes",adatosHisto_i);
      var adatosHisto_i = caracteresEspecialesRegistrar(adatosHisto_i);
      //console.log("adatosHisto_i",adatosHisto_i);

      var aSignificado_i = caracteresEspecialesRegistrar(aSignificado_i);
      var acomplementarios_i = caracteresEspecialesRegistrar(acomplementarios_i);
      var afuentes_i = caracteresEspecialesRegistrar(afuentes_i);

      var anivelprotect_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+anivel_proteccion_legal1+', "resvalor": "Ninguno"},{"resid": 285, "estado": '+anivel_proteccion_legal2+', "resvalor": "Patrimonio de la Humanidad"},{"resid": 280, "estado": '+anivel_proteccion_legal3+', "resvalor": "Patrimonio Departamental"},{"resid": 283, "estado": '+anivel_proteccion_legal4+', "resvalor": "Patrimonio Municipal"},{"resid": 282, "estado": '+anivel_proteccion_legal5+', "resvalor": "Patrimonio Nacional"}]';

      var amacrodistrito_i_check = '[{"tipo":"CHKM"},{"resid": 304, "estado": '+amacrodistrito_i1+', "resvalor": "MACRODISTRITO 1 COTAHUMA"},{"resid": 305, "estado": '+amacrodistrito_i2+', "resvalor": "MACRODISTRITO 2 MAXIMILIANO PAREDES"},{"resid": 306, "estado": '+amacrodistrito_i3+', "resvalor": "MACRODISTRITO 3 PERIFERICA"},{"resid": 307, "estado": '+amacrodistrito_i4+', "resvalor": "MACRODISTRITO 4 SAN ANTONIO"},{"resid": 308, "estado": '+amacrodistrito_i5+', "resvalor": "MACRODISTRITO 5 SUR"},{"resid": 309, "estado": '+amacrodistrito_i6+', "resvalor": "MACRODISTRITO 6 MALLASA"},{"resid": 310, "estado": '+amacrodistrito_i7+', "resvalor": "MACRODISTRITO 7 CENTRO"},{"resid": 311, "estado": '+amacrodistrito_i8+', "resvalor": "MACRODISTRITO 8 HAMPATURI"},{"resid": 312, "estado": '+amacrodistrito_i9+', "resvalor": "MACRODISTRITO 9 ZONGO"}]';
 ////console.log("nivelprotect_check",anivelprotect_check);

 var cas_data_i ='{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"P4","PTR_CREADOR":"'+acreador_in+'","PTR_EXPR":"'+aexpreCultural_i+'","PTR_DEN":"'+adenominacion_i+'","PTR_ETIM":"'+aetimologia_i+'","PTR_PERIODO":"'+aano_i+'","PTR_DEP":"'+adepartamento_i+'","PTR_MUNI":"'+amunicipio_i+'","PTR_PROV":"'+aprovincia_i+'","PTR_MACRO":'+amacrodistrito_i_check+',"PTR_ZONA":"'+azona_i+'","PTR_DIR":"'+adireccion_i+'","PTR_CARACT":"'+acaracterExpresion_i+'","PTR_CONTEX":"'+acontexto_socio_i+'","PTR_NARR_DEN":"'+adescripDenomina_i+'","PTR_DAT_HIS":"'+adatosHisto_i+'","PTR_SIG_INTER":"'+aSignificado_i+'","PTR_NIV_PROTEC":'+anivelprotect_check+',"PTR_PROT_LEGAL":"'+aorganoEmisor_i+'","PTR_INSTR_PRLEGAL":"'+ainstrumento_i+'","PTR_NUM_LEGAL":"'+anrolegal_i+'","PTR_DAT_COMP":"'+acomplementarios_i+'","PTR_OTROS":"'+afuentes_i+'","PTR_PALABRA_CLAVE":"'+apalabra_i+'"}';
 var jdata_i = JSON.stringify(cas_data_i);
 //console.log("jdataaaa",jdata_i);

 var xcas_act_id = 137;
 var xcas_usr_actual_id = 1155;
 var xcas_estado_paso = 'Recibido';
 var xcas_nodo_id = 1;
 var xcas_usr_id = 1;
 var xcas_ws_id = 1;
 var xcas_asunto = '';
 var xcas_tipo_hr = '';
 var xcas_id_padre = 1;
 var xcampo_f =''; 

 var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_i+',"xcas_nro_caso":'+anroFicha_i+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_i+',"xcas_nombre_caso":"'+acodPatrimonio_i+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
 //console.log("formData",formData);

 $.ajax({
   type        : 'POST',            
   url         : urlRegla,
   data        : formData,
   dataType    : 'json',
   crossDomain : true,
   headers: { 
     'authorization': 'Bearer' + token,
   },
   success: function(data){
    //console.log("dataaaaaaInmaterial",data);
    listarPatrimonioCultural(g_tipo);
    swal( "Exíto..!", "Se actualizo correctamente el registro del Patrimonio Inmaterial...!", "success" );

  },
  error: function(result) {
    swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
  }
});         
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});  
});


$( "#actualizarInmaterialA2" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      var acasoId_p = $( "#acasoId_p" ).val();
      var anroFicha_p = $( "#anroFicha_p" ).val(); 
      var acodPatrimonio_p = 'PTR_PCI'+(anroFicha_p)+'/2018';
      var abien_cultural_p = $("#abien_cultural_p").val();
      var atipo_cultural_p = $("#atipo_cultural_p").val();
      var acategoria_declaratoria_p = document.getElementById('acategoria_declaratoria_p').value;
      var anorma_legal_p = document.getElementById('anorma_legal_p').value;
      var anumero_legal_p = $("#anumero_legal_p").val();
      var afecha_programacion_p = $("#afecha_programacion_p").val();
      var apalabra_clave_p = $("#apalabra_clave_p").val();
      var g_tipo = 'PTR-PCI';

      var cas_data_p ='{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"P2","PTR_DEN":"'+abien_cultural_p+'","PTR_TB_CUL":"'+atipo_cultural_p+'","PTR_PROT_LEGAL":"'+acategoria_declaratoria_p+'","PTR_NIV_PROTEC":"'+anorma_legal_p+'","PTR_NUM_PRLEGAL":"'+anumero_legal_p+'","PTR_FEC_PROG":"'+afecha_programacion_p+'","PTR_PALABRA_CLAVE":"'+apalabra_clave_p+'"}';

      var jdata_p = JSON.stringify(cas_data_p);
  //////////console.log("jdataaaa",jdata_p);

  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = '';
  var xcas_tipo_hr = '';
  var xcas_id_padre = 1;
  var xcampo_f =''; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_p+',"xcas_nro_caso":'+anroFicha_p+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_p+',"xcas_nombre_caso":"'+acodPatrimonio_p+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

      //////////console.log("formData",formData);
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: { 
         'authorization': 'Bearer' + token,
       },
       success: function(data){
        //////////console.log("dataaaaaa",data);
        listarPatrimonioCultural(g_tipo);
        swal( "Exíto..!", "Se actualizo correctamente el registro del Patrimonio Inmaterial...!", "success" );

      },
      error: function(result) {
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });         
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
});
/*-----------------END PATRIMONIO INMATERIAL-----------------------*/


/*-----------------BEGIN ESCULTURAS Y MONUMENTOS---------------------*/

$("#guardarEsculturas").click(function(){
  obtenercorrelativo('PTR-IME');
  setTimeout(function(){

  var PTR_NOMBRE = $( "#cescdenominacion" ).val();
  var PTR_EPOCA = $("#cepocaelaboracion").val();
  var PTR_PAIS = $( "#cpais" ).val();
  var PTR_DEP = $( "#cdepartamento" ).val();
  var PTR_MUNI = $( "#cmunicipio" ).val();
  var PTR_PROV = $( "#cprovincia" ).val();
  var macrodistritoTxt = document.getElementById("cmacrodistritoescultura");
  var PTR_MACRO = macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
  var PTR_ZONA = $( "#czona" ).val();
  var PTR_COMU = $( "#ccomunidad").val();
  var PTR_DIR = $( "#cdireccion" ).val();
  var PTR_AU = $( "#cdatosautor" ).val(); //duda
  var PTR_ESP = $( "#cespecialidad" ).val();
  var PTR_TEC_MAT = $( "#ctecnicamaterial" ).val(); 
  //var PTR_CONJ_ESC = $( "#cpiezaesculturica" ).val();
  var PTR_ALTO = $( "#calto" ).val();
  var PTR_ANCHO = $( "#cancho" ).val();
  var PTR_PESO = $( "#cpeso" ).val();
  var PTR_DESC_OBRA = $( "#cdescripcionobra" ).val();
  var PTR_ORG_PIE = $( "#corigenpieza" ).val();
  var PTR_PROP = $( "#cnombrepropietarioescultura" ).val();
  var PTR_RESP = $( "#cresponsable" ).val();
  var PTR_TIPO = $( "#ctipo" ).val();
  var PTR_LEY_OR = $( "#cordenanzamunicipal" ).val();
  var PTR_DES_PER = $( "#cdescripcionpersonaje" ).val();
  var PTR_REF_HIST = $( "#creferenciahistorica" ).val();
  var PTR_REF_CON = $( "#cestadoconservacion" ).val();
  var PTR_OBS = $( "#cobservaciones" ).val();
  var PTR_FUENT_BIBLIO = $( "#cfuentesbibliograficos" ).val(); 
  var g_tipo = 'PTR-IME';
  var PTR_AU = caracteresEspecialesRegistrar(PTR_AU);
  var PTR_DESC_OBRA = caracteresEspecialesRegistrar(PTR_DESC_OBRA);
  var PTR_ORG_PIE = caracteresEspecialesRegistrar(PTR_ORG_PIE);
  var PTR_DES_PER = caracteresEspecialesRegistrar(PTR_DES_PER);
  var PTR_REF_HIST = caracteresEspecialesRegistrar(PTR_REF_HIST);
  var PTR_OBS = caracteresEspecialesRegistrar(PTR_OBS);
  var PTR_FUENT_BIBLIO = caracteresEspecialesRegistrar(PTR_FUENT_BIBLIO);
  var cpiezaesculturica1 = document.getElementById("cpiezaesculturica1").checked;
  var cpiezaesculturica2 = document.getElementById("cpiezaesculturica2").checked;
  var cpiezaesculturica_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cpiezaesculturica1+', "resvalor": "Conjunto Escultórico"},{"resid": 285, "estado": '+cpiezaesculturica2+', "resvalor": "Una sola pieza"}]';

  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOMBRE":"'+PTR_NOMBRE+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_PAIS":"'+PTR_PAIS+'","PTR_DEP":"'+PTR_DEP+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_PROV":"'+PTR_PROV+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_ZONA":"'+PTR_ZONA+'","PTR_COMU":"'+PTR_COMU+'","PTR_DIR":"'+PTR_DIR+'","PTR_AU":"'+PTR_AU+'","PTR_ESP":"'+PTR_ESP+'","PTR_TEC_MAT":"'+PTR_TEC_MAT+'","PTR_CONJ_ESC":'+cpiezaesculturica_check+',"PTR_ALTO":"'+PTR_ALTO+'","PTR_ANCHO":"'+PTR_ANCHO+'","PTR_PESO":"'+PTR_PESO+'","PTR_DESC_OBRA":"'+PTR_DESC_OBRA+'","PTR_ORG_PIE":"'+PTR_ORG_PIE+'","PTR_PROP":"'+PTR_PROP+'","PTR_RESP":"'+PTR_RESP+'","PTR_TIPO":"'+PTR_TIPO+'","PTR_LEY_OR":"'+PTR_LEY_OR+'","PTR_DES_PER":"'+PTR_DES_PER+'","PTR_REF_HIST":"'+PTR_REF_HIST+'","PTR_REF_CON":"'+PTR_REF_CON+'","PTR_OBS":"'+PTR_OBS+'","PTR_FUENT_BIBLIO":"'+PTR_FUENT_BIBLIO+'"}';

  var data = JSON.stringify(data);
 ////console.log("datadataESCULTURA",data);
 var cas_nro_caso = correlativog ;
 var cas_act_id = 137 ;
 var cas_usr_actual_id = 1155 ;
 var PTR_NRO_FIC = $( "#cnumeroficha" ).val();
 var cas_nombre_caso = 'PTR-IME'+correlativog+'/2018';;      
 var cas_estado_paso = 'RECIBIDO';
 var cas_nodo_id = 1; 
 var cas_usr_id = 1;
 var cas_ws_id = 1;  
 var cas_asunto = 'asunto';
 var cas_tipo_hr = 'hora';
 var cas_id_padre = 1;
 var campo_f = 'campo';
 contesculturas = contesculturas+1; 
 var formData = {"identificador":"SERVICIO_PATRIMONIO-941","parametros":'{"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");

  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 


        //console.log("dataaEEEEEa",data);
        listarPatrimonioCultural(g_tipo); 
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );
        var numeroficha='PTR-IME'+contesculturas+'/2018';
        $( "#cnumeroficha" ).val(numeroficha); 

        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
        setTimeout(function(){
          var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
          url = url.replace('casosId1', idCasos);
          //console.log("url",url);
          location.href = url;
        }, 1000);           
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 800);
});



function editarMonumentoEscultura(cas_id,numero_ficha,g_tipo,codigo_ficha,denominacion,epocaelaboracion,pais,departamento, municipio,provincia,macrodistrito,zona,comunidad, direccion , datosautor ,especialidad,tecnicamaterial,piezaesculturica,piezaesculturica,alto, ancho ,peso,descripcionobra, origenpieza ,nombrepropietario, responsable ,tipo,ordenanzamunicipal,descripcionpersonaje, referenciahistorica,estadoconservacion,observaciones,fuentesbibliograficos)
{
  ////////console.log(cas_id,numero_ficha, g_tipo,codigo_ficha,denominacion,epocaelaboracion,pais,departamento, municipio,provincia,macrodistrito,zona,comunidad, direccion , datosautor ,especialidad,tecnicamaterial,piezaesculturica,piezaesculturica,alto, ancho ,peso,descripcionobra, origenpieza ,nombrepropietario, responsable ,tipo,ordenanzamunicipal,descripcionpersonaje, referenciahistorica,estadoconservacion,observaciones,fuentesbibliograficos);

  var datosautor = caracteresEspecialesActualizar(datosautor);
  var descripcionobra = caracteresEspecialesActualizar(descripcionobra);
  var origenpieza = caracteresEspecialesActualizar(origenpieza);
  var descripcionpersonaje = caracteresEspecialesActualizar(descripcionpersonaje);
  var referenciahistorica = caracteresEspecialesActualizar(referenciahistorica);
  var observaciones = caracteresEspecialesActualizar(observaciones);
  var fuentesbibliograficos = caracteresEspecialesActualizar(fuentesbibliograficos);

  $( "#acas_id" ).val(cas_id);
  $( "#aescnumero_ficha" ).val(numero_ficha);
  $( "#aesccodigo_ficha" ).val(codigo_ficha);
  $( "#ag_tipo" ).val(g_tipo);
  $( "#anumeroficha" ).val(codigo_ficha);
  $( "#aescdenominacion" ).val(denominacion);
  $( "#aepocaelaboracion").val(epocaelaboracion);
  $( "#apais" ).val(pais);
  $( "#adepartamento" ).val(departamento);
  $( "#amunicipio" ).val(municipio);
  $( "#aprovincia" ).val(provincia);
  $( "#amacrodistrito" ).val(macrodistrito);
  document.getElementById("amacrodistritoescultura").length=0;
  document.getElementById("cmacrodistritoescultura").length=1;
  listaMacrodistritoescultura(macrodistrito);
  $( "#azona" ).val(zona);
  $( "#acomunidad").val(comunidad);
  $( "#adireccion" ).val(direccion);
  $( "#adatosautor" ).val(datosautor);
  $( "#aespecialidad" ).val(especialidad);
  $( "#atecnicamaterial" ).val(tecnicamaterial);
  //$( "#apiezaesculturica" ).val(piezaesculturica);
  var piezaesculturica = piezaesculturica.split(',');
  ////console.log(0000000000000000000,piezaesculturica)
  document.getElementById("apiezaesculturica1").checked = getBool(piezaesculturica[0]);
  document.getElementById("apiezaesculturica2").checked = getBool(piezaesculturica[1]);

  $( "#aalto" ).val(alto);
  $( "#aancho" ).val(ancho);
  $( "#apeso" ).val(peso);
  $( "#adescripcionobra" ).val(descripcionobra);
  $( "#aorigenpieza" ).val(origenpieza);
  $( "#anombrepropietarioescultura" ).val(nombrepropietario);
  $( "#aresponsable" ).val(responsable);
  $( "#atipo" ).val(tipo);
  $( "#aordenanzamunicipal" ).val(ordenanzamunicipal);
  $( "#adescripcionpersonaje" ).val(descripcionpersonaje);
  $( "#areferenciahistorica" ).val(referenciahistorica);
  $( "#aestadoconservacion" ).val(estadoconservacion);
  $( "#aobservaciones" ).val(observaciones);
  $( "#afuentesbibliograficos" ).val(fuentesbibliograficos); 
  

}

$("#actualizaresculturas").click(function(){
  var cas_id = $( "#acas_id" ).val();
  var cas_nro_caso = $( "#aescnumero_ficha" ).val();
  var PTR_NOMBRE = $( "#aescdenominacion" ).val();
  var PTR_EPOCA = $("#aepocaelaboracion").val();
  var PTR_PAIS = $( "#apais" ).val();
  var PTR_DEP = $( "#adepartamento" ).val();
  var PTR_MUNI = $( "#amunicipio" ).val();
  var PTR_PROV = $( "#aprovincia" ).val();
  var macrodistritoTxt=document.getElementById("amacrodistritoescultura");
  var PTR_MACRO=macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
  var PTR_ZONA = $( "#azona" ).val();
  var PTR_COMU = $( "#acomunidad").val();
  var PTR_DIR = $( "#adireccion" ).val();
  var PTR_AU = $( "#adatosautor" ).val(); //duda
  var PTR_ESP = $( "#aespecialidad" ).val();
  var PTR_TEC_MAT = $( "#atecnicamaterial" ).val();
  //var PTR_CONJ_ESC = $( "#apiezaesculturica" ).val();
  var PTR_ALTO = $( "#aalto" ).val();
  var PTR_ANCHO = $( "#aancho" ).val();
  var PTR_PESO = $( "#apeso" ).val();
  var PTR_DESC_OBRA = $( "#adescripcionobra" ).val();
  var PTR_ORG_PIE = $( "#aorigenpieza" ).val();
  var PTR_PROP = $( "#anombrepropietarioescultura" ).val();
  var PTR_RESP = $( "#aresponsable" ).val();
  var PTR_TIPO = $( "#atipo" ).val();
  var PTR_LEY_OR = $( "#aordenanzamunicipal" ).val();
  var PTR_DES_PER = $( "#adescripcionpersonaje" ).val();
  var PTR_REF_HIST = $( "#areferenciahistorica" ).val();
  var PTR_REF_CON = $( "#aestadoconservacion" ).val();
  var PTR_OBS = $( "#aobservaciones" ).val();
  var PTR_FUENT_BIBLIO = $( "#afuentesbibliograficos" ).val(); 
  var g_tipo = 'PTR-IME';

  var PTR_AU = caracteresEspecialesRegistrar(PTR_AU);
  var PTR_DESC_OBRA = caracteresEspecialesRegistrar(PTR_DESC_OBRA);
  var PTR_ORG_PIE = caracteresEspecialesRegistrar(PTR_ORG_PIE);
  var PTR_DES_PER = caracteresEspecialesRegistrar(PTR_DES_PER);
  var PTR_REF_HIST = caracteresEspecialesRegistrar(PTR_REF_HIST);
  var PTR_OBS = caracteresEspecialesRegistrar(PTR_OBS);
  var PTR_FUENT_BIBLIO = caracteresEspecialesRegistrar(PTR_FUENT_BIBLIO);

  var apiezaesculturica1 = document.getElementById("apiezaesculturica1").checked;
  var apiezaesculturica2 = document.getElementById("apiezaesculturica2").checked;
  var apiezaesculturica_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+apiezaesculturica1+', "resvalor": "Conjunto Escultórico"},{"resid": 285, "estado": '+apiezaesculturica2+', "resvalor": "Una sola pieza"}]';

  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOMBRE":"'+PTR_NOMBRE+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_PAIS":"'+PTR_PAIS+'","PTR_DEP":"'+PTR_DEP+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_PROV":"'+PTR_PROV+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_ZONA":"'+PTR_ZONA+'","PTR_COMU":"'+PTR_COMU+'","PTR_DIR":"'+PTR_DIR+'","PTR_AU":"'+PTR_AU+'","PTR_ESP":"'+PTR_ESP+'","PTR_TEC_MAT":"'+PTR_TEC_MAT+'","PTR_CONJ_ESC":'+apiezaesculturica_check+',"PTR_ALTO":"'+PTR_ALTO+'","PTR_ANCHO":"'+PTR_ANCHO+'","PTR_PESO":"'+PTR_PESO+'","PTR_DESC_OBRA":"'+PTR_DESC_OBRA+'","PTR_ORG_PIE":"'+PTR_ORG_PIE+'","PTR_PROP":"'+PTR_PROP+'","PTR_RESP":"'+PTR_RESP+'","PTR_TIPO":"'+PTR_TIPO+'","PTR_LEY_OR":"'+PTR_LEY_OR+'","PTR_DES_PER":"'+PTR_DES_PER+'","PTR_REF_HIST":"'+PTR_REF_HIST+'","PTR_REF_CON":"'+PTR_REF_CON+'","PTR_OBS":"'+PTR_OBS+'","PTR_FUENT_BIBLIO":"'+PTR_FUENT_BIBLIO+'"}';

  var data = JSON.stringify(data);
  //////////console.log("datadata",data);
  
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  var PTR_NRO_FIC = $( "#anumeroficha" ).val();
  var cas_nombre_caso = PTR_NRO_FIC;  
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-943","parametros":'{"xcas_id":'+cas_id+',"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");

  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        listarPatrimonioCultural(g_tipo); 
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );

      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 
});

function listaMacrodistritoescultura(macrodistrito_descripcion)
{ 
  $.ajax(
  {
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) 
    {         
      var formData = {"identificador": 'SISTEMA_VALLE-616',"parametros": '{}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) 
       {  
        var datos = dataIN;
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          document.getElementById("cmacrodistritoescultura").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+ '</option>';
        }
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          if (macrodistrito == macrodistrito_descripcion) 
          {       
            document.getElementById("amacrodistritoescultura").innerHTML += '<option value='+macrodistrito+' selected>'+macrodistrito+'</option>';
          }
          else
          {
            document.getElementById("amacrodistritoescultura").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+'</option>';
          } 
        }      
      },     
      error: function (xhr, status, error) { }
    });
    },
    error: function(result) {
      swal( "Error..!", "No se puede recuperar los datos", "error" );
    },
  });
};


/*-----------------END ESCULTURAS Y MONUMENTOS-----------------------*/
/*-----------------BEGIN CONJUNTOS PATRIMONIALES---------------------*/
$("#guardarConjuntosPatrimoniales").click(function(){
 obtenercorrelativo('PTR_CON');
 setTimeout(function(){

  var PTR_NOM = $( "#cnominacion" ).val();
  var PTR_UBIC = $( "#cubicacion" ).val();
  var PTR_TIP_PROT = $( "#ctipoproteccion" ).val();
  var PTR_NRO_PROT = $( "#cnroproteccion" ).val();
  var PTR_NUM_INM = $( "#cnrototalinmuebletramo" ).val();
  var PTR_NUM_PAT = $( "#cnrototalinmueblepatri" ).val();
  var PTR_IND_URB = $( "#cindicadorurbano" ).val();
  var PTR_IND_ART = $( "#cindicadorartistico" ).val();
  var PTR_IND_TIP = $( "#cindicadortipologico" ).val();
  var PTR_IND_TEC = $( "#cindicadortecnologico" ).val();
  var PTR_IND_INT = $( "#cindicadorintegridad" ).val();
  var PTR_IND_HIST = $( "#cindicadorhistcult" ).val();
  var PTR_IND_SIMB = $( "#cindicadorsimbolico" ).val();
  var PTR_NOM_VIA = $( "#cnombrevia" ).val();
  var PTR_MAT_VIA = $( "#cmateriavia" ).val();
  var PTR_CLASS = $( "#cclasificacion" ).val();
  var PTR_ENT_CALL = $( "#centrecalles" ).val();

  var g_tipo = 'PTR_CON';

  var PTR_IND_URB = caracteresEspecialesRegistrar(PTR_IND_URB);
  var PTR_IND_ART = caracteresEspecialesRegistrar(PTR_IND_ART);
  var PTR_IND_TIP = caracteresEspecialesRegistrar(PTR_IND_TIP);
  var PTR_IND_TEC = caracteresEspecialesRegistrar(PTR_IND_TEC);
  var PTR_IND_INT = caracteresEspecialesRegistrar(PTR_IND_INT);
  var PTR_IND_HIST = caracteresEspecialesRegistrar(PTR_IND_HIST);
  var PTR_IND_SIMB = caracteresEspecialesRegistrar(PTR_IND_SIMB);

   //--------GRILLA CONJUNTOS ABIERTOS---------------VERO


     var cont=0;
        arrDibujoCANIN.forEach(function (item, index, array) 
        {
          arrCodigoCANIN[cont].arg_imn_cat = $("#imn_cat"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_prot = $("#inm_prot"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_des = $("#inm_des"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_total = $("#inm_total"+item.id+"").val();
          cont++;
        });

  var bloqueCANIN = '[{"tipo":"GRD","campos":"imn_cat|inm_prot|inm_des|inm_total","titulos":"INMUEBLES POR CATEGORIA Y CONDICIÓN DE PROTECCIÓN|C/PROTECCION|S/PROTECCION|TOTAL"}';
  for (var i = 0; i<arrCodigoCANIN.length;i++)
  {
    var grillaFiguracon = '{"imn_cat":"'+arrCodigoCANIN[i].arg_imn_cat+'", "inm_prot":"'+arrCodigoCANIN[i].arg_inm_prot+'", "inm_des":"'+arrCodigoCANIN[i].arg_inm_des+'", "inm_total":"'+arrCodigoCANIN[i].arg_inm_total+'"}';
    bloqueCANIN = (bloqueCANIN.concat(',').concat(grillaFiguracon));

  }
  PTR_CANT_INM = bloqueCANIN.concat(']');
 

  //--------GRILLA CONJUNTOS ABIERTOS---------------


  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOM":"'+PTR_NOM+'","PTR_UBIC":"'+PTR_UBIC+'","PTR_TIP_PROT":"'+PTR_TIP_PROT+'","PTR_NRO_PROT":"'+PTR_NRO_PROT+'","PTR_NUM_INM":"'+PTR_NUM_INM+'","PTR_NUM_PAT":"'+PTR_NUM_PAT+'","PTR_IND_URB":"'+PTR_IND_URB+'","PTR_IND_ART":"'+PTR_IND_ART+'","PTR_IND_TIP":"'+PTR_IND_TIP+'","PTR_IND_TEC":"'+PTR_IND_TEC+'","PTR_IND_INT":"'+PTR_IND_INT+'","PTR_IND_HIST":"'+PTR_IND_HIST+'","PTR_IND_SIMB":"'+PTR_IND_SIMB+'","PTR_NOM_VIA":"'+PTR_NOM_VIA+'","PTR_MAT_VIA":"'+PTR_MAT_VIA+'","PTR_CLASS":"'+PTR_CLASS+'","PTR_ENT_CALL":"'+PTR_ENT_CALL+'","PTR_CANT_INM":'+PTR_CANT_INM+'}';

  var data = JSON.stringify(data);

  //////////console.log("datadata",data);
  var cas_nro_caso = correlativog ;

  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  var cas_nombre_caso = 'PTR_CON'+correlativog+'/2018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-941","parametros":'{"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");

  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        listarPatrimonioCultural(g_tipo);  
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );
        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
        setTimeout(function(){
          var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
          url = url.replace('casosId1', idCasos);
          //console.log("url",url);
          location.href = url;
        }, 1000);     


      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500);
});



function editarConjuntoPatrimonial(cas_id,numero_ficha ,codigo_ficha,g_tipo,nominacion,ubicacion,tipoproteccion,nroproteccion, nrototalinmuebletramo,nrototalinmueblepatri,indicadorurbano,indicadorartistico,indicadortipologico,indicadortecnologico,indicadorintegridad,indicadorhistcult,indicadorsimbolico,nombrevia,materiavia,clasificacion,entrecalles)
{
 ////////console.log(cas_id,g_tipo,nominacion,ubicacion,tipoproteccion,nroproteccion, nrototalinmuebletramo,nrototalinmueblepatri,indicadorurbano,indicadorartistico,indicadortipologico,indicadortecnologico,indicadorintegridad,indicadorhistcult,indicadorsimbolico,nombrevia,materiavia,clasificacion,entrecalles, 222221111133333);
 var indicadorurbano = caracteresEspecialesActualizar(indicadorurbano);
 var indicadorartistico = caracteresEspecialesActualizar(indicadorartistico);
 var indicadortipologico = caracteresEspecialesActualizar(indicadortipologico);
 var indicadortecnologico = caracteresEspecialesActualizar(indicadortecnologico);
 var indicadorintegridad = caracteresEspecialesActualizar(indicadorintegridad);
 var indicadorhistcult = caracteresEspecialesActualizar(indicadorhistcult);
 var indicadorsimbolico = caracteresEspecialesActualizar(indicadorsimbolico);
 $( "#acas_id" ).val(cas_id);
 $( "#aconnumero_ficha" ).val(numero_ficha);
 $( "#aconcodigo_ficha" ).val(codigo_ficha);
 $( "#ag_tipo" ).val(g_tipo);
 $( "#anominacion" ).val(nominacion);
 $( "#aubicacion" ).val(ubicacion);
 $( "#atipoproteccion" ).val(tipoproteccion);
 $( "#anroproteccion" ).val(nroproteccion);
 $( "#anrototalinmuebletramo" ).val(nrototalinmuebletramo);
 $( "#anrototalinmueblepatri" ).val(nrototalinmueblepatri);
 $( "#aindicadorurbano" ).val(indicadorurbano);
 $( "#aindicadorartistico" ).val(indicadorartistico);
 $( "#aindicadortipologico" ).val(indicadortipologico);
 $( "#aindicadortecnologico" ).val(indicadortecnologico);
 $( "#aindicadorintegridad" ).val(indicadorintegridad);
 $( "#aindicadorhistcult" ).val(indicadorhistcult);
 $( "#aindicadorsimbolico" ).val(indicadorsimbolico);
 $( "#anombrevia" ).val(nombrevia);
 $( "#amateriavia" ).val(materiavia);
 $( "#aclasificacion" ).val(clasificacion);
 $( "#aentrecalles" ).val(entrecalles);


}

$("#actualizarConjuntosPatrimoniales").click(function(){

  var cas_id = $( "#acas_id" ).val();
  var PTR_NOM = $( "#anominacion" ).val();
  var PTR_UBIC = $( "#aubicacion" ).val();
  var PTR_TIP_PROT = $( "#atipoproteccion" ).val();
  var PTR_NRO_PROT = $( "#anroproteccion" ).val();
  var PTR_NUM_INM = $( "#anrototalinmuebletramo" ).val();
  var PTR_NUM_PAT = $( "#anrototalinmueblepatri" ).val();
  var PTR_IND_URB = $( "#aindicadorurbano" ).val();
  var PTR_IND_ART = $( "#aindicadorartistico" ).val();
  var PTR_IND_TIP = $( "#aindicadortipologico" ).val();
  var PTR_IND_TEC = $( "#aindicadortecnologico" ).val();
  var PTR_IND_INT = $( "#aindicadorintegridad" ).val();
  var PTR_IND_HIST = $( "#aindicadorhistcult" ).val();
  var PTR_IND_SIMB = $( "#aindicadorsimbolico" ).val();
  var PTR_NOM_VIA = $( "#anombrevia" ).val();
  var PTR_MAT_VIA = $( "#amateriavia" ).val();
  var PTR_CLASS = $( "#aclasificacion" ).val();
  var PTR_ENT_CALL = $( "#aentrecalles" ).val();

  var g_tipo = 'PTR_CON';
  var PTR_IND_URB = caracteresEspecialesRegistrar(PTR_IND_URB);
  var PTR_IND_ART = caracteresEspecialesRegistrar(PTR_IND_ART);
  var PTR_IND_TIP = caracteresEspecialesRegistrar(PTR_IND_TIP);
  var PTR_IND_TEC = caracteresEspecialesRegistrar(PTR_IND_TEC);
  var PTR_IND_INT = caracteresEspecialesRegistrar(PTR_IND_INT);
  var PTR_IND_HIST = caracteresEspecialesRegistrar(PTR_IND_HIST);
  var PTR_IND_SIMB = caracteresEspecialesRegistrar(PTR_IND_SIMB);

  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOM":"'+PTR_NOM+'","PTR_UBIC":"'+PTR_UBIC+'","PTR_TIP_PROT":"'+PTR_TIP_PROT+'","PTR_NRO_PROT":"'+PTR_NRO_PROT+'","PTR_NUM_INM":"'+PTR_NUM_INM+'","PTR_NUM_PAT":"'+PTR_NUM_PAT+'","PTR_IND_URB":"'+PTR_IND_URB+'","PTR_IND_ART":"'+PTR_IND_ART+'","PTR_IND_TIP":"'+PTR_IND_TIP+'","PTR_IND_TEC":"'+PTR_IND_TEC+'","PTR_IND_INT":"'+PTR_IND_INT+'","PTR_IND_HIST":"'+PTR_IND_HIST+'","PTR_IND_SIMB":"'+PTR_IND_SIMB+'","PTR_NOM_VIA":"'+PTR_NOM_VIA+'","PTR_MAT_VIA":"'+PTR_MAT_VIA+'","PTR_CLASS":"'+PTR_CLASS+'","PTR_ENT_CALL":"'+PTR_ENT_CALL+'"}';

  var data = JSON.stringify(data);
  //////////console.log("datadata",data);
  var cas_nro_caso = $( "#aconnumero_ficha" ).val();
  var cas_nombre_caso = $( "#aconcodigo_ficha" ).val();
  //var cas_nro_caso = 11 ;
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  //var cas_nombre_caso = 'PTR_CON77/2018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-943","parametros":'{"xcas_id":'+cas_id+',"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");

  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );          
        listarPatrimonioCultural(g_tipo); 
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 
});



/*-----------------END CONJUNTOS PATRIMONIALES-----------------------*/

/*-----------------BEGIN ESPACIOS ABIERTOS---------------------------*/
$("#guardarEspaciosAbiertos").click(function(){
  obtenercorrelativo('PTR-ESP');
  setTimeout(function(){

    var PTR_CREADOR = $( "#ccreador" ).val();
    var PTR_ACTUALIZA = $( "#cautorizador" ).val();
    var PTR_COD_CAT = $( "#ccodigocatastral" ).val();
    var PTR_GEOREF = $( "#cgeoreferencia" ).val();
    var PTR_TRAD = $( "#ctradicional" ).val();
    var PTR_ACT = $( "#cactual" ).val();
    var PTR_DEP = $( "#cdepartamento" ).val();
    var PTR_CIPO = $( "#cprovincia" ).val();
    var PTR_MUNI = $( "#cmunicipio" ).val();
    var PTR_MACRO = $( "#cmacrodistritoespabie" ).val();
    var macrodistritoTxt=document.getElementById("cmacrodistritoespabie");
    var PTR_MACRO=macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
    var PTR_BARR = $( "#cbarrio" ).val();
    var PTR_DIR = $( "#cdireccioncalle" ).val();
    var PTR_ESQ1 = $( "#cesquina1" ).val();
    var PTR_ESQ2 = $( "#cesquina2" ).val();
    var PTR_ALT = $( "#caltitud" ).val();
    var PTR_NOMPRO = $( "#cnombrepropietario" ).val();
    var PTR_REPRO = $( "#ctiporegpropietario" ).val();
  ///------------------------------

   /////GRILLA USO SUELO /////////
    
    var contU=0;
    arrDibujoUso.forEach(function (item, index, array) 
    {
      arrCodigoUso[contU].descripU = $("#descUso"+item.id+"").val();
      arrCodigoUso[contU].tradicionalU = document.getElementById("tradicionalUso"+item.id+"").checked;
      arrCodigoUso[contU].actualU = document.getElementById("actuallUso"+item.id+"").checked;
      contU++;

    });

    //console.log("arrCodigoUso",arrCodigoUso);
   
    var bloqueUso = '[ {"tipo": "GRD","campos": "f01_emision_G_SERV_DESC|f01_emision_G_TRAD|f01_emision_G_ACT","titulos": "Descripción|Tradicional|Actual","impresiones": "undefined|undefined|undefined|" }';
   
     for (var i = 0; i<arrCodigoUso.length;i++)
    {
    
    var grillaUsoSuelo = '{"f01_emision_G_ACT": "'+ cambiarBoolAString(arrCodigoUso[i].actualU)+'","f01_emision_G_TRAD": "'+cambiarBoolAString(arrCodigoUso[i].tradicionalU)+'","f01_emision_G_SERV_DESC": "'+arrCodigoUso[i].descripU+'","f01_emision_G_SERV_DESC_valor": "3"}';
    bloqueUso = (bloqueUso.concat(',').concat(grillaUsoSuelo));
    }

    bloqueUso = bloqueUso.concat(']');
    //console.log("bloqueUso",bloqueUso);
    var PTR_USO = bloqueUso; 
   

    

/////FIN GRILLA USO SUELO ///////
   /////GRILLA SERVICIOS /////////
    
   var contU=0;
    arrDibujoSer.forEach(function (item, index, array) 
    {
      arrCodigoSer[contU].descServicio = $("#descServicio"+item.id+"").val();
      arrCodigoSer[contU].proveedorServicio = $("#proveedorServicio"+item.id+"").val();
      
      contU++;
    });

    //console.log("arrCodigoSer",arrCodigoSer);
   
    var bloqueServicio = '[{"tipo": "GRD","campos": "f01_emision_G_SERV_DESC|f01_emision_G_SERV_PROV","titulos": "Descripción|Proveedor","impresiones": "undefined|undefined|"}';
   
     for (var i = 0; i<arrCodigoSer.length;i++)
    {
    
    var grillaServicio = '{"f01_emision_G_SERV_DESC": "'+arrCodigoSer[i].descServicio+'","f01_emision_G_SERV_PROV": "'+arrCodigoSer[i].proveedorServicio+'","f01_emision_G_SERV_DESC_valor": "3","f01_emision_G_SERV_PROV_valor": "REDP"}';
    bloqueServicio = (bloqueServicio.concat(',').concat(grillaServicio));
    }

    bloqueServicio = bloqueServicio.concat(']');
    //console.log("bloqueServicio",bloqueServicio);
      

    var PTR_SERV          = bloqueServicio;

/////FIN GRILLA SERVICIOS ///////


//var PTR_OTHER         = $("#cregprootrosservicios").val();
var ptr_other1 = document.getElementById("cregprootrosservicios1").checked;
var ptr_other2 = document.getElementById("cregprootrosservicios2").checked;
var ptr_other3 = document.getElementById("cregprootrosservicios3").checked;
var ptr_other_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_other1+', "resvalor": "Internet"},{"resid": 285, "estado": '+ptr_other2+', "resvalor": "Teléfono"},{"resid": 286, "estado": '+ptr_other3+', "resvalor": "TV Cable"}]';
////////console.log(888888888888, PTR_OTHER);
//var PTR_FIGPRO        = $("#cmarlegfiguraproyeccion").val();
var figuraproteccionTxt=document.getElementById("cmarlegfiguraproyeccion");
var PTR_FIGPRO=figuraproteccionTxt.options[figuraproteccionTxt.selectedIndex].text;

//var PTR_REFHIST       = $("#crefhisreferenciahistorica").val();
var referenciahistoricaTxt=document.getElementById("crefhisreferenciahistorica");
var PTR_REFHIST=referenciahistoricaTxt.options[referenciahistoricaTxt.selectedIndex].text;

////console.log('ffffffffffffffffffffffffff', PTR_REFHIST);

var PTR_FECCONS       = $("#cdatorafechaconstruccion").val();
var PTR_AUTOR         = $("#cdatorautorconstructor").val();
var PTR_PREEX         = $("#cdatorapreexistenciaedificacion").val();
var PTR_PREEX = caracteresEspecialesRegistrar(PTR_PREEX);

var PTR_PROP          = $("#cdatorapropietario").val();
var PTR_USOORG        = $("#cdatorausosoriginales").val();
var PTR_USOORG = caracteresEspecialesRegistrar(PTR_USOORG);

var PTR_HECHIS        = $("#cdatorahechoshistoricosdist").val();
var PTR_HECHIS = caracteresEspecialesRegistrar(PTR_HECHIS);

var PTR_FIEREL        = $("#cdatorafiestasreligiosas").val();
var PTR_FIEREL = caracteresEspecialesRegistrar(PTR_FIEREL);

var PTR_DATHISC       = $("#cdatoradatoshistoricos").val();
var PTR_DATHISC = caracteresEspecialesRegistrar(PTR_DATHISC);

var PTR_FUENTE        = $("#cdatorafuentebibliografia").val();
var PTR_FUENTE = caracteresEspecialesRegistrar(PTR_FUENTE);

//V. DATOS DOCUMENTALES  
var PTR_FECCONST      = $("#cdatdocfechadeconstruccion").val();
var PTR_AUTCONST      = $("#cdatdocautoroconstructor").val();
var PTR_PREDIN        = $("#cdatdocpreexistenciainvestigacion").val();
var PTR_PREDIN = caracteresEspecialesRegistrar(PTR_PREDIN);

var PTR_PROPIE        = $("#cdatdocpropietario").val();
var PTR_USOPREEX      = $("#cdatdocusospreexistentes").val();
var PTR_USOPREEX = caracteresEspecialesRegistrar(PTR_USOPREEX);

var PTR_HECHISD       = $("#cdatdochechoshistoricospers").val();
var PTR_HECHISD = caracteresEspecialesRegistrar(PTR_HECHISD);

var PTR_FIERELD       = $("#cdatdocfiestasreli").val();
var PTR_FIERELD = caracteresEspecialesRegistrar(PTR_FIERELD);

var PTR_DATHISTC      = $("#cdatdocdatoshistoricosconj").val();
var PTR_DATHISTC = caracteresEspecialesRegistrar(PTR_DATHISTC);

var PTR_FUENTBIO      = $("#cdatdocfuentebiblio").val();
var PTR_FUENTBIO = caracteresEspecialesRegistrar(PTR_FUENTBIO);

//VI. INSCRIPCIONES         
var PTR_INSCR         = $("#cinsinscripciones").val();
var PTR_INSCR = caracteresEspecialesRegistrar(PTR_INSCR);

//VII. ÉPOCA   
//var PTR_EPOCA         = $("#cepoepoca").val();
var epocaTxt=document.getElementById("cepoepoca");
var PTR_EPOCA=epocaTxt.options[epocaTxt.selectedIndex].text;

var PTR_ACONS         = $("#cepoanoconstruccion").val();
var PTR_AULTR         = $("#cepoanoremodelacion").val();
//VII. ESTILO    
//var PTR_ESTILO        = $("#cestestilo").val();
var estiloTxt=document.getElementById("cestestilo");
var PTR_ESTILO=estiloTxt.options[estiloTxt.selectedIndex].text;

//IX. DESCRIPCIÓN DEL AREA
//var PTR_UBICESP       = $("#cdesareubicacionespacio").val();
var ubicacionespacioareaTxt=document.getElementById("cdesareubicacionespacio");
var PTR_UBICESP=ubicacionespacioareaTxt.options[ubicacionespacioareaTxt.selectedIndex].text;

//var PTR_LINCONS       = $("#cdesarelineaconstruccion").val();
var lineacontruccionTxt=document.getElementById("cdesarelineaconstruccion");
var PTR_LINCONS=lineacontruccionTxt.options[lineacontruccionTxt.selectedIndex].text;

//var PTR_TIPOLO        = $("#cdesaretipologia").val();
var tipologiaTxt=document.getElementById("cdesaretipologia");
var PTR_TIPOLO=tipologiaTxt.options[tipologiaTxt.selectedIndex].text;

//var PTR_MURMAT        = $("#cdesarematerial").val();

var ptr_murmat1 = document.getElementById("cdesarematerial1").checked;
var ptr_murmat2 = document.getElementById("cdesarematerial2").checked;
var ptr_murmat3 = document.getElementById("cdesarematerial3").checked;
var ptr_murmat4 = document.getElementById("cdesarematerial4").checked;
var ptr_murmat5 = document.getElementById("cdesarematerial5").checked;
var ptr_murmat6 = document.getElementById("cdesarematerial6").checked;
var ptr_murmat7 = document.getElementById("cdesarematerial7").checked;
var ptr_murmat8 = document.getElementById("cdesarematerial8").checked;
var ptr_murmat9 = document.getElementById("cdesarematerial9").checked;
var ptr_murmat10 = document.getElementById("cdesarematerial10").checked;

var ptr_murmat_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_murmat1+', "resvalor": "Adobe"},{"resid": 278, "estado": '+ptr_murmat2+', "resvalor": "Concreto Armado"},{"resid": 279, "estado": '+ptr_murmat3+', "resvalor": "Hormigon Armado"},{"resid": 280, "estado": '+ptr_murmat4+', "resvalor": "Ladrillo de Barro"},{"resid": 281, "estado": '+ptr_murmat5+', "resvalor": "Madera"},{"resid": 282, "estado": '+ptr_murmat6+', "resvalor": "Otros"},{"resid": 283, "estado": '+ptr_murmat7+', "resvalor": "Piedra"},{"resid": 284, "estado": '+ptr_murmat8+', "resvalor": "Piedra Cal"},{"resid": 285, "estado": '+ptr_murmat9+', "resvalor": "Prefabricados"},{"resid": 286, "estado": '+ptr_murmat10+', "resvalor": "Tapial"}]';
////console.log(123456789,ptr_murmat_check);

var PTR_MURMAT_OTROS  = $("#cdesareotros").val();

//var PTR_MUREST        = $("#cdesareestado").val();
var estadootrosTxt=document.getElementById("cdesareestado");
var PTR_MUREST=estadootrosTxt.options[estadootrosTxt.selectedIndex].text;

//var PTR_MURINTE       = $("#cdesareintegridad").val();
var cdesareintegridadTxt=document.getElementById("cdesareintegridad");
var PTR_MURINTE=cdesareintegridadTxt.options[cdesareintegridadTxt.selectedIndex].text;
/////BEGIN GRILLA REJAS /////////
    var cont=0;
    arrDibujoRejas.forEach(function (item, index, array) 
    {
      arrCodigoRejas[cont].descripcionR = $("#descripcionRej"+item.id+"").val();
      arrCodigoRejas[cont].hierroR = document.getElementById("hierro"+item.id+"").checked;
      arrCodigoRejas[cont].tuboR = document.getElementById("tubo"+item.id+"").checked;
      arrCodigoRejas[cont].maderaR = document.getElementById("madera"+item.id+"").checked;
      arrCodigoRejas[cont].otrosR = document.getElementById("otros"+item.id+"").checked;
      arrCodigoRejas[cont].otrosDescR = $("#otrosDesc"+item.id+"").val();
      arrCodigoRejas[cont].integridadR = $("#integridRejas"+item.id+"").val();
      cont++;
    });

    //console.log("arrCodigoRejas",arrCodigoRejas);

     var bloqueRejas = '[{"tipo": "GRD","campos": "f01_reja_DES_|f01_reja_HIERRO_|f01_reja_TUBO_|f01_reja_MAD_|f01_reja_OTRO_|f01_reja_OTRODESC_|f01_reja_INT_","titulos": "Descripción|Hierro Forjado|Tubo Industrial|Madera|Otros|Otrosdesc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
    for (var i = 0; i<arrCodigoRejas.length;i++)
    {
      ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
      var grillaRejas = '{"f01_reja_DES_": "'+arrCodigoRejas[i].descripcionR+'","f01_reja_INT_": "'+arrCodigoRejas[i].integridadR+'","f01_reja_MAD_": "'+cambiarBoolAString(arrCodigoRejas[i].maderaR)+'","f01_reja_OTRO_": "'+cambiarBoolAString(arrCodigoRejas[i].otrosR)+'","f01_reja_TUBO_": "'+ cambiarBoolAString(arrCodigoRejas[i].tuboR)+'","f01_reja_HIERRO_": "'+cambiarBoolAString(arrCodigoRejas[i].hierroR)+'","f01_reja_OTRODESC_": "'+arrCodigoRejas[i].otrosDescR+'","f01_reja_DES__valor": "1","f01_reja_INT__valor": "2"}';
      bloqueRejas = (bloqueRejas.concat(',').concat(grillaRejas));

    }
    bloqueRejas = bloqueRejas.concat(']');
    var PTR_REJAS = bloqueRejas;

    //console.log("PTR_REJAS",PTR_REJAS);
/////END GRILLA REJAS/////////

/////BEGIN GRILLA ACABADO /////////
  var cont = 0;
      arrDibujoAcab.forEach(function (item, index, array) 
      {
        arrCodigoAcab[cont].descripcionA = $("#descripcionAcab"+item.id+"").val();
        arrCodigoAcab[cont].calArenaA = document.getElementById("calArena"+item.id+"").checked;
        arrCodigoAcab[cont].cementoA = document.getElementById("cemento"+item.id+"").checked;
        arrCodigoAcab[cont].piedraA = document.getElementById("piedra"+item.id+"").checked;
        arrCodigoAcab[cont].marmolA = document.getElementById("marmol"+item.id+"").checked;
        arrCodigoAcab[cont].maderaA = document.getElementById("madera"+item.id+"").checked;
        arrCodigoAcab[cont].azulejoA = document.getElementById("azulejo"+item.id+"").checked;
        arrCodigoAcab[cont].ladrilloA = document.getElementById("ladrillo"+item.id+"").checked;
        arrCodigoAcab[cont].otroAcabA = document.getElementById("otroAcab"+item.id+"").checked;
        arrCodigoAcab[cont].otroDescAcabA = $("#otrosDescAcab"+item.id+"").val();
        arrCodigoAcab[cont].integridadAcabA = $("#integridAcab"+item.id+"").val();
        cont++;
      });

      //console.log("VectorAcabadoGrillas",arrCodigoAcab);
        var bloqueAcabMuros = '[{"tipo": "GRD","campos": "f01_muro_DES_|f01_muro_CAL_|f01_muro_CEM_|f01_muro_PIE_|f01_muro_MAR_|f01_muro_MAD_|f01_muro_AZU_|f01_muro_LAD_|f01_muro_OTRO_|f01_muro_OTRODESC_|f01_muro_INT_","titulos": "Descripción|Cal y Arena|Cemento y Arena|Piedra|Mármol|Madera|Azulejo|Ladrillo|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';

        for (var i = 0; i<arrCodigoAcab.length;i++)
        {
          ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
          var grillaAcabMuros = '{"f01_muro_AZU_": "'+cambiarBoolAString(arrCodigoAcab[i].azulejoA)+'","f01_muro_CAL_": "'+cambiarBoolAString(arrCodigoAcab[i].calArenaA)+'","f01_muro_CEM_": "'+cambiarBoolAString(arrCodigoAcab[i].cementoA)+'","f01_muro_DES_": "'+arrCodigoAcab[i].descripcionA+'","f01_muro_INT_": "'+arrCodigoAcab[i].integridadAcabA+'","f01_muro_LAD_": "'+cambiarBoolAString(arrCodigoAcab[i].ladrilloA)+'","f01_muro_MAD_": "'+cambiarBoolAString(arrCodigoAcab[i].maderaA)+'","f01_muro_MAR_": "'+cambiarBoolAString(arrCodigoAcab[i].marmolA)+'","f01_muro_PIE_": "'+cambiarBoolAString(arrCodigoAcab[i].piedraA)+'","f01_muro_OTRO_": "'+cambiarBoolAString(arrCodigoAcab[i].otroAcabA)+'","f01_muro_OTRODESC_": "'+arrCodigoAcab[i].otroDescAcabA+'","f01_muro_DES__valor": "3","f01_muro_INT__valor": "3"}';

          bloqueAcabMuros = (bloqueAcabMuros.concat(',').concat(grillaAcabMuros));

        }
        bloqueAcabMuros = bloqueAcabMuros.concat(']');

      var PTR_ACAMUR = bloqueAcabMuros;
      //console.log("PTR_ACAMUR",PTR_ACAMUR);
/////END GRILLA ACABADO/////////
/////END GRILLA PINTURAS/////////

      var cont = 0;
      arrDibujoPinturas.forEach(function (item, index, array) 
      {
        arrCodigoPinturas[cont].descripcionPintu = $("#descripcionPintu"+item.id+"").val();
        arrCodigoPinturas[cont].calArenaPintu = document.getElementById("calArenaPintu"+item.id+"").checked;
        arrCodigoPinturas[cont].acrilicoPintu = document.getElementById("acrilicoPintu"+item.id+"").checked;
        arrCodigoPinturas[cont].aceitePintu = document.getElementById("aceitePintu"+item.id+"").checked;
        arrCodigoPinturas[cont].otroPintu = document.getElementById("otroPintu"+item.id+"").checked;
        arrCodigoPinturas[cont].otrosDescPintu = $("#otrosDescPintu"+item.id+"").val();
        arrCodigoPinturas[cont].integridPintu = $("#integridPintu"+item.id+"").val();
        cont++;
      });

      //console.log("arrCodigoPinturas",arrCodigoPinturas);
       var bloquePinturas = '[{"tipo": "GRD","campos": "f01_pint_COD_|f01_pint_CAL_|f01_pint_ACR_|f01_pint_ACE_|f01_pint_OTRO_|f01_pint_OTRODESC_|f01_pint_INT_","titulos": "Descripción|Cal|Acrílica|Aceite|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';

        for (var i = 0; i<arrCodigoPinturas.length;i++)
        {
          ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
          var grillaPinturas = '{"f01_pint_ACE_": "'+cambiarBoolAString(arrCodigoPinturas[i].aceitePintu)+'","f01_pint_ACR_": "'+cambiarBoolAString(arrCodigoPinturas[i].acrilicoPintu)+'","f01_pint_CAL_": "'+cambiarBoolAString(arrCodigoPinturas[i].calArenaPintu)+'","f01_pint_COD_": "'+arrCodigoPinturas[i].descripcionPintu+'","f01_pint_INT_": "'+arrCodigoPinturas[i].integridPintu+'","f01_pint_OTRO_": "'+cambiarBoolAString(arrCodigoPinturas[i].otroPintu)+'","f01_pint_OTRODESC_": "'+arrCodigoPinturas[i].otrosDescPintu+'","f01_pint_COD__valor": "2","f01_pint_INT__valor": "-1"}';

          bloquePinturas = (bloquePinturas.concat(',').concat(grillaPinturas));

        }
        bloquePinturas = bloquePinturas.concat(']');
       var PTR_PINTURA = bloquePinturas;
       //console.log("PTR_PINTURA",PTR_PINTURA);
/////END GRILLA PINTURAS/////////

//var PTR_REJAEST       = $("#cdesareestadorejas").val();
var cdesareestadorejasTxt=document.getElementById("cdesareestadorejas");
var PTR_REJAEST=cdesareestadorejasTxt.options[cdesareestadorejasTxt.selectedIndex].text;


//var PTR_ACAEST        = $("#cdesareestadoacabadomur").val();
var cdesareestadoacabadomurTxt=document.getElementById("cdesareestadoacabadomur");
var PTR_ACAEST=cdesareestadoacabadomurTxt.options[cdesareestadoacabadomurTxt.selectedIndex].text;


//var PTR_PINTEST       = $("#cdesareestadopint").val();
var cdesareestadopintTxt=document.getElementById("cdesareestadopint");
var PTR_PINTEST=cdesareestadopintTxt.options[cdesareestadopintTxt.selectedIndex].text;

///-- Grillas juan inicio --\\\
///--- GRILLA Puntos Colineales --- \\\
var cont = 0;
 arrDibujoPuntosColineales.forEach(function (item, index, array){
    arrCodigoPuntosColineales[cont].NroDescPuntosColineales = $("#NroDescPuntosColineales"+item.id+"").val();
    arrCodigoPuntosColineales[cont].PuntoColinealPuntosColineales = $("#PuntoColinealPuntosColineales"+item.id+"").val();
    cont++;
  });
var bloquePuntosColiniales = '[{"tipo": "GRD","campos": "f01_Nro|f01_Pto_Colineal","titulos": "Nro|Punto Colineal","impresiones": "undefined|undefined|"}';
for (var i = 0; i<arrCodigoPuntosColineales.length;i++) {
  var grillaPuntosColiniales = '  {"f01_Nro":"'+  arrCodigoPuntosColineales[i].NroDescPuntosColineales+'", "f01_Pto_Colineal":"'+  arrCodigoPuntosColineales[i].PuntoColinealPuntosColineales+'" }';
  bloquePuntosColiniales = (bloquePuntosColiniales.concat(',').concat(grillaPuntosColiniales));
}
bloquePuntosColiniales = bloquePuntosColiniales.concat(']');
var PTR_DOT_S = bloquePuntosColiniales;
///---FIN GRILLA Puntos Colineales--- \\\
///--- GRILLA PISOS --- \\\
var cont = 0;
arrDibujoPiso.forEach(function (item, index, array){
  arrCodigoPiso[cont].descripcionPisoA = $("#descripcionPiso"+item.id+"").val();     
  arrCodigoPiso[cont].baldosasPisoA = document.getElementById("baldosasPiso"+item.id+"").checked; 
  arrCodigoPiso[cont].mosaicosPisoA = document.getElementById("mosaicosPiso"+item.id+"").checked;
  arrCodigoPiso[cont].porcelanatoPisoA = document.getElementById("porcelanatoPiso"+item.id+"").checked;
  arrCodigoPiso[cont].plajaPisoA = document.getElementById("plajaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].prioPisoA = document.getElementById("prioPiso"+item.id+"").checked; 
  arrCodigoPiso[cont].ladrillosPisoA = document.getElementById("ladrillosPiso"+item.id+"").checked;
  arrCodigoPiso[cont].gramaPisoA = document.getElementById("gramaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].tierraPisoA = document.getElementById("tierraPiso"+item.id+"").checked;
  arrCodigoPiso[cont].concretoPisoA = document.getElementById("concretoPiso"+item.id+"").checked; 
  arrCodigoPiso[cont].ceramicaPisoA = document.getElementById("ceramicaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].maderaPisoA = document.getElementById("maderaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].otrosPisoA = document.getElementById("otrosPiso"+item.id+"").checked;
  arrCodigoPiso[cont].otroDescPisoA = $("#otrosDescPiso"+item.id+"").val();
  arrCodigoPiso[cont].integridadPisoA = $("#integridPiso"+item.id+"").val();
  cont++;
});
//console.log("arrCodigoPiso",arrCodigoPiso);
var bloquePisos = '[{"tipo": "GRD","campos": "f01_pis_DES_|f01_pis_BAL_|f01_pis_MOS_|f01_pis_POR_|f01_pis_LAJ_|f01_pis_RIO_|f01_pis_LAD_|f01_pis_GRA_|f01_pis_TIE_|f01_pis_CON_|f01_pis_CER_|f01_pis_MAD_|f01_pis_OTRO_|f01_pis_OTRODESC_|f01_pis_INT_"}';

for (var i = 0; i<arrCodigoPiso.length;i++) {
  var grillaPisos = '{ "f01_pis_BAL_":"'+ cambiarBoolAString(arrCodigoPiso[i].baldosasPisoA)+'","f01_pis_CER_":"'+ cambiarBoolAString(arrCodigoPiso[i].ceramicaPisoA)+'", "f01_pis_CON_":"'+ cambiarBoolAString(arrCodigoPiso[i].concretoPisoA)+'","f01_pis_DES_":"'+ arrCodigoPiso[i].descripcionPisoA+'","f01_pis_GRA_":"'+ cambiarBoolAString(arrCodigoPiso[i].gramaPisoA)+'","f01_pis_INT_":"'+ arrCodigoPiso[i].integridadPisoA+'","f01_pis_LAD_":"'+ cambiarBoolAString(arrCodigoPiso[i].ladrillosPisoA)+'","f01_pis_LAJ_":"'+ cambiarBoolAString(arrCodigoPiso[i].plajaPisoA)+'","f01_pis_MAD_":"'+ cambiarBoolAString(arrCodigoPiso[i].maderaPisoA)+'","f01_pis_MOS_":"'+cambiarBoolAString(arrCodigoPiso[i].mosaicosPisoA)+'","f01_pis_POR_":"'+ cambiarBoolAString(arrCodigoPiso[i].porcelanatoPisoA)+'", "f01_pis_RIO_":"'+ cambiarBoolAString(arrCodigoPiso[i].prioPisoA)+'","f01_pis_TIE_":"'+ cambiarBoolAString(arrCodigoPiso[i].tierraPisoA)+'","f01_pis_OTRO_":"'+ cambiarBoolAString(arrCodigoPiso[i].otrosPisoA)+'","f01_pis_OTRODESC_":"'+ arrCodigoPiso[i].otroDescPisoA+'","f01_pis_DES__valor":"0", "f01_pis_INT__valor":"0"}';
  bloquePisos = (bloquePisos.concat(',').concat(grillaPisos));
}
bloquePisos = bloquePisos.concat(']');
var PTR_PISOS = bloquePisos;

//console.log("PTR_PISOooooosss",PTR_PISOS);
///---FIN GRILLA PISOS --- \\\

///--- GRILLA puertas de acesoS --- \\\
var cont = 0;
arrDibujoPuertasAcceso.forEach(function (item, index, array) {
  arrCodigoPuertasAcceso[cont].descripcionPuertasAcceso = $("#descripcionPuertasAcceso"+item.id+"").val();     
  arrCodigoPuertasAcceso[cont].maderaPuertaAcceso = document.getElementById("maderaPuertasAcceso"+item.id+"").checked; 
  arrCodigoPuertasAcceso[cont].metalPuertaAcceso = document.getElementById("metalPuertasAcceso"+item.id+"").checked;
  arrCodigoPuertasAcceso[cont].otrosPuertaAcceso = document.getElementById("otrosPuertasAcceso"+item.id+"").checked;
  arrCodigoPuertasAcceso[cont].otroDescPuertasAccesoA = $("#otrosDescPuertasAcceso"+item.id+"").val();
  arrCodigoPuertasAcceso[cont].integridadPuertasAccesoA = $("#integridPuertasAcceso"+item.id+"").val();
  cont++;
});
var bloquePuertasAcceso = '[{ "tipo": "GRD", "campos": "f01_pue_DES_|f01_pue_MAD_|f01_pue_MET_|f01_pue_OTRO_|f01_pue_OTRODESC_|f01_pue_INT_", "titulos": "Descripción|Madera|Metal|Otros|Otros desc.|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|"}';

for (var i = 0; i<arrDibujoPuertasAcceso.length;i++) {
  var grillaPuertasAcceso = '{"f01_pue_DES_": "'+ arrCodigoPuertasAcceso[i].descripcionPuertasAcceso+'","f01_pue_INT_": "'+ arrCodigoPuertasAcceso[i].integridadPuertasAccesoA+'","f01_pue_MAD_": "'+ cambiarBoolAString(arrCodigoPuertasAcceso[i].maderaPuertaAcceso)+'","f01_pue_MET_": "'+ cambiarBoolAString(arrCodigoPuertasAcceso[i].metalPuertaAcceso)+'","f01_pue_OTRO_": "'+ cambiarBoolAString(arrCodigoPuertasAcceso[i].otrosPuertaAcceso)+'","f01_pue_OTRODESC_": "'+ arrCodigoPuertasAcceso[i].otroDescPuertasAccesoA+'","f01_pue_DES__valor":"0","f01_pue_INT__valor":"0"}'; 

  bloquePuertasAcceso = (bloquePuertasAcceso.concat(',').concat(grillaPuertasAcceso));
}
bloquePuertasAcceso = bloquePuertasAcceso.concat(']');
var PTR_PUERTA = bloquePuertasAcceso;

//console.log("boqueGrilla",bloquePuertasAcceso)
///---FIN GRILLA puertas de acesoS --- \\\

///--- GRILLA Mobiliario Urbano --- \\\
var cont = 0;
arrDibujoMovilidadUrbano.forEach(function (item, index, array) {
  arrCodigoMovilidadUrbano[cont].descripcionMovilidadUrbano = $("#descripcionMovilidadUrbano"+item.id+"").val();     
  arrCodigoMovilidadUrbano[cont].maderaMovilidadUrbano = document.getElementById("madera"+item.id+"").checked; 
  arrCodigoMovilidadUrbano[cont].metalMovilidadUrbano = document.getElementById("metal"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].barroMovilidadUrbano = document.getElementById("barro"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].calMovilidadUrbano = document.getElementById("cal"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].ladrilloMovilidadUrbano = document.getElementById("ladrillo"+item.id+"").checked; 
  arrCodigoMovilidadUrbano[cont].piedraMovilidadUrbano = document.getElementById("piedra"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].adobeMovilidadUrbano = document.getElementById("adobe"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].concretoMovilidadUrbano = document.getElementById("concreto"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].fcarbonMovilidadUrbano = document.getElementById("fcarbon"+item.id+"").checked; 
  arrCodigoMovilidadUrbano[cont].vidrioMovilidadUrbano = document.getElementById("vidrio"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].fibrocemMovilidadUrbano = document.getElementById("fibrocem"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].otrosMovilidadUrbano = document.getElementById("otros"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].otroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDescMovilidadUrbano"+item.id+"").val();
  arrCodigoMovilidadUrbano[cont].integridadMovilidadUrbanoMovilidadUrbano = $("#integridMovilidadUrbano"+item.id+"").val();
  cont++;
});
var bloqueMovilidadUrbano = '[ {"tipo": "GRD","campos": "f01_mob_DES_|f01_mob_MAD_|f01_mob_MET_|f01_mob_BARR_|f01_mob_CAL_|f01_mob_LAD_|f01_mob_PIE_|f01_mob_ADO_|f01_mob_CON_|f01_mob_CAR_|f01_mob_VID_|f01_mob_FIBR_|f01_mob_OTRO_|f01_mob_OTRODESC_|f01_mob_INT_","titulos": "Descripción|Madera|Metal|Barro|Cal|Ladrillo|Piedra|Adobe|Concreto|F. Carbono|Vidrio|Fibrocem|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
for (var i = 0; i<arrDibujoMovilidadUrbano.length;i++) {
  var grillaMovilidadUrbano = ' { "f01_mob_ADO_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].adobeMovilidadUrbano) +'", "f01_mob_CAL_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].calMovilidadUrbano) +'", "f01_mob_CAR_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].fcarbonMovilidadUrbano) +'", "f01_mob_CON_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].concretoMovilidadUrbano) +'", "f01_mob_DES_": "'+ arrCodigoMovilidadUrbano[i].descripcionMovilidadUrbano +'", "f01_mob_INT_": "'+ arrCodigoMovilidadUrbano[i].integridadMovilidadUrbanoMovilidadUrbano +'", "f01_mob_LAD_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].ladrilloMovilidadUrbano) +'", "f01_mob_MAD_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].maderaMovilidadUrbano) +'", "f01_mob_MET_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].metalMovilidadUrbano) +'", "f01_mob_PIE_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].piedraMovilidadUrbano) +'", "f01_mob_VID_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].vidrioMovilidadUrbano) +'", "f01_mob_BARR_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].barroMovilidadUrbano) +'", "f01_mob_FIBR_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].fibrocemMovilidadUrbano) +'", "f01_mob_OTRO_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].otrosMovilidadUrbano) +'", "f01_mob_OTRODESC_": "'+ arrCodigoMovilidadUrbano[i].otroDescMovilidadUrbanoMovilidadUrbano +'", "f01_mob_DES__valor":0 , "f01_mob_INT__valor":0 }';
  bloqueMovilidadUrbano = (bloqueMovilidadUrbano.concat(',').concat(grillaMovilidadUrbano));
}
bloqueMovilidadUrbano = bloqueMovilidadUrbano.concat(']');
var PTR_MOBURB = bloqueMovilidadUrbano;
///---FIN GRILLA Mobiliario Urbano --- \\\

///--- GRILLA Escaleras --- \\\
var cont = 0;
arrDibujoEscaleras.forEach(function (item, index, array) {
  arrCodigoEscaleras[cont].descripcionEscaleras = $("#descripcionEscaleras"+item.id+"").val();     
  arrCodigoEscaleras[cont].maderaEscaleras = document.getElementById("maderaEscaleras"+item.id+"").checked; 
  arrCodigoEscaleras[cont].metalEscaleras = document.getElementById("metalEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].ConcretoEscaleras = document.getElementById("ConcretoEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].PiedraEscaleras = document.getElementById("PiedraEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].ladrilloEscaleras = document.getElementById("ladrilloEscaleras"+item.id+"").checked; 
  arrCodigoEscaleras[cont].tierraEscaleras = document.getElementById("tierraEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].otrosEscaleras = document.getElementById("otrosEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].otroDescEscaleras = $("#otrosDescEscaleras"+item.id+"").val();
  arrCodigoEscaleras[cont].integridadEscaleras = $("#integridEscaleras"+item.id+"").val();
  cont++;
});
var bloqueEscaleras = '[ { "tipo": "GRD", "campos": "f01_esc_DES_|f01_esc_MAD_|f01_esc_MET_|f01_esc_CON_|f01_esc_PIE_|f01_esc_LAD_|f01_esc_TIE_|f01_esc_OTRO_|f01_esc_OTRODESC_|f01_esc_INT_", "titulos": "Descripción|Madera|Metal|Concreto|Piedra|Ladrillo|Tierra|Otro|Otro desc|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
for (var i = 0; i<arrDibujoEscaleras.length;i++) {
  var grillaEscaleras = '{ "f01_esc_CON_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].ConcretoEscaleras)+'", "f01_esc_DES_":"'+ arrCodigoEscaleras[i].descripcionEscaleras+'", "f01_esc_INT_":"'+ arrCodigoEscaleras[i].integridadEscaleras+'", "f01_esc_LAD_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].ladrilloEscaleras)+'", "f01_esc_MAD_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].maderaEscaleras)+'", "f01_esc_MET_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].metalEscaleras)+'", "f01_esc_PIE_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].PiedraEscaleras)+'", "f01_esc_TIE_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].tierraEscaleras)+'", "f01_esc_OTRO_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].otrosEscaleras)+'", "f01_esc_OTRODESC_":"'+ arrCodigoEscaleras[i].otroDescEscaleras+'", "f01_esc_DES__valor":0, "f01_esc_INT__valor":0 }';
  bloqueEscaleras = (bloqueEscaleras.concat(',').concat(grillaEscaleras));
}
bloqueEscaleras = bloqueEscaleras.concat(']');
var PTR_ESCAL = bloqueEscaleras;

///---FIN GRILLA Escaleras --- \\\

///--- GRILLA CARACTERISTICAS DE LA VEGETACION--- \\\
var cont = 0;
arrDibujoCarVegetacion.forEach(function (item, index, array) {
  arrCodigoCarVegetacion[cont].descripcionCarVegetacion = $("#descripcionCarVegetacion"+item.id+"").val();     
  arrCodigoCarVegetacion[cont].cantidadCarVegetacion = $("#cantidadCarVegetacion"+item.id+"").val();
  arrCodigoCarVegetacion[cont].sinFloracionCarVegetacion = document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked;
  arrCodigoCarVegetacion[cont].caducaCarVegetacion = document.getElementById("caducaCarVegetacion"+item.id+"").checked; 
  arrCodigoCarVegetacion[cont].perenneCarVegetacion = document.getElementById("perenneCarVegetacion"+item.id+"").checked;
  arrCodigoCarVegetacion[cont].otrosCarVegetacion = document.getElementById("otrosCarVegetacion"+item.id+"").checked;
  arrCodigoCarVegetacion[cont].otroDescCarVegetacion = $("#otrosDescCarVegetacion"+item.id+"").val();
  arrCodigoCarVegetacion[cont].integridadCarVegetacion = $("#integridCarVegetacion"+item.id+"").val();
  cont++;
});
var bloqueCarVegetacion = '[{"tipo": "GRD","campos": "f01_veg_DES_|f01_veg_CAN_|f01_veg_SFL_|f01_veg_CAD_|f01_veg_PER_|f01_veg_OTRO_|f01_veg_OTRODESC_|f01_veg_INT_","titulos": "Descripción|Cantidad|Sin Floracion|Caduca|Perenne|Otro|Otro desc|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|" }';
for (var i = 0; i<arrDibujoCarVegetacion.length;i++) {
  var grillaCarVegetacion = ' {"f01_veg_CAD_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].cantidadCarVegetacion)+'","f01_veg_CAN_":"'+ arrCodigoCarVegetacion[i].cantidadCarVegetacion+'","f01_veg_DES_":"'+ arrCodigoCarVegetacion[i].descripcionCarVegetacion+'","f01_veg_INT_":"'+ arrCodigoCarVegetacion[i].integridadCarVegetacion+'","f01_veg_PER_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].perenneCarVegetacion)+'","f01_veg_SFL_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].sinFloracionCarVegetacion)+'","f01_veg_OTRO_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].otrosCarVegetacion)+'","f01_veg_OTRODESC_":"'+ arrCodigoCarVegetacion[i].otroDescCarVegetacion+'","f01_veg_DES__valor":0, "f01_veg_INT__valor":0}';
  bloqueCarVegetacion = (bloqueCarVegetacion.concat(',').concat(grillaCarVegetacion));
}
bloqueCarVegetacion = bloqueCarVegetacion.concat(']');
var PTR_VEGET = bloqueCarVegetacion;

///---FIN GRILLA CARCATERISTICAS DE LA VEGETACION --- \\\

///--- GRILLA Detalles Artisitcos--- \\\
var cont = 0;
arrDibujoDetaArtistoco.forEach(function (item, index, array) {
  arrCodigoDetaArtistoco[cont].descripcionDetaArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
  arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
  cont++;
});
var bloqueDetaArtistoco = '[{ "tipo": "GRD", "campos": "f01_det_DES_|f01_det_INT_", "titulos": "Descripción|Integridad", "impresiones": "undefined|undefined|"}';
for (var i = 0; i<arrDibujoDetaArtistoco.length;i++) {
  var grillaDetaArtistoco = ' { "f01_det_DES_":"'+arrCodigoDetaArtistoco[i]+'", "f01_det_INT_":"'+arrCodigoDetaArtistoco[i]+'", "f01_det_DES__valor":"'+arrCodigoDetaArtistoco[i]+'", "f01_det_INT__valor":"'+arrCodigoDetaArtistoco[i]+'"}';
  bloqueDetaArtistoco = (bloqueDetaArtistoco.concat(',').concat(grillaDetaArtistoco));
}
bloqueDetaArtistoco = bloqueDetaArtistoco.concat(']');
var PTR_DETALLES = bloqueDetaArtistoco;
///---FIN GRILLA Detalles Artisitcos--- \\\

/// Grillas juan Fin \\\

//var PTR_PISOEST       = $("#cdesareestadopisos").val();
var cdesareestadopisosTxt=document.getElementById("cdesareestadopisos");
var PTR_PISOEST=cdesareestadopisosTxt.options[cdesareestadopisosTxt.selectedIndex].text;

//var PTR_PUERTAEST     = $("#cdesareestadopuertasacc").val();
var cdesareestadopuertasaccTxt=document.getElementById("cdesareestadopuertasacc");
var PTR_PUERTAEST=cdesareestadopuertasaccTxt.options[cdesareestadopuertasaccTxt.selectedIndex].text;

//var PTR_MOBEST        = $("#cdesareestadomoviliariourb").val();
var cdesareestadomoviliariourbTxt=document.getElementById("cdesareestadomoviliariourb");
var PTR_MOBEST=cdesareestadomoviliariourbTxt.options[cdesareestadomoviliariourbTxt.selectedIndex].text;

//var PTR_ESCEST        = $("#cdesareestadoescaleras").val();
var cdesareestadoescalerasTxt=document.getElementById("cdesareestadoescaleras");
var PTR_ESCEST=cdesareestadoescalerasTxt.options[cdesareestadoescalerasTxt.selectedIndex].text;


//var PTR_VEGEST        = $("#cdesareestadocaracteristicasvege").val();
var cdesareestadocaracteristicasvegeTxt=document.getElementById("cdesareestadocaracteristicasvege");
var PTR_VEGEST=cdesareestadocaracteristicasvegeTxt.options[cdesareestadocaracteristicasvegeTxt.selectedIndex].text;


//var PTR_DETALLEST     = $("#cdesareestadodetalleartistico").val();
var cdesareestadodetalleartisticoTxt=document.getElementById("cdesareestadodetalleartistico");
var PTR_DETALLEST=cdesareestadodetalleartisticoTxt.options[cdesareestadodetalleartisticoTxt.selectedIndex].text;

//x. ESQUEMAS
var PTR_ESQDES        =$("#cesqesquemas").val();
var PTR_ESQDES = caracteresEspecialesRegistrar(PTR_ESQDES);

//XI. ANGULO VISUAL HORIZONTAL
var PTR_ANGOR         = $("#cangvisorientacion").val();
var PTR_ANGGRA        = $("#cangvisgrados").val();
//var PTR_VISTAS        = $("#cangvisvistas").val();
var ptr_vistas1 = document.getElementById("cangvisvistas1").checked;
var ptr_vistas2 = document.getElementById("cangvisvistas2").checked;

var ptr_vistas_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_vistas1+', "resvalor": "Vista Parcial  "},{"resid": 271, "estado": '+ptr_vistas2+', "resvalor": "Vista Panorámica"}]';

var PTR_ANGOTR        = $("#cangvisotros").val();
//XII. ELEMENTOS VISUALES
var PTR_ELEMVISID     = $("#celevisizquierda_derecha").val();
var PTR_ELEMVISID = caracteresEspecialesRegistrar(PTR_ELEMVISID);

var PTR_ELEMVISAL     = $("#celivisalto").val();
var PTR_ELEMVISAL = caracteresEspecialesRegistrar(PTR_ELEMVISAL);

var PTR_ELEMVISAB     = $("#celivisbajo").val();
var PTR_ELEMVISAB = caracteresEspecialesRegistrar(PTR_ELEMVISAB);

//XIII. DESCRIPCIÓN DEL ENTORNO
var PTR_DESENTEI      = $("#cdesententorno_inmediato").val();
var PTR_DESENTEI = caracteresEspecialesRegistrar(PTR_DESENTEI);

//XIV. DESCRIPCIÓN DEL ESPACIO
var PTR_DESESPDES     = $("#cdesespdescrpcion_espacio").val();
var PTR_DESESPDES = caracteresEspecialesRegistrar(PTR_DESESPDES);

//XV. MEDIDAS 
var PTR_MEDIDAREA     = $("#cmedarea_total").val();
var PTR_MEDIDANC      = $("#cmedancho").val();
var PTR_MEDIDLAR      = $("#cmedlargo").val();
var PTR_MEDIAMC       = $("#cmedancho_muro").val();
//XVI. INFORMACIÓN ADICIONAL
//var PTR_INFADICIONAL  = $("#cinformacion_adicional").val();
var cinformacion_adicionalTxt=document.getElementById("cinformacion_adicional");
var PTR_INFADICIONAL=cinformacion_adicionalTxt.options[cinformacion_adicionalTxt.selectedIndex].text;

var PTR_INFOBS        = $("#cinfadicobservaciones").val();
var PTR_INFOBS = caracteresEspecialesRegistrar(PTR_INFOBS);

//XVIII. PATOLOGIAS  
//var PTR_PATODAN       = $("#cpatdanos").val();
var ptr_patodan1 = document.getElementById("cpatdanos1").checked;
var ptr_patodan2 = document.getElementById("cpatdanos2").checked;
var ptr_patodan3 = document.getElementById("cpatdanos3").checked;
var ptr_patodan4 = document.getElementById("cpatdanos4").checked;
var ptr_patodan5 = document.getElementById("cpatdanos5").checked;
var ptr_patodan6 = document.getElementById("cpatdanos6").checked;

var ptr_patodan_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patodan1+', "resvalor": "Asentamientos"},{"resid": 278, "estado": '+ptr_patodan2+', "resvalor": "Desplomes"},{"resid": 279, "estado": '+ptr_patodan3+', "resvalor": "Desprendimientos"},{"resid": 279, "estado": '+ptr_patodan4+', "resvalor": "Humedad"},{"resid": 280, "estado": '+ptr_patodan5+', "resvalor": "Otros"},{"resid": 281, "estado": '+ptr_patodan6+', "resvalor": "Podredumbre"}]';

var PTR_PATO_OTROS    = $("#cpatotros").val();
var PTR_PATOOBS       = $("#cpatobservaciones").val();
var PTR_PATOOBS = caracteresEspecialesRegistrar(PTR_PATOOBS);

//var PTR_PATOCAU       = $("#cpatcausas").val();
var ptr_patocau1 = document.getElementById("cpatcausas1").checked;
var ptr_patocau2 = document.getElementById("cpatcausas2").checked;
var ptr_patocau3 = document.getElementById("cpatcausas3").checked;
var ptr_patocau4 = document.getElementById("cpatcausas4").checked;
var ptr_patocau5 = document.getElementById("cpatcausas5").checked;
var ptr_patocau6 = document.getElementById("cpatcausas6").checked;
var ptr_patocau7 = document.getElementById("cpatcausas7").checked;



var ptr_patocau_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patocau1+', "resvalor": "Abandono"},{"resid": 278, "estado": '+ptr_patocau2+', "resvalor": "Botánicos"},{"resid": 279, "estado": '+ptr_patocau3+', "resvalor": "Calidad de los materiales"},{"resid": 280, "estado": '+ptr_patocau4+', "resvalor": "Catastrofe Natural"},{"resid": 281, "estado": '+ptr_patocau5+', "resvalor": "Deficiencias constructivas"},{"resid": 282, "estado": '+ptr_patocau6+', "resvalor": "Falta de mantenimiento"},{"resid": 283, "estado": '+ptr_patocau7+', "resvalor": "Otros"}]';

var PTR_PATOCAU_OTROS = $("#cpatotros ").val();
//XIX. VALORACIÓN  
//var PTR_VALORHIS      = $("#cvalhistorico").val();
var ptr_valorhis1 = document.getElementById("cvalhistorico1").checked;
var ptr_valorhis2 = document.getElementById("cvalhistorico2").checked;
var ptr_valorhis3 = document.getElementById("cvalhistorico3").checked;
var ptr_valorhis4 = document.getElementById("cvalhistorico4").checked;
var ptr_valorhis5 = document.getElementById("cvalhistorico5").checked;

var ptr_valorhis_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorhis1+', "resvalor": "Espacio de valor testimonial y documental que ilustra el desarrollo político, social, religioso, cultural, económico y de forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 278, "estado": '+ptr_valorhis2+', "resvalor": "Espacio que desde su creación se constituye en un hito en la memoria histórica"},{"resid": 279, "estado": '+ptr_valorhis3+', "resvalor": "Espacio que en el trascurso histórico se constituyo en hito, al ser escenario relacionado con personas o eventos importantes, dentro del proceso histórico, social, cultural, económico de las comunidades"},{"resid": 280, "estado": '+ptr_valorhis4+', "resvalor": "Espacios que forman parte de un conjunto histórico"},{"resid": 281, "estado": '+ptr_valorhis5+', "resvalor": "Espacio que posee elementos arqueológicos que documentan su evolución y desarrollo"}]';

//var PTR_VALORART      = $("#cvalartistico").val();
var ptr_valorart1 = document.getElementById("cvalartistico1").checked;
var ptr_valorart2 = document.getElementById("cvalartistico2").checked;
var ptr_valorart3 = document.getElementById("cvalartistico3").checked;

var ptr_valorart_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorart1+', "resvalor": "Ejemplo sobresaliente por su singularidad"},{"resid": 278, "estado": '+ptr_valorart2+', "resvalor": "Conservar elementos arquitectónicos y tradicionales de interés"},{"resid": 279, "estado": '+ptr_valorart3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';


//var PTR_VALORARQ      = $("#cvalarquitectonico").val();
var ptr_valorarq1 = document.getElementById("cvalarquitectonico1").checked;
var ptr_valorarq2 = document.getElementById("cvalarquitectonico2").checked;
var ptr_valorarq3 = document.getElementById("cvalarquitectonico3").checked;

var ptr_valorarq_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorarq1+', "resvalor": "Poseedor de caracteristicas tipológicas representativas de estilos arquitectónicos"},{"resid": 278, "estado": '+ptr_valorarq2+', "resvalor": "Con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalle"},{"resid": 279, "estado": '+ptr_valorarq3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';

//var PTR_VALORTEC      = $("#cvaltecnologico").val();
var ptr_valortec1 = document.getElementById("cvaltecnologico1").checked;
var ptr_valortec2 = document.getElementById("cvaltecnologico2").checked;
var ptr_valortec3 = document.getElementById("cvaltecnologico3").checked;

var ptr_valortec_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valortec1+', "resvalor": "Espacio donde la aplicación de técnicas contructivas son singulares o de especial interés"},{"resid": 278, "estado": '+ptr_valortec2+', "resvalor": "Espacio que se constituye en un exponente de las técnicas constructivas y uso de los materiales característicos de una época o región determinada"},{"resid": 279, "estado": '+ptr_valortec2+', "resvalor": "Espacios con sistemas constructivos y elementos arquitectónicos realizados por su mano de obra especializada"}]';



//var PTR_VALORINTG     = $("#cvalintegridad").val();
var ptr_valorintg1 = document.getElementById("cvalintegridad1").checked;
var ptr_valorintg2 = document.getElementById("cvalintegridad2").checked;

var ptr_valorintg_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorintg1+', "resvalor": "Espacio que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 278, "estado": '+ptr_valorintg2+', "resvalor": "Espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"}]';


//var PTR_VALORURB      = $("#cvalurbano").val();
var ptr_valorurb1 = document.getElementById("cvalurbano1").checked;
var ptr_valorurb2 = document.getElementById("cvalurbano2").checked;

var ptr_valorurb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorurb1+', "resvalor": "Espacio que contribuye a definir un entorno de valor por su configuración y calidad en la estructura urbanística, el paisaje y/o el espacio público"},{"resid": 278, "estado": '+ptr_valorurb2+', "resvalor": "Considerado hito de referencia por su emplazamiento"}]';

//var PTR_VALORINT      = $("#cvalintangible").val();
var ptr_valorint1 = document.getElementById("cvalintangible1").checked;

var ptr_valorint_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorint1+', "resvalor": "Espacio relacionado con la organización social o forma de vida: usos, representacines, expresiones conocimientos y técnicas que las comunidades y grupos sociales reconozcan como parte de su patrimonio"}]';

//var PTR_VALORSIMB     = $("#cvalsimbolico").val();
var ptr_valorsimb1 = document.getElementById("cvalsimbolico1").checked;

var ptr_valorsimb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorsimb1+', "resvalor": "Espacio o espacio abierto único que expresa, representa, o significa identidad y pertenencia para el colectivo"}]';

//XX. CRITERIOS DE VALORACIÓN
var PTR_CRITERHIS     = $("#ccrivalhistorico").val();
var PTR_CRITERHIS = caracteresEspecialesRegistrar(PTR_CRITERHIS);
var PTR_CRITERART     = $("#ccrivalartistico").val();
var PTR_CRITERART = caracteresEspecialesRegistrar(PTR_CRITERART);
var PTR_CRITERARQ     = $("#ccrivalarquitectonico").val();
var PTR_CRITERARQ = caracteresEspecialesRegistrar(PTR_CRITERARQ);
var PTR_CRITERTEC     = $("#ccrivaltecnologicos").val();
var PTR_CRITERTEC = caracteresEspecialesRegistrar(PTR_CRITERTEC);
var PTR_CRITERINT     = $("#ccrivalintegridad").val();
var PTR_CRITERINT = caracteresEspecialesRegistrar(PTR_CRITERINT);
var PTR_CRITERURB     = $("#ccrivalurbano").val();
var PTR_CRITERURB = caracteresEspecialesRegistrar(PTR_CRITERURB);
var PTR_CRITERINTA    = $("#ccrivalintangible").val();
var PTR_CRITERINTA = caracteresEspecialesRegistrar(PTR_CRITERINTA);
var PTR_CRITERSIMB    = $("#ccrivalsimbolico").val();
var PTR_CRITERSIMB = caracteresEspecialesRegistrar(PTR_CRITERSIMB);
//PERSONAL ENCARGADO  
var PTR_PERINV        = $("#cperencinventariador").val();
var PTR_PERREV        = $("#cperrenrevisado").val();
var PTR_PERDIG        = $("#cperencdigitalizado").val();
///-------------TEXTAREA-----------------


var g_tipo = 'PTR-ESP';
var data = '{"g_tipo":"'+g_tipo+'","PTR_CREA":"'+PTR_CREADOR+'","PTR_ACTUALIZA":"'+PTR_ACTUALIZA+'","PTR_COD_CAT":"'+PTR_COD_CAT+'","PTR_GEOREF":"'+PTR_GEOREF+'","PTR_TRAD":"'+PTR_TRAD+'","PTR_ACT":"'+PTR_ACT+'","PTR_DEP":"'+PTR_DEP+'","PTR_CIPO":"'+PTR_CIPO+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_BARR":"'+PTR_BARR+'","PTR_DIR":"'+PTR_DIR+'","PTR_ESQ1":"'+PTR_ESQ1+'","PTR_ESQ2":"'+PTR_ESQ2+'","PTR_ALT":"'+PTR_ALT+'","PTR_NOMPRO":"'+PTR_NOMPRO+'","PTR_REPRO":"'+PTR_REPRO+'","PTR_USO":'+PTR_USO+',"PTR_SERV":'+PTR_SERV+',"PTR_FIGPRO":"'+PTR_FIGPRO+'","PTR_OTHER":'+ptr_other_check+',"PTR_REFHIST":"'+PTR_REFHIST+'","PTR_FECCONS":"'+PTR_FECCONS+'","PTR_AUTOR":"'+PTR_AUTOR+'","PTR_PREEX":"'+PTR_PREEX+'","PTR_PROP":"'+PTR_PROP+'","PTR_USOORG":"'+PTR_USOORG+'","PTR_HECHIS":"'+PTR_HECHIS+'","PTR_FIEREL":"'+PTR_FIEREL+'","PTR_DATHISC":"'+PTR_DATHISC+'","PTR_FUENTE":"'+PTR_FUENTE+'","PTR_FECCONST":"'+PTR_FECCONST+'","PTR_AUTCONST":"'+PTR_AUTCONST+'","PTR_PREDIN":"'+PTR_PREDIN+'","PTR_PROPIE":"'+PTR_PROPIE+'","PTR_USOPREEX":"'+PTR_USOPREEX+'","PTR_HECHISD":"'+PTR_HECHISD+'","PTR_FIERELD":"'+PTR_FIERELD+'","PTR_DATHISTC":"'+PTR_DATHISTC+'","PTR_FUENTBIO":"'+PTR_FUENTBIO+'","PTR_INSCR":"'+PTR_INSCR+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_ACONS":"'+PTR_ACONS+'","PTR_AULTR":"'+PTR_AULTR+'","PTR_ESTILO":"'+PTR_ESTILO+'","PTR_UBICESP":"'+PTR_UBICESP+'","PTR_LINCONS":"'+PTR_LINCONS+'","PTR_TIPOLO":"'+PTR_TIPOLO+'","PTR_MURMAT":'+ptr_murmat_check+',"PTR_MURMAT_OTROS":"'+PTR_MURMAT_OTROS+'","PTR_MUREST":"'+PTR_MUREST+'","PTR_MURINTE":"'+PTR_MURINTE+'","PTR_REJAS":'+PTR_REJAS+',"PTR_REJAEST":"'+PTR_REJAEST+'","PTR_ACAMUR":'+PTR_ACAMUR+',"PTR_ACAEST":"'+PTR_ACAEST+'","PTR_PINTURA":'+PTR_PINTURA+',"PTR_PINTEST":"'+PTR_PINTEST+'","PTR_PISOS":'+PTR_PISOS+',"PTR_PISOEST":"'+PTR_PISOEST+'","PTR_PUERTA":'+PTR_PUERTA+',"PTR_PUERTAEST":"'+PTR_PUERTAEST+'","PTR_MOBURB":'+PTR_MOBURB+',"PTR_MOBEST":"'+PTR_MOBEST+'","PTR_ESCAL":'+PTR_ESCAL+',"PTR_ESCEST":"'+PTR_ESCEST+'","PTR_VEGET":'+PTR_VEGET+',"PTR_VEGEST":"'+PTR_VEGEST+'","PTR_DETALLES":'+PTR_DETALLES+',"PTR_DETALLEST":"'+PTR_DETALLEST+'","PTR_ESQDES":"'+PTR_ESQDES+'","PTR_ANGOR":"'+PTR_ANGOR+'","PTR_ANGGRA":"'+PTR_ANGGRA+'","PTR_VISTAS":'+ptr_vistas_check+',"PTR_ANGOTR":"'+PTR_ANGOTR+'","PTR_ELEMVISID":"'+PTR_ELEMVISID+'","PTR_ELEMVISAL":"'+PTR_ELEMVISAL+'","PTR_ELEMVISAB":"'+PTR_ELEMVISAB+'","PTR_DESENTEI":"'+PTR_DESENTEI+'","PTR_DESESPDES":"'+PTR_DESESPDES+'","PTR_MEDIDAREA":"'+PTR_MEDIDAREA+'","PTR_MEDIDANC":"'+PTR_MEDIDANC+'","PTR_MEDIDLAR":"'+PTR_MEDIDLAR+'","PTR_MEDIAMC":"'+PTR_MEDIAMC+'","PTR_INFADICIONAL":"'+PTR_INFADICIONAL+'","PTR_INFOBS":"'+PTR_INFOBS+'","PTR_PATODAN":'+ptr_patodan_check+',"PTR_PATO_OTROS":"'+PTR_PATO_OTROS+'","PTR_PATOOBS":"'+PTR_PATOOBS+'","PTR_PATOCAU":'+ptr_patocau_check+',"PTR_PATOCAU_OTROS":"'+PTR_PATOCAU_OTROS+'","PTR_VALORHIS":'+ptr_valorhis_check+',"PTR_VALORART":'+ptr_valorart_check+',"PTR_VALORARQ":'+ptr_valorarq_check+',"PTR_VALORTEC":'+ptr_valortec_check+',"PTR_VALORINTG":'+ptr_valorintg_check+',"PTR_VALORURB":'+ptr_valorurb_check+',"PTR_VALORINT":'+ptr_valorint_check+',"PTR_VALORSIMB":'+ptr_valorsimb_check+',"PTR_CRITERHIS":"'+PTR_CRITERHIS+'","PTR_CRITERART":"'+PTR_CRITERART+'","PTR_CRITERARQ":"'+PTR_CRITERARQ+'","PTR_CRITERTEC":"'+PTR_CRITERTEC+'","PTR_CRITERINT":"'+PTR_CRITERINT+'","PTR_CRITERURB":"'+PTR_CRITERURB+'","PTR_CRITERINTA":"'+PTR_CRITERINTA+'","PTR_CRITERSIMB":"'+PTR_CRITERSIMB+'","PTR_PERINV":"'+PTR_PERINV+'","PTR_PERREV":"'+PTR_PERREV+'","PTR_PERDIG":"'+PTR_PERDIG+'","PTR_DOT_S":'+PTR_DOT_S+'}';

  ////console.log("SOLO DATA",data);

  var dataesp = JSON.stringify(data);
  //////////console.log("dataPTR_ESP",dataesp);
  var cas_nro_caso = correlativog;
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  var cas_nombre_caso = 'PTR-ESP'+correlativog+'/2018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-941","parametros":'{"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+dataesp+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  ////console.log(222222222222,formData);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //console.log("dataaaaEspaciosAbiertos",data);
        listarPatrimonioCultural(g_tipo); 
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" ); 
        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
        setTimeout(function(){
          var url = '{{ url("adjuntosPatrimonio",[ "casosId"=>"casosId1"])}}';
          url = url.replace('casosId1', idCasos);
          //console.log("url",url);
          location.href = url;
        }, 1000);         
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500);
});

function darBaja(id, g_tipo)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-934","parametros": '{"xcas_id":'+id+', "xcas_usr_id":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
           listarPatrimonioCultural(g_tipo); 
           swal("Patrimonio!", "Fue eliminado correctamente!", "success");

         },
         error: function( result ) 
         {
          swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

function editarEspacioAbierto(cas_id,numero_ficha ,codigo_ficha ,g_tipo,creador,autorizador,codigocatastral,georeferencia,tradicional,actual,departamento,provincia,municipio,macrodistrito,barrio,direccioncalle ,esquina1 ,esquina2, altitud, nombrepropietario, tiporegpropietario,regprousosuelo,regproservicio, marlegfiguraproyeccion, regprootrosservicios, crefhisreferenciahistorica, cdatorafechaconstruccion,cdatorautorconstructor,cdatorapreexistenciaedificacion, cdatorapropietario, cdatorausosoriginales,cdatorahechoshistoricosdist,cdatorafiestasreligiosas,cdatoradatoshistoricos, cdatorafuentebibliografia, cdatdocfechadeconstruccion,cdatdocautoroconstructor,cdatdocpreexistenciainvestigacion, cdatdocpropietario, cdatdocusospreexistentes, cdatdochechoshistoricospers, cdatdocfiestasreli, cdatdocdatoshistoricosconj,cdatdocfuentebiblio, cinsinscripciones, cepoepoca, cepoanoconstruccion, cepoanoremodelacion, cestestilo, cdesareubicacionespacio, cdesarelineaconstruccion, cdesaretipologia, cdesarematerial, cdesareotros, cdesareestado, cdesareintegridad, rejasgrilla, cdesareestadorejas, acabadomurosgrilla, cdesareestadoacabadomur, pinturagrilla, cdesareestadopint, pisosgrilla, cdesareestadopisos, puertadeaccesogrilla, cdesareestadopuertasacc, mobiliariogrilla, cdesareestadomoviliariourb, escaleragrilla, cdesareestadoescaleras, caracterivegetagrilla, cdesareestadocaracteristicasvege, detalleartisticogrilla, cdesareestadodetalleartistico, cesqesquemas, cangvisorientacion, cangvisgrados , cangvisvistas, cangvisotros, celevisizquierda_derecha, celivisalto, celivisbajo, cdesententorno_inmediato, cdesespdescrpcion_espacio, cmedarea_total, cmedancho, cmedlargo, cmedancho_muro, cinformacion_adicional, cinfadicobservaciones, cpatdanos, cpatotros, cpatobservaciones, cpatcausas, cpatotros, cvalhistorico, cvalartistico, cvalarquitectonico, cvaltecnologico, cvalintegridad, cvalurbano, cvalintangible, cvalsimbolico, ccrivalhistorico, ccrivalartistico, ccrivalarquitectonico, ccrivaltecnologicos, ccrivalintegridad, ccrivalurbano, ccrivalintangible, ccrivalsimbolico, cperencinventariador, cperrenrevisado, cperencdigitalizado)

{ ////////console.log(cas_id,numero_ficha ,codigo_ficha ,g_tipo,creador,autorizador,codigocatastral,georeferencia,tradicional,actual,departamento,provincia,municipio,macrodistrito,barrio,direccioncalle ,esquina1 ,esquina2, altitud, nombrepropietario, tiporegpropietario);
  $( "#acas_id" ).val(cas_id);
  $( "#aespnumero_ficha" ).val(numero_ficha);
  $( "#aespcodigo_ficha" ).val(codigo_ficha);
  $( "#ag_tipo" ).val(g_tipo);
  $( "#acreador" ).val(creador);
  $( "#aautorizador" ).val(autorizador);
  $( "#acodigocatastral" ).val(codigocatastral);
  $( "#ageoreferencia" ).val(georeferencia);
  $( "#atradicional" ).val(tradicional);
  $( "#aactual" ).val(actual);
  $( "#adepartamento" ).val(departamento);
  $( "#aprovincia" ).val(provincia);
  $( "#amunicipio" ).val(municipio);
  document.getElementById("amacrodistritoespabie").length=0;
  document.getElementById("cmacrodistritoespabie").length=1;
  listaMacrodistritoespacioabierto(macrodistrito);
  $( "#abarrio" ).val(barrio);
  $( "#adireccioncalle" ).val(direccioncalle);
  $( "#aesquina1" ).val(esquina1);
  $( "#aesquina2" ).val(esquina2);
  $( "#aaltitud" ).val(altitud);
  $( "#anombrepropietario" ).val(nombrepropietario);
  $( "#atiporegpropietario" ).val(tiporegpropietario);
  /*-----------------*/

//var PTR_USO           = 'Uso de suelo';
//var PTR_SERV          = 'Servicio'
//$("#aregprootrosservicios").val(regprootrosservicios);
var regprootrosservicios = regprootrosservicios.split(',');
document.getElementById("aregprootrosservicios1").checked = getBool(regprootrosservicios[0]);
document.getElementById("aregprootrosservicios2").checked = getBool(regprootrosservicios[1]);
document.getElementById("aregprootrosservicios3").checked = getBool(regprootrosservicios[2]);

$("#amarlegfiguraproyeccion").val(marlegfiguraproyeccion);
$("#arefhisreferenciahistorica").val(crefhisreferenciahistorica);
$("#adatorafechaconstruccion").val(cdatorafechaconstruccion);
$("#adatorautorconstructor").val(cdatorautorconstructor);
var cdatorapreexistenciaedificacion = caracteresEspecialesActualizar(cdatorapreexistenciaedificacion);
$("#adatorapreexistenciaedificacion").val(cdatorapreexistenciaedificacion);
$("#adatorapropietario").val(cdatorapropietario);
var cdatorausosoriginales = caracteresEspecialesActualizar(cdatorausosoriginales);
$("#adatorausosoriginales").val(cdatorausosoriginales);
var cdatorahechoshistoricosdist = caracteresEspecialesActualizar(cdatorahechoshistoricosdist);
$("#adatorahechoshistoricosdist").val(cdatorahechoshistoricosdist);
var cdatorafiestasreligiosas = caracteresEspecialesActualizar(cdatorafiestasreligiosas);
$("#adatorafiestasreligiosas").val(cdatorafiestasreligiosas);
var cdatoradatoshistoricos = caracteresEspecialesActualizar(cdatoradatoshistoricos);
$("#adatoradatoshistoricos").val(cdatoradatoshistoricos);
var cdatorafuentebibliografia = caracteresEspecialesActualizar(cdatorafuentebibliografia);
$("#adatorafuentebibliografia").val(cdatorafuentebibliografia);
//V. DATOS DOCUMENTALES  
$("#adatdocfechadeconstruccion").val(cdatdocfechadeconstruccion);
$("#adatdocautoroconstructor").val(cdatdocautoroconstructor);
var cdatdocpreexistenciainvestigacion = caracteresEspecialesActualizar(cdatdocpreexistenciainvestigacion);
$("#adatdocpreexistenciainvestigacion").val(cdatdocpreexistenciainvestigacion);
$("#adatdocpropietario").val(cdatdocpropietario);
var cdatdocusospreexistentes = caracteresEspecialesActualizar(cdatdocusospreexistentes);
$("#adatdocusospreexistentes").val(cdatdocusospreexistentes);
var cdatdochechoshistoricospers = caracteresEspecialesActualizar(cdatdochechoshistoricospers);
$("#adatdochechoshistoricospers").val(cdatdochechoshistoricospers);
var cdatdocfiestasreli = caracteresEspecialesActualizar(cdatdocfiestasreli);
$("#adatdocfiestasreli").val(cdatdocfiestasreli);
var cdatdocdatoshistoricosconj = caracteresEspecialesActualizar(cdatdocdatoshistoricosconj);
$("#adatdocdatoshistoricosconj").val(cdatdocdatoshistoricosconj);
var cdatdocfuentebiblio = caracteresEspecialesActualizar(cdatdocfuentebiblio);
$("#adatdocfuentebiblio").val(cdatdocfuentebiblio);
//VI. INSCRIPCIONES  
var cinsinscripciones = caracteresEspecialesActualizar(cinsinscripciones);
$("#ainsinscripciones").val(cinsinscripciones);
//VII. ÉPOCA   
$("#aepoepoca").val(cepoepoca);
$("#aepoanoconstruccion").val(cepoanoconstruccion);
$("#aepoanoremodelacion").val(cepoanoremodelacion);
//VII. ESTILO    
$("#aestestilo").val(cestestilo);
//IX. DESCRIPCIÓN DEL AREA
$("#adesareubicacionespacio").val(cdesareubicacionespacio);
$("#adesarelineaconstruccion").val(cdesarelineaconstruccion);
$("#adesaretipologia").val(cdesaretipologia);
//$("#adesarematerial").val(cdesarematerial);
var cdesarematerial = cdesarematerial.split(',');
document.getElementById("adesarematerial1").checked = getBool(cdesarematerial[0]);
document.getElementById("adesarematerial2").checked = getBool(cdesarematerial[1]);
document.getElementById("adesarematerial3").checked = getBool(cdesarematerial[2]);
document.getElementById("adesarematerial4").checked = getBool(cdesarematerial[3]);
document.getElementById("adesarematerial5").checked = getBool(cdesarematerial[4]);
document.getElementById("adesarematerial6").checked = getBool(cdesarematerial[5]);
document.getElementById("adesarematerial7").checked = getBool(cdesarematerial[6]);
document.getElementById("adesarematerial8").checked = getBool(cdesarematerial[7]);
document.getElementById("adesarematerial9").checked = getBool(cdesarematerial[8]);
document.getElementById("adesarematerial10").checked = getBool(cdesarematerial[9]);


$("#adesareotros").val(cdesareotros);
$("#adesareestado").val(cdesareestado);
$("#adesareintegridad").val(cdesareintegridad);
//var PTR_REJAS = 'Rejas';
$("#adesareestadorejas").val(cdesareestadorejas);
//var PTR_ACAMUR = 'Acabados de Muros';
$("#adesareestadoacabadomur").val(cdesareestadoacabadomur);
//var PTR_PINTURA = 'Pintura';
$("#adesareestadopint").val(cdesareestadopint);
//var PTR_PISOS = 'Pisos';
$("#adesareestadopisos").val(cdesareestadopisos);
//var PTR_PUERTA = 'Puertas de Acceso';
$("#adesareestadopuertasacc").val(cdesareestadopuertasacc);
//var PTR_MOBURB  = "Mobiliario";
$("#adesareestadomoviliariourb").val(cdesareestadomoviliariourb);
//var PTR_ESCAL = 'Escalera';
$("#adesareestadoescaleras").val(cdesareestadoescaleras);
//var PTR_VEGET = 'Caracterísiticas de la Vegetación';
$("#adesareestadocaracteristicasvege").val(cdesareestadocaracteristicasvege);
//var PTR_DETALLES = 'Detalle artistico';
$("#adesareestadodetalleartistico").val(cdesareestadodetalleartistico);
//x. ESQUEMAS
var cesqesquemas = caracteresEspecialesActualizar(cesqesquemas);
$("#aesqesquemas").val(cesqesquemas);
//XI. ANGULO VISUAL HORIZONTAL
$("#aangvisorientacion").val(cangvisorientacion);
$("#aangvisgrados").val(cangvisgrados);
//$("#aangvisvistas").val(cangvisvistas);
var cangvisvistas = cangvisvistas.split(',');
document.getElementById("aangvisvistas1").checked = getBool(cangvisvistas[0]);
document.getElementById("aangvisvistas2").checked = getBool(cangvisvistas[1]);

$("#aangvisotros").val(cangvisotros);
//XII. ELEMENTOS VISUALES
var celevisizquierda_derecha = caracteresEspecialesActualizar(celevisizquierda_derecha);
$("#aelevisizquierda_derecha").val(celevisizquierda_derecha);
var celivisalto = caracteresEspecialesActualizar(celivisalto);
$("#aelivisalto").val(celivisalto);
var celivisbajo = caracteresEspecialesActualizar(celivisbajo);
$("#aelivisbajo").val(celivisbajo);
//XIII. DESCRIPCIÓN DEL ENTORNO
var cdesententorno_inmediato = caracteresEspecialesActualizar(cdesententorno_inmediato);
$("#adesententorno_inmediato").val(cdesententorno_inmediato);
//XIV. DESCRIPCIÓN DEL ESPACIO
//var PTR_DESESP
var cdesespdescrpcion_espacio = caracteresEspecialesActualizar(cdesespdescrpcion_espacio);
$("#adesespdescrpcion_espacio").val(cdesespdescrpcion_espacio);
//XV. MEDIDAS 
$("#amedarea_total").val(cmedarea_total);
$("#amedancho").val(cmedancho);
$("#amedlargo").val(cmedlargo);
$("#amedancho_muro").val(cmedancho_muro);
//XVI. INFORMACIÓN ADICIONAL
$("#ainformacion_adicional").val(cinformacion_adicional);
var cinfadicobservaciones = caracteresEspecialesActualizar(cinfadicobservaciones);
$("#ainfadicobservaciones").val(cinfadicobservaciones);
//XVIII. PATOLOGIAS  
//$("#apatdanos").val(cpatdanos);
var cpatdanos = cpatdanos.split(',');
////console.log(cpatdanos,'hghhghghghgh');
document.getElementById("apatdanos1").checked = getBool(cpatdanos[0]);
document.getElementById("apatdanos2").checked = getBool(cpatdanos[1]);
document.getElementById("apatdanos3").checked = getBool(cpatdanos[2]);
document.getElementById("apatdanos4").checked = getBool(cpatdanos[3]);
document.getElementById("apatdanos5").checked = getBool(cpatdanos[4]);
document.getElementById("apatdanos6").checked = getBool(cpatdanos[5]);

$("#apatotros").val(cpatotros);
var cpatobservaciones = caracteresEspecialesActualizar(cpatobservaciones);
$("#apatobservaciones").val(cpatobservaciones);

////console.log(cpatcausas,'apatcausas1 12121212121212121');
//$("#apatcausas").val(cpatcausas);
var cpatcausas = cpatcausas.split(',');
////console.log('causaseditar',cpatcausas);
document.getElementById("apatcausas1").checked = getBool(cpatcausas[0]);
document.getElementById("apatcausas2").checked = getBool(cpatcausas[1]);
document.getElementById("apatcausas3").checked = getBool(cpatcausas[2]);
document.getElementById("apatcausas4").checked = getBool(cpatcausas[3]);
document.getElementById("apatcausas5").checked = getBool(cpatcausas[4]);
document.getElementById("apatcausas6").checked = getBool(cpatcausas[5]);
document.getElementById("apatcausas7").checked = getBool(cpatcausas[6]);


$("#apatotros ").val(cpatotros);
//XIX. VALORACIÓN  
//$("#avalhistorico").val(cvalhistorico);
var cvalhistorico = cvalhistorico.split(',');
document.getElementById("avalhistorico1").checked = getBool(cvalhistorico[0]);
document.getElementById("avalhistorico2").checked = getBool(cvalhistorico[1]);
document.getElementById("avalhistorico3").checked = getBool(cvalhistorico[2]);
document.getElementById("avalhistorico4").checked = getBool(cvalhistorico[3]);
document.getElementById("avalhistorico5").checked = getBool(cvalhistorico[4]);

//$("#avalartistico").val(cvalartistico);
var cvalartistico = cvalartistico.split(',');
document.getElementById("avalartistico1").checked = getBool(cvalartistico[0]);
document.getElementById("avalartistico2").checked = getBool(cvalartistico[1]);
document.getElementById("avalartistico3").checked = getBool(cvalartistico[2]);

//$("#avalarquitectonico").val(cvalarquitectonico);
var cvalarquitectonico = cvalarquitectonico.split(',');
document.getElementById("avalarquitectonico1").checked = getBool(cvalarquitectonico[0]);
document.getElementById("avalarquitectonico2").checked = getBool(cvalarquitectonico[1]);
document.getElementById("avalarquitectonico3").checked = getBool(cvalarquitectonico[2]);

//$("#avaltecnologico").val(cvaltecnologico);\
var cvaltecnologico = cvaltecnologico.split(',');
document.getElementById("avaltecnologico1").checked = getBool(cvaltecnologico[0]);
document.getElementById("avaltecnologico2").checked = getBool(cvaltecnologico[1]);
document.getElementById("avaltecnologico3").checked = getBool(cvaltecnologico[2]);



//$("#avalintegridad").val(cvalintegridad);
var cvalintegridad = cvalintegridad.split(',');
document.getElementById("avalintegridad1").checked = getBool(cvalintegridad[0]);
document.getElementById("avalintegridad2").checked = getBool(cvalintegridad[1]);

//$("#avalurbano").val(cvalurbano);
var cvalurbano = cvalurbano.split(',');
document.getElementById("avalurbano1").checked = getBool(cvalurbano[0]);
document.getElementById("avalurbano2").checked = getBool(cvalurbano[1]);

//$("#avalintangible").val(cvalintangible);
var cvalintangible = cvalintangible.split(',');
document.getElementById("avalintangible1").checked = getBool(cvalintangible[0]);

//$("#avalsimbolico").val(cvalsimbolico);
var cvalsimbolico = cvalsimbolico.split(',');
document.getElementById("avalsimbolico1").checked = getBool(cvalsimbolico[0]);

//XX. CRITERIOS DE VALORACIÓN
var ccrivalhistorico = caracteresEspecialesActualizar(ccrivalhistorico);
$("#acrivalhistorico").val(ccrivalhistorico);
var ccrivalartistico = caracteresEspecialesActualizar(ccrivalartistico);
$("#acrivalartistico").val(ccrivalartistico);
var ccrivalarquitectonico = caracteresEspecialesActualizar(ccrivalarquitectonico);
$("#acrivalarquitectonico").val(ccrivalarquitectonico);
var ccrivaltecnologicos = caracteresEspecialesActualizar(ccrivaltecnologicos);
$("#acrivaltecnologicos").val(ccrivaltecnologicos);
var ccrivalintegridad = caracteresEspecialesActualizar(ccrivalintegridad);
$("#acrivalintegridad").val(ccrivalintegridad);
var ccrivalurbano = caracteresEspecialesActualizar(ccrivalurbano);
$("#acrivalurbano").val(ccrivalurbano);
var ccrivalintangible = caracteresEspecialesActualizar(ccrivalintangible);
$("#acrivalintangible").val(ccrivalintangible);
var ccrivalsimbolico = caracteresEspecialesActualizar(ccrivalsimbolico);
$("#acrivalsimbolico").val(ccrivalsimbolico);
//PERSONAL ENCARGADO  
$("#aperencinventariador").val(cperencinventariador);
$("#aperrenrevisado").val(cperrenrevisado);
$("#aperencdigitalizado").val(cperencdigitalizado);

}

$("#actualizarEspaciosAbiertos").click(function(){

  var cas_id = $( "#acas_id" ).val();
  var cas_nro_caso = $( "#aespnumero_ficha" ).val();
  var cas_nombre_caso = $( "#aespcodigo_ficha" ).val();
  var PTR_CREA = $( "#acreador" ).val();
  var PTR_ACTUALIZA = $( "#aautorizador" ).val();
  var PTR_COD_CAT = $( "#acodigocatastral" ).val();
  var PTR_GEOREF = $( "#ageoreferencia" ).val();
  var PTR_TRAD = $( "#atradicional" ).val();
  var PTR_ACT = $( "#aactual" ).val();
  var PTR_DEP = $( "#adepartamento" ).val();
  var PTR_CIPO = $( "#aprovincia" ).val();
  var PTR_MUNI = $( "#amunicipio" ).val();
  var macrodistritoTxt=document.getElementById("amacrodistritoespabie");
  var PTR_MACRO=macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
  var PTR_BARR = $( "#abarrio" ).val();
  var PTR_DIR = $( "#adireccioncalle" ).val();
  var PTR_ESQ1 = $( "#aesquina1" ).val();
  var PTR_ESQ2 = $( "#aesquina2" ).val();
  var PTR_ALT = $( "#aaltitud" ).val();
  var PTR_NOMPRO = $( "#anombrepropietario" ).val();
  var PTR_REPRO = $( "#atiporegpropietario" ).val();
  /*-----------------------------------------------*/

  var PTR_USO           = 'Uso de suelo';
  var PTR_SERV          = 'Servicio'
//var PTR_OTHER         = $("#aregprootrosservicios").val();
var ptr_other1 = document.getElementById("aregprootrosservicios1").checked;
var ptr_other2 = document.getElementById("aregprootrosservicios2").checked;
var ptr_other3 = document.getElementById("aregprootrosservicios3").checked;
var ptr_other_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_other1+', "resvalor": "Internet"},{"resid": 285, "estado": '+ptr_other2+', "resvalor": "Teléfono"},{"resid": 286, "estado": '+ptr_other3+', "resvalor": "TV Cable"}]';

//////////console.log(888888888888, PTR_OTHER);
//var PTR_FIGPRO        = $("#cmarlegfiguraproyeccion").val();
var figuraproteccionTxt=document.getElementById("amarlegfiguraproyeccion");
var PTR_FIGPRO=figuraproteccionTxt.options[figuraproteccionTxt.selectedIndex].text;

//var PTR_REFHIST       = $("#crefhisreferenciahistorica").val();
var referenciahistoricaTxt=document.getElementById("arefhisreferenciahistorica");
var PTR_REFHIST=referenciahistoricaTxt.options[referenciahistoricaTxt.selectedIndex].text;

var PTR_FECCONS       = $("#adatorafechaconstruccion").val();
var PTR_AUTOR         = $("#adatorautorconstructor").val();
var PTR_PREEX         = $("#adatorapreexistenciaedificacion").val();
var PTR_PREEX = caracteresEspecialesRegistrar(PTR_PREEX);

var PTR_PROP          = $("#adatorapropietario").val();
var PTR_USOORG        = $("#adatorausosoriginales").val();
var PTR_USOORG = caracteresEspecialesRegistrar(PTR_USOORG);

var PTR_HECHIS        = $("#adatorahechoshistoricosdist").val();
var PTR_HECHIS = caracteresEspecialesRegistrar(PTR_HECHIS);

var PTR_FIEREL        = $("#adatorafiestasreligiosas").val();
var PTR_FIEREL = caracteresEspecialesRegistrar(PTR_FIEREL);

var PTR_DATHISC       = $("#adatoradatoshistoricos").val();
var PTR_DATHISC = caracteresEspecialesRegistrar(PTR_DATHISC);

var PTR_FUENTE        = $("#adatorafuentebibliografia").val();
var PTR_FUENTE = caracteresEspecialesRegistrar(PTR_FUENTE);

//V. DATOS DOCUMENTALES  
var PTR_FECCONST      = $("#adatdocfechadeconstruccion").val();
var PTR_AUTCONST      = $("#adatdocautoroconstructor").val();
var PTR_PREDIN        = $("#adatdocpreexistenciainvestigacion").val();
var PTR_PREDIN = caracteresEspecialesRegistrar(PTR_PREDIN);

var PTR_PROPIE        = $("#adatdocpropietario").val();
var PTR_USOPREEX      = $("#adatdocusospreexistentes").val();
var PTR_USOPREEX = caracteresEspecialesRegistrar(PTR_USOPREEX);

var PTR_HECHISD       = $("#adatdochechoshistoricospers").val();
var PTR_HECHISD = caracteresEspecialesRegistrar(PTR_HECHISD);

var PTR_FIERELD       = $("#adatdocfiestasreli").val();
var PTR_FIERELD = caracteresEspecialesRegistrar(PTR_FIERELD);

var PTR_DATHISTC      = $("#adatdocdatoshistoricosconj").val();
var PTR_DATHISTC = caracteresEspecialesRegistrar(PTR_DATHISTC);

var PTR_FUENTBIO      = $("#adatdocfuentebiblio").val();
var PTR_FUENTBIO = caracteresEspecialesRegistrar(PTR_FUENTBIO);

//VI. INSCRIPCIONES         
var PTR_INSCR         = $("#ainsinscripciones").val();
var PTR_INSCR = caracteresEspecialesRegistrar(PTR_INSCR);

//VII. ÉPOCA   
//var PTR_EPOCA         = $("#cepoepoca").val();
var epocaTxt=document.getElementById("aepoepoca");
var PTR_EPOCA=epocaTxt.options[epocaTxt.selectedIndex].text;

var PTR_ACONS         = $("#aepoanoconstruccion").val();
var PTR_AULTR         = $("#aepoanoremodelacion").val();
//VII. ESTILO    
//var PTR_ESTILO        = $("#cestestilo").val();
var estiloTxt=document.getElementById("aestestilo");
var PTR_ESTILO=estiloTxt.options[estiloTxt.selectedIndex].text;

//IX. DESCRIPCIÓN DEL AREA
//var PTR_UBICESP       = $("#cdesareubicacionespacio").val();
var ubicacionespacioareaTxt=document.getElementById("adesareubicacionespacio");
var PTR_UBICESP=ubicacionespacioareaTxt.options[ubicacionespacioareaTxt.selectedIndex].text;

//var PTR_LINCONS       = $("#cdesarelineaconstruccion").val();
var lineacontruccionTxt=document.getElementById("adesarelineaconstruccion");
var PTR_LINCONS=lineacontruccionTxt.options[lineacontruccionTxt.selectedIndex].text;

//var PTR_TIPOLO        = $("#cdesaretipologia").val();
var tipologiaTxt=document.getElementById("adesaretipologia");
var PTR_TIPOLO=tipologiaTxt.options[tipologiaTxt.selectedIndex].text;

//var PTR_MURMAT        = $("#adesarematerial").val();
var ptr_murmat1 = document.getElementById("adesarematerial1").checked;
var ptr_murmat2 = document.getElementById("adesarematerial2").checked;
var ptr_murmat3 = document.getElementById("adesarematerial3").checked;
var ptr_murmat4 = document.getElementById("adesarematerial4").checked;
var ptr_murmat5 = document.getElementById("adesarematerial5").checked;
var ptr_murmat6 = document.getElementById("adesarematerial6").checked;
var ptr_murmat7 = document.getElementById("adesarematerial7").checked;
var ptr_murmat8 = document.getElementById("adesarematerial8").checked;
var ptr_murmat9 = document.getElementById("adesarematerial9").checked;
var ptr_murmat10 = document.getElementById("adesarematerial10").checked;

var ptr_murmat_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_murmat1+', "resvalor": "Adobe"},{"resid": 278, "estado": '+ptr_murmat2+', "resvalor": "Concreto Armado"},{"resid": 279, "estado": '+ptr_murmat3+', "resvalor": "Hormigon Armado"},{"resid": 280, "estado": '+ptr_murmat4+', "resvalor": "Ladrillo de Barro"},{"resid": 281, "estado": '+ptr_murmat5+', "resvalor": "Madera"},{"resid": 282, "estado": '+ptr_murmat6+', "resvalor": "Otros"},{"resid": 283, "estado": '+ptr_murmat7+', "resvalor": "Piedra"},{"resid": 284, "estado": '+ptr_murmat8+', "resvalor": "Piedra Cal"},{"resid": 285, "estado": '+ptr_murmat9+', "resvalor": "Prefabricados"},{"resid": 286, "estado": '+ptr_murmat10+', "resvalor": "Tapial"}]';


var PTR_MURMAT_OTROS  = $("#adesareotros").val();
//var PTR_MUREST        = $("#cdesareestado").val();
var estadootrosTxt=document.getElementById("adesareestado");
var PTR_MUREST=estadootrosTxt.options[estadootrosTxt.selectedIndex].text;

//var PTR_MURINTE       = $("#cdesareintegridad").val();
var cdesareintegridadTxt=document.getElementById("adesareintegridad");
var PTR_MURINTE=cdesareintegridadTxt.options[cdesareintegridadTxt.selectedIndex].text;

var PTR_REJAS = 'Rejas';
//var PTR_REJAEST       = $("#cdesareestadorejas").val();
var cdesareestadorejasTxt=document.getElementById("adesareestadorejas");
var PTR_REJAEST=cdesareestadorejasTxt.options[cdesareestadorejasTxt.selectedIndex].text;

var PTR_ACAMUR = 'Acabados de Muros';
//var PTR_ACAEST        = $("#cdesareestadoacabadomur").val();
var cdesareestadoacabadomurTxt=document.getElementById("adesareestadoacabadomur");
var PTR_ACAEST=cdesareestadoacabadomurTxt.options[cdesareestadoacabadomurTxt.selectedIndex].text;

var PTR_PINTURA = 'Pintura';
//var PTR_PINTEST       = $("#cdesareestadopint").val();
var cdesareestadopintTxt=document.getElementById("adesareestadopint");
var PTR_PINTEST=cdesareestadopintTxt.options[cdesareestadopintTxt.selectedIndex].text;

var PTR_PISOS = 'Pisos';
//var PTR_PISOEST       = $("#cdesareestadopisos").val();
var cdesareestadopisosTxt=document.getElementById("adesareestadopisos");
var PTR_PISOEST=cdesareestadopisosTxt.options[cdesareestadopisosTxt.selectedIndex].text;

var PTR_PUERTA = 'Puertas de Acceso';
//var PTR_PUERTAEST     = $("#cdesareestadopuertasacc").val();
var cdesareestadopuertasaccTxt=document.getElementById("adesareestadopuertasacc");
var PTR_PUERTAEST=cdesareestadopuertasaccTxt.options[cdesareestadopuertasaccTxt.selectedIndex].text;

var PTR_MOBURB  = "Mobiliario";
//var PTR_MOBEST        = $("#cdesareestadomoviliariourb").val();
var cdesareestadomoviliariourbTxt=document.getElementById("adesareestadomoviliariourb");
var PTR_MOBEST=cdesareestadomoviliariourbTxt.options[cdesareestadomoviliariourbTxt.selectedIndex].text;

var PTR_ESCAL = 'Escalera';
//var PTR_ESCEST        = $("#cdesareestadoescaleras").val();
var cdesareestadoescalerasTxt=document.getElementById("adesareestadoescaleras");
var PTR_ESCEST=cdesareestadoescalerasTxt.options[cdesareestadoescalerasTxt.selectedIndex].text;

var PTR_VEGET = 'Caracterísiticas de la Vegetación';
//var PTR_VEGEST        = $("#cdesareestadocaracteristicasvege").val();
var cdesareestadocaracteristicasvegeTxt=document.getElementById("adesareestadocaracteristicasvege");
var PTR_VEGEST=cdesareestadocaracteristicasvegeTxt.options[cdesareestadocaracteristicasvegeTxt.selectedIndex].text;


var PTR_DETALLES = 'Detalle artistico';
//var PTR_DETALLEST     = $("#cdesareestadodetalleartistico").val();
var cdesareestadodetalleartisticoTxt=document.getElementById("adesareestadodetalleartistico");
var PTR_DETALLEST=cdesareestadodetalleartisticoTxt.options[cdesareestadodetalleartisticoTxt.selectedIndex].text;

//x. ESQUEMAS
var PTR_ESQDES        =$("#aesqesquemas").val();
var PTR_ESQDES = caracteresEspecialesRegistrar(PTR_ESQDES);

//XI. ANGULO VISUAL HORIZONTAL
var PTR_ANGOR         = $("#aangvisorientacion").val();
var PTR_ANGGRA        = $("#aangvisgrados").val();
//var PTR_VISTAS        = $("#aangvisvistas").val();
var ptr_vistas1 = document.getElementById("aangvisvistas1").checked;
var ptr_vistas2 = document.getElementById("aangvisvistas2").checked;
var ptr_vistas_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_vistas1+', "resvalor": "Vista Parcial  "},{"resid": 271, "estado": '+ptr_vistas2+', "resvalor": "Vista Panorámica"}]';

var PTR_ANGOTR        = $("#aangvisotros").val();
//XII. ELEMENTOS VISUALES
var PTR_ELEMVISID     = $("#aelevisizquierda_derecha").val();
var PTR_ELEMVISID = caracteresEspecialesRegistrar(PTR_ELEMVISID);

var PTR_ELEMVISAL     = $("#aelivisalto").val();
var PTR_ELEMVISAL = caracteresEspecialesRegistrar(PTR_ELEMVISAL);

var PTR_ELEMVISAB     = $("#aelivisbajo").val();
var PTR_ELEMVISAB = caracteresEspecialesRegistrar(PTR_ELEMVISAB);

//XIII. DESCRIPCIÓN DEL ENTORNO
var PTR_DESENTEI      = $("#adesententorno_inmediato").val();
var PTR_DESENTEI = caracteresEspecialesRegistrar(PTR_DESENTEI);

//XIV. DESCRIPCIÓN DEL ESPACIO
var PTR_DESESPDES     = $("#adesespdescrpcion_espacio").val();
var PTR_DESESPDES = caracteresEspecialesRegistrar(PTR_DESESPDES);

//XV. MEDIDAS 
var PTR_MEDIDAREA     = $("#amedarea_total").val();
var PTR_MEDIDANC      = $("#amedancho").val();
var PTR_MEDIDLAR      = $("#amedlargo").val();
var PTR_MEDIAMC       = $("#amedancho_muro").val();
//XVI. INFORMACIÓN ADICIONAL
//var PTR_INFADICIONAL  = $("#cinformacion_adicional").val();
var cinformacion_adicionalTxt=document.getElementById("ainformacion_adicional");
var PTR_INFADICIONAL=cinformacion_adicionalTxt.options[cinformacion_adicionalTxt.selectedIndex].text;

var PTR_INFOBS        = $("#ainfadicobservaciones").val();
var PTR_INFOBS = caracteresEspecialesRegistrar(PTR_INFOBS);

//XVIII. PATOLOGIAS  
//var PTR_PATODAN       = $("#apatdanos").val();
var ptr_patodan1 = document.getElementById("apatdanos1").checked;
var ptr_patodan2 = document.getElementById("apatdanos2").checked;
var ptr_patodan3 = document.getElementById("apatdanos3").checked;
var ptr_patodan4 = document.getElementById("apatdanos4").checked;
var ptr_patodan5 = document.getElementById("apatdanos5").checked;
var ptr_patodan6 = document.getElementById("apatdanos6").checked;

var ptr_patodan_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patodan1+', "resvalor": "Asentamientos"},{"resid": 278, "estado": '+ptr_patodan2+', "resvalor": "Desplomes"},{"resid": 279, "estado": '+ptr_patodan3+', "resvalor": "Desprendimientos"},{"resid": 279, "estado": '+ptr_patodan4+', "resvalor": "Humedad"},{"resid": 280, "estado": '+ptr_patodan5+', "resvalor": "Otros"},{"resid": 281, "estado": '+ptr_patodan6+', "resvalor": "Podredumbre"}]';

var PTR_PATO_OTROS    = $("#apatotros").val();
var PTR_PATOOBS       = $("#apatobservaciones").val();
var PTR_PATOOBS = caracteresEspecialesRegistrar(PTR_PATOOBS);

//var PTR_PATOCAU       = $("#apatcausas").val();
var ptr_patocau1 = document.getElementById("apatcausas1").checked;
var ptr_patocau2 = document.getElementById("apatcausas2").checked;
var ptr_patocau3 = document.getElementById("apatcausas3").checked;
var ptr_patocau4 = document.getElementById("apatcausas4").checked;
var ptr_patocau5 = document.getElementById("apatcausas5").checked;
var ptr_patocau6 = document.getElementById("apatcausas6").checked;
var ptr_patocau7 = document.getElementById("apatcausas7").checked;



var ptr_patocau_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patocau1+', "resvalor": "Abandono"},{"resid": 278, "estado": '+ptr_patocau2+', "resvalor": "Botánicos"},{"resid": 279, "estado": '+ptr_patocau3+', "resvalor": "Calidad de los materiales"},{"resid": 280, "estado": '+ptr_patocau4+', "resvalor": "Catastrofe Natural"},{"resid": 281, "estado": '+ptr_patocau5+', "resvalor": "Deficiencias constructivas"},{"resid": 282, "estado": '+ptr_patocau6+', "resvalor": "Falta de mantenimiento"},{"resid": 283, "estado": '+ptr_patocau7+', "resvalor": "Otros"}]';

var PTR_PATOCAU_OTROS = $("#apatotros ").val();
//XIX. VALORACIÓN  
//var PTR_VALORHIS      = $("#avalhistorico").val();
//var PTR_VALORHIS      = $("#cvalhistorico").val();
var ptr_valorhis1 = document.getElementById("avalhistorico1").checked;
var ptr_valorhis2 = document.getElementById("avalhistorico2").checked;
var ptr_valorhis3 = document.getElementById("avalhistorico3").checked;
var ptr_valorhis4 = document.getElementById("avalhistorico4").checked;
var ptr_valorhis5 = document.getElementById("avalhistorico5").checked;

var ptr_valorhis_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorhis1+', "resvalor": "Espacio de valor testimonial y documental que ilustra el desarrollo político, social, religioso, cultural, económico y de forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 278, "estado": '+ptr_valorhis2+', "resvalor": "Espacio que desde su creación se constituye en un hito en la memoria histórica"},{"resid": 279, "estado": '+ptr_valorhis3+', "resvalor": "Espacio que en el trascurso histórico se constituyo en hito, al ser escenario relacionado con personas o eventos importantes, dentro del proceso histórico, social, cultural, económico de las comunidades"},{"resid": 280, "estado": '+ptr_valorhis4+', "resvalor": "Espacios que forman parte de un conjunto histórico"},{"resid": 281, "estado": '+ptr_valorhis5+', "resvalor": "Espacio que posee elementos arqueológicos que documentan su evolución y desarrollo"}]';

//var PTR_VALORART      = $("#avalartistico").val();
var ptr_valorart1 = document.getElementById("avalartistico1").checked;
var ptr_valorart2 = document.getElementById("avalartistico2").checked;
var ptr_valorart3 = document.getElementById("avalartistico3").checked;

var ptr_valorart_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorart1+', "resvalor": "Ejemplo sobresaliente por su singularidad"},{"resid": 278, "estado": '+ptr_valorart2+', "resvalor": "Conservar elementos arquitectónicos y tradicionales de interés"},{"resid": 279, "estado": '+ptr_valorart3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';

//var PTR_VALORARQ      = $("#avalarquitectonico").val();
var ptr_valorarq1 = document.getElementById("avalarquitectonico1").checked;
var ptr_valorarq2 = document.getElementById("avalarquitectonico2").checked;
var ptr_valorarq3 = document.getElementById("avalarquitectonico3").checked;

var ptr_valorarq_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorarq1+', "resvalor": "Poseedor de caracteristicas tipológicas representativas de estilos arquitectónicos"},{"resid": 278, "estado": '+ptr_valorarq2+', "resvalor": "Con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalle"},{"resid": 279, "estado": '+ptr_valorarq3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';


//var PTR_VALORTEC      = $("#avaltecnologico").val();
var ptr_valortec1 = document.getElementById("avaltecnologico1").checked;
var ptr_valortec2 = document.getElementById("avaltecnologico2").checked;
var ptr_valortec3 = document.getElementById("avaltecnologico3").checked;

var ptr_valortec_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valortec1+', "resvalor": "Espacio donde la aplicación de técnicas contructivas son singulares o de especial interés"},{"resid": 278, "estado": '+ptr_valortec2+', "resvalor": "Espacio que se constituye en un exponente de las técnicas constructivas y uso de los materiales característicos de una época o región determinada"},{"resid": 279, "estado": '+ptr_valortec2+', "resvalor": "Espacios con sistemas constructivos y elementos arquitectónicos realizados por su mano de obra especializada"}]';

//var PTR_VALORINTG     = $("#avalintegridad").val();
var ptr_valorintg1 = document.getElementById("avalintegridad1").checked;
var ptr_valorintg2 = document.getElementById("avalintegridad2").checked;

var ptr_valorintg_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorintg1+', "resvalor": "Espacio que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 278, "estado": '+ptr_valorintg2+', "resvalor": "Espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"}]';

//var PTR_VALORURB      = $("#avalurbano").val();
var ptr_valorurb1 = document.getElementById("avalurbano1").checked;
var ptr_valorurb2 = document.getElementById("avalurbano2").checked;

var ptr_valorurb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorurb1+', "resvalor": "Espacio que contribuye a definir un entorno de valor por su configuración y calidad en la estructura urbanística, el paisaje y/o el espacio público"},{"resid": 278, "estado": '+ptr_valorurb2+', "resvalor": "Considerado hito de referencia por su emplazamiento"}]';


//var PTR_VALORINT      = $("#avalintangible").val();
var ptr_valorint1 = document.getElementById("avalintangible1").checked;

var ptr_valorint_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorint1+', "resvalor": "Espacio relacionado con la organización social o forma de vida: usos, representacines, expresiones conocimientos y técnicas que las comunidades y grupos sociales reconozcan como parte de su patrimonio"}]';

//var PTR_VALORSIMB     = $("#avalsimbolico").val();
var ptr_valorsimb1 = document.getElementById("avalsimbolico1").checked;

var ptr_valorsimb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorsimb1+', "resvalor": "Espacio o espacio abierto único que expresa, representa, o significa identidad y pertenencia para el colectivo"}]';

//XX. CRITERIOS DE VALORACIÓN
var PTR_CRITERHIS     = $("#acrivalhistorico").val();
var PTR_CRITERHIS = caracteresEspecialesRegistrar(PTR_CRITERHIS);
var PTR_CRITERART     = $("#acrivalartistico").val();
var PTR_CRITERART = caracteresEspecialesRegistrar(PTR_CRITERART);
var PTR_CRITERARQ     = $("#acrivalarquitectonico").val();
var PTR_CRITERARQ = caracteresEspecialesRegistrar(PTR_CRITERARQ);
var PTR_CRITERTEC     = $("#acrivaltecnologicos").val();
var PTR_CRITERTEC = caracteresEspecialesRegistrar(PTR_CRITERTEC);
var PTR_CRITERINT     = $("#acrivalintegridad").val();
var PTR_CRITERINT = caracteresEspecialesRegistrar(PTR_CRITERINT);
var PTR_CRITERURB     = $("#acrivalurbano").val();
var PTR_CRITERURB = caracteresEspecialesRegistrar(PTR_CRITERURB);
var PTR_CRITERINTA    = $("#acrivalintangible").val();
var PTR_CRITERINTA = caracteresEspecialesRegistrar(PTR_CRITERINTA);
var PTR_CRITERSIMB    = $("#acrivalsimbolico").val();
var PTR_CRITERSIMB = caracteresEspecialesRegistrar(PTR_CRITERSIMB);
//PERSONAL ENCARGADO  
var PTR_PERINV        = $("#aperencinventariador").val();
var PTR_PERREV        = $("#aperrenrevisado").val();
var PTR_PERDIG        = $("#aperencdigitalizado").val();
///-------------TEXTAREA-----------------

/*-----------------------------------------------*/
var g_tipo = 'PTR-ESP';
var data = '{"g_tipo":"'+g_tipo+'","PTR_CREA":"'+PTR_CREA+'","PTR_ACTUALIZA":"'+PTR_ACTUALIZA+'","PTR_COD_CAT":"'+PTR_COD_CAT+'","PTR_GEOREF":"'+PTR_GEOREF+'","PTR_TRAD":"'+PTR_TRAD+'","PTR_ACT":"'+PTR_ACT+'","PTR_DEP":"'+PTR_DEP+'","PTR_CIPO":"'+PTR_CIPO+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_BARR":"'+PTR_BARR+'","PTR_DIR":"'+PTR_DIR+'","PTR_ESQ1":"'+PTR_ESQ1+'","PTR_ESQ2":"'+PTR_ESQ2+'","PTR_ALT":"'+PTR_ALT+'","PTR_NOMPRO":"'+PTR_NOMPRO+'","PTR_REPRO":"'+PTR_REPRO+'","PTR_USO":"'+PTR_USO+'","PTR_SERV":"'+PTR_SERV+'","PTR_FIGPRO":"'+PTR_FIGPRO+'","PTR_OTHER":'+ptr_other_check+',"PTR_REFHIST":"'+PTR_REFHIST+'","PTR_FECCONS":"'+PTR_FECCONS+'","PTR_AUTOR":"'+PTR_AUTOR+'","PTR_PREEX":"'+PTR_PREEX+'","PTR_PROP":"'+PTR_PROP+'","PTR_USOORG":"'+PTR_USOORG+'","PTR_HECHIS":"'+PTR_HECHIS+'","PTR_FIEREL":"'+PTR_FIEREL+'","PTR_DATHISC":"'+PTR_DATHISC+'","PTR_FUENTE":"'+PTR_FUENTE+'","PTR_FECCONST":"'+PTR_FECCONST+'","PTR_AUTCONST":"'+PTR_AUTCONST+'","PTR_PREDIN":"'+PTR_PREDIN+'","PTR_PROPIE":"'+PTR_PROPIE+'","PTR_USOPREEX":"'+PTR_USOPREEX+'","PTR_HECHISD":"'+PTR_HECHISD+'","PTR_FIERELD":"'+PTR_FIERELD+'","PTR_DATHISTC":"'+PTR_DATHISTC+'","PTR_FUENTBIO":"'+PTR_FUENTBIO+'","PTR_INSCR":"'+PTR_INSCR+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_ACONS":"'+PTR_ACONS+'","PTR_AULTR":"'+PTR_AULTR+'","PTR_ESTILO":"'+PTR_ESTILO+'","PTR_UBICESP":"'+PTR_UBICESP+'","PTR_LINCONS":"'+PTR_LINCONS+'","PTR_TIPOLO":"'+PTR_TIPOLO+'","PTR_MURMAT":'+ptr_murmat_check+',"PTR_MURMAT_OTROS":"'+PTR_MURMAT_OTROS+'","PTR_MUREST":"'+PTR_MUREST+'","PTR_MURINTE":"'+PTR_MURINTE+'","PTR_REJAS":"'+PTR_REJAS+'","PTR_REJAEST":"'+PTR_REJAEST+'","PTR_ACAMUR":"'+PTR_ACAMUR+'","PTR_ACAEST":"'+PTR_ACAEST+'","PTR_PINTURA":"'+PTR_PINTURA+'","PTR_PINTEST":"'+PTR_PINTEST+'","PTR_PISOS":"'+PTR_PISOS+'","PTR_PISOEST":"'+PTR_PISOEST+'","PTR_PUERTA":"'+PTR_PUERTA+'","PTR_PUERTAEST":"'+PTR_PUERTAEST+'","PTR_MOBURB":"'+PTR_MOBURB+'","PTR_MOBEST":"'+PTR_MOBEST+'","PTR_ESCAL":"'+PTR_ESCAL+'","PTR_ESCEST":"'+PTR_ESCEST+'","PTR_VEGET":"'+PTR_VEGET+'","PTR_VEGEST":"'+PTR_VEGEST+'","PTR_DETALLES":"'+PTR_DETALLES+'","PTR_DETALLEST":"'+PTR_DETALLEST+'","PTR_ESQDES":"'+PTR_ESQDES+'","PTR_ANGOR":"'+PTR_ANGOR+'","PTR_ANGGRA":"'+PTR_ANGGRA+'","PTR_VISTAS":'+ptr_vistas_check+',"PTR_ANGOTR":"'+PTR_ANGOTR+'","PTR_ELEMVISID":"'+PTR_ELEMVISID+'","PTR_ELEMVISAL":"'+PTR_ELEMVISAL+'","PTR_ELEMVISAB":"'+PTR_ELEMVISAB+'","PTR_DESENTEI":"'+PTR_DESENTEI+'","PTR_DESESPDES":"'+PTR_DESESPDES+'","PTR_MEDIDAREA":"'+PTR_MEDIDAREA+'","PTR_MEDIDANC":"'+PTR_MEDIDANC+'","PTR_MEDIDLAR":"'+PTR_MEDIDLAR+'","PTR_MEDIAMC":"'+PTR_MEDIAMC+'","PTR_INFADICIONAL":"'+PTR_INFADICIONAL+'","PTR_INFOBS":"'+PTR_INFOBS+'","PTR_PATODAN":'+ptr_patodan_check+',"PTR_PATO_OTROS":"'+PTR_PATO_OTROS+'","PTR_PATOOBS":"'+PTR_PATOOBS+'","PTR_PATOCAU":'+ptr_patocau_check+',"PTR_PATOCAU_OTROS":"'+PTR_PATOCAU_OTROS+'","PTR_VALORHIS":'+ptr_valorhis_check+',"PTR_VALORART":'+ptr_valorart_check+',"PTR_VALORARQ":'+ptr_valorarq_check+',"PTR_VALORTEC":'+ptr_valortec_check+',"PTR_VALORINTG":'+ptr_valorintg_check+',"PTR_VALORURB":'+ptr_valorurb_check+',"PTR_VALORINT":'+ptr_valorint_check+',"PTR_VALORSIMB":'+ptr_valorsimb_check+',"PTR_CRITERHIS":"'+PTR_CRITERHIS+'","PTR_CRITERART":"'+PTR_CRITERART+'","PTR_CRITERARQ":"'+PTR_CRITERARQ+'","PTR_CRITERTEC":"'+PTR_CRITERTEC+'","PTR_CRITERINT":"'+PTR_CRITERINT+'","PTR_CRITERURB":"'+PTR_CRITERURB+'","PTR_CRITERINTA":"'+PTR_CRITERINTA+'","PTR_CRITERSIMB":"'+PTR_CRITERSIMB+'","PTR_PERINV":"'+PTR_PERINV+'","PTR_PERREV":"'+PTR_PERREV+'","PTR_PERDIG":"'+PTR_PERDIG+'"}';
var dataesp = JSON.stringify(data);
  //////////console.log("dataPTR_ESP_ACTUALIZAR",dataesp);
  //var cas_nro_caso = 11 ;
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  //var cas_nombre_caso = 'PTR-ESP772018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo';

  var formData = {"identificador":"SERVICIO_PATRIMONIO-943","parametros":'{"xcas_id":'+cas_id+',"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+dataesp+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        swal( "Exíto..!", "Se actualizo correctamente...!", "success" );
        listarPatrimonioCultural(g_tipo);           
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 
});

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SISTEMA_VALLE-356","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
       ////////console.log(dataIN);
       var primerNombre = dataIN[0].vprs_nombres;
       var primerApellido = dataIN[0].vprs_paterno;
       var segundoApellido = dataIN[0].vprs_materno;
       var cedulaIdentidad = dataIN[0].vprs_ci;
       var razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
       
       $("#ccreador_p").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreadorM_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreadorS_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#cactualizador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreador").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#cautorizador").val(primerNombre+' '+primerApellido+' '+segundoApellido);  

     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};

function listaMacrodistritoespacioabierto(macrodistrito_descripcion)
{ 
  $.ajax(
  {
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) 
    {         
      var formData = {"identificador": 'SISTEMA_VALLE-616',"parametros": '{}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) 
       {  
        var datos = dataIN;
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          document.getElementById("cmacrodistritoespabie").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+ '</option>';
        }
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          if (macrodistrito == macrodistrito_descripcion) 
          {       
            document.getElementById("amacrodistritoespabie").innerHTML += '<option value='+macrodistrito+' selected>'+macrodistrito+'</option>';
          }
          else
          {
            document.getElementById("amacrodistritoespabie").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+'</option>';
          } 
        }      
      },     
      error: function (xhr, status, error) { }
    });
    },
    error: function(result) {
      swal( "Error..!", "No se puede recuperar los datos", "error" );
    },
  });
};

/*-----------------END ESPACIOS ABIERTOS-----------------------*/

$(document).ready(function (){


  var ejemplocomilla = "<mnmmjj hjkhkh 'hkj' hjgjgjh (fsfs)>";
  //console.log("res",ejemplocomilla);
  var res = caracteresEspecialesRegistrar(ejemplocomilla);

  //console.log("resCAracterescomillas",res);
 document.getElementById("panelVerAcapites").style="display:none;";
 document.getElementById("panelVerAcapites").style = "display:none;";
 document.getElementById("panelcver_elemestructurales").style = "display:none;";
 document.getElementById("panelcver_elemcompositivos").style = "display:none;";
 document.getElementById("panelcver_acabados").style = "display:none;";
 document.getElementById("panelcver_elemartisticoscompo").style = "display:none;";
 document.getElementById("panelcver_elemornamentales").style = "display:none;";
 document.getElementById("panelcver_elemtipocomp").style = "display:none;";
 document.getElementById("panelcver_hogares").style = "display:none;";
 document.getElementById("panelcver_servicios").style = "display:none;";
 listarTipoPatrimonioCombo();
 cargarDatosUsuario();
 document.getElementById("cmacrodistritoespabie").length=1;
 document.getElementById("amacrodistritoespabie").length=0;
 listaMacrodistritoespacioabierto(macrodistrito_descripcion);
 listaMacrodistritoescultura(macrodistrito_descripcion);
 var numeroficha='PTR-IME'+contesculturas+'/2018';
 $( "#cnumeroficha" ).val(numeroficha);

});

function verPanelServicios(){
    var cservicios = document.getElementById("cver_servicios").checked;
    //console.log("cservicios",cservicios);
    if(cservicios){
      document.getElementById("panelcver_servicios").style = "display:block;";
    }
    else{
       document.getElementById("panelcver_servicios").style = "display:none;";
    }
}

////Puntuacion Inmuebles////
function capturarCheckPuntuacion2(){
  var puntuacionTotal = 0;
  $("input[name=puntuacion]").each(function (index) {  
    if($(this).is(':checked')){
      puntuacionTotal = puntuacionTotal + parseFloat($(this).val());

    }
  });
  $("#cpuntuacion_in").val(puntuacionTotal);
};

///////end Puntuacion Inmuebles ///// 

/////BEGIN GRILLA////

var contCodigo=2;
var arrCodigo=[];
var arrDibujo=[];

htmlARG='Figura de Protección Legal:</label>'+
'<div class="col-md-3">'+
'<select class="form-control" id="tipoarg1" name="tipoarg1">'+
'<option value="N" selected="selected">--Seleccione--</option>'+
'<option value="Nivel Internacional">Nivel Internacional</option>'+
'<option value="Nivel Municipal">Nivel Municipal</option>'+
'<option value="Nivel Nacional">Nivel Nacional</option>'+
'<option value="Nivel Regional">Nivel Regional</option>'+
'<option value="Con Ordenanza General o de Conjunto">Con Ordenanza General o de Conjunto</option>'+
'<option value="Sin declaratoria especifica">Sin declaratoria especifica</option>'+
'</select>'+
'</div>'+
'<label class="col-md-2" for="">Observaciones:</label>'+
'<div class="col-md-3">'+
'<input class="form-control" type="text" id="valor1" name="valor1"></input>'+
'</div><div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
'</a></div></div>';
var cadena = '';  
arrCodigo.push({arg_valor:'', arg_tipo:'N'});
arrDibujo.push({scd_codigo:htmlARG,id: 1});
//console.log("arrCodigo",arrCodigo);

cadena='<div class="form-group"><label class="col-md-3">1. '+arrDibujo[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#argumentopintarCrear').html(cadena);
    $("#valor1").val(arrCodigo[0].arg_valor);
    $("#tipoarg1").val(arrCodigo[0].arg_tipo);
    

    function crearArgumento(){
      var htmlARG;
      htmlARG='Figura de Protección Legal:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="tipoarg'+contCodigo+'" name="tipoarg'+contCodigo+'">'+
      '<option value="N" selected="selected">--Seleccione--</option>'+
      '<option value="Nivel Internacional">Nivel Internacional</option>'+
      '<option value="Nivel Municipal">Nivel Municipal</option>'+
      '<option value="Nivel Nacional">Nivel Nacional</option>'+
      '<option value="Nivel Regional">Nivel Regional</option>'+
      '<option value="Con Ordenanza General o de Conjunto">Con Ordenanza General o de Conjunto</option>'+
      '<option value="Sin declaratoria especifica">Sin declaratoria especifica</option>'+
      '</select>'+
      '</div>'+
      '<label class="col-md-2" for="">Observaciones:</label>'+
      '<div class="col-md-3">'+
      '<input class="form-control" id="valor'+contCodigo+'" name="valor'+contCodigo+'"></input>'+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea('+contCodigo+');"></i>'+
      '</a></div></div>';
      var cadena='';
      var cont=0;
      arrDibujo.forEach(function (item, index, array) 
      {
        arrCodigo[cont].arg_valor = $("#valor"+item.id+"").val();
        arrCodigo[cont].arg_tipo = $("#tipoarg"+item.id+"").val();
        cont++;
      });
      arrCodigo.push({arg_valor:'', arg_tipo:'N'});
      arrDibujo.push({scd_codigo:htmlARG,id: contCodigo});
      contCodigo++;
      cont=1;
      for (var i=0; i<arrDibujo.length;i++)
      {
        cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+'. '+arrDibujo[i].scd_codigo;
        cont++;
      }
        ////console.log("cadenaaaa",cadena);
        $('#argumentopintarCrear').html(cadena); 
        var conto=0;
        arrDibujo.forEach(function (item, index, array) 
        { 
          $("#valor"+item.id+"").val(arrCodigo[conto].arg_valor);
          $("#tipoarg"+item.id+"").val(arrCodigo[conto].arg_tipo);
          conto=conto+1;
        });
        
      }


      function menosLinea($id){
        if(contCodigo>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujo.forEach(function (item, index, array) 
            {
              arrCodigo[cont].arg_valor=$("#valor"+item.id+"").val();
              arrCodigo[cont].arg_tipo=$("#tipoarg"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujo.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrCodigo.splice(pos,1);
            arrDibujo.splice(pos,1);
            //console.log("arrCodigo",arrCodigo);

            cont=1;
            for (var i=0; i<arrDibujo.length;i++)
            {
              cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+arrDibujo[i].scd_codigo;
              cont++;
            }

            $('#argumentopintarCrear').html(cadena);

            cont=0;
            arrDibujo.forEach(function (item, index, array) 
            {
              $("#valor"+item.id+"").val(arrCodigo[cont].arg_valor);
                    $("#tipoarg"+item.id+"").val(arrCodigo[cont].arg_tipo);//document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
                    cont=cont+1;
                  });
          });
        }
        //console.log("arrCodigoFinal",arrCodigo);
      }
    ///// FIN GRILLA /////


    /////////GRILLA FLAVIA/////////////////  GRILLAS OPCION VER ACABADOS  Y LOS PRIMEROS DOS BIENES INMUEBLES 

//--------------------------- Inicio Grilla 1 --------------------------------------------------------------------
var contCodigoRep=2;
var reg_proCodigo=[];
var reg_proDibujo=[];
htmlREP='Nombre Propietario:</label>'+
'<div class="col-md-3">'+
' <input id="cnombrePropietario_in1" type="text" class="form-control"  placeholder="Descripcion nombre"> '+
'</div>'+
'<label class="col-md-2" for="">Tipo de propiedad:</label>'+
'<div class="col-md-3">'+
'<select class="form-control" id="tipoPropiedad_in1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Privado Institucional"> Privado Institucional</option>'+ 
'<option value="Privado Particular"> Privado Particular</option>'+
'<option value="Privado Religioso"> Privado Religioso</option>'+
'<option value="Privado Estatal"> Privado Estatal</option>'+  
'<option value="Privado Municipal"> Privado Municipal</option>'+  
'</select> '+
'</div><div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMe(1);"></i>'+
'</a></div></div>';
var cadena = '';  
reg_proCodigo.push({arg_propietario:'', arg_tipoP:'N'});
reg_proDibujo.push({scd_codigo:htmlREP,id: 1});

cadena='<div class="form-group"><label class="col-md-3">1. '+reg_proDibujo[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#regimenPropietario').html(cadena);
    $("#cnombrePropietario_in1").val(reg_proCodigo[0].arg_propietario);
    $("#tipoPropiedad_in1").val(reg_proCodigo[0].arg_tipoP);
    

    function crearRegimenPropiedad(){
      var htmlREP;
      htmlREP='Nombre Propietario:</label>'+
      '<div class="col-md-3">'+
      ' <input id="cnombrePropietario_in'+contCodigoRep+'" type="text" class="form-control"  placeholder="Descripcion nombre"> '+
      '</div>'+
      '<label class="col-md-2" for="">Tipo de propiedad:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="tipoPropiedad_in'+contCodigoRep+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Privado Institucional"> Privado Institucional</option>'+ 
      '<option value="Privado Particular"> Privado Particular</option>'+
      '<option value="Privado Religioso"> Privado Religioso</option>'+
      '<option value="Privado Estatal"> Privado Estatal</option>'+  
      '<option value="Privado Municipal"> Privado Municipal</option>'+  
      '</select> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMe('+contCodigoRep+');"></i>'+
      '</a></div></div>';
      var cadena='';
      var cont=0;
      reg_proDibujo.forEach(function (item, index, array) 
      {
        reg_proCodigo[cont].arg_propietario = $("#cnombrePropietario_in"+item.id+"").val();
        reg_proCodigo[cont].arg_tipoP = $("#tipoPropiedad_in"+item.id+"").val();
        cont++;
      });
      reg_proCodigo.push({arg_propietario:'', arg_tipoP:'N'});
      reg_proDibujo.push({scd_codigo:htmlREP,id: contCodigoRep});
      contCodigoRep++;
      cont=1;
      for (var i=0; i<reg_proDibujo.length;i++)
      {
        cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+'. '+reg_proDibujo[i].scd_codigo;
        cont++;
      }
      $('#regimenPropietario').html(cadena); 
      var conto=0;
      reg_proDibujo.forEach(function (item, index, array) 
      { 
        $("#cnombrePropietario_in"+item.id+"").val(reg_proCodigo[conto].arg_propietario);
        $("#tipoPropiedad_in"+item.id+"").val(reg_proCodigo[conto].arg_tipoP);
        conto=conto+1;
      });
      
    }

    function menosLineaMe($id){
      if(contCodigoRep>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el argumento seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          reg_proDibujo.forEach(function (item, index, array) 
          {
            reg_proCodigo[cont].arg_propietario=$("#cnombrePropietario_in"+item.id+"").val();
            reg_proCodigo[cont].arg_tipoP=$("#tipoPropiedad_in"+item.id+"").val();
            cont=cont+1;
          });
          pos = reg_proDibujo.map(function(e) {return e.id;}).indexOf($id);
          //console.log("poos",pos);
          reg_proCodigo.splice(pos,1);
          reg_proDibujo.splice(pos,1);
          //console.log("arrCodigo",arrCodigo);
          cont=1;
          for (var i=0; i<reg_proDibujo.length;i++)
          {
            cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+reg_proDibujo[i].scd_codigo;
            cont++;
          }
          
          $('#regimenPropietario').html(cadena);
          cont=0;
          reg_proDibujo.forEach(function (item, index, array) 
          {
            $("#cnombrePropietario_in"+item.id+"").val(reg_proCodigo[cont].arg_propietario);
              $("#tipoPropiedad_in"+item.id+"").val(reg_proCodigo[cont].arg_tipoP);//document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
        });
      }
    }
//--------------------------- Fin Grilla 1 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 2 --------------------------------------------------------------------
var contCodigoref=2;
var ref_hisCodigo=[];
var ref_hisDibujo=[];
htmlREF='Referencias Históricas:</label>'+
'<div class="col-md-3">'+
'<select class="form-control" id="tipoRefrencia1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Nivel Internacional">Nivel Internacional</option>'+ 
'<option value="Nivel Local"> Nivel Local</option>'+
'<option value="Nivel Nacional"> Nivel Nacional</option>'+
'<option value="Nivel Regional"> Nivel Regional</option>'+  
'</select> '+
'</div>'+
'<label class="col-md-2" for="">Observacion:</label>'+
'<div class="col-md-3">'+
' <input id="observacion1" type="text" class="form-control"  placeholder="Descripcion Observación"> '+
'</div><div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMi(1);"></i>'+
'</a></div></div>';
var cadena = '';  
ref_hisCodigo.push({arg_tipoRef:'N',arg_observacion:''});
ref_hisDibujo.push({scd_codigo:htmlREF,id: 1});

cadena='<div class="form-group"><label class="col-md-3">1. '+ref_hisDibujo[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#referenciaHistorica').html(cadena);
    $("#tipoRefrencia1").val(ref_hisCodigo[0].arg_tipoRef);
    $("#observacion1").val(ref_hisCodigo[0].arg_observacion);
    

    function crearReferenciaHistorica(){
      var htmlREF;
      htmlREF='Referencias Históricas:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="tipoRefrencia'+contCodigoref+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Nivel Internacional">Nivel Internacional</option>'+ 
      '<option value="Nivel Local"> Nivel Local</option>'+
      '<option value="Nivel Nacional"> Nivel Nacional</option>'+
      '<option value="Nivel Regional"> Nivel Regional</option>'+  
      '</select> '+
      '</div>'+
      '<label class="col-md-2" for="">Observacion:</label>'+
      '<div class="col-md-3">'+
      ' <input id="observacion'+contCodigoref+'" type="text" class="form-control"  placeholder="Descripcion Observación"> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMi('+contCodigoref+');"></i>'+
      '</a></div></div>';
      var cadena='';
      var cont=0;
      ref_hisDibujo.forEach(function (item, index, array) 
      {
        ref_hisCodigo[cont].arg_tipoRef = $("#tipoRefrencia"+item.id+"").val();
        ref_hisCodigo[cont].arg_observacion = $("#observacion"+item.id+"").val();
        cont++;
      });
      ref_hisCodigo.push({arg_tipoRef:'N',arg_observacion:''});
      ref_hisDibujo.push({scd_codigo:htmlREF,id: contCodigoref});
      contCodigoref++;
      cont=1;
      for (var i=0; i<ref_hisDibujo.length;i++)
      {
        cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+'. '+ref_hisDibujo[i].scd_codigo;
        cont++;
      }
      $('#referenciaHistorica').html(cadena); 
      var conto=0;
      ref_hisDibujo.forEach(function (item, index, array) 
      { 
        $("#tipoRefrencia"+item.id+"").val(ref_hisCodigo[conto].arg_tipoRef);
        $("#observacion"+item.id+"").val(ref_hisCodigo[conto].arg_observacion);
        conto=conto+1;
      });
      
    }


    function menosLineaMi($id){
      if(contCodigoref>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el argumento seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          ref_hisDibujo.forEach(function (item, index, array) 
          {
            ref_hisCodigo[cont].arg_tipoRef=$("#tipoRefrencia"+item.id+"").val();
            ref_hisCodigo[cont].arg_observacion=$("#observacion"+item.id+"").val();
            cont=cont+1;
          });
          pos = ref_hisDibujo.map(function(e) {return e.id;}).indexOf($id);
          //console.log("poos",pos);
          ref_hisCodigo.splice(pos,1);
          ref_hisDibujo.splice(pos,1);
          cont=1;
          for (var i=0; i<ref_hisDibujo.length;i++)
          {
            cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+ref_hisDibujo[i].scd_codigo;
            cont++;
          }
          
          $('#referenciaHistorica').html(cadena);
          
          cont=0;
          ref_hisDibujo.forEach(function (item, index, array) 
          {
            $("#tipoRefrencia"+item.id+"").val(ref_hisCodigo[cont].arg_tipoRef);
              $("#observacion"+item.id+"").val(ref_hisCodigo[cont].arg_observacion);//document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
        });
      }
    }
//--------------------------- Fin Grilla 2 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 3 --------------------------------------------------------------------
var contCodigo1=2;
var muro_extCodigo=[];
var muro_extDibujo=[];
htmlMurExt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estado1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridad1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipo1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Policromia">Policromia</option>'+ 
'<option value="Bicromia"> Bicromia</option>'+
'<option value="Monocromia">Monocromia</option>'+
'<option value="Pintura Mural">Pintura Mural</option>'+
'<option value="Esgrafiado">Esgrafiado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="material1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Cal">Cal</option>'+ 
'<option value="Latex"> Latex</option>'+
'<option value="Ocre">Ocre</option>'+
'<option value="Oleo">Oleo</option>'+
'<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroExt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
muro_extCodigo.push({arg_estado:'N',arg_integridad:'N',arg_tipo:'N',arg_material:'N'});
muro_extDibujo.push({scd_codigo:htmlMurExt,id: 1});

cadena=muro_extDibujo[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divacaPinturasMurosExteriores').html(cadena);
    $("#estado1").val(muro_extCodigo[0].arg_estado);
    $("#integridad1").val(muro_extCodigo[0].arg_integridad);
    $("#tipo1").val(muro_extCodigo[0].arg_tipo);
    $("#material1").val(muro_extCodigo[0].arg_material);
    

    function acaPinturasMurosExteriores(){
      var htmlMurExt;
      htmlMurExt=
      '<div class="col-md-4">'+
      '<select class="form-control" id="estado'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Bueno">Bueno</option>'+ 
      '<option value="Regular"> Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="integridad'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Original">Original</option>'+ 
      '<option value="Nuevo"> Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2">'+
      '<select class="form-control" id="tipo'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Policromia">Policromia</option>'+ 
      '<option value="Bicromia"> Bicromia</option>'+
      '<option value="Monocromia">Monocromia</option>'+
      '<option value="Pintura Mural">Pintura Mural</option>'+
      '<option value="Esgrafiado">Esgrafiado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2">'+
      '<select class="form-control" id="material'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Cal">Cal</option>'+ 
      '<option value="Latex"> Latex</option>'+
      '<option value="Ocre">Ocre</option>'+
      '<option value="Oleo">Oleo</option>'+
      '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroExt('+contCodigo1+');"></i>'+
      '</a></div><br>';
      var cadena='';
      var cont=0;
      muro_extDibujo.forEach(function (item, index, array) 
      {
        muro_extCodigo[cont].arg_estado = $("#estado"+item.id+"").val();
        muro_extCodigo[cont].arg_integridad = $("#integridad"+item.id+"").val();
        muro_extCodigo[cont].arg_tipo = $("#tipo"+item.id+"").val();
        muro_extCodigo[cont].arg_material = $("#material"+item.id+"").val();

        cont++;
      });
      muro_extCodigo.push({arg_estado:'N',arg_integridad:'N',arg_tipo:'N',arg_material:'N'});
      muro_extDibujo.push({scd_codigo:htmlMurExt,id: contCodigo1});
      contCodigo1++;
      cont=1;
      for (var i=0; i<muro_extDibujo.length;i++)
      {
        cadena=cadena + muro_extDibujo[i].scd_codigo;
        cont++;
      }
      $('#divacaPinturasMurosExteriores').html(cadena); 
      var conto=0;
      muro_extDibujo.forEach(function (item, index, array) 
      { 
        $("#estado"+item.id+"").val(muro_extCodigo[conto].arg_estado);
        $("#integridad"+item.id+"").val(muro_extCodigo[conto].arg_integridad);
        $("#tipo"+item.id+"").val(muro_extCodigo[conto].arg_tipo);
        $("#material"+item.id+"").val(muro_extCodigo[conto].arg_material);
        conto=conto+1;
      });
    }

    function menosLineaMuroExt($id){
      if(contCodigo1>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el argumento seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          muro_extDibujo.forEach(function (item, index, array) 
          {
            muro_extCodigo[cont].arg_estado=$("#estado"+item.id+"").val();
            muro_extCodigo[cont].arg_integridad=$("#integridad"+item.id+"").val();
            muro_extCodigo[cont].arg_tipo=$("#tipo"+item.id+"").val();
            muro_extCodigo[cont].arg_material=$("#material"+item.id+"").val();
            cont=cont+1;
          });
          pos = muro_extDibujo.map(function(e) {return e.id;}).indexOf($id);
          //console.log("poos",pos);
          muro_extCodigo.splice(pos,1);
          muro_extDibujo.splice(pos,1);
          cont=1;
          for (var i=0; i<muro_extDibujo.length;i++)
          {
            cadena=cadena + muro_extDibujo[i].scd_codigo;
            cont++;
          }
          
          $('#divacaPinturasMurosExteriores').html(cadena);
          
          cont=0;
          muro_extDibujo.forEach(function (item, index, array) 
          {
            $("#estado"+item.id+"").val(muro_extCodigo[cont].arg_estado);
            $("#integridad"+item.id+"").val(muro_extCodigo[cont].arg_integridad);
            $("#tipo"+item.id+"").val(muro_extCodigo[cont].arg_tipo);
            $("#material"+item.id+"").val(muro_extCodigo[cont].arg_material);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
        });
      }
    }

//-----------------GRILLA NANCY

//------GRILLAS PARA BINES INMUEBLES (BLOQUES)-----

var contPP=2;
var arrPP=[];
var arrDibujoPP=[];

htmlPP='Figura de Peligros Potenciales:</label>'+
'<select class="form-control" id="peligropPP1" name="peligropPP1">'+
'<option value="PP" selected="selected">--Seleccione--</option>'+
'<option value="Desastres Naturales">Desastres Naturales</option>'+
'<option value="Descarasterización">Descarasterización</option>'+
'<option value="Otros Riesgos">Otros Riesgos</option>'+
'<option value="Riesgos Extremos">Riesgos Extremos</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-4">'+
'<label class="control-label">Peligros Pontenciales:</label>'+
'<select class="form-control" id="peligropPPO1" name="peligropPPO1">'+
'<option value="PP" selected="selected">--Seleccione--</option>'+
'<option value="Hundimiento de Terreno">Hundimiento de Terreno</option>'+
'<option value="Inundación por filtración de aguas servidasl">Inundación por filtración de aguas servidas</option>'+
'<option value="Inundación por lluvias">Inundación por lluvias</option>'+
'<option value="Sismos">Sismos</option>'+
'<option value="Tormentas Eléctricasl">Tormentas Eléctricas</option>'+
'<option value="Vientos Fuertes">Vientos Fuertes</option>'+
'<option value="Cableado Aereo">Cableado Aereo</option>'+
'<option value="Comercio Informal">Comercio Informal</option>'+
'<option value="Metamorfosis urbana">Metamorfosis urbana</option>'+
'<option value="Parqueo">Parqueo</option>'+
'<option value="Publicidad Exterior inadecuada">Publicidad Exterior inadecuada</option>'+
'<option value="Explosiones">Explosiones</option>'+
'<option value="Fugas y Derrames tóxicos">Fugas y Derrames tóxicos</option>'+
'<option value="Incendios"> Incendios</option>'+
'<option value="Mitines y marchas populares">Mitines y marchas populares</option>'+
'<option value="Cables de alta Tención">Cables de alta Tención</option>'+
'<option value="Construcciones vecinas por colapsar">Construcciones vecinas por colapsar</option>'+
'<option value="Contaminación Acustica">Contaminación Acustica</option>'+
'<option value="Contaminación Ambiental">Contaminación Ambiental</option>'+
'<option value="Ductos en mal estado">Ductos en mal estado</option>'+
'<option value="Objetos ajenos que pueden caer o deslizarse">Objetos ajenos que pueden caer o deslizarse</option>'+
'<option value="Objetos propios queb pueden caer o deslizarse">Objetos propios queb pueden caer o deslizarse</option>'+
'<option value="Sustancias toxicas o inflamables">Sustancias toxicas o inflamables</option>'+
'<option value="Vibraciones">Vibraciones</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-3">'+
'<label class="control-label">Causas:</label>'+
'<select class="form-control" id="causasPP1" name="causasPP1">'+
'<option value="PC" selected="selected">--Seleccione--</option>'+
'<option value="Alto">Alto</option>'+
'<option value="Mediol">Medio</option>'+
'<option value="Bajo">Bajo</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Peligros Potenciales" onclick="darBajaPP(1);"></i>'+
'</a></div>';
var cadena = '';  
arrPP.push({val_pe:'PP', val_pel:'PP', val_cau:'PC'});
arrDibujoPP.push({scd_codigo:htmlPP,id: 1});

cadena='<div class="form-group col-md-4"><label class="control-label">1. '+arrDibujoPP[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#peligrospotenciales').html(cadena);
    $("#peligropPP1").val(arrPP[0].val_pe);
    $("#peligropPPO1").val(arrPP[0].val_pel);
    $("#causasPP1").val(arrPP[0].val_cau);
    

    function crearPeligrosPotenciales(){
      var htmlPP;
      htmlPP='Figura de Peligros Potenciales:</label>'+
      '<select class="form-control" id="peligropPP'+contPP+'" name="peligropPP'+contPP+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Desastres Naturales">Desastres Naturales</option>'+
      '<option value="Descarasterización">Descarasterización</option>'+
      '<option value="Otros Riesgos">Otros Riesgos</option>'+
      '<option value="Riesgos Extremos">Riesgos Extremos</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-4">'+
      '<label class="control-label">Peligros Pontenciales:</label>'+
      '<select class="form-control" id="peligropPPO'+contPP+'" name="peligropPPO'+contPP+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Hundimiento de Terreno">Hundimiento de Terreno</option>'+
      '<option value="Inundación por filtración de aguas servidasl">Inundación por filtración de aguas servidas</option>'+
      '<option value="Inundación por lluvias">Inundación por lluvias</option>'+
      '<option value="Sismos">Sismos</option>'+
      '<option value="Tormentas Eléctricasl">Tormentas Eléctricas</option>'+
      '<option value="Vientos Fuertes">Vientos Fuertes</option>'+
      '<option value="Cableado Aereo">Cableado Aereo</option>'+
      '<option value="Comercio Informal">Comercio Informal</option>'+
      '<option value="Metamorfosis urbana">Metamorfosis urbana</option>'+
      '<option value="Parqueo">Parqueo</option>'+
      '<option value="Publicidad Exterior inadecuada">Publicidad Exterior inadecuada</option>'+
      '<option value="Explosiones">Explosiones</option>'+
      '<option value="Fugas y Derrames tóxicos">Fugas y Derrames tóxicos</option>'+
      '<option value="Incendios"> Incendios</option>'+
      '<option value="Mitines y marchas populares">Mitines y marchas populares</option>'+
      '<option value="Cables de alta Tención">Cables de alta Tención</option>'+
      '<option value="Construcciones vecinas por colapsar">Construcciones vecinas por colapsar</option>'+
      '<option value="Contaminación Acustica">Contaminación Acustica</option>'+
      '<option value="Contaminación Ambiental">Contaminación Ambiental</option>'+
      '<option value="Ductos en mal estado">Ductos en mal estado</option>'+
      '<option value="Objetos ajenos que pueden caer o deslizarse">Objetos ajenos que pueden caer o deslizarse</option>'+
      '<option value="Objetos propios queb pueden caer o deslizarse">Objetos propios queb pueden caer o deslizarse</option>'+
      '<option value="Sustancias toxicas o inflamables">Sustancias toxicas o inflamables</option>'+
      '<option value="Vibraciones">Vibraciones</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Causas:</label>'+
      '<select class="form-control" id="causasPP'+contPP+'" name="causasPP'+contPP+'">'+
      '<option value="PC" selected="selected">--Seleccione--</option>'+
      '<option value="Alto">Alto</option>'+
      '<option value="Mediol">Medio</option>'+
      '<option value="Bajo">Bajo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Peligros Potenciales" onclick="darBajaPP('+contPP+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoPP.forEach(function (item, index, array) 
      {
        arrPP[cont].val_pe = $("#peligropPP"+item.id+"").val();
        arrPP[cont].val_pel = $("#peligropPPO"+item.id+"").val();
        arrPP[cont].val_cau = $("#causasPP"+item.id+"").val();
        cont++;
      });

      arrPP.push({val_pe:'PP', val_pel:'PP',val_cau:'PC'});
      arrDibujoPP.push({scd_codigo:htmlPP,id: contPP});
      contPP++;
      cont=1;
      for (var i=0; i<arrDibujoPP.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+arrDibujoPP[i].scd_codigo;
        cont++;
      }
        ////console.log("cadenaaaa",cadena);
        $('#peligrospotenciales').html(cadena); 
        var conto=0;
        arrDibujoPP.forEach(function (item, index, array) 
        { 
          //console.log('arrPP[conto].val_pe P',arrPP[conto].val_pe);
          $("#peligropPP"+item.id+"").val(arrPP[conto].val_pe);
          $("#peligropPPO"+item.id+"").val(arrPP[conto].val_pel);
          $("#causasPP"+item.id+"").val(arrPP[conto].val_cau);
          conto=conto+1;
        });
      }

      function darBajaPP($id){
        if(contPP>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoPP.forEach(function (item, index, array) 
            {
              arrPP[cont].val_pe=$("#peligropPP"+item.id+"").val();
              arrPP[cont].val_pel=$("#peligropPPO"+item.id+"").val();
              arrPP[cont].val_cau=$("#causasPP"+item.id+"").val();
              cont=cont+1;
            });
            //console.log('',);
            pos = arrDibujoPP.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrPP.splice(pos,1);
            arrDibujoPP.splice(pos,1);
            //console.log("arrPP",arrPP);

            cont=1;
            for (var i=0; i<arrDibujoPP.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+cont+arrDibujoPP[i].scd_codigo;
              cont++;
            }

            $('#peligrospotenciales').html(cadena);

            cont=0;
            arrDibujoPP.forEach(function (item, index, array) 
            {
              $("#peligropPP"+item.id+"").val(arrPP[cont].val_pe);
              $("#peligropPPO"+item.id+"").val(arrPP[cont].val_pel);
              $("#causasPP"+item.id+"").val(arrPP[cont].val_cau);
              cont=cont+1;
            });
          });
        }
        //console.log("arrPPFinal",arrPP);
      }


//////////////// Patologias

var contPE=2;
var arrPE=[];
var arrDibujoPE=[];

htmlPE='Figura de Patologias Edificación:</label>'+
'<select class="form-control" id="dañosPE1" name="dañosPE1">'+
'<option value="PE" selected="selected">--Seleccione--</option>'+
'<option value="Asentamiento">Asentamiento</option>'+
'<option value="Desplomes">Desplomes</option>'+
'<option value="Desprendimientos">Desprendimientos</option>'+
'<option value="Disgregación de MAterial">Disgregación de MAterial</option>'+
'<option value="Eflorescencias">Eflorescencias</option>'+
'<option value="Grietas y/o Fisuras">Grietas y/o Fisuras</option>'+
'<option value="Podredumbre">Podredumbre</option>'+
'<option value="Suciedad Superficial">Suciedad Superficial</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-5">'+
'<label class="control-label">Causas:</label>'+
'<select class="form-control" id="causasPE1" name="causasPE1">'+
'<option value="PC" selected="selected">--Seleccione--</option>'+
'<option value="Abandono">Abandono</option>'+
'<option value="Acción del Hombre">Acción del Hombre</option>'+
'<option value="Agentes Botánicos">Agentes Botánicos</option>'+
'<option value="Catástrofe Natural">Catástrofe Natural</option>'+
'<option value="Deficiencias Constructivas">Deficiencias Constructivas</option>'+
'<option value="Falta de Mantenimiento">Falta de Mantenimiento</option>'+
'<option value="Humedad">Humedad</option>'+
'<option value="Insectos">Insectos</option>'+
'<option value="Imtemperie">Imtemperie</option>'+
'<option value="Mala Calidad de Materiales">Mala Calidad de Materiales</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Patologías Edificación" onclick="darBajaPE(1);"></i>'+
'</a></div>';
var cadena = '';  
arrPE.push({val_daños:'PE', val_causas:'PC'});
arrDibujoPE.push({scd_codigo:htmlPE,id: 1});
//console.log("arrPE",arrPE);

cadena='<div class="form-group col-md-6"><label class="control-label">'+arrDibujoPE[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#patologiaedificacion').html(cadena);
    $("#dañosPE1").val(arrPE[0].val_daños);
    $("#causasPE1").val(arrPE[0].val_causas);


    function crearPatologiaEdificacion(){
      var htmlPE;
      htmlPE='Figura de Patologias Edificación:</label>'+
      '<select class="form-control" id="dañosPE'+contPE+'" name="dañosPE'+contPE+'">'+
      '<option value="PL" selected="selected">--Seleccione--</option>'+
      '<option value="Asentamiento">Asentamiento</option>'+
      '<option value="Desplomes">Desplomes</option>'+
      '<option value="Desprendimientos">Desprendimientos</option>'+
      '<option value="Disgregación de MAterial">Disgregación de MAterial</option>'+
      '<option value="Eflorescencias">Eflorescencias</option>'+
      '<option value="Grietas y/o Fisuras">Grietas y/o Fisuras</option>'+
      '<option value="Podredumbre">Podredumbre</option>'+
      '<option value="Suciedad Superficial">Suciedad Superficial</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<label class="control-label">Causas:</label>'+
      '<select class="form-control" id="causasPE'+contPE+'" name="causasPE'+contPE+'">'+
      '<option value="C" selected="selected">--Seleccione--</option>'+
      '<option value="Abandono">Abandono</option>'+
      '<option value="Acción del Hombre">Acción del Hombre</option>'+
      '<option value="Agentes Botánicos">Agentes Botánicos</option>'+
      '<option value="Catástrofe Natural">Catástrofe Natural</option>'+
      '<option value="Deficiencias Constructivas">Deficiencias Constructivas</option>'+
      '<option value="Falta de Mantenimiento">Falta de Mantenimiento</option>'+
      '<option value="Humedad">Humedad</option>'+
      '<option value="Insectos">Insectos</option>'+
      '<option value="Imtemperie">Imtemperie</option>'+
      '<option value="Mala Calidad de Materiales">Mala Calidad de Materiales</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Patologías Edificación" onclick="darBajaPE('+contPE+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoPE.forEach(function (item, index, array) 
      {
        arrPE[cont].val_daños = $("#dañosPE"+item.id+"").val();
        arrPE[cont].val_causas = $("#causasPE"+item.id+"").val();
        cont++;
      });
      arrPE.push({val_daños:'PE', val_causas:'PC'});
      arrDibujoPE.push({scd_codigo:htmlPE,id: contPE});
      contPE++;
      cont=1;
      for (var i=0; i<arrDibujoPE.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoPE[i].scd_codigo;
        cont++;
      }
        ////console.log("cadenaaaa",cadena);
        $('#patologiaedificacion').html(cadena); 
        var conto=0;
        arrDibujoPE.forEach(function (item, index, array) 
        { 
          $("#dañosPE"+item.id+"").val(arrPE[conto].val_daños);
          $("#causasPE"+item.id+"").val(arrPE[conto].val_causas);
          conto=conto+1;
        });
        
      }

      function darBajaPE($id){
        if(contPE>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoPE.forEach(function (item, index, array) 
            {
              arrPE[cont].val_daños=$("#dañosPE"+item.id+"").val();
              arrPE[cont].val_causas=$("#causasPE"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoPE.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrPE.splice(pos,1);
            arrDibujoPE.splice(pos,1);
            //console.log("arrPE",arrPE);

            cont=1;
            for (var i=0; i<arrDibujoPE.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+arrDibujoPE[i].scd_codigo;
              cont++;
            }

            $('#patologiaedificacion').html(cadena);

            cont=0;
            arrDibujoPE.forEach(function (item, index, array) 
            {
              $("#dañosPE"+item.id+"").val(arrPE[cont].val_daños);
              $("#causasPE"+item.id+"").val(arrPE[cont].val_causas);
              cont=cont+1;
            });
          });
        }
        
      }


    //------GRILLA INSTALACIONES ESTADO----------------------
    var contINE=2;
    var arrINE=[];
    var arrDibujoINE=[];

    htmlINE='Instalacones Estado:</label>'+
    '<select class="form-control" id="instalacionINE1" name="instalacionINE1">'+
    '<option value="IE" selected="selected">--Seleccione--</option>'+
    '<option value="Telefónica">Telefónica</option>'+
    '<option value="Energia Electrica">Energia Electrica</option>'+
    '<option value="Gas">Gas</option>'+
    '<option value="Sanitaria">Sanitaria</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Piso de Servidumbre:</label>'+
    '<select class="form-control" id="pisoservINE1" name="pisoservINE1">'+
    '<option value="PS" selected="selected">--Seleccione--</option>'+
    '<option value="SI">SI</option>'+
    '<option value="NO">NO</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Estado:</label>'+
    '<select class="form-control" id="estadoINE1" name="estadoINE1">'+
    '<option value="E" selected="selected">--Seleccione--</option>'+
    '<option value="Bueno">Bueno</option>'+
    '<option value="Regular">Regular</option>'+
    '<option value="Malo">Malo</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="integridadINE1" name="integridadINE1">'+
    '<option value="II" selected="selected">--Seleccione--</option>'+
    '<option value="Original">Original</option>'+
    '<option value="Nuevo">Nuevo</option>'+
    '<option value="Modificado">Modificado</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaINE(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrINE.push({var_inst_est:'IE', var_pisose:'PS', var_est:'E', var_int:'II'});
    arrDibujoINE.push({scd_codigo:htmlINE,id: 1});
    //console.log("arrINE",arrINE);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujoINE[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cinstalacionesestado').html(cadena);
    $("#instalacionINE1").val(arrINE[0].var_inst_est);
    $("#pisoservINE1").val(arrINE[0].var_pisose);
    $("#estadoINE1").val(arrINE[0].var_est);
    $("#integridadINE1").val(arrINE[0].var_int);


    function crearInstalacionesEstado(){
      var htmlINE;
      htmlINE='Instalacones Estado:</label>'+
      '<select class="form-control" id="instalacionINE'+contINE+'" name="instalacionINE'+contINE+'">'+
      '<option value="IE" selected="selected">--Seleccione--</option>'+
      '<option value="Telefónica">Telefónica</option>'+
      '<option value="Energia Electrica">Energia Electrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Sanitaria">Sanitaria</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Piso de Servidumbre:</label>'+
      '<select class="form-control" id="pisoservINE'+contINE+'" name="pisoservINE'+contINE+'">'+
      '<option value="PS" selected="selected">--Seleccione--</option>'+
      '<option value="SI">SI</option>'+
      '<option value="NO">NO</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Estado:</label>'+
      '<select class="form-control" id="estadoINE'+contINE+'" name="estadoINE'+contINE+'">'+
      '<option value="E" selected="selected">--Seleccione--</option>'+
      '<option value="Bueno">Bueno</option>'+
      '<option value="Regular">Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<label class="control-label">Integridad:</label>'+
      '<select class="form-control" id="integridadINE'+contINE+'" name="integridadINE'+contINE+'">'+
      '<option value="II" selected="selected">--Seleccione--</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Instalaciones Estado" onclick="darBajaINE('+contINE+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoINE.forEach(function (item, index, array) 
      {
        arrINE[cont].var_inst_est = $("#instalacionINE"+item.id+"").val();
        arrINE[cont].var_pisose = $("#pisoservINE"+item.id+"").val();
        arrINE[cont].var_est = $("#estadoINE"+item.id+"").val();
        arrINE[cont].var_int = $("#integridadINE"+item.id+"").val();
        cont++;
      });
      arrINE.push({var_inst_est:'IE', var_pisose:'PS', var_est:'E', var_int:'II'});
      arrDibujoINE.push({scd_codigo:htmlINE,id: contINE});
      contINE++;
      cont=1;
      for (var i=0; i<arrDibujoINE.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoINE[i].scd_codigo;
        cont++;
      }
        ////console.log("cadenaaaa",cadena);
        $('#cinstalacionesestado').html(cadena); 
        var conto=0;
        arrDibujoINE.forEach(function (item, index, array) 
        { 
          $("#instalacionINE"+item.id+"").val(arrINE[conto].var_inst_est);
          $("#pisoservINE"+item.id+"").val(arrINE[conto].var_pisose);
          $("#estadoINE"+item.id+"").val(arrINE[conto].var_est);
          $("#integridadINE"+item.id+"").val(arrINE[conto].var_int);
          conto=conto+1;
        });
        
      }


      function darBajaINE($id){
        if(contINE>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoINE.forEach(function (item, index, array) 
            {
              arrINE[cont].var_inst_est=$("#instalacionINE"+item.id+"").val();
              arrINE[cont].var_pisose=$("#pisoservINE"+item.id+"").val();
              arrINE[cont].var_est=$("#estadoINE"+item.id+"").val();
              arrINE[cont].var_int=$("#integridadINE"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoINE.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrINE.splice(pos,1);
            arrDibujoINE.splice(pos,1);
            //console.log("arrINE",arrINE);

            cont=1;
            for (var i=0; i<arrDibujoINE.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoINE[i].scd_codigo;
              cont++;
            }

            $('#cinstalacionesestado').html(cadena);

            cont=0;
            arrDibujoINE.forEach(function (item, index, array) 
            {
              $("#instalacionINE"+item.id+"").val(arrINE[cont].var_inst_est);
              $("#pisoservINE"+item.id+"").val(arrINE[cont].var_pisose);
              $("#estadoINE"+item.id+"").val(arrINE[cont].var_est);
              $("#integridadINE"+item.id+"").val(arrINE[cont].var_int);
              cont=cont+1;
            });
          });
        }
        
      }
//--------------GRILLA SERVICIOS------------------

var contSERV=2;
var arrSERV=[];
var arrDibujoSERV=[];

htmlSERV='Descripción:</label>'+
'<select class="form-control" id="descripcionSERV1" name="descripcionSERV1">'+
'<option value="D" selected="selected">--Seleccione--</option>'+
'<option value="Agua">Agua</option>'+
'<option value="Energia Eléctrica">Energia Eléctrica</option>'+
'<option value="Gas">Gas</option>'+
'<option value="Recolección de Basura">Recolección de Basura</option>'+
'<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
'<option value="Seguridad">Seguridad</option>'+
'<option value="Otros Servicios">Otros Servicios</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-5">'+
'<label class="control-label">Proveedor:</label>'+
'<select class="form-control" id="proveedorSERV1" name="proveedorSERV1">'+
'<option value="P" selected="selected">--Seleccione--</option>'+
'<option value="Red Pública">Red Pública</option>'+
'<option value="Red Pública">Red Pública</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaSERV(1);"></i>'+
'</a></div>';
var cadena = '';  
arrSERV.push({val_desc:'D', val_prov:'P'});
arrDibujoSERV.push({scd_codigo:htmlSERV,id: 1});
//console.log("arrSERV",arrSERV);

cadena='<div class="form-group col-md-6"><label class="control-label">1. '+arrDibujoSERV[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cservicios').html(cadena);
    $("#descripcionSERV1").val(arrSERV[0].val_desc);
    $("#proveedorSERV1").val(arrSERV[0].val_prov);

    function crearServicios(){
      var htmlSERV;
      htmlSERV='Descripción:</label>'+
      '<select class="form-control" id="descripcionSERV'+contSERV+'" name="descripcionSERV'+contSERV+'">'+
      '<option value="D" selected="selected">--Seleccione--</option>'+
      '<option value="Agua">Agua</option>'+
      '<option value="Energia Eléctrica">Energia Eléctrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Recolección de Basura">Recolección de Basura</option>'+
      '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
      '<option value="Seguridad">Seguridad</option>'+
      '<option value="Otros Servicios">Otros Servicios</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<label class="control-label">Proveedor:</label>'+
      '<select class="form-control" id="proveedorSERV'+contSERV+'" name="proveedorSERV'+contSERV+'">'+
      '<option value="P" selected="selected">--Seleccione--</option>'+
      '<option value="Red Pública">Red Pública</option>'+
      '<option value="Red Pública">Red Pública</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Servicios" onclick="darBajaSERV('+contSERV+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoSERV.forEach(function (item, index, array) 
      {
        arrSERV[cont].val_desc = $("#descripcionSERV"+item.id+"").val();
        arrSERV[cont].val_prov = $("#proveedorSERV"+item.id+"").val();
        cont++;
      });
      arrSERV.push({val_desc:'D', val_prov:'P'});
      arrDibujoSERV.push({scd_codigo:htmlSERV,id: contINE});
      contINE++;
      cont=1;
      for (var i=0; i<arrDibujoSERV.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoSERV[i].scd_codigo;
        cont++;
      }
        ////console.log("cadenaaaa",cadena);
        $('#cservicios').html(cadena); 
        var conto=0;
        arrDibujoSERV.forEach(function (item, index, array) 
        { 
          $("#descripcionSERV"+item.id+"").val(arrSERV[conto].val_desc);
          $("#proveedorSERV"+item.id+"").val(arrSERV[conto].val_prov);
          conto=conto+1;
        });
        
      }

      function darBajaSERV($id){
        if(contINE>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoSERV.forEach(function (item, index, array) 
            {
              arrSERV[cont].val_desc=$("#descripcionSERV"+item.id+"").val();
              arrSERV[cont].val_prov=$("#proveedorSERV"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoSERV.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrSERV.splice(pos,1);
            arrDibujoSERV.splice(pos,1);
            //console.log("arrSERV",arrSERV);

            cont=1;
            for (var i=0; i<arrDibujoSERV.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+arrDibujoSERV[i].scd_codigo;
              cont++;
            }

            $('#cservicios').html(cadena);

            cont=0;
            arrDibujoSERV.forEach(function (item, index, array) 
            {
              $("#descripcionSERV"+item.id+"").val(arrSERV[cont].val_desc);
              $("#proveedorSERV"+item.id+"").val(arrSERV[cont].val_prov);
              cont=cont+1;
            });
          });
        }
        
      }



//-----------------FINAL NANCY


//--------------------------- Fin Grilla 3 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 4 --------------------------------------------------------------------
var contCodigo2=2;
var muro_intCodigo=[];
var muro_intDibujo=[];
htmlMurInt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estadoInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridadInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipoInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Policromia">Policromia</option>'+ 
'<option value="Bicromia"> Bicromia</option>'+
'<option value="Monocromia">Monocromia</option>'+
'<option value="Pintura Mural">Pintura Mural</option>'+
'<option value="Esgrafiado">Esgrafiado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="materialInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Cal">Cal</option>'+ 
'<option value="Latex"> Latex</option>'+
'<option value="Ocre">Ocre</option>'+
'<option value="Oleo">Oleo</option>'+
'<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroInt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
muro_intCodigo.push({arg_estadoInt:'N',arg_integridadInt:'N',arg_tipoInt:'N',arg_materialInt:'N'});
muro_intDibujo.push({scd_codigo:htmlMurInt,id: 1});

cadena=muro_intDibujo[0].scd_codigo;
////console.log("cadenaaaa",cadena);
$('#acaPinturasMurosInteriores').html(cadena);
$("#estadoInt1").val(muro_intCodigo[0].arg_estadoInt);
$("#integridadInt1").val(muro_intCodigo[0].arg_integridadInt);
$("#tipoInt1").val(muro_intCodigo[0].arg_tipoInt);
$("#materialInt1").val(muro_intCodigo[0].arg_materialInt);


function acaPinturasMurosInteriores(){
  var htmlMurInt;
  htmlMurInt=
  '<div class="col-md-4">'+
  '<select class="form-control" id="estadoInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3">'+
  '<select class="form-control" id="integridadInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2">'+
  '<select class="form-control" id="tipoInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Policromia">Policromia</option>'+ 
  '<option value="Bicromia"> Bicromia</option>'+
  '<option value="Monocromia">Monocromia</option>'+
  '<option value="Pintura Mural">Pintura Mural</option>'+
  '<option value="Esgrafiado">Esgrafiado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2">'+
  '<select class="form-control" id="materialInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Cal">Cal</option>'+ 
  '<option value="Latex"> Latex</option>'+
  '<option value="Ocre">Ocre</option>'+
  '<option value="Oleo">Oleo</option>'+
  '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroInt('+contCodigo2+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  muro_intDibujo.forEach(function (item, index, array) 
  {
    muro_intCodigo[cont].arg_estadoInt = $("#estadoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_integridadInt = $("#integridadInt"+item.id+"").val();
    muro_intCodigo[cont].arg_tipoInt = $("#tipoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_materialInt = $("#materialInt"+item.id+"").val();

    cont++;
  });
  muro_intCodigo.push({arg_estadoInt:'N',arg_integridadInt:'N',arg_tipoInt:'N',arg_materialInt:'N'});
  muro_intDibujo.push({scd_codigo:htmlMurInt,id: contCodigo2});
  contCodigo2++;
  cont=1;
  for (var i=0; i<muro_intDibujo.length;i++)
  {
    cadena=cadena + muro_intDibujo[i].scd_codigo;
    cont++;
  }
  $('#acaPinturasMurosInteriores').html(cadena); 
  var conto=0;
  muro_intDibujo.forEach(function (item, index, array) 
  { 
    $("#estadoInt"+item.id+"").val(muro_intCodigo[conto].arg_estadoInt);
    $("#integridadInt"+item.id+"").val(muro_intCodigo[conto].arg_integridadInt);
    $("#tipoInt"+item.id+"").val(muro_intCodigo[conto].arg_tipoInt);
    $("#materialInt"+item.id+"").val(muro_intCodigo[conto].arg_materialInt);
    conto=conto+1;
  });
}

function menosLineaMuroInt($id){
  if(contCodigo2>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      muro_intDibujo.forEach(function (item, index, array) 
      {
        muro_intCodigo[cont].arg_estadoInt=$("#estadoInt"+item.id+"").val();
        muro_intCodigo[cont].arg_integridadInt=$("#integridadInt"+item.id+"").val();
        muro_intCodigo[cont].arg_tipoInt=$("#tipoInt"+item.id+"").val();
        muro_intCodigo[cont].arg_materialInt=$("#materialInt"+item.id+"").val();
        cont=cont+1;
      });
      pos = muro_intDibujo.map(function(e) {return e.id;}).indexOf($id);
      //console.log("poos",pos);
      muro_intCodigo.splice(pos,1);
      muro_intDibujo.splice(pos,1);
      cont=1;
      for (var i=0; i<muro_intDibujo.length;i++)
      {
        cadena=cadena + muro_intDibujo[i].scd_codigo;
        cont++;
      }

      $('#acaPinturasMurosInteriores').html(cadena);
      cont=0;
      muro_intDibujo.forEach(function (item, index, array) 
      {
        $("#estadoInt"+item.id+"").val(muro_intCodigo[cont].arg_estadoInt);
        $("#integridadInt"+item.id+"").val(muro_intCodigo[cont].arg_integridadInt);
        $("#tipoInt"+item.id+"").val(muro_intCodigo[cont].arg_tipoInt);
        $("#materialInt"+item.id+"").val(muro_intCodigo[cont].arg_materialInt);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
    });
  }
}
//--------------------------- Fin Grilla 4 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 5 --------------------------------------------------------------------
var contCodigo3=2;
var aca_extCodigo=[];
var aca_extDibujo=[];
htmlAcaExt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estadoAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridadAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipoAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Revoque Grueso">Revoque Grueso</option>'+ 
'<option value="Revoque Fino"> Revoque Fino</option>'+
'<option value="Material Visto">Material Visto</option>'+
'<option value="Texturizado">Texturizado</option>'+
'<option value="Enchape">Enchape</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="materialAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Piedra">Piedra</option>'+ 
'<option value="Madera"> Madera</option>'+
'<option value="Ladrillo">Ladrillo</option>'+
'<option value="Cerámica">Cerámica</option>'+
'<option value="Marmol">Marmol</option>'+
'<option value="Azulejo">Azulejo</option>'+
'<option value="Cal y Arena">Cal y Arena</option>'+
'<option value="Barro y Paja">Barro y Paja</option>'+
'<option value="Cemento y Arena">Cemento y Arena</option>'+
'<option value="Yeso">Yeso</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoExt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
aca_extCodigo.push({arg_estadoAcaExt:'N',arg_integridadAcaExt:'N',arg_tipoAcaExt:'N',arg_materialAcaExt:'N'});
aca_extDibujo.push({scd_codigo:htmlAcaExt,id: 1});

cadena=aca_extDibujo[0].scd_codigo;
////console.log("cadenaaaa",cadena);
$('#divacaAcabadosMurosExteriores').html(cadena);
$("#estadoAcaExt1").val(aca_extCodigo[0].arg_estadoAcaExt);
$("#integridadAcaExt1").val(aca_extCodigo[0].arg_integridadAcaExt);
$("#tipoAcaExt1").val(aca_extCodigo[0].arg_tipoAcaExt);
$("#materialAcaExt1").val(aca_extCodigo[0].arg_materialAcaExt);


function acaAcabadosMurosExteriores(){
  var htmlAcaExt;
  htmlAcaExt=
  '<div class="col-md-4">'+
  '<select class="form-control" id="estadoAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3">'+
  '<select class="form-control" id="integridadAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2">'+
  '<select class="form-control" id="tipoAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2">'+
  '<select class="form-control" id="materialAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoExt('+contCodigo3+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  aca_extDibujo.forEach(function (item, index, array) 
  {
    aca_extCodigo[cont].arg_estadoAcaExt = $("#estadoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_integridadAcaExt = $("#integridadAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_tipoAcaExt = $("#tipoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_materialAcaExt = $("#materialAcaExt"+item.id+"").val();

    cont++;
  });
  aca_extCodigo.push({arg_estadoAcaExt:'N',arg_integridadAcaExt:'N',arg_tipoAcaExt:'N',arg_materialAcaExt:'N'});
  aca_extDibujo.push({scd_codigo:htmlAcaExt,id: contCodigo3});
  contCodigo3++;
  cont=1;
  for (var i=0; i<aca_extDibujo.length;i++)
  {
    cadena=cadena + aca_extDibujo[i].scd_codigo;
    cont++;
  }
  $('#divacaAcabadosMurosExteriores').html(cadena); 
  var conto=0;
  aca_extDibujo.forEach(function (item, index, array) 
  { 
    $("#estadoAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_estadoAcaExt);
    $("#integridadAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_integridadAcaExt);
    $("#tipoAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_tipoAcaExt);
    $("#materialAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_materialAcaExt);
    conto=conto+1;
  });
}
function menosLineaAcabadoExt($id){
  if(contCodigo3>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      aca_extDibujo.forEach(function (item, index, array) 
      {
        aca_extCodigo[cont].arg_estadoAcaExt=$("#estadoAcaExt"+item.id+"").val();
        aca_extCodigo[cont].arg_integridadAcaExt=$("#integridadAcaExt"+item.id+"").val();
        aca_extCodigo[cont].arg_tipoAcaExt=$("#tipoAcaExt"+item.id+"").val();
        aca_extCodigo[cont].arg_materialAcaExt=$("#materialAcaExt"+item.id+"").val();
        cont=cont+1;
      });
      pos = aca_extDibujo.map(function(e) {return e.id;}).indexOf($id);
      //console.log("poos",pos);
      aca_extCodigo.splice(pos,1);
      aca_extDibujo.splice(pos,1);
      cont=1;
      for (var i=0; i<aca_extDibujo.length;i++)
      {
        cadena=cadena + aca_extDibujo[i].scd_codigo;
        cont++;
      }

      $('#divacaAcabadosMurosExteriores').html(cadena);
      cont=0;
      aca_extDibujo.forEach(function (item, index, array) 
      {
        $("#estadoAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_estadoAcaExt);
        $("#integridadAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_integridadAcaExt);
        $("#tipoAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_tipoAcaExt);
        $("#materialAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_materialAcaExt);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
    });
  }
}
//--------------------------- Fin Grilla 5 --------------------------------------------------------------------


//--------------------------- Inicio Grilla 6 --------------------------------------------------------------------
var contCodigo4=2;
var aca_intCodigo=[];
var aca_intDibujo=[];
htmlAcaInt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estadoAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridadAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipoAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Revoque Grueso">Revoque Grueso</option>'+ 
'<option value="Revoque Fino"> Revoque Fino</option>'+
'<option value="Material Visto">Material Visto</option>'+
'<option value="Texturizado">Texturizado</option>'+
'<option value="Enchape">Enchape</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="materialAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Piedra">Piedra</option>'+ 
'<option value="Madera"> Madera</option>'+
'<option value="Ladrillo">Ladrillo</option>'+
'<option value="Cerámica">Cerámica</option>'+
'<option value="Marmol">Marmol</option>'+
'<option value="Azulejo">Azulejo</option>'+
'<option value="Cal y Arena">Cal y Arena</option>'+
'<option value="Barro y Paja">Barro y Paja</option>'+
'<option value="Cemento y Arena">Cemento y Arena</option>'+
'<option value="Yeso">Yeso</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoInt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
aca_intCodigo.push({arg_estadoAcaInt:'N',arg_integridadAcaInt:'N',arg_tipoAcaInt:'N',arg_materialAcaInt:'N'});
aca_intDibujo.push({scd_codigo:htmlAcaInt,id: 1});

cadena=aca_intDibujo[0].scd_codigo;
////console.log("cadenaaaa",cadena);
$('#divacaAcabadosMurosInteriores').html(cadena);
$("#estadoAcaInt1").val(aca_intCodigo[0].arg_estadoAcaInt);
$("#integridadAcaInt1").val(aca_intCodigo[0].arg_integridadAcaInt);
$("#tipoAcaInt1").val(aca_intCodigo[0].arg_tipoAcaInt);
$("#materialAcaInt1").val(aca_intCodigo[0].arg_materialAcaInt);


function acaAcabadosMurosInteriores(){
  var htmlAcaInt;
  htmlAcaInt=
  '<div class="col-md-4">'+
  '<select class="form-control" id="estadoAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3">'+
  '<select class="form-control" id="integridadAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2">'+
  '<select class="form-control" id="tipoAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2">'+
  '<select class="form-control" id="materialAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoInt('+contCodigo4+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  aca_intDibujo.forEach(function (item, index, array) 
  {
    aca_intCodigo[cont].arg_estadoAcaInt = $("#estadoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_integridadAcaInt = $("#integridadAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_tipoAcaInt = $("#tipoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_materialAcaInt = $("#materialAcaInt"+item.id+"").val();

    cont++;
  });
  aca_intCodigo.push({arg_estadoAcaInt:'N',arg_integridadAcaInt:'N',arg_tipoAcaInt:'N',arg_materialAcaInt:'N'});
  aca_intDibujo.push({scd_codigo:htmlAcaInt,id: contCodigo4});
  contCodigo4++;
  cont=1;
  for (var i=0; i<aca_intDibujo.length;i++)
  {
    cadena=cadena + aca_intDibujo[i].scd_codigo;
    cont++;
  }
  $('#divacaAcabadosMurosInteriores').html(cadena); 
  var conto=0;
  aca_intDibujo.forEach(function (item, index, array) 
  { 
    $("#estadoAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_estadoAcaInt);
    $("#integridadAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_integridadAcaInt);
    $("#tipoAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_tipoAcaInt);
    $("#materialAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_materialAcaInt);
    conto=conto+1;
  });
}

function menosLineaAcabadoInt($id){
  if(contCodigo4>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      aca_intDibujo.forEach(function (item, index, array) 
      {
        aca_intCodigo[cont].arg_estadoAcaInt=$("#estadoAcaInt"+item.id+"").val();
        aca_intCodigo[cont].arg_integridadAcaInt=$("#integridadAcaInt"+item.id+"").val();
        aca_intCodigo[cont].arg_tipoAcaInt=$("#tipoAcaInt"+item.id+"").val();
        aca_intCodigo[cont].arg_materialAcaInt=$("#materialAcaInt"+item.id+"").val();
        cont=cont+1;
      });
      pos = aca_intDibujo.map(function(e) {return e.id;}).indexOf($id);
      //console.log("poos",pos);
      aca_intCodigo.splice(pos,1);
      aca_intDibujo.splice(pos,1);
      cont=1;
      for (var i=0; i<aca_intDibujo.length;i++)
      {
        cadena=cadena + aca_intDibujo[i].scd_codigo;
        cont++;
      }

      $('#divacaAcabadosMurosInteriores').html(cadena);
      cont=0;
      aca_intDibujo.forEach(function (item, index, array) 
      {
        $("#estadoAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_estadoAcaInt);
        $("#integridadAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_integridadAcaInt);
        $("#tipoAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_tipoAcaInt);
        $("#materialAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_materialAcaInt);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
    });
  }
}
//--------------------------- Fin Grilla 6 --------------------------------------------------------------------
//-------------------------------begin grilla servicio--------------------//

    var contCodigoSer = 2;
    var arrCodigoSer = [];
    var arrDibujoSer = [];
  
    htmlServicio = 'Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descServicio1" name="tipoarg1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Agua">Agua</option>'+
              '<option value="Gas">Gas</option>'+
              '<option value="Recopilación">Recopilación</option>'+
              '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
              '<option value="Seguridad">Seguridad</option>'+
              '<option value="Otros Servicios">Otros Servicios</option>'+
          '</select>'+
      '</div>'+
      '<label class="col-md-2">Proveedor:</label>'+
      '<div class="col-md-3">'+
        '<select class="form-control" id="proveedorServicio1" name="tipoarg1">'+
            '<option value="N" selected="selected">--Seleccione--</option>'+
            '<option value="Red Publica">Red Publica</option>'+
            '<option value="Sistema Propio">Sistema Propio</option>'+
        '</select>'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaServicio(1);"></i>'+
      '</a></div></div>';
    var cadenaServicio = '';  
    arrCodigoSer.push({descServicio:'N',proveedorServicio:'N'});
    arrDibujoSer.push({scd_codigo:htmlServicio,id: 1});
    //////console.log("arrCodigo",arrCodigoUso);
    
    cadenaServicio='<div class="form-group"><label class="col-md-2">1. '+arrDibujoSer[0].scd_codigo;
    //////console.log("cadenaaaaUso",cadenaUso);
    $('#crearGrillaServicio').html(cadenaServicio);

    $("#descServicio1").val(arrCodigoSer[0].descServicio); 
    $("#proveedorServicio1").val(arrCodigoSer[0].proveedorServicio); 
   
function GrillaServicio(){
   
    var htmlServicio;
        htmlServicio = 'Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descServicio'+contCodigoSer+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Agua">Agua</option>'+
              '<option value="Gas">Gas</option>'+
              '<option value="Recopilación">Recopilación</option>'+
              '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
              '<option value="Seguridad">Seguridad</option>'+
              '<option value="Otros Servicios">Otros Servicios</option>'+
          '</select>'+
      '</div>'+
      '<label class="col-md-2">Proveedor:</label>'+
      '<div class="col-md-3">'+
        '<select class="form-control" id="proveedorServicio'+contCodigoSer+'">'+
            '<option value="N" selected="selected">--Seleccione--</option>'+
            '<option value="Red Publica">Red Publica</option>'+
            '<option value="Sistema Propio">Sistema Propio</option>'+
        '</select>'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaServicio('+contCodigoSer+');"></i>'+
      '</a></div></div>';
        var cadenaServicio='';
        var contU=0;
        arrDibujoSer.forEach(function (item, index, array) 
        {
          arrCodigoSer[contU].descServicio = $("#descServicio"+item.id+"").val();
          arrCodigoSer[contU].proveedorServicio = $("#proveedorServicio"+item.id+"").val();
          
          contU++;
        });

        arrCodigoSer.push({descServicio:'N',proveedorServicio:'N'});
        arrDibujoSer.push({scd_codigo:htmlServicio,id: contCodigoSer});

        contCodigoSer++;
        contU=1;
        for (var i=0; i<arrDibujoSer.length;i++)
        {
          cadenaServicio = cadenaServicio + '<br><div class="form-group"><label class="col-md-2">'+contU+'. '+arrDibujoSer[i].scd_codigo;
          contU++;
        }
        //////console.log("cadenaaaaUso",cadenaUso);
        $('#crearGrillaServicio').html(cadenaServicio);

        var contoU=0;
        arrDibujoSer.forEach(function (item, index, array) 
        { 
          
          $("#descServicio"+item.id+"").val(arrCodigoSer[contoU].descServicio); 
          $("#proveedorServicio"+item.id+"").val(arrCodigoSer[contoU].proveedorServicio); 
   
          contoU=contoU+1;
          
        });
        //////console.log("vectoooorr",arrCodigoUso);     
    }
    function eliminarGrillaServicio($id){
      
        if(contCodigoSer>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaServicio='';
                var pos=-1;
               
                var contU=0;
                arrDibujoSer.forEach(function (item, index, array) 
                {
                  arrCodigoSer[contU].descServicio = $("#descServicio"+item.id+"").val();
                  arrCodigoSer[contU].proveedorServicio = $("#proveedorServicio"+item.id+"").val();
                  
                  contU++;
                });
                            ////console.log("arrAntesPos",arrCodigoUso);
                pos = arrDibujoSer.map(function(e) {return e.id;}).indexOf($id);
                    ////console.log("poos",pos);
                arrCodigoSer.splice(pos,1);
                arrDibujoSer.splice(pos,1);
                    ////console.log("arrdespuesPos",arrCodigoUso);
                    
                contU=1;
                for (var i=0; i<arrDibujoSer.length;i++)
                {
                  cadenaServicio = cadenaServicio + '<br><div class="form-group"><label class="col-md-2">'+contU+'. '+arrDibujoSer[i].scd_codigo;
                  contU++;
                }

                $('#crearGrillaServicio').html(cadenaServicio);

                var contoU=0;
                arrDibujoSer.forEach(function (item, index, array) 
                { 
                  
                  $("#descServicio"+item.id+"").val(arrCodigoSer[contoU].descServicio); 
                  $("#proveedorServicio"+item.id+"").val(arrCodigoSer[contoU].proveedorServicio); 
           
                  contoU=contoU+1;
                  
                });

            });
        }
        //////console.log("arrEliminarFinal",arrCodigoUso);
    }
 
//------------------------end grilla servicio--------------------//
//--------------------GRILLA USO SUELO--------------------//

    var contCodigoUso = 2;
    var arrCodigoUso = [];
    var arrDibujoUso = [];
  
    htmlUso='Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descUso1" name="tipoarg1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Recreación activa">Recreación activa</option>'+
              '<option value="Parque Infantil">Parque Infantil</option>'+
              '<option value="Parques Urbanos">Parques Urbanos</option>'+
              '<option value="Recreación">Recreación</option>'+
              '<option value="Deportivo">Deportivo</option>'+
              '<option value="Industria">Industria</option>'+
              '<option value="Artistico Cultural">Artistico Cultural</option>'+
              '<option value="Recreación Pasiva">Recreación Pasiva</option>'+ 
          '</select>'+
      '</div>'+
      '<div class="col-md-3">'+
          'Tradicional <input type="checkbox" id="tradicionalUso1" name="tradicionalUso1">'+
      '</div>'+
      '<div class="col-md-3">'+
          'Actual <input type="checkbox" id="actuallUso1">'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaUso(1);"></i>'+
      '</a></div></div>';
    var cadenaUso = '';  
    arrCodigoUso.push({descripU:'N',tradicionalU:'',actualU:''});
    arrDibujoUso.push({scd_codigo:htmlUso,id: 1});
    //////console.log("arrCodigo",arrCodigoUso);
    
    cadenaUso='<div class="form-group"><label class="col-md-2">1. '+arrDibujoUso[0].scd_codigo;
    //////console.log("cadenaaaaUso",cadenaUso);
    $('#crearGrillaUso').html(cadenaUso);
    
    $("#descUso1").val(arrCodigoUso[0].descripU); 
    document.getElementById("tradicionalUso1").checked = (arrCodigoUso[0].tradicionalU); 
    document.getElementById("actuallUso1").checked = (arrCodigoUso[0].actualU);

    function GrillaUsoSuelo(){
   
    var htmlUso;
        htmlUso='Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descUso'+contCodigoUso+'" name="tipoarg'+contCodigoUso+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Recreación activa">Recreación activa</option>'+
              '<option value="Parque Infantil">Parque Infantil</option>'+
              '<option value="Parques Urbanos">Parques Urbanos</option>'+
              '<option value="Recreación">Recreación</option>'+
              '<option value="Deportivo">Deportivo</option>'+
              '<option value="Industria">Industria</option>'+
              '<option value="Artistico Cultural">Artistico Cultural</option>'+
              '<option value="Recreación Pasiva">Recreación Pasiva</option>'+
          '</select>'+
      '</div>'+
      '<div class="col-md-3">'+
          'Tradicional <input type="checkbox" id="tradicionalUso'+contCodigoUso+'">'+
      '</div>'+
      '<div class="col-md-3">'+
          'Actual <input type="checkbox" id="actuallUso'+contCodigoUso+'">'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaUso('+contCodigoUso+');"></i>'+
      '</a></div></div>';
        var cadenaUso='';
        var contU=0;
        arrDibujoUso.forEach(function (item, index, array) 
        {
          arrCodigoUso[contU].descripU = $("#descUso"+item.id+"").val();
          arrCodigoUso[contU].tradicionalU = document.getElementById("tradicionalUso"+item.id+"").checked;
          arrCodigoUso[contU].actualU = document.getElementById("actuallUso"+item.id+"").checked;
          
          contU++;
        });

        arrCodigoUso.push({descripU:'N',tradicionalU:'',actualU:''});
        arrDibujoUso.push({scd_codigo:htmlUso,id: contCodigoUso});

        contCodigoUso++;
        contU=1;
        for (var i=0; i<arrDibujoUso.length;i++)
        {
          cadenaUso = cadenaUso + '<br><div class="form-group"><label class="col-md-2">'+contU+'. '+arrDibujoUso[i].scd_codigo;
          contU++;
        }
        //////console.log("cadenaaaaUso",cadenaUso);
        $('#crearGrillaUso').html(cadenaUso); 
        
        var contoUso=0;
        arrDibujoUso.forEach(function (item, index, array) 
        { 
          
          $("#descUso"+item.id+"").val(arrCodigoUso[contoUso].descripU);
          document.getElementById("tradicionalUso"+item.id+"").checked = (arrCodigoUso[contoUso].tradicionalU); 
          document.getElementById("actuallUso"+item.id+"").checked = (arrCodigoUso[contoUso].actualU);
          contoUso=contoUso+1;
          
        });
        //////console.log("vectoooorr",arrCodigoUso);     
    }

    function eliminarGrillaUso($id){
      
        if(contCodigoUso>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaUso='';
                var cont=0;
                var pos=-1;
               
                arrDibujoUso.forEach(function (item, index, array) 
                {
                  arrCodigoUso[cont].descripU = $("#descUso"+item.id+"").val();
                  arrCodigoUso[cont].tradicionalU = document.getElementById("tradicionalUso"+item.id+"").checked;
                  arrCodigoUso[cont].actualU = document.getElementById("actuallUso"+item.id+"").checked;
                  //////console.log("contU",contU);
                  cont++;

                });
                ////console.log("arrAntesPos",arrCodigoUso);
                pos = arrDibujo.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigoUso.splice(pos,1);
                arrDibujoUso.splice(pos,1);
                ////console.log("arrdespuesPos",arrCodigoUso);
                
                cont=1;
                for (var i=0; i<arrDibujoUso.length;i++)
                {
                    cadenaUso=cadenaUso + '<br><div class="form-group"><label class="col-md-2">'+cont+'. '+arrDibujoUso[i].scd_codigo;
                    cont++;
                }
                
                $('#crearGrillaUso').html(cadenaUso);
                
                
               
                var contoUso=0;
                arrDibujoUso.forEach(function (item, index, array) 
                { 
                  
                  $("#descUso"+item.id+"").val(arrCodigoUso[contoUso].descripU);
                  document.getElementById("tradicionalUso"+item.id+"").checked = (arrCodigoUso[contoUso].tradicionalU); 
                  document.getElementById("actuallUso"+item.id+"").checked = (arrCodigoUso[contoUso].actualU);
                  contoUso=contoUso+1;
                  
                });

            });
        }
        //////console.log("arrEliminarFinal",arrCodigoUso);
    }
 

//---------------------------- FIN GRILLA USO SUELO----------------------//

//-----------------------BEGIN GRILLA REJAS-------------------- //
    var contCodigoRejas = 2;
    var arrCodigoRejas = [];
    var arrDibujoRejas = [];

     htmlRejas = '<div class="col-md-12">'+
          '<div class="form-group col-md-3">'+
            '<select id="descripcionRej1" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="1">Dibujos geo.</option>'+
              '<option value="2">Dibujos Vegetales.</option>'+
              '<option value="3">Concavo</option>'+
              '<option value="4">Antepechado</option>'+
            '</select> '+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="hierro1"> Hierro<br>'+
          '<input type="checkbox" id="tubo1"> Tubo<br>'+
          '<input type="checkbox" id="madera1"> Madera <br>'+
          '<input type="checkbox" id="otros1"> OTros'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="text" id="otrosDesc1" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-3">'+
            '<select id="integridRejas1" class="form-control">'+
              '<option value="" selected="selected">....Seleccionar...</option>'+
              '<option value="1">Original</option>'+
              '<option value="3">Modificado</option>'+
              '<option value="4">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
        '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaRejas(1);"></i>'+
      '</a></div>'+
      '</div>';

    var cadenaRejas = '';  
    arrCodigoRejas.push({descripcionR:'N',hierroR:'',tuboR:'',maderaR:'',otrosR:'',otrosDescR:'',integridadR:''});
    arrDibujoRejas.push({scd_codigo:htmlRejas,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaRejas=arrDibujoRejas[0].scd_codigo;
    ////console.log("cadenaRejas",cadenaRejas);
    $('#crearGrillaRejas').html(cadenaRejas);

    $("#descripcionRej1").val(arrCodigoRejas[0].descripcionR); 
    document.getElementById("hierro1").checked = (arrCodigoRejas[0].hierroR); 
    document.getElementById("tubo1").checked = (arrCodigoRejas[0].tuboR);
    document.getElementById("madera1").checked = (arrCodigoRejas[0].maderaR);
    document.getElementById("otros1").checked = (arrCodigoRejas[0].otrosR);
    $("#otrosDesc1").val(arrCodigoRejas[0].otrosDescR); 
    $("#integridRejas1").val(arrCodigoRejas[0].integridadR); 

   
    function grillaRejas(){
    var htmlRejas;
        htmlRejas = '<div class="col-md-12">'+
          '<div class="form-group col-md-3">'+
            '<select id="descripcionRej'+contCodigoRejas+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="1">Dibujos geo.</option>'+
              '<option value="2">Dibujos Vegetales.</option>'+
              '<option value="3">Concavo</option>'+
              '<option value="4">Antepechado</option>'+
            '</select> '+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="hierro'+contCodigoRejas+'"> Hierro<br>'+
          '<input type="checkbox" id="tubo'+contCodigoRejas+'"> Tubo<br>'+
          '<input type="checkbox" id="madera'+contCodigoRejas+'"> Madera <br>'+
          '<input type="checkbox" id="otros'+contCodigoRejas+'"> OTros'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="text" id="otrosDesc'+contCodigoRejas+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-3">'+
            '<select id="integridRejas'+contCodigoRejas+'" class="form-control">'+
              '<option value="" selected="selected">....Seleccionar...</option>'+
              '<option value="1">Original</option>'+
              '<option value="3">Modificado</option>'+
              '<option value="4">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
          '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaRejas('+contCodigoRejas+');"></i>'+
       '</a></div>'+
       '</div>';

        var cadenaRejas='';
        var cont=0;
        arrDibujoRejas.forEach(function (item, index, array) 
        {
          arrCodigoRejas[cont].descripcionR = $("#descripcionRej"+item.id+"").val();
          arrCodigoRejas[cont].hierroR = document.getElementById("hierro"+item.id+"").checked;
          arrCodigoRejas[cont].tuboR = document.getElementById("tubo"+item.id+"").checked;
          arrCodigoRejas[cont].maderaR = document.getElementById("madera"+item.id+"").checked;
          arrCodigoRejas[cont].otrosR = document.getElementById("otros"+item.id+"").checked;
          arrCodigoRejas[cont].otrosDescR = $("#otrosDesc"+item.id+"").val();
          arrCodigoRejas[cont].integridadR = $("#integridRejas"+item.id+"").val();
          cont++;
        });
     
        arrCodigoRejas.push({descripcionR:'N',hierroR:'',tuboR:'',maderaR:'',otrosR:'',otrosDescR:'',integridadR:''});
        arrDibujoRejas.push({scd_codigo:htmlRejas,id: contCodigoRejas});
        contCodigoRejas++;

        cont=1;
        for (var i=0; i<arrDibujoRejas.length;i++)
        {
          cadenaRejas = cadenaRejas + arrDibujoRejas[i].scd_codigo;
          cont++;
        }
      
        $('#crearGrillaRejas').html(cadenaRejas); 

        var conto=0;
        arrDibujoRejas.forEach(function (item, index, array) 
        { 
          $("#descripcionRej"+item.id+"").val(arrCodigoRejas[conto].descripcionR); 
          document.getElementById("hierro"+item.id+"").checked = (arrCodigoRejas[conto].hierroR); 
          document.getElementById("tubo"+item.id+"").checked = (arrCodigoRejas[conto].tuboR);
          document.getElementById("madera"+item.id+"").checked = (arrCodigoRejas[conto].maderaR);
          document.getElementById("otros"+item.id+"").checked = (arrCodigoRejas[conto].otrosR);
          $("#otrosDesc"+item.id+"").val(arrCodigoRejas[conto].otrosDescR); 
          $("#integridRejas"+item.id+"").val(arrCodigoRejas[conto].integridadR); 

          conto=conto+1;
        });

        ////console.log("arrCodigoRejas",arrCodigoRejas);
    } 

    function eliminarGrillaRejas($id){
        if(contCodigoRejas>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaRejas = '';
                var pos = -1;
                var cont = 0;

                arrDibujoRejas.forEach(function (item, index, array) 
                {
                  arrCodigoRejas[cont].descripcionR = $("#descripcionRej"+item.id+"").val();
                  arrCodigoRejas[cont].hierroR = document.getElementById("hierro"+item.id+"").checked;
                  arrCodigoRejas[cont].tuboR = document.getElementById("tubo"+item.id+"").checked;
                  arrCodigoRejas[cont].maderaR = document.getElementById("madera"+item.id+"").checked;
                  arrCodigoRejas[cont].otrosR = document.getElementById("otros"+item.id+"").checked;
                  arrCodigoRejas[cont].otrosDescR = $("#otrosDesc"+item.id+"").val();
                  arrCodigoRejas[cont].integridadR = $("#integridRejas"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoRejas.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoRejas.splice(pos,1);
                arrDibujoRejas.splice(pos,1);
                //console.log("EliminarRejas",arrCodigoRejas);
                
                cont=1;
                for (var i=0; i<arrDibujoRejas.length;i++)
                {
                  cadenaRejas = cadenaRejas + arrDibujoRejas[i].scd_codigo;
                  cont++;
                }
                
                $('#crearGrillaRejas').html(cadenaRejas);
                
                var conto=0;
                arrDibujoRejas.forEach(function (item, index, array) 
                { 
                  $("#descripcionRej"+item.id+"").val(arrCodigoRejas[conto].descripcionR); 
                  document.getElementById("hierro"+item.id+"").checked = (arrCodigoRejas[conto].hierroR); 
                  document.getElementById("tubo"+item.id+"").checked = (arrCodigoRejas[conto].tuboR);
                  document.getElementById("madera"+item.id+"").checked = (arrCodigoRejas[conto].maderaR);
                  document.getElementById("otros"+item.id+"").checked = (arrCodigoRejas[conto].otrosR);
                  $("#otrosDesc"+item.id+"").val(arrCodigoRejas[conto].otrosDescR); 
                  $("#integridRejas"+item.id+"").val(arrCodigoRejas[conto].integridadR); 

                  conto=conto+1;
                });
            });
        }
        
    }


//----------------------FIN GRILLA REJAS--------------------------------- //
//-----------------------BEGIN GRILLA ACABADOS DE MURO-------------------- //
    var contCodigoAcab = 2;
    var arrCodigoAcab = [];
    var arrDibujoAcab = [];


        htmlAcab='<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionAcab1" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="1">Revoque Grueso</option>'+
              '<option value="2">Revoque Fino</option>'+
              '<option value="3">Estructura Aparente</option>'+
              '<option value="4">Texturizado</option>'+
              '<option value="5">Enchape</option>'+
            '</select> '+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArena1"> Cal y Arena<br>'+
          '<input type="checkbox" id="cemento1"> Cemento y Arena<br>'+
          '<input type="checkbox" id="piedra1"> Piedra<br>'+
          '<input type="checkbox" id="marmol1"> Mármol'+
        '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="madera1"> Madera<br>'+
          '<input type="checkbox" id="azulejo1"> Azulejo<br>'+
          '<input type="checkbox" id="ladrillo1"> Ladrillo<br>'+
          '<input type="checkbox" id="otroAcab1"> Otros'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="text" id="otrosDescAcab1" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridAcab1" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="1">Original</option>'+
              '<option value="2">Nuevo</option>'+
              '<option value="3">Modificado</option>'+
              '<option value="4">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
       '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaAcabado(1);"></i>'+
       '</a></div>'+
        '</div>';

    var cadenaAcab = '';  
    arrCodigoAcab.push({descripcionA:'N',calArenaA:'',cementoA:'',piedraA:'',marmolA:'',maderaA:'',azulejoA:'',ladrilloA:'',otroAcabA:'',otroDescAcabA:'',integridadAcabA:''});
    arrDibujoAcab.push({scd_codigo:htmlAcab,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaAcab=arrDibujoAcab[0].scd_codigo;
    //console.log("cadenaAcab",cadenaAcab);
    $('#crearGrillaAcabado').html(cadenaAcab);

    $("#descripcionAcab1").val(arrCodigoAcab[0].descripcionA); 
    document.getElementById("calArena1").checked = (arrCodigoAcab[0].calArenaA); 
    document.getElementById("cemento1").checked = (arrCodigoAcab[0].cementoA);
    document.getElementById("piedra1").checked = (arrCodigoAcab[0].piedraA);
    document.getElementById("marmol1").checked = (arrCodigoAcab[0].marmolA);
    document.getElementById("madera1").checked = (arrCodigoAcab[0].maderaA); 
    document.getElementById("azulejo1").checked = (arrCodigoAcab[0].azulejoA);
    document.getElementById("ladrillo1").checked = (arrCodigoAcab[0].ladrilloA);
    document.getElementById("otroAcab1").checked = (arrCodigoAcab[0].otroAcabA);
    $("#otrosDescAcab1").val(arrCodigoAcab[0].otroDescAcabA); 
    $("#integridAcab1").val(arrCodigoAcab[0].integridadAcabA);

    function grillaAcabadoMuro(){

    var htmlAcab;
        htmlAcab = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionAcab'+contCodigoAcab+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="1">Revoque Grueso</option>'+
              '<option value="2">Revoque Fino</option>'+
              '<option value="3">Estructura Aparente</option>'+
              '<option value="4">Texturizado</option>'+
              '<option value="5">Enchape</option>'+
            '</select> '+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArena'+contCodigoAcab+'"> Cal y Arena<br>'+
          '<input type="checkbox" id="cemento'+contCodigoAcab+'"> Cemento y Arena<br>'+
          '<input type="checkbox" id="piedra'+contCodigoAcab+'"> Piedra<br>'+
          '<input type="checkbox" id="marmol'+contCodigoAcab+'"> Mármol'+
        '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="madera'+contCodigoAcab+'"> Madera<br>'+
          '<input type="checkbox" id="azulejo'+contCodigoAcab+'"> Azulejo<br>'+
          '<input type="checkbox" id="ladrillo'+contCodigoAcab+'"> Ladrillo<br>'+
          '<input type="checkbox" id="otroAcab'+contCodigoAcab+'"> Otros'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="text" id="otrosDescAcab'+contCodigoAcab+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridAcab'+contCodigoAcab+'" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="1">Original</option>'+
              '<option value="2">Nuevo</option>'+
              '<option value="3">Modificado</option>'+
              '<option value="4">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
       '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaAcabado('+contCodigoAcab+');"></i>'+
       '</a></div>'+
        '</div>';

        var cadenaAcab = '';
        var cont = 0;
        arrDibujoAcab.forEach(function (item, index, array) 
        {
          arrCodigoAcab[cont].descripcionA = $("#descripcionAcab"+item.id+"").val();
          arrCodigoAcab[cont].calArenaA = document.getElementById("calArena"+item.id+"").checked;
          arrCodigoAcab[cont].cementoA = document.getElementById("cemento"+item.id+"").checked;
          arrCodigoAcab[cont].piedraA = document.getElementById("piedra"+item.id+"").checked;
          arrCodigoAcab[cont].marmolA = document.getElementById("marmol"+item.id+"").checked;
          arrCodigoAcab[cont].maderaA = document.getElementById("madera"+item.id+"").checked;
          arrCodigoAcab[cont].azulejoA = document.getElementById("azulejo"+item.id+"").checked;
          arrCodigoAcab[cont].ladrilloA = document.getElementById("ladrillo"+item.id+"").checked;
          arrCodigoAcab[cont].otroAcabA = document.getElementById("otroAcab"+item.id+"").checked;
          arrCodigoAcab[cont].otroDescAcabA = $("#otrosDescAcab"+item.id+"").val();
          arrCodigoAcab[cont].integridadAcabA = $("#integridAcab"+item.id+"").val();
          cont++;
        });
     
        arrCodigoAcab.push({descripcionA:'N',calArenaA:'',cementoA:'',piedraA:'',marmolA:'',maderaA:'',azulejoA:'',ladrilloA:'',otroAcabA:'',otroDescAcabA:'',integridadAcabA:''});
        arrDibujoAcab.push({scd_codigo:htmlAcab, id: contCodigoAcab});
        contCodigoAcab++;

        cont=1;
        for (var i=0; i<arrDibujoAcab.length;i++)
        {
          cadenaAcab = cadenaAcab + arrDibujoAcab[i].scd_codigo;
          cont++;
        }
      
        $('#crearGrillaAcabado').html(cadenaAcab); 

        var conto=0;
        arrDibujoAcab.forEach(function (item, index, array) 
        { 
          $("#descripcionAcab"+item.id+"").val(arrCodigoAcab[conto].descripcionA); 
          document.getElementById("calArena"+item.id+"").checked = (arrCodigoAcab[conto].calArenaA); 
          document.getElementById("cemento"+item.id+"").checked = (arrCodigoAcab[conto].cementoA);
          document.getElementById("piedra"+item.id+"").checked = (arrCodigoAcab[conto].piedraA);
          document.getElementById("marmol"+item.id+"").checked = (arrCodigoAcab[conto].marmolA);
          document.getElementById("madera"+item.id+"").checked = (arrCodigoAcab[conto].maderaA); 
          document.getElementById("azulejo"+item.id+"").checked = (arrCodigoAcab[conto].azulejoA);
          document.getElementById("ladrillo"+item.id+"").checked = (arrCodigoAcab[conto].ladrilloA);
          document.getElementById("otroAcab"+item.id+"").checked = (arrCodigoAcab[conto].otroAcabA);
          $("#otrosDescAcab"+item.id+"").val(arrCodigoAcab[conto].otroDescAcabA); 
          $("#integridAcab"+item.id+"").val(arrCodigoAcab[conto].integridadAcabA); 

          conto=conto+1;
        });

        ////console.log("arrCodigoAcab",arrCodigoAcab);
    }

     function eliminarGrillaAcabado($id){
        if(contCodigoAcab>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaAcab='';
                var pos=-1;
                var cont = 0;
                arrDibujoAcab.forEach(function (item, index, array) 
                {
                  arrCodigoAcab[cont].descripcionA = $("#descripcionAcab"+item.id+"").val();
                  arrCodigoAcab[cont].calArenaA = document.getElementById("calArena"+item.id+"").checked;
                  arrCodigoAcab[cont].cementoA = document.getElementById("cemento"+item.id+"").checked;
                  arrCodigoAcab[cont].piedraA = document.getElementById("piedra"+item.id+"").checked;
                  arrCodigoAcab[cont].marmolA = document.getElementById("marmol"+item.id+"").checked;
                  arrCodigoAcab[cont].maderaA = document.getElementById("madera"+item.id+"").checked;
                  arrCodigoAcab[cont].azulejoA = document.getElementById("azulejo"+item.id+"").checked;
                  arrCodigoAcab[cont].ladrilloA = document.getElementById("ladrillo"+item.id+"").checked;
                  arrCodigoAcab[cont].otroAcabA = document.getElementById("otroAcab"+item.id+"").checked;
                  arrCodigoAcab[cont].otroDescAcabA = $("#otrosDescAcab"+item.id+"").val();
                  arrCodigoAcab[cont].integridadAcabA = $("#integridAcab"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoAcab.map(function(e) {return e.id;}).indexOf($id);
                arrCodigoAcab.splice(pos,1);
                arrDibujoAcab.splice(pos,1);
             
                  cont=1;
                for (var i=0; i<arrDibujoAcab.length;i++)
                {
                  cadenaAcab = cadenaAcab + arrDibujoAcab[i].scd_codigo;
                  cont++;
                }
                $('#crearGrillaAcabado').html(cadenaAcab);
                
                var conto=0;
                arrDibujoAcab.forEach(function (item, index, array) 
                { 
                  $("#descripcionAcab"+item.id+"").val(arrCodigoAcab[conto].descripcionA); 
                  document.getElementById("calArena"+item.id+"").checked = (arrCodigoAcab[conto].calArenaA); 
                  document.getElementById("cemento"+item.id+"").checked = (arrCodigoAcab[conto].cementoA);
                  document.getElementById("piedra"+item.id+"").checked = (arrCodigoAcab[conto].piedraA);
                  document.getElementById("marmol"+item.id+"").checked = (arrCodigoAcab[conto].marmolA);
                  document.getElementById("madera"+item.id+"").checked = (arrCodigoAcab[conto].maderaA); 
                  document.getElementById("azulejo"+item.id+"").checked = (arrCodigoAcab[conto].azulejoA);
                  document.getElementById("ladrillo"+item.id+"").checked = (arrCodigoAcab[conto].ladrilloA);
                  document.getElementById("otroAcab"+item.id+"").checked = (arrCodigoAcab[conto].otroAcabA);
                  $("#otrosDescAcab"+item.id+"").val(arrCodigoAcab[conto].otroDescAcabA); 
                  $("#integridAcab"+item.id+"").val(arrCodigoAcab[conto].integridadAcabA); 

                  conto=conto+1;
                });
            });
        }
   
    }

/////FIN GRILLA ACABADOS DE MURO////

/////BEGIN GRILLA PINTURAS////
    var contCodigoPinturas = 2;
    var arrCodigoPinturas = [];
    var arrDibujoPinturas = [];


        htmlPinturas='<div class="col-md-12">'+
          '<div class="form-group col-md-3">'+
            '<select id="descripcionPintu1" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Policromia">Policromia</option>'+
              '<option value="2">Bicromia</option>'+
              '<option value="3">Monocromia</option>'+
            '</select> '+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArenaPintu1"> Cal<br>'+
          '<input type="checkbox" id="acrilicoPintu1"> Acrilica<br>'+
          '<input type="checkbox" id="aceitePintu1"> Aceite<br>'+
          '<input type="checkbox" id="otroPintu1"> Otros'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="text" id="otrosDescPintu1" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-3">'+
            '<select id="integridPintu1" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="1">Original</option>'+
              '<option value="2">Nuevo</option>'+
              '<option value="3">Modificado</option>'+
              '<option value="4">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
        '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPintura(1);"></i>'+
      '</a></div>'+
        '</div>';

    var cadenaPinturas = '';  
    arrCodigoPinturas.push({descripcionPintu:'N',calArenaPintu:'',acrilicoPintu:'',aceitePintu:'',otroPintu:'',otrosDescPintu:'',integridPintu:''});
    arrDibujoPinturas.push({scd_codigo:htmlPinturas,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaPinturas=arrDibujoPinturas[0].scd_codigo;
   // //console.log("cadenaAcab",cadenaAcab);
    $('#crearGrillaPinturas').html(cadenaPinturas); 
    $("#descripcionPintu1").val(arrCodigoPinturas[0].descripcionPintu);
    document.getElementById("calArenaPintu1").checked = (arrCodigoPinturas[0].calArenaPintu); 
    document.getElementById("acrilicoPintu1").checked = (arrCodigoPinturas[0].acrilicoPintu); 
    document.getElementById("aceitePintu1").checked = (arrCodigoPinturas[0].aceitePintu);
    document.getElementById("otroPintu1").checked = (arrCodigoPinturas[0].otroPintu);
    $("#otrosDescPintu1").val(arrCodigoPinturas[0].otrosDescPintu); 
    $("#integridPintu1").val(arrCodigoPinturas[0].integridPintu);

    function grillaPinturas(){

    var htmlPinturas;
        htmlPinturas='<div class="col-md-12">'+
          '<div class="form-group col-md-3">'+
            '<select id="descripcionPintu'+contCodigoPinturas+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Policromia">Policromia</option>'+
              '<option value="2">Bicromia</option>'+
              '<option value="3">Monocromia</option>'+
            '</select> '+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArenaPintu'+contCodigoPinturas+'"> Cal<br>'+
          '<input type="checkbox" id="acrilicoPintu'+contCodigoPinturas+'"> Acrilica<br>'+
          '<input type="checkbox" id="aceitePintu'+contCodigoPinturas+'"> Aceite<br>'+
          '<input type="checkbox" id="otroPintu'+contCodigoPinturas+'"> Otros'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="text" id="otrosDescPintu'+contCodigoPinturas+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-3">'+
            '<select id="integridPintu'+contCodigoPinturas+'" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="1">Original</option>'+
              '<option value="2">Nuevo</option>'+
              '<option value="3">Modificado</option>'+
              '<option value="4">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
          '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPintura('+contCodigoPinturas+');"></i>'+
      '</a></div>'+
        '</div>';

        var cadenaPinturas = '';
        var cont = 0;
        arrDibujoPinturas.forEach(function (item, index, array) 
        {
          arrCodigoPinturas[cont].descripcionPintu = $("#descripcionPintu"+item.id+"").val();
          arrCodigoPinturas[cont].calArenaPintu = document.getElementById("calArenaPintu"+item.id+"").checked;
          arrCodigoPinturas[cont].acrilicoPintu = document.getElementById("acrilicoPintu"+item.id+"").checked;
          arrCodigoPinturas[cont].aceitePintu = document.getElementById("aceitePintu"+item.id+"").checked;
          arrCodigoPinturas[cont].otroPintu = document.getElementById("otroPintu"+item.id+"").checked;
          arrCodigoPinturas[cont].otrosDescPintu = $("#otrosDescPintu"+item.id+"").val();
          arrCodigoPinturas[cont].integridPintu = $("#integridPintu"+item.id+"").val();
          cont++;
        });
     
        arrCodigoPinturas.push({descripcionPintu:'N',calArenaPintu:'',acrilicoPintu:'',aceitePintu:'',otroPintu:'',otrosDescPintu:'',integridPintu:''});
        arrDibujoPinturas.push({scd_codigo:htmlPinturas, id: contCodigoPinturas});
        contCodigoPinturas++;

        cont=1;
        for (var i=0; i<arrDibujoPinturas.length;i++)
        {
          cadenaPinturas = cadenaPinturas + arrDibujoPinturas[i].scd_codigo;
          cont++;
        }
      
        $('#crearGrillaPinturas').html(cadenaPinturas); 

        var conto=0;
        arrDibujoPinturas.forEach(function (item, index, array) 
        { 
          $("#descripcionPintu"+item.id+"").val(arrCodigoPinturas[conto].descripcionPintu);
          document.getElementById("calArenaPintu"+item.id+"").checked = (arrCodigoPinturas[conto].calArenaPintu); 
          document.getElementById("acrilicoPintu"+item.id+"").checked = (arrCodigoPinturas[conto].acrilicoPintu); 
          document.getElementById("aceitePintu"+item.id+"").checked = (arrCodigoPinturas[conto].aceitePintu);
          document.getElementById("otroPintu"+item.id+"").checked = (arrCodigoPinturas[conto].otroPintu);
          $("#otrosDescPintu"+item.id+"").val(arrCodigoPinturas[conto].otrosDescPintu); 
          $("#integridPintu"+item.id+"").val(arrCodigoPinturas[conto].integridPintu);

          conto=conto+1;
        });

    }

     function eliminarGrillaPintura($id){
        if(contCodigoPinturas>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaPinturas='';
                var pos=-1;
                 var cont = 0;
                arrDibujoPinturas.forEach(function (item, index, array) 
                {
                  arrCodigoPinturas[cont].descripcionPintu = $("#descripcionPintu"+item.id+"").val();
                  arrCodigoPinturas[cont].calArenaPintu = document.getElementById("calArenaPintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].acrilicoPintu = document.getElementById("acrilicoPintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].aceitePintu = document.getElementById("aceitePintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].otroPintu = document.getElementById("otroPintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].otrosDescPintu = $("#otrosDescPintu"+item.id+"").val();
                  arrCodigoPinturas[cont].integridPintu = $("#integridPintu"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoPinturas.map(function(e) {return e.id;}).indexOf($id);
            
                arrCodigoPinturas.splice(pos,1);
                arrDibujoPinturas.splice(pos,1);  
               cont=1;
                for (var i=0; i<arrDibujoPinturas.length;i++)
                {
                  cadenaPinturas = cadenaPinturas + arrDibujoPinturas[i].scd_codigo;
                  cont++;
                }
              
                $('#crearGrillaPinturas').html(cadenaPinturas); 
                 var conto=0;
                arrDibujoPinturas.forEach(function (item, index, array) 
                { 
                  $("#descripcionPintu"+item.id+"").val(arrCodigoPinturas[conto].descripcionPintu);
                  document.getElementById("calArenaPintu"+item.id+"").checked = (arrCodigoPinturas[conto].calArenaPintu); 
                  document.getElementById("acrilicoPintu"+item.id+"").checked = (arrCodigoPinturas[conto].acrilicoPintu); 
                  document.getElementById("aceitePintu"+item.id+"").checked = (arrCodigoPinturas[conto].aceitePintu);
                  document.getElementById("otroPintu"+item.id+"").checked = (arrCodigoPinturas[conto].otroPintu);
                  $("#otrosDescPintu"+item.id+"").val(arrCodigoPinturas[conto].otrosDescPintu); 
                  $("#integridPintu"+item.id+"").val(arrCodigoPinturas[conto].integridPintu);

                  conto=conto+1;
                });
            });
        }
   
    }
    /////FIN GRILLA PINTURAS////





    /////////GRILLA JUAN/////////////////  LOS ULTIMAS 7 GRILLAS DE ESPACIOS ABIERTOS
/////////GRILLA JUAN/////////////////  LOS ULTIMAS 7 GRILLAS DE ESPACIOS ABIERTOS




    /////////GRILLA JUAN/////////////////  LOS ULTIMAS 7 GRILLAS DE ESPACIOS ABIERTOS
/////////GRILLA JUAN/////////////////  LOS ULTIMAS 7 GRILLAS DE ESPACIOS ABIERTOS

///--- inicio grilla bienes arqueologicos ubicacion geografica utm ---\\\
var contCodigoBnArqBienes = 2;
var arrCodigoBnArqBienes = [];
var arrDibujoBnArqBienes = [];
htmlBnArqBienes='<div class="col-md-12">'+
'<div class="form-group col-md-5">'+
'<input type="text" id="muneroDescBnArqBienes1" class="form-control"  placeholder="Numero">'+
'</div>'+
'<div class="form-group col-md-6">'+
'<input type="text" id="coordenadasColinealBnArqBienes1" class="form-control"  placeholder="Coordenadas">'+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaBnArqBienes(1);"></i>'+
'</a></div>'+
'</div>';
var cadenaBnArqBienes = '';  
arrCodigoBnArqBienes.push({NroDescBnArqBienes:'', PuntoColinealBnArqBienes: ''});
arrDibujoBnArqBienes.push({scd_codigo:htmlBnArqBienes, id: 1});
cadenaBnArqBienes=arrDibujoBnArqBienes[0].scd_codigo;
//console.log(arrDibujoBnArqBienes,htmlBnArqBienes);
$('#crearGrillaBnArqBienesado').html(cadenaBnArqBienes);
$("#muneroDescBnArqBienes1").val(arrCodigoBnArqBienes[0].NroDescBnArqBienes); 
$("#coordenadasColinealBnArqBienes1").val(arrCodigoBnArqBienes[0].PuntoColinealBnArqBienes);
function grillaBnArqBienes() {
  var htmlBnArqBienes;
  htmlBnArqBienes='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
'<input type="text" id="muneroDescBnArqBienes'+contCodigoBnArqBienes+'" class="form-control"  placeholder="Numero">'+
'</div>'+
'<div class="form-group col-md-6">'+
'<input type="text" id="coordenadasColinealBnArqBienes'+contCodigoBnArqBienes+'" class="form-control"  placeholder="Coordenadas">'+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaBnArqBienes('+contCodigoBnArqBienes+');"></i>'+
'</a></div>'+
'</div>';
  var cadenaBnArqBienes = '';
  var cont = 0;
  arrDibujoBnArqBienes.forEach(function (item, index, array){
    arrCodigoBnArqBienes[cont].NroDescBnArqBienes = $("#muneroDescBnArqBienes"+item.id+"").val();
    arrCodigoBnArqBienes[cont].PuntoColinealBnArqBienes = $("#coordenadasColinealBnArqBienes"+item.id+"").val();
    cont++;
  });
 arrCodigoBnArqBienes.push({NroDescBnArqBienes:'', PuntoColinealBnArqBienes: ''});
  arrDibujoBnArqBienes.push({scd_codigo:htmlBnArqBienes, id: contCodigoBnArqBienes});
  contCodigoBnArqBienes++;
  cont=1;
  for (var i=0; i<arrDibujoBnArqBienes.length;i++) {
    cadenaBnArqBienes = cadenaBnArqBienes + arrDibujoBnArqBienes[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaBnArqBienesado').html(cadenaBnArqBienes); 
  var conto=0;
  arrDibujoBnArqBienes.forEach(function (item, index, array) { 
    $("#muneroDescBnArqBienes"+item.id+"").val(arrCodigoBnArqBienes[conto].NroDescBnArqBienes); 
    $("#coordenadasColinealBnArqBienes"+item.id+"").val(arrCodigoBnArqBienes[conto].PuntoColinealBnArqBienes); 
    conto=conto+1;
  });
}

function eliminarGrillaBnArqBienes($id){
  if(contCodigoBnArqBienes>2) {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaBnArqBienes ='';
      var pos=-1;
      var cont = 0;
      arrDibujoBnArqBienes.forEach(function (item, index, array) {
     arrCodigoBnArqBienes[cont].NroDescBnArqBienes = $("#muneroDescBnArqBienes"+item.id+"").val();
      arrCodigoBnArqBienes[cont].PuntoColinealBnArqBienes = $("#coordenadasColinealBnArqBienes"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoBnArqBienes.map(function(e) {return e.id;}).indexOf($id);
      arrCodigoBnArqBienes.splice(pos,1);
      arrDibujoBnArqBienes.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoBnArqBienes.length;i++) {
        cadenaBnArqBienes = cadenaBnArqBienes + arrDibujoBnArqBienes[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaBnArqBienesado').html(cadenaBnArqBienes);
      var conto=0;
      arrDibujoBnArqBienes.forEach(function (item, index, array) { 
      $("#muneroDescBnArqBienes"+item.id+"").val(arrCodigoBnArqBienes[conto].NroDescBnArqBienes); 
    $("#coordenadasColinealBnArqBienes"+item.id+"").val(arrCodigoBnArqBienes[conto].PuntoColinealBnArqBienes); 
        conto=conto+1;
      });
    });
  }
}
///--- fin grilla  bienes arqueologicos ubicacion geografica utm ---\\\

//PTR_IA_UBIC
///--- inicio grilla  sitios arqueologicos ubicacion geografica utm ---\\\
var contCodigoBnArqSitios = 2;
var arrCodigoBnArqSitios = [];
var arrDibujoBnArqSitios = [];
htmlBnArqSitios='<div class="col-md-12">'+
'<div class="form-group col-md-5">'+
'<input type="text" id="muneroDescBnArqSitios1" class="form-control"  placeholder="Numero">'+
'</div>'+
'<div class="form-group col-md-6">'+
'<input type="text" id="coordenadasColinealBnArqSitios1" class="form-control"  placeholder="Coordenadas">'+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaBnArqSitios(1);"></i>'+
'</a></div>'+
'</div>';
var cadenaBnArqSitios = '';  
arrCodigoBnArqSitios.push({NroDescBnArqSitios:'', PuntoColinealBnArqSitios: ''});
arrDibujoBnArqSitios.push({scd_codigo:htmlBnArqSitios, id: 1});
cadenaBnArqSitios=arrDibujoBnArqSitios[0].scd_codigo;
//console.log(arrDibujoBnArqSitios,htmlBnArqSitios);
$('#crearGrillaBnArqSitiosado').html(cadenaBnArqSitios);
$("#muneroDescBnArqSitios1").val(arrCodigoBnArqSitios[0].NroDescBnArqSitios); 
$("#coordenadasColinealBnArqSitios1").val(arrCodigoBnArqSitios[0].PuntoColinealBnArqSitios);
function grillaBnArqSitios() {
  var htmlBnArqSitios;
  htmlBnArqSitios='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
'<input type="text" id="muneroDescBnArqSitios'+contCodigoBnArqSitios+'" class="form-control"  placeholder="Numero">'+
'</div>'+
'<div class="form-group col-md-6">'+
'<input type="text" id="coordenadasColinealBnArqSitios'+contCodigoBnArqSitios+'" class="form-control"  placeholder="Coordenadas">'+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaBnArqSitios('+contCodigoBnArqSitios+');"></i>'+
'</a></div>'+
'</div>';
  var cadenaBnArqSitios = '';
  var cont = 0;
  arrDibujoBnArqSitios.forEach(function (item, index, array){
    arrCodigoBnArqSitios[cont].NroDescBnArqSitios = $("#muneroDescBnArqSitios"+item.id+"").val();
    arrCodigoBnArqSitios[cont].PuntoColinealBnArqSitios = $("#coordenadasColinealBnArqSitios"+item.id+"").val();
    cont++;
  });
 arrCodigoBnArqSitios.push({NroDescBnArqSitios:'', PuntoColinealBnArqSitios: ''});
  arrDibujoBnArqSitios.push({scd_codigo:htmlBnArqSitios, id: contCodigoBnArqSitios});
  contCodigoBnArqSitios++;
  cont=1;
  for (var i=0; i<arrDibujoBnArqSitios.length;i++) {
    cadenaBnArqSitios = cadenaBnArqSitios + arrDibujoBnArqSitios[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaBnArqSitiosado').html(cadenaBnArqSitios); 
  var conto=0;
  arrDibujoBnArqSitios.forEach(function (item, index, array) { 
    $("#muneroDescBnArqSitios"+item.id+"").val(arrCodigoBnArqSitios[conto].NroDescBnArqSitios); 
    $("#coordenadasColinealBnArqSitios"+item.id+"").val(arrCodigoBnArqSitios[conto].PuntoColinealBnArqSitios); 
    conto=conto+1;
  });
}

function eliminarGrillaBnArqSitios($id){
  if(contCodigoBnArqSitios>2) {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaBnArqSitios ='';
      var pos=-1;
      var cont = 0;
      arrDibujoBnArqSitios.forEach(function (item, index, array) {
     arrCodigoBnArqSitios[cont].NroDescBnArqSitios = $("#muneroDescBnArqSitios"+item.id+"").val();
      arrCodigoBnArqSitios[cont].PuntoColinealBnArqSitios = $("#coordenadasColinealBnArqSitios"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoBnArqSitios.map(function(e) {return e.id;}).indexOf($id);
      arrCodigoBnArqSitios.splice(pos,1);
      arrDibujoBnArqSitios.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoBnArqSitios.length;i++) {
        cadenaBnArqSitios = cadenaBnArqSitios + arrDibujoBnArqSitios[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaBnArqSitiosado').html(cadenaBnArqSitios);
      var conto=0;
      arrDibujoBnArqSitios.forEach(function (item, index, array) { 
      $("#muneroDescBnArqSitios"+item.id+"").val(arrCodigoBnArqSitios[conto].NroDescBnArqSitios); 
    $("#coordenadasColinealBnArqSitios"+item.id+"").val(arrCodigoBnArqSitios[conto].PuntoColinealBnArqSitios); 
        conto=conto+1;
      });
    });
  }
}
///--- fin grilla  sitios arqueologicos ubicacion geografica utm ---\\\


///--- inicio grilla  Puntos Colineales ---\\\
var contCodigoPuntosColineales = 2;
var arrCodigoPuntosColineales = [];
var arrDibujoPuntosColineales = [];

htmlPuntosColineales='<div class="col-md-12">'+
                    '<div class="form-group col-md-5">'+
                    '<input type="text" id="NroDescPuntosColineales1" class="form-control"  placeholder="Nro">'+
                    '</div>'+
                    '<div class="form-group col-md-6">'+
                    '<input type="text" id="PuntoColinealPuntosColineales1" class="form-control"  placeholder="Punto Colineal">'+
                    '</div>'+
                    '<div class="col-md-1">'+
                    '<a style="cursor:pointer;" type="button">'+
                    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuntosColineales(1);"></i>'+
                    '</a></div>'+
                    '</div>';

var cadenaPuntosColineales = '';  
arrCodigoPuntosColineales.push({NroDescPuntosColineales:'', PuntoColinealPuntosColineales: ''});
arrDibujoPuntosColineales.push({scd_codigo:htmlPuntosColineales, id: 1});
cadenaPuntosColineales=arrDibujoPuntosColineales[0].scd_codigo;

$('#crearGrillaPuntosColinealesado').html(cadenaPuntosColineales);
$("#NroDescPuntosColineales1").val(arrCodigoPuntosColineales[0].NroDescPuntosColineales); 
$("#PuntoColinealPuntosColineales1").val(arrCodigoPuntosColineales[0].PuntoColinealPuntosColineales);

function grillaPuntosColineales() {
  var htmlPuntosColineales;
  htmlPuntosColineales='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
    '<input type="text" id="NroDescPuntosColineales'+contCodigoPuntosColineales+'" class="form-control"  placeholder="Nro">'+
    '</div>'+
    '<div class="form-group col-md-6">'+
    '<input type="text" id="PuntoColinealPuntosColineales'+contCodigoPuntosColineales+'" class="form-control"  placeholder="Punto Colineal">'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuntosColineales('+contCodigoPuntosColineales+');"></i>'+
    '</a></div>'+
    '</div>';
  var cadenaPuntosColineales = '';
  var cont = 0;
  arrDibujoPuntosColineales.forEach(function (item, index, array){
    arrCodigoPuntosColineales[cont].NroDescPuntosColineales = $("#NroDescPuntosColineales"+item.id+"").val();
    arrCodigoPuntosColineales[cont].PuntoColinealPuntosColineales = $("#PuntoColinealPuntosColineales"+item.id+"").val();
    cont++;
  });
 arrCodigoPuntosColineales.push({NroDescPuntosColineales:'', PuntoColinealPuntosColineales: ''});
  arrDibujoPuntosColineales.push({scd_codigo:htmlPuntosColineales, id: contCodigoPuntosColineales});
  contCodigoPuntosColineales++;
  cont=1;
  for (var i=0; i<arrDibujoPuntosColineales.length;i++) {
    cadenaPuntosColineales = cadenaPuntosColineales + arrDibujoPuntosColineales[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaPuntosColinealesado').html(cadenaPuntosColineales); 
  var conto=0;
  arrDibujoPuntosColineales.forEach(function (item, index, array) { 
    $("#NroDescPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].NroDescPuntosColineales); 
    $("#PuntoColinealPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].PuntoColinealPuntosColineales); 
    conto=conto+1;
  });
}

function eliminarGrillaPuntosColineales($id){
  if(contCodigoPuntosColineales>2) {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaPuntosColineales ='';
      var pos=-1;
      var cont = 0;
      arrDibujoPuntosColineales.forEach(function (item, index, array) {
     arrCodigoPuntosColineales[cont].NroDescPuntosColineales = $("#NroDescPuntosColineales"+item.id+"").val();
      arrCodigoPuntosColineales[cont].PuntoColinealPuntosColineales = $("#PuntoColinealPuntosColineales"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoPuntosColineales.map(function(e) {return e.id;}).indexOf($id);
      arrCodigoPuntosColineales.splice(pos,1);
      arrDibujoPuntosColineales.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoPuntosColineales.length;i++) {
        cadenaPuntosColineales = cadenaPuntosColineales + arrDibujoPuntosColineales[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaPuntosColinealesado').html(cadenaPuntosColineales);
      var conto=0;
      arrDibujoPuntosColineales.forEach(function (item, index, array) { 
      $("#NroDescPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].NroDescPuntosColineales); 
    $("#PuntoColinealPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].PuntoColinealPuntosColineales); 
        conto=conto+1;
      });
    });
  }
}
///--- fin grilla  Puntos Colineales ---\\\

//--------------------GRILLA PISO INICIO--------------------//

var contCodigoPiso = 2;
var arrCodigoPiso = [];
var arrDibujoPiso = [];
htmlPiso='<div class="col-md-12">'+
'<div class="form-group col-md-2">'+
'<select id="descripcionPiso1" class="form-control">'+
'<option value="N" selected="selected">..Seleccionar..</option>'+
'<option value="1">Interior</option>'+
'<option value="2">Aceras</option>'+
'</select> '+
'</div>'+
'<div class="form-group col-md-2">'+
'<input type="checkbox" id="baldosasPiso1">Baldosas<br>'+
'<input type="checkbox" id="mosaicosPiso1">Mosaicos<br>'+
'<input type="checkbox" id="porcelanatoPiso1"> Porcelanato<br>'+
'<input type="checkbox" id="plajaPiso1"> P.Laja<br>'+
'<input type="checkbox" id="prioPiso1"> P.Rio<br>'+
'<input type="checkbox" id="ladrillosPiso1"> Ladrillos<br>'+
'</div>'+
'<div class="form-group col-md-2">'+
'<input type="checkbox" id="gramaPiso1"> Grama<br>'+
'<input type="checkbox" id="tierraPiso1"> Tierra<br>'+
'<input type="checkbox" id="concretoPiso1"> Concreto<br>'+
'<input type="checkbox" id="ceramicaPiso1"> Ceramica<br>'+
'<input type="checkbox" id="maderaPiso1"> Madera<br>'+
'<input type="checkbox" id="otrosPiso1"> Otros'+
'</div>'+
'<div class="form-group col-md-3">'+
'<input type="text" id="otrosDescPiso1" class="form-control"  placeholder="Otras Descrip.">'+
'</div>'+
'<div class="form-group col-md-2">'+
'<select id="integriPiso1" class="form-control">'+
'<option value="N" selected="selected">..Seleccionar..</option>'+
'<option value="1">Original</option>'+
'<option value="2">Nuevo</option>'+
'<option value="3">Modificado</option>'+
'<option value="4">Original y Nuevo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPiso(1);"></i>'+
'</a></div>'+
'</div>';

var cadenaPiso = '';  
arrCodigoPiso.push({descripcionPisoA:'N', baldosasPisoA: '', mosaicosPisoA: '', porcelanatoPisoA: '', plajaPisoA: '', prioPisoA: '', ladrillosPisoA: '', gramaPisoA: '', tierraPisoA: '', concretoPisoA: '', ceramicaPisoA: '', maderaPisoA: '',otrosPisoA: '', otroDescPisoA:'', integridadPisoA:''});
arrDibujoPiso.push({scd_codigo:htmlPiso,id: 1});

cadenaPiso=arrDibujoPiso[0].scd_codigo;
//console.log("cadenaPiso",cadenaPiso);
$('#crearGrillaPisoado').html(cadenaPiso);
$("#descripcionPiso1").val(arrCodigoPiso[0].descripcionPisoA); 
document.getElementById("baldosasPiso1").checked = (arrCodigoPiso[0].baldosasPisoA); 
document.getElementById("mosaicosPiso1").checked = (arrCodigoPiso[0].mosaicosPisoA);
document.getElementById("porcelanatoPiso1").checked = (arrCodigoPiso[0].porcelanatoPisoA);
document.getElementById("plajaPiso1").checked = (arrCodigoPiso[0].plajaPisoA);
document.getElementById("prioPiso1").checked = (arrCodigoPiso[0].prioPisoA); 
document.getElementById("ladrillosPiso1").checked = (arrCodigoPiso[0].ladrillosPisoA);
document.getElementById("gramaPiso1").checked = (arrCodigoPiso[0].gramaPisoA);
document.getElementById("tierraPiso1").checked = (arrCodigoPiso[0].tierraPisoA);
document.getElementById("concretoPiso1").checked = (arrCodigoPiso[0].concretoPisoA); 
document.getElementById("ceramicaPiso1").checked = (arrCodigoPiso[0].ceramicaPisoA);
document.getElementById("maderaPiso1").checked = (arrCodigoPiso[0].maderaPisoA);
document.getElementById("otrosPiso1").checked = (arrCodigoPiso[0].otrosPisoA);
$("#otrosDescPiso1").val(arrCodigoPiso[0].otroDescPisoA); 
$("#integridPiso1").val(arrCodigoPiso[0].integridadPisoA);

function grillaPiso(){
  var htmlPiso;
  htmlPiso='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionPiso'+contCodigoPiso+'" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="1">Interior</option>'+
  '<option value="2">Aceras</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="checkbox" id="baldosasPiso'+contCodigoPiso+'">Baldosas<br>'+
  '<input type="checkbox" id="mosaicosPiso'+contCodigoPiso+'">Mosaicos<br>'+
  '<input type="checkbox" id="porcelanatoPiso'+contCodigoPiso+'"> Porcelanato<br>'+
  '<input type="checkbox" id="plajaPiso'+contCodigoPiso+'"> P.Laja<br>'+
  '<input type="checkbox" id="prioPiso'+contCodigoPiso+'"> P.Rio<br>'+
  '<input type="checkbox" id="ladrillosPiso'+contCodigoPiso+'"> Ladrillos<br>'+
  '</div>'+
  '<div class="form-group col-md-2">'+

  '<input type="checkbox" id="gramaPiso'+contCodigoPiso+'"> Grama<br>'+
  '<input type="checkbox" id="tierraPiso'+contCodigoPiso+'"> Tierra<br>'+
  '<input type="checkbox" id="concretoPiso'+contCodigoPiso+'"> Concreto<br>'+
  '<input type="checkbox" id="ceramicaPiso'+contCodigoPiso+'"> Ceramica<br>'+
  '<input type="checkbox" id="maderaPiso'+contCodigoPiso+'"> Madera<br>'+
  '<input type="checkbox" id="otrosPiso'+contCodigoPiso+'"> Otros'+
  '</div>'+
  '<div class="form-group col-md-3">'+
  '<input type="text" id="otrosDescPiso'+contCodigoPiso+'" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridPiso'+contCodigoPiso+'" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="1">Original</option>'+
  '<option value="2">Nuevo</option>'+
  '<option value="3">Modificado</option>'+
  '<option value="4">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPiso('+contCodigoPiso+');"></i>'+
  '</a></div>'+
  '</div>';
  var cadenaPiso = '';
  var cont = 0;
  arrDibujoPiso.forEach(function (item, index, array){
    arrCodigoPiso[cont].descripcionPisoA = $("#descripcionPiso"+item.id+"").val();     
    arrCodigoPiso[cont].baldosasPisoA = document.getElementById("baldosasPiso"+item.id+"").checked; 
    arrCodigoPiso[cont].mosaicosPisoA = document.getElementById("mosaicosPiso"+item.id+"").checked;
    arrCodigoPiso[cont].porcelanatoPisoA = document.getElementById("porcelanatoPiso"+item.id+"").checked;
    arrCodigoPiso[cont].plajaPisoA = document.getElementById("plajaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].prioPisoA = document.getElementById("prioPiso"+item.id+"").checked; 
    arrCodigoPiso[cont].ladrillosPisoA = document.getElementById("ladrillosPiso"+item.id+"").checked;
    arrCodigoPiso[cont].gramaPisoA = document.getElementById("gramaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].tierraPisoA = document.getElementById("tierraPiso"+item.id+"").checked;
    arrCodigoPiso[cont].concretoPisoA = document.getElementById("concretoPiso"+item.id+"").checked; 
    arrCodigoPiso[cont].ceramicaPisoA = document.getElementById("ceramicaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].maderaPisoA = document.getElementById("maderaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].otrosPisoA = document.getElementById("otrosPiso"+item.id+"").checked;
    arrCodigoPiso[cont].otroDescPisoA = $("#otrosDescPiso"+item.id+"").val();
    arrCodigoPiso[cont].integridadPisoA = $("#integridPiso"+item.id+"").val();
    cont++;
  });
  arrCodigoPiso.push({descripcionPisoA:'N', baldosasPisoA: '', mosaicosPisoA: '', porcelanatoPisoA: '', plajaPisoA: '', prioPisoA: '', ladrillosPisoA: '', gramaPisoA: '', tierraPisoA: '', concretoPisoA: '', ceramicaPisoA: '', maderaPisoA: '',otrosPisoA: '', otroDescPisoA:'', integridadPisoA:''});
  arrDibujoPiso.push({scd_codigo:htmlPiso, id: contCodigoPiso});
  contCodigoPiso++;
  cont=1;
  for (var i=0; i<arrDibujoPiso.length;i++){
    cadenaPiso = cadenaPiso + arrDibujoPiso[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaPisoado').html(cadenaPiso); 
  var conto=0;
  arrDibujoPiso.forEach(function (item, index, array) { 
    $("#descripcionPiso"+item.id+"").val(arrCodigoPiso[conto].descripcionPisoA); 
    document.getElementById("baldosasPiso"+item.id+"").checked = (arrCodigoPiso[conto].baldosasPisoA); 
    document.getElementById("mosaicosPiso"+item.id+"").checked = (arrCodigoPiso[conto].mosaicosPisoA);
    document.getElementById("porcelanatoPiso"+item.id+"").checked = (arrCodigoPiso[conto].porcelanatoPisoA);
    document.getElementById("plajaPiso"+item.id+"").checked = (arrCodigoPiso[conto].plajaPisoA);
    document.getElementById("prioPiso"+item.id+"").checked = (arrCodigoPiso[conto].prioPisoA); 
    document.getElementById("ladrillosPiso"+item.id+"").checked = (arrCodigoPiso[conto].ladrillosPisoA);
    document.getElementById("gramaPiso"+item.id+"").checked = (arrCodigoPiso[conto].gramaPisoA);
    document.getElementById("tierraPiso"+item.id+"").checked = (arrCodigoPiso[conto].tierraPisoA);
    document.getElementById("concretoPiso"+item.id+"").checked = (arrCodigoPiso[conto].concretoPisoA); 
    document.getElementById("ceramicaPiso"+item.id+"").checked = (arrCodigoPiso[conto].ceramicaPisoA);
    document.getElementById("maderaPiso"+item.id+"").checked = (arrCodigoPiso[conto].maderaPisoA);
    document.getElementById("otrosPiso"+item.id+"").checked = (arrCodigoPiso[conto].otrosPisoA);
    $("#otrosDescPiso"+item.id+"").val(arrCodigoPiso[conto].otroDescPisoA); 
    $("#integridPiso"+item.id+"").val(arrCodigoPiso[conto].integridadPisoA); 
    conto=conto+1;
  });
}

function eliminarGrillaPiso($id){
  if(contCodigoPiso>2){
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaPiso ='';
      var pos=-1;
      var cont = 0;
      arrDibujoPiso.forEach(function (item, index, array) {
        arrCodigoPiso[cont].descripcionPisoA = $("#descripcionPiso"+item.id+"").val();     
        arrCodigoPiso[cont].baldosasPisoA = document.getElementById("baldosasPiso"+item.id+"").checked; 
        arrCodigoPiso[cont].mosaicosPisoA = document.getElementById("mosaicosPiso"+item.id+"").checked;
        arrCodigoPiso[cont].porcelanatoPisoA = document.getElementById("porcelanatoPiso"+item.id+"").checked;
        arrCodigoPiso[cont].plajaPisoA = document.getElementById("plajaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].prioPisoA = document.getElementById("prioPiso"+item.id+"").checked; 
        arrCodigoPiso[cont].ladrillosPisoA = document.getElementById("ladrillosPiso"+item.id+"").checked;
        arrCodigoPiso[cont].gramaPisoA = document.getElementById("gramaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].tierraPisoA = document.getElementById("tierraPiso"+item.id+"").checked;
        arrCodigoPiso[cont].concretoPisoA = document.getElementById("concretoPiso"+item.id+"").checked; 
        arrCodigoPiso[cont].ceramicaPisoA = document.getElementById("ceramicaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].maderaPisoA = document.getElementById("maderaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].otrosPisoA = document.getElementById("otrosPiso"+item.id+"").checked;
        arrCodigoPiso[cont].otroDescPisoA = $("#otrosDescPiso"+item.id+"").val();
        arrCodigoPiso[cont].integridadPisoA = $("#integridPiso"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoPiso.map(function(e) {return e.id;}).indexOf($id);
      //console.log("poos",pos);
      arrCodigoPiso.splice(pos,1);
      arrDibujoPiso.splice(pos,1);
      //console.log("arrCodigo",arrCodigoPiso);
      cont=1;
      for (var i=0; i<arrDibujoPiso.length;i++) {
        cadenaPiso = cadenaPiso + arrDibujoPiso[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaPisoado').html(cadenaPiso);
      var conto=0;
      arrDibujoPiso.forEach(function (item, index, array) { 
        $("#descripcionPiso"+item.id+"").val(arrCodigoPiso[conto].descripcionPisoA); 
        document.getElementById("baldosasPiso"+item.id+"").checked = (arrCodigoPiso[conto].baldosasPisoA); 
        document.getElementById("mosaicosPiso"+item.id+"").checked = (arrCodigoPiso[conto].mosaicosPisoA);
        document.getElementById("porcelanatoPiso"+item.id+"").checked = (arrCodigoPiso[conto].porcelanatoPisoA);
        document.getElementById("plajaPiso"+item.id+"").checked = (arrCodigoPiso[conto].plajaPisoA);
        document.getElementById("prioPiso"+item.id+"").checked = (arrCodigoPiso[conto].prioPisoA); 
        document.getElementById("ladrillosPiso"+item.id+"").checked = (arrCodigoPiso[conto].ladrillosPisoA);
        document.getElementById("gramaPiso"+item.id+"").checked = (arrCodigoPiso[conto].gramaPisoA);
        document.getElementById("tierraPiso"+item.id+"").checked = (arrCodigoPiso[conto].tierraPisoA);
        document.getElementById("concretoPiso"+item.id+"").checked = (arrCodigoPiso[conto].concretoPisoA); 
        document.getElementById("ceramicaPiso"+item.id+"").checked = (arrCodigoPiso[conto].ceramicaPisoA);
        document.getElementById("maderaPiso"+item.id+"").checked = (arrCodigoPiso[conto].maderaPisoA);
        document.getElementById("otrosPiso"+item.id+"").checked = (arrCodigoPiso[conto].otrosPisoA);
        $("#otrosDescPiso"+item.id+"").val(arrCodigoPiso[conto].otroDescPisoA); 
        $("#integridPiso"+item.id+"").val(arrCodigoPiso[conto].integridadPisoA); 
        conto=conto+1;
      });
    });
  }
}
//--------------------GRILLA PISO FIN-------------------//
//--------------------GRILLA puertas de aceso INICIO--------------------//

var contCodigoPuertasAcceso = 2;
var arrCodigoPuertasAcceso= [];
var arrDibujoPuertasAcceso = [];
htmlPuertasAcceso='<div class="col-md-12">'+
'<div class="form-group col-md-2">'+
'<select id="descripcionPuertasAcceso1" class="form-control">'+
'<option value="N" selected="selected">..Seleccionar..</option>'+
'<option value="1">Arco</option>'+
'<option value="2">Recto</option>'+
'<option value="3">Recto con Esquinas</option>'+
'<option value="4">Texturizado</option>'+
'<option value="5">Enchape</option>'+
'</select> '+
'</div>'+
'<div class="form-group col-md-4">'+
'<input type="checkbox" id="maderaPuertasAcceso1">Madera<br>'+
'<input type="checkbox" id="metalPuertasAcceso1"> Metal<br>'+
'<input type="checkbox" id="otrosPuertasAcceso1"> Otros<br>'+
'</div>'+
'<div class="form-group col-md-3">'+
'<input type="text" id="otrosDescPuertasAcceso1" class="form-control"  placeholder="Otras Descrip.">'+
'</div>'+
'<div class="form-group col-md-2">'+
'<select id="integridPuertasAcceso1" class="form-control">'+
'<option value="-1" selected="selected">..hhhhh.</option>'+
'<option value="1">Original</option>'+
'<option value="2">Nuevo</option>'+
'<option value="3">Modificado</option>'+
'<option value="4">Original y Nuevo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuertasAcceso(1);"></i>'+
'</a></div>'+
'</div>';

var cadenaPuertasAcceso = '';  
arrCodigoPuertasAcceso.push({descripcionPuertasAcceso:'N', maderaPuertaAcceso: '', metalPuertaAcceso: '', otrosPuertaAcceso: '', otroDescPuertasAccesoA:'', integridadPuertasAccesoA:''});
arrDibujoPuertasAcceso.push({scd_codigo:htmlPuertasAcceso,id: 1});
cadenaPuertasAcceso=arrDibujoPuertasAcceso[0].scd_codigo;
//console.log("cadenaPuertasAcceso",cadenaPuertasAcceso);
$('#crearGrillaPuertasAcceso').html(cadenaPuertasAcceso);

$("#descripcionPuertasAcceso1").val(arrCodigoPuertasAcceso[0].descripcionPuertasAcceso); 
document.getElementById("maderaPuertasAcceso1").checked = (arrCodigoPuertasAcceso[0].maderaPuertaAcceso); 
document.getElementById("metalPuertasAcceso1").checked = (arrCodigoPuertasAcceso[0].metalPuertaAcceso);
document.getElementById("otrosPuertasAcceso1").checked = (arrCodigoPuertasAcceso[0].otrosPuertaAcceso);
$("#otrosDescPuertasAcceso1").val(arrCodigoPuertasAcceso[0].otroDescPuertasAccesoA); 
$("#integridPuertasAcceso1").val(arrCodigoPuertasAcceso[0].integridadPuertasAccesoA);

function grillaPuertasAcceso(){
  var htmlPuertasAcceso;
  htmlPuertasAcceso='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionPuertasAcceso'+contCodigoPuertasAcceso+'" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="1">Arco</option>'+
  '<option value="2">Recto</option>'+
  '<option value="3">Recto con Esquinas</option>'+
  '<option value="4">Texturizado</option>'+
  '<option value="5">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-4">'+
  '<input type="checkbox" id="maderaPuertasAcceso'+contCodigoPuertasAcceso+'">Madera<br>'+
  '<input type="checkbox" id="metalPuertasAcceso'+contCodigoPuertasAcceso+'"> Metal<br>'+
  '<input type="checkbox" id="otrosPuertasAcceso'+contCodigoPuertasAcceso+'"> Otros<br>'+
  '</div>'+
  '<div class="form-group col-md-3">'+
  '<input type="text" id="otrosDescPuertasAcceso'+contCodigoPuertasAcceso+'" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridPuertasAcceso'+contCodigoPuertasAcceso+'" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="1">Original</option>'+
  '<option value="2">Nuevo</option>'+
  '<option value="3">Modificado</option>'+
  '<option value="4">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuertasAcceso('+contCodigoPuertasAcceso+');"></i>'+
  '</a></div>'+
  '</div>';
  var cadenaPuertasAcceso = '';
  var cont = 0;
  arrDibujoPuertasAcceso.forEach(function (item, index, array) 
  {
    arrCodigoPuertasAcceso[cont].descripcionPuertasAcceso = $("#descripcionPuertasAcceso"+item.id+"").val();     
    arrCodigoPuertasAcceso[cont].maderaPuertaAcceso = document.getElementById("maderaPuertasAcceso"+item.id+"").checked; 
    arrCodigoPuertasAcceso[cont].metalPuertaAcceso = document.getElementById("metalPuertasAcceso"+item.id+"").checked;
    arrCodigoPuertasAcceso[cont].otrosPuertaAcceso = document.getElementById("otrosPuertasAcceso"+item.id+"").checked;
    arrCodigoPuertasAcceso[cont].otroDescPuertasAccesoA = $("#otrosDescPuertasAcceso"+item.id+"").val();
    arrCodigoPuertasAcceso[cont].integridadPuertasAccesoA = $("#integridPuertasAcceso"+item.id+"").val();
    cont++;
  });

  arrCodigoPuertasAcceso.push({descripcionPuertasAcceso:'N', maderaPuertaAcceso: '', metalPuertaAcceso: '', otrosPuertaAcceso: '', otroDescPuertasAccesoA:'', integridadPuertasAccesoA:''});
  arrDibujoPuertasAcceso.push({scd_codigo:htmlPuertasAcceso, id: contCodigoPuertasAcceso});
  contCodigoPuertasAcceso++;

  cont=1;
  for (var i=0; i<arrDibujoPuertasAcceso.length;i++)
  {
    cadenaPuertasAcceso = cadenaPuertasAcceso + arrDibujoPuertasAcceso[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaPuertasAcceso').html(cadenaPuertasAcceso); 
  var conto=0;
  arrDibujoPuertasAcceso.forEach(function (item, index, array) 
  { 
    $("#descripcionPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].descripcionA); 
    document.getElementById("maderaPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].maderaPuertaAcceso); 
    document.getElementById("metalPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].metalPuertaAcceso);
    document.getElementById("otrosPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].otrosPuertaAcceso);
    $("#otrosDescPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].otroDescPuertasAccesoA); 
    $("#integridPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].integridadPuertasAccesoA); 
    conto=conto+1;
  });

        ////console.log("arrCodigoPuertasAcceso",arrCodigoPuertasAcceso);
      }


      function eliminarGrillaPuertasAcceso($id){
        if(contCodigoPuertasAcceso>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaPuertasAcceso ='';
            var pos=-1;
            var cont = 0;
            arrDibujoPuertasAcceso.forEach(function (item, index, array) 
            {
              arrCodigoPuertasAcceso[cont].descripcionPuertasAcceso = $("#descripcionPuertasAcceso"+item.id+"").val();                      
              arrCodigoPuertasAcceso[cont].maderaPuertaAcceso = document.getElementById("maderaPuertasAcceso"+item.id+"").checked; 
              arrCodigoPuertasAcceso[cont].metalPuertaAcceso = document.getElementById("metalPuertasAcceso"+item.id+"").checked;
              arrCodigoPuertasAcceso[cont].otrosPuertaAcceso = document.getElementById("otrosPuertasAcceso"+item.id+"").checked;
              arrCodigoPuertasAcceso[cont].otroDescPuertasAccesoA = $("#otrosDescPuertasAcceso"+item.id+"").val();
              arrCodigoPuertasAcceso[cont].integridadPuertasAccesoA = $("#integridPuertasAcceso"+item.id+"").val();

              cont++;
            });
            pos = arrDibujoPuertasAcceso.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrCodigoPuertasAcceso.splice(pos,1);
            arrDibujoPuertasAcceso.splice(pos,1);
            //console.log("arrCodigo",arrCodigoPuertasAcceso);

            cont=1;
            for (var i=0; i<arrDibujoPuertasAcceso.length;i++)
            {
              cadenaPuertasAcceso = cadenaPuertasAcceso + arrDibujoPuertasAcceso[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaPuertasAcceso').html(cadenaPuertasAcceso);
            var conto=0;
            arrDibujoPuertasAcceso.forEach(function (item, index, array) 
            { 
              $("#descripcionPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].descripcionPuertasAcceso);                 
              document.getElementById("maderaPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].maderaPuertaAcceso); 
              document.getElementById("metalPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].metalPuertaAcceso);
              document.getElementById("otrosPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].otrosPuertaAcceso);
              $("#otrosDescPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].otroDescPuertasAccesoA); 
              $("#integridPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].integridadPuertasAccesoA); 
              conto=conto+1;
            });
          });
        }
        //console.log("arrCodigoFinal",arrCodigoPuertasAcceso);
      }

    //--------------------GRILLA puerta de acceso FIN-------------------//
    //--------------------GRILLA Mobiliario Urbano--------------------//

    var contCodigoMovilidadUrbano = 2;
    var arrCodigoMovilidadUrbano = [];
    var arrDibujoMovilidadUrbano = [];


    htmlMovilidadUrbano='<div class="col-md-12">'+
    '<div class="form-group col-md-2">'+
    '<select id="descripcionMovilidadUrbano1" class="form-control">'+
    '<option value="N" selected="selected">..Seleccionar..</option>'+
    '<option value="1">Asientos</option>'+
    '<option value="2">Balaustrada</option>'+
    '<option value="3">Bancas</option>'+
    '<option value="4">Basureros</option>'+
    '<option value="5">Cabinas Tel.</option>'+
    '</select> '+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<input type="checkbox" id="madera1">Madera<br>'+
    '<input type="checkbox" id="metal1">Metal<br>'+
    '<input type="checkbox" id="barro1"> Barro<br>'+
    '<input type="checkbox" id="cal1"> Cal<br>'+
    '<input type="checkbox" id="ladrillo1">Ladrillo<br>'+
    '<input type="checkbox" id="piedra1"> Piedra<br>'+
    '</div>'+
    '<div class="form-group col-md-2">'+

    '<input type="checkbox" id="adobe1">Adobe<br>'+
    '<input type="checkbox" id="concreto1"> Concreto<br>'+
    '<input type="checkbox" id="fcarbon1">F. carbon<br>'+
    '<input type="checkbox" id="vidrio1">Vidrio<br>'+
    '<input type="checkbox" id="fibrocem1">Fibro<br>'+
    '<input type="checkbox" id="otros1"> Otros'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<input type="text" id="otrosDescMovilidadUrbano'+contCodigoMovilidadUrbano+'" class="form-control"  placeholder="Otras Descrip.">'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<select id="integridMovilidadUrbano1" class="form-control">'+
    '<option value="N" selected="selected">....Seleccionar...</option>'+
    '<option value="1">Original</option>'+
    '<option value="2">Nuevo</option>'+
    '<option value="3">Modificado</option>'+
    '<option value="4">Original y Nuevo</option>'+
    '</select> '+
    '</div>'+
    '<div class="col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaMovilidadUrbano(1);"></i>'+
    '</a></div>'+
    '</div>';

    var cadenaMovilidadUrbano = '';  
    arrCodigoMovilidadUrbano.push({descripcionMovilidadUrbano: 'N',maderaMovilidadUrbano: '',metalMovilidadUrbano: '',barroMovilidadUrbano: '',calMovilidadUrbano: '',ladrilloMovilidadUrbano: '',piedraMovilidadUrbano: '',adobeMovilidadUrbano: '',concretoMovilidadUrbano: '',fcarbonMovilidadUrbano: '',vidrioMovilidadUrbano: '',fibrocemMovilidadUrbano: '',otrosMovilidadUrbano: '',otroDescMovilidadUrbanoMovilidadUrbano: '',integridadMovilidadUrbanoMovilidadUrbano: ''});
    arrDibujoMovilidadUrbano.push({scd_codigo:htmlMovilidadUrbano,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaMovilidadUrbano=arrDibujoMovilidadUrbano[0].scd_codigo;
    //console.log("cadenaMovilidadUrbano",cadenaMovilidadUrbano);
    $('#crearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano);

    $("#descripcionMovilidadUrbano1").val(arrCodigoMovilidadUrbano[0].descripcionMovilidadUrbano); 
    document.getElementById("madera1").checked = (arrCodigoMovilidadUrbano[0].maderaMovilidadUrbano); 
    document.getElementById("metal1").checked = (arrCodigoMovilidadUrbano[0].metalMovilidadUrbano);
    document.getElementById("barro1").checked = (arrCodigoMovilidadUrbano[0].barroMovilidadUrbano);
    document.getElementById("cal1").checked = (arrCodigoMovilidadUrbano[0].calMovilidadUrbano);
    document.getElementById("ladrillo1").checked = (arrCodigoMovilidadUrbano[0].ladrilloMovilidadUrbano); 
    document.getElementById("piedra1").checked = (arrCodigoMovilidadUrbano[0].piedraMovilidadUrbano);
    document.getElementById("adobe1").checked = (arrCodigoMovilidadUrbano[0].adobeMovilidadUrbano);
    document.getElementById("concreto1").checked = (arrCodigoMovilidadUrbano[0].concretoMovilidadUrbano);
    document.getElementById("fcarbon1").checked = (arrCodigoMovilidadUrbano[0].fcarbonMovilidadUrbano); 
    document.getElementById("vidrio1").checked = (arrCodigoMovilidadUrbano[0].vidrioMovilidadUrbano);
    document.getElementById("fibrocem1").checked = (arrCodigoMovilidadUrbano[0].fibrocemMovilidadUrbano);
    document.getElementById("otros1").checked = (arrCodigoMovilidadUrbano[0].otrosMovilidadUrbano);
    $("#otrosDescMovilidadUrbano1").val(arrCodigoMovilidadUrbano[0].otroDescMovilidadUrbanoMovilidadUrbano); 
    $("#integridMovilidadUrbano1").val(arrCodigoMovilidadUrbano[0].integridadMovilidadUrbanoMovilidadUrbano);

    function grillaMovilidadUrbano(){

      var htmlMovilidadUrbano;

      htmlMovilidadUrbano='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionMovilidadUrbano'+contCodigoMovilidadUrbano+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="1">Asientos</option>'+
      '<option value="2">Balaustrada</option>'+
      '<option value="3">Bancas</option>'+
      '<option value="4">Basureros</option>'+
      '<option value="5">Cabinas Tel.</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="madera'+contCodigoMovilidadUrbano+'">Madera<br>'+
      '<input type="checkbox" id="metal'+contCodigoMovilidadUrbano+'">Metal<br>'+
      '<input type="checkbox" id="barro'+contCodigoMovilidadUrbano+'"> Barro<br>'+
      '<input type="checkbox" id="cal'+contCodigoMovilidadUrbano+'"> Cal<br>'+
      '<input type="checkbox" id="ladrillo'+contCodigoMovilidadUrbano+'">Ladrillo<br>'+
      '<input type="checkbox" id="piedra'+contCodigoMovilidadUrbano+'"> Piedra<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+

      '<input type="checkbox" id="adobe'+contCodigoMovilidadUrbano+'">Adobe<br>'+
      '<input type="checkbox" id="concreto'+contCodigoMovilidadUrbano+'"> Concreto<br>'+
      '<input type="checkbox" id="fcarbon'+contCodigoMovilidadUrbano+'">F. carbon<br>'+
      '<input type="checkbox" id="vidrio'+contCodigoMovilidadUrbano+'">Vidrio<br>'+
      '<input type="checkbox" id="fibrocem'+contCodigoMovilidadUrbano+'">Fibro<br>'+
      '<input type="checkbox" id="otros'+contCodigoMovilidadUrbano+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<input type="text" id="otrosDescMovilidadUrbano'+contCodigoMovilidadUrbano+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridMovilidadUrbano'+contCodigoMovilidadUrbano+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="1">Original</option>'+
      '<option value="2">Nuevo</option>'+
      '<option value="3">Modificado</option>'+
      '<option value="4">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaMovilidadUrbano('+contCodigoMovilidadUrbano+');"></i>'+
      '</a></div>'+
      '</div>';

      var cadenaMovilidadUrbano = '';
      var cont = 0;
      arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
      {
        arrCodigoMovilidadUrbano[cont].descripcionMovilidadUrbano = $("#descripcionMovilidadUrbano"+item.id+"").val();     
        arrCodigoMovilidadUrbano[cont].maderaMovilidadUrbano = document.getElementById("madera"+item.id+"").checked; 
        arrCodigoMovilidadUrbano[cont].metalMovilidadUrbano = document.getElementById("metal"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].barroMovilidadUrbano = document.getElementById("barro"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].calMovilidadUrbano = document.getElementById("cal"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].ladrilloMovilidadUrbano = document.getElementById("ladrillo"+item.id+"").checked; 
        arrCodigoMovilidadUrbano[cont].piedraMovilidadUrbano = document.getElementById("piedra"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].adobeMovilidadUrbano = document.getElementById("adobe"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].concretoMovilidadUrbano = document.getElementById("concreto"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].fcarbonMovilidadUrbano = document.getElementById("fcarbon"+item.id+"").checked; 
        arrCodigoMovilidadUrbano[cont].vidrioMovilidadUrbano = document.getElementById("vidrio"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].fibrocemMovilidadUrbano = document.getElementById("fibrocem"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].otrosMovilidadUrbano = document.getElementById("otros"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].otroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDescMovilidadUrbano"+item.id+"").val();
        arrCodigoMovilidadUrbano[cont].integridadMovilidadUrbanoMovilidadUrbano = $("#integridMovilidadUrbano"+item.id+"").val();
        cont++;
      });

      arrCodigoMovilidadUrbano.push({descripcionMovilidadUrbano: 'N',maderaMovilidadUrbano: '',metalMovilidadUrbano: '',barroMovilidadUrbano: '',calMovilidadUrbano: '',ladrilloMovilidadUrbano: '',piedraMovilidadUrbano: '',adobeMovilidadUrbano: '',concretoMovilidadUrbano: '',fcarbonMovilidadUrbano: '',vidrioMovilidadUrbano: '',fibrocemMovilidadUrbano: '',otrosMovilidadUrbano: '',otroDescMovilidadUrbanoMovilidadUrbano: '',integridadMovilidadUrbanoMovilidadUrbano: ''});
      arrDibujoMovilidadUrbano.push({scd_codigo:htmlMovilidadUrbano, id: contCodigoMovilidadUrbano});
      contCodigoMovilidadUrbano++;

      cont=1;
      for (var i=0; i<arrDibujoMovilidadUrbano.length;i++)
      {
        cadenaMovilidadUrbano = cadenaMovilidadUrbano + arrDibujoMovilidadUrbano[i].scd_codigo;
        cont++;
      }
      
      $('#crearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano); 

      var conto=0;
      arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
      { 
        $("#descripcionMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].descripcionMovilidadUrbano); 
        document.getElementById("madera"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].maderaMovilidadUrbano); 
        document.getElementById("metal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].metalMovilidadUrbano);
        document.getElementById("barro"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].barroMovilidadUrbano);
        document.getElementById("cal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].calMovilidadUrbano);
        document.getElementById("ladrillo"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].ladrilloMovilidadUrbano); 
        document.getElementById("piedra"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].piedraMovilidadUrbano);
        document.getElementById("adobe"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].adobeMovilidadUrbano);
        document.getElementById("concreto"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].concretoMovilidadUrbano);
        document.getElementById("fcarbon"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fcarbonMovilidadUrbano); 
        document.getElementById("vidrio"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].vidrioMovilidadUrbano);
        document.getElementById("fibrocem"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fibrocemMovilidadUrbano);
        document.getElementById("otros"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].otrosMovilidadUrbano);
        $("#otrosDescMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].otroDescMovilidadUrbanoMovilidadUrbano); 
        $("#integridMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].integridadMovilidadUrbanoMovilidadUrbano); 

        conto=conto+1;
      });

        ////console.log("arrCodigoMovilidadUrbano",arrCodigoMovilidadUrbano);
      }


      function eliminarGrillaMovilidadUrbano($id){
        if(contCodigoMovilidadUrbano>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaMovilidadUrbano ='';
            var pos=-1;
            var cont = 0;
            arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
            {
              arrCodigoMovilidadUrbano[cont].descripcionMovilidadUrbano = $("#descripcionMovilidadUrbano"+item.id+"").val();     
              arrCodigoMovilidadUrbano[cont].maderaMovilidadUrbano = document.getElementById("madera"+item.id+"").checked; 
              arrCodigoMovilidadUrbano[cont].metalMovilidadUrbano = document.getElementById("metal"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].barroMovilidadUrbano = document.getElementById("barro"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].calMovilidadUrbano = document.getElementById("cal"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].ladrilloMovilidadUrbano = document.getElementById("ladrillo"+item.id+"").checked; 
              arrCodigoMovilidadUrbano[cont].piedraMovilidadUrbano = document.getElementById("piedra"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].adobeMovilidadUrbano = document.getElementById("adobe"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].concretoMovilidadUrbano = document.getElementById("concreto"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].fcarbonMovilidadUrbano = document.getElementById("fcarbon"+item.id+"").checked; 
              arrCodigoMovilidadUrbano[cont].vidrioMovilidadUrbano = document.getElementById("vidrio"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].fibrocemMovilidadUrbano = document.getElementById("fibrocem"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].otrosMovilidadUrbano = document.getElementById("otros"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].otroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDescMovilidadUrbano"+item.id+"").val();
              arrCodigoMovilidadUrbano[cont].integridadMovilidadUrbanoMovilidadUrbano = $("#integridMovilidadUrbano"+item.id+"").val();

              cont++;
            });
            pos = arrDibujoMovilidadUrbano.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrCodigoMovilidadUrbano.splice(pos,1);
            arrDibujoMovilidadUrbano.splice(pos,1);
            //console.log("arrCodigo",arrCodigoMovilidadUrbano);

            cont=1;
            for (var i=0; i<arrDibujoMovilidadUrbano.length;i++)
            {
              cadenaMovilidadUrbano = cadenaMovilidadUrbano + arrDibujoMovilidadUrbano[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano);
            var conto=0;
            arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
            { 

              $("#descripcionMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].descripcionMovilidadUrbano); 
              document.getElementById("madera"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].maderaMovilidadUrbano); 
              document.getElementById("metal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].metalMovilidadUrbano);
              document.getElementById("barro"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].barroMovilidadUrbano);
              document.getElementById("cal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].calMovilidadUrbano);
              document.getElementById("ladrillo"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].ladrilloMovilidadUrbano); 
              document.getElementById("piedra"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].piedraMovilidadUrbano);
              document.getElementById("adobe"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].adobeMovilidadUrbano);
              document.getElementById("concreto"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].concretoMovilidadUrbano);
              document.getElementById("fcarbon"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fcarbonMovilidadUrbano); 
              document.getElementById("vidrio"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].vidrioMovilidadUrbano);
              document.getElementById("fibrocem"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fibrocemMovilidadUrbano);
              document.getElementById("otros"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].otrosMovilidadUrbano);
              $("#otrosDescMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].otroDescMovilidadUrbanoMovilidadUrbano); 
              $("#integridMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].integridadMovilidadUrbanoMovilidadUrbano); 
              conto=conto+1;
            });
          });
}
        ///////console.log("arrCodigoFinal",arrCodigoAcab);
      }

    //--------------------GRILLA Mobiliario Urbano FIN-------------------//

   //--------------------GRILLA Escaleras--------------------//

   var contCodigoEscaleras = 2;
   var arrCodigoEscaleras = [];
   var arrDibujoEscaleras = [];
   htmlEscaleras='<div class="col-md-12">'+
   '<div class="form-group col-md-2">'+
   '<select id="descripcionEscaleras1" class="form-control">'+
   '<option value="N" selected="selected">..Seleccionar..</option>'+
   '<option value="1">Recta</option>'+
   '<option value="2">Imperial</option>'+
   '<option value="3">Caracol</option>'+
   '<option value="4">U</option>'+
   '<option value="5">L</option>'+
   '</select> '+
   '</div>'+
   '<div class="form-group col-md-2">'+
   '<input type="checkbox" id="maderaEscaleras1">Madera<br>'+
   '<input type="checkbox" id="metalEscaleras1">Metal<br>'+
   '<input type="checkbox" id="ConcretoEscaleras1"> Concreto<br>'+
   '<input type="checkbox" id="PiedraEscaleras1">Piedra<br>'+
   '</div>'+
   '<div class="form-group col-md-2">'+

   '<input type="checkbox" id="ladrilloEscaleras1">Ladrillo<br>'+
   '<input type="checkbox" id="tierraEscaleras1">Tierra<br>'+
   '<input type="checkbox" id="otrosEscaleras1"> Otros'+
   '</div>'+
   '<div class="form-group col-md-3">'+
   '<input type="text" id="otrosDescEscaleras1" class="form-control"  placeholder="Otras Descrip.">'+
   '</div>'+
   '<div class="form-group col-md-2">'+
   '<select id="integridEscaleras1" class="form-control">'+
   '<option value="N" selected="selected">....Seleccionar...</option>'+
   '<option value="1">Original</option>'+
   '<option value="2">Nuevo</option>'+
   '<option value="3">Modificado</option>'+
   '<option value="4">Original y Nuevo</option>'+
   '</select> '+
   '</div>'+
   '<div class="col-md-1">'+
   '<a style="cursor:pointer;" type="button">'+
   '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaEscaleras(1);"></i>'+
   '</a></div>'+
   '</div>';

   var cadenaEscaleras = '';  
   arrCodigoEscaleras.push({descripcionEscaleras: 'N',maderaEscaleras: '', metalEscaleras: '', ConcretoEscaleras: '', PiedraEscaleras: '', ladrilloEscaleras: '', tierraEscaleras: '', otrosEscaleras: '', otroDescEscaleras: '',integridadEscaleras: ''});
   arrDibujoEscaleras.push({scd_codigo:htmlEscaleras,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaEscaleras=arrDibujoEscaleras[0].scd_codigo;
    //console.log("cadenaEscaleras",cadenaEscaleras);
    $('#crearGrillaEscalerasado').html(cadenaEscaleras);


    $("#descripcionEscaleras1").val(arrCodigoEscaleras[0].descripcionEscaleras); 

    document.getElementById("maderaEscaleras1").checked = (arrCodigoEscaleras[0].maderaEscaleras); 
    document.getElementById("metalEscaleras1").checked = (arrCodigoEscaleras[0].metalEscaleras);
    document.getElementById("ConcretoEscaleras1").checked = (arrCodigoEscaleras[0].ConcretoEscaleras);
    document.getElementById("PiedraEscaleras1").checked = (arrCodigoEscaleras[0].PiedraEscaleras);
    document.getElementById("ladrilloEscaleras1").checked = (arrCodigoEscaleras[0].ladrilloEscaleras); 
    document.getElementById("tierraEscaleras1").checked = (arrCodigoEscaleras[0].tierraEscaleras);
    document.getElementById("otrosEscaleras1").checked = (arrCodigoEscaleras[0].otrosEscaleras);
    $("#otrosDescEscaleras1").val(arrCodigoEscaleras[0].otroDescEscaleras); 
    $("#integridEscaleras1").val(arrCodigoEscaleras[0].integridadEscaleras);

    function grillaEscaleras(){

      var htmlEscaleras;

      htmlEscaleras='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionEscaleras'+contCodigoEscaleras+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="1">Recta</option>'+
      '<option value="2">Imperial</option>'+
      '<option value="3">Caracol</option>'+
      '<option value="4">U</option>'+
      '<option value="5">L</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="maderaEscaleras'+contCodigoEscaleras+'">Madera<br>'+
      '<input type="checkbox" id="metalEscaleras'+contCodigoEscaleras+'">Metal<br>'+
      '<input type="checkbox" id="ConcretoEscaleras'+contCodigoEscaleras+'"> Concreto<br>'+
      '<input type="checkbox" id="PiedraEscaleras'+contCodigoEscaleras+'">Piedra<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+

      '<input type="checkbox" id="ladrilloEscaleras'+contCodigoEscaleras+'">Ladrillo<br>'+
      '<input type="checkbox" id="tierraEscaleras'+contCodigoEscaleras+'">Tierra<br>'+
      '<input type="checkbox" id="otrosEscaleras'+contCodigoEscaleras+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<input type="text" id="otrosDescEscaleras'+contCodigoEscaleras+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridEscaleras'+contCodigoEscaleras+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="1">Original</option>'+
      '<option value="2">Nuevo</option>'+
      '<option value="3">Modificado</option>'+
      '<option value="4">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaEscaleras('+contCodigoEscaleras+');"></i>'+
      '</a></div>'+
      '</div>';



      var cadenaEscaleras = '';
      var cont = 0;
      arrDibujoEscaleras.forEach(function (item, index, array) 
      {
        arrCodigoEscaleras[cont].descripcionEscaleras = $("#descripcionEscaleras"+item.id+"").val();     
        arrCodigoEscaleras[cont].maderaEscaleras = document.getElementById("maderaEscaleras"+item.id+"").checked; 
        arrCodigoEscaleras[cont].metalEscaleras = document.getElementById("metalEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].ConcretoEscaleras = document.getElementById("ConcretoEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].PiedraEscaleras = document.getElementById("PiedraEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].ladrilloEscaleras = document.getElementById("ladrilloEscaleras"+item.id+"").checked; 
        arrCodigoEscaleras[cont].tierraEscaleras = document.getElementById("tierraEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].otrosEscaleras = document.getElementById("otrosEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].otroDescEscaleras = $("#otrosDescEscaleras"+item.id+"").val();
        arrCodigoEscaleras[cont].integridadEscaleras = $("#integridEscaleras"+item.id+"").val();
        cont++;
      });

      arrCodigoEscaleras.push({descripcionEscaleras: 'N',maderaEscaleras: '', metalEscaleras: '', ConcretoEscaleras: '', PiedraEscaleras: '', ladrilloEscaleras: '', tierraEscaleras: '', otrosEscaleras: '', otroDescEscaleras: '',integridadEscaleras: ''});
      arrDibujoEscaleras.push({scd_codigo:htmlEscaleras, id: contCodigoEscaleras});
      contCodigoEscaleras++;

      cont=1;
      for (var i=0; i<arrDibujoEscaleras.length;i++)
      {
        cadenaEscaleras = cadenaEscaleras + arrDibujoEscaleras[i].scd_codigo;
        cont++;
      }
      
      $('#crearGrillaEscalerasado').html(cadenaEscaleras); 

      var conto=0;
      arrDibujoEscaleras.forEach(function (item, index, array) 
      { 
        $("#descripcionEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].descripcionEscaleras); 
        document.getElementById("metalEscaleras"+item.id+"").checked = (arrCodigoEscaleras[0].metalEscaleras);
        document.getElementById("ConcretoEscaleras"+item.id+"").checked = (arrCodigoEscaleras[0].ConcretoEscaleras);
        document.getElementById("PiedraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[0].PiedraEscaleras);
        document.getElementById("ladrilloEscaleras"+item.id+"").checked = (arrCodigoEscaleras[0].ladrilloEscaleras); 
        document.getElementById("tierraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[0].tierraEscaleras);
        document.getElementById("otrosEscaleras"+item.id+"").checked = (arrCodigoEscaleras[0].otrosEscaleras);
        $("#otrosDescEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].otroDescEscaleras); 
        $("#integridEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].integridadEscaleras); 

        conto=conto+1;
      });

        ////console.log("arrCodigoEscaleras",arrCodigoEscaleras);
      }


      function eliminarGrillaEscaleras($id){
        if(contCodigoEscaleras>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaEscaleras ='';
            var pos=-1;
            var cont = 0;
            arrDibujoEscaleras.forEach(function (item, index, array) 
            {
              arrCodigoEscaleras[cont].descripcionEscaleras = $("#descripcionEscaleras"+item.id+"").val();     
              arrCodigoEscaleras[cont].maderaEscaleras = document.getElementById("maderaEscaleras"+item.id+"").checked; 
              arrCodigoEscaleras[cont].metalEscaleras = document.getElementById("metalEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].ConcretoEscaleras = document.getElementById("ConcretoEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].PiedraEscaleras = document.getElementById("PiedraEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].ladrilloEscaleras = document.getElementById("ladrilloEscaleras"+item.id+"").checked; 
              arrCodigoEscaleras[cont].tierraEscaleras = document.getElementById("tierraEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].otrosEscaleras = document.getElementById("otrosEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].otroDescEscaleras = $("#otrosDescEscaleras"+item.id+"").val();
              arrCodigoEscaleras[cont].integridadEscaleras = $("#integridEscaleras"+item.id+"").val();
              cont++;
            });
            pos = arrDibujoEscaleras.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrCodigoEscaleras.splice(pos,1);
            arrDibujoEscaleras.splice(pos,1);
            //console.log("arrCodigo",arrCodigoEscaleras);

            cont=1;
            for (var i=0; i<arrDibujoEscaleras.length;i++)
            {
              cadenaEscaleras = cadenaEscaleras + arrDibujoEscaleras[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaEscalerasado').html(cadenaEscaleras);
            var conto=0;
            arrDibujoEscaleras.forEach(function (item, index, array) 
            { 
              $("#descripcionEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].descripcionEscaleras); 
              document.getElementById("maderaEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].maderaEscaleras); 
              document.getElementById("metalEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].metalEscaleras);
              document.getElementById("ConcretoEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].ConcretoEscaleras);
              document.getElementById("PiedraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].PiedraEscaleras);
              document.getElementById("ladrilloEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].ladrilloEscaleras); 
              document.getElementById("tierraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].tierraEscaleras);
              document.getElementById("otrosEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].otrosEscaleras);             

              $("#otrosDescEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].otroDescEscaleras); 
              $("#integridEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].integridadEscaleras); 
              conto=conto+1;
            });
          });
        }
        ///////console.log("arrCodigoFinal",arrCodigoAcab);
      }

//--------------------GRILLA Escalesras FIN-------------------//
//--------------------GRILLA CARACTERISTICAS DE LA VEGETACION--------------------//

var contCodigoCarVegetacion = 2;
var arrCodigoCarVegetacion = [];
var arrDibujoCarVegetacion = [];
 htmlCarVegetacion='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionCarVegetacion1" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="1">Arboles</option>'+
  '<option value="2">Arbustos</option>'+
  '<option value="3">Trepadoras</option>'+
  '<option value="4">Herbaceas</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="cantidadCarVegetacion1" class="form-control">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="checkbox" id="sinFloracionCarVegetacion1">Sin Floracion<br>'+
  '<input type="checkbox" id="caducaCarVegetacion1">Caduca<br>'+
  '<input type="checkbox" id="perenneCarVegetacion1">Perenne<br>'+
  '<input type="checkbox" id="otrosCarVegetacion1"> Otros'+
  '</div>'+
  '<div class="form-group col-md-3">'+
  '<input type="text" id="otrosDescCarVegetacion1" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridCarVegetacion1" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="1">Original</option>'+
  '<option value="2">Nuevo</option>'+
  '<option value="3">Modificado</option>'+
  '<option value="4">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaCarVegetacion(1);"></i>'+
  '</a></div>'+
  '</div>';


var cadenaCarVegetacion = '';  
arrCodigoCarVegetacion.push({descripcionCarVegetacion: 'N', cantidadCarVegetacion: '', sinFloracionCarVegetacion: '', caducaCarVegetacion: '', perenneCarVegetacion: '', otrosCarVegetacion: '', otroDescCarVegetacion: '', integridadCarVegetacion: ''});
arrDibujoCarVegetacion.push({scd_codigo:htmlCarVegetacion,id: 1});


cadenaCarVegetacion=arrDibujoCarVegetacion[0].scd_codigo;
//console.log("cadenaCarVegetacion",cadenaCarVegetacion);
$('#crearGrillaCarVegetacionado').html(cadenaCarVegetacion);

$("#descripcionCarVegetacion1").val(arrCodigoCarVegetacion[0].descripcionCarVegetacion);
$("#cantidadCarVegetacion1").val(arrCodigoCarVegetacion[0].cantidadCarVegetacion);  
document.getElementById("sinFloracionCarVegetacion1").checked = (arrCodigoCarVegetacion[0].sinFloracionCarVegetacion);
document.getElementById("caducaCarVegetacion1").checked = (arrCodigoCarVegetacion[0].caducaCarVegetacion); 
document.getElementById("perenneCarVegetacion1").checked = (arrCodigoCarVegetacion[0].perenneCarVegetacion);
document.getElementById("otrosCarVegetacion1").checked = (arrCodigoCarVegetacion[0].otrosCarVegetacion);
$("#otrosDescCarVegetacion1").val(arrCodigoCarVegetacion[0].otroDescCarVegetacion); 
$("#integridCarVegetacion1").val(arrCodigoCarVegetacion[0].integridadCarVegetacion);

function grillaCarVegetacion(){

  var htmlCarVegetacion;

  htmlCarVegetacion='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionCarVegetacion'+contCodigoCarVegetacion+'" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="1">Arboles</option>'+
  '<option value="2">Arbustos</option>'+
  '<option value="3">Trepadoras</option>'+
  '<option value="4">Herbaceas</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="cantidadCarVegetacion'+contCodigoCarVegetacion+'" class="form-control">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="checkbox" id="sinFloracionCarVegetacion'+contCodigoCarVegetacion+'">Sin Floracion<br>'+
  '<input type="checkbox" id="caducaCarVegetacion'+contCodigoCarVegetacion+'">Caduca<br>'+
  '<input type="checkbox" id="perenneCarVegetacion'+contCodigoCarVegetacion+'">Perenne<br>'+
  '<input type="checkbox" id="otrosCarVegetacion'+contCodigoCarVegetacion+'"> Otros'+
  '</div>'+
  '<div class="form-group col-md-3">'+
  '<input type="text" id="otrosDescCarVegetacion'+contCodigoCarVegetacion+'" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridCarVegetacion'+contCodigoCarVegetacion+'" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="1">Original</option>'+
  '<option value="2">Nuevo</option>'+
  '<option value="3">Modificado</option>'+
  '<option value="4">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaCarVegetacion('+contCodigoCarVegetacion+');"></i>'+
  '</a></div>'+
  '</div>';

  var cadenaCarVegetacion = '';
  var cont = 0;
  arrDibujoCarVegetacion.forEach(function (item, index, array) {
    arrCodigoCarVegetacion[cont].descripcionCarVegetacion = $("#descripcionCarVegetacion"+item.id+"").val();     
    arrCodigoCarVegetacion[cont].cantidadCarVegetacion = $("#cantidadCarVegetacion"+item.id+"").val();
    arrCodigoCarVegetacion[cont].sinFloracionCarVegetacion = document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked;
    arrCodigoCarVegetacion[cont].caducaCarVegetacion = document.getElementById("caducaCarVegetacion"+item.id+"").checked; 
    arrCodigoCarVegetacion[cont].perenneCarVegetacion = document.getElementById("perenneCarVegetacion"+item.id+"").checked;
    arrCodigoCarVegetacion[cont].otrosCarVegetacion = document.getElementById("otrosCarVegetacion"+item.id+"").checked;
    arrCodigoCarVegetacion[cont].otroDescCarVegetacion = $("#otrosDescCarVegetacion"+item.id+"").val();
    arrCodigoCarVegetacion[cont].integridadCarVegetacion = $("#integridCarVegetacion"+item.id+"").val();
    cont++;
  });

  arrCodigoCarVegetacion.push({descripcionCarVegetacion: 'N', cantidadCarVegetacion: '', sinFloracionCarVegetacion: '', caducaCarVegetacion: '', perenneCarVegetacion: '', otrosCarVegetacion: '', otroDescCarVegetacion: '', integridadCarVegetacion: ''});
  arrDibujoCarVegetacion.push({scd_codigo:htmlCarVegetacion, id: contCodigoCarVegetacion});
  contCodigoCarVegetacion++;

  cont=1;
  for (var i=0; i<arrDibujoCarVegetacion.length;i++)
  {
    cadenaCarVegetacion = cadenaCarVegetacion + arrDibujoCarVegetacion[i].scd_codigo;
    cont++;
  }
  
  $('#crearGrillaCarVegetacionado').html(cadenaCarVegetacion); 

  var conto=0;
  arrDibujoCarVegetacion.forEach(function (item, index, array) 
  { 

    $("#descripcionCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].descripcionCarVegetacion);
    $("#cantidadCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].cantidadCarVegetacion);  
    document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].sinFloracionCarVegetacion);
    document.getElementById("caducaCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].caducaCarVegetacion); 
    document.getElementById("perenneCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].perenneCarVegetacion);
    document.getElementById("otrosCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].otrosCarVegetacion);
    $("#otrosDescCarVegetacion").val(arrCodigoCarVegetacion[conto].otroDescCarVegetacion); 
    $("#integridCarVegetacion").val(arrCodigoCarVegetacion[conto].integridadCarVegetacion);
    conto=conto+1;
  });
}


function eliminarGrillaCarVegetacion($id){
  if(contCodigoCarVegetacion>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaCarVegetacion ='';
      var pos=-1;
      var cont = 0;
      arrDibujoCarVegetacion.forEach(function (item, index, array) 
      {
        arrCodigoCarVegetacion[cont].descripcionCarVegetacion = $("#descripcionCarVegetacion"+item.id+"").val();     
        arrCodigoCarVegetacion[cont].cantidadCarVegetacion = $("#cantidadCarVegetacion"+item.id+"").val();
        arrCodigoCarVegetacion[cont].sinFloracionCarVegetacion = document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked;
        arrCodigoCarVegetacion[cont].caducaCarVegetacion = document.getElementById("caducaCarVegetacion"+item.id+"").checked; 
        arrCodigoCarVegetacion[cont].perenneCarVegetacion = document.getElementById("perenneCarVegetacion"+item.id+"").checked;
        arrCodigoCarVegetacion[cont].otrosCarVegetacion = document.getElementById("otrosCarVegetacion"+item.id+"").checked;
        arrCodigoCarVegetacion[cont].otroDescCarVegetacion = $("#otrosDescCarVegetacion"+item.id+"").val();
        arrCodigoCarVegetacion[cont].integridadCarVegetacion = $("#integridCarVegetacion"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoCarVegetacion.map(function(e) {return e.id;}).indexOf($id);
      //console.log("poos",pos);
      arrCodigoCarVegetacion.splice(pos,1);
      arrDibujoCarVegetacion.splice(pos,1);
      //console.log("arrCodigo",arrCodigoCarVegetacion);

      cont=1;
      for (var i=0; i<arrDibujoCarVegetacion.length;i++)
      {
        cadenaCarVegetacion = cadenaCarVegetacion + arrDibujoCarVegetacion[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaCarVegetacionado').html(cadenaCarVegetacion);
      var conto=0;
      arrDibujoCarVegetacion.forEach(function (item, index, array) 
      { 


         $("#descripcionCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].descripcionCarVegetacion);
    $("#cantidadCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].cantidadCarVegetacion);  
    document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].sinFloracionCarVegetacion);
    document.getElementById("caducaCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].caducaCarVegetacion); 
    document.getElementById("perenneCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].perenneCarVegetacion);
    document.getElementById("otrosCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].otrosCarVegetacion);
    $("#otrosDescCarVegetacion").val(arrCodigoCarVegetacion[conto].otroDescCarVegetacion); 
    $("#integridCarVegetacion").val(arrCodigoCarVegetacion[conto].integridadCarVegetacion);

        conto=conto+1;
      });
    });
  }
}
    //--------------------GRILLA CARCATERISTICAS DE LA VEGETACION FIN-------------------//
    
    //--------------------GRILLA Detalles Artisitcos--------------------//
    var contCodigoDetaArtistoco = 2;
    var arrCodigoDetaArtistoco = [];
    var arrDibujoDetaArtistoco = [];
    htmlDetaArtistoco='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
    '<select id="descripcionDetaArtistoco1" class="form-control">'+
    '<option value="N" selected="selected">..Seleccionar..</option>'+
    '<option value="1">Pinturas Mural</option>'+
    '<option value="2">Esculuturas</option>'+
    '<option value="3">Caracol</option>'+
    '<option value="4">Enchapes de Ceramica</option>'+
    '<option value="5">Herrajes</option>'+
    '<option value="6">Portada</option>'+
    '</select> '+
    '</div>'+
    '<div class="form-group col-md-5">'+
    '<select id="integridDetaArtistoco1" class="form-control">'+
    '<option value="N" selected="selected">....Seleccionar...</option>'+
    '<option value="1">Original</option>'+
    '<option value="2">Nuevo</option>'+
    '<option value="3">Modificado</option>'+
    '<option value="4">Original y Nuevo</option>'+
    '</select> '+
    '</div>'+
    '<div class="col-md-2">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaDetaArtistoco(1);"></i>'+
    '</a></div>'+
    '</div>';
    var cadenaDetaArtistoco = '';  
    arrCodigoDetaArtistoco.push({descripcionDetaArtistoco: 'N',integridadDetaArtistoco: ''});
    arrDibujoDetaArtistoco.push({scd_codigo:htmlDetaArtistoco,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    cadenaDetaArtistoco=arrDibujoDetaArtistoco[0].scd_codigo;
    //console.log("cadenaDetaArtistoco",cadenaDetaArtistoco);
    $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco);
    $("#integridDetaArtistoco1").val(arrCodigoDetaArtistoco[0].integridadDetaArtistoco);

    function grillaDetaArtistoco(){

      var htmlDetaArtistoco;

      htmlDetaArtistoco='<div class="col-md-12">'+
      '<div class="form-group col-md-5">'+
      '<select id="descripcionDetaArtistoco'+contCodigoDetaArtistoco+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="1">Pinturas Mural</option>'+
      '<option value="2">Esculuturas</option>'+
      '<option value="3">Caracol</option>'+
      '<option value="4">Enchapes de Ceramica</option>'+
      '<option value="5">Herrajes</option>'+
      '<option value="6">Portada</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<select id="integridDetaArtistoco'+contCodigoDetaArtistoco+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="1">Original</option>'+
      '<option value="2">Nuevo</option>'+
      '<option value="3">Modificado</option>'+
      '<option value="4">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaDetaArtistoco('+contCodigoDetaArtistoco+');"></i>'+
      '</a></div>'+
      '</div>';



      var cadenaDetaArtistoco = '';
      var cont = 0;
      arrDibujoDetaArtistoco.forEach(function (item, index, array) 
      {
        arrCodigoDetaArtistoco[cont].descripcionDetaArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
        arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
        cont++;
      });

      arrCodigoDetaArtistoco.push({descripcionDetaArtistoco: 'N', integridadDetaArtistoco: ''});
      arrDibujoDetaArtistoco.push({scd_codigo:htmlDetaArtistoco, id: contCodigoDetaArtistoco});
      contCodigoDetaArtistoco++;

      cont=1;
      for (var i=0; i<arrDibujoDetaArtistoco.length;i++)
      {
        cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDetaArtistoco[i].scd_codigo;
        cont++;
      }
      
      $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco); 

      var conto=0;
      arrDibujoDetaArtistoco.forEach(function (item, index, array) 
      { 
        $("#descripcionDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].descripcionDetaArtistoco); 
        $("#integridDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].integridadDetaArtistoco); 

        conto=conto+1;
      });

        ////console.log("arrCodigoDetaArtistoco",arrCodigoDetaArtistoco);
      }


      function eliminarGrillaDetaArtistoco($id){
        if(contCodigoDetaArtistoco>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaDetaArtistoco ='';
            var pos=-1;
            var cont = 0;
            arrDibujoDetaArtistoco.forEach(function (item, index, array) 
            {
              arrCodigoDetaArtistoco[cont].descripcionDetaArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
              arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
              cont++;
            });
            pos = arrDibujoDetaArtistoco.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrCodigoDetaArtistoco.splice(pos,1);
            arrDibujoDetaArtistoco.splice(pos,1);
            //console.log("arrCodigo",arrCodigoDetaArtistoco);

            cont=1;
            for (var i=0; i<arrDibujoDetaArtistoco.length;i++)
            {
              cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDetaArtistoco[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco);
            var conto=0;
            arrDibujoDetaArtistoco.forEach(function (item, index, array) 
            { 
              $("#descripcionDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].descripcionDetaArtistoco); 
              $("#integridDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].integridadDetaArtistoco); 
              conto=conto+1;
            });
          });
        }
        ///////console.log("arrCodigoFinal",arrCodigoAcab);
      }

    //--------------------GRILLA Detalles Artisitcos-------------------//
     //GRILLA TIPOLOGICOS COMPOSITIVOS 
    var contCodigoTIC=2;
    var arrCodigoTIC=[];
    var arrDibujoTIC=[];    
      htmlTIC='<div class="col-md-12">'+                     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="tipologicos11" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+            
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-3">'+                
                    '<select class="form-control" id="tipologicos21" >'+
                       '<option value="N" selected="selected">--Seleccione--</option>'+
                       '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+ 
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="tipologicos31" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="atrio">Atrio</option>'+
                        '<option value="capilla">Capilla</option>'+
                        '<option value="chifon">Chifon</option>'+
                        '<option value="claustro">Claustro</option>'+   
                        '<option value="corredor">Corredor</option>'+
                        '<option value="corredor ardiente">Corredor ardiente</option>'+
                        '<option value="crujias">Crujias</option>'+
                        '<option value="enfarolado">Enfarolado</option>'+
                        '<option value="estacionamiento">Estacionamiento</option>'+
                        '<option value="galeria">Galeria</option>'+
                        '<option value="jardin interior">Jardin interior</option>'+
                        '<option value="hall">Hall</option>'+
                        '<option value="nave unica">Nave unica</option>'+
                        '<option value="naves">Naves</option>'+
                        '<option value="patio primero">Patio primero</option>'+
                        '<option value="patio segundo">Patio segundo</option>'+
                        '<option value="peribolo">Peribolo</option>'+
                        '<option value="planta basilical">planta basilical</option>'+
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaTIC = '';  
    arrCodigoTIC.push({tipologicos1:'N', tipologicos2:'N',tipologicos3:'N'});
    arrDibujoTIC.push({scd_codigo:htmlTIC,id: 1});        
    //cadenaTIC=arrDibujoTIC[0].scd_codigo;    
    cadenaTIC='<div class="form-group"><label class="col-md-12">'+arrDibujoTIC[0].scd_codigo;
    $('#grillaTipologicosC').html(cadenaTIC);
    $("#tipologicos11").val(arrCodigoTIC[0].tipologicos1);
    $("#tipologicos21").val(arrCodigoTIC[0].tipologicos2);
    $("#tipologicos31").val(arrCodigoTIC[0].tipologicos3);

function crearTIC(){
    var htmlTIC;
        htmlTIC='<br><div class="col-md-12">'+                   
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="tipologicos1'+contCodigoTIC+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-3">'+                
                    '<select class="form-control" id="tipologicos2'+contCodigoTIC+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="tipologicos3'+contCodigoTIC+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="atrio">Atrio</option>'+
                        '<option value="capilla">Capilla</option>'+
                        '<option value="chifon">Chifon</option>'+
                        '<option value="claustro">Claustro</option>'+   
                        '<option value="corredor">Corredor</option>'+
                        '<option value="corredor ardiente">Corredor ardiente</option>'+
                        '<option value="crujias">Crujias</option>'+
                        '<option value="enfarolado">Enfarolado</option>'+
                        '<option value="estacionamiento">Estacionamiento</option>'+
                        '<option value="galeria">Galeria</option>'+
                        '<option value="jardin interior">Jardin interior</option>'+
                        '<option value="hall">Hall</option>'+
                        '<option value="nave unica">Nave unica</option>'+
                        '<option value="naves">Naves</option>'+
                        '<option value="patio primero">Patio primero</option>'+
                        '<option value="patio segundo">Patio segundo</option>'+
                        '<option value="peribolo">Peribolo</option>'+
                        '<option value="planta basilical">planta basilical</option>'+
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaTIC('+contCodigoTIC+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaTIC='';
        var cont=0;
        arrDibujoTIC.forEach(function (item, index, array) 
        {
          arrCodigoTIC[cont].tipologicos1 = $("#tipologicos1"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos2 = $("#tipologicos2"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos3 = $("#tipologicos3"+item.id+"").val();
          cont++;
        });
        arrCodigoTIC.push({tipologicos1:'N', tipologicos2:'N',tipologicos3:'N'});
        arrDibujoTIC.push({scd_codigo:htmlTIC,id: contCodigoTIC});
        contCodigoTIC++;
        cont=1;
        for (var i=0; i<arrDibujoTIC.length;i++)
        {
          cadenaTIC=cadenaTIC + arrDibujoTIC[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#grillaTipologicosC').html(cadenaTIC); 
        var conto=0;
        arrDibujoTIC.forEach(function (item, index, array) 
        { 
          $("#tipologicos1"+item.id+"").val(arrCodigoTIC[conto].tipologicos1);
          $("#tipologicos2"+item.id+"").val(arrCodigoTIC[conto].tipologicos2);
          $("#tipologicos3"+item.id+"").val(arrCodigoTIC[conto].tipologicos3);
          conto=conto+1;
        });
        //console.log("arrCodigoTIC",arrCodigoTIC);
        
    }
        function menosLineaTIC($id){
        if(contCodigoTIC>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaTIC='';
                var cont=0;
                var pos=-1;
                arrDibujoTIC.forEach(function (item, index, array) 
                {
                    arrCodigoTIC[cont].tipologicos1 = $("#tipologicos1"+item.id+"").val();
                    arrCodigoTIC[cont].tipologicos2 = $("#tipologicos2"+item.id+"").val();
                    arrCodigoTIC[cont].tipologicos3 = $("#tipologicos3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoTIC.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoTIC.splice(pos,1);
                arrDibujoTIC.splice(pos,1);
                //console.log("arrCodigoTIC",arrCodigoTIC);
                
                cont=1;
                for (var i=0; i<arrDibujoTIC.length;i++)
                {
                    cadenaTIC=cadenaTIC +arrDibujoTIC[i].scd_codigo;
                    cont++;
                }                
                $('#grillaTipologicosC').html(cadenaTIC);
                
                cont=0;
                arrDibujoTIC.forEach(function (item, index, array) 
                {
                    $("#tipologicos1"+item.id+"").val(arrCodigoTIC[cont].tipologicos1);
                    $("#tipologicos2"+item.id+"").val(arrCodigoTIC[cont].tipologicos2);
                    $("#tipologicos3"+item.id+"").val(arrCodigoTIC[cont].tipologicos3);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigoFinal",arrCodigoTIC);
    }
    //GRILLA ELEMENTOS ORNAMENTALES
    var contCodigoOR=2;
    var arrCodigoOR=[];
    var arrDibujoOR=[];    
      htmlOR='<div class="col-md-12">'+                     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="ornamentales11" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+            
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-3">'+                
                    '<select class="form-control" id="ornamentales21" >'+
                       '<option value="N" selected="selected">--Seleccione--</option>'+
                       '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+ 
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="ornamentales31" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alero con canales vistos">Alero con canales vistos</option>'+
                        '<option value="alero encajonado">Alejo encajonado</option>'+
                        '<option value="alfiz">Alfiz</option>'+
                        '<option value="avitolado">Avitolado</option>'+
                        '<option value="alto relieve">Alto relieve</option>'+   
                        '<option value="hojas de acanto">Hojas de acanto</option>'+
                        '<option value="adaraja">Adaraja</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="alfeizar">Alfeizar</option>'+
                        '<option value="almohadillado">Almohadillado</option>'+  
                        '<option value="balustre">Balustre</option>'+
                        '<option value=canesillos">Canesillos</option>'+
                        '<option value="cancel">cancel</option>'+
                        '<option value="cartela">Cartela</option>'+
                        '<option value="columna adosada">Columna adosada</option>'+   
                        '<option value="columna esquinera">Columna esquinera</option>'+
                        '<option value="columna exenta">Columna exenta</option>'+
                        '<option value="corniza">Corniza</option>'+
                        '<option value="denticulos">Denticulos</option>'+                                
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaOR = '';  
    arrCodigoOR.push({ornamentales1:'N', ornamentales2:'N',ornamentales3:'N'});
    arrDibujoOR.push({scd_codigo:htmlOR,id: 1}); 
    cadenaOR=arrDibujoOR[0].scd_codigo;   
    $('#grillaOrnamentalesA').html(cadenaOR);
    $("#ornamentales11").val(arrCodigoOR[0].ornamentales1);
    $("#ornamentales21").val(arrCodigoOR[0].ornamentales2);
    $("#ornamentales31").val(arrCodigoOR[0].ornamentales3);

function crearORA(){
    var htmlOR;
        htmlOR='<br><div class="col-md-12">'+                   
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="ornamentales1'+contCodigoOR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-3">'+                
                    '<select class="form-control" id="ornamentales2'+contCodigoOR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="ornamentales3'+contCodigoOR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alero con canales vistos">Alero con canales vistos</option>'+
                        '<option value="alero encajonado">Alejo encajonado</option>'+
                        '<option value="alfiz">Alfiz</option>'+
                        '<option value="avitolado">Avitolado</option>'+
                        '<option value="alto relieve">Alto relieve</option>'+   
                        '<option value="hojas de acanto">Hojas de acanto</option>'+
                        '<option value="adaraja">Adaraja</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="alfeizar">Alfeizar</option>'+
                        '<option value="almohadillado">Almohadillado</option>'+  
                        '<option value="balustre">Balustre</option>'+
                        '<option value=canesillos">Canesillos</option>'+
                        '<option value="cancel">cancel</option>'+
                        '<option value="cartela">Cartela</option>'+
                        '<option value="columna adosada">Columna adosada</option>'+   
                        '<option value="columna esquinera">Columna esquinera</option>'+
                        '<option value="columna exenta">Columna exenta</option>'+
                        '<option value="corniza">Corniza</option>'+
                        '<option value="denticulos">Denticulos</option>'+                                
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaOR('+contCodigoOR+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaOR='';
        var cont=0;
        arrDibujoOR.forEach(function (item, index, array) 
        {
          arrCodigoOR[cont].ornamentales1 = $("#ornamentales1"+item.id+"").val();
          arrCodigoOR[cont].ornamentales2 = $("#ornamentales2"+item.id+"").val();
          arrCodigoOR[cont].ornamentales3 = $("#ornamentales3"+item.id+"").val();
          cont++;
        });
        arrCodigoOR.push({ornamentales1:'N', ornamentales2:'N',ornamentales3:'N'});
        arrDibujoOR.push({scd_codigo:htmlOR,id: contCodigoOR});
        contCodigoOR++;
        cont=1;
        for (var i=0; i<arrDibujoOR.length;i++)
        {
          cadenaOR=cadenaOR + arrDibujoOR[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#grillaOrnamentalesA').html(cadenaOR); 
        var conto=0;
        arrDibujoOR.forEach(function (item, index, array) 
        { 
          $("#ornamentales1"+item.id+"").val(arrCodigoOR[conto].ornamentales1);
          $("#ornamentales2"+item.id+"").val(arrCodigoOR[conto].ornamentales2);
          $("#ornamentales3"+item.id+"").val(arrCodigoOR[conto].ornamentales3);
          conto=conto+1;
        });
        //console.log("arrCodigoOR",arrCodigoOR);
        
    }
    function menosLineaOR($id){
        if(contCodigoOR>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaOR='';
                var cont=0;
                var pos=-1;
                arrDibujoOR.forEach(function (item, index, array) 
                {
                    arrCodigoOR[cont].ornamentales1 = $("#ornamentales1"+item.id+"").val();
                    arrCodigoOR[cont].ornamentales2 = $("#ornamentales2"+item.id+"").val();
                    arrCodigoOR[cont].ornamentales3 = $("#ornamentales3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoOR.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoOR.splice(pos,1);
                arrDibujoOR.splice(pos,1);
                //console.log("arrCodigoOR",arrCodigoOR);
                
                cont=1;
                for (var i=0; i<arrDibujoOR.length;i++)
                {
                    cadenaOR=cadenaOR +arrDibujoOR[i].scd_codigo;
                    cont++;
                }                
                $('#grillaOrnamentalesA').html(cadenaOR);
                
                cont=0;
                arrDibujoOR.forEach(function (item, index, array) 
                {
                    $("#ornamentales1"+item.id+"").val(arrCodigoOR[cont].ornamentales1);
                    $("#ornamentales2"+item.id+"").val(arrCodigoOR[cont].ornamentales2);
                    $("#ornamentales3"+item.id+"").val(arrCodigoOR[cont].ornamentales3);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigoFinal",arrCodigoOR);
    }
    //GRILLA ARTISITICOS COMPOSITIVOS
    var contCodigoAR=2;
    var arrCodigoAR=[];
    var arrDibujoAR=[];    
      htmlAR='<div class="col-md-12">'+                     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="artisticosC11" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+            
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-3">'+                
                    '<select class="form-control" id="artisticosC21" >'+
                       '<option value="N" selected="selected">--Seleccione--</option>'+
                       '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+ 
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="artisticosC31" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="Pintura en cielo(plafon)">Pintura en cielo(plafon)</option>'+
                        '<option value="pinturas murales">Pinturas murales</option>'+
                        '<option value="moldura de cielos">Moldura de cielos</option>'+
                        '<option value="Enchapes de ceramica vidriada">Enchapes de ceramica vidriada</option>'+
                        '<option value="chapas">Chapas</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAR(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaAR = '';  
    arrCodigoAR.push({artisticosC1:'N', artisticosC2:'N',artisticosC3:'N'});
    arrDibujoAR.push({scd_codigo:htmlAR,id: 1});  
    //cadenaAR=arrDibujoAR[0].scd_codigo;    
    cadenaAR='<div class="form-group"><label class="col-md-12">'+arrDibujoAR[0].scd_codigo;
    $('#grillaArtisiticoC').html(cadenaAR);
    $("#artisticosC11").val(arrCodigoAR[0].artisticosC1);
    $("#artisticosC21").val(arrCodigoAR[0].artisticosC2);
    $("#artisticosC31").val(arrCodigoAR[0].artisticosC3);

function crearART(){
    var htmlAR;
        htmlAR='<br><div class="col-md-12">'+                   
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="artisticosC1'+contCodigoAR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-3">'+                
                    '<select class="form-control" id="artisticosC2'+contCodigoAR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="artisticosC3'+contCodigoAR+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="Pintura en cielo(plafon)">Pintura en cielo(plafon)</option>'+
                        '<option value="pinturas murales">Pinturas murales</option>'+
                        '<option value="moldura de cielos">Moldura de cielos</option>'+
                        '<option value="Enchapes de ceramica vidriada">Enchapes de ceramica vidriada</option>'+
                        '<option value="chapas">Chapas</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAR('+contCodigoAR+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaAR='';
        var cont=0;
        arrDibujoAR.forEach(function (item, index, array) 
        {
          arrCodigoAR[cont].artisticosC1 = $("#artisticosC1"+item.id+"").val();
          arrCodigoAR[cont].artisticosC2 = $("#artisticosC2"+item.id+"").val();
          arrCodigoAR[cont].artisticosC3 = $("#artisticosC3"+item.id+"").val();
          cont++;
        });
        arrCodigoAR.push({artisticosC1:'N', artisticosC2:'N',artisticosC3:'N'});
        arrDibujoAR.push({scd_codigo:htmlAR,id: contCodigoAR});
        contCodigoAR++;
        cont=1;
        for (var i=0; i<arrDibujoAR.length;i++)
        {
          cadenaAR=cadenaAR + arrDibujoAR[i].scd_codigo;
          cont++;
        }       
        $('#grillaArtisiticoC').html(cadenaAR); 
        var conto=0;
        arrDibujoAR.forEach(function (item, index, array) 
        { 
          $("#artisticosC1"+item.id+"").val(arrCodigoAR[conto].artisticosC1);
          $("#artisticosC2"+item.id+"").val(arrCodigoAR[conto].artisticosC2);
          $("#artisticosC3"+item.id+"").val(arrCodigoAR[conto].artisticosC3);
          conto=conto+1;
          //console.log(arrCodigoAR, "arrCodigoAR");
        });
        //console.log("arrCodigoAR",arrCodigoAR);
    }
    function menosLineaAR($id){
        if(contCodigoAR>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaAR='';
                var cont=0;
                var pos=-1;
                arrDibujoAR.forEach(function (item, index, array) 
                {
                    arrCodigoAR[cont].artisticosC1 = $("#artisticosC1"+item.id+"").val();
                    arrCodigoAR[cont].artisticosC2 = $("#artisticosC2"+item.id+"").val();
                    arrCodigoAR[cont].artisticosC3 = $("#artisticosC3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoAR.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoAR.splice(pos,1);
                arrDibujoAR.splice(pos,1);
                //console.log("arrCodigoAR",arrCodigoAR);
                
                cont=1;
                for (var i=0; i<arrDibujoAR.length;i++)
                {
                    cadenaAR=cadenaAR +arrDibujoAR[i].scd_codigo;
                    cont++;
                }                
                $('#grillaArtisiticoC').html(cadenaAR);
                
                cont=0;
                arrDibujoAR.forEach(function (item, index, array) 
                {
                    $("#artisticosC1"+item.id+"").val(arrCodigoAR[cont].artisticosC1);
                    $("#artisticosC2"+item.id+"").val(arrCodigoAR[cont].artisticosC2);
                    $("#artisticosC3"+item.id+"").val(arrCodigoAR[cont].artisticosC3);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigoFinal",arrCodigoAR);
    }
    //GRILLA HOGARES 
    var contCodigoH=2;
    var arrCodigoHogar=[];
    var arrDibujoHogar=[];

    
      htmlHogar='<div class="col-md-12">'+
                '<div class="col-md-2">'+
                    '<input class="form-control" type="text" id="hogar11"  placeholder="hogar Descripcion"></input>'+
                '</div>'+      
                '<div class="col-md-2">'+          
                    '<select class="form-control" id="hogar21" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="0-4">0  - 4</option>'+
                        '<option value="5-10">5  - 10</option>'+
                        '<option value="11-20">11  - 20</option>'+
                        '<option value="21-40">21  - 40</option>'+
                        '<option value="41-65">41  - 65</option>'+
                        '<option value="65 mas">65 mas</option>'+              
                    '</select>'+
                '</div>'+ 
                '<div class="col-md-2">'+
                '<input type="checkbox" id="hogar31"> -   </input>'+       
                '<input type="checkbox" id="hogar41"> </input>'+ 
                '</div>'+  
                '<div class="col-md-2">'+                
                    '<select class="form-control" id="hogar51" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alquiler">Alquiler</option>'+
                        '<option value="anticresis">Anticresis</option>'+ 
                        '<option value="desconocida">Desconocida</option>'+                       
                        '<option value="prestamo">Prestamo</option>'+
                        '<option value="propia">Propia</option>'+
                        '<option value="usucapion">Usucapion</option>'+
                    '</select>'+
                '</div>'+     
                '<div class="col-md-2">'+          
                    '<select class="form-control" id="hogar61" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="ninguno">Ninguno</option>'+
                        '<option value="desconocido">Desconocido</option>'+
                        '<option value="litigio">En litigio</option>'+
                        '<option value="compartida">Prop. Compartida</option>'+
                        '<option value="saneado">Saneado</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaHogar = '';  
     arrCodigoHogar.push({hogar1:'', hogar2:'N',hogar3:'',hogar4:'',hogar5:'N',hogar6:'N'});
    arrDibujoHogar.push({scd_codigo:htmlHogar,id: 1});
    //console.log("arrCodigo",arrCodigo);
    
    cadenaHogar=arrDibujoHogar[0].scd_codigo;
    //console.log("cadenaaaa",cadenaHogar);
    $('#grillaHogares').html(cadenaHogar);
    $("#hogar11").val(arrCodigoHogar[0].hogar1);
    $("#hogar21").val(arrCodigoHogar[0].hogar2);
    //$("#hogar31").val(arrCodigoHogar[0].hogar3);
    //$("#hogar41").val(arrCodigoHogar[0].hogar4);
    document.getElementById("hogar31").checked =arrCodigoHogar[0].hogar3 ;
    document.getElementById("hogar41").checked =arrCodigoHogar[0].hogar4 ;
    $("#hogar51").val(arrCodigoHogar[0].hogar5);
    $("#hogar61").val(arrCodigoHogar[0].hogar6);

    

function crearhogar(){
    var htmlHogar;
        htmlHogar='<br><div class="col-md-12">'+
                '<div class="col-md-2">'+
                    '<input class="form-control" type="text" id="hogar1'+contCodigoH+'"  placeholder="hogar Descripcion"></input>'+
                '</div>'+      
                '<div class="col-md-2">'+          
                    '<select class="form-control" id="hogar2'+contCodigoH+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="0-4">0  - 4</option>'+
                        '<option value="5-10">5  - 10</option>'+
                        '<option value="11-20">11  - 20</option>'+
                        '<option value="21-40">21  - 40</option>'+
                        '<option value="41-65">41  - 65</option>'+
                        '<option value="65 mas">65 mas</option>'+              
                    '</select>'+
                '</div>'+ 
                '<div class="col-md-2">'+
                '<input type="checkbox" id="hogar3'+contCodigoH+'"> -   </input>'+       
                '<input type="checkbox" id="hogar4'+contCodigoH+'"> </input>'+ 
                '</div>'+  
                '<div class="col-md-2">'+                
                    '<select class="form-control" id="hogar5'+contCodigoH+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alquiler">Alquiler</option>'+
                        '<option value="anticresis">Anticresis</option>'+  
                        '<option value="desconocida">Desconocida</option>'+                       
                        '<option value="prestamo">Prestamo</option>'+
                        '<option value="propia">Propia</option>'+
                        '<option value="usucapion">Usucapion</option>'+
                    '</select>'+
                '</div>'+     
                '<div class="col-md-2">'+          
                    '<select class="form-control" id="hogar6'+contCodigoH+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="ninguno">Ninguno</option>'+ 
                        '<option value="desconocido">Desconocido</option>'+                       
                        '<option value="litigio">En litigio</option>'+
                        '<option value="compartida">Prop. Compartida</option>'+
                        '<option value="saneado">Saneado</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaHogar('+contCodigoH+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaHogar='';
        var cont=0;
        arrDibujoHogar.forEach(function (item, index, array) 
        {
          arrCodigoHogar[cont].hogar1 = $("#hogar1"+item.id+"").val();
          arrCodigoHogar[cont].hogar2 = $("#hogar2"+item.id+"").val();
          arrCodigoHogar[cont].hogar3 = document.getElementById("hogar3"+item.id+"").checked;
          arrCodigoHogar[cont].hogar4 = document.getElementById("hogar4"+item.id+"").checked;
          arrCodigoHogar[cont].hogar5 = $("#hogar5"+item.id+"").val();
          arrCodigoHogar[cont].hogar6 = $("#hogar6"+item.id+"").val();
          cont++;
        });
        arrCodigoHogar.push({hogar1:'', hogar2:'N',hogar3:'',hogar4:'',hogar5:'N',hogar6:'N'});
        arrDibujoHogar.push({scd_codigo:htmlHogar,id: contCodigoH});
        contCodigoH++;
        cont=1;
        for (var i=0; i<arrDibujoHogar.length;i++)
        {
          cadenaHogar=cadenaHogar + arrDibujoHogar[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#grillaHogares').html(cadenaHogar); 
        var conto=0;
        arrDibujoHogar.forEach(function (item, index, array) 
        { 
          $("#hogar1"+item.id+"").val(arrCodigoHogar[conto].hogar1);
          $("#hogar2"+item.id+"").val(arrCodigoHogar[conto].hogar2);        
          document.getElementById("hogar3"+item.id+"").checked =arrCodigoHogar[conto].hogar3 ;
          document.getElementById("hogar4"+item.id+"").checked =arrCodigoHogar[conto].hogar4 ;
          $("#hogar5"+item.id+"").val(arrCodigoHogar[conto].hogar5);
          $("#hogar6"+item.id+"").val(arrCodigoHogar[conto].hogar6);
          conto=conto+1;
        });
        //console.log("arrCodigoHogar",arrCodigoHogar);
    }
   function menosLineaHogar($id){
        if(contCodigoH>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaHogar='';
                var cont=0;
                var pos=-1;
                arrDibujoHogar.forEach(function (item, index, array) 
                {
                    arrCodigoHogar[cont].hogar1 = $("#hogar1"+item.id+"").val();
                    arrCodigoHogar[cont].hogar2 = $("#hogar2"+item.id+"").val();
                    arrCodigoHogar[cont].hogar3 = document.getElementById("hogar3"+item.id+"").checked;
                    arrCodigoHogar[cont].hogar4 = document.getElementById("hogar4"+item.id+"").checked;
                    arrCodigoHogar[cont].hogar5 = $("#hogar5"+item.id+"").val();
                    arrCodigoHogar[cont].hogar6 = $("#hogar6"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoHogar.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoHogar.splice(pos,1);
                arrDibujoHogar.splice(pos,1);
                //console.log("arrCodigoHogar",arrCodigoHogar);
                
                cont=1;
                for (var i=0; i<arrDibujoHogar.length;i++)
                {
                    cadenaHogar=cadenaHogar +arrDibujoHogar[i].scd_codigo;
                    cont++;
                }                
                $('#grillaHogares').html(cadenaHogar);
                
                cont=0;
                arrDibujoHogar.forEach(function (item, index, array) 
                {
                    $("#hogar1"+item.id+"").val(arrCodigoHogar[cont].hogar1);
                    $("#hogar2"+item.id+"").val(arrCodigoHogar[cont].hogar2);        
                    document.getElementById("hogar3"+item.id+"").checked =arrCodigoHogar[cont].hogar3 ;
                    document.getElementById("hogar4"+item.id+"").checked =arrCodigoHogar[cont].hogar4 ;
                    $("#hogar5"+item.id+"").val(arrCodigoHogar[cont].hogar5);
                    $("#hogar6"+item.id+"").val(arrCodigoHogar[cont].hogar6);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigoFinal",arrCodigoHogar);
    }

    /////////GRILLA GISEL///////////////// OPCION VER ELEMENTOS ESTRUCTURALES

     /////////GRILLA GISEL///////////////// OPCION VER ELEMENTOS ESTRUCTURALES bienes inmuebles

  var contPEX=2;
  var arrPEX=[];
  var arrDibujoPEX=[];

    htmlPEX='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPEX1" name="estadocPEX1">'+
            '<option value="EC" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPEX1" name="integridadPEX1">'+
            '<option value="I" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPEX1" name="tipoPEX1">'+
             '<option value="T" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPEX1" name="materialPEX1">'+
            '<option value="M" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado<">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Exteriores" onclick="darBajaPEX(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrPEX.push({val_est_con:'EC', val_int:'I', val_tipo:'T', val_mat:'M'});
    arrDibujoPEX.push({scd_codigo:htmlPEX,id: 1});
    //console.log("arrPEX",arrPEX);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoPEX[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cparedesexteriores').html(cadena);
    $("#estadocPEX1").val(arrPEX[0].val_est_con);
    $("#integridadPEX1").val(arrPEX[0].val_int);
    $("#tipoPEX1").val(arrPEX[0].val_tipo);
    $("#materialPEX1").val(arrPEX[0].val_mat);


function crearParedesExteriores(){
    var htmlPEX;
        htmlPEX='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPEX'+contPEX+'" name="estadocPEX'+contPEX+'">'+
            '<option value="EC" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPEX'+contPEX+'" name="integridadPEX'+contPEX+'">'+
            '<option value="I" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPEX'+contPEX+'" name="tipoPEX'+contPEX+'">'+
             '<option value="T" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPEX'+contPEX+'" name="materialPEX'+contPEX+'">'+
            '<option value="M" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado<">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Exteriores" onclick="darBajaPEX('+contPEX+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPEX.forEach(function (item, index, array) 
        {
          arrPEX[cont].val_est_con = $("#estadocPEX"+item.id+"").val();
          arrPEX[cont].val_int = $("#integridadPEX"+item.id+"").val();
          arrPEX[cont].val_tipo = $("#tipoPEX"+item.id+"").val();
          arrPEX[cont].val_mat = $("#materialPEX"+item.id+"").val();
          cont++;
        });
        arrPEX.push({val_est_con:'EC', val_int:'I', val_tipo:'T', val_mat:'M'});
        arrDibujoPEX.push({scd_codigo:htmlPEX,id: contPEX});
        contPEX++;
        cont=1;
        for (var i=0; i<arrDibujoPEX.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPEX[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#cparedesexteriores').html(cadena); 
        var conto=0;
        arrDibujoPEX.forEach(function (item, index, array) 
        { 
          $("#estadocPEX"+item.id+"").val(arrPEX[conto].val_est_con);
          $("#integridadPEX"+item.id+"").val(arrPEX[conto].val_int);
          $("#tipoPEX"+item.id+"").val(arrPEX[conto].val_tipo);
          $("#materialPEX"+item.id+"").val(arrPEX[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaPEX($id){
        if(contPEX>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoPEX.forEach(function (item, index, array) 
                {
                    arrPEX[cont].val_est_con=$("#estadocPEX"+item.id+"").val();
                    arrPEX[cont].val_int=$("#integridadPEX"+item.id+"").val();
                    arrPEX[cont].val_tipo=$("#tipoPEX"+item.id+"").val();
                    arrPEX[cont].val_mat=$("#materialPEX"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoPEX.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrPEX.splice(pos,1);
                arrDibujoPEX.splice(pos,1);
                //console.log("arrPEX",arrPEX);
                
                cont=1;
                for (var i=0; i<arrDibujoPEX.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPEX[i].scd_codigo;
                    cont++;
                }
                
                $('#cparedesexteriores').html(cadena);
                
                cont=0;
                arrDibujoPEX.forEach(function (item, index, array) 
                {
                    $("#estadocPEX"+item.id+"").val(arrPEX[cont].val_est_con);
                    $("#integridadPEX"+item.id+"").val(arrPEX[cont].val_int);
                    $("#tipoPEX"+item.id+"").val(arrPEX[cont].val_tipo);
                    $("#materialPEX"+item.id+"").val(arrPEX[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }

//-------------COBERTURA DEL TECHO-------------------------
    
 var contCT=2;
  var arrCT=[];
  var arrDibujoCT=[];

    htmlCT='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCT1" name="estadocCT1">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCT1" name="integridadCT1">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCT1" name="tipoCT1">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Escama de Pez">Escama de Pez</option>'+
              '<option value="Enchape">Enchape</option>'+
              '<option value="Techumbre">Techumbre</option>'+
              '<option value="Acabado">Acabado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCT1" name="materialCT1">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Calamina Plástica">Calamina Plástica</option>'+
            '<option value="Paja">Paja</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Policarbonato">Policarbonato</option>'+
            '<option value="Aceite<">Aceite</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Barro">Barro</option>'+
            '<option value="Fibro Cemento">Fibro Cemento</option>'+
            '<option value="Placa de Zinc">Placa de Zinc</option>'+
            '<option value="Hojas de Palma">Hojas de Palma</option>'+
            '<option value="Placa de Cobre">Placa de Cobre</option>'+
            '<option value="Teja Artesanal">Teja Artesanal</option>'+
            '<option value="Calamina Metálica">Calamina Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cobertura del Techo" onclick="darBajaCT(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrCT.push({val_est_con:'CE', val_inte:'CI', val_tipo:'CT', val_mate:'CM'});
    arrDibujoCT.push({scd_codigo:htmlCT,id: 1});
    //console.log("arrCT",arrCT);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoCT[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#ccoberturadeltecho').html(cadena);
    $("#estadocCT1").val(arrCT[0].val_est_con);
    $("#integridadCT1").val(arrCT[0].val_inte);
    $("#tipoCT1").val(arrCT[0].val_tipo);
    $("#materialCT1").val(arrCT[0].val_mate);

  function crearCoberturaTecho(){
    var htmlCT;
        htmlCT='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCT'+contCT+'" name="estadocCT'+contCT+'">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCT'+contCT+'" name="integridadCT'+contCT+'">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCT'+contCT+'" name="tipoCT'+contCT+'">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Escama de Pez">Escama de Pez</option>'+
              '<option value="Enchape">Enchape</option>'+
              '<option value="Techumbre">Techumbre</option>'+
              '<option value="Acabado">Acabado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCT'+contCT+'" name="materialCT'+contCT+'">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Calamina Plástica">Calamina Plástica</option>'+
            '<option value="Paja">Paja</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Policarbonato">Policarbonato</option>'+
            '<option value="Aceite<">Aceite</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Barro">Barro</option>'+
            '<option value="Fibro Cemento">Fibro Cemento</option>'+
            '<option value="Placa de Zinc">Placa de Zinc</option>'+
            '<option value="Hojas de Palma">Hojas de Palma</option>'+
            '<option value="Placa de Cobre">Placa de Cobre</option>'+
            '<option value="Teja Artesanal">Teja Artesanal</option>'+
            '<option value="Calamina Metálica">Calamina Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cobertura del Techo" onclick="darBajaCT('+contCT+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoCT.forEach(function (item, index, array) 
        {
          arrCT[cont].val_est_con = $("#estadocCT"+item.id+"").val();
          arrCT[cont].val_inte = $("#integridadCT"+item.id+"").val();
          arrCT[cont].val_tipo = $("#tipoCT"+item.id+"").val();
          arrCT[cont].val_mate = $("#materialCT"+item.id+"").val();
          cont++;
        });
        arrCT.push({val_est_con:'CE', val_inte:'CI', val_tipo:'CT', val_mate:'CM'});
        arrDibujoCT.push({scd_codigo:htmlCT,id: contCT});
        contCT++;
        cont=1;
        for (var i=0; i<arrDibujoCT.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoCT[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#ccoberturadeltecho').html(cadena); 
        var conto=0;
        arrDibujoCT.forEach(function (item, index, array) 
        { 
          $("#estadocCT"+item.id+"").val(arrCT[conto].val_est_con);
          $("#integridadCT"+item.id+"").val(arrCT[conto].val_inte);
          $("#tipoCT"+item.id+"").val(arrCT[conto].val_tipo);
          $("#materialCT"+item.id+"").val(arrCT[conto].val_mate);
          conto=conto+1;
        });
        
    }

function darBajaCT($id){
        if(contCT>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoCT.forEach(function (item, index, array) 
                {
                    arrCT[cont].val_est_con=$("#estadocCT"+item.id+"").val();
                    arrCT[cont].val_inte=$("#integridadCT"+item.id+"").val();
                    arrCT[cont].val_tipo=$("#tipoCT"+item.id+"").val();
                    arrCT[cont].val_mate=$("#materialCT"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoCT.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCT.splice(pos,1);
                arrDibujoCT.splice(pos,1);
                //console.log("arrCT",arrCT);
                
                cont=1;
                for (var i=0; i<arrDibujoCT.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoCT[i].scd_codigo;
                    cont++;
                }
                
                $('#ccoberturadeltecho').html(cadena);
                
                cont=0;
                arrDibujoCT.forEach(function (item, index, array) 
                {
                    $("#estadocCT"+item.id+"").val(arrCT[cont].val_est_con);
                    $("#integridadCT"+item.id+"").val(arrCT[cont].val_inte);
                    $("#tipoCT"+item.id+"").val(arrCT[cont].val_tipo);
                    $("#materialCT"+item.id+"").val(arrCT[cont].val_mate);
                    cont=cont+1;
                });
            });
        }
        
    }
//-----------GRILLA PARA PAREDES INTERIORES-----------

var contPI=2;
  var arrPI=[];
  var arrDibujoPI=[];

    htmlPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPI1" name="estadocPI1">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPI1" name="integridadPI1">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPI1" name="tipoPI1">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPI1" name="materialPI1">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Interiores" onclick="darBajaPI(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrPI.push({val_est_conser:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
    arrDibujoPI.push({scd_codigo:htmlPI,id: 1});
    //console.log("arrPI",arrPI);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoPI[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cparedesinteriores').html(cadena);
    $("#estadocPI1").val(arrPI[0].val_est_conser);
    $("#integridadPI1").val(arrPI[0].val_int);
    $("#tipoPI1").val(arrPI[0].val_tipo);
    $("#materialPI1").val(arrPI[0].val_mat);

function crearParedesInteriores(){
    var htmlPI;
        htmlPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPI'+contPI+'" name="estadocPI'+contPI+'">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPI'+contPI+'" name="integridadPI'+contPI+'">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPI'+contPI+'" name="tipoPI'+contPI+'">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPI'+contPI+'" name="materialPI'+contPI+'">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Interiores" onclick="darBajaPI('+contPI+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPI.forEach(function (item, index, array) 
        {
          arrPI[cont].val_est_conser = $("#estadocPI"+item.id+"").val();
          arrPI[cont].val_int = $("#integridadPI"+item.id+"").val();
          arrPI[cont].val_tipo = $("#tipoPI"+item.id+"").val();
          arrPI[cont].val_mat = $("#materialPI"+item.id+"").val();
          cont++;
        });
        arrPI.push({val_est_conser:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
        arrDibujoPI.push({scd_codigo:htmlPI,id: contPI});
        contPI++;
        cont=1;
        for (var i=0; i<arrDibujoPI.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPI[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#cparedesinteriores').html(cadena); 
        var conto=0;
        arrDibujoPI.forEach(function (item, index, array) 
        { 
          $("#estadocPI"+item.id+"").val(arrPI[conto].val_est_conser);
          $("#integridadPI"+item.id+"").val(arrPI[conto].val_int);
          $("#tipoPI"+item.id+"").val(arrPI[conto].val_tipo);
          $("#materialPI"+item.id+"").val(arrPI[conto].val_mat);
          conto=conto+1;
        });
        
    }



function darBajaPI($id){
        if(contPI>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoPI.forEach(function (item, index, array) 
                {
                    arrPI[cont].val_est_conser=$("#estadocPI"+item.id+"").val();
                    arrPI[cont].val_int=$("#integridadPI"+item.id+"").val();
                    arrPI[cont].val_tipo=$("#tipoPI"+item.id+"").val();
                    arrPI[cont].val_mat=$("#materialPI"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoPI.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrPI.splice(pos,1);
                arrDibujoPI.splice(pos,1);
                //console.log("arrPI",arrPI);
                
                cont=1;
                for (var i=0; i<arrDibujoPI.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPI[i].scd_codigo;
                    cont++;
                }
                
                $('#cparedesinteriores').html(cadena);
                
                cont=0;
                arrDibujoPI.forEach(function (item, index, array) 
                {
                    $("#estadocPI"+item.id+"").val(arrPI[cont].val_est_conser);
                    $("#integridadPI"+item.id+"").val(arrPI[cont].val_int);
                    $("#tipoPI"+item.id+"").val(arrPI[cont].val_tipo);
                    $("#materialPI"+item.id+"").val(arrPI[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }

//-------------GRILLA ENTRE PISOS---------------------------

var contEPI=2;
  var arrEPI=[];
  var arrDibujoEPI=[];

    htmlEPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEPI1" name="estadocEPI1">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEPI1" name="integridadEPI1">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEPI1" name="tipoEPI1">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Envigado">Envigado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEPI1" name="materialEPI1">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Piso Técnico">Piso Técnico</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Perfil Metálico">Perfil Metálico</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Entre Pisos" onclick="darBajaEPI(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrEPI.push({val_esta:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
    arrDibujoEPI.push({scd_codigo:htmlEPI,id: 1});
    //console.log("arrEPI",arrEPI);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoEPI[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#centrepisos').html(cadena);
    $("#estadocEPI1").val(arrEPI[0].val_esta);
    $("#integridadEPI1").val(arrEPI[0].val_int);
    $("#tipoEPI1").val(arrEPI[0].val_tipo);
    $("#materialEPI1").val(arrEPI[0].val_mat);



function crearEntrePisos(){
    var htmlEPI;
        htmlEPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEPI'+contEPI+'" name="estadocEPI'+contEPI+'">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEPI'+contEPI+'" name="integridadEPI'+contEPI+'">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEPI'+contEPI+'" name="tipoEPI'+contEPI+'">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Envigado">Envigado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEPI'+contEPI+'" name="materialEPI'+contEPI+'">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Piso Técnico">Piso Técnico</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Perfil Metálico">Perfil Metálico</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Entre Pisos" onclick="darBajaEPI('+contEPI+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoEPI.forEach(function (item, index, array) 
        {
          arrEPI[cont].val_esta = $("#estadocEPI"+item.id+"").val();
          arrEPI[cont].val_int = $("#integridadEPI"+item.id+"").val();
          arrEPI[cont].val_tipo = $("#tipoEPI"+item.id+"").val();
          arrEPI[cont].val_mat = $("#materialEPI"+item.id+"").val();
          cont++;
        });
        arrEPI.push({val_esta:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoEPI.push({scd_codigo:htmlEPI,id: contEPI});
        contEPI++;
        cont=1;
        for (var i=0; i<arrDibujoEPI.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEPI[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#centrepisos').html(cadena); 
        var conto=0;
        arrDibujoEPI.forEach(function (item, index, array) 
        { 
          $("#estadocEPI"+item.id+"").val(arrEPI[conto].val_esta);
          $("#integridadEPI"+item.id+"").val(arrEPI[conto].val_int);
          $("#tipoEPI"+item.id+"").val(arrEPI[conto].val_tipo);
          $("#materialEPI"+item.id+"").val(arrEPI[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaEPI($id){
        if(contEPI>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoEPI.forEach(function (item, index, array) 
                {
                    arrEPI[cont].val_esta=$("#estadocEPI"+item.id+"").val();
                    arrEPI[cont].val_int=$("#integridadEPI"+item.id+"").val();
                    arrEPI[cont].val_tipo=$("#tipoEPI"+item.id+"").val();
                    arrEPI[cont].val_mat=$("#materialEPI"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoEPI.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrEPI.splice(pos,1);
                arrDibujoEPI.splice(pos,1);
                //console.log("arrEPI",arrEPI);
                
                cont=1;
                for (var i=0; i<arrDibujoEPI.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEPI[i].scd_codigo;
                    cont++;
                }
                
                $('#centrepisos').html(cadena);
                
                cont=0;
                arrDibujoEPI.forEach(function (item, index, array) 
                {
                    $("#estadocEPI"+item.id+"").val(arrEPI[cont].val_esta);
                    $("#integridadEPI"+item.id+"").val(arrEPI[cont].val_int);
                    $("#tipoEPI"+item.id+"").val(arrEPI[cont].val_tipo);
                    $("#materialEPI"+item.id+"").val(arrEPI[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }

//---------GRILLA ESCALERAS------------------------

var contES=2;
  var arrES=[];
  var arrDibujoES=[];

    htmlES='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocES1" name="estadocES1">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadES1" name="integridadES1">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoES1" name="tipoES1">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Recta">Recta</option>'+
              '<option value="Imperial">Imperial</option>'+
              '<option value="Caracol">Caracol</option>'+
              '<option value="U">U</option>'+
              '<option value="L">L</option>'+
              '<option value="Elicodal">Elicodal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialES1" name="materialES1">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
            '<option value="Marmol">Marmol</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaES(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrES.push({val_estado:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
    arrDibujoES.push({scd_codigo:htmlES,id: 1});
    //console.log("arrES",arrES);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoES[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cescaleras').html(cadena);
    $("#estadocES1").val(arrES[0].val_estado);
    $("#integridadES1").val(arrES[0].val_int);
    $("#tipoES1").val(arrES[0].val_tipo);
    $("#materialES1").val(arrES[0].val_mat);

function crearEscalera(){
    var htmlES;
        htmlES='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocES'+contES+'" name="estadocES'+contES+'">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadES'+contES+'" name="integridadES'+contES+'">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoES'+contES+'" name="tipoES'+contES+'">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Recta">Recta</option>'+
              '<option value="Imperial">Imperial</option>'+
              '<option value="Caracol">Caracol</option>'+
              '<option value="U">U</option>'+
              '<option value="L">L</option>'+
              '<option value="Elicodal">Elicodal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialES'+contES+'" name="materialES'+contES+'">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
            '<option value="Marmol">Marmol</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaES('+contES+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoES.forEach(function (item, index, array) 
        {
          arrES[cont].val_estado = $("#estadocES"+item.id+"").val();
          arrES[cont].val_int = $("#integridadES"+item.id+"").val();
          arrES[cont].val_tipo = $("#tipoES"+item.id+"").val();
          arrES[cont].val_mat = $("#materialES"+item.id+"").val();
          cont++;
        });
        arrES.push({val_estado:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoES.push({scd_codigo:htmlES,id: contES});
        contES++;
        cont=1;
        for (var i=0; i<arrDibujoES.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoES[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#cescaleras').html(cadena); 
        var conto=0;
        arrDibujoES.forEach(function (item, index, array) 
        { 
          $("#estadocES"+item.id+"").val(arrES[conto].val_estado);
          $("#integridadES"+item.id+"").val(arrES[conto].val_int);
          $("#tipoES"+item.id+"").val(arrES[conto].val_tipo);
          $("#materialES"+item.id+"").val(arrES[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaES($id){
        if(contES>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoES.forEach(function (item, index, array) 
                {
                    arrES[cont].val_estado=$("#estadocES"+item.id+"").val();
                    arrES[cont].val_int=$("#integridadES"+item.id+"").val();
                    arrES[cont].val_tipo=$("#tipoES"+item.id+"").val();
                    arrES[cont].val_mat=$("#materialES"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoES.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrES.splice(pos,1);
                arrDibujoES.splice(pos,1);
                //console.log("arrES",arrES);
                
                cont=1;
                for (var i=0; i<arrDibujoES.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoES[i].scd_codigo;
                    cont++;
                }
                
                $('#cescaleras').html(cadena);
                
                cont=0;
                arrDibujoES.forEach(function (item, index, array) 
                {
                    $("#estadocES"+item.id+"").val(arrES[cont].val_estado);
                    $("#integridadES"+item.id+"").val(arrES[cont].val_int);
                    $("#tipoES"+item.id+"").val(arrES[cont].val_tipo);
                    $("#materialES"+item.id+"").val(arrES[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }

//---------------GRILLA CIELO-------------------------------
var contCIE=2;
  var arrCIE=[];
  var arrDibujoCIE=[];

    htmlCIE='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCIE1" name="estadocCIE1">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCIE1" name="integridadCIE1">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCIE1" name="tipoCIE1">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Razo">Razo</option>'+
              '<option value="Falso">Falso</option>'+
              '<option value="Vitral">Vitral</option>'+
              '<option value="Viga Vista">Viga Vista</option>'+
              '<option value="Entraquillado">Entraquillado</option>'+
              '<option value="Tumbadillo">Tumbadillo</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCIE1" name="materialCIE1">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Tela">Tela</option>'+
            '<option value="Panel">Panel</option>'+
            '<option value="Plástico">Plástico</option>'+
            '<option value="Caña hueca entortada">Caña hueca entortada</option>'+
            '<option value="Caña hueca Encalada">Caña hueca Encalada</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Madera y Estuco">Madera y Estuco</option>'+
            '<option value="Malla Metálica">Malla Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cielo" onclick="darBajaCIE(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrCIE.push({val_est:'CE', val_int:'CI', val_tipo:'CT', val_mat:'CM'});
    arrDibujoCIE.push({scd_codigo:htmlCIE,id: 1});
    //console.log("arrCIE",arrCIE);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoCIE[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#ccielo').html(cadena);
    $("#estadocCIE1").val(arrCIE[0].val_est);
    $("#integridadCIE1").val(arrCIE[0].val_int);
    $("#tipoCIE1").val(arrCIE[0].val_tipo);
    $("#materialCIE1").val(arrCIE[0].val_mat);


function crearCielo(){
    var htmlCIE;
        htmlCIE='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCIE'+contCIE+'" name="estadocCIE'+contCIE+'">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCIE'+contCIE+'" name="integridadCIE'+contCIE+'">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCIE'+contCIE+'" name="tipoCIE'+contCIE+'">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Razo">Razo</option>'+
              '<option value="Falso">Falso</option>'+
              '<option value="Vitral">Vitral</option>'+
              '<option value="Viga Vista">Viga Vista</option>'+
              '<option value="Entraquillado">Entraquillado</option>'+
              '<option value="Tumbadillo">Tumbadillo</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCIE'+contCIE+'" name="materialCIE'+contCIE+'">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Tela">Tela</option>'+
            '<option value="Panel">Panel</option>'+
            '<option value="Plástico">Plástico</option>'+
            '<option value="Caña hueca entortada">Caña hueca entortada</option>'+
            '<option value="Caña hueca Encalada">Caña hueca Encalada</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Madera y Estuco">Madera y Estuco</option>'+
            '<option value="Malla Metálica">Malla Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cielo" onclick="darBajaCIE('+contCIE+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoCIE.forEach(function (item, index, array) 
        {
          arrCIE[cont].val_est = $("#estadocCIE"+item.id+"").val();
          arrCIE[cont].val_int = $("#integridadCIE"+item.id+"").val();
          arrCIE[cont].val_tipo = $("#tipoCIE"+item.id+"").val();
          arrCIE[cont].val_mat = $("#materialCIE"+item.id+"").val();
          cont++;
        });
        arrCIE.push({val_est:'CE', val_int:'CI', val_tipo:'CT', val_mat:'CM'});
        arrDibujoCIE.push({scd_codigo:htmlCIE,id: contCIE});
        contCIE++;
        cont=1;
        for (var i=0; i<arrDibujoCIE.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoCIE[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#ccielo').html(cadena); 
        var conto=0;
        arrDibujoCIE.forEach(function (item, index, array) 
        { 
          $("#estadocCIE"+item.id+"").val(arrCIE[conto].val_est);
          $("#integridadCIE"+item.id+"").val(arrCIE[conto].val_int);
          $("#tipoCIE"+item.id+"").val(arrCIE[conto].val_tipo);
          $("#materialCIE"+item.id+"").val(arrCIE[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaCIE($id){
        if(contCIE>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoCIE.forEach(function (item, index, array) 
                {
                    arrCIE[cont].val_est=$("#estadocCIE"+item.id+"").val();
                    arrCIE[cont].val_int=$("#integridadCIE"+item.id+"").val();
                    arrCIE[cont].val_tipo=$("#tipoCIE"+item.id+"").val();
                    arrCIE[cont].val_mat=$("#materialCIE"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoCIE.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCIE.splice(pos,1);
                arrDibujoCIE.splice(pos,1);
                //console.log("arrCIE",arrCIE);
                
                cont=1;
                for (var i=0; i<arrDibujoCIE.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoCIE[i].scd_codigo;
                    cont++;
                }
                
                $('#ccielo').html(cadena);
                
                cont=0;
                arrDibujoCIE.forEach(function (item, index, array) 
                {
                    $("#estadocCIE"+item.id+"").val(arrCIE[cont].val_est);
                    $("#integridadCIE"+item.id+"").val(arrCIE[cont].val_int);
                    $("#tipoCIE"+item.id+"").val(arrCIE[cont].val_tipo);
                    $("#materialCIE"+item.id+"").val(arrCIE[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }

//--------------GRILLA BALCONES----------------------------
var contBAL=2;
  var arrBAL=[];
  var arrDibujoBAL=[];

    htmlBAL='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocBAL1" name="estadocBAL1">'+
            '<option value="BE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadBAL1" name="integridadBAL1">'+
            '<option value="BI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoBAL1" name="tipoBAL1">'+
             '<option value="BT" selected="selected">--Seleccione--</option>'+
              '<option value="De cajon">De cajon</option>'+
              '<option value="Abierto">Abierto</option>'+
              '<option value="Corrido">Corrido</option>'+
              '<option value="Balconcillo">Balconcillo</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialBAL1" name="materialBAL1">'+
            '<option value="BM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Balcones" onclick="darBajaBAL(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrBAL.push({val_est:'BE', val_inte:'BI', val_tipo:'BT', val_mat:'BM'});
    arrDibujoBAL.push({scd_codigo:htmlBAL,id: 1});
    //console.log("arrBAL",arrBAL);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoBAL[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cbalcones').html(cadena);
    $("#estadocBAL1").val(arrBAL[0].val_est);
    $("#integridadBAL1").val(arrBAL[0].val_inte);
    $("#tipoBAL1").val(arrBAL[0].val_tipo);
    $("#materialBAL1").val(arrBAL[0].val_mat);

function crearBalcones(){
    var htmlBAL;
        htmlBAL='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocBAL'+contBAL+'" name="estadocBAL'+contBAL+'">'+
            '<option value="BE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadBAL'+contBAL+'" name="integridadBAL'+contBAL+'">'+
            '<option value="BI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoBAL'+contBAL+'" name="tipoBAL'+contBAL+'">'+
             '<option value="BT" selected="selected">--Seleccione--</option>'+
              '<option value="De cajon">De cajon</option>'+
              '<option value="Abierto">Abierto</option>'+
              '<option value="Corrido">Corrido</option>'+
              '<option value="Balconcillo">Balconcillo</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialBAL'+contBAL+'" name="materialBAL'+contBAL+'">'+
            '<option value="BM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Balcones" onclick="darBajaBAL('+contBAL+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoBAL.forEach(function (item, index, array) 
        {
          arrBAL[cont].val_est = $("#estadocBAL"+item.id+"").val();
          arrBAL[cont].val_inte = $("#integridadBAL"+item.id+"").val();
          arrBAL[cont].val_tipo = $("#tipoBAL"+item.id+"").val();
          arrBAL[cont].val_mat = $("#materialBAL"+item.id+"").val();
          cont++;
        });
        arrBAL.push({val_est:'BE', val_inte:'BI', val_tipo:'BT', val_mat:'BM'});
        arrDibujoBAL.push({scd_codigo:htmlBAL,id: contBAL});
        contBAL++;
        cont=1;
        for (var i=0; i<arrDibujoBAL.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoBAL[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#cbalcones').html(cadena); 
        var conto=0;
        arrDibujoBAL.forEach(function (item, index, array) 
        { 
          $("#estadocBAL"+item.id+"").val(arrBAL[conto].val_est);
          $("#integridadBAL"+item.id+"").val(arrBAL[conto].val_inte);
          $("#tipoBAL"+item.id+"").val(arrBAL[conto].val_tipo);
          $("#materialBAL"+item.id+"").val(arrBAL[conto].val_mat);
          conto=conto+1;
        });
        
    }


function darBajaBAL($id){
        if(contBAL>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoBAL.forEach(function (item, index, array) 
                {
                    arrBAL[cont].val_est=$("#estadocBAL"+item.id+"").val();
                    arrBAL[cont].val_inte=$("#integridadBAL"+item.id+"").val();
                    arrBAL[cont].val_tipo=$("#tipoBAL"+item.id+"").val();
                    arrBAL[cont].val_mat=$("#materialBAL"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoBAL.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrBAL.splice(pos,1);
                arrDibujoBAL.splice(pos,1);
                //console.log("arrBAL",arrBAL);
                
                cont=1;
                for (var i=0; i<arrDibujoBAL.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoBAL[i].scd_codigo;
                    cont++;
                }
                
                $('#cbalcones').html(cadena);
                
                cont=0;
                arrDibujoBAL.forEach(function (item, index, array) 
                {
                    $("#estadocBAL"+item.id+"").val(arrBAL[cont].val_est);
                    $("#integridadBAL"+item.id+"").val(arrBAL[cont].val_inte);
                    $("#tipoBAL"+item.id+"").val(arrBAL[cont].val_tipo);
                    $("#materialBAL"+item.id+"").val(arrBAL[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
//--------------ESTRUCTURA CUBIERTA----------------------

var contEC=2;
  var arrEC=[];
  var arrDibujoEC=[];

    htmlECU='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEC1" name="estadocEC1">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEC1" name="integridadEC1">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEC1" name="tipoEC1">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha y Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEC1" name="materialEC1">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Perfil de Acero">Perfil de Acero</option>'+
            '<option value="Perfil de Aluminio">Perfil de Aluminio</option>'+
            '<option value="Obra de Ladrillo">Obra de Ladrillo</option>'+
            '<option value="Obra de Adobe">Obra de Adobe</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Estructura Cubierta" onclick="darBajaEC(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrEC.push({val_est:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
    arrDibujoEC.push({scd_codigo:htmlECU,id: 1});
    //console.log("arrEC",arrEC);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoEC[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cestructuracubierta').html(cadena);
    $("#estadocEC1").val(arrEC[0].val_est);
    $("#integridadEC1").val(arrEC[0].val_int);
    $("#tipoEC1").val(arrEC[0].val_tipo);
    $("#materialEC1").val(arrEC[0].val_mat);


function crearEstructuraCubierta(){
    var htmlECU;
        htmlECU='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEC'+contEC+'" name="estadocEC'+contEC+'">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEC'+contEC+'" name="integridadEC'+contEC+'">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEC'+contEC+'" name="tipoEC'+contEC+'">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha y Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEC'+contEC+'" name="materialEC'+contEC+'">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Perfil de Acero">Perfil de Acero</option>'+
            '<option value="Perfil de Aluminio">Perfil de Aluminio</option>'+
            '<option value="Obra de Ladrillo">Obra de Ladrillo</option>'+
            '<option value="Obra de Adobe">Obra de Adobe</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Estructura Cubierta" onclick="darBajaEC('+contEC+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoEC.forEach(function (item, index, array) 
        {
          arrEC[cont].val_est = $("#estadocEC"+item.id+"").val();
          arrEC[cont].val_int = $("#integridadEC"+item.id+"").val();
          arrEC[cont].val_tipo = $("#tipoEC"+item.id+"").val();
          arrEC[cont].val_mat = $("#materialEC"+item.id+"").val();
          cont++;
        });
        arrEC.push({val_est:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoEC.push({scd_codigo:htmlECU,id: contEC});
        contEC++;
        cont=1;
        for (var i=0; i<arrDibujoEC.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEC[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#cestructuracubierta').html(cadena); 
        var conto=0;
        arrDibujoEC.forEach(function (item, index, array) 
        { 
          $("#estadocEC"+item.id+"").val(arrEC[conto].val_est);
          $("#integridadEC"+item.id+"").val(arrEC[conto].val_int);
          $("#tipoEC"+item.id+"").val(arrEC[conto].val_tipo);
          $("#materialEC"+item.id+"").val(arrEC[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaEC($id){
        if(contEC>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoEC.forEach(function (item, index, array) 
                {
                    arrEC[cont].val_est=$("#estadocEC"+item.id+"").val();
                    arrEC[cont].val_int=$("#integridadEC"+item.id+"").val();
                    arrEC[cont].val_tipo=$("#tipoEC"+item.id+"").val();
                    arrEC[cont].val_mat=$("#materialEC"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoEC.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrEC.splice(pos,1);
                arrDibujoEC.splice(pos,1);
                //console.log("arrEC",arrEC);
                
                cont=1;
                for (var i=0; i<arrDibujoEC.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEC[i].scd_codigo;
                    cont++;
                }
                
                $('#cestructuracubierta').html(cadena);
                
                cont=0;
                arrDibujoEC.forEach(function (item, index, array) 
                {
                    $("#estadocEC"+item.id+"").val(arrEC[cont].val_est);
                    $("#integridadEC"+item.id+"").val(arrEC[cont].val_int);
                    $("#tipoEC"+item.id+"").val(arrEC[cont].val_tipo);
                    $("#materialEC"+item.id+"").val(arrEC[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
//------------------GRILLA PISOS-----------------------    
var contP=2;
  var arrP=[];
  var arrDibujoP=[];

    htmlPISO='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocP1" name="estadocP1">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadP1" name="integridadP1">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoP1" name="tipoP1">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha o Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialP1" name="materialP1">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Mosaico">Mosaico</option>'+
            '<option value="Loseta">Loseta</option>'+
            '<option value="Piedra Huevillo">Piedra Huevillo</option>'+
            '<option value="Piedra Manzana">Piedra Manzana</option>'+
            '<option value="Céramica">Céramica</option>'+
            '<option value="Obra de Adobe">Marmol</option>'+
            '<option value="Marmol">Madera Machiembre</option>'+
            '<option value="Madera Entablonado">Madera Entablonado</option>'+
            '<option value="Madera Parquet">Madera Parquet</option>'+
            '<option value="Revestimiento de Porcelanato">Revestimiento de Porcelanato</option>'+
            '<option value="Revestimiento de Vinil">Revestimiento de Vinil</option>'+
            '<option value="Revestimiento de Alfombra">Revestimiento de Alfombra</option>'+
            '<option value="Vitroblok">Vitroblok</option>'+
            '<option value="Azulejo">Azulejo</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Piedra Ormamental">Piedra Ormamental</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Pisos" onclick="darBajaPisos(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrP.push({val_est:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
    arrDibujoP.push({scd_codigo:htmlPISO,id: 1});
    //console.log("arrP",arrP);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoP[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#cpisos').html(cadena);
    $("#estadocP1").val(arrP[0].val_est);
    $("#integridadP1").val(arrP[0].val_int);
    $("#tipoP1").val(arrP[0].val_tipo);
    $("#materialP1").val(arrP[0].val_mat);


function crearPisos(){
    var htmlPISO;
        htmlPISO='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocP'+contP+'" name="estadocP'+contP+'">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadP'+contP+'" name="integridadP'+contP+'">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoP'+contP+'" name="tipoP'+contP+'">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha o Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialP'+contP+'" name="materialP'+contP+'">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Mosaico">Mosaico</option>'+
            '<option value="Loseta">Loseta</option>'+
            '<option value="Piedra Huevillo">Piedra Huevillo</option>'+
            '<option value="Piedra Manzana">Piedra Manzana</option>'+
            '<option value="Céramica">Céramica</option>'+
            '<option value="Obra de Adobe">Marmol</option>'+
            '<option value="Marmol">Madera Machiembre</option>'+
            '<option value="Madera Entablonado">Madera Entablonado</option>'+
            '<option value="Madera Parquet">Madera Parquet</option>'+
            '<option value="Revestimiento de Porcelanato">Revestimiento de Porcelanato</option>'+
            '<option value="Revestimiento de Vinil">Revestimiento de Vinil</option>'+
            '<option value="Revestimiento de Alfombra">Revestimiento de Alfombra</option>'+
            '<option value="Vitroblok">Vitroblok</option>'+
            '<option value="Azulejo">Azulejo</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Piedra Ormamental">Piedra Ormamental</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Pisos" onclick="darBajaPisos('+contP+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoP.forEach(function (item, index, array) 
        {
          arrP[cont].val_est = $("#estadocP"+item.id+"").val();
          arrP[cont].val_int = $("#integridadP"+item.id+"").val();
          arrP[cont].val_tipo = $("#tipoP"+item.id+"").val();
          arrP[cont].val_mat = $("#materialP"+item.id+"").val();
          cont++;
        });
        arrP.push({val_est:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
        arrDibujoP.push({scd_codigo:htmlPISO,id: contP});
        contP++;
        cont=1;
        for (var i=0; i<arrDibujoP.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoP[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#cpisos').html(cadena); 
        var conto=0;
        arrDibujoP.forEach(function (item, index, array) 
        { 
          $("#estadocP"+item.id+"").val(arrP[conto].val_est);
          $("#integridadP"+item.id+"").val(arrP[conto].val_int);
          $("#tipoP"+item.id+"").val(arrP[conto].val_tipo);
          $("#materialP"+item.id+"").val(arrP[conto].val_mat);
          conto=conto+1;
        });
        
    }
function darBajaPisos($id){
        if(contP>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoP.forEach(function (item, index, array) 
                {
                    arrP[cont].val_est=$("#estadocP"+item.id+"").val();
                    arrP[cont].val_int=$("#integridadP"+item.id+"").val();
                    arrP[cont].val_tipo=$("#tipoP"+item.id+"").val();
                    arrP[cont].val_mat=$("#materialP"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoP.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrP.splice(pos,1);
                arrDibujoP.splice(pos,1);
                //console.log("arrP",arrP);
                
                cont=1;
                for (var i=0; i<arrDibujoP.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoP[i].scd_codigo;
                    cont++;
                }
                
                $('#cpisos').html(cadena);
                
                cont=0;
                arrDibujoP.forEach(function (item, index, array) 
                {
                    $("#estadocP"+item.id+"").val(arrP[cont].val_est);
                    $("#integridadP"+item.id+"").val(arrP[cont].val_int);
                    $("#tipoP"+item.id+"").val(arrP[cont].val_tipo);
                    $("#materialP"+item.id+"").val(arrP[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }

//-------FIN GRILLAS PARA BIENES INMUEBLES BLOQUES----------



    /////////GRILLA HEIDE///////////////// GRILLA LOS PRIMEROS 6 GRILLA 

    

    /////////GRILLA TANIA///////////////// GRILLAS OPCION VER ELEMENTOS COMPOSITIVOS


//INICIO HTML TANYA
/******************-------Vanos Ventanas Interiores**********************/
    var contCodigoEleComVanosVenInteriores=2;
    var arrCodigoEleComVanosVenInteriores=[];
    var arrDibujoEleComVanosVenInteriores=[];

     htmlEleComVanosVenInteriores='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_int_estado1" name="vent_int_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_int_inte1" name="vent_int_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_int_tipo1" name="vent_int_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_int_mat1" name="vent_int_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ven_int(1);"></i>'+
    '</a></div>';
    var cadenaEleComVanosVenInteriores = '';  

    arrCodigoEleComVanosVenInteriores.push({arg_vent_int_estado:'N', arg_vent_int_inte:'N', arg_vent_int_tipo:'N', arg_vent_int_mat:'N'});  
    arrDibujoEleComVanosVenInteriores.push({scd_codigo:htmlEleComVanosVenInteriores,id: 1});
    //console.log("arrCodigo",arrCodigoEleComVanosVenInteriores);
        
    cadenaEleComVanosVenInteriores='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujoEleComVanosVenInteriores[0].scd_codigo;
    ////console.log("cadenaaaa",cadenaEleComVanosVenInteriores);
    $('#divelcoVanosVentanasInteriores').html(cadenaEleComVanosVenInteriores);
    $("#vent_int_estado1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_estado);
    $("#vent_int_inte1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_inte);
    $("#vent_int_tipo1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_tipo);
    $("#vent_int_mat1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_mat);
    //$('#grillaPrueba').val(JSON.stringify(arrCodigoEleComVanosVenInteriores));

    function elcoVanosVentanasInteriores(){
    
    var htmlEleComVanosVenInteriores;
    htmlEleComVanosVenInteriores='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_int_estado'+contCodigoEleComVanosVenInteriores+'" name="vent_int_estado'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_int_inte'+contCodigoEleComVanosVenInteriores+'" name="vent_int_inte'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_int_tipo'+contCodigoEleComVanosVenInteriores+'" name="vent_int_tipo'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_int_mat'+contCodigoEleComVanosVenInteriores+'" name="vent_int_mat'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ven_int('+contCodigoEleComVanosVenInteriores+');"></i>'+
    '</a></div>';
        //RECUPERA VALORES EN LA GRILLA
        var cadenaEleComVanosVenInteriores='';
        var cont=0;
        arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
        {
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_estado = $("#vent_int_estado"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_inte = $("#vent_int_inte"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_tipo = $("#vent_int_tipo"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_mat = $("#vent_int_mat"+item.id+"").val();
          cont++;
        });

        arrCodigoEleComVanosVenInteriores.push({arg_vent_int_estado:'N', arg_vent_int_inte:'N', arg_vent_int_tipo:'N', arg_vent_int_mat:'N'});  
        arrDibujoEleComVanosVenInteriores.push({scd_codigo:htmlEleComVanosVenInteriores,id: contCodigoEleComVanosVenInteriores});
     
        contCodigoEleComVanosVenInteriores++;
        cont=1;
        //CONCATENA HTML GRILLA EN LA VISTA
        //console.log(arrDibujoEleComVanosVenInteriores.length);
        for (var i=0; i<arrDibujoEleComVanosVenInteriores.length;i++)
        {
          cadenaEleComVanosVenInteriores=cadenaEleComVanosVenInteriores +'<div class="form-group col-md-3"><label class="control-label">'+cont+'.'+arrDibujoEleComVanosVenInteriores[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadenaEleComVanosVenInteriores);
        $('#divelcoVanosVentanasInteriores').html(cadenaEleComVanosVenInteriores);

        //MUESTRA VALORES EN LA GRILLA
        var conto=0;
        arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
        { 
          $("#vent_int_estado"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_estado);
          $("#vent_int_inte"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_inte);
          $("#vent_int_tipo"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_tipo);
          $("#vent_int_mat"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_mat);
          conto=conto+1;
        });
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo));
    }
    function menosLinea_ven_int($id){
        if(contCodigoEleComVanosVenInteriores>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaEleComVanosVenInteriores='';
                var cont=0;
                var pos=-1;
                arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
                {
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_estado = $("#vent_int_estado"+item.id+"").val();
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_inte = $("#vent_int_inte"+item.id+"").val();
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_tipo = $("#vent_int_tipo"+item.id+"").val();
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_mat = $("#vent_int_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoEleComVanosVenInteriores.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoEleComVanosVenInteriores.splice(pos,1);
                arrDibujoEleComVanosVenInteriores.splice(pos,1);
                ////console.log("arrCodigo",arrCodigoEleComVanosVenInteriores);
                cont=1;
                for (var i=0; i<arrDibujoEleComVanosVenInteriores.length;i++)
                {
                    cadenaEleComVanosVenInteriores=cadenaEleComVanosVenInteriores + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEleComVanosVenInteriores[i].scd_codigo;
                    cont++;
                }                
                $('#divelcoVanosVentanasInteriores').html(cadenaEleComVanosVenInteriores);
                cont=0;
                arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
                {
                    $("#vent_int_estado"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_estado);
                    $("#vent_int_inte"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_inte);
                    $("#vent_int_tipo"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_tipo);
                    $("#vent_int_mat"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_mat);
                    //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigoFinal",arrCodigoEleComVanosVenInteriores);
    }

/******************-------Ventanas Interiores**********************/

/******************-------Ventanas Interiores**********************/
    var contCodigovent_inte_es_in=2;
    var arrCodigovent_inte_es_in=[];
    var arrDibujovent_inte_es_in=[];

    htmlARGvent_inte_es_in='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_inte_estado1" name="vent_inte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_inte_inte1" name="vent_inte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_inte_tipo1" name="vent_inte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+                     
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_inte_mat1" name="vent_inte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineavent_inte_es_in(1);"></i>'+
    '</a></div>';
    var cadenavent_inte_es_in = '';  
    arrCodigovent_inte_es_in.push({arg_vent_inte_estado:'N', arg_vent_inte_inte:'N', arg_vent_inte_tipo:'N', arg_vent_inte_mat:'N'});
    arrDibujovent_inte_es_in.push({scd_codigo:htmlARGvent_inte_es_in,id: 1});
    //console.log("arrCodigovent_inte_es_in",arrCodigovent_inte_es_in);
    
    cadenavent_inte_es_in='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujovent_inte_es_in[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divelcoVentanasInteriores').html(cadenavent_inte_es_in);
    $("#vent_inte_estado1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_estado);
    $("#vent_inte_inte1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_inte);
    $("#vent_inte_tipo1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_tipo);
    $("#vent_inte_mat1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_mat);

    function elcoVentanasInteriores(){
    var htmlARGvent_inte_es_in;
        htmlARGvent_inte_es_in='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_inte_estado'+contCodigovent_inte_es_in+'" name="vent_inte_estado'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_inte_inte'+contCodigovent_inte_es_in+'" name="vent_inte_inte'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_inte_tipo'+contCodigovent_inte_es_in+'" name="vent_inte_tipo'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+                     
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_inte_mat'+contCodigovent_inte_es_in+'" name="vent_inte_mat'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineavent_inte_es_in('+contCodigovent_inte_es_in+');"></i>'+
    '</a></div>';
        var cadenavent_inte_es_in='';
        var cont=0;
        arrDibujovent_inte_es_in.forEach(function (item, index, array) 
        {
          arrCodigovent_inte_es_in[cont].arg_vent_inte_estado = $("#vent_inte_estado"+item.id+"").val();
          arrCodigovent_inte_es_in[cont].arg_vent_inte_inte = $("#vent_inte_inte"+item.id+"").val();
          arrCodigovent_inte_es_in[cont].arg_vent_inte_tipo = $("#vent_inte_tipo"+item.id+"").val();
          arrCodigovent_inte_es_in[cont].arg_vent_inte_mat = $("#vent_inte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigovent_inte_es_in.push({arg_vent_inte_estado:'N', arg_vent_inte_inte:'N', arg_vent_inte_tipo:'N', arg_vent_inte_mat:'N'});
        arrDibujovent_inte_es_in.push({scd_codigo:htmlARGvent_inte_es_in,id: contCodigovent_inte_es_in});
        contCodigovent_inte_es_in++;
        cont=1;
        for (var i=0; i<arrDibujovent_inte_es_in.length; i++)
        {
          cadenavent_inte_es_in=cadenavent_inte_es_in + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujovent_inte_es_in[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        
         $('#divelcoVentanasInteriores').html(cadenavent_inte_es_in); 
        var conto=0;
        arrDibujovent_inte_es_in.forEach(function (item, index, array) 
        { 
          $("#vent_inte_estado"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_estado);
          $("#vent_inte_inte"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_inte);
          $("#vent_inte_tipo"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_tipo);
          $("#vent_inte_mat"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigovent_inte_es_in));
    }

 
    function menosLineavent_inte_es_in($id){
      alert(contCodigovent_inte_es_in);
        if(contCodigovent_inte_es_in>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenavent_inte_es_in='';
                var cont=0;
                var pos=-1;
                arrDibujovent_inte_es_in.forEach(function (item, index, array) 
                {
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_estado = $("#vent_inte_estado"+item.id+"").val();
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_inte = $("#vent_inte_inte"+item.id+"").val();
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_tipo = $("#vent_inte_tipo"+item.id+"").val();
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_mat = $("#vent_inte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujovent_inte_es_in.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigovent_inte_es_in.splice(pos,1);
                arrDibujovent_inte_es_in.splice(pos,1);
                //console.log("arrCodigovent_inte_es_in",arrCodigovent_inte_es_in);
                
                cont=1;
                for (var i=0; i<arrDibujovent_inte_es_in.length;i++)
                {
                    cadenavent_inte_es_in=cadenavent_inte_es_in + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujovent_inte_es_in[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVentanasInteriores').html(cadenavent_inte_es_in);
                
                cont=0;
                arrDibujovent_inte_es_in.forEach(function (item, index, array) 
                {   
                    $("#vent_inte_estado"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_estado);
                    $("#vent_inte_inte"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_inte);
                    $("#vent_inte_tipo"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_tipo);
                    $("#vent_inte_mat"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_mat);
                   ;//document.getElementById("tipoarg"+item.id+"").value(arrCodigovent_inte_es_in[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigovent_inte_es_inFinal",arrCodigovent_inte_es_in);
    }
 

 /******************-------Vanos Ventanas Exteriores**********************/
//PTR_VAN_VENT_EXT
 var contCodigoVanVenExt_es_int=2;
    var arrCodigoVanVenExt_es_int=[];
    var arrDibujoVanVenExt_es_int=[];
    htmlARGVanVenExt_es_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_ext_estado1" name="vent_ext_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_ext_inte1" name="vent_ext_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_ext_tipo1" name="vent_ext_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_ext_mat1" name="vent_ext_mat">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaVanVenExt_es_int(1);"></i>'+
    '</a></div>';
    var cadenaVanVenExt_es_int= '';  
    arrCodigoVanVenExt_es_int.push({arg_vent_ext_estado:'N', arg_vent_ext_inte:'N', arg_vent_ext_tipo:'N', arg_vent_ext_mat:'N'});
    arrDibujoVanVenExt_es_int.push({scd_codigo:htmlARGVanVenExt_es_int,id: 1});
    //console.log("arrCodigoVanVenExt_es_int",arrCodigoVanVenExt_es_int);
    
    cadenaVanVenExt_es_int='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujoVanVenExt_es_int[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divelcoVanosVentanasExteriores').html(cadenaVanVenExt_es_int);

    $("#vent_ext_estado1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_estado);
    $("#vent_ext_inte1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_inte);
    $("#vent_ext_tipo1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_tipo);
    $("#vent_ext_mat1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_mat);
   
    $('#grillaPrueba').val(JSON.stringify(arrCodigoVanVenExt_es_int));

    function elcoVanosVentanasExteriores(){

        var htmlARGVanVenExt_es_int;
        htmlARGVanVenExt_es_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_ext_estado1'+contCodigoVanVenExt_es_int+'" name="vent_ext_estado'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_ext_inte1'+contCodigoVanVenExt_es_int+'" name="vent_ext_inte'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_ext_tipo1'+contCodigoVanVenExt_es_int+'" name="vent_ext_inte'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_ext_mat1'+contCodigoVanVenExt_es_int+'" name="vent_ext_mat'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaVanVenExt_es_int('+contCodigoVanVenExt_es_int+');"></i>'+
    '</a></div>';
        var cadenaVanVenExt_es_int='';
        var cont=0;
        arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
        {
          arrCodigoVanVenExt_es_int[cont].arg_vent_ext_estado = $("#vent_ext_estado"+item.id+"").val();
          arrCodigoVanVenExt_es_int[cont].arg_vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
          arrCodigoVanVenExt_es_int[cont].vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
          arrCodigoVanVenExt_es_int[cont].vent_ext_mat = $("#vent_ext_mat"+item.id+"").val();

          cont++;
        });
        arrCodigoVanVenExt_es_int.push({arg_vent_ext_estado:'N', arg_vent_ext_inte:'N', arg_vent_ext_tipo:'N', arg_vent_ext_mat:'N'});
        arrDibujoVanVenExt_es_int.push({scd_codigo:htmlARGVanVenExt_es_int,id: contCodigoVanVenExt_es_int});
        contCodigoVanVenExt_es_int++;
        cont=1;
        for (var i=0; i<arrDibujoVanVenExt_es_int.length;i++)
        {
          cadenaVanVenExt_es_int=cadenaVanVenExt_es_int + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVanVenExt_es_int[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#divelcoVanosVentanasExteriores').html(cadenaVanVenExt_es_int); 
        var conto=0;
        arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
        { 
          $("#vent_ext_estado"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].arg_vent_ext_estado);
          $("#vent_ext_tipo"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].arg_vent_ext_tipo);
          $("#vent_ext_tipo"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].vent_ext_tipo);
          $("#vent_ext_mat"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].vent_ext_mat);
   


          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigoVanVenExt_es_int));
    }

 
    function menosLineaVanVenExt_es_int($id){
        if(contCodigoVanVenExt_es_int>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
                {
                  
                    arrCodigoVanVenExt_es_int[cont].arg_vent_ext_estado = $("#vent_ext_estado"+item.id+"").val();
                    arrCodigoVanVenExt_es_int[cont].arg_vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
                    arrCodigoVanVenExt_es_int[cont].vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
                    arrCodigoVanVenExt_es_int[cont].vent_ext_mat = $("#vent_ext_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoVanVenExt_es_int.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoVanVenExt_es_int.splice(pos,1);
                arrDibujoVanVenExt_es_int.splice(pos,1);
                //console.log("arrCodigoVanVenExt_es_int",arrCodigoVanVenExt_es_int);
                
                cont=1;
                for (var i=0; i<arrDibujoVanVenExt_es_int.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVanVenExt_es_int[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVanosVentanasExteriores').html(cadena);
                
                cont=0;
                arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
                {
                   
                     $("#vent_ext_estado"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].arg_vent_ext_estado);
                      $("#vent_ext_tipo"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].arg_vent_ext_tipo);
                      $("#vent_ext_tipo"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].vent_ext_tipo);
                      $("#vent_ext_mat"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].vent_ext_mat);//document.getElementById("tipoarg"+item.id+"").value(arrCodigoVanVenExt_es_int[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigoVanVenExt_es_intFinal",arrCodigoVanVenExt_es_int);
    }

    
/******************-------Ventanas Exteriores**********************/
/******************-------Ventanas Exteriores**********************/
//no
    var contCodigo_ptr_vent_ext=2;
    var arrCodigo_ptr_vent_ext=[];
    var arrDibujo_ptr_vent_ext=[];

    htmlARG_ptr_vent_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_exte_estado1" name="vent_exte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_exte_inte1" name="vent_exte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_exte_tipo1" name="vent_exte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_exte_mat1" name="vent_exte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio Claro">Vidrio Claro</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_vent_ext(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_vent_ext.push({arg_vent_exte_estado:'N', vent_exte_inte:'N',vent_exte_tipo:'N',vent_exte_mat:'N' });
    arrDibujo_ptr_vent_ext.push({scd_codigo:htmlARG_ptr_vent_ext,id: 1});
    //console.log("arrCodigo_ptr_vent_ext",arrCodigo_ptr_vent_ext);
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_vent_ext[0].scd_codigo;
    
    ////console.log("cadenaaaa",cadena);
    $('#divelcoVentanasExteriores').html(cadena);
    $("#vent_exte_estado1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_estado);
    $("#vent_exte_inte1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_inte);
    $("#vent_exte_tipo1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_tipo);
    $("#vent_exte_mat1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_mat);


    function elcoVentanasExteriores(){
    var htmlARG_ptr_vent_ext;
        htmlARG_ptr_vent_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_exte_estado'+contCodigo_ptr_vent_ext+'" name="vent_exte_estado'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_exte_inte'+contCodigo_ptr_vent_ext+'" name="vent_exte_inte'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_exte_tipo'+contCodigo_ptr_vent_ext+'" name="vent_exte_tipo'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_exte_mat'+contCodigo_ptr_vent_ext+'" name="vent_exte_mat'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio Claro">Vidrio Claro</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_vent_ext('+contCodigo_ptr_vent_ext+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_estado = $("#vent_exte_estado"+item.id+"").val();
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_inte = $("#vent_exte_inte"+item.id+"").val();
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_tipo = $("#vent_exte_tipo"+item.id+"").val();
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_mat = $("#vent_exte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_vent_ext.push({arg_vent_exte_estado:'N', vent_exte_inte:'N',vent_exte_tipo:'N',vent_exte_mat:'N' });
        arrDibujo_ptr_vent_ext.push({scd_codigo:htmlARG_ptr_vent_ext,id: contCodigo_ptr_vent_ext});
        contCodigo_ptr_vent_ext++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_vent_ext.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_vent_ext[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#divelcoVentanasExteriores').html(cadena); 
        var conto=0;
        arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
        { 
          $("#vent_exte_estado"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_estado);
          $("#vent_exte_inte"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_inte);
          $("#vent_exte_tipo"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_tipo);
          $("#vent_exte_mat"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_mat);

          conto=conto+1;
        });
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_vent_ext));
    }

 
    function menosLinea_ptr_vent_ext($id){
        if(contCodigo_ptr_vent_ext>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_estado = $("#vent_exte_estado"+item.id+"").val();
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_inte = $("#vent_exte_inte"+item.id+"").val();
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_tipo = $("#vent_exte_tipo"+item.id+"").val();
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_mat = $("#vent_exte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_vent_ext.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigo_ptr_vent_ext.splice(pos,1);
                arrDibujo_ptr_vent_ext.splice(pos,1);
                //console.log("arrCodigo_ptr_vent_ext",arrCodigo_ptr_vent_ext);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_vent_ext.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_vent_ext[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVentanasExteriores').html(cadena);
                
                cont=0;
                arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
                {
                    $("#vent_exte_estado"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_estado);
                    $("#vent_exte_inte"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_inte);
                    $("#vent_exte_tipo"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_tipo);
                    $("#vent_exte_mat"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_mat);
                    

                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigo_ptr_vent_extFinal",arrCodigo_ptr_vent_ext);
    }

    ///////////////////////////////

/******************-------Vanos Puertas Interiores**********************/
//no
    var contCodigo_ptr_van_pue_int=2;
    var arrCodigo_ptr_van_pue_int=[];
    var arrDibujo_ptr_van_pue_int=[];

   htmlARG_ptr_van_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_int_estado1" name="pue_int_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_int_inte1" name="pue_int_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_int_tipo1" name="pue_int_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajad">Arco Rebajad</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_int_mat1" name="pue_int_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_int(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_van_pue_int.push({arg_pue_int_estado:'N',arg_pue_int_inte:'N',arg_pue_int_tipo:'N',arg_pue_int_mat:'N'});
    arrDibujo_ptr_van_pue_int.push({scd_codigo:htmlARG_ptr_van_pue_int,id: 1});
    //console.log("arrCodigo",arrCodigo_ptr_van_pue_int);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_van_pue_int[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divelcoVanosPuertasInteriores').html(cadena);
    $("#pue_int_estado1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_estado);
    $("#pue_int_inte1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_inte);
    $("#pue_int_tipo1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_tipo);
    $("#pue_int_mat1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_van_pue_int));

    function elcoVanosPuertasInteriores(){
    var htmlARG_ptr_van_pue_int;
        htmlARG_ptr_van_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_int_estado'+contCodigo_ptr_van_pue_int+'" name="pue_int_estado'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_int_inte'+contCodigo_ptr_van_pue_int+'" name="pue_int_inte'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_int_tipo'+contCodigo_ptr_van_pue_int+'" name="pue_int_tipo'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajad">Arco Rebajad</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_int_mat'+contCodigo_ptr_van_pue_int+'" name="pue_int_mat'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_int('+contCodigo_ptr_van_pue_int+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_estado = $("#pue_int_estado"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_inte = $("#pue_int_inte"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_tipo = $("#pue_int_tipo"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_mat = $("#pue_int_mat"+item.id+"").val();
          cont++;
        });
      
        arrCodigo_ptr_van_pue_int.push({arg_pue_int_estado:'N',arg_pue_int_inte:'N',arg_pue_int_tipo:'N',arg_pue_int_mat:'N'});
        arrDibujo_ptr_van_pue_int.push({scd_codigo:htmlARG_ptr_van_pue_int,id: contCodigo_ptr_van_pue_int});
        contCodigo_ptr_van_pue_int++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_van_pue_int.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_van_pue_int[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#divelcoVanosPuertasInteriores').html(cadena); 
        var conto=0;
        arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
        { 
          $("#pue_int_estado"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_estado);
          $("#pue_int_inte"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_inte);
          $("#pue_int_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_tipo);
          $("#pue_int_mat"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_van_pue_int));
    }

 
    function menosLinea_ptr_van_pue_int($id){
        if(contCodigo_ptr_van_pue_int>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_estado = $("#pue_int_estado"+item.id+"").val();
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_inte = $("#pue_int_inte"+item.id+"").val();
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_tipo = $("#pue_int_tipo"+item.id+"").val();
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_mat = $("#pue_int_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_van_pue_int.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigo_ptr_van_pue_int.splice(pos,1);
                arrDibujo_ptr_van_pue_int.splice(pos,1);
                //console.log("arrCodigo_ptr_van_pue_int",arrCodigo_ptr_van_pue_int);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_van_pue_int.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_van_pue_int[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVanosPuertasInteriores').html(cadena); 
                
                cont=0;
                arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
                {
                    $("#pue_int_estado"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_estado);
                    $("#pue_int_inte"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_inte);
                    $("#pue_int_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_tipo);
                    $("#pue_int_mat"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_mat);
                    
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigo_ptr_van_pue_intFinal",arrCodigo_ptr_van_pue_int);
    }
//////////

/******************-------Vanos Puertas Exteriores**********************/
 var contCodigo_ptr_van_pue_ext=2;
    var arrCodigo_ptr_van_pue_ext=[];
    var arrDibujo_ptr_van_pue_ext=[];

    htmlARG_ptr_van_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_ext_estado1" name="pue_ext_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_ext_inte1" name="pue_ext_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_ext_tipo1" name="pue_ext_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+
        

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_ext_mat1" name="pue_ext_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_ext(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_van_pue_ext.push({arg_pue_ext_estado:'N', arg_pue_ext_inte:'N', arg_pue_ext_tipo:'N', arg_pue_ext_mat:'N'});
    arrDibujo_ptr_van_pue_ext.push({scd_codigo:htmlARG_ptr_van_pue_ext,id: 1});
    //console.log("arrCodigo",arrCodigo);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_van_pue_ext[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divelcoVanosPuertasExteriores').html(cadena);
    $("#pue_ext_estado1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_estado);
    $("#pue_ext_inte1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_inte);
    $("#pue_ext_tipo1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_tipo);
    $("#pue_ext_mat1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_van_pue_ext));


    function elcoVanosPuertasExteriores(){
    var htmlARG_ptr_van_pue_ext;
        htmlARG_ptr_van_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_ext_estado'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_estado'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_ext_inte'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_inte'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_ext_tipo'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_tipo'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+
        

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_ext_mat'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_mat'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_ext('+contCodigo_ptr_van_pue_ext+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_estado = $("#pue_ext_estado").val();
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_inte = $("#pue_ext_inte").val();
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_tipo = $("#pue_ext_tipo").val();
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_mat = $("#pue_ext_mat").val();            
          cont++;
        });
        
        arrCodigo_ptr_van_pue_ext.push({arg_pue_ext_estado:'N', arg_pue_ext_inte:'N', arg_pue_ext_tipo:'N', arg_pue_ext_mat:'N'});
        arrDibujo_ptr_van_pue_ext.push({scd_codigo:htmlARG_ptr_van_pue_ext,id: contCodigo_ptr_van_pue_ext});
        contCodigo_ptr_van_pue_ext++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_van_pue_ext.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_van_pue_ext[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#divelcoVanosPuertasExteriores').html(cadena);
        var conto=0;
        arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
        { 
          $("#pue_ext_estado"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_estado);
          $("#pue_ext_inte"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_inte);
          $("#pue_ext_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_tipo);
          $("#pue_ext_mat"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_van_pue_ext));
    }

 
    function menosLinea_ptr_van_pue_ext($id){

        if(contCodigo_ptr_van_pue_ext>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_van_pue_ext[cont].arg_valor=$("#valor"+item.id+"").val();
                    arrCodigo_ptr_van_pue_ext[cont].arg_tipo=$("#tipoarg"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_van_pue_ext.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigo_ptr_van_pue_ext.splice(pos,1);
                arrDibujo_ptr_van_pue_ext.splice(pos,1);
                //console.log("arrCodigo_ptr_van_pue_ext",arrCodigo_ptr_van_pue_ext);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_van_pue_ext.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_van_pue_ext[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVanosPuertasExteriores').html(cadena);
                
                cont=0;
                arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
                {
                  $("#pue_ext_estado"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_estado);
                  $("#pue_ext_inte"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_inte);
                  $("#pue_ext_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_tipo);
                  $("#pue_ext_mat"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_mat);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigo_ptr_van_pue_extFinal",arrCodigo_ptr_van_pue_ext);
    }
////////6

/******************-------Puertas Interiores**********************/
 var contCodigo_ptr_pue_int=2;
    var arrCodigo_ptr_pue_int=[];
    var arrDibujo_ptr_pue_int=[];

   htmlARG_ptr_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_inte_estado1" name="pue_inte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_inte_inte1" name="pue_inte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_inte_tipo1" name="pue_inte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_inte_mat1" name="pue_inte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_int(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_pue_int.push({arg_pue_inte_estado:'N', arg_pue_inte_inte:'N', arg_pue_inte_tipo:'N', arg_pue_inte_mat:'N'});
    arrDibujo_ptr_pue_int.push({scd_codigo:htmlARG_ptr_pue_int,id: 1});
    //console.log("arrCodigo",arrCodigo);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_pue_int[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divelcpPuertasInteriores').html(cadena);
    $("#pue_inte_estado1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_estado);
    $("#pue_inte_inte1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_inte);
    $("#pue_inte_tipo1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_tipo);
    $("#pue_inte_mat1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_pue_int));


    function elcpPuertasInteriores(){
    var htmlARG_ptr_pue_int;
        htmlARG_ptr_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_inte_estado'+contCodigo_ptr_pue_int+'" name="pue_inte_estado'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_inte_inte'+contCodigo_ptr_pue_int+'" name="pue_inte_inte'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_inte_tipo'+contCodigo_ptr_pue_int+'" name="pue_inte_tipo'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_inte_mat'+contCodigo_ptr_pue_int+'" name="pue_inte_mat'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_int('+contCodigo_ptr_pue_int+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_estado = $("#pue_inte_estado"+item.id+"").val();
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_inte = $("#pue_inte_inte"+item.id+"").val();
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_tipo = $("#pue_inte_tipo"+item.id+"").val();
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_mat = $("#pue_inte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_pue_int.push({arg_valor:'', arg_tipo:'N'});
        arrDibujo_ptr_pue_int.push({scd_codigo:htmlARG_ptr_pue_int,id: contCodigo_ptr_pue_int});
        contCodigo_ptr_pue_int++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_pue_int.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_pue_int[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#divelcpPuertasInteriores').html(cadena); 
        var conto=0;
        arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
        { 
          $("#pue_inte_estado"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_estado);
          $("#pue_inte_inte"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_inte);
          $("#pue_inte_tipo"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_tipo);
          $("#pue_inte_mat"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_pue_int));
    }

 
    function menosLinea_ptr_pue_int($id){
        if(contCodigo_ptr_pue_int>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_estado = $("#pue_inte_estado"+item.id+"").val();
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_inte = $("#pue_inte_inte"+item.id+"").val();
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_tipo = $("#pue_inte_tipo"+item.id+"").val();
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_mat = $("#pue_inte_mat"+item.id+"").val();

                    cont=cont+1;
                });
                pos = arrDibujo_ptr_pue_int.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigo_ptr_pue_int.splice(pos,1);
                arrDibujo_ptr_pue_int.splice(pos,1);
                //console.log("arrCodigo_ptr_pue_int",arrCodigo_ptr_pue_int);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_pue_int.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_pue_int[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcpPuertasInteriores').html(cadena); 
                
                cont=0;
                arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
                {
                    $("#pue_inte_estado"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_estado);
                    $("#pue_inte_inte"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_inte);
                    $("#pue_inte_tipo"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_tipo);
                    $("#pue_inte_mat"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_mat);


                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigo_ptr_pue_intFinal",arrCodigo_ptr_pue_int);
    }
/////////7

/******************-------Puertas Exteriores**********************/
 var contCodigo_ptr_pue_ext=2;
    var arrCodigo_ptr_pue_ext=[];
    var arrDibujo_ptr_pue_ext=[];

   htmlARG_ptr_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_exte_estado1" name="pue_exte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_exte_inte1" name="pue_exte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
       '<select class="form-control" id="pue_exte_tipo1" name="pue_exte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
     '<select class="form-control" id="pue_exte_mat1" name="pue_exte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_ext(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_pue_ext.push({arg_pue_exte_estado:'N',arg_pue_exte_inte:'N', arg_pue_exte_tipo:'N',arg_pue_exte_mat:'N'});
    arrDibujo_ptr_pue_ext.push({scd_codigo:htmlARG_ptr_pue_ext,id: 1});
    //console.log("arrCodigo",arrCodigo_ptr_pue_ext);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_pue_ext[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divelcpPuertasExteriores').html(cadena);
    $("#pue_exte_estado1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_estado);
    $("#pue_exte_inte1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_inte);
    $("#pue_exte_tipo1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_tipo);
    $("#pue_exte_mat1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_pue_ext));

    function elcpPuertasExteriores(){
    var htmlARG_ptr_pue_ext;
        htmlARG_ptr_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_exte_estado'+contCodigo_ptr_pue_ext+'" name="pue_exte_estado'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_exte_inte'+contCodigo_ptr_pue_ext+'" name="pue_exte_inte'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
       '<select class="form-control" id="pue_exte_tipo'+contCodigo_ptr_pue_ext+'" name="pue_exte_tipo'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
     '<select class="form-control" id="pue_exte_mat'+contCodigo_ptr_pue_ext+'" name="pue_exte_mat'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_ext_ptr_pue_ext('+contCodigo_ptr_pue_ext+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_estado = $("#pue_exte_estado"+item.id+"").val();
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_inte = $("#pue_exte_inte"+item.id+"").val();
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_tipo = $("#pue_exte_tipo"+item.id+"").val();
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_mat = $("#pue_exte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_pue_ext.push({arg_pue_exte_estado:'N',arg_pue_exte_inte:'N', arg_pue_exte_tipo:'N',arg_pue_exte_mat:'N'});
        arrDibujo_ptr_pue_ext.push({scd_codigo:htmlARG_ptr_pue_ext,id: contCodigo_ptr_pue_ext});
        contCodigo_ptr_pue_ext++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_pue_ext.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_pue_ext[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#divelcpPuertasExteriores').html(cadena);
        var conto=0;
        arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
        { 
          $("#pue_exte_estado"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_estado);
          $("#pue_exte_inte"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_inte);
          $("#pue_exte_tipo"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_tipo);
          $("#pue_exte_mat"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_pue_ext));
    }

 
    function menosLinea_ptr_pue_ext_ptr_pue_ext($id){
        if(contCodigo_ptr_pue_ext>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_estado = $("#pue_exte_estado"+item.id+"").val();
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_inte = $("#pue_exte_inte"+item.id+"").val();
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_tipo = $("#pue_exte_tipo"+item.id+"").val();
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_mat = $("#pue_exte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_pue_ext.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigo_ptr_pue_ext.splice(pos,1);
                arrDibujo_ptr_pue_ext.splice(pos,1);
                //console.log("arrCodigo_ptr_pue_ext",arrCodigo_ptr_pue_ext);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_pue_ext.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_pue_ext[i].scd_codigo;
                    cont++;
                }
                
                 $('#divelcpPuertasExteriores').html(cadena);
                
                cont=0;
                arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
                {
                      $("#pue_exte_estado"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_estado);
                      $("#pue_exte_inte"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_inte);
                      $("#pue_exte_tipo"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_tipo);
                      $("#pue_exte_mat"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_mat);
                    
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigo_ptr_pue_extFinal",arrCodigo_ptr_pue_ext);
    }
    //////8


    /******************-------Rejas**********************/
    var contCodigo_ptr_reja=2;
    var arrCodigo_ptr_reja=[];
    var arrDibujo_ptr_reja=[];

    htmlARG_ptr_reja='Estado de Conservación:</label>'+
    '<select class="form-control" id="reja_estado1" name="reja_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="reja_inte1" name="reja_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="reja_tipo1" name="reja_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Figuras Geométricos">Figuras Geométricos</option>'+
              '<option value="Figuras Fitomarfas">Figuras Fitomarfas</option>'+

         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="reja_mat1" name="reja_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Aluminio">Aluminio</option>'+
              '<option value="Hierro Fundido">Hierro Fundido</option>'+
              '<option value="Hierro Forjado">Hierro Forjado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_reja(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_reja.push({arg_reja_estado:'N', arg_reja_inte:'N', arg_reja_tipo:'N', arg_reja_mat:'N'});
    arrDibujo_ptr_reja.push({scd_codigo:htmlARG_ptr_reja,id: 1});
    //console.log("arrCodigo",arrCodigo_ptr_reja);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_reja[0].scd_codigo;
    ////console.log("cadenaaaa",cadena);
    $('#divelcpRejas').html(cadena);
    $("#reja_estado1").val(arrCodigo_ptr_reja[0].arg_reja_estado);
    $("#reja_inte1").val(arrCodigo_ptr_reja[0].arg_reja_inte);
    $("#reja_tipo1").val(arrCodigo_ptr_reja[0].arg_reja_tipo);
    $("#reja_mat1").val(arrCodigo_ptr_reja[0].arg_reja_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_reja));


    function elcpRejas(){
    var htmlARG_ptr_reja;
        htmlARG_ptr_reja='Estado de Conservación:</label>'+
    '<select class="form-control" id="reja_estado'+contCodigo_ptr_reja+'" name="reja_estado'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="reja_inte'+contCodigo_ptr_reja+'" name="reja_inte'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="reja_tipo'+contCodigo_ptr_reja+'" name="reja_tipo'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Figuras Geométricos">Figuras Geométricos</option>'+
              '<option value="Figuras Fitomarfas">Figuras Fitomarfas</option>'+

         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="reja_mat'+contCodigo_ptr_reja+'" name="reja_mat'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Aluminio">Aluminio</option>'+
              '<option value="Hierro Fundido">Hierro Fundido</option>'+
              '<option value="Hierro Forjado">Hierro Forjado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_reja('+contCodigo_ptr_reja+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_reja.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_reja[cont].arg_reja_estado = $("#reja_estado"+item.id+"").val();
          arrCodigo_ptr_reja[cont].arg_reja_inte = $("#reja_inte"+item.id+"").val();
          arrCodigo_ptr_reja[cont].arg_reja_tipo = $("#reja_tipo"+item.id+"").val();
          arrCodigo_ptr_reja[cont].arg_reja_mat = $("#reja_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_reja.push({arg_reja_estado:'N', arg_reja_inte:'N', arg_reja_tipo:'N', arg_reja_mat:'N'});
        arrDibujo_ptr_reja.push({scd_codigo:htmlARG_ptr_reja,id: contCodigo_ptr_reja});
               
        contCodigo_ptr_reja++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_reja.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'.'+arrDibujo_ptr_reja[i].scd_codigo;
          cont++;
        }
        ////console.log("cadenaaaa",cadena);
        $('#divelcpRejas').html(cadena); 
        var conto=0;
        arrDibujo_ptr_reja.forEach(function (item, index, array) 
        { 
          $("#reja_estado"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_estado);
          $("#reja_inte"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_inte);
          $("#reja_tipo"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_tipo);
          $("#reja_mat"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_reja));
    }

 
    function menosLinea_ptr_reja($id){
        if(contCodigo>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_reja.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_reja[cont].arg_reja_estado = $("#reja_estado"+item.id+"").val();
                    arrCodigo_ptr_reja[cont].arg_reja_inte = $("#reja_inte"+item.id+"").val();
                    arrCodigo_ptr_reja[cont].arg_reja_tipo = $("#reja_tipo"+item.id+"").val();
                    arrCodigo_ptr_reja[cont].arg_reja_mat = $("#reja_mat"+item.id+"").val();

                    cont=cont+1;
                });
                pos = arrDibujo_ptr_reja.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigo_ptr_reja.splice(pos,1);
                arrDibujo_ptr_reja.splice(pos,1);
                //console.log("arrCodigo_ptr_reja",arrCodigo_ptr_reja);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_reja.length;i++)
                {
                    cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+arrDibujo_ptr_reja[i].scd_codigo;
                    cont++;
                }
                
                $('#grillaPrueba').html(cadena);
                
                cont=0;
                arrDibujo_ptr_reja.forEach(function (item, index, array) 
                {
                    $("#reja_estado"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_estado);
                    $("#reja_inte"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_inte);
                    $("#reja_tipo"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_tipo);
                    $("#reja_mat"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_mat);
                    cont=cont+1;
                });
            });
        }
        //console.log("arrCodigo_ptr_rejaFinal",arrCodigo_ptr_reja);
    }
////9

//FIN HTML TANYA


    
//ESPACIOS ABIERTOS

//-----------------GRILLAS CONJUNTOS OPATRIMONIALES-------------
/******************-------Cantidad de Inmuebles **********************/
    var contCodigoCANIN=2;
    var arrCodigoCANIN=[];
    var arrDibujoCANIN=[];

     htmlEleCANIN = 'INMUEBLE CAT. Y CONDICIÓN DE PROTECCIÓN:</label>'+
    '<input type="text" class="form-control" id="imn_cat1" name="imn_cat1">'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">CON PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_prot1" name="inm_prot1">'+              
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">SIN PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_des1" name="inm_des1">'+           
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">TOTAL:</label>'+
    '<input type="text" class="form-control" id="inm_total1" name="inm_total1">'+             
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaCANIN(1);"></i>'+
    '</a></div>';
    var cadenaCANIN = '';  

    arrCodigoCANIN.push({arg_imn_cat:'', arg_inm_prot:'', arg_inm_des:'', arg_total:''});  
    arrDibujoCANIN.push({scd_codigo:htmlEleCANIN ,id: 1});

   
         
    cadenaCANIN='<div class="form-group col-md-5"><label class="control-label">1.'+arrDibujoCANIN[0].scd_codigo;
    ////console.log("cadenaCANIN",cadenaCANIN);
    $('#crear_cantidad_inmuebles').html(cadenaCANIN);
    $("#imn_cat1").val(arrCodigoCANIN[0].arg_imn_cat);
    $("#inm_prot1").val(arrCodigoCANIN[0].arg_inm_prot);
    $("#inm_des1").val(arrCodigoCANIN[0].arg_inm_des);
    $("#inm_total1").val(arrCodigoCANIN[0].arg_inm_total);
    //$('#grillaPrueba').val(JSON.stringify(arrCodigoEleComVanosVenInteriores));

    function crearCantidadInmuebles(){
   
    var htmlEleCANIN;
    htmlEleCANIN='INMUEBLE CAT. Y CONDICIÓN DE PROTECCIÓN:</label>'+
    '<input type="text" class="form-control" id="imn_cat'+contCodigoCANIN+'" name="imn_cat'+contCodigoCANIN+'">'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">CON PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_prot'+contCodigoCANIN+'" name="inm_prot'+contCodigoCANIN+'">'+              
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">SIN PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_des'+contCodigoCANIN+'" name="inm_des'+contCodigoCANIN+'">'+           
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">TOTAL:</label>'+
    '<input type="text" class="form-control" id="inm_total'+contCodigoCANIN+'" name="inm_total'+contCodigoCANIN+'">'+             
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaCANIN('+contCodigoCANIN+');"></i>'+
    '</a></div>';
        //RECUPERA VALORES EN LA GRILLA
        var cadenaCANIN='';
        var cont=0;
        arrDibujoCANIN.forEach(function (item, index, array) 
        {
          arrCodigoCANIN[cont].arg_imn_cat = $("#imn_cat"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_prot = $("#inm_prot"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_des = $("#inm_des"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_total = $("#inm_total"+item.id+"").val();
          cont++;
        });

            
        arrCodigoCANIN.push({arg_imn_cat:'', arg_inm_prot:'', arg_inm_des:'', arg_total:''});  
        arrDibujoCANIN.push({scd_codigo:htmlEleCANIN,id:contCodigoCANIN});

        contCodigoCANIN++;
        cont=1;
        //CONCATENA HTML GRILLA EN LA VISTA

        for (var i=0; i<arrDibujoCANIN.length; i++)
        {
           //console.log("arrDibujoCANINXXXXXXXXX",arrDibujoCANIN[i].scd_codigo);
          cadenaCANIN=cadenaCANIN +'<div class="form-group col-md-5"><label class="control-label">'+cont+'.'+arrDibujoCANIN[i].scd_codigo;
          cont++;
        }
         
        //vero
        //console.log("cadenaCANIN",cadenaCANIN);
        $('#crear_cantidad_inmuebles').html(cadenaCANIN);

        //MUESTRA VALORES EN LA GRILLA
        var conto=0;
        arrDibujoCANIN.forEach(function (item, index, array) 
        { 
        $("#imn_cat"+item.id+"").val(arrCodigoCANIN[conto].arg_imn_cat);
        $("#inm_prot"+item.id+"").val(arrCodigoCANIN[conto].arg_inm_prot);
        $("#inm_des"+item.id+"").val(arrCodigoCANIN[conto].arg_inm_des);
        $("#inm_total"+item.id+"").val(arrCodigoCANIN[conto].arg_inm_total);
          conto=conto+1;
        });

        ////console.log("arrCodigoCANIN",arrCodigoCANIN);
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo));
    }

    function menosLineaCANIN($id){
        if(contCodigoCANIN>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaCANIN='';
                var cont=0;
                var pos=-1;
                arrDibujoCANIN.forEach(function (item, index, array) 
                {
                    arrCodigoCANIN[cont].arg_imn_cat = $("#imn_cat"+item.id+"").val();
                    arrCodigoCANIN[cont].arg_inm_prot = $("#inm_prot"+item.id+"").val();
                    arrCodigoCANIN[cont].arg_inm_des = $("#inm_des"+item.id+"").val();
                    arrCodigoCANIN[cont].arg_inm_total = $("#inm_total"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoCANIN.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoCANIN.splice(pos,1);
                arrDibujoCANIN.splice(pos,1);
                ////console.log("arrCodigo",arrCodigoCANIN);
                cont=1;
                for (var i=0; i<arrDibujoCANIN.length;i++)
                {
                    cadenaCANIN=cadenaCANIN + '<div class="form-group col-md-5"><label class="control-label">'+cont+arrDibujoCANIN[i].scd_codigo;
                    cont++;
                }                
                $('#crear_cantidad_inmuebles').html(cadenaCANIN);
                cont=0;
                arrDibujoCANIN.forEach(function (item, index, array) 
                {
                  $("#imn_cat"+item.id+"").val(arrCodigoCANIN[cont].arg_imn_cat);
                  $("#inm_prot"+item.id+"").val(arrCodigoCANIN[cont].arg_inm_prot);
                  $("#inm_des"+item.id+"").val(arrCodigoCANIN[cont].arg_inm_des);
                  $("#inm_total"+item.id+"").val(arrCodigoCANIN[cont].arg_inm_total);
                    //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
       
    }

/******************-------Cantidad de Inmuebles **********************/

//--FUNCIÓNES PARA LA PUNTUACIÓN DE LOS CHECK-----
var sumaepo = 0;
var sumasisconst = 0;
var suma1 = 0; 
var suma2 = 0;
var suma3 = 0;
var suma4 = 0;
var suma5 = 0;
var suma6 = 0;
var suma7 = 0;
var suma8 = 0;

function epocaEstilo(){
  var total = 0;
  $("input[name=epocaestilo]").each(function(index) {
    if ($(this).is(':checked')) {
      total += parseFloat($(this).val());
    } 
    
  });
  sumaepo = total;
  //console.log('EPOCA Y ESTILO',sumaepo);
}

function sistemaConstructivo(){
  var total = 0;
  $("input[name=sistemaconstructivo]").each(function(index) {
    if ($(this).is(':checked')) {
      total += parseFloat($(this).val());
    } 
    
  });
  sumasisconst = total;
  //console.log('SISTEMA CONSTRUCTIVO',sumasisconst);
}

function acapites(){
  var total = 0;
  $("input[name=cacapites]").each(function(index) {
    if ($(this).is(':checked')) {
      total += parseFloat($(this).val());
    } 
    suma1 = total;
  });
  //document.getElementById('spTotal').innerHTML = total;
  
  //console.log('ACAPITES',suma1);
}


function indicadorHistorico(){
 var total = 0;
 $("input[name=ihistorico]").each(function(index) {
  if ($(this).is(':checked')) {
    total += parseFloat($(this).val());
  } 

});
 suma2 = total;
 //console.log('INDICADOR HISTORICO',suma2);
}


function indicadorArtistico(){
 var total = 0;
 $("input[name=iartistico]").each(function(index) {
  if ($(this).is(':checked')) {
    total += parseFloat($(this).val());
  } 

});
 suma3 = total;
 //console.log('INDICADOR ARTISTICO',suma3);
}

function indicadorArquitectonico(){
 var total = 0;
 $("input[name=iarquitectonico]").each(function(index) {
  if ($(this).is(':checked')) {
    total += parseFloat($(this).val());
  }  
});
 suma4 = total;
 //console.log('INDICADOR ARQUITECTONICO',suma4);
}

function indicadorTecnologico(){
 var total = 0;
 $("input[name=itecnologico]").each(function(index) {
  if ($(this).is(':checked')) {
    total += parseFloat($(this).val());
  }  
});
 suma5 = total;
 //console.log('INDICADOR TECNOLOGICO',suma5);
}

function indicadorIntegridad(){
 var total = 0;
 $("input[name=iintegridad]").each(function(index) {
  if ($(this).is(':checked')) {
    total += parseFloat($(this).val());
  }  
});
 suma6 = total;
 //console.log('INDICADOR INTEGRIDAD',suma6);
}

function indicadorUrbano(){
 var total = 0;
 $("input[name=iurbano]").each(function(index) {
  if ($(this).is(':checked')) {
    total += parseFloat($(this).val());
  }  
});
 suma7 = total;
 //console.log('INDICADOR URBANO',suma7);
}

function indicadorInmaterial(){
 var total = 0;
 $("input[name=iinmaterial]").each(function(index) {
  if ($(this).is(':checked')) {
    total += parseFloat($(this).val());
  }  
});
 suma8 = total;
 //console.log('INDICADOR INMATERIAL',suma8);
}

function Puntuacion(){
  total = suma1 +suma2 +suma3 + suma4 +suma5 +suma6 + suma7 + suma8 + sumaepo + sumasisconst;
  //console.log('TOTAL',total);
  document.getElementById('cpuntuacion').value =  total;
  if(total<50){
    if(total==0){
      document.getElementById('ccategoria_inmueble').value='D';
    }else{ 
      document.getElementById('ccategoria_inmueble').value='C';
    }
  }else{
    if(total<80){
      document.getElementById('ccategoria_inmueble').value='B';
    }else{
      if(total<=105){
        document.getElementById('ccategoria_inmueble').value='A';
      }
    }
  }
}

//----------FUNCIONES PARA VALIDAR LAS PÁGINAS DE LOS MODALES
function validar1(){
  var valor = document.getElementById('cbloqueInventario_in').value;
  var pag1 = document.getElementById('pagina1').value;
  var codigoCatastral = document.getElementById('ccodCatastral_in').value;
  $( "#ccodigo_catastral" ).val(codigoCatastral);

  if(valor == 0 && pag1==1){
    swal('Campo Obligatorio: Bloque Inventariado'); 
  }else{
    if(valor>0  && pag1 == 1)
    {
     $("#myCreateBienesInmueble").modal('toggle');
     $("#myCreateBienesInmueble1").modal();
   }
 }
}
// -- pagina 2 bloques--//
function volver2()
{
  var valor = document.getElementById('cbloqueInventario_in').value;
  var pag2 = document.getElementById('pagina2').value;
  if(valor>0 && pag2 == 2)
  {
   $("#myCreateBienesInmueble").modal();
   $("#myCreateBienesInmueble1").modal('toggle');
 }
}
function validar2(){
  var valor = document.getElementById('cbloqueInventario_in').value;
  var pag2 = document.getElementById('pagina2').value;
  if((valor>0 && valor<2)  && pag2 == 2)
  {
   swal('guardar 2 ');
   $('#myCreateBienesInmueble2').modal('hide'); 
 }
 else {if( valor>0 && pag2 == 2)
  {
    $("#myCreateBienesInmueble1").modal('toggle');
    $("#myCreateBienesInmueble2").modal();
  }
}
}

function validarUpdate1(){
  var valor = document.getElementById('abloqueInventario_in').value;
  var pag1 = document.getElementById('apagina1').value;
  var acodigoCatastral = document.getElementById('acodCatastral_in').value;
  $( "#acodigo_catastral" ).val(acodigoCatastral);

  if(valor == 1 && pag1==1){
    $("#myUpdateBienesInmueble").modal('toggle');
    $("#myUpdateBienesInmueble1").modal();
  }else{
    if(valor>0  && pag1 == 1)
    {
     $("#myUpdateBienesInmueble").modal('toggle');
     $("#myUpdateBienesInmueble1").modal();
   }
 }
}

function validarUpdate2(){
  var valor = document.getElementById('abloqueInventario_in').value;
  var pag2 = document.getElementById('apagina2').value;
  if((valor>0 && valor==1)  && pag2 == 2)
  {
   swal('guardar 2 ');
   $('#myUpdateBienesInmueble2').modal('hide'); 
 }
 else {if( valor>0 && pag2 == 2)
  {
    $("#myUpdateBienesInmueble1").modal('toggle');
    $("#myUpdateBienesInmueble2").modal();
  }
}
}


function volverUpdate2()
{
  var valor = document.getElementById('abloqueInventario_in').value;
  var pag2 = document.getElementById('apagina2').value;
  if(valor>0 && pag2 == 2)
  {
   $("#myUpdateBienesInmueble").modal();
   $("#myUpdateBienesInmueble1").modal('toggle');
 }
}



  // -- fin pagina 2 bloques--//
</script>

<script  src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script >

  $('textarea').ckeditor();


</script>
@endpush




