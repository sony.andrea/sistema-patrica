CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_insertar_adjuntos_patrimonio2(
    xdoc_idcasos integer,
    xdoc_nombre text,
    xdoc_url_logica text,
    xdoc_datos text,
    xdoc_titulo text,
    xdoc_cuerpo text,
    xdoc_palabras text,
    xdoc_correlativo text)
  RETURNS boolean AS
$BODY$
BEGIN
    INSERT INTO patrimonio_cultural.dms_gt_documentos(
    doc_id,
    doc_nombre , 
    doc_url_logica,
    doc_datos,
    doc_titulo,
    doc_cuerpo,
    doc_palabras,
    doc_correlativo)
    VALUES (
    xdoc_idCasos,
    xdoc_nombre ,
    xdoc_url_logica,
    xdoc_datos,
    xdoc_titulo,
    xdoc_cuerpo,
    xdoc_palabras,
    xdoc_correlativo 
        );
         RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION patrimonio_cultural.sp_insertar_adjuntos_patrimonio2(integer, text, text, text, text, text, text, text)
  OWNER TO postgres;

/*----------------*/
SERVICIO_VALLE-915


$_conn1=coneccionBD("18");
$_tipo1="Select";
$_query1="select * from patrimonio_cultural.sp_insertar_adjuntos_patrimonio2(
    $xdoc_idCasos,
    '$xdoc_nombre',
    '$xdoc_url_logica',
    '$xdoc_datos',
    '$xdoc_titulo',
    '$xdoc_cuerpo',
    '$xdoc_palabras',
    '$xdoc_correlativo')";
$_dataDB1=ejecutar_Query($_conn1,$_query1,$_tipo1);
echo($_dataDB1);

/*----------------------------------------------------otra regla-*/
SERVICIO_VALLE-926

$_conn1=coneccionBD("18");
$_tipo1="Select";
$_query1="select * from patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1($xid_doccasos, '$xg_tipo')";
$_dataDB1=ejecutar_Query($_conn1,$_query1,$_tipo1);
echo($_dataDB1);

-- Function: patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text)

-- DROP FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text);

CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(
    IN yid_doccasos integer,
    IN yg_tipo text)
  RETURNS TABLE(xdoc_idd integer, xdoc_id integer, xdoc_nombre text, xdoc_url_logica text, xdoc_datos text, xdoc_titulo text, xdoc_cuerpo text, xdoc_palabras text, xdoc_correlativo text) AS
$BODY$

BEGIN     

 IF yg_tipo = 'PTR-IME' THEN
   RETURN QUERY    
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
        or cast(doc_correlativo as integer)= 4 
        )
        and doc_estado = 'A'
        order by cast (doc_correlativo as integer) asc;

  return;
  end if;

  IF yg_tipo = 'PTR-ESP' or  yg_tipo = 'PTR-CON' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
              )
           and doc_estado = 'A'
       order by cast (doc_correlativo as integer) asc;
  return;
  end if;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text)
  OWNER TO postgres;


/*-------------------------------------*/
SERVICIO_VALLE-927


$_conn1=coneccionBD("18");
$_tipo1="Select";
$_query1="select * from patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1($xid_doccasos, '$xg_tipo')";
$_dataDB1=ejecutar_Query($_conn1,$_query1,$_tipo1);
echo($_dataDB1);

-- Function: patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(integer, text)

-- DROP FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(integer, text);

CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(
    IN yid_doccasos integer,
    IN yg_tipo text)
  RETURNS TABLE(xdoc_idd integer, xdoc_id integer, xdoc_nombre text, xdoc_url_logica text, xdoc_datos text, xdoc_titulo text, xdoc_cuerpo text, xdoc_palabras text, xdoc_correlativo text) AS
$BODY$

BEGIN     

 IF yg_tipo = 'PTR-IME' THEN
   RETURN QUERY    
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6 
        or cast(doc_correlativo as integer) = 7 
        or cast(doc_correlativo as integer) = 8
        or cast(doc_correlativo as integer) = 9
        or cast(doc_correlativo as integer) = 10)
        and doc_estado = 'A'
        order by cast (doc_correlativo as integer) asc;

  return;
  end if;

  IF yg_tipo = 'PTR-ESP' or  yg_tipo = 'PTR-CON' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 4 
        or cast(doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6 
        or cast(doc_correlativo as integer) = 7
        or cast(doc_correlativo as integer) = 8
        or cast(doc_correlativo as integer) = 9
       )
       and doc_estado = 'A'
       order by cast (doc_correlativo as integer) asc;
  return;
  end if;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(integer, text)
  OWNER TO postgres;


  /*--------------------------------------------****************************************hasta aqui------*/


SERVICIO_VALLE-916

$_conn1=coneccionBD("18");
$_tipo1="Select";
$_query1="select * from patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio1( $xid_doccasos)";
$_dataDB1=ejecutar_Query($_conn1,$_query1,$_tipo1);
echo($_dataDB1);

CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio1(IN yid_doccasos integer)
  RETURNS TABLE(xdoc_idd integer, xdoc_id integer, xdoc_nombre text, xdoc_url_logica text, xdoc_datos text, xdoc_titulo text, xdoc_cuerpo text, xdoc_palabras text, xdoc_correlativo text) AS
$BODY$
BEGIN     
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos;
return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio1(integer)
  OWNER TO postgres;

