<div class="modal fade modal-default" id="myCreate" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical puestoMercado-validate','id'=>'formNuevaFicha_create'])!!}
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h3>
              Nueva Ficha Bienes Arqueologicos
              <small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h3>
          </div>
        </section>
      </div>   
    </div>
  </div>

  <div  style="padding:20px">
    <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
    <input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">
    
    <div class="col-md-12">
      <div class="form-group col-md-6">
        <label class="control-label">
          Proveniencia del bien Mueble::
        </label>

        <select class="form-control" id="cprovenienciaMueble" >
          <option value=""> Selecionar</option> 
          <option value=""> Contexto Arqueologico </option>
          <option value=""> Colección</option>
          <option value=""> Otro</option>  
        </select> 
      </div>
      <div class="form-group col-md-6">
        <label class="control-label">
          Relación de la Pieza con el Yacimiento:
        </label>
        <input id="crelacionPieza" type="text" class="form-control"  placeholder="Relación de la Pieza">
      </div>

    </div> 
    <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
        Ubicacion Actual:
      </label>
       <input id="cubicacionActual" type="text" class="form-control"  placeholder="Ubicación Actual"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Descripción del bien Mueble:
      </label>
      <input id="cDescripcionMueble" type="text" class="form-control"  placeholder="Descripción"> 
    </div>
  </div>
  <div class="col-md-12">
        <div class="form-group col-md-6">
      <label class="control-label">
        Material:
      </label>
      <input id="cmaterial"  type="text" class="form-control"  placeholder="Material">
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Técnica:
      </label>
      <input id="ctecnica"  type="text" class="form-control"  placeholder="Técnica">
    </div>
  </div>
   <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
       Manufactura:
      </label>
       <input id="cmanufactura" type="text" class="form-control"  placeholder="Manufactura"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Decoración::
      </label>
      <input id="cdecoracion" type="text" class="form-control"  placeholder="Decoración"> 
    </div>
  </div>
     <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
       Atributos:
      </label>
       <input id="catributos" type="text" class="form-control"  placeholder="Atributos"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Estado de Conservación:
      </label>
      <select class="form-control" id="cestadoConserva" >
          <option value=""> Selecionar</option> 
          <option value=""> Bueno</option>
          <option value=""> Malo</option>
          <option value=""> Regular</option>  
        </select> 
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
       Descripción del Estado:
      </label>
       <input id="cdescripEstado" type="text" class="form-control"  placeholder="Descripción"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Observaciones:
      </label>
      <input id="cobservacion" type="text" class="form-control"  placeholder="Observaciones"> 
    </div>
  </div>

<div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
       Cronología:
      </label>
       <input id="ccronologia" type="text" class="form-control"  placeholder="Cronología"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Función:
      </label>
      <input id="cfuncion" type="text" class="form-control"  placeholder="Función"> 
    </div>
  </div>


<div class="modal-footer">
  <button class="btn btn-default "  type="button" onClick="limpiar()">
    <i class="fa fa-eraser"></i> Limpiar
  </button>
  <a type = "button" class = "btn btn-primary"  id="registrar"  data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
</div> 
</div>

{!! Form::close() !!}
</div>
</div>
</div>
