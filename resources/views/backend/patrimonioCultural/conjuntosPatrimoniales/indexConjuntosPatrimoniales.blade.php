@extends('backend.template.app')
@section('main-content')
@include('backend.patrimonioCultural.conjuntosPatrimoniales.partial.modalCreate')
@include('backend.patrimonioCultural.conjuntosPatrimoniales.partial.modalUpdate')


<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>OPCIONES DE BUSQUEDA - CONJUNTOS PATRIMONIALES</b></div>
    <div class="panel-body">     
      <br>
      <div class="col-md-12">
       <div class="form-group col-md-4">
        <label class="control-label">
          Tipo Patrimonio Cultural:
        </label>
         <input type="hidden" name="tipoPatriCultural" id="tipoPatriCultural" value="5">
        <select class="form-control" name="tipoPatriCulturalMuestra" id="tipoPatriCulturalMuestra" disabled="">
          <option selected="">CONJUNTOS</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Campo:
        </label>
        <select class="form-control" name="camposPorPatrimonio" id="camposPorPatrimonio">
          <option value="-1">Seleccionar...</option>
        </select>
        
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Valor:
        </label>
        <input type="text" id="valorPatrimonio" class="form-control" name="valorPatrimonio">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="container">
        <div class="col-md-6"></div>
        <div class="col-md-6"> 
          <button type ="button" class = "btn btn-primary" onclick="buscarPatrimonioTipoCampo(1)"> <i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-primary" onclick="limpiarPrincipal()"> <i class="fa fa fa-eraser"></i> Limpiar</button>
          <a class= "btn btn-primary" data-target= "#myCreateConjuntoPatrimonial" data-toggle= "modal"><i class="glyphicon glyphicon-plus"></i> Nuevo Adjunto</a>
     </div>
   </div>
 </div>
</div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-body">
        <div id="listPatrimonio">
        </div>  
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@push('scripts')
<script>


  var i = 20;
  var _usrid={{$usuarioId}};
  var macrodistrito_descripcion='0';
  var contespaciosabiertos = 1;
  var contconjuntos = 1;
  var contesculturas = 1;
  var correlativog="";

  function buscarPatrimonioPorTipo(paging_pagenumbe)
  { 
   document.getElementById("listado_campo_valor").style = "display:none;";
    document.getElementById("listado_principal").style = "display:block;";
    var paging_pagesize = $( "#reg" ).val(); 
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1207',"parametros": '{"paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'}; 
    var band = 'list_principal';
    buscar(formData, band);  
  }
  function buscarPatrimonioTipoCampo(paging_pagenumbe){
    document.getElementById("listado_campo_valor").style = "display:block;";
    document.getElementById("listado_principal").style = "display:none;";
    var paging_pagesize = $( "#reg_campo" ).val(); 
    var campoPatrimonio = $( '#camposPorPatrimonio' ).val();
    var valorPatrimonio = $( '#valorPatrimonio' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1208',"parametros": '{"xcampo_id":' +campoPatrimonio+ ', "xcampo_valor":"' +valorPatrimonio+ '","xpaging_pagesize":'+paging_pagesize+',"xpaging_pagenumber":'+paging_pagenumbe+'}'};  
    var band = 'list_por_campo'; 
    buscar(formData, band,444444444);   
  }

  function limpiarPrincipal()
  {
    $( '#tipoPatriCultural' ).val("");
    $( '#camposPorPatrimonio' ).val("");
    $( '#valorPatrimonio' ).val("");
  }

  /*function listarPatrimonioCultural(g_tipo){
    var tipo = 5;
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-933',"parametros": '{"xtipo_id":' +tipo+ '}'}; 
    buscar(formData);  
  }*/

  function generarProforma(id_cas, g_tipo){
    setTimeout(function(){
      var urlPdf = "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerar',['datos'=>'d1']) }}";
      urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirArchivoFotografico(id_cas, g_tipo){
    setTimeout(function(){
      urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirFichaTodo(id_cas, g_tipo){
    setTimeout(function(){   
     var urlFicha = "{{ action('reportesPatrimonioCultural\reporteConjuntoPatrimonial@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     urlFicha = urlFicha.replace('d1', id_cas);
    ////console.log("urlFicha",urlFicha);
    window.open(urlFicha); 
  }, 500);
  }


  function buscar(formData, band){

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-patrimonio" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Nro de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código Catastral</th>';
    htmlListaBus += '<th align="left" class="form-natural">Denominación</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Fecha</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Adjuntos</th>' ;
    htmlListaBus += '<th width="15%" align="center" class="form-natural">Impresiones</th></tr>' ;
    htmlListaBus += '</thead>'; 
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          var datos = dataIN;
          //console.log('busquedaaaaa',dataIN);
          var tam = datos.length;
        //////console.log('tam',tam);
        if(dataIN == "[{ }]"){
          tam = 0;
        }; 
        var tipoContribuyenteEnvio = $( "#tipo-contribuyente" ).val();

        for (var i = 0; i < tam; i++){ 

      var cas_id = JSON.parse(dataIN[i].data).cas_id;
      var dataP= JSON.parse(dataIN[i].data);
      var g_tipo='PTR-CON'; 
      var numero_ficha = JSON.parse(dataIN[i].data).cas_nro_caso;
      var codigo_ficha = JSON.parse(dataIN[i].data).cas_nombre_caso;
      var codigo_catastral = '';
      var fecha_registro = JSON.parse(dataIN[i].data).cas_registrado;
      fecha_registro = fecha_registro.split('T');
      fecha_registro = fecha_registro[0];
      var denominacion = ''; 

       htmlListaBus += '<tr>';
       htmlListaBus += '<td align="left">'+(i+1)+'</td>';
       htmlListaBus += '<td align="center">';
       htmlListaBus+='<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + cas_id +',\''+ g_tipo +'\');">';
       htmlListaBus+='<i class="glyphicon glyphicon-trash"></i></button>';
       htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateConjuntoPatrimonial" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarConjuntoPatrimonial('+ cas_id +');"><i class="glyphicon glyphicon-pencil"></i></button>';   
      htmlListaBus += '</td>';
      htmlListaBus += '<td align="left" class="form-natural">'+numero_ficha+'</td>'; 
      htmlListaBus += '<td align="left" class="form-natural">'+codigo_ficha+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+codigo_catastral+'</td>'; 
      htmlListaBus += '<td align="left" class="form-natural">'+denominacion+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+fecha_registro+'</td>';
      var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
      url = url.replace('casosId1', cas_id);
      url = url.replace('numeroficha1', numero_ficha);

      htmlListaBus+='<td style="text-align: center;"><a href="'+url+'" class="btncirculo btn-xs btn-default" fa fa-plus-square pull-right"  > <i class="fa fa-folder"></i></a>';
      htmlListaBus+='</td>';
      htmlListaBus+='<td align="center">';
      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Ficha" type="button" onClick= "generarProforma(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-print"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Archivo Fotografico" type="button" onClick= "imprimirArchivoFotografico(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-camera"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Todo" type="button" onClick= "imprimirFichaTodo(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-list"></i></button>';

      htmlListaBus+='</td>';

      htmlListaBus += '</tr>';
    } 
    htmlListaBus += '</table></div></div>';
    htmlListaBus +='<div>';
    htmlListaBus +='<ul class="pager">';
    if(band =='list_principal'){
    htmlListaBus +='<li><a href="#" onClick="btnAnterior_prin()">Previous</a></li>';
    htmlListaBus +='<li><a href="#" onClick="btnSiguiente_prin()">Next</a></li>';
    }else{
    htmlListaBus +='<li><a href="#" onClick="btnAnterior_campo_valor()">Previous</a></li>';
    htmlListaBus +='<li><a href="#" onClick="btnSiguiente_campo_valor()">Next</a></li>';  
    }
    htmlListaBus+='</ul>';
    htmlListaBus +='</div>';
    $( '#listPatrimonio' ).html(htmlListaBus);


  },
  error: function(result) {
    swal( "Error..!", "Error....", "error" );
  }
});
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});
};
function getBool (val){
  return !!JSON.parse(String(val).toLowerCase());
}

function selectCampos()
{  
  var tipoCampoPatrimonio = $('#tipoPatriCultural').val();
  document.getElementById("camposPorPatrimonio").length=1;
  //////////console.log(tipoCampoPatrimonio,333333333333);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-931',"parametros": '{"ytipoid":'+tipoCampoPatrimonio+'}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        //////////console.log("listadoCampos",dataIN);
        for(var i = 0; i < dataIN.length; i++){
         var campo_id = dataIN[i].xcampo_id;
         var campo_tipo = JSON.parse(dataIN[i].xcampo_data).campoTipo;   
         document.getElementById("camposPorPatrimonio").innerHTML += '<option value=' +campo_id+ '>' +campo_tipo+ '</option>';            
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
}

function obtenercorrelativo(tipoPatrimonio)
{
 $.ajax({
  type        : 'GET',
  url         : urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": 'SERVICIO_PATRIMONIO-936',"parametros":'{"xtipoPat":"'+tipoPatrimonio+'"}'};
    //////////console.log("formdataaa",formData);
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
      var data=dataIN;
      correlativog=dataIN[0].sp_correlativo_patrimonio4;       
    //////////console.log("correl99999999999999999ativog",correlativog);

  },     
  error: function (xhr, status, error) { }
});
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

}

/*-----------------BEGIN CONJUNTOS PATRIMONIALES---------------------*/
$("#guardarConjuntosPatrimoniales").click(function(){
 obtenercorrelativo('PTR-CON');
 setTimeout(function(){

  var PTR_NOM = $( "#cnominacion" ).val();
  var PTR_UBIC = $( "#cubicacion" ).val();
  var PTR_TIP_PROT = $( "#ctipoproteccion" ).val();
  var PTR_NRO_PROT = $( "#cnroproteccion" ).val();
  var PTR_NUM_INM = $( "#cnrototalinmuebletramo" ).val();
  var PTR_NUM_PAT = $( "#cnrototalinmueblepatri" ).val();
  var PTR_IND_URB = $( "#cindicadorurbano" ).val();
  var PTR_IND_ART = $( "#cindicadorartistico" ).val();
  var PTR_IND_TIP = $( "#cindicadortipologico" ).val();
  var PTR_IND_TEC = $( "#cindicadortecnologico" ).val();
  var PTR_IND_INT = $( "#cindicadorintegridad" ).val();
  var PTR_IND_HIST = $( "#cindicadorhistcult" ).val();
  var PTR_IND_SIMB = $( "#cindicadorsimbolico" ).val();
  var PTR_NOM_VIA = $( "#cnombrevia" ).val();
  var PTR_MAT_VIA = $( "#cmateriavia" ).val();
  var PTR_CLASS = $( "#cclasificacion" ).val();
  var PTR_ENT_CALL = $( "#centrecalles" ).val();

  var g_tipo = 'PTR-CON';

  var PTR_IND_URB = caracteresEspecialesRegistrar(PTR_IND_URB);
  var PTR_IND_ART = caracteresEspecialesRegistrar(PTR_IND_ART);
  var PTR_IND_TIP = caracteresEspecialesRegistrar(PTR_IND_TIP);
  var PTR_IND_TEC = caracteresEspecialesRegistrar(PTR_IND_TEC);
  var PTR_IND_INT = caracteresEspecialesRegistrar(PTR_IND_INT);
  var PTR_IND_HIST = caracteresEspecialesRegistrar(PTR_IND_HIST);
  var PTR_IND_SIMB = caracteresEspecialesRegistrar(PTR_IND_SIMB);

   //--------GRILLA CONJUNTOS ABIERTOS---------------VERO


     var cont=0;
        arrDibujoCANIN.forEach(function (item, index, array) 
        {
          arrCodigoCANIN[cont].arg_imn_cat = $("#imn_cat"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_prot = $("#inm_prot"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_des = $("#inm_des"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_total = $("#inm_total"+item.id+"").val();
          cont++;
        });

  var bloqueCANIN = '[{"tipo":"GRD","campos":"imn_cat|inm_prot|inm_des|inm_total","titulos":"INMUEBLES POR CATEGORIA Y CONDICIÓN DE PROTECCIÓN|C/PROTECCION|S/PROTECCION|TOTAL"}';
  for (var i = 0; i<arrCodigoCANIN.length;i++)
  {
    var grillaFiguracon = '{"imn_cat":"'+arrCodigoCANIN[i].arg_imn_cat+'", "inm_prot":"'+arrCodigoCANIN[i].arg_inm_prot+'", "inm_des":"'+arrCodigoCANIN[i].arg_inm_des+'", "inm_total":"'+arrCodigoCANIN[i].arg_inm_total+'"}';
    bloqueCANIN = (bloqueCANIN.concat(',').concat(grillaFiguracon));

  }
  PTR_CANT_INM = bloqueCANIN.concat(']');
 

  //--------GRILLA CONJUNTOS ABIERTOS---------------


  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOM":"'+PTR_NOM+'","PTR_UBIC":"'+PTR_UBIC+'","PTR_TIP_PROT":"'+PTR_TIP_PROT+'","PTR_NRO_PROT":"'+PTR_NRO_PROT+'","PTR_NUM_INM":"'+PTR_NUM_INM+'","PTR_NUM_PAT":"'+PTR_NUM_PAT+'","PTR_IND_URB":"'+PTR_IND_URB+'","PTR_IND_ART":"'+PTR_IND_ART+'","PTR_IND_TIP":"'+PTR_IND_TIP+'","PTR_IND_TEC":"'+PTR_IND_TEC+'","PTR_IND_INT":"'+PTR_IND_INT+'","PTR_IND_HIST":"'+PTR_IND_HIST+'","PTR_IND_SIMB":"'+PTR_IND_SIMB+'","PTR_NOM_VIA":"'+PTR_NOM_VIA+'","PTR_MAT_VIA":"'+PTR_MAT_VIA+'","PTR_CLASS":"'+PTR_CLASS+'","PTR_ENT_CALL":"'+PTR_ENT_CALL+'","PTR_CANT_INM":'+PTR_CANT_INM+'}';

  var data = JSON.stringify(data);

  //////////console.log("datadata",data);
  var cas_nro_caso = correlativog ;

  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  var cas_nombre_caso = 'PTR-CON'+correlativog+'/2018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-941","parametros":'{"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");

  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        buscarPatrimonioPorTipo(1);  
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );
        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
        setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', cas_nro_caso);
          location.href = url;
        }, 1000);     


      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500);
});

function editarConjuntoPatrimonial(cas_id){

    var formData = {"identificador": "SERVICIO_PATRIMONIO-1016", "parametros":'{"id_caso":'+cas_id+'}'};

    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {

            var cas_id = JSON.parse(dataIN[0].data).cas_id;
            var numero_ficha = JSON.parse(dataIN[0].data).cas_nro_caso;
            var g_tipo= JSON.parse(dataIN[0].data).cas_datos.g_tipo;
            var codigo_ficha = JSON.parse(dataIN[0].data).cas_nombre_caso;

            var nominacion = JSON.parse(dataIN[0].data).cas_datos.PTR_NOM;
            var ubicacion = JSON.parse(dataIN[0].data).cas_datos.PTR_UBIC;
            var tipoproteccion = JSON.parse(dataIN[0].data).cas_datos.PTR_TIP_PROT;
            var nroproteccion = JSON.parse(dataIN[0].data).cas_datos.PTR_NRO_PROT;
            var nrototalinmuebletramo = JSON.parse(dataIN[0].data).cas_datos.PTR_NUM_INM;
            var nrototalinmueblepatri = JSON.parse(dataIN[0].data).cas_datos.PTR_NUM_PAT;
            var indicadorurbano = JSON.parse(dataIN[0].data).cas_datos.PTR_IND_URB;
            var indicadorartistico = JSON.parse(dataIN[0].data).cas_datos.PTR_IND_ART;
            var indicadortipologico = JSON.parse(dataIN[0].data).cas_datos.PTR_IND_TIP;
            var indicadortecnologico = JSON.parse(dataIN[0].data).cas_datos.PTR_IND_TEC;
            var indicadorintegridad = JSON.parse(dataIN[0].data).cas_datos.PTR_IND_INT;
            var indicadorhistcult = JSON.parse(dataIN[0].data).cas_datos.PTR_IND_HIST;
            var indicadorsimbolico = JSON.parse(dataIN[0].data).cas_datos.PTR_IND_SIMB;
            var nombrevia = JSON.parse(dataIN[0].data).cas_datos.PTR_NOM_VIA;
            var materiavia = JSON.parse(dataIN[0].data).cas_datos.PTR_MAT_VIA;
            var clasificacion = JSON.parse(dataIN[0].data).cas_datos.PTR_CLASS;
            var entrecalles = JSON.parse(dataIN[0].data).cas_datos.PTR_ENT_CALL; 

            var indicadorurbano = caracteresEspecialesActualizar(indicadorurbano);
            var indicadorartistico = caracteresEspecialesActualizar(indicadorartistico);
            var indicadortipologico = caracteresEspecialesActualizar(indicadortipologico);
            var indicadortecnologico = caracteresEspecialesActualizar(indicadortecnologico);
            var indicadorintegridad = caracteresEspecialesActualizar(indicadorintegridad);
            var indicadorhistcult = caracteresEspecialesActualizar(indicadorhistcult);
            var indicadorsimbolico = caracteresEspecialesActualizar(indicadorsimbolico);
            $( "#acas_id" ).val(cas_id);
            $( "#aconnumero_ficha" ).val(numero_ficha);
            $( "#aconcodigo_ficha" ).val(codigo_ficha);
            $( "#ag_tipo" ).val(g_tipo);
            $( "#anominacion" ).val(nominacion);
            $( "#aubicacion" ).val(ubicacion);
            $( "#atipoproteccion" ).val(tipoproteccion);
            $( "#anroproteccion" ).val(nroproteccion);
            $( "#anrototalinmuebletramo" ).val(nrototalinmuebletramo);
            $( "#anrototalinmueblepatri" ).val(nrototalinmueblepatri);
            $( "#aindicadorurbano" ).val(indicadorurbano);
            $( "#aindicadorartistico" ).val(indicadorartistico);
            $( "#aindicadortipologico" ).val(indicadortipologico);
            $( "#aindicadortecnologico" ).val(indicadortecnologico);
            $( "#aindicadorintegridad" ).val(indicadorintegridad);
            $( "#aindicadorhistcult" ).val(indicadorhistcult);
            $( "#aindicadorsimbolico" ).val(indicadorsimbolico);
            $( "#anombrevia" ).val(nombrevia);
            $( "#amateriavia" ).val(materiavia);
            $( "#aclasificacion" ).val(clasificacion);
            $( "#aentrecalles" ).val(entrecalles);    
      
           },
        error: function(result) {
          swal( "Error..!", "Error....", "error" );
        }
      });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
      });
};

$("#actualizarConjuntosPatrimoniales").click(function(){

  var cas_id = $( "#acas_id" ).val();
  var PTR_NOM = $( "#anominacion" ).val();
  var PTR_UBIC = $( "#aubicacion" ).val();
  var PTR_TIP_PROT = $( "#atipoproteccion" ).val();
  var PTR_NRO_PROT = $( "#anroproteccion" ).val();
  var PTR_NUM_INM = $( "#anrototalinmuebletramo" ).val();
  var PTR_NUM_PAT = $( "#anrototalinmueblepatri" ).val();
  var PTR_IND_URB = $( "#aindicadorurbano" ).val();
  var PTR_IND_ART = $( "#aindicadorartistico" ).val();
  var PTR_IND_TIP = $( "#aindicadortipologico" ).val();
  var PTR_IND_TEC = $( "#aindicadortecnologico" ).val();
  var PTR_IND_INT = $( "#aindicadorintegridad" ).val();
  var PTR_IND_HIST = $( "#aindicadorhistcult" ).val();
  var PTR_IND_SIMB = $( "#aindicadorsimbolico" ).val();
  var PTR_NOM_VIA = $( "#anombrevia" ).val();
  var PTR_MAT_VIA = $( "#amateriavia" ).val();
  var PTR_CLASS = $( "#aclasificacion" ).val();
  var PTR_ENT_CALL = $( "#aentrecalles" ).val();

  var g_tipo = 'PTR-CON';
  var PTR_IND_URB = caracteresEspecialesRegistrar(PTR_IND_URB);
  var PTR_IND_ART = caracteresEspecialesRegistrar(PTR_IND_ART);
  var PTR_IND_TIP = caracteresEspecialesRegistrar(PTR_IND_TIP);
  var PTR_IND_TEC = caracteresEspecialesRegistrar(PTR_IND_TEC);
  var PTR_IND_INT = caracteresEspecialesRegistrar(PTR_IND_INT);
  var PTR_IND_HIST = caracteresEspecialesRegistrar(PTR_IND_HIST);
  var PTR_IND_SIMB = caracteresEspecialesRegistrar(PTR_IND_SIMB);

  var data = '{"g_tipo":"'+g_tipo+'","PTR_NOM":"'+PTR_NOM+'","PTR_UBIC":"'+PTR_UBIC+'","PTR_TIP_PROT":"'+PTR_TIP_PROT+'","PTR_NRO_PROT":"'+PTR_NRO_PROT+'","PTR_NUM_INM":"'+PTR_NUM_INM+'","PTR_NUM_PAT":"'+PTR_NUM_PAT+'","PTR_IND_URB":"'+PTR_IND_URB+'","PTR_IND_ART":"'+PTR_IND_ART+'","PTR_IND_TIP":"'+PTR_IND_TIP+'","PTR_IND_TEC":"'+PTR_IND_TEC+'","PTR_IND_INT":"'+PTR_IND_INT+'","PTR_IND_HIST":"'+PTR_IND_HIST+'","PTR_IND_SIMB":"'+PTR_IND_SIMB+'","PTR_NOM_VIA":"'+PTR_NOM_VIA+'","PTR_MAT_VIA":"'+PTR_MAT_VIA+'","PTR_CLASS":"'+PTR_CLASS+'","PTR_ENT_CALL":"'+PTR_ENT_CALL+'"}';

  var data = JSON.stringify(data);
  //////////console.log("datadata",data);
  var cas_nro_caso = $( "#aconnumero_ficha" ).val();
  var cas_nombre_caso = $( "#aconcodigo_ficha" ).val();
  //var cas_nro_caso = 11 ;
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  //var cas_nombre_caso = 'PTR-CON77/2018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-943","parametros":'{"xcas_id":'+cas_id+',"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+data+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  //////////console.log(formData,"formData");

  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" );          
        buscarPatrimonioPorTipo(1); 
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 
});



/*-----------------END CONJUNTOS PATRIMONIALES-----------------------*/
function darBaja(id, g_tipo)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-934","parametros": '{"xcas_id":'+id+', "xcas_usr_id":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
           buscarPatrimonioPorTipo(1); 
           swal("Patrimonio!", "Fue eliminado correctamente!", "success");

         },
         error: function( result ) 
         {
          swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SISTEMA_VALLE-356","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
       ////////console.log(dataIN);
       var primerNombre = dataIN[0].vprs_nombres;
       var primerApellido = dataIN[0].vprs_paterno;
       var segundoApellido = dataIN[0].vprs_materno;
       var cedulaIdentidad = dataIN[0].vprs_ci;
       var razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
       
       $("#ccreador_p").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreadorM_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreadorS_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#cactualizador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreador").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#cautorizador").val(primerNombre+' '+primerApellido+' '+segundoApellido);  

     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};

$(document).ready(function (){


 selectCampos();
 buscarPatrimonioPorTipo(1);

 listarTipoPatrimonioCombo();
 cargarDatosUsuario();
 document.getElementById("cmacrodistritoespabie").length=1;
 document.getElementById("amacrodistritoespabie").length=0;

});

//-----------------GRILLAS CONJUNTOS OPATRIMONIALES-------------
/******************-------Cantidad de Inmuebles **********************/
    var contCodigoCANIN=2;
    var arrCodigoCANIN=[];
    var arrDibujoCANIN=[];

     htmlEleCANIN = 'INMUEBLE CAT. Y CONDICIÓN DE PROTECCIÓN:</label>'+
    '<input type="text" class="form-control" id="imn_cat1" name="imn_cat1">'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">CON PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_prot1" name="inm_prot1">'+              
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">SIN PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_des1" name="inm_des1">'+           
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">TOTAL:</label>'+
    '<input type="text" class="form-control" id="inm_total1" name="inm_total1">'+             
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaCANIN(1);"></i>'+
    '</a></div>';
    var cadenaCANIN = '';  

    arrCodigoCANIN.push({arg_imn_cat:'', arg_inm_prot:'', arg_inm_des:'', arg_total:''});  
    arrDibujoCANIN.push({scd_codigo:htmlEleCANIN ,id: 1});

   
         
    cadenaCANIN='<div class="form-group col-md-5"><label class="control-label">1.'+arrDibujoCANIN[0].scd_codigo;
    ////console.log("cadenaCANIN",cadenaCANIN);
    $('#crear_cantidad_inmuebles').html(cadenaCANIN);
    $("#imn_cat1").val(arrCodigoCANIN[0].arg_imn_cat);
    $("#inm_prot1").val(arrCodigoCANIN[0].arg_inm_prot);
    $("#inm_des1").val(arrCodigoCANIN[0].arg_inm_des);
    $("#inm_total1").val(arrCodigoCANIN[0].arg_inm_total);
    //$('#grillaPrueba').val(JSON.stringify(arrCodigoEleComVanosVenInteriores));

    function crearCantidadInmuebles(){
   
    var htmlEleCANIN;
    htmlEleCANIN='INMUEBLE CAT. Y CONDICIÓN DE PROTECCIÓN:</label>'+
    '<input type="text" class="form-control" id="imn_cat'+contCodigoCANIN+'" name="imn_cat'+contCodigoCANIN+'">'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">CON PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_prot'+contCodigoCANIN+'" name="inm_prot'+contCodigoCANIN+'">'+              
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">SIN PROTECCION:</label>'+
    '<input type="text" class="form-control" id="inm_des'+contCodigoCANIN+'" name="inm_des'+contCodigoCANIN+'">'+           
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">TOTAL:</label>'+
    '<input type="text" class="form-control" id="inm_total'+contCodigoCANIN+'" name="inm_total'+contCodigoCANIN+'">'+             
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaCANIN('+contCodigoCANIN+');"></i>'+
    '</a></div>';
        //RECUPERA VALORES EN LA GRILLA
        var cadenaCANIN='';
        var cont=0;
        arrDibujoCANIN.forEach(function (item, index, array) 
        {
          arrCodigoCANIN[cont].arg_imn_cat = $("#imn_cat"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_prot = $("#inm_prot"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_des = $("#inm_des"+item.id+"").val();
          arrCodigoCANIN[cont].arg_inm_total = $("#inm_total"+item.id+"").val();
          cont++;
        });

            
        arrCodigoCANIN.push({arg_imn_cat:'', arg_inm_prot:'', arg_inm_des:'', arg_total:''});  
        arrDibujoCANIN.push({scd_codigo:htmlEleCANIN,id:contCodigoCANIN});

        contCodigoCANIN++;
        cont=1;
        //CONCATENA HTML GRILLA EN LA VISTA

        for (var i=0; i<arrDibujoCANIN.length; i++)
        {
           //console.log("arrDibujoCANINXXXXXXXXX",arrDibujoCANIN[i].scd_codigo);
          cadenaCANIN=cadenaCANIN +'<div class="form-group col-md-5"><label class="control-label">'+cont+'.'+arrDibujoCANIN[i].scd_codigo;
          cont++;
        }
         
        //vero
        //console.log("cadenaCANIN",cadenaCANIN);
        $('#crear_cantidad_inmuebles').html(cadenaCANIN);

        //MUESTRA VALORES EN LA GRILLA
        var conto=0;
        arrDibujoCANIN.forEach(function (item, index, array) 
        { 
        $("#imn_cat"+item.id+"").val(arrCodigoCANIN[conto].arg_imn_cat);
        $("#inm_prot"+item.id+"").val(arrCodigoCANIN[conto].arg_inm_prot);
        $("#inm_des"+item.id+"").val(arrCodigoCANIN[conto].arg_inm_des);
        $("#inm_total"+item.id+"").val(arrCodigoCANIN[conto].arg_inm_total);
          conto=conto+1;
        });

        ////console.log("arrCodigoCANIN",arrCodigoCANIN);
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo));
    }

    function menosLineaCANIN($id){
        if(contCodigoCANIN>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaCANIN='';
                var cont=0;
                var pos=-1;
                arrDibujoCANIN.forEach(function (item, index, array) 
                {
                    arrCodigoCANIN[cont].arg_imn_cat = $("#imn_cat"+item.id+"").val();
                    arrCodigoCANIN[cont].arg_inm_prot = $("#inm_prot"+item.id+"").val();
                    arrCodigoCANIN[cont].arg_inm_des = $("#inm_des"+item.id+"").val();
                    arrCodigoCANIN[cont].arg_inm_total = $("#inm_total"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoCANIN.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoCANIN.splice(pos,1);
                arrDibujoCANIN.splice(pos,1);
                ////console.log("arrCodigo",arrCodigoCANIN);
                cont=1;
                for (var i=0; i<arrDibujoCANIN.length;i++)
                {
                    cadenaCANIN=cadenaCANIN + '<div class="form-group col-md-5"><label class="control-label">'+cont+arrDibujoCANIN[i].scd_codigo;
                    cont++;
                }                
                $('#crear_cantidad_inmuebles').html(cadenaCANIN);
                cont=0;
                arrDibujoCANIN.forEach(function (item, index, array) 
                {
                  $("#imn_cat"+item.id+"").val(arrCodigoCANIN[cont].arg_imn_cat);
                  $("#inm_prot"+item.id+"").val(arrCodigoCANIN[cont].arg_inm_prot);
                  $("#inm_des"+item.id+"").val(arrCodigoCANIN[cont].arg_inm_des);
                  $("#inm_total"+item.id+"").val(arrCodigoCANIN[cont].arg_inm_total);
                    //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
       
    }

/******************-------Cantidad de Inmuebles **********************/
</script>

<script  src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script >

  $('textarea').ckeditor();


</script>
@endpush