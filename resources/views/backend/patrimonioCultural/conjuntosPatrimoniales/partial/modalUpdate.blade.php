<div class="modal fade modal-default" id="myUpdateConjuntoPatrimonial" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical conjuntopatrimonial-validate','id'=>'formNuevaFicha_update'])!!}
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h3>
              Actualizar Ficha Conjuntos Patrimoniales
              <small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h3>
          </div>
        </section>
      </div>   
    </div>
</div>

<div  style="padding:20px">
<input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
<input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">
<input type="hidden" name="acas_id" id="acas_id">
<input type="hidden" name="aconnumero_ficha" id="aconnumero_ficha">
<input type="hidden" name="aconcodigo_ficha" id="aconcodigo_ficha">    
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       DATOS GENERALES DE IDENTIFICACIÓN:
      </label>
    </div>
</div>    
<div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Nominación:
      </label>
      <input id="anominacion" type="text" class="form-control"  placeholder="Nominación"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Ubicación:
     </label>
     <input id="aubicacion" type="text" class="form-control"  placeholder="Ubicación"> 
   </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       PROTECCIÓN LEGAL:
      </label>
    </div>
 </div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Tipo de Protección:
      </label>
      <input id="atipoproteccion" type="text" class="form-control"  placeholder="Tipo de Protección"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Nro de Protección:
     </label>
     <input id="anroproteccion" type="number" class="form-control"  placeholder="Nro de Protección"> 
   </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       INFORMACIÓN CUANTITATIVA DEL TRAMO:
      </label>
    </div>
</div>
   <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Número total de inmuebles tramo:
      </label>
      <input id="anrototalinmuebletramo" type="number" class="form-control"  placeholder="Número total de inmuebles tramo"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Número total de inmuebles patrimoniales:
     </label>
     <input id="anrototalinmueblepatri" type="number" class="form-control"  placeholder="Número total de inmuebles patrimoniales"> 
   </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       INFORMACIÓN CUALITATIVA DEL CONJUNTO:
      </label>
    </div>
 </div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Indicador Urbano:
  </label>
<textarea rows="4" cols="50" id="aindicadorurbano" class="form-control "  placeholder="Indicador Urbano"></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Indicador Artístico:
  </label>
<textarea rows="4" cols="50" id="aindicadorartistico" class="form-control "  placeholder="Indicador Artistico"></textarea>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Indicador Tipológico:
  </label>
<textarea rows="4" cols="50" id="aindicadortipologico" class="form-control "  placeholder="Indicador Tipológico"></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
  Indicador Tecnológico:
  </label>
  <textarea rows="4" cols="50" id="aindicadortecnologico" class="form-control "  placeholder="Indicador Tecnológico"> </textarea>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Indicador de Integridad:
  </label>
 <textarea rows="4" cols="50" id="aindicadorintegridad" class="form-control "  placeholder="Indicador de Integridad"> </textarea>
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Indicador Histórico Cultural:
  </label>
  <textarea rows="4" cols="50" id="aindicadorhistcult" class="form-control "  placeholder="Indicador Histórico Cultural"> </textarea>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Indicador Simbólico:
  </label>
 <textarea rows="4" cols="50" id="aindicadorsimbolico" class="form-control "  placeholder="Indicador Simbólico"></textarea> 
</div>
<div class="form-group col-md-6">
  
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       IDENTIFICACIÓN DE TRAMOS:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Nombre de la Vía:
 </label>
 <input id="anombrevia" type="text" class="form-control"  placeholder="Nombre de la Vía"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Material de la Vía:
  </label>
  <input id="amateriavia" type="text" class="form-control"  placeholder="Material de la Vía"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Clasificación:
 </label>
 <input id="aclasificacion" type="text" class="form-control"  placeholder="Clasificación"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Entre Calles:
  </label>
  <input id="aentrecalles" type="text" class="form-control"  placeholder="Entre Calles"> 
</div>
</div>
<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
  <a type = "button" class = "btn btn-primary"  id="actualizarConjuntosPatrimoniales" name="actualizarConjuntosPatrimoniales" data-dismiss="modal"><i class="fa fa-save"></i>Actualizar</a>        
</div> 
</div>
{!! Form::close() !!}
</div>
</div>
</div>
