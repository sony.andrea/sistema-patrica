<div class="modal fade modal-default" id="myCreateConjuntoPatrimonial" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical puestoMercado-validate','id'=>'formNuevaFicha_create'])!!}
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h3>
              Nueva Ficha Conjuntos Patrimoniales
              <small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h3>
          </div>
        </section>
      </div>   
    </div>
    
</div>
<div  style="padding:20px">
<input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
<input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">    
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       DATOS GENERALES DE IDENTIFICACIÓN:
      </label>
    </div>
</div>    
<div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Nominación:
      </label>
      <input id="cnominacion" type="text" class="form-control"  placeholder="Nominación"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Ubicación:
     </label>
     <input id="cubicacion" type="text" class="form-control"  placeholder="Ubicación"> 
   </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       PROTECCIÓN LEGAl:
      </label>
    </div>
 </div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Tipo de Protección:
      </label>
      <input id="ctipoproteccion" type="text" class="form-control"  placeholder="Tipo de Protección"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Nro de Proteccion:
     </label>
     <input id="cnroproteccion" type="number" class="form-control"  placeholder="Nro de Protección"> 
   </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       INFORMACIÓN CUANTITATIVA DEL TRAMO:
      </label>
    </div>
</div>
   <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Número total de inmuebles tramo:
      </label>
      <input id="cnrototalinmuebletramo" type="number" class="form-control"  placeholder="Número total de inmuebles tramo"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Número total de inmuebles patrimoniales:
     </label>
     <input id="cnrototalinmueblepatri" type="number" class="form-control"  placeholder="Número total de inmuebles patrimoniales"> 
   </div>
</div>

<!--GRILLA INICIO-->

 <div class="col-md-12">
              <h4><center>Cantidad de Inmuebles:</center></h4> 
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Cantidad de Inmuebles" onclick="crearCantidadInmuebles();"></i>
                  </a>
                </div>
              </small>
             <div id="crear_cantidad_inmuebles"></div>
            </div>
            
<!--GRILLA FINAL -->

<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       INFORMACIÓN CUALITATIVA DEL CONJUNTO:
      </label>
    </div>
 </div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Indicador Urbano:
  </label>
  <textarea rows="4" cols="50" id="cindicadorurbano" class="form-control "  placeholder="Indicador Urbano"></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
  Indicador Artístico:
  </label>
  <textarea rows="4" cols="50" id="cindicadorartistico" class="form-control "  placeholder="Indicador Artistico"></textarea> 
</div>
</div>

<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Indicador Tipológico:
  </label>
  <textarea rows="4" cols="50" id="cindicadortipologico" class="form-control "  placeholder="Indicador Tipologico"></textarea>  
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
  Indicador Tecnológico:
  </label>
 
   <textarea rows="4" cols="50" id="cindicadortecnologico" class="form-control "  placeholder="Indicador Tecnologico"></textarea>  
</div>
</div>

<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Indicador de Integridad:
  </label>
  <textarea rows="4" cols="50" id="cindicadorintegridad" class="form-control "  placeholder="Indicador de Integridad"></textarea>  
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
  Indicador Histórico Cultural:
  </label>
  <textarea rows="4" cols="50" id="cindicadorhistcult" class="form-control "  placeholder="Indicador Historico Cultural"></textarea>  
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Indicador Simbólico:
  </label>
  <textarea rows="4" cols="50" id="cindicadorsimbolico" class="form-control "  placeholder="Indicador de Integridad"></textarea>  
</div>
</div>

<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       IDENTIFICACIÓN DE TRAMOS:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Nombre de la Vía:
 </label>
 <input id="cnombrevia" type="text" class="form-control"  placeholder="Nombre de la Vía"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Material de la Vía:
  </label>
  <input id="cmateriavia" type="text" class="form-control"  placeholder="Material de la Vía"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Clasificación:
 </label>
 <input id="cclasificacion" type="text" class="form-control"  placeholder="Clasificacion"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Entre Calles:
  </label>
  <input id="centrecalles" type="text" class="form-control"  placeholder="Entre Calles"> 
</div>
</div>
<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" type="button">
  <i class="fa fa-close"></i> Cancelar</button>
  <button class="btn btn-default "  type="reset">
    <i class="fa fa-eraser"></i> Limpiar
  </button>
  <a type = "button" class = "btn btn-primary"  id="guardarConjuntosPatrimoniales" name="guardarConjuntosPatrimoniales" data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
</div> 
</div>
{!! Form::close() !!}
</div>
</div>
</div>
