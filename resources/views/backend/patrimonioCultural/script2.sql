-- Function: patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text)

-- DROP FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text);

CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(
    IN yid_doccasos integer,
    IN yg_tipo text)
  RETURNS TABLE(xdoc_idd integer, xdoc_id integer, xdoc_nombre text, xdoc_url_logica text, xdoc_datos text, xdoc_titulo text, xdoc_cuerpo text, xdoc_palabras text, xdoc_correlativo text) AS
$BODY$

BEGIN     

 IF yg_tipo = 'PTR-IME' or  yg_tipo = 'PTR-CBI' THEN
   RETURN QUERY    
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
        or cast(doc_correlativo as integer)= 4 );

  return;
  end if;

  IF yg_tipo = 'PTR-ESP' or  yg_tipo = 'PTR-CON'  THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
       );
  return;
  end if;
  
    IF yg_tipo = 'PTR-PCI'  THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
       );
  return;
  end if;
  
    IF yg_tipo = 'PTR-BMA'  THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
     
       );
  return;
  end if;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text)
  OWNER TO postgres;
  ==============================================================================
  SERVICIO_VALLE-927


$_conn1=coneccionBD("18");
$_tipo1="Select";
$_query1="select * from patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1($xid_doccasos, '$xg_tipo')";
$_dataDB1=ejecutar_Query($_conn1,$_query1,$_tipo1);
echo($_dataDB1);

  CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(
    IN yid_doccasos integer,
    IN yg_tipo text)
  RETURNS TABLE(xdoc_idd integer, xdoc_id integer, xdoc_nombre text, xdoc_url_logica text, xdoc_datos text, xdoc_titulo text, xdoc_cuerpo text, xdoc_palabras text, xdoc_correlativo text) AS
$BODY$

BEGIN     

 IF yg_tipo = 'PTR-IME' or yg_tipo = 'PTR-CBI' THEN
   RETURN QUERY    
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6 
        or cast(doc_correlativo as integer) = 7 
        or cast(doc_correlativo as integer) = 8
        or cast(doc_correlativo as integer) = 9
        or cast(doc_correlativo as integer) = 10);

  return;
  end if;

  IF yg_tipo = 'PTR-ESP' or  yg_tipo = 'PTR-CON' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 4 
        or cast(doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6 
        or cast(doc_correlativo as integer) = 7
        or cast(doc_correlativo as integer) = 8
        or cast(doc_correlativo as integer) = 9
       );
  return;
  end if;

   IF yg_tipo = 'PTR-PCI' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 3 
        or cast(doc_correlativo as integer) = 4 
        or cast(doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6
        or cast(doc_correlativo as integer) = 7
        or cast(doc_correlativo as integer) = 8
       );
  return;
  end if;

   IF yg_tipo = 'PTR-BMA' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
        or cast(doc_correlativo as integer) = 4 
        or cast(doc_correlativo as integer) = 5
        or cast(doc_correlativo as integer) = 6
        or cast(doc_correlativo as integer) = 7
       );
  return;
  end if;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(integer, text)
  OWNER TO postgres;
-----------------------------------------------------
-----------------------------------------------------
CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(
    IN yid_doccasos integer,
    IN yg_tipo text)
  RETURNS TABLE(xdoc_idd integer, xdoc_id integer, xdoc_nombre text, xdoc_url_logica text, xdoc_datos text, xdoc_titulo text, xdoc_cuerpo text, xdoc_palabras text, xdoc_correlativo text) AS
$BODY$

BEGIN     

 IF yg_tipo = 'PTR-IME' or yg_tipo = 'PTR-CBI' THEN
   RETURN QUERY    
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6 
        or cast(doc_correlativo as integer) = 7 
        or cast(doc_correlativo as integer) = 8
        or cast(doc_correlativo as integer) = 9
        or cast(doc_correlativo as integer) = 10)
        and doc_estado = 'A'
  order by cast (doc_correlativo as integer) asc;

  return;
  end if;

  IF yg_tipo = 'PTR-ESP' or  yg_tipo = 'PTR-CON' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 4 
        or cast(doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6 
        or cast(doc_correlativo as integer) = 7
        or cast(doc_correlativo as integer) = 8
        or cast(doc_correlativo as integer) = 9
       )
       and doc_estado = 'A'
       order by cast (doc_correlativo as integer) asc;
  return;
  end if;

   IF yg_tipo = 'PTR-PCI' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 3 
        or cast(doc_correlativo as integer) = 4 
        or cast(doc_correlativo as integer) = 5 
        or cast(doc_correlativo as integer) = 6
        or cast(doc_correlativo as integer) = 7
        or cast(doc_correlativo as integer) = 8
       )
       and doc_estado = 'A'
       order by cast (doc_correlativo as integer) asc;
  return;
  end if;

   IF yg_tipo = 'PTR-BMA' THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
        or cast(doc_correlativo as integer) = 4 
        or cast(doc_correlativo as integer) = 5
        or cast(doc_correlativo as integer) = 6
        or cast(doc_correlativo as integer) = 7
       )
       and doc_estado = 'A'
       order by cast (doc_correlativo as integer) asc;
  return;
  end if;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_archivo_fotografico1(integer, text)
  OWNER TO postgres;
  -------------------------------------------------
  -------------------------------------------------
  -------------------------------------------------

  -- Function: patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text)

-- DROP FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text);

CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(
    IN yid_doccasos integer,
    IN yg_tipo text)
  RETURNS TABLE(xdoc_idd integer, xdoc_id integer, xdoc_nombre text, xdoc_url_logica text, xdoc_datos text, xdoc_titulo text, xdoc_cuerpo text, xdoc_palabras text, xdoc_correlativo text) AS
$BODY$

BEGIN     

 IF yg_tipo = 'PTR-IME' or  yg_tipo = 'PTR-CBI' THEN
   RETURN QUERY    
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
        or cast(doc_correlativo as integer)= 4 )
        and doc_estado = 'A'
  order by cast (doc_correlativo as integer) asc;

  return;
  end if;

  IF yg_tipo = 'PTR-ESP' or  yg_tipo = 'PTR-CON'  THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
        or cast(doc_correlativo as integer) = 3 
       )
       and doc_estado = 'A'
       order by cast (doc_correlativo as integer) asc;
  return;
  end if;
  
    IF yg_tipo = 'PTR-PCI'  THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
        or cast(doc_correlativo as integer) = 2 
       )
       and doc_estado = 'A'
       order by cast (doc_correlativo as integer) asc;
  return;
  end if;
  
    IF yg_tipo = 'PTR-BMA'  THEN 
  RETURN QUERY 
  SELECT 
  doc_idd,
  doc_id,
  doc_nombre,
  doc_url_logica,
  doc_datos,
  doc_titulo,
  doc_cuerpo,
  doc_palabras,
  doc_correlativo
  FROM patrimonio_cultural.dms_gt_documentos
  where doc_id=yid_doccasos and (cast (doc_correlativo as integer) = 1 
     
       )
  and doc_estado = 'A'
  order by cast (doc_correlativo as integer) asc;
  return;
  end if;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION patrimonio_cultural.sp_obtener_dms_doc_casos_patrimonio_ficha1(integer, text)
  OWNER TO postgres;
