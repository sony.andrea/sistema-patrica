@extends('backend.template.app')
@section('main-content')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalCreate')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalCreate1')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalUpdate')
@include('backend.patrimonioCultural.bienesInmuebles.partial.modalUpdate1')
<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>BUSQUEDA DE FICHAS</b></div>
    <div class="panel-body">     
      <br>
      <div class="col-md-12">
       <div class="form-group col-md-4">
        <label class="control-label">
          Tipo Patrimonio Cultural:
        </label>
       <input id="tipoPatriCultural" type="text" class="form-control" value="BIENES INMUEBLES" disabled>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Campo:
        </label>
        <select class="form-control" name="camposPorPatrimonio" id="camposPorPatrimonio">
          <option value="-1">Seleccionar...</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Valor:
        </label>
        <input type="text" id="valorPatrimonio" class="form-control" name="valorPatrimonio">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="container">
        <div class="col-md-6"></div>
        <div class="col-md-6"> 
          <button type ="button" class = "btn btn-primary" onclick="buscarPatrimonioTipoCampo(1)"> <i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-primary" onclick="limpiarPrincipal()"> <i class="fa fa fa-eraser"></i> Limpiar</button>
          <button type="button" class="btn btn-primary" data-target="#myCreateBienesInmueble" 
          data-toggle="modal"><i class="fa fa-plus"></i> Nueva Ficha</button>       
     </div>
   </div>
 </div>
</div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-body">
        <div id="listBienesInmueble">
        </div>  
      </div>
    </div>
  </div>
</div>
</section>
@endsection
@push('scripts')
<script>  
  var _usrid = {{$usuarioId}};
  var datosUsuario = "";
  var correlativog="";
  var puntuacionEditar = 0;
  var f = new Date();
  var fechaActual = (f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
  $("#cfecha_creador_in").val(fechaActual);
  $("#afecha_modificador_in").val(fechaActual);
  function getBool (val){
  return !!JSON.parse(String(val).toLowerCase());
  }
  function refrescarPantalla(){
  location.reload();
  }
  
 function buscarPatrimonioPorTipo(paging_pagenumbe)
  { 
    document.getElementById("listado_campo_valor").style = "display:none;";
    document.getElementById("listado_principal").style = "display:block;";
    var paging_pagesize = $( "#reg" ).val(); 
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1127',"parametros": '{"paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'}; 
    var band = 'list_principal';
    buscar(formData, band);

  }

function buscarPatrimonioTipoCampo(paging_pagenumbe){
    document.getElementById("listado_campo_valor").style = "display:block;";
    document.getElementById("listado_principal").style = "display:none;";
    var paging_pagesize = $( "#reg_campo" ).val(); 
    var campoPatrimonio = $( '#camposPorPatrimonio' ).val();
    var valorPatrimonio = $( '#valorPatrimonio' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1129',"parametros": '{"xcampo_id":' +campoPatrimonio+ ', "xcampo_valor":"' +valorPatrimonio+ '","xpaging_pagesize":'+paging_pagesize+',"xpaging_pagenumber":'+paging_pagenumbe+'}'};  
    var band = 'list_por_campo'; 
    buscar(formData, band);  
  }
    function limpiarPrincipal()
  {
    $( '#camposPorPatrimonio' ).val("-1");
    $( '#valorPatrimonio' ).val("");
  }


function selectCampos()
{  
  var tipoCampoPatrimonio = 6;
  document.getElementById("camposPorPatrimonio").length=1;
  
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-931',"parametros": '{"ytipoid":'+tipoCampoPatrimonio+'}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        
        for(var i = 0; i < dataIN.length; i++){
         var campo_id = dataIN[i].xcampo_id;
         var campo_tipo = JSON.parse(dataIN[i].xcampo_data).campoTipo;   
         document.getElementById("camposPorPatrimonio").innerHTML += '<option value=' +campo_id+ '>' +campo_tipo+ '</option>';            
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
}


function obtenercorrelativo(tipoPatrimonio)
{
 $.ajax({
  type        : 'GET',
  url         : urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": 'SERVICIO_PATRIMONIO-936',"parametros":'{"xtipoPat":"'+tipoPatrimonio+'"}'};
    
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
      var data=dataIN;
      correlativog = dataIN[0].sp_correlativo_patrimonio4;       
    

  },     
  error: function (xhr, status, error) { }
});
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

}

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SERVICIO_PATRIMONIO-1023","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
       var primerNombre = dataIN[0].vprs_nombres;
       var primerApellido = dataIN[0].vprs_paterno;
       var segundoApellido = dataIN[0].vprs_materno;
       var cedulaIdentidad = dataIN[0].vprs_ci;
       var razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
       $("#ccreador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#amodificador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
      datosUsuario = primerNombre+' '+primerApellido+' '+segundoApellido;
     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};

function darBaja(id)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-934","parametros": '{"xcas_id":'+id+', "xcas_usr_id":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
            buscarPatrimonioPorTipo(1);
           swal("Patrimonio!", "Fue eliminado correctamente!", "success");

         },
         error: function( result ) 
         {
          swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

function buscar(formData, band){

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-patrimonio" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Nro de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código Catastral</th>';
    htmlListaBus += '<th align="left" class="form-natural">Denominación</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Fecha</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Adjuntos</th>' ;
    htmlListaBus += '<th width="12%"align="center" class="form-natural">Impresiones</th></tr>' ;
    htmlListaBus += '</thead>';

    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          var datos = dataIN;
          var tam = datos.length;        
        if(dataIN == "[{ }]"){
          tam = 0;
        }; 
        var tipoContribuyenteEnvio = $( "#tipo-contribuyente" ).val();
        for (var i = 0; i < tam; i++){ 
        var dataP = JSON.parse(dataIN[i].data);
        var cas_id = dataP.cas_id;
        var numero_ficha = dataP.cas_nro_caso;
        var g_tipo='PTR_CBI';  
        var nroFicha=dataP.cas_nro_caso;
        var denominacion = dataP.ptr_act;
        var codigo_ficha = dataP.cas_nombre_caso;
        var codigo_catastral = dataP.ptr_cod_cat;
        var fecha_registro = dataP.cas_registrado;
        var estado_ = dataP.cas_estado;
        fecha_registro = fecha_registro.split('T');
        fecha_registro = fecha_registro[0];
        if ( numero_ficha == null) {
        numero_ficha = '';
        }
        if ( denominacion == null) {
        denominacion = '';                  
        }
        if ( codigo_ficha == null) {
        codigo_ficha = '';
        }
        if ( codigo_catastral == null) {
        codigo_catastral = '';
        }
        if ( denominacion == 'PTR_DEN_ACT') {
        denominacion = '';                  
        }
        htmlListaBus += '<tr>';
        htmlListaBus += '<td align="left">'+(i+1)+'</td>';
        htmlListaBus += '<td align="center">';
        htmlListaBus += '<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="tooltip" data-placement="bottom" title="Eliminar" type="button" onClick= "darBaja(' + cas_id +');">';
        htmlListaBus += '<i class="glyphicon glyphicon-trash"></i></button>';

        htmlListaBus += '<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateBienesInmueble" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick="editarBienesInmuebles('+cas_id+');"><i class="glyphicon glyphicon-pencil"></i></button>';
        htmlListaBus += '</td>';
        htmlListaBus += '<td align="left" class="form-natural">'+numero_ficha+'</td>'; 
        htmlListaBus += '<td align="left" class="form-natural">'+codigo_ficha+'</td>';                           
        htmlListaBus += '<td align="left" class="form-natural">'+codigo_catastral+'</td>'; 
        htmlListaBus += '<td align="left" class="form-natural">'+denominacion+'</td>';                           
        htmlListaBus += '<td align="left" class="form-natural">'+fecha_registro+'</td>';
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', cas_id);
        url = url.replace('numeroficha1', numero_ficha);
        htmlListaBus+='<td style="text-align: center;"><a href="'+url+'" class="btncirculo btn-xs btn-default" fa fa-plus-square pull-right"  > <i class="fa fa-folder"></i></a>';
        htmlListaBus+='</td>';
        htmlListaBus+='<td align="center">';
        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Ficha" type="button" onClick= "generarProforma(' + cas_id +');">';
        htmlListaBus+='<i class="glyphicon glyphicon-print"></i></button> &nbsp;';
        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Archivo Fotografico" type="button" onClick= "imprimirArchivoFotografico(' + cas_id +','+numero_ficha+');">';
        htmlListaBus+='<i class="glyphicon glyphicon-camera"></i></button> &nbsp;';
        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Todo" type="button" onClick= "imprimirFichaTodo(' + cas_id +');">';
        htmlListaBus+='<i class="glyphicon glyphicon-list"></i></button>';
        htmlListaBus+='</td>';
        htmlListaBus += '</tr>';
        } 
        htmlListaBus += '</table></div></div>';
        htmlListaBus +='<div>';
        htmlListaBus +='<ul class="pager">';
        if(band =='list_principal'){
        htmlListaBus +='<li><a href="#" onClick="btnAnterior_prin()">Previous</a></li>';
        htmlListaBus +='<li><a href="#" onClick="btnSiguiente_prin()">Next</a></li>';
        }else{
        htmlListaBus +='<li><a href="#" onClick="btnAnterior_campo_valor()">Previous</a></li>';
        htmlListaBus +='<li><a href="#" onClick="btnSiguiente_campo_valor()">Next</a></li>';  
        }
        htmlListaBus+='</ul>';
        htmlListaBus +='</div>';
        $( '#listBienesInmueble' ).html(htmlListaBus);
      },
      error: function(result) {
        swal( "Error..!", "Error....", "error" );
      }
    });
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});
};

 function editarBienesInmuebles(cas_id){

    var formData = {"identificador": "SERVICIO_PATRIMONIO-1016", "parametros":'{"id_caso":'+cas_id+'}'};

    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {

          var idCaso = JSON.parse(dataIN[0].data).cas_id;
          var nroFicha = JSON.parse(dataIN[0].data).cas_nro_caso;
          var dataP = JSON.parse(dataIN[0].data);
          var g_tipo = dataP.cas_datos.g_tipo;           
          var g_tipoArq = dataP.cas_datos.TIPO_ARQ; 
          var cas_nombre_caso = JSON.parse(dataIN[0].data).cas_nombre_caso;
          var PTR_CREA = "";
          if(typeof dataP.cas_datos.PTR_CREA !== 'undefined'){
          PTR_CREA = dataP.cas_datos.PTR_CREA;
          } 
          else {
          PTR_CREA = datosUsuario;
          }
          var PTR_FEC_CREA = "";
          if(typeof dataP.cas_datos.PTR_FEC_CREA !== 'undefined'){
          PTR_FEC_CREA = dataP.cas_datos.PTR_FEC_CREA;
          } 
          else {PTR_FEC_CREA = fechaActual; }
          var PTR_COD_CAT = dataP.cas_datos.PTR_COD_CAT; 
          var PTR_COD_CAT_HIS1 = dataP.cas_datos.PTR_COD_CAT_HIS1; 
          var PTR_OR = dataP.cas_datos.PTR_OR; 
          var PTR_TRAD = dataP.cas_datos.PTR_TRAD; 
          var PTR_ACT = dataP.cas_datos.PTR_ACT;
          var PTR_DEP = "";
          if(typeof dataP.cas_datos.PTR_DEP !== 'undefined'){
            PTR_DEP = dataP.cas_datos.PTR_DEP;
          }
          else{PTR_DEP = "La Paz"; }
          var PTR_CIU = "";
          if(typeof dataP.cas_datos.PTR_CIU !== 'undefined'){
          PTR_CIU = dataP.cas_datos.PTR_CIU;
          }
          else{PTR_CIU = "La Paz"; } 
          var PTR_MUNI = "";
          if(typeof dataP.cas_datos.PTR_MUNI !== 'undefined'){
          PTR_MUNI = dataP.cas_datos.PTR_MUNI;
          }
          else {PTR_MUNI = "La Paz"; }
          var PTR_MACRO = dataP.cas_datos.PTR_MACRO; 
          var PTR_TIP_VIAS = dataP.cas_datos.PTR_TIP_VIAS; 
          var PTR_CALL_ESQ = dataP.cas_datos.PTR_CALL_ESQ; 
          var PTR_DIRE = dataP.cas_datos.PTR_DIRE; 
          var PTR_ZONA = dataP.cas_datos.PTR_ZONA; 
          var PTR_NUM1 = dataP.cas_datos.PTR_NUM1;
          var PTR_NUM2 = dataP.cas_datos.PTR_NUM2;
          var PTR_NUM3 = dataP.cas_datos.PTR_NUM3;
          var PTR_NUM4 = dataP.cas_datos.PTR_NUM4;
          var PTR_NUM5 = dataP.cas_datos.PTR_NUM5; 
          var PTR_ARE_PRED = dataP.cas_datos.PTR_ARE_PRED; 
          var PTR_ANCH_FREN = dataP.cas_datos.PTR_ANCH_FREN; 
          var PTR_FOND_PRED = dataP.cas_datos.PTR_FOND_PRED; 
          var PTR_ANCH_MUR = dataP.cas_datos.PTR_ANCH_MUR; 
          var PTR_ALT_PROM = dataP.cas_datos.PTR_ALT_PROM; 
          var PTR_ALT_FACH = dataP.cas_datos.PTR_ALT_FACH; 
          var PTR_ALT_EDIF = dataP.cas_datos.PTR_ALT_EDIF; 
          var PTR_UBIC_MAN = dataP.cas_datos.PTR_UBIC_MAN; 
          var PTR_LIN_CONS = dataP.cas_datos.PTR_LIN_CONS; 
          var PTR_TRAZ_MAN = dataP.cas_datos.PTR_TRAZ_MAN; 
          var PTR_TIP_ARQ = dataP.cas_datos.PTR_TIP_ARQ; 
          var PTR_AER_EDIF = dataP.cas_datos.PTR_AER_EDIF; 
          var PTR_MAT_VIA = dataP.cas_datos.PTR_MAT_VIA; 
          var PTR_TIP_VIA = dataP.cas_datos.PTR_TIP_VIA;
          var PTR_BLOQ_INV = "1";
          if(typeof dataP.cas_datos.PTR_BLOQ_INV !== 'undefined'){
            PTR_BLOQ_INV = dataP.cas_datos.PTR_BLOQ_INV;
          }
          var PTR_BLOQ_REAL = dataP.cas_datos.PTR_BLOQ_REAL; 
          var PTR_HOG_INV = dataP.cas_datos.PTR_HOG_INV; 
          var PTR_HOG_REAL = dataP.cas_datos.PTR_HOG_REAL; 
          var PTR_EST_GEN = dataP.cas_datos.PTR_EST_GEN; 
          var PTR_RAN_EPO = dataP.cas_datos.PTR_RAN_EPO; 
          var PTR_EST_CON = dataP.cas_datos.PTR_EST_CON; 
          var PTR_FECH_CONS = dataP.cas_datos.PTR_FECH_CONS; 
          var PTR_AUT_CON = dataP.cas_datos.PTR_AUT_CON; 
          var PTR_FEC_CONST_DOC = dataP.cas_datos.PTR_FEC_CONST_DOC; 
          var PTR_AUT_CONS = dataP.cas_datos.PTR_AUT_CONS; 
          var PTR_CONJUNTO = dataP.cas_datos.PTR_CONJUNTO; 
          var PTR_EST_VERF = dataP.cas_datos.PTR_EST_VERF; 
          var PTR_VAL_CAT = dataP.cas_datos.PTR_VAL_CAT;
          //-----------------check de puntuación-----------------------------
          var ubicacion_manzana = "";
          if(typeof dataP.cas_datos.PTR_UBIC_MANC !== 'undefined'){
          ubicacion_manzana = dataP.cas_datos.PTR_UBIC_MANC;
          document.getElementById("apuntuacion_umcheck").checked = getBool(ubicacion_manzana[1].valor);
          }
          var linea_construccion = "";
          if(typeof dataP.cas_datos.PTR_LIN_CONSC !== 'undefined'){
          linea_construccion = dataP.cas_datos.PTR_LIN_CONSC;
          document.getElementById("apuntuacion_lccheck").checked = getBool(linea_construccion[1].valor);
          }
          var trazo_manzano = "";
          if(typeof dataP.cas_datos.PTR_TRAZ_MANC !== 'undefined'){
          trazo_manzano = dataP.cas_datos.PTR_TRAZ_MANC;
          document.getElementById("apuntuacion_tmcheck").checked = getBool(trazo_manzano[1].valor);
          }
          var tipologia_arqui = "";
          if(typeof dataP.cas_datos.PTR_TIP_ARQC !== 'undefined'){
          tipologia_arqui = dataP.cas_datos.PTR_TIP_ARQC;
          document.getElementById("apuntuacion_tacheck").checked = getBool(tipologia_arqui[1].valor);
          }
          var area_edificablec = "";
          if(typeof dataP.cas_datos.PTR_AER_EDIFC !== 'undefined'){
          var area_edificablec = dataP.cas_datos.PTR_AER_EDIFC;
          document.getElementById('apuntuacion_aeccheck').checked = getBool(area_edificablec[1].valor);
          }
          var fecha_construccion = "";
          if(typeof dataP.cas_datos.PTR_FECH_CONSC !== 'undefined'){
          fecha_construccion = dataP.cas_datos.PTR_FECH_CONSC;
          document.getElementById('apuntuacion_fccheck').checked = getBool(fecha_construccion[1].valor);
          } 
          var autor_constructor = "";
          if(typeof dataP.cas_datos.PTR_AUT_CONS_C !== 'undefined'){
          autor_constructor = dataP.cas_datos.PTR_AUT_CONS_C;
          document.getElementById('apuntuacion_accheck').checked = getBool(autor_constructor[1].valor);
          }
          var fecha_cons_datosd = "";
          if(typeof dataP.cas_datos.PTR_FEC_CONST_C !== 'undefined'){
          fecha_cons_datosd = dataP.cas_datos.PTR_FEC_CONST_C;
          document.getElementById('apuntuacion_fcdccheck').checked = getBool(fecha_cons_datosd[1].valor);
          } 
          var autor_constructor2 = "";
          if(typeof dataP.cas_datos.PTR_AUT_CONSC !== 'undefined'){
          autor_constructor2 = dataP.cas_datos.PTR_AUT_CONSC; 
          document.getElementById('apuntuacion_acdccheck').checked = getBool(autor_constructor2[1].valor);
          }
          var existencia_patrimonio2 = "";
          if(typeof dataP.cas_datos.PTR_EXIST_PAT !== 'undefined'){
          existencia_patrimonio2 = dataP.cas_datos.PTR_EXIST_PAT;
          document.getElementById('apuntuacion_epmcheck').checked = getBool(existencia_patrimonio2[1].valor);
          }
          var existencia_restos_arq = "";
          if(typeof dataP.cas_datos.PTR_EXIST_ARQ !== 'undefined'){
          existencia_restos_arq = dataP.cas_datos.PTR_EXIST_ARQ;
          document.getElementById('apuntuacion_eracheck').checked = getBool(existencia_restos_arq[1].valor);
          }
          var existencia_elemen_nat = "";
          if(typeof dataP.cas_datos.PTR_EXIST_ELEMNAT !== 'undefined'){
          var existencia_elemen_nat = dataP.cas_datos.PTR_EXIST_ELEMNAT;
          document.getElementById('apuntuacion_eencheck').checked = getBool(existencia_elemen_nat[1].valor);
          };
          var ritos_mitos = "";
          if(typeof dataP.cas_datos.PTR_EXIST_RITOS !== 'undefined'){
          var ritos_mitos = dataP.cas_datos.PTR_EXIST_RITOS;
          document.getElementById('apuntuacion_rmtocheck').checked = getBool(ritos_mitos[1].valor);
          }

          if(typeof dataP.cas_datos.PTR_VAL_CAT !== 'undefined'){
          var PTR_VAL_CAT = dataP.cas_datos.PTR_VAL_CAT;
            $("#apuntuacion_in").val(PTR_VAL_CAT);
           }
           else{
            capturarCheckPuntuacionAct();
           
           }
           
          var PTR_DES_PRED = caracteresEspecialesActualizar(dataP.cas_datos.PTR_DES_PRED);
          var PTR_PREEX_EDIF = caracteresEspecialesActualizar(dataP.cas_datos.PTR_PREEX_EDIF);
          var PTR_PROP_ANT = caracteresEspecialesActualizar(dataP.cas_datos.PTR_PROP_ANT);
          var PTR_USO_ORG = caracteresEspecialesActualizar(dataP.cas_datos.PTR_USO_ORG);
          var PTR_HEC_HIST = caracteresEspecialesActualizar(dataP.cas_datos.PTR_HEC_HIST);
          var PTR_FIE_REL = caracteresEspecialesActualizar(dataP.cas_datos.PTR_FIE_REL);
          var PTR_DAT_HIST_CONJ = caracteresEspecialesActualizar(dataP.cas_datos.PTR_DAT_HIST_CONJ);
          var PTR_FUENT_DOC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_FUENT_DOC);
          var PTR_PREEX_INTER = caracteresEspecialesActualizar(dataP.cas_datos.PTR_PREEX_INTER);
          var PTR_PROP_ANT_DOC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_PROP_ANT_DOC);
          var PTR_USOS_ORG_DOC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_USOS_ORG_DOC);
          var PTR_HEC_HIS_DOC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_HEC_HIS_DOC);
          var PTR_FIE_REL_DOC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_FIE_REL_DOC);
          var PTR_DAT_HIS_DOC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_DAT_HIS_DOC);
          var PTR_FUENTE_DOC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_FUENTE_DOC);
          var PTR_INSCRIP = caracteresEspecialesActualizar(dataP.cas_datos.PTR_INSCRIP);
          var PTR_EXIST = caracteresEspecialesActualizar(dataP.cas_datos.PTR_EXIST);
          var PTR_EXIST_AR = caracteresEspecialesActualizar(dataP.cas_datos.PTR_EXIST_AR);
          var PTR_EXIST_ELEM = caracteresEspecialesActualizar(dataP.cas_datos.PTR_EXIST_ELEM);
          var PTR_EXIST_RITO = caracteresEspecialesActualizar(dataP.cas_datos.PTR_EXIST_RITO);

          $("#acas_nombre_caso1").html(cas_nombre_caso);
          $("#acas_nombre_caso2").html(cas_nombre_caso);
          $("#acasoId_in").val(idCaso);
          $("#anroficha_in").val(nroFicha);
          $("#acreador_in").val(PTR_CREA);
          $("#afecha_creador_in").val(PTR_FEC_CREA);
          $("#acodCatastral_in").val(PTR_COD_CAT);
          $("#acodCatastral1_in").val(PTR_COD_CAT_HIS1);
          $("#aoriginal_in").val(PTR_OR);
          $("#atradicional_in1").val(PTR_TRAD);
          $("#aActual_in").val(PTR_ACT);
          $("#adepartamento_in").val(PTR_DEP);
          $("#aciudad_in").val(PTR_CIU);
          $("#amunicipio_in").val(PTR_MUNI);
          $("#aMacrodistrito_in").val(PTR_MACRO);
          $("#atipoViaLocalizacion_in").val(PTR_TIP_VIAS);
          $("#acalleEsquina_in").val(PTR_CALL_ESQ);
          $("#adireccion_in").val(PTR_DIRE);
          $("#azona_in").val(PTR_ZONA);
          $("#anumero_in").val(PTR_NUM1);
          $("#anumero_in2").val(PTR_NUM2);
          $("#anumero_in3").val(PTR_NUM3);
          $("#anumero_in4").val(PTR_NUM4);
          $("#anumero_in5").val(PTR_NUM5);
          $("#aareaPredio_in").val(PTR_ARE_PRED);
          $("#aanchoFrente_in").val(PTR_ANCH_FREN);
          $("#afondoPredio_in").val(PTR_FOND_PRED);
          $("#aanchoMuro_in").val(PTR_ANCH_MUR);
          $("#aalturaInterior_in").val(PTR_ALT_PROM);
          $("#aalturaFachada_in").val(PTR_ALT_FACH);
          $("#aalturaMaxima_in").val(PTR_ALT_EDIF);
          $("#adescripcion_in").val(PTR_DES_PRED);
          $("#aubicacionManzana_in").val(PTR_UBIC_MAN);
          $("#alineaConstruccion_in").val(PTR_LIN_CONS);
          $("#atrazoManzana_in").val(PTR_TRAZ_MAN);
          $("#atipologiaArqui_in").val(PTR_TIP_ARQ);
          $("#aareaEdificable_in").val(PTR_AER_EDIF);
          $("#amaterialVia_in").val(PTR_MAT_VIA);
          $("#atipoVia_in").val(PTR_TIP_VIA);
          $("#abloqueInventario_in").val(PTR_BLOQ_INV);
          $("#abloquesReales_in").val(PTR_BLOQ_REAL);///cambiar este caso
          $("#ahogaresInventario_in").val(PTR_HOG_INV);
          $("#ahogaresReales_in").val(PTR_HOG_REAL);
          $("#aestiloGeneral_in").val(PTR_EST_GEN);
          $("#arangoEpoca_in").val(PTR_RAN_EPO);
          $("#aestadoConserva_in").val(PTR_EST_CON);
          $("#afechaConstruccion_in").val(PTR_FECH_CONS);
          $("#aautor_in").val(PTR_AUT_CON);
          $("#apreexistencia_in").val(PTR_PREEX_EDIF);
          $("#apropietarios_in").val(PTR_PROP_ANT);
          $("#ausoOriginal_in").val(PTR_USO_ORG);
          $("#ahechoHistorico_in").val(PTR_HEC_HIST);
          $("#afiestaReligiosa_in").val(PTR_FIE_REL);
          $("#adatoHistorico_in").val(PTR_DAT_HIST_CONJ);
          $("#afuente_in").val(PTR_FUENT_DOC);
          $("#afechaConstruccion_in3").val(PTR_FEC_CONST_DOC);
          $("#aautor_in3").val(PTR_AUT_CONS);
          $("#apreexistencia_in3").val(PTR_PREEX_INTER);
          $("#apropietarios_in3").val(PTR_PROP_ANT_DOC);
          $("#ausoOriginal_in3").val(PTR_USOS_ORG_DOC);
          $("#ahechoHistorico_in3").val(PTR_HEC_HIS_DOC);
          $("#afiestaReligiosa_in3").val(PTR_FIE_REL_DOC);
          $("#adatoHistorico_in3").val(PTR_DAT_HIS_DOC);
          $("#afuente_in3").val(PTR_FUENTE_DOC);
          $("#ainscripcion_in").val(PTR_INSCRIP);
          $("#aexistePatrimonio_in").val(PTR_EXIST);
          $("#aexisteResto_in").val(PTR_EXIST_AR);
          $("#aexisteElemento_in").val(PTR_EXIST_ELEM);
          $("#aritosMitos_in").val(PTR_EXIST_RITO);

          $("#aconjunto_in").val(PTR_CONJUNTO);
          $("#aestadoVerifica_in").val(PTR_EST_VERF);
          //$("#apuntuacion_in").val(PTR_VAL_CAT);
          var tradicional = "";
          if(typeof dataP.cas_datos.PTR_USO_TRAD !== 'undefined'){
          tradicional = dataP.cas_datos.PTR_USO_TRAD;
          document.getElementById("atradicional1").checked = tradicional[1].estado;
          document.getElementById("atradicional2").checked = tradicional[2].estado;
          document.getElementById("atradicional3").checked = tradicional[3].estado;
          document.getElementById("atradicional4").checked = tradicional[4].estado;
          document.getElementById("atradicional5").checked = tradicional[5].estado;
          document.getElementById("atradicional6").checked = tradicional[6].estado;
          document.getElementById("atradicional7").checked = tradicional[7].estado;
          document.getElementById("atradicional8").checked = tradicional[8].estado;
          document.getElementById("atradicional9").checked = tradicional[9].estado;
          document.getElementById("atradicional10").checked = tradicional[10].estado;
          document.getElementById("atradicional11").checked = tradicional[11].estado;
          document.getElementById("atradicional12").checked = tradicional[12].estado;
          }
          var actual = "";
          if(typeof dataP.cas_datos.PTR_USO_ACT !== 'undefined'){
          actual = dataP.cas_datos.PTR_USO_ACT;
          document.getElementById("aactual1").checked = actual[1].estado;
          document.getElementById("aactual2").checked = actual[2].estado;
          document.getElementById("aactual3").checked = actual[3].estado;
          document.getElementById("aactual4").checked = actual[4].estado;
          document.getElementById("aactual5").checked = actual[5].estado;
          document.getElementById("aactual6").checked = actual[6].estado;
          document.getElementById("aactual7").checked = actual[7].estado;
          document.getElementById("aactual8").checked = actual[8].estado;
          document.getElementById("aactual9").checked = actual[9].estado;
          document.getElementById("aactual10").checked = actual[10].estado;
          document.getElementById("aactual11").checked = actual[11].estado;
          document.getElementById("aactual12").checked = actual[12].estado;
         }
         var tendencia = "";
         if(typeof dataP.cas_datos.PTR_USO_ACT !== 'undefined'){
          tendencia = dataP.cas_datos.PTR_USO_TEN;
          document.getElementById("atendencia1").checked = tendencia[1].estado;
          document.getElementById("atendencia2").checked = tendencia[2].estado;
          document.getElementById("atendencia3").checked = tendencia[3].estado;
          document.getElementById("atendencia4").checked = tendencia[4].estado;
          document.getElementById("atendencia5").checked = tendencia[5].estado;
          document.getElementById("atendencia6").checked = tendencia[6].estado;
          document.getElementById("atendencia7").checked = tendencia[7].estado;
          document.getElementById("atendencia8").checked = tendencia[8].estado;
          document.getElementById("atendencia9").checked = tendencia[9].estado;
          document.getElementById("atendencia10").checked = tendencia[10].estado;
          document.getElementById("atendencia11").checked = tendencia[11].estado;
          document.getElementById("atendencia12").checked = tendencia[12].estado;
         }
         var aacapites = "";
         if(typeof dataP.cas_datos.PTR_ACAP1 !== 'undefined'){ 
          aacapites = dataP.cas_datos.PTR_ACAP1;
          console.log("aacapites",aacapites);
          var tamAcapites = aacapites.length;
          if(tamAcapites == 7){
          document.getElementById("aacapites1").checked = aacapites[1].estado;
          document.getElementById("aacapites2").checked = aacapites[2].estado;
          document.getElementById("aacapites3").checked = aacapites[3].estado;
          document.getElementById("aacapites4").checked = aacapites[4].estado;
          document.getElementById("aacapites5").checked = aacapites[5].estado;
          document.getElementById("aacapites6").checked = aacapites[6].estado;
         }
         if(tamAcapites == 6){
          document.getElementById("aacapites1").checked = aacapites[0].estado;
          document.getElementById("aacapites2").checked = aacapites[1].estado;
          document.getElementById("aacapites3").checked = aacapites[2].estado;
          document.getElementById("aacapites4").checked = aacapites[3].estado;
          document.getElementById("aacapites5").checked = aacapites[4].estado;
          document.getElementById("aacapites6").checked = aacapites[5].estado;
         }
         }
         var aidicadorhistorico = "";
          if(typeof dataP.cas_datos.PTR_VAL_HIST1 !== 'undefined'){
          aidicadorhistorico = dataP.cas_datos.PTR_VAL_HIST1;
          var tam_idicadorhistorico = aidicadorhistorico.length;
          if(tam_idicadorhistorico == 6){
          document.getElementById("aindicador_historico1").checked = aidicadorhistorico[1].estado;
          document.getElementById("aindicador_historico2").checked = aidicadorhistorico[2].estado;
          document.getElementById("aindicador_historico3").checked = aidicadorhistorico[3].estado;
          document.getElementById("aindicador_historico4").checked = aidicadorhistorico[4].estado;
          document.getElementById("aindicador_historico5").checked = aidicadorhistorico[5].estado;
          }
          if(tam_idicadorhistorico == 5){
          document.getElementById("aindicador_historico1").checked = aidicadorhistorico[0].estado;
          document.getElementById("aindicador_historico2").checked = aidicadorhistorico[1].estado;
          document.getElementById("aindicador_historico3").checked = aidicadorhistorico[2].estado;
          document.getElementById("aindicador_historico4").checked = aidicadorhistorico[3].estado;
          document.getElementById("aindicador_historico5").checked = aidicadorhistorico[4].estado;
          }
          }
          var aiartistico = "";
          if(typeof dataP.cas_datos.PTR_VAL_ART1 !== 'undefined'){
          aiartistico= dataP.cas_datos.PTR_VAL_ART1;
          var tam_aiartistico = aiartistico.length;
          if(tam_aiartistico == 4){ 
          document.getElementById("aindicador_artistico1").checked = aiartistico[1].estado;
          document.getElementById("aindicador_artistico2").checked = aiartistico[2].estado;
          document.getElementById("aindicador_artistico3").checked = aiartistico[3].estado;
          }
          if(tam_aiartistico == 3){ 
          document.getElementById("aindicador_artistico1").checked = aiartistico[0].estado;
          document.getElementById("aindicador_artistico2").checked = aiartistico[1].estado;
          document.getElementById("aindicador_artistico3").checked = aiartistico[2].estado;
          }
          }
          var aiarquitectonico = "";
          if(typeof dataP.cas_datos.PTR_VAL_ARQ1 !== 'undefined'){
          aiarquitectonico = dataP.cas_datos.PTR_VAL_ARQ1;
          var tam_aiarquitectonico = aiarquitectonico.length;
          if(tam_aiarquitectonico == 3){
          document.getElementById("aindicador_arquitectonico1").checked = aiarquitectonico[1].estado;
          document.getElementById("aindicador_arquitectonico2").checked = aiarquitectonico[2].estado;
          }
          if(tam_aiarquitectonico == 2){
          document.getElementById("aindicador_arquitectonico1").checked = aiarquitectonico[0].estado;
          document.getElementById("aindicador_arquitectonico2").checked = aiarquitectonico[1].estado;
          }
          }
          var aitecnologico = "";
          if(typeof dataP.cas_datos.PTR_VAL_TEC1 !== 'undefined'){
          aitecnologico = dataP.cas_datos.PTR_VAL_TEC1;
          var tam_aitecnologico = aitecnologico.length;
          if(tam_aitecnologico == 4){  
          document.getElementById("aindicador_teclogico1").checked = aitecnologico[1].estado;
          document.getElementById("aindicador_teclogico2").checked = aitecnologico[2].estado;
          document.getElementById("aindicador_teclogico3").checked = aitecnologico[3].estado;
          }
          if(tam_aitecnologico == 3){  
          document.getElementById("aindicador_teclogico1").checked = aitecnologico[0].estado;
          document.getElementById("aindicador_teclogico2").checked = aitecnologico[1].estado;
          document.getElementById("aindicador_teclogico3").checked = aitecnologico[2].estado;
          }
          }
          var aiintegridad = "";
          if(typeof dataP.cas_datos.PTR_VAL_INTE1 !== 'undefined'){
          aiintegridad = dataP.cas_datos.PTR_VAL_INTE1;
          var tam_aiintegridad = aiintegridad.length; 
          if(tam_aiintegridad == 4){ 
          document.getElementById("aindicador_integridad1").checked = aiintegridad[1].estado;
          document.getElementById("aindicador_integridad2").checked = aiintegridad[2].estado;
          document.getElementById("aindicador_integridad3").checked = aiintegridad[3].estado;
          }
          if(tam_aiintegridad == 3){ 
          document.getElementById("aindicador_integridad1").checked = aiintegridad[0].estado;
          document.getElementById("aindicador_integridad2").checked = aiintegridad[1].estado;
          document.getElementById("aindicador_integridad3").checked = aiintegridad[2].estado;
          }
          }
          var iurbano = "";
          if(typeof dataP.cas_datos.PTR_VAL_URB1 !== 'undefined'){
          iurbano = dataP.cas_datos.PTR_VAL_URB1;
          var tam_iurbano = iurbano.length; 
          if(tam_iurbano == 4){ 
          document.getElementById("aindicador_urbano1").checked = iurbano[1].estado;
          document.getElementById("aindicador_urbano2").checked = iurbano[2].estado;
          document.getElementById("aindicador_urbano3").checked = iurbano[3].estado;
          }
          if(tam_iurbano == 3){ 
          document.getElementById("aindicador_urbano1").checked = iurbano[0].estado;
          document.getElementById("aindicador_urbano2").checked = iurbano[1].estado;
          document.getElementById("aindicador_urbano3").checked = iurbano[2].estado;
          }
          }
          var iinmaterial = "";
          if(typeof dataP.cas_datos.PTR_VALORES_1 !== 'undefined'){
          iinmaterial= dataP.cas_datos.PTR_VALORES_1;
          var tam_iinmaterial = iinmaterial.length;
          if(tam_iinmaterial == 3){ 
          document.getElementById("aindicador_inmaterial1").checked = iinmaterial[1].estado;
          document.getElementById("aindicador_inmaterial2").checked = iinmaterial[2].estado;
          }
          if(tam_iinmaterial == 2){ 
          document.getElementById("aindicador_inmaterial1").checked = iinmaterial[0].estado;
          document.getElementById("aindicador_inmaterial2").checked = iinmaterial[1].estado;
          }
          }
          //----------------------Grillas Regimen de Propiedad-----------------------------//
          var grillaReferenciaH = "";
          if(typeof dataP.cas_datos.PTR_REG_PROPI !== 'undefined'){
          grillaReferenciaH = dataP.cas_datos.PTR_REG_PROPI;
          var longitudRP  = grillaReferenciaH.length;
          contCodigoRepA = longitudRP;
          editarRegimenPropiedad(longitudRP);
           for (var j = 1; j < longitudRP; j++)
            {
            $("#nombreprpRP"+j+"").val(grillaReferenciaH[j].f01_nombre_PROP_);
            $("#tpropiedaRP"+j+"").val(grillaReferenciaH[j].f01_reja_INT_);   
            } 
          //---------llenado de valores a vector de codigo -------------//
           arrDibujoRP.forEach(function (item, index, array) 
          {
            arrCodigoRP.push({arg_propietario:$("#nombreprpRP"+item.id).val(), arg_tipoP:$("#tpropiedaRP"+item.id).val()});
          });
          }
         //----------------------Grillas Figura de proteccion---------------------------------//
          var grillaFiguraProtect = "";
          if(typeof dataP.cas_datos.PTR_FIG_PROT !== 'undefined'){
          grillaFiguraProtect = dataP.cas_datos.PTR_FIG_PROT;
          var longitudFP  = grillaFiguraProtect.length;
          contCodigoAct = longitudFP;
          dibujoFiguraProteccion(longitudFP);
           for (var j = 1; j < longitudFP; j++)
            {
            var obsFiguraProtec = caracteresEspecialesActualizar(grillaFiguraProtect[j].f01_FIG_OBS);
            $("#valorA"+j+"").val(obsFiguraProtec);
            $("#tipoargA"+j+"").val(grillaFiguraProtect[j].f01_FIG_LEG_);   
            } 
        
        //---------llenado de valores a vector de codigo -------------//
        
           arrDibujoA.forEach(function (item, index, array) 
           {
            arrCodigoA.push({argvalor:$("#valorA"+item.id).val(), argtipo:$("#tipoargA"+item.id).val()});
           });
           }
          //----------------------Grillas Figura de proteccion-----------------------------//
            var refHistoricas = "";
            if(typeof dataP.cas_datos.PTR_REF_HIST !== 'undefined'){
            refHistoricas = dataP.cas_datos.PTR_REF_HIST;
            var longitudRH  = refHistoricas.length;
            contCodigorefA = longitudRH;
            editarReferenciaHistorica(longitudRH);
           for (var j = 1; j < longitudRH; j++)
            {
              var obsReferencias = "";
              if(typeof refHistoricas[j].f01_FIG_OBS !== 'undefined'){
                obsReferencias = caracteresEspecialesActualizar(refHistoricas[j].f01_FIG_OBS);
              }else if(typeof refHistoricas[j].f01_Obs_REF_ !== 'undefined'){
                obsReferencias = caracteresEspecialesActualizar(refHistoricas[j].f01_Obs_REF_);
              }
            $("#treferenciaRH"+j+"").val(refHistoricas[j].f01_ref_hist_);
            $("#observacionRH"+j+"").val(obsReferencias);   
            }
            //---------llenado de valores a vector de codigo -------------//
         
            arrDibujoRH.forEach(function (item, index, array) 
            {
              arrCodigoRH.push({arg_tipoRef:$("#treferenciaRH"+item.id).val(), arg_observacion:$("#observacionRH"+item.id).val()});
            });
           }

          var PTR_DES_BLOQ1 = dataP.cas_datos.PTR_DES_BLOQ1;
          var PTR_ID_BLOQ1 = dataP.cas_datos.PTR_ID_BLOQ1;
          var PTR_RANG_EPO1 = dataP.cas_datos.PTR_RANG_EPO1;
          var PTR_EST_BLOQ1 = dataP.cas_datos.PTR_EST_BLOQ1;
          var PTR_SIS_CONSTRUC1 = dataP.cas_datos.PTR_SIS_CONSTRUC1;
          var PTR_NRO_VIV1 = dataP.cas_datos.PTR_NRO_VIV1;
          var PTR_NRO_HOG1 = dataP.cas_datos.PTR_NRO_HOG1;
          var PTR_CATEGORIA1 = dataP.cas_datos.PTR_CATEGORIA1;
          var PTR_PUNTO = dataP.cas_datos.PTR_PUNTO;
          var PTR_ID_BLOQ1 = dataP.cas_datos.PTR_ID_BLOQ1;
          
          var PTR_VAL_HIS_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_HIS_DESC1);
          var PTR_VAL_ART_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_ART_DESC1);
          var PTR_VAL_ARQ_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_ARQ_DESC1);
          var PTR_VAL_TEC_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_TEC_DESC1);
          var PTR_VAL_INTE_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_INTE_DESC1);
          var PTR_VAL_URB_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_URB_DESC1);
          var PTR_VAL_INMAT_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_INMAT_DESC1);
          var PTR_VAL_SIMB_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_SIMB_DESC1);
          var PTR_VAL_INT_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_INT_DESC1);
          var PTR_VAL_EXT_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_EXT_DESC1);

          $("#aid_bloque").val(PTR_ID_BLOQ1);
          $("#adescripcion").val(PTR_DES_BLOQ1);
          $("#arango_epoca").val(PTR_RAN_EPO);
          $("#aestilo_bloque").val(PTR_EST_GEN);
          console.log("PTR_EST_GEN",PTR_EST_GEN);
          $("#arango_epoca_hidden").val(PTR_RANG_EPO1);
          $("#aestilo_bloque_hidden").val(PTR_EST_BLOQ1);
          var epoca_estilo = "";
          if(typeof dataP.cas_datos.PTR_EPO_EST1 !== 'undefined'){
          epoca_estilo = dataP.cas_datos.PTR_EPO_EST1; 
          document.getElementById("aepocaestilocheck").checked = epoca_estilo[1].valor;//var PTR_EPO_EST1
          }
          var sistema_constructivo = "";
          if(typeof dataP.cas_datos.PTR_SIS_CONST1 !== 'undefined'){
          sistema_constructivo = dataP.cas_datos.PTR_SIS_CONST1;
          document.getElementById("asistemaconstructivocheck").checked = sistema_constructivo[1].valor;//var PTR_SIS_CONST1
          }
          $("#asistema_constructivo").val(PTR_SIS_CONSTRUC1);
          $("#anro_viviendas").val(PTR_NRO_VIV1);
          $("#anro_hogares").val(PTR_NRO_HOG1);
          //-------------------campos texto enriquecido-----------------------//
          var PTR_VAL_HIS_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_HIS_DESC1);
          var PTR_VAL_ART_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_ART_DESC1);
          var PTR_VAL_ARQ_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_ARQ_DESC1);
          var PTR_VAL_TEC_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_TEC_DESC1);
          var PTR_VAL_INTE_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_INTE_DESC1);
          var PTR_VAL_URB_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_URB_DESC1);
          var PTR_VAL_INMAT_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_INMAT_DESC1);
          var PTR_VAL_SIMB_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_SIMB_DESC1);
          var PTR_VAL_INT_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_INT_DESC1);
          var PTR_VAL_EXT_DESC1 = caracteresEspecialesActualizar(dataP.cas_datos.PTR_VAL_EXT_DESC1);
          $("#aindicador_historico").val(PTR_VAL_HIS_DESC1);
          $("#aindicador_atistico").val(PTR_VAL_ART_DESC1);
          $("#aindicador_arquitectonico").val(PTR_VAL_ARQ_DESC1);
          $("#aindicador_teclogico").val(PTR_VAL_TEC_DESC1);
          $("#aindicador_integrid").val(PTR_VAL_INTE_DESC1);
          $("#aindicador_urbano").val(PTR_VAL_URB_DESC1);
          $("#aindicador_inmaterial").val(PTR_VAL_INMAT_DESC1);
          $("#aindicador_simbolico").val(PTR_VAL_SIMB_DESC1);
          $("#aindicador_interior").val(PTR_VAL_INT_DESC1);
          $("#adescripcion_exterior").val(PTR_VAL_EXT_DESC1);
          //-------------------fin campos texto enriquecido------------------//
          $("#acategoria_inmueble").val(PTR_CATEGORIA1); 
          $("#apuntuacion").val(PTR_PUNTO);
          $("#aid_bloque").val(PTR_ID_BLOQ1);
       

        
        var grillaPPact = "";
          if(typeof dataP.cas_datos.PTR_PEL_POT1 !== 'undefined'){
          grillaPPact = dataP.cas_datos.PTR_PEL_POT1;
          var longitudPPac  = grillaPPact.length;
          contPPactu = longitudPPac;
          editarPeligroPotenciales(longitudPPac);
           for (var j = 1; j < longitudPPac; j++)
            {
            $("#apeligropPP"+j+"").val(grillaPPact[j].f01_peligro_DES_);
            $("#apeligropPPO"+j+"").val(grillaPPact[j].f01_reja_DES_);
            var causas =   grillaPPact[j].f01_reja_INT_; 
            $("#acausasPP"+j+"").val(causas); 
            } 
           arrDibujoPPact.forEach(function (item, index, array) 
           {
            arrayPPactualizar.push({aval_pe:$("#apeligropPP"+item.id).val(), aval_pel:$("#apeligropPPO"+item.id).val(), aval_cau:$("#acausasPP").val()});
           });
           }

          //------------fin peligro poetncial--------------------
          //------------Inicio grilla patologia edificación-------
          var grillaPEdif = "";
          if(typeof dataP.cas_datos.PTR_PAT_EDIF1 !== 'undefined'){
          grillaPEdif = dataP.cas_datos.PTR_PAT_EDIF1;
          var longitudPEdif  = grillaPEdif.length;
          contPEact = longitudPEdif;
          editarPatologiaEdificacion(longitudPEdif);
           for (var j = 1; j < longitudPEdif; j++)
            {
             $("#dañoPEa"+j+"").val(grillaPEdif[j].f01_reja_DES_);
            $("#causaPEa"+j+"").val(grillaPEdif[j].f01_causa_DES_);
            } 
        
           arrDibujoPEact.forEach(function (item, index, array) 
           {
            arrPEAct.push({aval_daños:$("#dañoPEa"+item.id).val(), aval_causas:$("#causaPEa"+item.id).val()});
           });
           }
          //--------- fin grilla patologia edificacion------------
          //-------------Inicio grilla Instalacion estado------------------
          var grillaIEsact = "";
          if(typeof dataP.cas_datos.PTR_INST_EST1 !== 'undefined'){
          grillaIEsact = dataP.cas_datos.PTR_INST_EST1;
          var longitudIEst  = grillaIEsact.length;
          console.log('longitudIEst',longitudIEst);
          contINEact = longitudIEst;
          console.log('contINEact',contINEact);
          editarInstalacioEstado(longitudIEst);
          for (var j = 1; j < longitudIEst; j++)
          {
            $("#ainstalacionINE"+j+"").val(grillaIEsact[j].f01_Instalaciones_DESC);
            $("#apisoservINE"+j+"").val(grillaIEsact[j].f01_servidumbre_DESC); 
            $("#aestadoINE"+j+"").val(grillaIEsact[j].f01_estado_G_SERV_PROV);
            $("#aintegridadINE"+j+"").val(grillaIEsact[j].f01_integridad_G_SERV_PROV);
          } 

          arrDibujoIEs.forEach(function (item, index, array) 
          {
          arrINEactualiza.push({avar_inst_est:$("#ainstalacionINE"+item.id).val(), avar_pisose:$("#apisoservINE"+item.id).val(), avar_est:$("#aestadoINE"+item.id).val(), avar_int:$("#aintegridadINE"+item.id).val()});
          });
          }
          //-------------fin grilla instalacion estado---------------------
          
          //-------------Inicio grilla servicios---------------------------
          var grillaSedit = "";
          if(typeof dataP.cas_datos.PTR_SERVICIOS1 !== 'undefined'){
          grillaSedit = dataP.cas_datos.PTR_SERVICIOS1;
          var longitudSer  = grillaSedit.length;
          //console.log('longitudIEst',longitudSer);
          contSact = longitudSer;
          console.log('contSact',contSact);
          editarServicios(longitudSer);
          for (var j = 1; j < longitudSer; j++)
          {
            $("#adescripcionSERV"+j+"").val(grillaSedit[j].f01_emision_G_SERV_DESC);
            $("#aproveedorSERV"+j+"").val(grillaSedit[j].f01_emision_G_SERV_PROV);   
          } 

          arrDibujoServAct.forEach(function (item, index, array) 
          {
          arrSERVact.push({aval_desc:$("#adescripcionSERV"+item.id).val(), aval_prov:$("#aproveedorSERV"+item.id).val()});
          });
          }
          //--------------fin grilla servicios-----------------------------
           //-----------Inicio grilla paredes exteriores--------------------
          var grillaPPext = ""; 
          if(typeof dataP.cas_datos.PTR_PAR_EXT !== 'undefined'){
          grillaPPext= dataP.cas_datos.PTR_PAR_EXT;
          //console.log("grillaPPext",grillaPPext);
          var longitudPPext = grillaPPext.length;
          console.log("longitudPPext",longitudPPext);
          contParedes = longitudPPext;
          console.log("contParedes",contParedes);
          editarParedesExteriores(longitudPPext);
          for (var j = 1; j < longitudPPext; j++)
          {
          $("#estadPEx"+j+"").val(grillaPPext[j].par_estado);
          $("#intePEx"+j+"").val(grillaPPext[j].par_inte);
          $("#tipPEx"+j+"").val(grillaPPext[j].par_tipo);
          $("#matPEx"+j+"").val(grillaPPext[j].par_mat);   
          }
          
           //---------llenado de valores a vector de codigo -------------//
           arrDibujoPExt.forEach(function (item, index, array) 
          {
            arrCodigoPExt.push({
              par_estado:$("#estadPEx"+item.id).val(),
              par_inte:$("#intePEx"+item.id).val(),
              par_tipo:$("#tipPEx"+item.id).val(),
              par_mat:$("#matPEx"+item.id).val()});
          });
          

           //console.log("array dibbujo",arrDibujoPExt)
          console.log("llenarVectorEditar",arrCodigoPExt);
          }
          //---------------fin grilla paredes exteriores-----------------
          //----------------Inicio grilla cobertura techo----------------
          var grillaCtec = "";
          if(typeof dataP.cas_datos.PTR_COB_TEC !== 'undefined'){
          grillaCtec= dataP.cas_datos.PTR_COB_TEC;
          console.log("grillaCtec",grillaCtec);
          var longitudCtec  = grillaCtec.length;
          contCT = longitudCtec;
          editarCoberturaTecho(longitudCtec);
          for (var j = 1; j < longitudCtec; j++)
          {
          $("#estadocCTe"+j+"").val(grillaCtec[j].cob_estado);
          $("#integridadCTec"+j+"").val(grillaCtec[j].cob_inte);
          $("#tipoCTec"+j+"").val(grillaCtec[j].cob_tipo);
          $("#materialCTec"+j+"").val(grillaCtec[j].par_mat);   
          }

           //---------llenado de valores a vector de codigo -------------//
           arrDibujoCTec.forEach(function (item, index, array) 
          {
            arrCodigoCTec.push({
              val_est_con:$("#estadocCTe"+item.id).val(),
              val_inte:$("#integridadCTec"+item.id).val(),
              par_tipo:$("#tipoCTec"+item.id).val(),
              par_mat:$("#materialCTec"+item.id).val()});
          });
          }
          console.log("arrCodigoCTeeeee",arrCodigoCTec);
          //--------------Inicio paredes interiores---------------------
          var grillaPInt = "";
          if(typeof dataP.cas_datos.PTR_PAR_INT !== 'undefined'){
          grillaPInt= dataP.cas_datos.PTR_PAR_INT;
          console.log("grillaPInt",grillaPInt);
          var longitudPInt = grillaPInt.length;
          contPInt = longitudPInt;
          editarParedesInteriores(longitudPInt);

          for (var j = 1; j < longitudPInt; j++)
          {
          $("#estadocPIn"+j+"").val(grillaPInt[j].pari_estado);
          $("#integridadPIn"+j+"").val(grillaPInt[j].pari_inte);
          $("#tipoPIn"+j+"").val(grillaPInt[j].pari_tipo);
          $("#materialPIn"+j+"").val(grillaPInt[j].pari_mat); 

          }
          //---------llenado de valores a vector de codigo -------------//
           arrDibujoPInt.forEach(function (item, index, array) 
          {
            arrCodigoPInt.push({
              val_est_conser:$("#estadocPIn"+item.id).val(),
              val_int:$("#integridadPIn"+item.id).val(),
              val_tipo:$("#tipoPIn"+item.id).val(),
              val_mat:$("#materialPIn"+item.id).val()});
          });
          }
          //-----------fin paredes interiores--------------------------
          //----------Inicio grilla entre pisos------------------------
          var grillaEnP = "";
          if(typeof dataP.cas_datos.PTR_ENT_PIS !== 'undefined'){
          grillaEnP= dataP.cas_datos.PTR_ENT_PIS;
          var longitudEnP = grillaEnP.length;
          contEPisos = longitudEnP;
          editarEntrePisos(longitudEnP);
          for (var j = 1; j < longitudEnP; j++)
          {
          $("#estadocEnP"+j+"").val(grillaEnP[j].ent_estado);
          $("#integridadEnP"+j+"").val(grillaEnP[j].ent_inte);
          $("#tipoEnP"+j+"").val(grillaEnP[j].ent_tipo);
          $("#materialEnP"+j+"").val(grillaEnP[j].ent_mat);   
          }
            //---------llenado de valores a vector de codigo -------------//
           arrDibujoEP.forEach(function (item, index, array) 
          {
            arrCodigoEP.push({
              val_esta:$("#estadocEnP"+item.id).val(),
              val_int:$("#integridadEnP"+item.id).val(),
              val_tipo:$("#tipoEnP"+item.id).val(),
              val_mat:$("#materialEnP"+item.id).val()});
          });
          }
          //-----------fin grilla entre pisos-----------------------
          //-----------Inicio grilla Escalera-----------------------
          var grillaEsc = ""; 
          if(typeof dataP.cas_datos.PTR_ESC !== 'undefined'){
          grillaEsc= dataP.cas_datos.PTR_ESC;
          var longitudEsc = grillaEsc.length;
          contEsca = longitudEsc;
          editarEscaleras(longitudEsc);
          for (var j = 1; j < longitudEsc; j++)
          {
          $("#estadocEs"+j+"").val(grillaEsc[j].esc_estado);
          $("#integridadEs"+j+"").val(grillaEsc[j].esc_inte);
          $("#tipoEs"+j+"").val(grillaEsc[j].esc_tipo);
          $("#materialEs"+j+"").val(grillaEsc[j].esc_mat);

          } 
          //---------llenado de valores a vector de codigo -------------//
           arrDibujoEsc.forEach(function (item, index, array) 
          {
            arrCodigoEsc.push({
              val_estado:$("#estadocEs"+item.id).val(),
              val_int:$("#integridadEs"+item.id).val(),
              val_tipo:$("#tipoEs"+item.id).val(),
              val_mat:$("#materialEs"+item.id).val()});
          });
          }
          //---------fin grilla escalera-----------------------
          //--------  Inicio grilla cielo----------------------
          var grillaCielo = "";
          if(typeof dataP.cas_datos.PTR_CIE !== 'undefined'){
          grillaCielo= dataP.cas_datos.PTR_CIE;
          var longitudCielo  = grillaCielo.length;
          contCielo = longitudCielo;
          editarCielo(longitudCielo);
          for (var j = 1; j < longitudCielo; j++)
          {
          $("#estadocCi"+j+"").val(grillaCielo[j].cie_estado);
          $("#integridadCi"+j+"").val(grillaCielo[j].cie_inte);
          $("#tipoCi"+j+"").val(grillaCielo[j].cie_tipo);
          $("#materialCi"+j+"").val(grillaCielo[j].cie_mat);   
          }
          //---------llenado de valores a vector de codigo -------------//
           arrDibujoCielo.forEach(function (item, index, array) 
          {
            arrCodigoCielo.push({
              val_est:$("#estadocCi"+item.id).val(),
              val_int:$("#integridadCi"+item.id).val(),
              val_tipo:$("#tipoCi"+item.id).val(),
              val_mat:$("#materialCi"+item.id).val()});
          });
          }
          //-------------fin grilla cielo-----------------------
          //------------Inicio grilla balcones------------------
          var grillaBal = "";
          if(typeof dataP.cas_datos.PTR_BAL !== 'undefined'){
          grillaBal= dataP.cas_datos.PTR_BAL;
          var longitudBal  = grillaBal.length;
          contBal = longitudBal;
          editarBalcones(longitudBal);
          for (var j = 1; j < longitudBal; j++)
          {
          $("#estadocBal"+j+"").val(grillaBal[j].bal_estado);
          $("#integridadBal"+j+"").val(grillaBal[j].bal_inte);
          $("#tipoBal"+j+"").val(grillaBal[j].bal_tipo);
          $("#materialBal"+j+"").val(grillaBal[j].bal_mat);   
          }
           //---------llenado de valores a vector de codigo -------------//
           arrDibujoBal.forEach(function (item, index, array) 
          {
            arrCodigoBal.push({
              val_est:$("#estadocBal"+item.id).val(),
              val_inte:$("#integridadBal"+item.id).val(),
              val_tipo:$("#tipoBal"+item.id).val(),
              val_mat:$("#materialBal"+item.id).val()});
          });
          }
          //--------------fin grilla balcones------------------
          //--------------Inicio grilla estructura cubierta----
          var grillaEsCub = ""; 
          if(typeof dataP.cas_datos.PTR_EST_CUB !== 'undefined'){
          grillaEsCub= dataP.cas_datos.PTR_EST_CUB;
          var longitudEsCub  = grillaEsCub.length;
          contEsC = longitudEsCub;
          editarEstructuraCubierta(longitudEsCub);
          for (var j = 1; j < longitudEsCub; j++)
          {
          $("#estadocEsC"+j+"").val(grillaEsCub[j].est_cub_estado);
          $("#integridadEsC"+j+"").val(grillaEsCub[j].est_cub_inte);
          $("#tipoEsC"+j+"").val(grillaEsCub[j].est_cub_tipo);
          $("#materialEsC"+j+"").val(grillaEsCub[j].est_cub_mat);   
          }
         //---------llenado de valores a vector de codigo -------------//
           arrDibujoEsC.forEach(function (item, index, array) 
          {
            arrCodigoEsC.push({
              val_est:$("#estadocEsC"+item.id).val(),
              val_int:$("#integridadEsC"+item.id).val(),
              val_tipo:$("#tipoEsC"+item.id).val(),
              val_mat:$("#materialEsC"+item.id).val()});
          });
          }
          //--------fin grilla estructura cubierta-------------
          //--------Inicio grilla pisos------------------------
          var grillaPiso = "";
          if(typeof dataP.cas_datos.PTR_PISO !== 'undefined'){
          grillaPiso= dataP.cas_datos.PTR_PISO;
          var longitudPiso  = grillaPiso.length;
          contPisos = longitudPiso;
          editarPisos(longitudPiso);
          for (var j = 1; j < longitudPiso; j++)
          {
          $("#estadoPi"+j+"").val(grillaPiso[j].piso_estado);
          $("#integridadPi"+j+"").val(grillaPiso[j].piso_inte);
          $("#tipoPi"+j+"").val(grillaPiso[j].piso_tipo);
          $("#materialPi"+j+"").val(grillaPiso[j].piso_mat);   
          } 
          //---------llenado de valores a vector de codigo -------------//
           arrDibujoPiso.forEach(function (item, index, array) 
          {
            arrCodigoPiso.push({
              val_est:$("#estadoPi"+item.id).val(),
              val_int:$("#integridadPi"+item.id).val(),
              val_tipo:$("#tipoPi"+item.id).val(),
              val_mat:$("#materialPi"+item.id).val()});
          });
          }
          
          //--------Inicio grilla vanos ventanas interiores--
          var grillaVVIact = "";
          if(typeof dataP.cas_datos.PTR_VAN_VENT !== 'undefined'){
          grillaVVIact = dataP.cas_datos.PTR_VAN_VENT;
          var longitudVVIact  = grillaVVIact.length;
          contVVIact = longitudVVIact;
          editarVanosVentanasInt(longitudVVIact);
          for (var j = 1; j < longitudVVIact; j++)
          {
            $("#avent_int_estado"+j+"").val(grillaVVIact[j].vent_int_estado);
            $("#avent_int_inte"+j+"").val(grillaVVIact[j].vent_int_inte);
            $("#avent_int_tipo"+j+"").val(grillaVVIact[j].vent_int_tipo);
            var vvint = grillaVVIact[j].vent_int_mat;
            var vvint = validarMaterial(vvint);
            $("#avent_int_mat"+j+"").val(vvint);  
          } 

          arrDibujoVVInt.forEach(function (item, index, array) 

          {
          arrVVIactualizar.push({aarg_vent_int_estado:$("#avent_int_estado"+item.id).val(), aarg_vent_int_inte:$("#avent_int_inte"+item.id).val(), aarg_vent_int_tipo:$("#avent_int_tipo"+item.id).val(), aarg_vent_int_mat:$("#avent_int_mat"+item.id).val()});
          });
          }
          //-----fin grilla vanos ventanas interiores----------
          //-----Inicio ventana interiores-------------------------
          var grillaVIntac = "";
          if(typeof dataP.cas_datos.PTR_VENT_INT !== 'undefined'){
          grillaVIntac = dataP.cas_datos.PTR_VENT_INT;
          var longitudVIact  = grillaVIntac.length;
          contVIac = longitudVIact;
          editarVentanasInteriores(longitudVIact);
          for (var j = 1; j < longitudVIact; j++)
          {
          $("#avent_inte_estado"+j+"").val(grillaVIntac[j].vent_inte_estado);
          $("#avent_inte_inte"+j+"").val(grillaVIntac[j].vent_inte_inte);
          $("#avent_inte_tipo"+j+"").val(grillaVIntac[j].vent_inte_tipo);
          $("#avent_inte_mat"+j+"").val(grillaVIntac[j].vent_inte_mat);  
          } 

          arrDibujoVIact.forEach(function (item, index, array) 
          {
          arrayVIactualizar.push({aarg_vent_inte_estado:$("#avent_inte_estado"+item.id).val(), aarg_vent_inte_inte:$("#avent_inte_inte"+item.id).val(), aarg_vent_inte_tipo:$("#avent_inte_tipo"+item.id).val(), aarg_vent_inte_mat:$("#avent_inte_mat"+item.id).val()});
          });
          } 
          //--------fin ventana interiores--------------------------
          //-------Inicio grilla vano ventana externa-----------
          var grillaVVEx = "";
          if(typeof dataP.cas_datos.PTR_VAN_VENT_EXT !== 'undefined'){
          grillaVVEx = dataP.cas_datos.PTR_VAN_VENT_EXT;
          var longitudVEExt  = grillaVVEx.length;
          contVVExtact = longitudVEExt;
          editarVanosVentanasExteriores(longitudVEExt);
          for (var j = 1; j < longitudVEExt; j++)
          {
          $("#avent_ext_estado"+j+"").val(grillaVVEx[j].vent_ext_estado);
          $("#avent_ext_inte"+j+"").val(grillaVVEx[j].vent_ext_inte);
          $("#avent_ext_tipo"+j+"").val(grillaVVEx[j].vent_ext_tipo);
          var vvexterior = grillaVVEx[j].vent_ext_mat;
          var vvexterior = validarMaterial(vvexterior);
          $("#avent_ext_mat"+j+"").val(vvexterior); 

          } 
          arrDibujoVVExt.forEach(function (item, index, array) 
          {
          arrayVVExtact.push({aarg_vent_ext_estado:$("#avent_ext_estado"+item.id).val(), aarg_vent_ext_inte:$("#avent_ext_inte"+item.id).val(), aarg_vent_ext_tipo:$("#avent_ext_tipo"+item.id).val(), aarg_vent_ext_mat:$("#avent_ext_mat"+item.id).val()});
          });
          }

          //--------fin vano ventana externa----------------------
          //------------Inicio grilla Ventana Exterior------------
          var grillaVEx = "";
          if(typeof dataP.cas_datos.PTR_VENT_EXT !== 'undefined'){
          grillaVEx = dataP.cas_datos.PTR_VENT_EXT;
          var longitudVExtact  = grillaVEx.length;
          contVExactualizar = longitudVExtact;
          editarVentanasExteriores(longitudVExtact);
          for (var j = 1; j < longitudVExtact; j++)
          {
          $("#avent_exte_estado"+j+"").val(grillaVEx[j].vent_exte_estado);
          $("#avent_exte_inte"+j+"").val(grillaVEx[j].vent_exte_inte);
          $("#avent_exte_tipo"+j+"").val(grillaVEx[j].vent_exte_tipo);
          $("#avent_exte_mat"+j+"").val(grillaVEx[j].vent_exte_mat);     
          } 
          arrDibujoVExact.forEach(function (item, index, array) 
          {
          arrayVExactualizar.push({aarg_vent_exte_estado:$("#avent_exte_estado"+item.id).val(), aarg_vent_exte_inte:$("#avent_exte_inte"+item.id).val(), aarg_vent_exte_tipo:$("#avent_exte_tipo"+item.id).val(), aarg_vent_exte_mat:$("#avent_exte_mat"+item.id).val()});
          });
          }

          //-------fin grilla ventana exterior-------------------
          //-------Inicio grilla vano puerta interior-----------------
           var grillaVPIn = "";
          if(typeof dataP.cas_datos.PTR_VAN_PUE_INT !== 'undefined'){
          grillaVPIn = dataP.cas_datos.PTR_VAN_PUE_INT;
          var longitudVPInt  = grillaVPIn.length;
          contVPIntactua = longitudVPInt;
          editarVanosPuertasInteriores(longitudVPInt);
          for (var j = 1; j < longitudVPInt; j++)
          {
          $("#apue_int_estado"+j+"").val(grillaVPIn[j].pue_int_estado);
          $("#apue_int_inte"+j+"").val(grillaVPIn[j].pue_int_inte);
          $("#apue_int_tipo"+j+"").val(grillaVPIn[j].pue_int_tipo);
          var vpinterna = grillaVPIn[j].pue_int_mat;
          var vpinterna = validarMaterial(vpinterna);
          $("#apue_int_mat"+j+"").val(vpinterna);    
          } 

          arrDibujoVPIn.forEach(function (item, index, array) 
          {
          arrayVPIntact.push({aarg_pue_int_estado:$("#apue_int_estado"+item.id).val(), aarg_pue_int_inte:$("#apue_int_inte"+item.id).val(), aarg_pue_int_tipo:$("#apue_int_tipo"+item.id).val(), aarg_pue_int_mat:$("#apue_int_mat"+item.id).val()});
          });
          }
          //-------fin grilla puerta interior-------------------
          //-------Inicio grilla vano puerta externa------------
          
          var grillaVPEx = "";
          if(typeof dataP.cas_datos.PTR_VAN_PUE_EXT !== 'undefined'){
          grillaVPEx = dataP.cas_datos.PTR_VAN_PUE_EXT;
          var longitudVPEac  = grillaVPEx.length;
          contVPIntAct = longitudVPEac;
          editarVanosPuertasExteriores(longitudVPEac);
          for (var j = 1; j < longitudVPEac; j++)
          {
          $("#apue_ext_estado"+j+"").val(grillaVPEx[j].pue_ext_estado);
          $("#apue_ext_inte"+j+"").val(grillaVPEx[j].pue_ext_inte);
          $("#apue_ext_tipo"+j+"").val(grillaVPEx[j].pue_ext_tipo);
          var vpexterna = grillaVPEx[j].pue_ext_mat;
          var vpexterna = validarMaterial(vpexterna);
          $("#apue_ext_mat"+j+"").val(vpexterna);       
          } 

          arrDibujoVPEact.forEach(function (item, index, array) 
          {
          aarrCodigo_ptr_van_pue_ext.push({aarg_pue_ext_estado:$("#apue_ext_estado"+item.id).val(),
          aarg_pue_ext_inte:$("#apue_ext_inte"+item.id).val(), aarg_pue_ext_tipo:$("#apue_ext_tipo"+item.id).val(),
          aarg_pue_ext_mat:$("#apue_ext_mat"+item.id).val()});
          });
          }
          //------fin puerta exterior---------------------------
          //-------Inicio grilla puerta interior----------------
         
         var grillaPInterior = "";
          if(typeof dataP.cas_datos.PTR_PUE_INT !== 'undefined'){
          grillaPInterior = dataP.cas_datos.PTR_PUE_INT;
          var longitudPIact  = grillaPInterior.length;
          contPIntAct = longitudPIact;
          editarPuertasInteriores(longitudPIact);
          for (var j = 1; j < longitudPIact; j++)
          {
           $("#apue_inte_estado"+j+"").val(grillaPInterior[j].pue_inte_estado);
          $("#apue_inte_inte"+j+"").val(grillaPInterior[j].pue_inte_inte);
          $("#apue_inte_tipo"+j+"").val(grillaPInterior[j].pue_inte_tipo);
          var materialPI = grillaPInterior[j].pue_inte_mat; 
          var materialPI = validarMaterial(materialPI);
          $("#apue_inte_mat"+j+"").val(materialPI);      
          } 

          arrDibujoPIntAct.forEach(function (item, index, array) 
          {
          aarrCodigo_ptr_pue_int.push({aarg_pue_inte_estado:$("#apue_inte_estado"+item.id).val(),
           aarg_pue_inte_inte:$("#apue_inte_inte"+item.id).val(), 
           aarg_pue_inte_tipo:$("#apue_inte_tipo"+item.id).val(), 
           aarg_pue_inte_mat:$("#apue_inte_mat"+item.id).val()});
          });
          }
          //------fin grilla puerta interior--------------------
          //------Inicio grilla puerta exterior-----------------
           var grillaPExterior = "";
          if(typeof dataP.cas_datos.PTR_PUE_EXT !== 'undefined'){
          grillaPExterior = dataP.cas_datos.PTR_PUE_EXT;
          var longitudPEact  = grillaPExterior.length;
          contPExtact = longitudPEact;
          editarPuertasExteriores(longitudPEact);
          for (var j = 1; j < longitudPEact; j++)
          {
          $("#apue_exte_estado"+j+"").val(grillaPExterior[j].pue_exte_estado);
          $("#apue_exte_inte"+j+"").val(grillaPExterior[j].pue_exte_inte);
          $("#apue_exte_tipo"+j+"").val(grillaPExterior[j].pue_exte_tipo);
          var metalPR = grillaPExterior[j].pue_exte_mat;
          var metalPR = validarMaterial(metalPR);
          $("#apue_exte_mat"+j+"").val(metalPR);
          } 
          arrDibujoPExact.forEach(function (item, index, array) 
          {
          arrcodPExtact.push({aarg_pue_exte_estado:$("#apue_exte_estado"+item.id).val(), 
          aarg_pue_exte_inte:$("#apue_exte_inte"+item.id).val(), 
          aarg_pue_exte_tipo:$("#apue_exte_tipo"+item.id).val(),
           aarg_pue_exte_mat:$("#apue_exte_mat"+item.id).val()});
          });
          }
          //-------fin grilla puerta esterior--------------------
          //-------Inicio grilla rejas---------------------------
          var grillaRejaact = "";
          if(typeof dataP.cas_datos.PTR_REJA !== 'undefined'){
          grillaRejaact = dataP.cas_datos.PTR_REJA;
          var longitudRact  = grillaRejaact.length;
          contrejasact = longitudRact;
          editarRejas(longitudRact);
          for (var j = 1; j < longitudRact; j++)
          {
          $("#areja_estado"+j+"").val(grillaRejaact[j].reja_estado);
          $("#areja_inte"+j+"").val(grillaRejaact[j].reja_inte);
          $("#areja_tipo"+j+"").val(grillaRejaact[j].reja_tipo);
          $("#areja_mat"+j+"").val(grillaRejaact[j].reja_mat);   
          } 
          arrDibRejas.forEach(function (item, index, array) 
          {
          arrayrejasact.push({aarg_reja_estado:$("#areja_estado"+item.id).val(), 
          aarg_reja_inte:$("#areja_inte"+item.id).val(), 
          aarg_reja_tipo:$("#areja_tipo"+item.id).val(), 
          aarg_reja_mat:$("#areja_mat"+item.id).val()});
          });
          }
          //-----Inicio grilla muros extriores---------------
          var grillaPMExt = "";
          if(typeof dataP.cas_datos.PTR_PINT_MUR_EXT !== 'undefined'){
          grillaPMExt = dataP.cas_datos.PTR_PINT_MUR_EXT;
          var longitudPME  = grillaPMExt.length;
          contPMEact = longitudPME;
          editarPinturasMurosExt(longitudPME);
          for (var j = 1; j < longitudPME; j++)
          {
          $("#aestado"+j+"").val(grillaPMExt[j].pint_ext_estado);
          $("#aintegridad"+j+"").val(grillaPMExt[j].pint_ext_inte);
          $("#atipo"+j+"").val(grillaPMExt[j].pint_ext_tipo);
          $("#amaterial"+j+"").val(grillaPMExt[j].pint_ext_mat);    
          } 
          arrDibujoPMExact.forEach(function (item, index, array) 
          {
          arrayPMExtactualizar.push({aarg_estado:$("#aestado"+item.id).val(), 
          aarg_integridad:$("#aintegridad"+item.id).val(), 
          aarg_tipo:$("#atipo"+item.id).val(), aarg_material:$("#amaterial"+item.id).val()});
          });
          } 
          //----fin pinturas muros exteriores----------------
          //----Inicio pinturas muros interiores-------------
          var grillaPMInt = "";
          if(typeof dataP.cas_datos.PTR_PINT_MUR_INT !== 'undefined'){
          grillaPMInt = dataP.cas_datos.PTR_PINT_MUR_INT;
          var longitudPMI  = grillaPMInt.length;
          contPMIactua = longitudPMI;
          editarPinturasMurosInt(longitudPMI);
          for (var j = 1; j < longitudPMI; j++)
          {
           $("#aestadoInt"+j+"").val(grillaPMInt[j].pint_int_estado);
          $("#aintegridadInt"+j+"").val(grillaPMInt[j].pint_int_inte);
          $("#atipoInt"+j+"").val(grillaPMInt[j].pint_int_tipo);
          $("#amaterialInt"+j+"").val(grillaPMInt[j].pint_int_mat);  
          } 
          arrDibujoPMInact.forEach(function (item, index, array) 
          {
          arrayPMIntact.push({aarg_estadoInt:$("#aestadoInt"+item.id).val(), 
          aarg_integridadInt:$("#aintegridadInt"+item.id).val(),
          aarg_tipoInt:$("#atipoInt"+item.id).val(), 
          aarg_materialInt:$("#amaterialInt"+item.id).val()});
          });
          }
          //----fin pinturas muros exteriores----------------            
          //----Inicio acabados muros exteriores-------------
          var grillaAMExt = "";
          if(typeof dataP.cas_datos.PTR_ACA_MUR_EXT !== 'undefined'){
          grillaAMExt = dataP.cas_datos.PTR_ACA_MUR_EXT;
          var longitudAMEx  = grillaAMExt.length;
          contAMEact = longitudAMEx;
          editarAcabadosMurosExt(longitudAMEx);
          for (var j = 1; j < longitudAMEx; j++)
          {
          $("#aestadoAcaExt"+j+"").val(grillaAMExt[j].aca_ext_estado);
          $("#aintegridadAcaExt"+j+"").val(grillaAMExt[j].aca_ext_inte);
          $("#atipoAcaExt"+j+"").val(grillaAMExt[j].aca_ext_tipo);
          $("#amaterialAcaExt"+j+"").val(grillaAMExt[j].aca_ext_mat);    
          } 
          arrDibujoAMExAct.forEach(function (item, index, array) 
          {
          aaca_extCodigo.push({aarg_estadoAcaExt:$("#aestadoAcaExt"+item.id).val(),
          aarg_integridadAcaExt:$("#aintegridadAcaExt"+item.id).val(), 
          aarg_tipoAcaExt:$("#atipoAcaExt"+item.id).val(), aarg_materialAcaExt:$("#amaterialAcaExt"+item.id).val()});
          });
          }
          //----fin acabados muros interiores---------------
          //----Inicio acabados muros interiores------------
          var grillaAMInt = "";
          if(typeof dataP.cas_datos.PTR_ACA_MUR_INT !== 'undefined'){
          grillaAMInt = dataP.cas_datos.PTR_ACA_MUR_INT;
          var longitudAMIn  = grillaAMInt.length;
          contAMIntAct = longitudAMIn;
          editarAcabadosMurosInt(longitudAMIn);
          for (var j = 1; j < longitudAMIn; j++)
          {
          $("#aestadoAcaInt"+j+"").val(grillaAMInt[j].aca_int_estado);
          $("#aintegridadAcaInt"+j+"").val(grillaAMInt[j].aca_int_inte);
          $("#atipoAcaInt"+j+"").val(grillaAMInt[j].aca_int_tipo);
          $("#amaterialAcaInt"+j+"").val(grillaAMInt[j].aca_int_mat);   
          } 
          arrDibujoAMIact.forEach(function (item, index, array) 
          {
          aaca_intCodigo.push({aarg_estadoAcaInt:$("#aestadoAcaInt"+item.id).val(), aarg_integridadAcaInt:$("#aintegridadAcaInt"+item.id).val(), aarg_tipoAcaInt:$("#atipoAcaInt"+item.id).val(), aarg_materialAcaInt:$("#amaterialAcaInt"+item.id).val()});
          });
          }
          
          //-----fin acabados muros interiores-------------        
          //-----Inicio elementos compositivos artísticos--
          var grillaECArt = "";
          if(typeof dataP.cas_datos.PTR_ELEM_ART_COMP !== 'undefined'){
          grillaECArt = dataP.cas_datos.PTR_ELEM_ART_COMP;
          var longitudECA  = grillaECArt.length;
          contECAactual = longitudECA;
          editarElementosComArt(longitudECA);
          for (var j = 1; j < longitudECA; j++)
          {
          $("#aartisticosC1"+j+"").val(grillaECArt[j].elem_art_estado);
          $("#aartisticosC2"+j+"").val(grillaECArt[j].elem_art_inte);
          $("#aartisticosC3"+j+"").val(grillaECArt[j].elem_art_tipo);  
          } 
          arrDibujoECAact.forEach(function (item, index, array) 
          {
          aarrCodigoAR.push({aartisticosC1:$("#aartisticosC1"+item.id).val(), aartisticosC2:$("#aartisticosC2"+item.id).val(), aartisticosC3:$("#aartisticosC3"+item.id).val()});
          });
          }  
          //----fin elementos compositivos artísticos---------
           //----Inicio elementos ornamentales-----------------
          var grillaEODes = "";
          if(typeof dataP.cas_datos.PTR_ELEM_ORN_DEC !== 'undefined'){
          grillaEODes = dataP.cas_datos.PTR_ELEM_ORN_DEC;
          var longitudEO  = grillaEODes.length;
          contEOactual = longitudEO;
          editarElementosOrnamentales(longitudEO);
          for (var j = 1; j < longitudEO; j++)
          {
         $("#aornamentales1"+j+"").val(grillaEODes[j].elem_orn_estado);
          $("#aornamentales2"+j+"").val(grillaEODes[j].elem_orn_inte);
          $("#aornamentales3"+j+"").val(grillaEODes[j].elem_orn_tipo);   
          } 
          arrDibujoEOact.forEach(function (item, index, array) 
          {
          aarrCodigoOR.push({aornamentales1:$("#aornamentales1"+item.id).val(), aornamentales2:$("#aornamentales2"+item.id).val(), aornamentales3:$("#aornamentales3"+item.id).val()});
          });
          }
          //----fin elemnetos ornamentales--------------------   

           //-----Inicio elementos tipologicos compositivos----
          var grillaETComp = "";
          if(typeof dataP.cas_datos.PTR_ELEM_TIP_COMP !== 'undefined'){
          grillaETComp = dataP.cas_datos.PTR_ELEM_TIP_COMP;
          var longitudETC  = grillaETComp.length;
          contETCactual = longitudETC;
          editarElementosTipologicosComp(longitudETC);
          for (var j = 1; j < longitudETC; j++)
          {
          $("#atipologicos1"+j+"").val(grillaETComp[j].elem_art_estado);
          $("#atipologicos2"+j+"").val(grillaETComp[j].elem_art_inte);
          $("#atipologicos3"+j+"").val(grillaETComp[j].elem_art_tipo);  
          } 
          arrDibujoETComAct.forEach(function (item, index, array) 
          {
          aarrCodigoTIC.push({atipologicos1:$("#atipologicos1"+item.id).val(), atipologicos2:$("#atipologicos2"+item.id).val(), atipologicos3:$("#atipologicos3"+item.id).val()});
          });
          }
          //---fin elementos tipologicos compositivos---------
          //----Inicio grilla hogar---------------------------
          var grillahogar = "";
          if(typeof dataP.cas_datos.PTR_AG_HOGARES !== 'undefined'){
          grillahogar = dataP.cas_datos.PTR_AG_HOGARES;
          var longitudH  = grillahogar.length;
          contHact = longitudH;
          editarHogar(longitudH);
          for (var j = 1; j < longitudH; j++)
          {
          $("#ahogar1"+j+"").val(grillahogar[j].hog_desc);
          $("#ahogar2"+j+"").val(grillahogar[j].edad_hog);
          var hogar3 = grillahogar[j].muj_hog;
          var hogar3 = cambiarValorBool(hogar3);
          document.getElementById("ahogar3"+j+"").checked = hogar3; 
          var hogar4 = grillahogar[j].hom_hog;
          var hogar4 = cambiarValorBool(hogar4);
          document.getElementById("ahogar4"+j+"").checked = hogar4; 
          $("#ahogar5"+j+"").val(grillahogar[j].ten_hog);
          $("#ahogar6"+j+"").val(grillahogar[j].der_hog); 
          } 
          arrDibujoHogAct.forEach(function (item, index, array) 
          {
          aarrCodigoHogar.push({ahogar1:$("#ahogar1"+item.id).val(), ahogar2:$("#ahogar2"+item.id).val(), ahogar3:document.getElementById("ahogar3"+item.id).checked, ahogar4:document.getElementById("ahogar4"+item.id).checked, ahogar5:$("#ahogar5"+item.id).val(), ahogar6:$("#ahogar6"+item.id).val()});
          });
          }
         //----fin grilla editar hogar------------    
        },
        error: function(result) {
          swal( "Error..!", "Error....", "error" );
        }
      });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
      });
};



$( "#registrarBienesInmuebles" ).click(function() 
{
  obtenercorrelativo('PTR_CBI');
  setTimeout(function(){
  var ccreador_in = $("#ccreador_in").val();
  var cfecha_creador_in = $("#cfecha_creador_in").val();
  var ccodPatrimonio_in = 'PTR_CBI'+correlativog+'/2018';
  var ccodCatastral_in = $("#ccodCatastral_in").val();
  var ccodCatastral1_in = $("#ccodCatastral1_in").val();
  var coriginal_in = $("#coriginal_in").val();
  var ctradicional_in = $("#ctradicional_in").val();
  var cActual_in = $("#cActual_in").val();
  var cdepartamento_in = $("#cdepartamento_in").val();
  var cciudad_in = $("#cciudad_in").val();
  var cmunicipio_in = $("#cmunicipio_in").val();
  var cMacrodistrito_in = document.getElementById('cMacrodistrito_in').value;
  var ctipoViaLocalizacion_in = document.getElementById('ctipoViaLocalizacion_in').value;
  var ccalleEsquina_in = $("#ccalleEsquina_in").val();
  var cdireccion_in = $("#cdireccion_in").val();
  var czona_in = $("#czona_in").val();
  var cnumero_in = $("#cnumero_in").val();
  var cnumero_in2 = $("#cnumero_in2").val();
  var cnumero_in3 = $("#cnumero_in3").val();
  var cnumero_in4 = $("#cnumero_in4").val();
  var cnumero_in5 = $("#cnumero_in5").val();
  var careaPredio_in = $("#careaPredio_in").val();
  var canchoFrente_in = $("#canchoFrente_in").val();
  var cfondoPredio_in = $("#cfondoPredio_in").val();
  var canchoMuro_in = $("#canchoMuro_in").val();
  var calturaInterior_in = $("#calturaInterior_in").val();
  var calturaFachada_in = $("#calturaFachada_in").val();
  var calturaMaxima_in = $("#calturaMaxima_in").val();
  var cdescripcion_in = $("#cdescripcion_in").val();
  var cubicacionManzana_in = document.getElementById('cubicacionManzana_in').value;
  var clineaConstruccion_in = document.getElementById('clineaConstruccion_in').value;
  var ctrazoManzana_in = document.getElementById('ctrazoManzana_in').value;
  var ctipologiaArqui_in = document.getElementById('ctipologiaArqui_in').value;
  var careaEdificable_in = document.getElementById('careaEdificable_in').value;
  var cmaterialVia_in = document.getElementById('cmaterialVia_in').value;
  var ctipoVia_in = document.getElementById('ctipoVia_in').value;
  //var cbloqueInventario_in = $("#cbloqueInventario_in").val();
  var cbloqueInventario_in = document.getElementById('cbloqueInventario_in').value;
  var cbloquesReales_in = $("#cbloquesReales_in").val();
  var chogaresInventario_in = $("#chogaresInventario_in").val();
  var chogaresReales_in = $("#chogaresReales_in").val();
  var cestiloGeneral_in = document.getElementById('cestiloGeneral_in').value;
  var crangoEpoca_in = document.getElementById('crangoEpoca_in').value;
  var cestadoConserva_in = document.getElementById('cestadoConserva_in').value;

  /////GRILLA FIGURA DE PROTECCION /////////
  var cont=0;
  arrDibujo.forEach(function (item, index, array) 
  {
    arrCodigo[cont].arg_valor = $("#valor"+item.id+"").val();
    arrCodigo[cont].arg_tipo = $("#tipoarg"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"f01_FIG_LEG_|f01_FIG_OBS","titulos":"Figura de Proteccion Legal|Observaciones"}';
  for (var i = 0; i<arrCodigo.length;i++)
  {
    
     var cobsFiguraProtec = caracteresEspecialesRegistrar(arrCodigo[i].arg_valor);
     var grillaFigura = '{"f01_FIG_OBS":"'+cobsFiguraProtec+'","f01_FIG_LEG_":"'+arrCodigo[i].arg_tipo+'","f01_FIG_LEG__valor":"'+arrCodigo[i].arg_tipo+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaFiguraProtec = bloqueFiguraProtec; 

/////FIN GRILLA FIGURA DE PROTECCION  /////

    /////GRILLA ELEMENTOS ARTISTICOS COMPOSITIVOS /////////
  var cont=0;
   arrDibujoAR.forEach(function (item, index, array) 
        {
          arrCodigoAR[cont].artisticosC1 = $("#artisticosC1"+item.id+"").val();
          arrCodigoAR[cont].artisticosC2 = $("#artisticosC2"+item.id+"").val();
          arrCodigoAR[cont].artisticosC3 = $("#artisticosC3"+item.id+"").val();
          cont++;
         
        });
   console.log("arrCodigoAR",arrCodigoAR);
    var bloqueFiguraProtecAR = '[{"tipo":"GRD","campos": "elem_art_estado|elem_art_inte|elem_art_tipo", "titulos":"Estado de Conservación|Integridad|Tipo"}';
    for (var i = 0; i<arrCodigoAR.length;i++)
    {
    var grillaFiguraAR = '{"elem_art_estado":"'+arrCodigoAR[i].artisticosC1+'", "elem_art_estado_valor":"'+arrCodigoAR[i].artisticosC1+'", "elem_art_inte":"'+arrCodigoAR[i].artisticosC2+'", "elem_art_inte_valor":"'+arrCodigoAR[i].artisticosC2+'", "elem_art_tipo":"'+arrCodigoAR[i].artisticosC3+'", "elem_art_tipo_valor":"'+arrCodigoAR[i].artisticosC3+'"}';

    bloqueFiguraProtecAR = (bloqueFiguraProtecAR.concat(',').concat(grillaFiguraAR));
     
    }
    bloqueFiguraProtecAR = bloqueFiguraProtecAR.concat(']');
    var cgrillaFiguraProtecAR = bloqueFiguraProtecAR
    
    /////GRILLA ELEMENTOS ORNAMENTALES /////////
  var cont=0;
   arrDibujoOR.forEach(function (item, index, array) 
        {
          arrCodigoOR[cont].ornamentales1 = $("#ornamentales1"+item.id+"").val();
          arrCodigoOR[cont].ornamentales2 = $("#ornamentales2"+item.id+"").val();
          arrCodigoOR[cont].ornamentales3 = $("#ornamentales3"+item.id+"").val();
          cont++;         
        });
   console.log("arrCodigoOR",arrCodigoOR);
    var bloqueFiguraProtecOR = '[{"tipo":"GRD","campos":"elem_orn_estado|elem_orn_inte|elem_orn_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
    for (var i = 0; i<arrCodigoOR.length;i++)
    {
   
    var grillaFiguraOR = '{"elem_orn_estado":"'+arrCodigoOR[i].ornamentales1+'", "elem_orn_estado_valor":"'+arrCodigoOR[i].ornamentales1+'", "elem_orn_inte":"'+arrCodigoOR[i].ornamentales2+'", "elem_orn_inte_valor":"'+arrCodigoOR[i].ornamentales2+'", "elem_orn_tipo":"'+arrCodigoOR[i].ornamentales3+'", "elem_orn_tipo_valor":"'+arrCodigoOR[i].ornamentales3+'"}';

    bloqueFiguraProtecOR = (bloqueFiguraProtecOR.concat(',').concat(grillaFiguraOR));
     
    }
    bloqueFiguraProtecOR = bloqueFiguraProtecOR.concat(']');
    var cgrillaFiguraProtecOR = bloqueFiguraProtecOR

/////GRILLA ELEMENTOS TIPOLOGICOS COMPOSITIVOS /////////

  var cont=0;
   arrDibujoTIC.forEach(function (item, index, array) 
        {
          arrCodigoTIC[cont].tipologicos1 = $("#tipologicos1"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos2 = $("#tipologicos2"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos3 = $("#tipologicos3"+item.id+"").val();
          cont++;
         
        });
  
    var bloqueFiguraProtecTIC = '[{"tipo":"GRD","campos":"elem_art_estado|elem_art_inte|elem_art_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
    for (var i = 0; i<arrCodigoTIC.length;i++)
    {
  
    var grillaFiguraTIC = '{"elem_art_estado":"'+arrCodigoTIC[i].tipologicos1+'", "elem_art_estado_valor":"'+arrCodigoTIC[i].tipologicos1+'", "elem_art_inte":"'+arrCodigoTIC[i].tipologicos2+'", "elem_art_inte_valor":"'+arrCodigoTIC[i].tipologicos2+'", "elem_art_tipo":"'+arrCodigoTIC[i].tipologicos3+'", "elem_art_tipo_valor":"'+arrCodigoTIC[i].tipologicos3+'"}';

    bloqueFiguraProtecTIC = (bloqueFiguraProtecTIC.concat(',').concat(grillaFiguraTIC));     
    }
    bloqueFiguraProtecTIC = bloqueFiguraProtecTIC.concat(']');
    var cgrillaFiguraProtecTIC = bloqueFiguraProtecTIC
    
    /////GRILLA HOGAR /////////
  var cont=0;
   arrDibujoHogar.forEach(function (item, index, array) 
        {
          arrCodigoHogar[cont].hogar1 = $("#hogar1"+item.id+"").val();
          arrCodigoHogar[cont].hogar2 = $("#hogar2"+item.id+"").val();
          arrCodigoHogar[cont].hogar3 = document.getElementById("hogar3"+item.id+"").checked;
          arrCodigoHogar[cont].hogar4 = document.getElementById("hogar4"+item.id+"").checked;
          arrCodigoHogar[cont].hogar5 = $("#hogar5"+item.id+"").val();
          arrCodigoHogar[cont].hogar6 = $("#hogar6"+item.id+"").val();
          cont++;         
        });

    var bloqueFiguraProtecHogar = '[{"tipo":"GRD","campos":"hog_desc|edad_hog|hom_hog|muj_hog|ten_hog|der_hog","titulos":"Hogar Des.|Edad|Hombre|Mujer|Tendencia|Derecho Propietario"}';
    for (var i = 0; i<arrCodigoHogar.length;i++)
    {
   
    var grillaFiguraHogar = '{"hog_desc":"'+arrCodigoHogar[i].hogar1+'", "edad_hog":"'+arrCodigoHogar[i].hogar2+'", "edad_hog_valor":"'+arrCodigoHogar[i].hogar2+'", "hom_hog":"'+ cambiarBoolAString(arrCodigoHogar[i].hogar3)+'", "muj_hog":"'+ cambiarBoolAString(arrCodigoHogar[i].hogar4)+'", "ten_hog":"'+arrCodigoHogar[i].hogar5+'", "ten_hog_valor":"'+arrCodigoHogar[i].hogar5+'", "der_hog":"'+arrCodigoHogar[i].hogar6+'", "der_hog_valor":"'+arrCodigoHogar[i].hogar6+'"}';

    bloqueFiguraProtecHogar = (bloqueFiguraProtecHogar.concat(',').concat(grillaFiguraHogar));
     
    }
    bloqueFiguraProtecHogar = bloqueFiguraProtecHogar.concat(']');
    var cgrillaFiguraProtecHogar = bloqueFiguraProtecHogar

//------------------- GRILLA REGIMEN PROPIEDA ------------

var cont=0;
reg_proDibujo.forEach(function (item, index, array) 
{
  reg_proCodigo[cont].arg_propietario = $("#cnombrePropietario_in"+item.id+"").val();
  reg_proCodigo[cont].arg_tipoP = $("#tipoPropiedad_in"+item.id+"").val();
  cont++;

});
var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"f01_nombre_PROP_|f01_reja_INT","titulos":"Nombre de Propietario|Tipo de Propiedad"}';
for (var i = 0; i<reg_proDibujo.length;i++)
{
    var grillaFigura = '{"f01_nombre_PROP_":"'+reg_proCodigo[i].arg_propietario+'","f01_reja_INT_":"'+reg_proCodigo[i].arg_tipoP+'","f01_reja_INT_valor":"'+reg_proCodigo[i].arg_tipoP+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaRegimenPropietario = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------

  //------------------- GRILLA MARCO LEGAL ------------

  var cont=0;
  ref_hisDibujo.forEach(function (item, index, array) 
  {
    ref_hisCodigo[cont].arg_tipoRef = $("#tipoRefrencia"+item.id+"").val();
    ref_hisCodigo[cont].arg_observacion = $("#observacion"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"f01_ref_hist_|f01_Obs_REF_","titulos":"Referencias Históricas|Observación"}';
  for (var i = 0; i<ref_hisDibujo.length;i++)
  {
    var cobsReferencias = caracteresEspecialesRegistrar(ref_hisCodigo[i].arg_observacion);
    var grillaFigura = '{"f01_ref_hist_":"'+ref_hisCodigo[i].arg_tipoRef+'","f01_ref_hist__valor":"'+ref_hisCodigo[i].arg_tipoRef+'","f01_FIG_OBS":"'+cobsReferencias+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaRefHist = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------
  //------------------- BLOQUE PELIGORS POTENCIALES-------------

 var contPelPot=0;
  arrDibujoPP.forEach(function (item, index, array) 
  {
    arrPP[contPelPot].val_pe = $("#peligropPP"+item.id+"").val();
    arrPP[contPelPot].val_pel = $("#peligropPPO"+item.id+"").val();
    arrPP[contPelPot].val_cau = $("#causasPP"+item.id+"").val();
    contPelPot++;
  });
  var bloqueFiguraProtecpp = '[{"tipo":"GRD","campos":"f01_peligro_DES_|f01_reja_DES_|f01_reja_INT_","titulos":"Peligros Potenciales|Peligros Potenciales|Nivel de Peligro"}';
  for (var i = 0; i<arrDibujoPP.length;i++)
  {
    //////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigurapp = '{"f01_peligro_DES_":"'+arrPP[i].val_pe+'","f01_reja_DES_":"'+arrPP[i].val_pel+'","f01_reja_INT_":"'+arrPP[i].val_cau+'","f01_peligro_DES_valor":"'+arrPP[i].val_pe+'","f01_reja_DES_valor":"'+arrPP[i].val_pel+'","f01_reja_INT_valor":"'+arrPP[i].val_cau+'"}';
    bloqueFiguraProtecpp = (bloqueFiguraProtecpp.concat(',').concat(grillaFigurapp));

  }
  bloqueFiguraProtecpp = bloqueFiguraProtecpp.concat(']');
   var peligro_potencial_grilla = bloqueFiguraProtecpp;
  //PTR_PEL_POT1 = bloqueFiguraProtecpp.concat(']');
  ////console.log(PTR_PEL_POT1,'INSERSION DE GRILLAS PELIGROS');

  //------------------- Patologías Edificación---------------------

  
 var contPE=0;
  arrDibujoPE.forEach(function (item, index, array) 
  {
    arrPE[contPE].val_daños = $("#dañosPE"+item.id+"").val();
    arrPE[contPE].val_causas = $("#causasPE"+item.id+"").val();
    contPE++;
  });
  var bloqueFiguraProtecPE = '[{"tipo":"GRD","campos":"f01_reja_DES_|f01_causa_DES_","titulos":"Daños|Causas"}';
  for (var i = 0; i<arrDibujoPE.length;i++)
  {
    //////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraPE = '{"f01_reja_DES_":"'+arrPE[i].val_daños+'","f01_causa_DES_":"'+arrPE[i].val_causas+'","f01_reja_DES_valor":"'+arrPE[i].val_daños+'","f01_causa_DES_valor":"'+arrPE[i].val_causas+'"}';
    bloqueFiguraProtecPE = (bloqueFiguraProtecPE.concat(',').concat(grillaFiguraPE));

  }
  bloqueFiguraProtecPE = bloqueFiguraProtecPE.concat(']');
  var patologia_edif_grilla = bloqueFiguraProtecPE;
  // PTR_PAT_EDIF1 = bloqueFiguraProtecPE.concat(']');
  ////console.log(PTR_PAT_EDIF1,'PATOLOGIAS EDIFICACION');

  //------------------- Instalaciones de Estado---------------------

  var contINE=0;
   arrDibujoINE.forEach(function (item, index, array) 
   {
   arrINE[contINE].var_inst_est = $("#instalacionINE"+item.id+"").val();
   arrINE[contINE].var_pisose = $("#pisoservINE"+item.id+"").val();
   arrINE[contINE].var_est = $("#estadoINE"+item.id+"").val();
   arrINE[contINE].var_int = $("#integridadINE"+item.id+"").val();
    contINE++;
  });
   var bloqueFiguraProtecINE = '[{"tipo":"GRD","campos":"f01_Instalaciones_DESC|f01_servidumbre_DESC|f01_estado_G_SERV_PROV|f01_integridad_G_SERV_PROV","titulos":"Instalaciones Estado|Paso de Servidumbre|Estado|Integridad"}';
   for (var i = 0; i<arrDibujoINE.length;i++)
   {
    var grillaFiguraINE = '{"f01_Instalaciones_DESC":"'+arrINE[i].var_inst_est+'","f01_servidumbre_DESC":"'+arrINE[i].var_pisose+'","f01_estado_G_SERV_PROV":"'+arrINE[i].var_est+'","f01_integridad_G_SERV_PROV":"'+arrINE[i].var_int+'","f01_Instalaciones_DESC_valor": "'+arrINE[i].var_inst_est+'","f01_servidumbre_DESC_valor": "'+arrINE[i].var_pisose+'","f01_estado_G_SERV_PROV_valor": "'+arrINE[i].var_est+'","f01_integridad_G_SERV_PROV_valor": "'+arrINE[i].var_int+'"}';
    bloqueFiguraProtecINE = (bloqueFiguraProtecINE.concat(',').concat(grillaFiguraINE));
  }
  bloqueFiguraProtecINE = bloqueFiguraProtecINE.concat(']');
  var instalaciones_estado_grilla = bloqueFiguraProtecINE;
  //PTR_INST_EST1 = bloqueFiguraProtecINE.concat(']');
  ////console.log(PTR_INST_EST1,'INSTALACIONES DE ESTADO');

   //------------------- Servicios ---------------------

  var contSERV=0;
   arrDibujoSERV.forEach(function (item, index, array) 
   {
    arrSERV[contSERV].val_desc = $("#descripcionSERV"+item.id+"").val();
    arrSERV[contSERV].val_prov = $("#proveedorSERV"+item.id+"").val();
    contSERV++;
  });
   var bloqueFiguraProtecSERV = '[{"tipo":"GRD","campos":"f01_emision_G_SERV_DESC|f01_emision_G_SERV_PROV","titulos":"Descripción|Proveedor"}';

   for (var i = 0; i<arrDibujoSERV.length;i++)
   {
    //////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFiguraSERV = '{"f01_emision_G_SERV_DESC":"'+arrSERV[i].val_desc+'","f01_emision_G_SERV_PROV":"'+arrSERV[i].val_prov+'","f01_emision_G_SERV_DESC_valor":"'+arrSERV[i].val_desc+'","f01_emision_G_SERV_PROV_valor":"'+arrSERV[i].val_prov+'"}';
    bloqueFiguraProtecSERV = (bloqueFiguraProtecSERV.concat(',').concat(grillaFiguraSERV));

  }
  bloqueFiguraProtecSERV = bloqueFiguraProtecSERV.concat(']');
  var servios_grilla = bloqueFiguraProtecSERV;
 // PTR_SERVICIOS1 = bloqueFiguraProtecSERV.concat(']');
  ////console.log(PTR_SERVICIOS1,'SERVICIOS');
  //------------------- FINAL--------------------------------------

  //------------------- GRILLA MURO EXTERIOR ------------  
 var cont=0;
  muro_extDibujo.forEach(function (item, index, array) 
  {
    muro_extCodigo[cont].arg_estado = $("#estado"+item.id+"").val();
    muro_extCodigo[cont].arg_integridad = $("#integridad"+item.id+"").val();
    muro_extCodigo[cont].arg_tipo = $("#tipo"+item.id+"").val();
    muro_extCodigo[cont].arg_material = $("#material"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"pint_ext_estado|pint_ext_inte|pint_ext_tipo|pint_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<muro_extDibujo.length;i++)
  {
    //////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigura = '{"pint_ext_estado":"'+muro_extCodigo[i].arg_estado+'","pint_ext_estado_valor":"'+muro_extCodigo[i].arg_estado+'","pint_ext_inte":"'+muro_extCodigo[i].arg_integridad+'","pint_ext_inte_valor":"'+muro_extCodigo[i].arg_integridad+'","pint_ext_tipo":"'+muro_extCodigo[i].arg_tipo+'","pint_ext_tipo_valor":"'+muro_extCodigo[i].arg_tipo+'","pint_ext_mat":"'+muro_extCodigo[i].arg_material+'","pint_ext_mat_valor":"'+muro_extCodigo[i].arg_material+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaMuroExt = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------



  //------------------- GRILLA MURO INTERIOR ------------  
  var cont=0;
  muro_intDibujo.forEach(function (item, index, array) 
  {
    muro_intCodigo[cont].arg_estadoInt = $("#estadoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_integridadInt = $("#integridadInt"+item.id+"").val();
    muro_intCodigo[cont].arg_tipoInt = $("#tipoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_materialInt = $("#materialInt"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"pint_int_estado|pint_int_inte|pint_int_tipo|pint_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<muro_intDibujo.length;i++)
  {
    //////console.log("grillapruebaI"+i+"",arrCodigo[i]);
    var grillaFigura = '{"pint_int_estado":"'+muro_intCodigo[i].arg_estadoInt+'","pint_int_inte":"'+muro_intCodigo[i].arg_integridadInt+'","pint_int_tipo":"'+muro_intCodigo[i].arg_tipoInt+'","pint_int_mat":"'+muro_intCodigo[i].arg_materialInt+'","pint_int_estado_valor":"'+muro_intCodigo[i].arg_estadoInt+'","pint_int_inte_valor":"'+muro_intCodigo[i].arg_integridadInt+'","pint_int_tipo_valor":"'+muro_intCodigo[i].arg_tipoInt+'","pint_int_mat_valor":"'+muro_intCodigo[i].arg_materialInt+'"}';
    bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaMuroInt = bloqueFiguraProtec;
  //------------------- FIN GRILLA ----------------------------

  //------------------- GRILLA ACABADO EXTERIOR ------------  
  var cont=0;
  aca_extDibujo.forEach(function (item, index, array) 
  {
    aca_extCodigo[cont].arg_estadoAcaExt = $("#estadoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_integridadAcaExt = $("#integridadAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_tipoAcaExt = $("#tipoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_materialAcaExt = $("#materialAcaExt"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"aca_ext_estado|aca_ext_inte|aca_ext_tipo|aca_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<aca_extDibujo.length;i++)
  {
  var grillaFigura = '{"aca_ext_estado":"'+aca_extCodigo[i].arg_estadoAcaExt+'","aca_ext_estado_valor":"'+aca_extCodigo[i].arg_estadoAcaExt+'","aca_ext_inte":"'+aca_extCodigo[i].arg_integridadAcaExt+'","aca_ext_inte_valor":"'+aca_extCodigo[i].arg_integridadAcaExt+'","aca_ext_tipo":"'+aca_extCodigo[i].arg_tipoAcaExt+'","aca_ext_tipo_valor":"'+aca_extCodigo[i].arg_tipoAcaExt+'","aca_ext_mat":"'+aca_extCodigo[i].arg_materialAcaExt+'","aca_ext_mat_valor":"'+aca_extCodigo[i].arg_materialAcaExt+'"}';
  bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));

  }
  bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
  var cgrillaAcabadoEx = bloqueFiguraProtec; 

  //------------------- FIN GRILLA ----------------------------

  //------------------- GRILLA ACABADO INTERIOR ------------  
  var cont=0;
  aca_intDibujo.forEach(function (item, index, array) 
  {
    aca_intCodigo[cont].arg_estadoAcaInt = $("#estadoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_integridadAcaInt = $("#integridadAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_tipoAcaInt = $("#tipoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_materialAcaInt = $("#materialAcaInt"+item.id+"").val();
    cont++;

  });
  var bloqueFiguraProtec = '[{"tipo":"GRD","campos":"aca_int_estado|aca_int_inte|aca_int_tipo|aca_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
  for (var i = 0; i<aca_intDibujo.length;i++)
  {
 
  var grillaFigura = '{"aca_int_estado":"'+aca_intCodigo[i].arg_estadoAcaInt+'","aca_int_inte":"'+aca_intCodigo[i].arg_integridadAcaInt+'","aca_int_tipo":"'+aca_intCodigo[i].arg_tipoAcaInt+'","aca_int_mat":"'+aca_intCodigo[i].arg_materialAcaInt+'","aca_int_estado_valor": "'+aca_intCodigo[i].arg_estadoAcaInt+'","aca_int_inte_valor": "'+aca_intCodigo[i].arg_integridadAcaInt+'","aca_int_tipo_valor": "'+aca_intCodigo[i].arg_tipoAcaInt+'","aca_int_mat_valor": "'+aca_intCodigo[i].arg_materialAcaInt+'"}';
  bloqueFiguraProtec = (bloqueFiguraProtec.concat(',').concat(grillaFigura));
}
bloqueFiguraProtec = bloqueFiguraProtec.concat(']');
var cgrillaAcabadoInt = bloqueFiguraProtec; 


  //--------------------------------GISEL-------------------------------------------//
  //-----------------GRILLA PAREDES EXTERIORES--------------------------------------//

  var cont=0;
  arrDibujoPEX.forEach(function (item, index, array) 
  {
    arrPEX[cont].val_est_con = $("#estadocPEX"+item.id+"").val();
    arrPEX[cont].val_int = $("#integridadPEX"+item.id+"").val();
    arrPEX[cont].val_tipo = $("#tipoPEX"+item.id+"").val();
    arrPEX[cont].val_mat = $("#materialPEX"+item.id+"").val();
    cont++;
  });
  var bloqueParedExterna = '[{"tipo": "GRD","campos": "par_estado|par_inte|par_tipo|par_mat", "titulos": "EstadodeConservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';

  for (var i = 0; i<arrPEX.length;i++)
  {
    var grillaParedExterna = '{"par_mat": "'+arrPEX[i].val_mat+'","par_inte": "'+arrPEX[i].val_int+'","par_tipo": "'+arrPEX[i].val_tipo+'","par_estado": "'+arrPEX[i].val_est_con+'","par_mat_valor": 1,"par_inte_valor": 1,"par_tipo_valor": 1,"par_estado_valor": 2}';
    bloqueParedExterna = (bloqueParedExterna.concat(',').concat(grillaParedExterna));
     
  }
  bloqueParedExterna = bloqueParedExterna.concat(']');
  var cgrillaParedExterna = bloqueParedExterna;


   //-------------FIN GRILLA PAREDES EXTERIORES-------------------//

   //---------------GRILLA COBERTURA  DEL TECHO-------------------//
  var cont=0;
  arrDibujoCT.forEach(function (item, index, array) 
  {
      arrCT[cont].val_est_con = $("#estadocCT"+item.id+"").val();
      arrCT[cont].val_inte = $("#integridadCT"+item.id+"").val();
      arrCT[cont].val_tipo = $("#tipoCT"+item.id+"").val();
      arrCT[cont].val_mate = $("#materialCT"+item.id+"").val();
      cont++;
    });
  var bloqueCoberturaTecho = '[ {"tipo": "GRD","campos": "cob_estado|cob_inte|cob_tipo|par_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCT.length;i++)
  {
    var grillaCoberturaTecho = '{"par_mat": "'+arrCT[i].val_mate+'","cob_inte": "'+arrCT[i].val_inte+'","cob_tipo": "'+arrCT[i].val_tipo+'","cob_estado": "'+arrCT[i].val_est_con+'","par_mat_valor": 1,"cob_inte_valor": 1,"cob_tipo_valor": 1,"cob_estado_valor": 1}';
    bloqueCoberturaTecho = (bloqueCoberturaTecho.concat(',').concat(grillaCoberturaTecho));    
  }

  bloqueCoberturaTecho = bloqueCoberturaTecho.concat(']');
  var cgrillaCoberturaTecho = bloqueCoberturaTecho; 

   //---------------GRILLA PAREDES INTERIORES -------------------//

   var cont=0;
        arrDibujoPI.forEach(function (item, index, array) 
        {
          arrPI[cont].val_est_conser = $("#estadocPI"+item.id+"").val();
          arrPI[cont].val_int = $("#integridadPI"+item.id+"").val();
          arrPI[cont].val_tipo = $("#tipoPI"+item.id+"").val();
          arrPI[cont].val_mat = $("#materialPI"+item.id+"").val();
          cont++;
        });
  var bloqueParedeInterior = '[ {"tipo": "GRD","campos": "pari_estado|pari_inte|pari_tipo|pari_mat","titulos": "Estado Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrPI.length;i++)
  {
    var grillaParedInterior = '{"pari_mat": "'+arrPI[i].val_mat+'","pari_inte": "'+arrPI[i].val_int+'","pari_tipo": "'+arrPI[i].val_tipo+'","pari_estado": "'+arrPI[i].val_est_conser+'","pari_mat_valor": 1,"pari_inte_valor": 1,"pari_tipo_valor": 2,"pari_estado_valor": 1}';
    bloqueParedeInterior = (bloqueParedeInterior.concat(',').concat(grillaParedInterior));    
  }

  bloqueParedeInterior = bloqueParedeInterior.concat(']');
  var cgrillaParedInterior = bloqueParedeInterior; 

  //---------------FIN  PAREDES INTERIORES--------------------------//

//---------------GRILLA ENTRE PISOS PTR_ENT_PIS-------------------//

     var cont=0;
        arrDibujoEPI.forEach(function (item, index, array) 
        {
          arrEPI[cont].val_esta = $("#estadocEPI"+item.id+"").val();
          arrEPI[cont].val_int = $("#integridadEPI"+item.id+"").val();
          arrEPI[cont].val_tipo = $("#tipoEPI"+item.id+"").val();
          arrEPI[cont].val_mat = $("#materialEPI"+item.id+"").val();
          cont++;
        });
  var bloqueEntrePisos = '[{"tipo": "GRD","campos": "ent_estado|ent_inte|ent_tipo|ent_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrEPI.length;i++)
  {

    var grillaEntrePisos = '{"ent_mat": "'+arrEPI[i].val_mat+'","ent_inte": "'+arrEPI[i].val_int+'","ent_tipo": "'+arrEPI[i].val_tipo+'","ent_estado": "'+arrEPI[i].val_esta+'","ent_mat_valor": 1,"ent_inte_valor": 1,"ent_tipo_valor": 1,"ent_estado_valor": 1}';
    bloqueEntrePisos = (bloqueEntrePisos.concat(',').concat(grillaEntrePisos));    
  }
  bloqueEntrePisos = bloqueEntrePisos.concat(']');
  var cgrillaEntrePisos = bloqueEntrePisos; 


 //---------------FIN GRILLA ENTRE PISOS----------------------------//

 //-----------------GRILLA ESCALERAS  -------------------------------//
    var cont=0;
        arrDibujoES.forEach(function (item, index, array) 
        {
          arrES[cont].val_estado = $("#estadocES"+item.id+"").val();
          arrES[cont].val_int = $("#integridadES"+item.id+"").val();
          arrES[cont].val_tipo = $("#tipoES"+item.id+"").val();
          arrES[cont].val_mat = $("#materialES"+item.id+"").val();
          cont++;
        });
  var bloqueEscalera = '[{"tipo": "GRD","campos": "esc_estado|esc_inte|esc_tipo|esc_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrES.length;i++)
  {

    var grillaEscalera = '{"esc_mat": "'+arrES[i].val_mat+'","esc_inte": "'+arrES[i].val_int+'","esc_tipo": "'+arrES[i].val_tipo+'","esc_estado": "'+arrES[i].val_estado+'","esc_mat_valor": 2,"esc_inte_valor": 1,"esc_tipo_valor": 4,"esc_estado_valor": 2}';
    bloqueEscalera = (bloqueEscalera.concat(',').concat(grillaEscalera));    
  }
  bloqueEscalera = bloqueEscalera.concat(']');
  var cgrillaEscalera = bloqueEscalera; 

 //---------------FIN GRILLA ESCALERAS----------------------------//

 //---------------GRILLA CIELOS ------------------//

     var cont=0;
      arrDibujoCIE.forEach(function (item, index, array) 
      {
          arrCIE[cont].val_est = $("#estadocCIE"+item.id+"").val();
          arrCIE[cont].val_int = $("#integridadCIE"+item.id+"").val();
          arrCIE[cont].val_tipo = $("#tipoCIE"+item.id+"").val();
          arrCIE[cont].val_mat = $("#materialCIE"+item.id+"").val();
          cont++;
      });
  var bloqueCielo = '[{"tipo": "GRD","campos": "cie_estado|cie_inte|cie_tipo|cie_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCIE.length;i++)
  {

    var grillaCielo = '{"cie_mat": "'+arrCIE[i].val_mat+'","cie_inte": "'+arrCIE[i].val_int+'","cie_tipo": "'+arrCIE[i].val_tipo+'","cie_estado": "'+arrCIE[i].val_est+'","cie_mat_valor": 2,"cie_inte_valor": 1,"cie_tipo_valor": 4,"cie_estado_valor": 2}';
    bloqueCielo = (bloqueCielo.concat(',').concat(grillaCielo));    
  }
  bloqueCielo = bloqueCielo.concat(']');
  var cgrillaCielo = bloqueCielo; 
 //---------------FIN CIELOS----------------------------//

 //---------------GRILLA BALCONES ------------------//

   var cont=0;
        arrDibujoBAL.forEach(function (item, index, array) 
        {
          arrBAL[cont].val_est = $("#estadocBAL"+item.id+"").val();
          arrBAL[cont].val_inte = $("#integridadBAL"+item.id+"").val();
          arrBAL[cont].val_tipo = $("#tipoBAL"+item.id+"").val();
          arrBAL[cont].val_mat = $("#materialBAL"+item.id+"").val();
          cont++;
        });

  var bloqueBalcon = '[ {"tipo": "GRD","campos": "bal_estado|bal_inte|bal_tipo|bal_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrBAL.length;i++)
  {
    var grillaBalcon = '{"bal_mat": "'+arrBAL[i].val_mat+'","bal_inte": "'+arrBAL[i].val_inte+'","bal_tipo": "'+arrBAL[i].val_tipo+'","bal_estado": "'+arrBAL[i].val_est+'","bal_mat_valor": 1,"bal_inte_valor": 2,"bal_tipo_valor": 1,"bal_estado_valor": 3}';
    bloqueBalcon = (bloqueBalcon.concat(',').concat(grillaBalcon));    
  }

  bloqueBalcon = bloqueBalcon.concat(']');
  var cgrillaBalcon = bloqueBalcon; 

 //---------------FIN GRILLA BALCONES----------------------------//

 //---------------GRILLA ESTRUCTURA CUBIERTA ------------------//

 var cont=0;
        arrDibujoEC.forEach(function (item, index, array) 
        {
          arrEC[cont].val_est = $("#estadocEC"+item.id+"").val();
          arrEC[cont].val_int = $("#integridadEC"+item.id+"").val();
          arrEC[cont].val_tipo = $("#tipoEC"+item.id+"").val();
          arrEC[cont].val_mat = $("#materialEC"+item.id+"").val();
          cont++;
        });
  var bloqueEstructuraCubierta = '[{"tipo": "GRD","campos": "est_cub_estado|est_cub_inte|est_cub_tipo|est_cub_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrEC.length;i++)
  {

    var grillaEstructuraCubierta = '{"est_cub_mat": "'+arrEC[i].val_mat+'","est_cub_inte": "'+arrEC[i].val_int+'","est_cub_tipo": "'+arrEC[i].val_tipo+'","est_cub_estado": "'+arrEC[i].val_est+'","est_cub_mat_valor": 2,"est_cub_inte_valor": 3,"est_cub_tipo_valor": 2,"est_cub_estado_valor": 1}';
    bloqueEstructuraCubierta = (bloqueEstructuraCubierta.concat(',').concat(grillaEstructuraCubierta));    
  }
  bloqueEstructuraCubierta = bloqueEstructuraCubierta.concat(']');
  var cgrillaEstructuraCubierta = bloqueEstructuraCubierta; 

 //---------------FIN GRILLA ESTRUCTURA CUBIERTA-------------------//

//------------------------GRILLA PISOS---------------------------//
        
      var cont=0;
      arrDibujoP.forEach(function (item, index, array) 
        {
          arrP[cont].val_est = $("#estadocP"+item.id+"").val();
          arrP[cont].val_int = $("#integridadP"+item.id+"").val();
          arrP[cont].val_tipo = $("#tipoP"+item.id+"").val();
          arrP[cont].val_mat = $("#materialP"+item.id+"").val();
          cont++;
      });

  var bloquePiso = '[{"tipo": "GRD","campos": "piso_estado|piso_inte|piso_tipo|piso_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrP.length;i++)
  {

    var grillaPiso = '{"piso_mat": "'+arrP[i].val_mat+'","piso_inte": "'+arrP[i].val_int+'","piso_tipo": "'+arrP[i].val_tipo+'","piso_estado": "'+arrP[i].val_est+'","piso_mat_valor": 1,"piso_inte_valor": 1,"piso_tipo_valor": 1,"piso_estado_valor": 2}';
    bloquePiso = (bloquePiso.concat(',').concat(grillaPiso));    
  }
  bloquePiso = bloquePiso.concat(']');
  var cgrillaPiso = bloquePiso; 

//JSON TANYA
 /////GRILLA Vanos Ventanas Interiores /////////
 var contvvi=0;
   arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
        {
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_estado = $("#vent_int_estado"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_inte = $("#vent_int_inte"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_tipo = $("#vent_int_tipo"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[contvvi].arg_vent_int_mat = $("#vent_int_mat"+item.id+"").val();
          contvvi++;
         
        });
    var bloqueFiguraProtecvvi= '[{"tipo":"GRD","campos":"vent_int_estado|vent_int_inte|vent_int_tipo|vent_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
   
    var grillaFiguravvi = '';   
    for (var i = 0; i<arrCodigoEleComVanosVenInteriores.length;i++)
    {
    grillaFiguravvi = '{"vent_int_estado":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_estado +'","vent_int_inte":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_inte +'","vent_int_tipo":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_tipo +'","vent_int_mat":"'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_mat +'","vent_int_estado_valor": "'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_estado +'","vent_int_inte_valor": "'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_inte +'","vent_int_tipo_valor": "'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_tipo +'","vent_int_mat_valor": "'+ arrCodigoEleComVanosVenInteriores[i].arg_vent_int_mat +'"}';

    bloqueFiguraProtecvvi = (bloqueFiguraProtecvvi.concat(',').concat(grillaFiguravvi));
     
    }
    bloqueFiguraProtecvvi = bloqueFiguraProtecvvi.concat(']');
    var van_venta_int = bloqueFiguraProtecvvi;
    //alert(bloqueFiguraProtecvvi);


    //--registrar ventana interiores---
    var contdvi=0;
    arrDibujovent_inte_es_in.forEach(function (item, index, array) 
        {
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_estado = $("#vent_inte_estado"+item.id+"").val();
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_inte = $("#vent_inte_inte"+item.id+"").val();
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_tipo = $("#vent_inte_tipo"+item.id+"").val();
          arrCodigovent_inte_es_in[contdvi].arg_vent_inte_mat = $("#vent_inte_mat"+item.id+"").val();
          contdvi++;
         
        });
    var bloqueFiguraProtecdvi= '[{"tipo":"GRD","campos":"vent_inte_estado|vent_inte_inte|vent_inte_tipo|vent_inte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
      ////console.log('titulos',bloqueFiguraProtecdvi);
      var grillaFiguradvi = '';   
      for (var i = 0; i<arrCodigovent_inte_es_in.length;i++)
      {
      grillaFiguradvi = '{"vent_inte_estado":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_estado +'","vent_inte_inte":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_inte +'","vent_inte_tipo":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_tipo +'","vent_inte_mat":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_mat +'","vent_inte_estado_valor":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_estado +'","vent_inte_inte_valor":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_inte +'","vent_inte_tipo_valor":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_tipo +'","vent_inte_mat_valor":"'+ arrCodigovent_inte_es_in[i].arg_vent_inte_mat +'"}';
      bloqueFiguraProtecdvi = (bloqueFiguraProtecdvi.concat(',').concat(grillaFiguradvi));
       
      }
      bloqueFiguraProtecdvi = bloqueFiguraProtecdvi.concat(']');
     var ventana_interior = bloqueFiguraProtecdvi;
    //alert(bloqueFiguraProtecdvi);
    

    //----registrar vanos ventanas exteriores-------
    var contdvve=0;
    arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
        {
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_estado = $("#vent_ext_estado"+item.id+"").val();
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_inte = $("#vent_ext_inte"+item.id+"").val();
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
          arrCodigoVanVenExt_es_int[contdvve].arg_vent_ext_mat = $("#vent_ext_mat"+item.id+"").val();
          contdvve++;
         
        });
    var bloqueFiguraProtecdvve= '[{"tipo":"GRD","campos":"vent_ext_estado|vent_ext_inte|vent_ext_tipo|vent_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    ////console.log('titulos',bloqueFiguraProtecdvve);
    var grillaFiguradvve = '';   
    for (var i = 0; i<arrCodigoVanVenExt_es_int.length;i++)
    {
    grillaFiguradvve = '{"vent_ext_estado":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_estado +'","vent_ext_inte":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_inte +'","vent_ext_tipo":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_tipo +'","vent_ext_mat":"'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_mat +'","vent_ext_estado_valor": "'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_estado +'","vent_ext_inte_valor": "'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_inte +'","vent_ext_tipo_valor": "'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_tipo +'","vent_ext_mat_valor": "'+ arrCodigoVanVenExt_es_int[i].arg_vent_ext_mat +'"}';
    bloqueFiguraProtecdvve = (bloqueFiguraProtecdvve.concat(',').concat(grillaFiguradvve));
     
    }
    bloqueFiguraProtecdvve = bloqueFiguraProtecdvve.concat(']');
    var van_vent_exterior = bloqueFiguraProtecdvve;

    //(bloqueFiguraProtecdvve);
    ///GRILLA3

    ///GRILLA4 
    var contdve=0;
    arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_estado = $("#vent_exte_estado"+item.id+"").val();
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_inte = $("#vent_exte_inte"+item.id+"").val();
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_tipo = $("#vent_exte_tipo"+item.id+"").val();
          arrCodigo_ptr_vent_ext[contdve].arg_vent_exte_mat = $("#vent_exte_mat"+item.id+"").val();
          contdve++;
         
        });
    var bloqueFiguraProtecdve= '[{"tipo":"GRD","campos":"vent_exte_estado|vent_exte_inte|vent_exte_tipo|vent_exte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    ////console.log('titulos',bloqueFiguraProtecdve);
    var grillaFiguradve = '';   
    for (var i = 0; i<arrCodigo_ptr_vent_ext.length;i++)
    {
    grillaFiguradve = '{"vent_exte_estado":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_estado +'","vent_exte_inte":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_inte +'","vent_exte_tipo":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_tipo +'","vent_exte_mat":"'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_mat +'","vent_exte_estado_valor": "'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_estado +'","vent_exte_inte_valor": "'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_inte +'","vent_exte_tipo_valor": "'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_tipo +'","vent_exte_mat_valor": "'+ arrCodigo_ptr_vent_ext[i].arg_vent_exte_mat +'"}';
    bloqueFiguraProtecdve = (bloqueFiguraProtecdve.concat(',').concat(grillaFiguradve));
     
    }
    bloqueFiguraProtecdve = bloqueFiguraProtecdve.concat(']');
    var ventana_exterior = bloqueFiguraProtecdve;
    //alert(bloqueFiguraProtecdve);
    ///GRILLA4 
    //GRILL5-------registrar vanos puertas interiores----
    var contdvpi=0;
    arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_estado = $("#pue_int_estado"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_inte = $("#pue_int_inte"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_tipo = $("#pue_int_tipo"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[contdvpi].arg_pue_int_mat = $("#pue_int_mat"+item.id+"").val();
          contdvpi++;         
        });
    var bloqueFiguraProtecdvpi= '[{"tipo":"GRD","campos":"pue_int_estado|pue_int_inte|pue_int_tipo|pue_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradvpi = '';   
    for (var i = 0; i<arrCodigo_ptr_van_pue_int.length;i++)
    {
    grillaFiguradvpi = '{"pue_int_estado":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_estado +'","pue_int_inte":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_inte +'","pue_int_tipo":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_tipo +'","pue_int_mat":"'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_mat +'","pue_int_estado_valor": "'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_estado +'","pue_int_inte_valor": "'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_inte +'","pue_int_tipo_valor": "'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_tipo +'","pue_int_mat_valor": "'+ arrCodigo_ptr_van_pue_int[i].arg_pue_int_mat +'"}';
    bloqueFiguraProtecdvpi = (bloqueFiguraProtecdvpi.concat(',').concat(grillaFiguradvpi));
    }
    bloqueFiguraProtecdvpi = bloqueFiguraProtecdvpi.concat(']');
    var van_puert_int = bloqueFiguraProtecdvpi;
    //alert(bloqueFiguraProtecdvpi);
    ///GRILLA5
    //--GRILLA6 --registra vanos puertas exteriores------
    var contdvpe=0;
    arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_estado = $("#pue_ext_estado"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_inte = $("#pue_ext_inte"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_tipo = $("#pue_ext_tipo"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[contdvpe].arg_pue_ext_mat = $("#pue_ext_mat"+item.id+"").val();
          contdvpe++;         
        });
    var bloqueFiguraProtecdvpe= '[{"tipo":"GRD","campos":"pue_ext_estado|pue_ext_inte|pue_ext_tipo|pue_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradvpe = '';   
    for (var i = 0; i<arrCodigo_ptr_van_pue_ext.length;i++)
    {
    grillaFiguradvpe = '{"pue_ext_estado":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_estado +'","pue_ext_inte":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_inte +'","pue_ext_tipo":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_tipo +'","pue_ext_mat":"'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_mat +'","pue_ext_estado_valor": "'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_estado +'","pue_ext_inte_valor": "'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_inte +'","pue_ext_tipo_valor": "'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_tipo +'","pue_ext_mat_valor": "'+ arrCodigo_ptr_van_pue_ext[i].arg_pue_ext_mat +'"}';
    bloqueFiguraProtecdvpe = (bloqueFiguraProtecdvpe.concat(',').concat(grillaFiguradvpe));
    }
    bloqueFiguraProtecdvpe = bloqueFiguraProtecdvpe.concat(']');
    var van_puert_ext = bloqueFiguraProtecdvpe;
    ///GRILLA6
    ///GRILLA7
     var contdpi=0;
    arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_estado = $("#pue_inte_estado"+item.id+"").val();
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_inte = $("#pue_inte_inte"+item.id+"").val();
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_tipo = $("#pue_inte_tipo"+item.id+"").val();
          arrCodigo_ptr_pue_int[contdpi].arg_pue_inte_mat = $("#pue_inte_mat"+item.id+"").val();
          contdpi++;         
        });
    var bloqueFiguraProtecdpi= '[{"tipo":"GRD","campos":"pue_inte_estado|pue_inte_inte|pue_inte_tipo|pue_inte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradpi = '';   
    for (var i = 0; i<arrCodigo_ptr_pue_int.length;i++)
    {
    grillaFiguradpi = '{"pue_inte_estado":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_estado +'","pue_inte_inte":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_inte +'","pue_inte_tipo":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_tipo +'","pue_inte_mat":"'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_mat +'", "pue_inte_estado_valor": "'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_estado +'","pue_inte_inte_valor": "'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_inte +'","pue_inte_tipo_valor": "'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_tipo +'","pue_inte_mat_valor": "'+ arrCodigo_ptr_pue_int[i].arg_pue_inte_mat +'"}';
    bloqueFiguraProtecdpi = (bloqueFiguraProtecdpi.concat(',').concat(grillaFiguradpi));
    }
    bloqueFiguraProtecdpi = bloqueFiguraProtecdpi.concat(']');
    var puerta_ineterior = bloqueFiguraProtecdpi;
    ///GRILLA7  
    ///GRILLA8
     var contdpe=0;
      arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
          {
            arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_estado = $("#pue_exte_estado"+item.id+"").val();
            arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_inte = $("#pue_exte_inte"+item.id+"").val();
            arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_tipo = $("#pue_exte_tipo"+item.id+"").val();
            arrCodigo_ptr_pue_ext[contdpe].arg_pue_exte_mat = $("#pue_exte_mat"+item.id+"").val();
            contdpe++;         
          });
      var bloqueFiguraProtecdpe= '[{"tipo":"GRD","campos":"pue_exte_estado|pue_exte_inte|pue_exte_tipo|pue_exte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
      var grillaFiguradpe = '';   
      for (var i = 0; i<arrCodigo_ptr_pue_ext.length;i++)
      {
      grillaFiguradpe = '{"pue_exte_estado":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_estado +'","pue_exte_inte":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_inte +'","pue_exte_tipo":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_tipo +'","pue_exte_mat":"'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_mat +'","pue_exte_estado_valor": "'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_estado +'","pue_exte_inte_valor": "'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_inte +'","pue_exte_tipo_valor": "'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_tipo +'","pue_exte_mat_valor": "'+ arrCodigo_ptr_pue_ext[i].arg_pue_exte_mat +'"}';
      bloqueFiguraProtecdpe = (bloqueFiguraProtecdpe.concat(',').concat(grillaFiguradpe));
      }
      bloqueFiguraProtecdpe = bloqueFiguraProtecdpe.concat(']');
      var puerta_externa = bloqueFiguraProtecdpe;
    ///GRILLA8
    ///GRILLA9
    var contdr=0;
    arrDibujo_ptr_reja.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_reja[contdr].arg_reja_estado = $("#reja_estado"+item.id+"").val();
          arrCodigo_ptr_reja[contdr].arg_reja_inte = $("#reja_inte"+item.id+"").val();
          arrCodigo_ptr_reja[contdr].arg_reja_tipo = $("#reja_tipo"+item.id+"").val();
          arrCodigo_ptr_reja[contdr].arg_reja_mat = $("#reja_mat"+item.id+"").val();
          contdr++;         
        });
    var bloqueFiguraProtecdr= '[{"tipo":"GRD","campos":"reja_estado|reja_inte|reja_tipo|reja_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
    var grillaFiguradr = '';   
    for (var i = 0; i<arrCodigo_ptr_reja.length;i++)
    {
    grillaFiguradr = '{"reja_estado":"'+ arrCodigo_ptr_reja[i].arg_reja_estado +'","reja_inte":"'+ arrCodigo_ptr_reja[i].arg_reja_inte +'","reja_tipo":"'+ arrCodigo_ptr_reja[i].arg_reja_tipo +'","reja_mat":"'+ arrCodigo_ptr_reja[i].arg_reja_mat +'","reja_estado_valor": "'+ arrCodigo_ptr_reja[i].arg_reja_estado +'","reja_inte_valor": "'+ arrCodigo_ptr_reja[i].arg_reja_inte +'","reja_tipo_valor": "'+ arrCodigo_ptr_reja[i].arg_reja_tipo +'","reja_mat_valor": "'+ arrCodigo_ptr_reja[i].arg_reja_mat +'"}';
    bloqueFiguraProtecdr = (bloqueFiguraProtecdr.concat(',').concat(grillaFiguradr));
    }
    bloqueFiguraProtecdr = bloqueFiguraProtecdr.concat(']');
    var rejas_grila = bloqueFiguraProtecdr;
    ///GRILLA9


    //FIN JSON TANYA

  //------------------- FIN GRILLA ----------------------------

  var cfechaConstruccion_in = $("#cfechaConstruccion_in").val();
  var cautor_in = $("#cautor_in").val();
  var cpreexistencia_in = $("#cpreexistencia_in").val();
  var cpropietarios_in = $("#cpropietarios_in").val();
  var cusoOriginal_in = $("#cusoOriginal_in").val();
  var chechoHistorico_in = $("#chechoHistorico_in").val();
  var cfiestaReligiosa_in = $("#cfiestaReligiosa_in").val();
  var cdatoHistorico_in = $("#cdatoHistorico_in").val();
  var cfuente_in = $("#cfuente_in").val();
  var cfechaConstruccion_in3 = $("#cfechaConstruccion_in3").val();
  var cautor_in3 = $("#cautor_in3").val();
  var cpreexistencia_in3 = $("#cpreexistencia_in3").val();
  var cpropietarios_in3 = $("#cpropietarios_in3").val();
  var cusoOriginal_in3 = $("#cusoOriginal_in3").val();
  var chechoHistorico_in3 = $("#chechoHistorico_in3").val();
  var cfiestaReligiosa_in3 = $("#cfiestaReligiosa_in3").val();
  var cdatoHistorico_in3 = $("#cdatoHistorico_in3").val();
  var cfuente_in3 = $("#cfuente_in3").val();
  var cinscripcion_in= $("#cinscripcion_in").val();
  var cexistePatrimonio_in = $("#cexistePatrimonio_in").val();
  var cexisteResto_in = $("#cexisteResto_in").val();
  var cexisteElemento_in = $("#cexisteElemento_in").val();
  var critosMitos_in = $("#critosMitos_in").val();
  var cconjunto_in = document.getElementById('cconjunto_in').value;
  var cestadoVerifica_in = document.getElementById('cestadoVerifica_in').value;
  var cpuntuacion_in = $("#cpuntuacion_in").val();
  var g_tipo = 'PTR_CBI';

  var cdescripcion_in = caracteresEspecialesRegistrar(cdescripcion_in);
  var cpreexistencia_in = caracteresEspecialesRegistrar(cpreexistencia_in);
  var cpropietarios_in = caracteresEspecialesRegistrar(cpropietarios_in);
  var cusoOriginal_in = caracteresEspecialesRegistrar(cusoOriginal_in);
  var chechoHistorico_in = caracteresEspecialesRegistrar(chechoHistorico_in);
  var cfiestaReligiosa_in = caracteresEspecialesRegistrar(cfiestaReligiosa_in);
  var cdatoHistorico_in = caracteresEspecialesRegistrar(cdatoHistorico_in);
  var cfuente_in = caracteresEspecialesRegistrar(cfuente_in);
  var cpreexistencia_in3 = caracteresEspecialesRegistrar(cpreexistencia_in3);
  var cpropietarios_in3 = caracteresEspecialesRegistrar(cpropietarios_in3);
  var cusoOriginal_in3 = caracteresEspecialesRegistrar(cusoOriginal_in3);
  var chechoHistorico_in3 = caracteresEspecialesRegistrar(chechoHistorico_in3);
  var cfiestaReligiosa_in3 = caracteresEspecialesRegistrar(cfiestaReligiosa_in3);
  var cdatoHistorico_in3 = caracteresEspecialesRegistrar(cdatoHistorico_in3);
  var cfuente_in3 = caracteresEspecialesRegistrar(cfuente_in3);
  var cinscripcion_in = caracteresEspecialesRegistrar(cinscripcion_in);
  var cexistePatrimonio_in = caracteresEspecialesRegistrar(cexistePatrimonio_in);
  var cexisteResto_in = caracteresEspecialesRegistrar(cexisteResto_in);
  var cexisteElemento_in = caracteresEspecialesRegistrar(cexisteElemento_in);
  var critosMitos_in = caracteresEspecialesRegistrar(critosMitos_in);

  var tradicional1 = document.getElementById("tradicional1").checked;
  var tradicional2 = document.getElementById("tradicional2").checked;
  var tradicional3 = document.getElementById("tradicional3").checked;
  var tradicional4 = document.getElementById("tradicional4").checked;
  var tradicional5 = document.getElementById("tradicional5").checked;
  var tradicional6 = document.getElementById("tradicional6").checked;
  var tradicional7 = document.getElementById("tradicional7").checked;
  var tradicional8 = document.getElementById("tradicional8").checked;
  var tradicional9 = document.getElementById("tradicional9").checked;
  var tradicional10 = document.getElementById("tradicional10").checked;
  var tradicional11= document.getElementById("tradicional11").checked;
  var tradicional12 = document.getElementById("tradicional12").checked;

  var actual1 = document.getElementById("actual1").checked;
  var actual2 = document.getElementById("actual2").checked;
  var actual3 = document.getElementById("actual3").checked;
  var actual4 = document.getElementById("actual4").checked;
  var actual5 = document.getElementById("actual5").checked;
  var actual6 = document.getElementById("actual6").checked;
  var actual7 = document.getElementById("actual7").checked;
  var actual8 = document.getElementById("actual8").checked;
  var actual9 = document.getElementById("actual9").checked;
  var actual10 = document.getElementById("actual10").checked;
  var actual11 = document.getElementById("actual11").checked;
  var actual12 = document.getElementById("actual12").checked;

  var tendencia1 = document.getElementById("tendencia1").checked;
  var tendencia2 = document.getElementById("tendencia2").checked;
  var tendencia3 = document.getElementById("tendencia3").checked;
  var tendencia4 = document.getElementById("tendencia4").checked;
  var tendencia5 = document.getElementById("tendencia5").checked;
  var tendencia6 = document.getElementById("tendencia6").checked;
  var tendencia7 = document.getElementById("tendencia7").checked;
  var tendencia8 = document.getElementById("tendencia8").checked;
  var tendencia9 = document.getElementById("tendencia9").checked;
  var tendencia10 = document.getElementById("tendencia10").checked;
  var tendencia11 = document.getElementById("tendencia11").checked;
  var tendencia12 = document.getElementById("tendencia12").checked;

  var ctradicional_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+tradicional1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+tradicional2+', "resvalor": "Culto"},{"resid": 280, "estado": '+tradicional3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+tradicional4+', "resvalor": "Industria"},{"resid": 282, "estado": '+tradicional5+', "resvalor": "Militar"},{"resid": 281, "estado": '+tradicional6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+tradicional7+', "resvalor": "Salud"},{"resid": 278, "estado": '+tradicional8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+tradicional9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+tradicional10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+tradicional11+', "resvalor": "Taller"},{"resid": 276, "estado": '+tradicional12+', "resvalor": "Vivienda"}]';

  var cactual_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+actual1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+actual2+', "resvalor": "Culto"},{"resid": 280, "estado": '+actual3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+actual4+', "resvalor": "Industria"},{"resid": 282, "estado": '+actual5+', "resvalor": "Militar"},{"resid": 281, "estado": '+actual6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+actual7+', "resvalor": "Salud"},{"resid": 278, "estado": '+actual8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+actual9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+actual10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+actual11+', "resvalor": "Taller"},{"resid": 276, "estado": '+actual12+', "resvalor": "Vivienda"}]';

  var ctendencia_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+tendencia1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+tendencia2+', "resvalor": "Culto"},{"resid": 280, "estado": '+tendencia3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+tendencia4+', "resvalor": "Industria"},{"resid": 282, "estado": '+tendencia5+', "resvalor": "Militar"},{"resid": 281, "estado": '+tendencia6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+tendencia7+', "resvalor": "Salud"},{"resid": 278, "estado": '+tendencia8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+tendencia9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+tendencia10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+tendencia11+', "resvalor": "Taller"},{"resid": 276, "estado": '+tendencia12+', "resvalor": "Vivienda"}]'; 

////////////////FORMULARIO BLOQUE////////////////////////////////
   var PTR_ID_BLOQ1 = '1';
   var PTR_DES_BLOQ1 = $("#cdescripcion").val();
   //var PTR_COD_CAT = '12DM';
   var PTR_RANG_EPO1 = document.getElementById('crango_epoca').value;
   var PTR_EST_BLOQ1 = document.getElementById('cestilo_bloque').value;
   var cepocaestilo = document.getElementById("cepocaestilocheck").checked;
   var csistemaconstructivo = document.getElementById("csistemaconstructivo").checked;
   var PTR_SIS_CONSTRUC1 = document.getElementById('csistema_constructivo_insertar').value; 
   var PTR_NRO_VIV1 = $("#cnro_viviendas").val(); 
   var PTR_NRO_HOG1 = $("#cnro_hogares").val(); 
   
   var cacapites1 = document.getElementById("cacapites1").checked;
   var cacapites2 = document.getElementById("cacapites2").checked;
   var cacapites3 = document.getElementById("cacapites3").checked;
   var cacapites4 = document.getElementById("cacapites4").checked;
   var cacapites5 = document.getElementById("cacapites5").checked;
   var cacapites6 = document.getElementById("cacapites6").checked;
   //-----------4 check Indicador Histórico (10% / 20%)------------------------------
   var cindicador_historico1 = document.getElementById("cindicador_historico1").checked;
   var cindicador_historico2 = document.getElementById("cindicador_historico2").checked;
   var cindicador_historico3 = document.getElementById("cindicador_historico3").checked;
   var cindicador_historico4 = document.getElementById("cindicador_historico4").checked;
   var cindicador_historico5 = document.getElementById("cindicador_historico5").checked;
   //----------- 5 check Indicador Artístico (6% / 15%)------------------------------
   var cindicador_artistico1 = document.getElementById("cindicador_artistico1").checked;
   var cindicador_artistico2 = document.getElementById("cindicador_artistico2").checked;
   var cindicador_artistico3 = document.getElementById("cindicador_artistico3").checked;
   //----------- 6 check Indicador Arquitectónico(4% / 15%)------------------------------
   var cindicador_arquitectonico1 = document.getElementById("cindicador_arquitectonico1").checked;
   var cindicador_arquitectonico2 = document.getElementById("cindicador_arquitectonico2").checked;
   //----------- 7 check Indicador Tecnológico (6% / 13%)------------------------------
   var cindicador_teclogico1 = document.getElementById("cindicador_teclogico1").checked;
   var cindicador_teclogico2 = document.getElementById("cindicador_teclogico2").checked;
   var cindicador_teclogico3 = document.getElementById("cindicador_teclogico3").checked;
   //----------- 8 check Indicador Integridad (4% / 15%)------------------------------
   var cindicador_integridad1 = document.getElementById("cindicador_integridad1").checked;
   var cindicador_integridad2 = document.getElementById("cindicador_integridad2").checked;
   var cindicador_integridad3 = document.getElementById("cindicador_integridad3").checked;
  //----------- 9 check Indicador Urbano  (9% / 15%)------------------------------
  var cindicador_urbano1 = document.getElementById("cindicador_urbano1").checked;
  var cindicador_urbano2 = document.getElementById("cindicador_urbano2").checked;
  var cindicador_urbano3 = document.getElementById("cindicador_urbano3").checked;
   //----------- 10 check Indicador Inmaterial------------------------------
   var cindicador_inmaterial1 = document.getElementById("cindicador_inmaterial1").checked;
   var cindicador_inmaterial2 = document.getElementById("cindicador_inmaterial2").checked;

   var PTR_EPO_EST1 = '[{"tipo": "CHK"},{"valor": '+cepocaestilo+'}]';
   ////console.log('EPOCA Y ESTILO',PTR_EPO_EST1);

   var PTR_SIS_CONST1 = '[{"tipo": "CHK"},{"valor": '+csistemaconstructivo+'}]';
   ////console.log('SISTEMA CONSTRUCTIVO',PTR_SIS_CONST1);

   var PTR_ACAP1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cacapites1+', "resvalor": "Elementos Estructurantes (8%)"},{"resid": 285, "estado": '+cacapites2+', "resvalor": "Elementos Compositivos (6%)"},{"resid": 280, "estado": '+cacapites3+', "resvalor": "Acabados (3%)"},{"resid": 283, "estado": '+cacapites4+', "resvalor": "Elementos Artisiticos Compositivos (3%)"},{"resid": 282, "estado": '+cacapites5+', "resvalor": "Elementos Ornamentales (4%)"},{"resid": 281, "estado": '+cacapites6+', "resvalor": "Elementos Tipologicos Compositivos (9%)"}]';
   ////console.log('ACAPITES',PTR_ACAP1);

   var PTR_VAL_HIST1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_historico1+', "resvalor": "Inmueble que posee un valor testimonial y documental, que ilustra el desarrollo político, social, religioso, cultural, económico y la forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 285, "estado": '+cindicador_historico2+', "resvalor": "Inmueble que desde su creación se constituye en un hito en la memoria historica"},{"resid": 280, "estado": '+cindicador_historico3+', "resvalor": "Inmueble que en el transcurso del tiempo se constituyo en hito en la memoria historica, al ser escenario relacionado con personas o eventos importantes, dentro del proceso Histórico,social, cultural, económico de las comunidades"},{"resid": 283, "estado": '+cindicador_historico4+', "resvalor": "Inmueble que forma parte de un conjunto Histórico"},{"resid": 282, "estado": '+cindicador_historico5+', "resvalor": "Inmueble que posee elementos arqueológicos o testimonios arquitectonicos de construcciones preexistentes que documentan su evolución y desarrollo"}]';
   ////console.log('INDICADOR HISTÓRICO',PTR_VAL_HIST1);

   var PTR_VAL_ART1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_artistico1+', "resvalor": "Inmueble que se constituye en un ejemplo sobresaliente por su singularidad arquitectónica/artística"},{"resid": 285, "estado": '+cindicador_artistico2+', "resvalor": "Inmueble que conserva elementos arquitectónicos y tradicionales de interés"},{"resid": 280, "estado": '+cindicador_artistico3+', "resvalor": "Inmueble poseedor de expresiones artísticas y decorativas de interés"}]';
   ////console.log('INDICADOR ARTÍSTICO',PTR_VAL_ART1);


   var PTR_VAL_ARQ1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_arquitectonico1+', "resvalor": "Inmueble poseedor de características Tipológicas representativas de estilos arquitectónicos"},{"resid": 285, "estado": '+cindicador_arquitectonico2+', "resvalor": "Inmueble con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalles"}]';
   ////console.log('INDICADOR ARQUITECTÓNICO',PTR_VAL_ARQ1);


   var PTR_VAL_TEC1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_teclogico1+', "resvalor": "Inmueble que se constituye en un exponente de las técnicas constructivas y uso de materiales característicos de una época o región determinada"},{"resid": 285, "estado": '+cindicador_teclogico2+', "resvalor": "Inmueble donde la aplicación de técnicas constructivas son singulares o de especial interés"},{"resid": 280, "estado": '+cindicador_teclogico3+', "resvalor":"Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
   ////console.log('INDICADOR TECNOLÓGICO',PTR_VAL_TEC1);

   var PTR_VAL_INTE1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_integridad1+', "resvalor": "Inmueble que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 285, "estado": '+cindicador_integridad2+', "resvalor": "Inmueble o espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"},{"resid": 280, "estado": '+cindicador_integridad3+', "resvalor": "Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
   ////console.log('INDICADOR ITEGRIDAD',PTR_VAL_INTE1);

   var PTR_VAL_URB1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_urbano1+', "resvalor": "Inmueble que contribuye a definir un entorno de valor por su configuración y calidad en su estructura urbanística, el paisaje y/o el espacio público"},{"resid": 285, "estado": '+cindicador_urbano2+', "resvalor": "Inmueble que forma un perfil homogéneo y/ó armónico"},{"resid": 280, "estado": '+cindicador_urbano3+', "resvalor": "Inmueble considerado hito de referencia por su emplazamiento"}]';
   ////console.log('INDICADOR URBANO',PTR_VAL_URB1);

   var PTR_VALORES_1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+cindicador_inmaterial1+', "resvalor": "INDICADOR INMATERIAL (2% / 7%) Inmueble relacionado con la organización social, forma de vida, usos, representaciones, expresiones, conocimientos y técnicas que las comunidades y grupos sociales reconocen como parte de patrimonio"},{"resid": 285, "estado": '+cindicador_inmaterial2+', "resvalor": "INDICADOR SIMBÓLICO (1% / 5%) Inmueble o espacio abierto único que expresa una significación cultural, representa identidad y pertenencia para el colectivo"}]';
   ////console.log('INDICADOR INMATERIAL',PTR_VALORES_1);

  var cpuntuacion_ubiman = document.getElementById("cpuntuacion_umcheck").checked;
  var cpuntuacion_lineac = document.getElementById("cpuntuacion_lccheck").checked;
  var cpuntuacion_trazoma = document.getElementById("cpuntuacion_tmcheck").checked;
  var cpuntuacion_tipologiaa = document.getElementById("cpuntuacion_tacheck").checked;
  var cpuntucaion_areaedificablec = document.getElementById('cpuntuacion_aeccheck').checked;
  var cpuntuacion_fechaconst = document.getElementById('cpuntuacion_fccheck').checked;
  var cpuntuacion_autorconstr = document.getElementById('cpuntuacion_accheck').checked;
  var cpuntuacion_fechac = document.getElementById('cpuntuacion_fcdccheck').checked;
  var cpun_autorconstructor = document.getElementById('cpuntuacion_acdccheck').checked;
  var cpunt_existenciapm = document.getElementById('cpuntuacion_epmcheck').checked;
  var cpun_existenciara = document.getElementById('cpuntuacion_eracheck').checked;
  var cpun_existenciaen = document.getElementById('cpuntuacion_eencheck').checked;
  var cpun_ritosmitos = document.getElementById('cpuntuacion_rmtocheck').checked;

  var PTR_UBIC_MANC = '[{"tipo": "CHK"},{"valor": "'+cpuntuacion_ubiman+'"}]';
  var PTR_LIN_CONSC = '[{"tipo": "CHK"},{"valor": "'+cpuntuacion_lineac+'"}]';
  var PTR_TRAZ_MANC = '[{"tipo": "CHK"},{"valor": "'+cpuntuacion_trazoma+'"}]';
  var PTR_TIP_ARQC =  '[{"tipo": "CHK"},{"valor": "'+cpuntuacion_tipologiaa+'"}]';
  var PTR_AER_EDIFC = '[{"tipo": "CHK"},{"valor": "'+cpuntucaion_areaedificablec+'"}]';
  var PTR_FECH_CONSC = '[{"tipo": "CHK"},{"valor": "'+cpuntuacion_fechaconst+'"}]';
  var PTR_AUT_CONS_C = '[{"tipo": "CHK"},{"valor": "'+cpuntuacion_autorconstr+'"}]';
  var PTR_FEC_CONST_C = '[{"tipo": "CHK"},{"valor": "'+cpuntuacion_fechac+'"}]';
  var PTR_AUT_CONSC = '[{"tipo": "CHK"},{"valor": "'+cpun_autorconstructor+'"}]';
  var PTR_EXIST_PAT = '[{"tipo": "CHK"},{"valor": "'+cpunt_existenciapm+'"}]';
  var PTR_EXIST_ARQ = '[{"tipo": "CHK"},{"valor": "'+cpun_existenciara+'"}]';
  var PTR_EXIST_ELEMNAT = '[{"tipo": "CHK"},{"valor": "'+cpun_existenciaen+'"}]';
  var PTR_EXIST_RITOS = '[{"tipo": "CHK"},{"valor": "'+cpun_ritosmitos+'"}]';

  //----------------------------------------------------------------------
  var cindicador_historico3 = $("#cindicador_historico").val();
  var PTR_VAL_HIS_DESC1 = caracteresEspecialesRegistrar(cindicador_historico3);
  var cindicador_atistico3 = $("#cindicador_atistico").val();
  var PTR_VAL_ART_DESC1 = caracteresEspecialesRegistrar(cindicador_atistico3);
  var cindicador_arquitectonico3 = $("#cindicador_arquitectonico").val();
  var PTR_VAL_ARQ_DESC1 = caracteresEspecialesRegistrar(cindicador_arquitectonico3);
  var cindicador_teclogico3 = $("#cindicador_teclogico").val();
  var PTR_VAL_TEC_DESC1 = caracteresEspecialesRegistrar(cindicador_teclogico3);
  var cindicador_integrid3 = $("#cindicador_integrid").val();
  var PTR_VAL_INTE_DESC1 = caracteresEspecialesRegistrar(cindicador_integrid3);
  var cindicador_urbano3 = $("#cindicador_urbano").val();
  var PTR_VAL_URB_DESC1 = caracteresEspecialesRegistrar(cindicador_urbano3);
  var cindicador_inmaterial3 = $("#cindicador_inmaterial").val();
  var PTR_VAL_INMAT_DESC1 = caracteresEspecialesRegistrar(cindicador_inmaterial3);
  var cindicador_simbolico3 = $("#cindicador_simbolico").val();
  var PTR_VAL_SIMB_DESC1 = caracteresEspecialesRegistrar(cindicador_simbolico3);
  var cindicador_interior3 = $("#cindicador_interior").val();
  var PTR_VAL_INT_DESC1 = caracteresEspecialesRegistrar(cindicador_interior3);
  var cdescripcion_exterior3 = $("#cdescripcion_exterior").val();
  var PTR_VAL_EXT_DESC1 = caracteresEspecialesRegistrar(cdescripcion_exterior3);
  var PTR_CATEGORIA1 = $("#ccategoria_inmueble").val();
  var PTR_PUNTO = $("#cpuntuacion").val();
  var PTR_ID_BLOQ1 = $("#cid_bloque").val();

  var cas_data_in ='{"g_tipo":"'+g_tipo+'","PTR_CREA":"'+ccreador_in+'","PTR_FEC_CREA":"'+cfecha_creador_in+'","PTR_DEP":"'+cdepartamento_in+'","PTR_CIU":"'+cciudad_in+'","PTR_MUNI":"'+cmunicipio_in+'","PTR_COD_CAT":"'+ccodCatastral_in+'","PTR_COD_CAT_HIS1":"'+ccodCatastral1_in+'","PTR_OR":"'+coriginal_in+'","PTR_TRAD":"'+ctradicional_in+'","PTR_ACT":"'+cActual_in+'","PTR_MACRO":"'+cMacrodistrito_in+'","PTR_TIP_VIAS":"'+ctipoViaLocalizacion_in+'","PTR_CALL_ESQ":"'+ccalleEsquina_in+'","PTR_DIRE":"'+cdireccion_in+'","PTR_ZONA":"'+czona_in+'","PTR_NUM1":"'+cnumero_in+'","PTR_NUM2":"'+cnumero_in2+'","PTR_NUM3":"'+cnumero_in3+'","PTR_NUM4":"'+cnumero_in4+'","PTR_NUM5":"'+cnumero_in5+'","PTR_ARE_PRED":"'+careaPredio_in+'","PTR_ANCH_FREN":"'+canchoFrente_in+'","PTR_FOND_PRED":"'+cfondoPredio_in+'","PTR_ANCH_MUR":"'+canchoMuro_in+'","PTR_ALT_PROM":"'+calturaInterior_in+'","PTR_ALT_FACH":"'+calturaFachada_in+'","PTR_ALT_EDIF":"'+calturaMaxima_in+'","PTR_USO_TRAD":'+ctradicional_check+',"PTR_USO_ACT":'+cactual_check+',"PTR_USO_TEN":'+ctendencia_check+',"PTR_DES_PRED":"'+cdescripcion_in+'","PTR_UBIC_MAN":"'+cubicacionManzana_in+'","PTR_LIN_CONS":"'+clineaConstruccion_in+'","PTR_TRAZ_MAN":"'+ctrazoManzana_in+'","PTR_TIP_ARQ":"'+ctipologiaArqui_in+'","PTR_AER_EDIF":"'+careaEdificable_in+'","PTR_MAT_VIA":"'+cmaterialVia_in+'","PTR_TIP_VIA":"'+ctipoVia_in+'","PTR_BLOQ_INV":"'+cbloqueInventario_in+'","PTR_BLOQ_REAL":"'+cbloquesReales_in+'","PTR_HOG_INV":"'+chogaresInventario_in+'","PTR_HOG_REAL":"'+chogaresReales_in+'","PTR_EST_GEN":"'+cestiloGeneral_in+'","PTR_RAN_EPO":"'+crangoEpoca_in+'","PTR_EST_CON":"'+cestadoConserva_in+'","PTR_FIG_PROT":'+cgrillaFiguraProtec+',"PTR_FECH_CONS":"'+cfechaConstruccion_in+'","PTR_AUT_CON":"'+cautor_in+'","PTR_PREEX_EDIF":"'+cpreexistencia_in+'","PTR_PROP_ANT":"'+cpropietarios_in+'","PTR_USO_ORG":"'+cusoOriginal_in+'","PTR_HEC_HIST":"'+chechoHistorico_in+'","PTR_FIE_REL":"'+cfiestaReligiosa_in+'","PTR_DAT_HIST_CONJ":"'+cdatoHistorico_in+'","PTR_FUENT_DOC":"'+cfuente_in+'","PTR_FEC_CONST_DOC":"'+cfechaConstruccion_in3+'","PTR_AUT_CONS":"'+cautor_in3+'","PTR_PREEX_INTER":"'+cpreexistencia_in3+'","PTR_PROP_ANT_DOC":"'+cpropietarios_in3+'","PTR_USOS_ORG_DOC":"'+cusoOriginal_in3+'","PTR_HEC_HIS_DOC":"'+chechoHistorico_in3+'","PTR_FIE_REL_DOC":"'+cfiestaReligiosa_in3+'","PTR_DAT_HIS_DOC":"'+cdatoHistorico_in3+'","PTR_FUENTE_DOC":"'+cfuente_in3+'","PTR_INSCRIP":"'+cinscripcion_in+'","PTR_EXIST":"'+cexistePatrimonio_in+'","PTR_EXIST_AR":"'+cexisteResto_in+'","PTR_EXIST_ELEM":"'+cexisteElemento_in+'","PTR_EXIST_RITO":"'+critosMitos_in+'","PTR_CONJUNTO":"'+cconjunto_in+'","PTR_EST_VERF":"'+cestadoVerifica_in+'","PTR_VAL_CAT":"'+cpuntuacion_in+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'","PTR_DES_BLOQ1":"'+PTR_DES_BLOQ1+'","PTR_RANG_EPO1":"'+PTR_RANG_EPO1+'","PTR_EST_BLOQ1":"'+PTR_EST_BLOQ1+'","PTR_EPO_EST1":'+PTR_EPO_EST1+',"PTR_SIS_CONST1":'+PTR_SIS_CONST1+',"PTR_SIS_CONSTRUC1":"'+PTR_SIS_CONSTRUC1+'","PTR_NRO_VIV1":"'+PTR_NRO_VIV1+'","PTR_NRO_HOG1":"'+PTR_NRO_HOG1+'","PTR_ACAP1":'+PTR_ACAP1+',"PTR_VAL_HIST1":'+PTR_VAL_HIST1+',"PTR_VAL_ART1":'+PTR_VAL_ART1+',"PTR_VAL_ARQ1":'+PTR_VAL_ARQ1+',"PTR_VAL_TEC1":'+PTR_VAL_TEC1+',"PTR_VAL_INTE1":'+PTR_VAL_INTE1+',"PTR_VAL_URB1":'+PTR_VAL_URB1+',"PTR_VALORES_1":'+PTR_VALORES_1+',"PTR_VAL_HIS_DESC1":"'+PTR_VAL_HIS_DESC1+'","PTR_VAL_ART_DESC1":"'+PTR_VAL_ART_DESC1+'","PTR_VAL_ARQ_DESC1":"'+PTR_VAL_ARQ_DESC1+'","PTR_VAL_TEC_DESC1":"'+PTR_VAL_TEC_DESC1+'","PTR_VAL_INTE_DESC1":"'+PTR_VAL_INTE_DESC1+'","PTR_VAL_URB_DESC1":"'+PTR_VAL_URB_DESC1+'","PTR_VAL_INMAT_DESC1":"'+PTR_VAL_INMAT_DESC1+'","PTR_VAL_SIMB_DESC1":"'+PTR_VAL_SIMB_DESC1+'","PTR_VAL_INT_DESC1":"'+PTR_VAL_INT_DESC1+'","PTR_VAL_EXT_DESC1":"'+PTR_VAL_EXT_DESC1+'","PTR_REG_PROPI":'+cgrillaRegimenPropietario+',"PTR_REF_HIST":'+cgrillaRefHist+',"PTR_PINT_MUR_EXT":'+cgrillaMuroExt+',"PTR_PINT_MUR_INT":'+cgrillaMuroInt+',"PTR_ACA_MUR_EXT":'+cgrillaAcabadoEx+',"PTR_ACA_MUR_INT":'+cgrillaAcabadoInt+',"PTR_PEL_POT1":'+peligro_potencial_grilla+',"PTR_PAT_EDIF1":'+patologia_edif_grilla+',"PTR_INST_EST1":'+instalaciones_estado_grilla+',"PTR_SERVICIOS1":'+servios_grilla+', "PTR_ELEM_ART_COMP":'+cgrillaFiguraProtecAR+', "PTR_ELEM_ORN_DEC":'+cgrillaFiguraProtecOR+', "PTR_ELEM_TIP_COMP":'+cgrillaFiguraProtecTIC+', "PTR_AG_HOGARES":'+cgrillaFiguraProtecHogar+',"PTR_CATEGORIA1":"'+PTR_CATEGORIA1+'","PTR_PUNTO":"'+PTR_PUNTO+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'","PTR_UBIC_MANC":'+PTR_UBIC_MANC+',"PTR_LIN_CONSC":'+PTR_LIN_CONSC+',"PTR_TRAZ_MANC":'+PTR_TRAZ_MANC+',"PTR_TIP_ARQC":'+PTR_TIP_ARQC+',"PTR_AER_EDIFC":'+PTR_AER_EDIFC+',"PTR_FECH_CONSC":'+PTR_FECH_CONSC+',"PTR_AUT_CONS_C":'+PTR_AUT_CONS_C+',"PTR_FEC_CONST_C":'+PTR_FEC_CONST_C+',"PTR_AUT_CONSC":'+PTR_AUT_CONSC+',"PTR_EXIST_PAT":'+PTR_EXIST_PAT+',"PTR_EXIST_ARQ":'+PTR_EXIST_ARQ+',"PTR_EXIST_ELEMNAT":'+PTR_EXIST_ELEMNAT+',"PTR_EXIST_RITOS":'+PTR_EXIST_RITOS+',"PTR_PAR_EXT":'+cgrillaParedExterna+',"PTR_COB_TEC":'+cgrillaCoberturaTecho+',"PTR_PAR_INT":'+cgrillaParedInterior+',"PTR_ENT_PIS":'+cgrillaEntrePisos+',"PTR_ESC":'+cgrillaEscalera+',"PTR_CIE":'+cgrillaCielo+',"PTR_BAL":'+cgrillaBalcon+',"PTR_EST_CUB":'+cgrillaEstructuraCubierta+',"PTR_PISO":'+cgrillaPiso+',"PTR_VAN_VENT":'+van_venta_int+',"PTR_VENT_INT":'+ventana_interior+',"PTR_VAN_VENT_EXT":'+van_vent_exterior+',"PTR_VENT_EXT":'+ventana_exterior+',"PTR_VAN_PUE_INT":'+van_puert_int+',"PTR_VAN_PUE_EXT":'+van_puert_ext+',"PTR_PUE_INT":'+puerta_ineterior+',"PTR_PUE_EXT":'+puerta_externa+',"PTR_REJA":'+rejas_grila+'}';


  
  var jdata_m = JSON.stringify(cas_data_in);
  
  var xcas_nro_caso =correlativog;
  
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = 'asunto';
  var xcas_tipo_hr = 'hora';
  var xcas_id_padre = 1;
  var xcampo_f = 'campo'; 



  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_m+',"xcas_nombre_caso":"'+ccodPatrimonio_in+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};

    

  $.ajax({
    type        : 'GET',
    url         :  '/prueba/VALLE/public/v0.0/getToken',
    data        : '',
    success: function(token) {

    //var ccodPatrimonio = $("#ccodPatrimonio").val();
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },                  
     success: function(data){ 
    
       buscarPatrimonioPorTipo(1);
      swal( "Exíto..!", "Se registro correctamente los datos de Bienes Inmuebles...!", "success" );
       $( "#cpuntuacion_in" ).val(0); 
      limpiar();
      //$( "#formBienesArqM_create").data('bootstrapValidator').resetForm();
      //$( "#myCreateBienesArqMueble" ).modal('toggle');
      var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', xcas_nro_caso);
        ////console.log("url",url);
        location.href = url;
      }, 1000); 
      
    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500); 
});

function limpiar(){

  document.getElementById("cpuntuacion_lccheck").checked = false;
  document.getElementById("cpuntuacion_tmcheck").checked = false;
  document.getElementById("cpuntuacion_tacheck").checked = false;
  document.getElementById('cpuntuacion_aeccheck').checked = false;
  document.getElementById('cpuntuacion_fccheck').checked = false;
  document.getElementById('cpuntuacion_accheck').checked = false;
  document.getElementById('cpuntuacion_fcdccheck').checked = false;
  document.getElementById('cpuntuacion_acdccheck').checked = false;
  document.getElementById('cpuntuacion_epmcheck').checked = false;
  document.getElementById('cpuntuacion_eracheck').checked = false;
  document.getElementById('cpuntuacion_eencheck').checked = false;
  document.getElementById('cpuntuacion_rmtocheck').check = false;
}



$( "#actualizarBienesInmuebles" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

  var acreador_in = $("#acreador_in").val();
  var afecha_creador_in = $("#afecha_creador_in").val();
  var amodificador_in = $("#amodificador_in").val();
  var afecha_modificador_in = $("#afecha_modificador_in").val();
  var adepartamento_in = $("#adepartamento_in").val();
  var aciudad_in = $("#aciudad_in").val();
  var amunicipio_in = $("#amunicipio_in").val();
  var acasoId_in = $( "#acasoId_in" ).val();
  var anroficha_in = $( "#anroficha_in" ).val();  
  var acodPatrimonio_in = 'PTR_CBI'+(anroficha_in)+'/2018';
  var acodCatastral_in = $("#acodCatastral_in").val();
  var acodCatastral1_in = $("#acodCatastral1_in").val();
  var aoriginal_in = $("#aoriginal_in").val();
  var atradicional_in1 = $("#atradicional_in1").val();
  var aActual_in = $("#aActual_in").val();
  var aMacrodistrito_in = document.getElementById('aMacrodistrito_in').value;
  var atipoViaLocalizacion_in = document.getElementById('atipoViaLocalizacion_in').value;
  var acalleEsquina_in = $("#acalleEsquina_in").val();
  var adireccion_in = $("#adireccion_in").val();
  var azona_in = $("#azona_in").val();
  var anumero_in = $("#anumero_in").val();
  var anumero_in2 = $("#anumero_in2").val();
  var anumero_in3 = $("#anumero_in3").val();
  var anumero_in4 = $("#anumero_in4").val();
  var anumero_in5 = $("#anumero_in5").val();
  var aareaPredio_in = $("#aareaPredio_in").val();
  var aanchoFrente_in = $("#aanchoFrente_in").val();
  var afondoPredio_in = $("#afondoPredio_in").val();
  var aanchoMuro_in = $("#aanchoMuro_in").val();
  var aalturaInterior_in = $("#aalturaInterior_in").val();
  var aalturaFachada_in = $("#aalturaFachada_in").val();
  var aalturaMaxima_in = $("#aalturaMaxima_in").val();
  var adescripcion_in = $("#adescripcion_in").val();
  var aubicacionManzana_in = document.getElementById('aubicacionManzana_in').value;
  var alineaConstruccion_in = document.getElementById('alineaConstruccion_in').value;
  var atrazoManzana_in = document.getElementById('atrazoManzana_in').value;
  var atipologiaArqui_in = document.getElementById('atipologiaArqui_in').value;
  var aareaEdificable_in = document.getElementById('aareaEdificable_in').value;
  var amaterialVia_in = document.getElementById('amaterialVia_in').value;
  var atipoVia_in = document.getElementById('atipoVia_in').value;
  //var abloqueInventario_in = $("#abloqueInventario_in").val();
  var abloqueInventario_in = document.getElementById('abloqueInventario_in').value;
  var abloquesReales_in = $("#abloquesReales_in").val();
  var ahogaresInventario_in = $("#ahogaresInventario_in").val();
  var ahogaresReales_in = $("#ahogaresReales_in").val();
  var aestiloGeneral_in = document.getElementById('aestiloGeneral_in').value;
  var arangoEpoca_in = document.getElementById('arangoEpoca_in').value;
  var aestadoConserva_in = document.getElementById('aestadoConserva_in').value;
  var afechaConstruccion_in = $("#afechaConstruccion_in").val();
  var aautor_in = $("#aautor_in").val();
  var apreexistencia_in = $("#apreexistencia_in").val();
  var apropietarios_in = $("#apropietarios_in").val();
  var ausoOriginal_in = $("#ausoOriginal_in").val();
  var ahechoHistorico_in = $("#ahechoHistorico_in").val();
  var afiestaReligiosa_in = $("#afiestaReligiosa_in").val();
  var adatoHistorico_in = $("#adatoHistorico_in").val();
  var afuente_in = $("#afuente_in").val();
  var afechaConstruccion_in3 = $("#afechaConstruccion_in3").val();
  var aautor_in3 = $("#aautor_in3").val();
  var apreexistencia_in3 = $("#apreexistencia_in3").val();
  var apropietarios_in3 = $("#apropietarios_in3").val();
  var ausoOriginal_in3 = $("#ausoOriginal_in3").val();
  var ahechoHistorico_in3 = $("#ahechoHistorico_in3").val();
  var afiestaReligiosa_in3 = $("#afiestaReligiosa_in3").val();
  var adatoHistorico_in3 = $("#adatoHistorico_in3").val();
  var afuente_in3 = $("#afuente_in3").val();
  var ainscripcion_in= $("#ainscripcion_in").val();
  var aexistePatrimonio_in = $("#aexistePatrimonio_in").val();
  var aexisteResto_in = $("#aexisteResto_in").val();
  var aexisteElemento_in = $("#aexisteElemento_in").val();
  var aritosMitos_in = $("#aritosMitos_in").val();
  var aconjunto_in = document.getElementById('aconjunto_in').value;
  var aestadoVerifica_in = document.getElementById('aestadoVerifica_in').value;
  var apuntuacion_in = $("#apuntuacion_in").val();
  var g_tipo = 'PTR_CBI';


  //-----------------Regimen Propiedad --------------------------//
    var cont=0;
      arrDibujoRP.forEach(function (item, index, array) 
      {
        arrCodigoRP[cont].arg_propietario = $("#nombreprpRP"+item.id+"").val();
        arrCodigoRP[cont].arg_tipoP = $("#tpropiedaRP"+item.id+"").val();
        cont++;
      });
    console.log("arrCodigoRP",arrCodigoRP);

    var abloqueRegimenPropiedad = '[{"tipo":"GRD","campos":"f01_nombre_PROP_|f01_reja_INT","titulos":"Nombre de Propietario|Tipo de Propiedad"}';
    for (var i = 0; i<arrDibujoRP.length;i++)
    {
        ////////console.log("grillapruebaI"+i+"",arrCodigo[i]);
        var grilla = '{"f01_nombre_PROP_":"'+arrCodigoRP[i].arg_propietario+'","f01_reja_INT_":"'+arrCodigoRP[i].arg_tipoP+'","f01_reja_INT_valor":"'+arrCodigoRP[i].arg_tipoP+'"}';
        abloqueRegimenPropiedad = (abloqueRegimenPropiedad.concat(',').concat(grilla));

    }
      var agrillaRegimenPropietario = abloqueRegimenPropiedad.concat(']');

      console.log("agrillaRegimenPropietario",agrillaRegimenPropietario);
      

  //-----------------Figura de Proteccion --------------------------//
    var cont = 0;
      arrDibujoA.forEach(function (item, index, array) 
      {
        arrCodigoA[cont].argtipo = $("#tipoargA"+item.id+"").val();
        arrCodigoA[cont].argvalor = $("#valorA"+item.id+"").val();
       
        cont++;
      });
    console.log("arrCodigoActualizar",arrCodigoA);

    var abloqueFiguraProtec = '[{"tipo":"GRD","campos":"f01_FIG_LEG_|f01_FIG_OBS","titulos":"Figura de Proteccion Legal|Observaciones"}';
          for (var i = 0; i<arrCodigoA.length;i++)
          {
            
             var aobsFiguraProtec = caracteresEspecialesRegistrar(arrCodigoA[i].argvalor);
             var agrillaFigura = '{"f01_FIG_OBS":"'+aobsFiguraProtec+'","f01_FIG_LEG_":"'+arrCodigoA[i].argtipo+'","f01_FIG_LEG__valor":"'+arrCodigoA[i].argtipo+'"}';
            abloqueFiguraProtec = (abloqueFiguraProtec.concat(',').concat(agrillaFigura));

          }
    var agrillaFiguraProtec = abloqueFiguraProtec.concat(']');
    console.log("agrillaFiguraProtec",agrillaFiguraProtec);
    //-----------------Referencias Historicas--------------------------//
   
    var cont=0;
      arrDibujoRH.forEach(function (item, index, array) 
      {
        arrCodigoRH[cont].arg_tipoRef = $("#treferenciaRH"+item.id+"").val();
        arrCodigoRH[cont].arg_observacion = $("#observacionRH"+item.id+"").val();
        cont++;
      });
    //console.log("arrCodigoRH",arrCodigoRH);

    var abloqueRefereciaHisto = '[{"tipo":"GRD","campos":"f01_ref_hist_|f01_Obs_REF_","titulos":"Referencias Históricas|Observación"}';
    for (var i = 0; i<arrDibujoRH.length;i++)
    {
      var aobsReferencias = caracteresEspecialesRegistrar(arrCodigoRH[i].arg_observacion);
      var grillaReferenciaHisto = '{"f01_ref_hist_":"'+arrCodigoRH[i].arg_tipoRef+'","f01_ref_hist__valor":"'+arrCodigoRH[i].arg_tipoRef+'","f01_FIG_OBS":"'+aobsReferencias+'"}';
      abloqueRefereciaHisto = (abloqueRefereciaHisto.concat(',').concat(grillaReferenciaHisto));

    }
    var agrillaRefHist = abloqueRefereciaHisto.concat(']');
    //console.log("agrillaRefHist",agrillaRefHist);
    
   var adescripcion_in = caracteresEspecialesRegistrar(adescripcion_in);
   var apreexistencia_in = caracteresEspecialesRegistrar(apreexistencia_in);
   var apropietarios_in = caracteresEspecialesRegistrar(apropietarios_in);
   var ausoOriginal_in = caracteresEspecialesRegistrar(ausoOriginal_in);
   var ahechoHistorico_in = caracteresEspecialesRegistrar(ahechoHistorico_in);
   var afiestaReligiosa_in = caracteresEspecialesRegistrar(afiestaReligiosa_in);
   var adatoHistorico_in = caracteresEspecialesRegistrar(adatoHistorico_in);
   var afuente_in = caracteresEspecialesRegistrar(afuente_in);
   var apreexistencia_in3 = caracteresEspecialesRegistrar(apreexistencia_in3);
   var apropietarios_in3 = caracteresEspecialesRegistrar(apropietarios_in3);
   var ausoOriginal_in3 = caracteresEspecialesRegistrar(ausoOriginal_in3);
   var ahechoHistorico_in3 = caracteresEspecialesRegistrar(ahechoHistorico_in3);
   var afiestaReligiosa_in3 = caracteresEspecialesRegistrar(afiestaReligiosa_in3);
   var adatoHistorico_in3 = caracteresEspecialesRegistrar(adatoHistorico_in3);
   var afuente_in3 = caracteresEspecialesRegistrar(afuente_in3);
   var ainscripcion_in = caracteresEspecialesRegistrar(ainscripcion_in);
   var aexistePatrimonio_in = caracteresEspecialesRegistrar(aexistePatrimonio_in);
   var aexisteResto_in = caracteresEspecialesRegistrar(aexisteResto_in);
   var aexisteElemento_in = caracteresEspecialesRegistrar(aexisteElemento_in);
   var aritosMitos_in = caracteresEspecialesRegistrar(aritosMitos_in);

   var atradicional1 = document.getElementById("atradicional1").checked;
   var atradicional2 = document.getElementById("atradicional2").checked;
   var atradicional3 = document.getElementById("atradicional3").checked;
   var atradicional4 = document.getElementById("atradicional4").checked;
   var atradicional5 = document.getElementById("atradicional5").checked;
   var atradicional6 = document.getElementById("atradicional6").checked;
   var atradicional7 = document.getElementById("atradicional7").checked;
   var atradicional8 = document.getElementById("atradicional8").checked;
   var atradicional9 = document.getElementById("atradicional9").checked;
   var atradicional10 = document.getElementById("atradicional10").checked;
   var atradicional11= document.getElementById("atradicional11").checked;
   var atradicional12 = document.getElementById("atradicional12").checked;

   var aactual1 = document.getElementById("aactual1").checked;
   var aactual2 = document.getElementById("aactual2").checked;
   var aactual3 = document.getElementById("aactual3").checked;
   var aactual4 = document.getElementById("aactual4").checked;
   var aactual5 = document.getElementById("aactual5").checked;
   var aactual6 = document.getElementById("aactual6").checked;
   var aactual7 = document.getElementById("aactual7").checked;
   var aactual8 = document.getElementById("aactual8").checked;
   var aactual9 = document.getElementById("aactual9").checked;
   var aactual10 = document.getElementById("aactual10").checked;
   var aactual11 = document.getElementById("aactual11").checked;
   var aactual12 = document.getElementById("aactual12").checked;

   var atendencia1 = document.getElementById("atendencia1").checked;
   var atendencia2 = document.getElementById("atendencia2").checked;
   var atendencia3 = document.getElementById("atendencia3").checked;
   var atendencia4 = document.getElementById("atendencia4").checked;
   var atendencia5 = document.getElementById("atendencia5").checked;
   var atendencia6 = document.getElementById("atendencia6").checked;
   var atendencia7 = document.getElementById("atendencia7").checked;
   var atendencia8 = document.getElementById("atendencia8").checked;
   var atendencia9 = document.getElementById("atendencia9").checked;
   var atendencia10 = document.getElementById("atendencia10").checked;
   var atendencia11 = document.getElementById("atendencia11").checked;
   var atendencia12 = document.getElementById("atendencia12").checked;

   var atradicional_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+atradicional1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+atradicional2+', "resvalor": "Culto"},{"resid": 280, "estado": '+atradicional3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+atradicional4+', "resvalor": "Industria"},{"resid": 282, "estado": '+atradicional5+', "resvalor": "Militar"},{"resid": 281, "estado": '+atradicional6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+atradicional7+', "resvalor": "Salud"},{"resid": 278, "estado": '+atradicional8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+atradicional9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+atradicional10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+atradicional11+', "resvalor": "Taller"},{"resid": 276, "estado": '+atradicional12+', "resvalor": "Vivienda"}]';

    var aactual_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aactual1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+aactual2+', "resvalor": "Culto"},{"resid": 280, "estado": '+aactual3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+aactual4+', "resvalor": "Industria"},{"resid": 282, "estado": '+aactual5+', "resvalor": "Militar"},{"resid": 281, "estado": '+aactual6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+aactual7+', "resvalor": "Salud"},{"resid": 278, "estado": '+aactual8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+aactual9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+aactual10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+aactual11+', "resvalor": "Taller"},{"resid": 276, "estado": '+aactual12+', "resvalor": "Vivienda"}]';

   var atendencia_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+atendencia1+', "resvalor": "Comercio"},{"resid": 285, "estado": '+atendencia2+', "resvalor": "Culto"},{"resid": 280, "estado": '+atendencia3+', "resvalor": "Educacion"},{"resid": 283, "estado": '+atendencia4+', "resvalor": "Industria"},{"resid": 282, "estado": '+atendencia5+', "resvalor": "Militar"},{"resid": 281, "estado": '+atendencia6+', "resvalor": "Recreacion"},{"resid": 279, "estado": '+atendencia7+', "resvalor": "Salud"},{"resid": 278, "estado": '+atendencia8+', "resvalor": "Servicio Publico"},{"resid": 287, "estado": '+atendencia9+', "resvalor": "Sin Uso"},{"resid": 284, "estado": '+atendencia10+', "resvalor": "Socio Cultural"},{"resid": 286, "estado": '+atendencia11+', "resvalor": "Taller"},{"resid": 276, "estado": '+atendencia12+', "resvalor": "Vivienda"}]'; 
   //------------------------------------------------------------------------------------------------
      var PTR_ID_BLOQ1 = '1';
      var PTR_DES_BLOQ1 = $("#adescripcion").val();
     // var PTR_COD_CAT = '12DM';
      var PTR_RANG_EPO1 = document.getElementById('arango_epoca_hidden').value;
      var PTR_EST_BLOQ1 = document.getElementById('aestilo_bloque_hidden').value;
      var aepocaestilo2 = document.getElementById("aepocaestilocheck").checked;//var PTR_EPO_EST1
      var asistemaconstructivo2 = document.getElementById("asistemaconstructivocheck").checked;//var PTR_SIS_CONST1
      var PTR_EPO_EST1 = '[{"tipo": "CHK"},{"valor": '+aepocaestilo2+'}]';
      var PTR_SIS_CONST1 = '[{"tipo": "CHK"},{"valor": '+asistemaconstructivo2+'}]';
      var PTR_SIS_CONSTRUC1 = document.getElementById('asistema_constructivo').value;

     
      var PTR_NRO_VIV1 = $("#anro_viviendas").val();
      var PTR_NRO_HOG1 = $("#anro_hogares").val();
      
      var aacapites1 = document.getElementById("aacapites1").checked;
      var aacapites2 = document.getElementById("aacapites2").checked;
      var aacapites3 = document.getElementById("aacapites3").checked;
      var aacapites4 = document.getElementById("aacapites4").checked;
      var aacapites5 = document.getElementById("aacapites5").checked;
      var aacapites6 = document.getElementById("aacapites6").checked;

      var PTR_ACAP1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aacapites1+', "resvalor": "Elementos Estructurales (8%)"},{"resid": 285, "estado": '+aacapites2+', "resvalor": "Elementos Compositivos (6%)"},{"resid": 280, "estado": '+aacapites3+', "resvalor": "Acabados (3%)"},{"resid": 283, "estado": '+aacapites4+', "resvalor": "Elementos Artisiticos Compositivos (3%)"},{"resid": 282, "estado": '+aacapites5+', "resvalor": "Elementos Ornamentales (4%)"},{"resid": 281, "estado": '+aacapites6+', "resvalor": "Elementos Tipologicos Compositivos (9%)"}]';
         ////console.log('ACAPITES',PTR_ACAP1);



      var aindicador_historico1 = document.getElementById("aindicador_historico1").checked;
      var aindicador_historico2 = document.getElementById("aindicador_historico2").checked;
      var aindicador_historico3 = document.getElementById("aindicador_historico3").checked;
      var aindicador_historico4 = document.getElementById("aindicador_historico4").checked;
      var aindicador_historico5 = document.getElementById("aindicador_historico5").checked;

      var PTR_VAL_HIST1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_historico1+', "resvalor": "Inmueble que posee un valor testimonial y documental, que ilustra el desarrollo político, social, religioso, cultural, económico y la forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 285, "estado": '+aindicador_historico2+', "resvalor": "Inmueble que desde su creación se constituye en un hito en la memoria historica"},{"resid": 280, "estado": '+aindicador_historico3+', "resvalor": "Inmueble que en el transcurso del tiempo se constituyo en hito en la memoria historica, al ser escenario relacionado con personas o eventos importantes, dentro del proceso Histórico,social, cultural, económico de las comunidades"},{"resid": 283, "estado": '+aindicador_historico4+', "resvalor": "Inmueble que forma parte de un conjunto Histórico"},{"resid": 282, "estado": '+aindicador_historico5+', "resvalor": "Inmueble que posee elementos arqueológicos o testimonios arquitectonicos de construcciones preexistentes que documentan su evolución y desarrollo"}]';
        ////console.log('INDICADOR HISTÓRICO',PTR_VAL_HIST1);

      var aindicador_artistico1 = document.getElementById("aindicador_artistico1").checked;
      var aindicador_artistico2 = document.getElementById("aindicador_artistico2").checked;
      var aindicador_artistico3 = document.getElementById("aindicador_artistico3").checked; 

      var PTR_VAL_ART1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_artistico1+', "resvalor": "Inmueble que se constituye en un ejemplo sobresaliente por su singularidad arquitectónica/artística"},{"resid": 285, "estado": '+aindicador_artistico2+', "resvalor": "Inmueble que conserva elementos arquitectónicos y tradicionales de interés"},{"resid": 280, "estado": '+aindicador_artistico3+', "resvalor": "Inmueble poseedor de expresiones artísticas y decorativas de interés"}]';
        ////console.log('INDICADOR ARTÍSTICO',PTR_VAL_ART1);

      var aindicador_arquitectonico1 = document.getElementById("aindicador_arquitectonico1").checked;
      var aindicador_arquitectonico2 = document.getElementById("aindicador_arquitectonico2").checked;

      var PTR_VAL_ARQ1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_arquitectonico1+', "resvalor": "Inmueble poseedor de características Tipológicas representativas de estilos arquitectónicos"},{"resid": 285, "estado": '+aindicador_arquitectonico2+', "resvalor": "Inmueble con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalles"}]';
        ////console.log('INDICADOR ARQUITECTÓNICO',PTR_VAL_ARQ1);

      var aindicador_teclogico1 = document.getElementById("aindicador_teclogico1").checked;
      var aindicador_teclogico2 = document.getElementById("aindicador_teclogico2").checked;
      var aindicador_teclogico3 = document.getElementById("aindicador_teclogico3").checked;

      var PTR_VAL_TEC1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_teclogico1+', "resvalor": "Inmueble que se constituye en un exponente de las técnicas constructivas y uso de materiales característicos de una época o región determinada"},{"resid": 285, "estado": '+aindicador_teclogico2+', "resvalor": "Inmueble donde la aplicación de técnicas constructivas son singulares o de especial interés"},{"resid": 280, "estado": '+aindicador_teclogico3+', "resvalor":"Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
        ////console.log('INDICADOR TECNOLÓGICO',PTR_VAL_TEC1);

      var aindicador_integridad1 = document.getElementById("aindicador_integridad1").checked;
      var aindicador_integridad2 = document.getElementById("aindicador_integridad2").checked;
      var aindicador_integridad3 = document.getElementById("aindicador_integridad3").checked;
      
      var PTR_VAL_INTE1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_integridad1+', "resvalor": "Inmueble que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 285, "estado": '+aindicador_integridad2+', "resvalor": "Inmueble o espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"},{"resid": 280, "estado": '+aindicador_integridad3+', "resvalor": "Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada"}]';
        ////console.log('INDICADOR ITEGRIDAD',PTR_VAL_INTE1);
      
      var aindicador_urbano1 = document.getElementById("aindicador_urbano1").checked;
      var aindicador_urbano2 = document.getElementById("aindicador_urbano2").checked;
      var aindicador_urbano3 = document.getElementById("aindicador_urbano3").checked;

        
      var PTR_VAL_URB1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_urbano1+', "resvalor": "Inmueble que contribuye a definir un entorno de valor por su configuración y calidad en su estructura urbanística, el paisaje y/o el espacio público"},{"resid": 285, "estado": '+aindicador_urbano2+', "resvalor": "Inmueble que forma un perfil homogéneo y/ó armónico"},{"resid": 280, "estado": '+aindicador_urbano3+', "resvalor": "Inmueble considerado hito de referencia por su emplazamiento"}]';
        ////console.log('INDICADOR URBANO',PTR_VAL_URB1);
      
      var aindicador_inmaterial1 = document.getElementById("aindicador_inmaterial1").checked;
      var aindicador_inmaterial2 = document.getElementById("aindicador_inmaterial2").checked;

      var PTR_VALORES_1 = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+aindicador_inmaterial1+', "resvalor": "INDICADOR INMATERIAL (2% / 7%) Inmueble relacionado con la organización social, forma de vida, usos, representaciones, expresiones, conocimientos y técnicas que las comunidades y grupos sociales reconocen como parte de patrimonio"},{"resid": 285, "estado": '+aindicador_inmaterial2+', "resvalor": "INDICADOR SIMBÓLICO (1% / 5%) Inmueble o espacio abierto único que expresa una significación cultural, representa identidad y pertenencia para el colectivo"}]';

        var apuntuacion_ubiman = document.getElementById("apuntuacion_umcheck").checked;
        var apuntuacion_lineac = document.getElementById("apuntuacion_lccheck").checked;
        var apuntuacion_trazoma = document.getElementById("apuntuacion_tmcheck").checked;
        var apuntuacion_tipologiaa = document.getElementById("apuntuacion_tacheck").checked;
        var apuntucaion_areaedificablec = document.getElementById('apuntuacion_aeccheck').checked;
        var apuntuacion_fechaconst = document.getElementById('apuntuacion_fccheck').checked;
        var apuntuacion_autorconstr = document.getElementById('apuntuacion_accheck').checked;
        var apuntuacion_fechac = document.getElementById('apuntuacion_fcdccheck').checked;
        var apun_autorconstructor = document.getElementById('apuntuacion_acdccheck').checked;
        var apunt_existenciapm = document.getElementById('apuntuacion_epmcheck').checked;
        var apun_existenciara = document.getElementById('apuntuacion_eracheck').checked;
        var apun_existenciaen = document.getElementById('apuntuacion_eencheck').checked;
        var apun_ritosmitos = document.getElementById('apuntuacion_rmtocheck').checked;

        var PTR_UBIC_MANC = '[{"tipo": "CHK"},{"valor": "'+apuntuacion_ubiman+'"}]';
        var PTR_LIN_CONSC = '[{"tipo": "CHK"},{"valor": "'+apuntuacion_lineac+'"}]';
        var PTR_TRAZ_MANC = '[{"tipo": "CHK"},{"valor": "'+apuntuacion_trazoma+'"}]';
        var PTR_TIP_ARQC =  '[{"tipo": "CHK"},{"valor": "'+apuntuacion_tipologiaa+'"}]';
        var PTR_AER_EDIFC = '[{"tipo": "CHK"},{"valor": "'+apuntucaion_areaedificablec+'"}]';
        var PTR_FECH_CONSC = '[{"tipo": "CHK"},{"valor": "'+apuntuacion_fechaconst+'"}]';
        var PTR_AUT_CONS_C = '[{"tipo": "CHK"},{"valor": "'+apuntuacion_autorconstr+'"}]';
        var PTR_FEC_CONST_C = '[{"tipo": "CHK"},{"valor": "'+apuntuacion_fechac+'"}]';
        var PTR_AUT_CONSC = '[{"tipo": "CHK"},{"valor": "'+apun_autorconstructor+'"}]';
        var PTR_EXIST_PAT = '[{"tipo": "CHK"},{"valor": "'+apunt_existenciapm+'"}]';
        var PTR_EXIST_ARQ = '[{"tipo": "CHK"},{"valor": "'+apun_existenciara+'"}]';
        var PTR_EXIST_ELEMNAT = '[{"tipo": "CHK"},{"valor": "'+apun_existenciaen+'"}]';
        var PTR_EXIST_RITOS = '[{"tipo": "CHK"},{"valor": "'+apun_ritosmitos+'"}]';

      var aindicador_historico = $("#aindicador_historico").val();
      var aindicador_atistico = $("#aindicador_atistico").val();
      var aindicador_arquitectonico = $("#aindicador_arquitectonico").val();
      var aindicador_teclogico = $("#aindicador_teclogico").val();
      var aindicador_integrid = $("#aindicador_integrid").val();
      var aindicador_urbano = $("#aindicador_urbano").val();
      var aindicador_inmaterial = $("#aindicador_inmaterial").val();
      var aindicador_simbolico = $("#aindicador_simbolico").val();
      var aindicador_interior = $("#aindicador_interior").val();
      var adescripcion_exterior = $("#adescripcion_exterior").val();

      var PTR_VAL_HIS_DESC1 = caracteresEspecialesRegistrar(aindicador_historico);
      var PTR_VAL_ART_DESC1 = caracteresEspecialesRegistrar(aindicador_atistico);
      var PTR_VAL_ARQ_DESC1 = caracteresEspecialesRegistrar(aindicador_arquitectonico);   
      var PTR_VAL_TEC_DESC1 = caracteresEspecialesRegistrar(aindicador_teclogico);
      var PTR_VAL_INTE_DESC1 = caracteresEspecialesRegistrar(aindicador_integrid);
      var PTR_VAL_URB_DESC1 = caracteresEspecialesRegistrar(aindicador_urbano);
      var PTR_VAL_INMAT_DESC1 = caracteresEspecialesRegistrar(aindicador_inmaterial);
      var PTR_VAL_SIMB_DESC1 = caracteresEspecialesRegistrar(aindicador_simbolico);
      var PTR_VAL_INT_DESC1 = caracteresEspecialesRegistrar(aindicador_interior);
      var PTR_VAL_EXT_DESC1 = caracteresEspecialesRegistrar(adescripcion_exterior);
      var PTR_CATEGORIA1 = $("#acategoria_inmueble").val();
      var PTR_PUNTO = $("#apuntuacion").val();
      var PTR_ID_BLOQ1 = $("#aid_bloque").val();

       //-----------------GRILLA PAREDES EXTERIORES--------------------------------------//

  var cont=0;
        arrDibujoPExt.forEach(function (item, index, array) 
        {
          arrCodigoPExt[cont].val_est_con = $("#estadPEx"+item.id+"").val();
          arrCodigoPExt[cont].val_int = $("#intePEx"+item.id+"").val();
          arrCodigoPExt[cont].val_tipo = $("#tipPEx"+item.id+"").val();
          arrCodigoPExt[cont].val_mat = $("#matPEx"+item.id+"").val();
          cont++;
        });

  var abloqueParedExterna = '[{"tipo": "GRD","campos": "par_estado|par_inte|par_tipo|par_mat", "titulos": "EstadodeConservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';

  for (var i = 0; i<arrCodigoPExt.length;i++)
  {
    var grillaParedExterna = '{"par_mat": "'+arrCodigoPExt[i].val_mat+'","par_inte": "'+arrCodigoPExt[i].val_int+'","par_tipo": "'+arrCodigoPExt[i].val_tipo+'","par_estado": "'+arrCodigoPExt[i].val_est_con+'","par_mat_valor": 1,"par_inte_valor": 1,"par_tipo_valor": 1,"par_estado_valor": 2}';
    abloqueParedExterna = (abloqueParedExterna.concat(',').concat(grillaParedExterna));
     
  }
  var agrillaParedExterna  = abloqueParedExterna.concat(']');

    //---------------GRILLA COBERTURA  DEL TECHO-------------------//
  var cont=0;
      arrDibujoCTec.forEach(function (item, index, array) 
      {
        arrCodigoCTec[cont].val_est_con = $("#estadocCTe"+item.id+"").val();
        arrCodigoCTec[cont].val_inte = $("#integridadCTec"+item.id+"").val();
        arrCodigoCTec[cont].val_tipo = $("#tipoCTec"+item.id+"").val();
        arrCodigoCTec[cont].val_mate = $("#materialCTec"+item.id+"").val();
        cont++;
      });

  var abloqueCoberturaTecho = '[ {"tipo": "GRD","campos": "cob_estado|cob_inte|cob_tipo|par_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoCTec.length;i++)
  {
    var grillaCoberturaTecho = '{"par_mat": "'+arrCodigoCTec[i].val_mate+'","cob_inte": "'+arrCodigoCTec[i].val_inte+'","cob_tipo": "'+arrCodigoCTec[i].val_tipo+'","cob_estado": "'+arrCodigoCTec[i].val_est_con+'","par_mat_valor": 1,"cob_inte_valor": 1,"cob_tipo_valor": 1,"cob_estado_valor": 1}';
    abloqueCoberturaTecho = (abloqueCoberturaTecho.concat(',').concat(grillaCoberturaTecho));    
  }

  var agrillaCoberturaTecho = abloqueCoberturaTecho.concat(']');
 
  //---------------GRILLA PAREDES INTERIORES-------------------//
    var cont=0;
    arrDibujoPInt.forEach(function (item, index, array) 
    {
      arrCodigoPInt[cont].val_est_conser = $("#estadocPIn"+item.id+"").val();
      arrCodigoPInt[cont].val_int = $("#integridadPIn"+item.id+"").val();
      arrCodigoPInt[cont].val_tipo = $("#tipoPIn"+item.id+"").val();
      arrCodigoPInt[cont].val_mat = $("#materialPIn"+item.id+"").val();
      cont++;
    });

  var abloqueParedeInterior = '[ {"tipo": "GRD","campos": "pari_estado|pari_inte|pari_tipo|pari_mat","titulos": "Estado Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoPInt.length;i++)
  {
    var agrillaParedInterior = '{"pari_mat": "'+arrCodigoPInt[i].val_mat+'","pari_inte": "'+arrCodigoPInt[i].val_int+'","pari_tipo": "'+arrCodigoPInt[i].val_tipo+'","pari_estado": "'+arrCodigoPInt[i].val_est_conser+'","pari_mat_valor": 1,"pari_inte_valor": 1,"pari_tipo_valor": 2,"pari_estado_valor": 1}';
    abloqueParedeInterior = (abloqueParedeInterior.concat(',').concat(agrillaParedInterior));    
  }

  var agrillaParedInterior  = abloqueParedeInterior.concat(']');
 
  //---------------GRILLA ENTRE PISOS-------------------//
    var cont=0;
    arrDibujoEP.forEach(function (item, index, array) 
    {
      arrCodigoEP[cont].val_esta = $("#estadocEnP"+item.id+"").val();
      arrCodigoEP[cont].val_int = $("#integridadEnP"+item.id+"").val();
      arrCodigoEP[cont].val_tipo = $("#tipoEnP"+item.id+"").val();
      arrCodigoEP[cont].val_mat = $("#materialEnP"+item.id+"").val();
      cont++;
    });

     var abloqueEntrePisos = '[{"tipo": "GRD","campos": "ent_estado|ent_inte|ent_tipo|ent_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoEP.length;i++)
  {
    var agrillaEntrePisos = '{"ent_mat": "'+arrCodigoEP[i].val_mat+'","ent_inte": "'+arrCodigoEP[i].val_int+'","ent_tipo": "'+arrCodigoEP[i].val_tipo+'","ent_estado": "'+arrCodigoEP[i].val_esta+'","ent_mat_valor": 1,"ent_inte_valor": 1,"ent_tipo_valor": 1,"ent_estado_valor": 1}';
    abloqueEntrePisos = (abloqueEntrePisos.concat(',').concat(agrillaEntrePisos));    
  }
    var agrillaEntrePisos = abloqueEntrePisos.concat(']');
 
  //---------------GRILLA ESCALERAS-------------------//
  var cont=0;
  arrDibujoEsc.forEach(function (item, index, array) 
  {
    arrCodigoEsc[cont].val_estado = $("#estadocES"+item.id+"").val();
    arrCodigoEsc[cont].val_int = $("#integridadES"+item.id+"").val();
    arrCodigoEsc[cont].val_tipo = $("#tipoES"+item.id+"").val();
    arrCodigoEsc[cont].val_mat = $("#materialES"+item.id+"").val();
    cont++;
  });

  var abloqueEscalera = '[{"tipo": "GRD","campos": "esc_estado|esc_inte|esc_tipo|esc_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoEsc.length;i++)
  {

    var agrillaEscalera = '{"esc_mat": "'+arrCodigoEsc[i].val_mat+'","esc_inte": "'+arrCodigoEsc[i].val_int+'","esc_tipo": "'+arrCodigoEsc[i].val_tipo+'","esc_estado": "'+arrCodigoEsc[i].val_estado+'","esc_mat_valor": 2,"esc_inte_valor": 1,"esc_tipo_valor": 4,"esc_estado_valor": 2}';
    abloqueEscalera = (abloqueEscalera.concat(',').concat(agrillaEscalera));    
  }
   var agrillaEscalera = abloqueEscalera.concat(']');

  //---------------GRILLA CIELOS-------------------//
   var cont=0;
  arrDibujoCielo.forEach(function (item, index, array) 
  {
    arrCodigoCielo[cont].val_est = $("#estadocCi"+item.id+"").val();
    arrCodigoCielo[cont].val_int = $("#integridadCi"+item.id+"").val();
    arrCodigoCielo[cont].val_tipo = $("#tipoCi"+item.id+"").val();
    arrCodigoCielo[cont].val_mat = $("#materialCi"+item.id+"").val();
    cont++;
  });
   var abloqueCielo = '[{"tipo": "GRD","campos": "cie_estado|cie_inte|cie_tipo|cie_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoCielo.length;i++)
  {

    var agrillaCielo = '{"cie_mat": "'+arrCodigoCielo[i].val_mat+'","cie_inte": "'+arrCodigoCielo[i].val_int+'","cie_tipo": "'+arrCodigoCielo[i].val_tipo+'","cie_estado": "'+arrCodigoCielo[i].val_est+'","cie_mat_valor": 2,"cie_inte_valor": 1,"cie_tipo_valor": 4,"cie_estado_valor": 2}';
    abloqueCielo = (abloqueCielo.concat(',').concat(agrillaCielo));    
  }
 var agrillaCielo= abloqueCielo.concat(']');
  
  //---------------GRILLA BALCONES-------------------//
  var cont=0;
  arrDibujoBal.forEach(function (item, index, array) 
  {
    arrCodigoBal[cont].val_est = $("#estadocBal"+item.id+"").val();
    arrCodigoBal[cont].val_inte = $("#integridadBal"+item.id+"").val();
    arrCodigoBal[cont].val_tipo = $("#tipoBal"+item.id+"").val();
    arrCodigoBal[cont].val_mat = $("#materialBal"+item.id+"").val();
    cont++;
  });

  var abloqueBalcon = '[ {"tipo": "GRD","campos": "bal_estado|bal_inte|bal_tipo|bal_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoBal.length;i++)
  {
    var agrillaBalcon = '{"bal_mat": "'+arrCodigoBal[i].val_mat+'","bal_inte": "'+arrCodigoBal[i].val_inte+'","bal_tipo": "'+arrCodigoBal[i].val_tipo+'","bal_estado": "'+arrCodigoBal[i].val_est+'","bal_mat_valor": 1,"bal_inte_valor": 2,"bal_tipo_valor": 1,"bal_estado_valor": 3}';
    abloqueBalcon = (abloqueBalcon.concat(',').concat(agrillaBalcon));    
  }

  var agrillaBalcon = abloqueBalcon.concat(']');
  
  //---------------GRILLA ESTRUCTURA ABIERTA-------------------//
   var cont=0;
    arrDibujoEsC.forEach(function (item, index, array) 
    {
      arrCodigoEsC[cont].val_est = $("#estadocEsC"+item.id+"").val();
      arrCodigoEsC[cont].val_int = $("#integridadEsC"+item.id+"").val();
      arrCodigoEsC[cont].val_tipo = $("#tipoEsC"+item.id+"").val();
      arrCodigoEsC[cont].val_mat = $("#materialEsC"+item.id+"").val();
      cont++;
    });

  var abloqueEstructuraCubierta = '[{"tipo": "GRD","campos": "est_cub_estado|est_cub_inte|est_cub_tipo|est_cub_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoEsC.length;i++)
  {

    var agrillaEstructuraCubierta = '{"est_cub_mat": "'+arrCodigoEsC[i].val_mat+'","est_cub_inte": "'+arrCodigoEsC[i].val_int+'","est_cub_tipo": "'+arrCodigoEsC[i].val_tipo+'","est_cub_estado": "'+arrCodigoEsC[i].val_est+'","est_cub_mat_valor": 2,"est_cub_inte_valor": 3,"est_cub_tipo_valor": 2,"est_cub_estado_valor": 1}';
    abloqueEstructuraCubierta = (abloqueEstructuraCubierta.concat(',').concat(agrillaEstructuraCubierta));    
  }
  var agrillaEstructuraCubierta = abloqueEstructuraCubierta.concat(']');
  
  //---------------GRILLA PISOS-------------------//
    var cont=0;
    arrDibujoPiso.forEach(function (item, index, array) 
    {
      arrCodigoPiso[cont].val_est = $("#estadoPi"+item.id+"").val();
      arrCodigoPiso[cont].val_int = $("#integridadPi"+item.id+"").val();
      arrCodigoPiso[cont].val_tipo = $("#tipoPi"+item.id+"").val();
      arrCodigoPiso[cont].val_mat = $("#materialPi"+item.id+"").val();
      cont++;
    });
  var abloquePiso = '[{"tipo": "GRD","campos": "piso_estado|piso_inte|piso_tipo|piso_mat","titulos": "Estado de Conservación|Integridad|Tipo|Materiales","impresiones": "undefined|undefined|undefined|undefined|"}';
  for (var i = 0; i<arrCodigoPiso.length;i++)
  {

    var agrillaPiso = '{"piso_mat": "'+arrCodigoPiso[i].val_mat+'","piso_inte": "'+arrCodigoPiso[i].val_int+'","piso_tipo": "'+arrCodigoPiso[i].val_tipo+'","piso_estado": "'+arrCodigoPiso[i].val_est+'","piso_mat_valor": 1,"piso_inte_valor": 1,"piso_tipo_valor": 1,"piso_estado_valor": 2}';
    abloquePiso = (abloquePiso.concat(',').concat(agrillaPiso));    
  }
  var agrillaPiso = abloquePiso.concat(']');

     //---- peligros potenciales----------------------
     var contPPactu=0;
      arrDibujoPPact.forEach(function (item, index, array) 
      {
        arrayPPactualizar[contPPactu].aval_pe = $("#apeligropPP"+item.id+"").val();
        arrayPPactualizar[contPPactu].aval_pel = $("#apeligropPPO"+item.id+"").val();
        arrayPPactualizar[contPPactu].aval_cau = $("#acausasPP"+item.id+"").val();
        contPPactu++;
      });
      var abloqueFiguraProtecpp = '[{"tipo":"GRD","campos":"f01_peligro_DES_|f01_reja_DES_|f01_reja_INT_","titulos":"Peligros Potenciales|Peligros Potenciales|Nivel de Peligro"}';
      for (var i = 0; i<arrDibujoPPact.length;i++)
      {
        //////console.log("grillapruebaI"+i+"",arrCodigo[i]);
        var agrillaFigurapp = '{"f01_peligro_DES_":"'+arrayPPactualizar[i].aval_pe+'","f01_reja_DES_":"'+arrayPPactualizar[i].aval_pel+'","f01_reja_INT_":"'+arrayPPactualizar[i].aval_cau+'","f01_peligro_DES_valor":"'+arrayPPactualizar[i].aval_pe+'","f01_reja_DES_valor":"'+arrayPPactualizar[i].aval_pel+'","f01_reja_INT_valor":"'+arrayPPactualizar[i].aval_cau+'"}';
        abloqueFiguraProtecpp = (abloqueFiguraProtecpp.concat(',').concat(agrillaFigurapp));

      }
      abloqueFiguraProtecpp = abloqueFiguraProtecpp.concat(']');
       var apeligro_potencial_grilla = abloqueFiguraProtecpp;
     
     //-------------actualizar patologias edificación---------------------
        var contPEact=0;
        arrDibujoPEact.forEach(function (item, index, array) 
        {
          arrPEAct[contPEact].aval_daños = $("#dañoPEa"+item.id+"").val();
          arrPEAct[contPEact].aval_causas = $("#causaPEa"+item.id+"").val();
          contPEact++;
        });
        var abloqueFiguraProtecPE = '[{"tipo":"GRD","campos":"f01_reja_DES_|f01_causa_DES_","titulos":"Daños|Causas"}';
        for (var i = 0; i<arrDibujoPEact.length;i++)
        {
          var agrillaFiguraPE = '{"f01_reja_DES_":"'+arrPEAct[i].aval_daños+'","f01_causa_DES_":"'+arrPEAct[i].aval_causas+'","f01_reja_DES_valor":"'+arrPEAct[i].aval_daños+'","f01_causa_DES_valor":"'+arrPEAct[i].aval_causas+'"}';
          abloqueFiguraProtecPE = (abloqueFiguraProtecPE.concat(',').concat(agrillaFiguraPE));
        }
        abloqueFiguraProtecPE = abloqueFiguraProtecPE.concat(']');
        var apatologia_edif_grilla = abloqueFiguraProtecPE;

      var contINEact=0;
       arrDibujoIEs.forEach(function (item, index, array) 
       {
       arrINEactualiza[contINEact].avar_inst_est = $("#ainstalacionINE"+item.id+"").val();
       arrINEactualiza[contINEact].avar_pisose = $("#apisoservINE"+item.id+"").val();
       arrINEactualiza[contINEact].avar_est = $("#aestadoINE"+item.id+"").val();
       arrINEactualiza[contINEact].avar_int = $("#aintegridadINE"+item.id+"").val();
        contINEact++;
      });
       var abloqueFiguraProtecINE = '[{"tipo":"GRD","campos":"f01_Instalaciones_DESC|f01_servidumbre_DESC|f01_estado_G_SERV_PROV|f01_integridad_G_SERV_PROV","titulos":"Instalaciones Estado|Paso de Servidumbre|Estado|Integridad"}';

       for (var i = 0; i<arrDibujoIEs.length;i++)
       {
        var agrillaFiguraINE = '{"f01_Instalaciones_DESC":"'+arrINEactualiza[i].avar_inst_est+'","f01_servidumbre_DESC":"'+arrINEactualiza[i].avar_pisose+'","f01_estado_G_SERV_PROV":"'+arrINEactualiza[i].avar_est+'","f01_integridad_G_SERV_PROV":"'+arrINEactualiza[i].avar_int+'","f01_Instalaciones_DESC_valor": "'+arrINEactualiza[i].avar_inst_est+'","f01_servidumbre_DESC_valor": "'+arrINEactualiza[i].avar_pisose+'","f01_estado_G_SERV_PROV_valor": "'+arrINEactualiza[i].avar_est+'","f01_integridad_G_SERV_PROV_valor": "'+arrINEactualiza[i].avar_int+'"}';
        abloqueFiguraProtecINE = (abloqueFiguraProtecINE.concat(',').concat(agrillaFiguraINE));

      }
      abloqueFiguraProtecINE = abloqueFiguraProtecINE.concat(']');
      var ainstalaciones_estado_grilla = abloqueFiguraProtecINE;

      //------Actualizar servicios-----------------------
       var contSact=0;
         arrDibujoServAct.forEach(function (item, index, array) 
         {
          arrSERVact[contSact].aval_desc = $("#adescripcionSERV"+item.id+"").val();
          arrSERVact[contSact].aval_prov = $("#aproveedorSERV"+item.id+"").val();
          contSact++;
        });
         var abloqueFiguraProtecSERV = '[{"tipo":"GRD","campos":"f01_emision_G_SERV_DESC|f01_emision_G_SERV_PROV","titulos":"Descripción|Proveedor"}';

         for (var i = 0; i<arrDibujoServAct.length;i++)
         {
          var agrillaFiguraSERV = '{"f01_emision_G_SERV_DESC":"'+arrSERVact[i].aval_desc+'","f01_emision_G_SERV_PROV":"'+arrSERVact[i].aval_prov+'","f01_emision_G_SERV_DESC_valor":"'+arrSERVact[i].aval_desc+'","f01_emision_G_SERV_PROV_valor":"'+arrSERVact[i].aval_prov+'"}';
          abloqueFiguraProtecSERV = (abloqueFiguraProtecSERV.concat(',').concat(agrillaFiguraSERV));

        }
        abloqueFiguraProtecSERV = abloqueFiguraProtecSERV.concat(']');
        var aservios_grilla = abloqueFiguraProtecSERV;

      //--------actualizar vanos ventanas interiores-----
      var contvvia=0;
       arrDibujoVVInt.forEach(function (item, index, array) 
            {
              arrVVIactualizar[contvvia].aarg_vent_int_estado = $("#avent_int_estado"+item.id+"").val();
              arrVVIactualizar[contvvia].aarg_vent_int_inte = $("#avent_int_inte"+item.id+"").val();
              arrVVIactualizar[contvvia].aarg_vent_int_tipo = $("#avent_int_tipo"+item.id+"").val();
              arrVVIactualizar[contvvia].aarg_vent_int_mat = $("#avent_int_mat"+item.id+"").val();
              contvvia++;
             
            });
        var abloqueFiguraProtecvvi= '[{"tipo":"GRD","campos":"vent_int_estado|vent_int_inte|vent_int_tipo|vent_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
        var agrillaFiguravvi = '';   
        for (var i = 0; i<arrDibujoVVInt.length;i++)
          {
          agrillaFiguravvi = '{"vent_int_estado":"'+ arrVVIactualizar[i].aarg_vent_int_estado +'","vent_int_inte":"'+ arrVVIactualizar[i].aarg_vent_int_inte +'","vent_int_tipo":"'+ arrVVIactualizar[i].aarg_vent_int_tipo +'","vent_int_mat":"'+ arrVVIactualizar[i].aarg_vent_int_mat +'","vent_int_estado_valor":"'+ arrVVIactualizar[i].aarg_vent_int_estado +'","vent_int_tipo_valor":"'+ arrVVIactualizar[i].aarg_vent_int_inte +'","vent_int_inte_valor":"'+ arrVVIactualizar[i].aarg_vent_int_tipo +'","vent_int_mat_valor":"'+ arrVVIactualizar[i].aarg_vent_int_mat +'"}';
          abloqueFiguraProtecvvi = (abloqueFiguraProtecvvi.concat(',').concat(agrillaFiguravvi));
          }
          abloqueFiguraProtecvvi = abloqueFiguraProtecvvi.concat(']');
          var avan_venta_int = abloqueFiguraProtecvvi;

        //----- actualizar ventanas interiores--------------------------------------
         var contdvi=0;
          arrDibujoVIact.forEach(function (item, index, array) 
              {
                arrayVIactualizar[contdvi].aarg_vent_inte_estado = $("#avent_inte_estado"+item.id+"").val();
                arrayVIactualizar[contdvi].aarg_vent_inte_inte = $("#avent_inte_inte"+item.id+"").val();
                arrayVIactualizar[contdvi].aarg_vent_inte_tipo = $("#avent_inte_tipo"+item.id+"").val();
                arrayVIactualizar[contdvi].aarg_vent_inte_mat = $("#avent_inte_mat"+item.id+"").val();
                contdvi++;
               
              });
          var abloqueFiguraProtecdvi= '[{"tipo":"GRD","campos":"vent_inte_estado|vent_inte_inte|vent_inte_tipo|vent_inte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
         
          var agrillaFiguradvi = '';   
          for (var i = 0; i<arrDibujoVIact.length;i++)
          {
          agrillaFiguradvi = '{"vent_inte_estado":"'+ arrayVIactualizar[i].aarg_vent_inte_estado +'","vent_inte_inte":"'+ arrayVIactualizar[i].aarg_vent_inte_inte +'","vent_inte_tipo":"'+ arrayVIactualizar[i].aarg_vent_inte_tipo +'","vent_inte_mat":"'+ arrayVIactualizar[i].aarg_vent_inte_mat +'","vent_inte_estado_valor":"'+ arrayVIactualizar[i].aarg_vent_inte_estado +'","vent_inte_inte_valor":"'+ arrayVIactualizar[i].aarg_vent_inte_inte +'","vent_inte_tipo_valor":"'+ arrayVIactualizar[i].aarg_vent_inte_tipo +'","vent_inte_mat_valor":"'+ arrayVIactualizar[i].aarg_vent_inte_mat +'"}';
          abloqueFiguraProtecdvi = (abloqueFiguraProtecdvi.concat(',').concat(agrillaFiguradvi));
           
          }
          abloqueFiguraProtecdvi = abloqueFiguraProtecdvi.concat(']');
          var aventana_interior = abloqueFiguraProtecdvi;
          //--------actualizar vanos ventanas exteriores-----------------
          var contVVExtact=0;
          arrDibujoVVExt.forEach(function (item, index, array) 
            {
              arrayVVExtact[contVVExtact].aarg_vent_ext_estado = $("#avent_ext_estado"+item.id+"").val();
              arrayVVExtact[contVVExtact].aarg_vent_ext_inte = $("#avent_ext_inte"+item.id+"").val();
              arrayVVExtact[contVVExtact].aarg_vent_ext_tipo = $("#avent_ext_tipo"+item.id+"").val();
              arrayVVExtact[contVVExtact].aarg_vent_ext_mat = $("#avent_ext_mat"+item.id+"").val();
              contVVExtact++;
            });
            var abloqueFiguraProtecdvve= '[{"tipo":"GRD","campos":"vent_ext_estado|vent_ext_inte|vent_ext_tipo|vent_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
            var agrillaFiguradvve = '';   
            for (var i = 0; i<arrDibujoVVExt.length;i++)
            {
            agrillaFiguradvve = '{"vent_ext_estado":"'+arrayVVExtact[i].aarg_vent_ext_estado+'","vent_ext_inte":"'+arrayVVExtact[i].aarg_vent_ext_inte+'","vent_ext_tipo":"'+arrayVVExtact[i].aarg_vent_ext_tipo+'","vent_ext_mat":"'+arrayVVExtact[i].aarg_vent_ext_mat+'","vent_ext_estado_valor": "'+ arrayVVExtact[i].aarg_vent_ext_estado +'","vent_ext_inte_valor": "'+ arrayVVExtact[i].aarg_vent_ext_inte +'","vent_ext_tipo_valor": "'+arrayVVExtact[i].aarg_vent_ext_tipo+'","vent_ext_mat_valor": "'+arrayVVExtact[i].aarg_vent_ext_mat+'"}';
            abloqueFiguraProtecdvve = (abloqueFiguraProtecdvve.concat(',').concat(agrillaFiguradvve));
            }
            abloqueFiguraProtecdvve = abloqueFiguraProtecdvve.concat(']');
            var avan_vent_exterior = abloqueFiguraProtecdvve;

            //---------actualizar ventanas exteriores-------
            var contdve=0;
            arrDibujoVExact.forEach(function (item, index, array) 
                {
                  arrayVExactualizar[contdve].aarg_vent_exte_estado = $("#avent_exte_estado"+item.id+"").val();
                  arrayVExactualizar[contdve].aaarg_vent_exte_tipo = $("#avent_exte_inte"+item.id+"").val();
                  arrayVExactualizar[contdve].arg_vent_exte_tipo = $("#avent_exte_tipo"+item.id+"").val();
                  arrayVExactualizar[contdve].aarg_vent_exte_mat = $("#avent_exte_mat"+item.id+"").val();
                  contdve++;
                 
                });
            var abloqueFiguraProtecdve= '[{"tipo":"GRD","campos":"vent_exte_estado|vent_exte_inte|vent_exte_tipo|vent_exte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
            var agrillaFiguradve = '';   
            for (var i = 0; i<arrDibujoVExact.length;i++)
            {
            agrillaFiguradve = '{"vent_exte_estado":"'+ arrayVExactualizar[i].aarg_vent_exte_estado +'","vent_exte_inte":"'+ arrayVExactualizar[i].aaarg_vent_exte_tipo +'","vent_exte_tipo":"'+ arrayVExactualizar[i].arg_vent_exte_tipo +'","vent_exte_mat":"'+ arrayVExactualizar[i].aarg_vent_exte_mat +'","vent_exte_estado_valor": "'+ arrayVExactualizar[i].aarg_vent_exte_estado +'","vent_exte_inte_valor": "'+ arrayVExactualizar[i].aaarg_vent_exte_tipo +'","vent_exte_tipo_valor": "'+ arrayVExactualizar[i].arg_vent_exte_tipo +'","vent_exte_mat_valor": "'+ arrayVExactualizar[i].aarg_vent_exte_mat +'"}';
            abloqueFiguraProtecdve = (abloqueFiguraProtecdve.concat(',').concat(agrillaFiguradve));
             
            }
            abloqueFiguraProtecdve = abloqueFiguraProtecdve.concat(']');
            var aventana_exterior = abloqueFiguraProtecdve;

            //-----------actualizar vanos puertas interiores-----------------
           var contVPIntactua=0;
            arrDibujoVPIn.forEach(function (item, index, array) 
                {
                  arrayVPIntact[contVPIntactua].aarg_pue_int_estado = $("#apue_int_estado"+item.id+"").val();
                  arrayVPIntact[contVPIntactua].aarg_pue_int_inte = $("#apue_int_inte"+item.id+"").val();
                  arrayVPIntact[contVPIntactua].aarg_pue_int_tipo = $("#apue_int_tipo"+item.id+"").val();
                  arrayVPIntact[contVPIntactua].aarg_pue_int_mat = $("#apue_int_mat"+item.id+"").val();
                  contVPIntactua++;         
                });
            var abloqueFiguraProtecdvpi= '[{"tipo":"GRD","campos":"pue_int_estado|pue_int_inte|pue_int_tipo|pue_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
            var agrillaFiguradvpi = '';   
            for (var i = 0; i<arrDibujoVPIn.length;i++)
            {
            agrillaFiguradvpi = '{"pue_int_estado":"'+ arrayVPIntact[i].aarg_pue_int_estado +'","pue_int_inte":"'+ arrayVPIntact[i].aarg_pue_int_inte +'","pue_int_tipo":"'+ arrayVPIntact[i].aarg_pue_int_tipo +'","pue_int_mat":"'+ arrayVPIntact[i].aarg_pue_int_mat +'","pue_int_estado_valor": "'+ arrayVPIntact[i].aarg_pue_int_estado +'","pue_int_inte_valor": "'+ arrayVPIntact[i].aarg_pue_int_inte +'","pue_int_tipo_valor": "'+ arrayVPIntact[i].aarg_pue_int_tipo +'","pue_int_mat_valor": "'+ arrayVPIntact[i].aarg_pue_int_mat +'"}';
            abloqueFiguraProtecdvpi = (abloqueFiguraProtecdvpi.concat(',').concat(agrillaFiguradvpi));
            }
            abloqueFiguraProtecdvpi = abloqueFiguraProtecdvpi.concat(']');
            var avan_puert_int = abloqueFiguraProtecdvpi;

          //-----actualizar vanos puertas exteriores----------
            var contdvpe=0;
            arrDibujoVPEact.forEach(function (item, index, array) 
                {
                  aarrCodigo_ptr_van_pue_ext[contdvpe].aarg_pue_ext_estado = $("#apue_ext_estado"+item.id+"").val();
                  aarrCodigo_ptr_van_pue_ext[contdvpe].aarg_pue_ext_inte = $("#apue_ext_inte"+item.id+"").val();
                  aarrCodigo_ptr_van_pue_ext[contdvpe].aarg_pue_ext_tipo = $("#apue_ext_tipo"+item.id+"").val();
                  aarrCodigo_ptr_van_pue_ext[contdvpe].aarg_pue_ext_mat = $("#apue_ext_mat"+item.id+"").val();
                  contdvpe++;         
                });
            var abloqueFiguraProtecdvpe= '[{"tipo":"GRD","campos":"pue_ext_estado|pue_ext_inte|pue_ext_tipo|pue_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
            var agrillaFiguradvpe = '';   
            for (var i = 0; i<arrDibujoVPEact.length;i++)
            {
            agrillaFiguradvpe = '{"pue_ext_estado":"'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_estado +'","pue_ext_inte":"'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_inte +'","pue_ext_tipo":"'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_tipo +'","pue_ext_mat":"'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_mat +'","pue_ext_estado_valor": "'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_estado +'","pue_ext_inte_valor": "'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_inte +'","pue_ext_tipo_valor": "'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_tipo +'","pue_ext_mat_valor": "'+ aarrCodigo_ptr_van_pue_ext[i].aarg_pue_ext_mat +'"}';
            abloqueFiguraProtecdvpe = (abloqueFiguraProtecdvpe.concat(',').concat(agrillaFiguradvpe));
            }
            abloqueFiguraProtecdvpe = abloqueFiguraProtecdvpe.concat(']');
            var avan_puert_ext = abloqueFiguraProtecdvpe;

           //-----actualizar puertas interiores---------------
           var contPIntAct=0;
            arrDibujoPIntAct.forEach(function (item, index, array) 
                {
                  aarrCodigo_ptr_pue_int[contPIntAct].aarg_pue_inte_estado = $("#apue_inte_estado"+item.id+"").val();
                  aarrCodigo_ptr_pue_int[contPIntAct].aarg_pue_inte_inte = $("#apue_inte_inte"+item.id+"").val();
                  aarrCodigo_ptr_pue_int[contPIntAct].aarg_pue_inte_tipo = $("#apue_inte_tipo"+item.id+"").val();
                  aarrCodigo_ptr_pue_int[contPIntAct].aarg_pue_inte_mat = $("#apue_inte_mat"+item.id+"").val();
                  contPIntAct++;         
                });
            var abloqueFiguraProtecdpi= '[{"tipo":"GRD","campos":"pue_inte_estado|pue_inte_inte|pue_inte_tipo|pue_inte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
            var agrillaFiguradpi = '';   
            for (var i = 0; i<arrDibujoPIntAct.length;i++)
            {
            agrillaFiguradpi = '{"pue_inte_estado":"'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_estado +'","pue_inte_inte":"'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_inte +'","pue_inte_tipo":"'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_tipo +'","pue_inte_mat":"'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_mat +'", "pue_inte_estado_valor": "'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_estado +'","pue_inte_inte_valor": "'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_inte +'","pue_inte_tipo_valor": "'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_tipo +'","pue_inte_mat_valor": "'+ aarrCodigo_ptr_pue_int[i].aarg_pue_inte_mat +'"}';
            abloqueFiguraProtecdpi = (abloqueFiguraProtecdpi.concat(',').concat(agrillaFiguradpi));
            }
            abloqueFiguraProtecdpi = abloqueFiguraProtecdpi.concat(']');
            var apuerta_ineterior = abloqueFiguraProtecdpi;

          //------actualizar puertas estriores---------------------
         var contdpe=0;
          arrDibujoPExact.forEach(function (item, index, array) 
              {
                arrcodPExtact[contdpe].aarg_pue_exte_estado = $("#apue_exte_estado"+item.id+"").val();
                arrcodPExtact[contdpe].aarg_pue_exte_inte = $("#apue_exte_inte"+item.id+"").val();
                arrcodPExtact[contdpe].aarg_pue_exte_tipo = $("#apue_exte_tipo"+item.id+"").val();
                arrcodPExtact[contdpe].aarg_pue_exte_mat = $("#apue_exte_mat"+item.id+"").val();
                contdpe++;         
              });
          var abloqueFiguraProtecdpe= '[{"tipo":"GRD","campos":"pue_exte_estado|pue_exte_inte|pue_exte_tipo|pue_exte_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
          var agrillaFiguradpe = '';   
          for (var i = 0; i<arrDibujoPExact.length;i++)
          {
          agrillaFiguradpe = '{"pue_exte_estado":"'+ arrcodPExtact[i].aarg_pue_exte_estado +'","pue_exte_inte":"'+ arrcodPExtact[i].aarg_pue_exte_inte +'","pue_exte_tipo":"'+ arrcodPExtact[i].aarg_pue_exte_tipo +'","pue_exte_mat":"'+ arrcodPExtact[i].aarg_pue_exte_mat +'","pue_exte_estado_valor": "'+ arrcodPExtact[i].aarg_pue_exte_estado +'","pue_exte_inte_valor": "'+ arrcodPExtact[i].aarg_pue_exte_inte +'","pue_exte_tipo_valor": "'+ arrcodPExtact[i].aarg_pue_exte_tipo +'","pue_exte_mat_valor": "'+ arrcodPExtact[i].aarg_pue_exte_mat +'"}';
          abloqueFiguraProtecdpe = (abloqueFiguraProtecdpe.concat(',').concat(agrillaFiguradpe));
          }
          abloqueFiguraProtecdpe = abloqueFiguraProtecdpe.concat(']');
          var apuerta_externa = abloqueFiguraProtecdpe;

        //---------actualizar rejas-----------------
        var contrejasact=0;
          arrDibRejas.forEach(function (item, index, array) 
              {
                arrayrejasact[contrejasact].aarg_reja_estado = $("#areja_estado"+item.id+"").val();
                arrayrejasact[contrejasact].aarg_reja_inte = $("#areja_inte"+item.id+"").val();
                arrayrejasact[contrejasact].aarg_reja_tipo = $("#areja_tipo"+item.id+"").val();
                arrayrejasact[contrejasact].aarg_reja_mat = $("#areja_mat"+item.id+"").val();
                contrejasact++;         
              });
          var abloqueFiguraProtecdr= '[{"tipo":"GRD","campos":"reja_estado|reja_inte|reja_tipo|reja_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
          var agrillaFiguradr = '';   
          for (var i = 0; i<arrDibRejas.length;i++)
          {
          agrillaFiguradr = '{"reja_estado":"'+ arrayrejasact[i].aarg_reja_estado +'","reja_inte":"'+ arrayrejasact[i].aarg_reja_inte +'","reja_tipo":"'+ arrayrejasact[i].aarg_reja_tipo +'","reja_mat":"'+ arrayrejasact[i].aarg_reja_mat +'","reja_estado_valor": "'+ arrayrejasact[i].aarg_reja_estado +'","reja_inte_valor": "'+ arrayrejasact[i].aarg_reja_inte +'","reja_tipo_valor": "'+ arrayrejasact[i].aarg_reja_tipo +'","reja_mat_valor": "'+ arrayrejasact[i].aarg_reja_mat +'"}';
          abloqueFiguraProtecdr = (abloqueFiguraProtecdr.concat(',').concat(agrillaFiguradr));
          }
          abloqueFiguraProtecdr = abloqueFiguraProtecdr.concat(']');
          var arejas_grila = abloqueFiguraProtecdr;

         //------actualizar pinturas muros exteriores------
         var contPMEact=0;
          arrDibujoPMExact.forEach(function (item, index, array) 
          {
            arrayPMExtactualizar[contPMEact].aarg_estado = $("#aestado"+item.id+"").val();
            arrayPMExtactualizar[contPMEact].aarg_integridad = $("#aintegridad"+item.id+"").val();
            arrayPMExtactualizar[contPMEact].aarg_tipo = $("#atipo"+item.id+"").val();
            arrayPMExtactualizar[contPMEact].aarg_material = $("#amaterial"+item.id+"").val();
            contPMEact++;

          });
          var abloqueFiguraProtec = '[{"tipo":"GRD","campos":"pint_ext_estado|pint_ext_inte|pint_ext_tipo|pint_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
          for (var i = 0; i<arrDibujoPMExact.length;i++)
          {
            var agrillaFigura = '{"pint_ext_estado":"'+arrayPMExtactualizar[i].aarg_estado+'","pint_ext_estado_valor":"'+arrayPMExtactualizar[i].aarg_estado+'","pint_ext_inte":"'+arrayPMExtactualizar[i].aarg_integridad+'","pint_ext_inte_valor":"'+arrayPMExtactualizar[i].aarg_integridad+'","pint_ext_tipo":"'+arrayPMExtactualizar[i].aarg_tipo+'","pint_ext_tipo_valor":"'+arrayPMExtactualizar[i].aarg_tipo+'","pint_ext_mat":"'+arrayPMExtactualizar[i].aarg_material+'","pint_ext_mat_valor":"'+arrayPMExtactualizar[i].aarg_material+'"}';
            abloqueFiguraProtec = (abloqueFiguraProtec.concat(',').concat(agrillaFigura));
          }
          abloqueFiguraProtec = abloqueFiguraProtec.concat(']');
          var agrillaMuroExt = abloqueFiguraProtec;
          //----actualizar pinturas muros interiores-------
          var contPMIactua=0;
          arrDibujoPMInact.forEach(function (item, index, array) 
          {
            arrayPMIntact[contPMIactua].aarg_estadoInt = $("#aestadoInt"+item.id+"").val();
            arrayPMIntact[contPMIactua].aarg_integridadInt = $("#aintegridadInt"+item.id+"").val();
            arrayPMIntact[contPMIactua].aarg_tipoInt = $("#atipoInt"+item.id+"").val();
            arrayPMIntact[contPMIactua].aarg_materialInt = $("#amaterialInt"+item.id+"").val();
            contPMIactua++;
          });
          var abloqueFiguraProtec = '[{"tipo":"GRD","campos":"pint_int_estado|pint_int_inte|pint_int_tipo|pint_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
          for (var i = 0; i<arrDibujoPMInact.length;i++)
          {
            var agrillaFigura = '{"pint_int_estado":"'+arrayPMIntact[i].aarg_estadoInt+'","pint_int_inte":"'+arrayPMIntact[i].aarg_integridadInt+'","pint_int_tipo":"'+arrayPMIntact[i].aarg_tipoInt+'","pint_int_mat":"'+arrayPMIntact[i].aarg_materialInt+'","pint_int_estado_valor":"'+arrayPMIntact[i].aarg_estadoInt+'","pint_int_inte_valor":"'+arrayPMIntact[i].aarg_integridadInt+'","pint_int_tipo_valor":"'+arrayPMIntact[i].aarg_tipoInt+'","pint_int_mat_valor":"'+arrayPMIntact[i].aarg_materialInt+'"}';
            abloqueFiguraProtec = (abloqueFiguraProtec.concat(',').concat(agrillaFigura));

          }
          abloqueFiguraProtec = abloqueFiguraProtec.concat(']');
          var agrillaMuroInt = abloqueFiguraProtec; 

         //-----actualizar acabados de muros exteriores-------------------------
         var cont=0;
          arrDibujoAMExAct.forEach(function (item, index, array) 
          {
            aaca_extCodigo[cont].aarg_estadoAcaExt = $("#aestadoAcaExt"+item.id+"").val();
            aaca_extCodigo[cont].aarg_integridadAcaExt = $("#aintegridadAcaExt"+item.id+"").val();
            aaca_extCodigo[cont].aarg_tipoAcaExt = $("#atipoAcaExt"+item.id+"").val();
            aaca_extCodigo[cont].aarg_materialAcaExt = $("#amaterialAcaExt"+item.id+"").val();
            cont++;
          });
          var abloqueFiguraProtec = '[{"tipo":"GRD","campos":"aca_ext_estado|aca_ext_inte|aca_ext_tipo|aca_ext_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
          for (var i = 0; i<arrDibujoAMExAct.length;i++)
          {
          var agrillaFigura = '{"aca_ext_estado":"'+aaca_extCodigo[i].aarg_estadoAcaExt+'","aca_ext_inte":"'+aaca_extCodigo[i].aarg_integridadAcaExt+'","aca_ext_tipo":"'+aaca_extCodigo[i].aarg_tipoAcaExt+'","aca_ext_mat":"'+aaca_extCodigo[i].aarg_materialAcaExt+'", "aca_ext_estado_valor": "'+aaca_extCodigo[i].aarg_estadoAcaExt+'","aca_ext_inte_valor": "'+aaca_extCodigo[i].aarg_integridadAcaExt+'","aca_ext_tipo_valor": "'+aaca_extCodigo[i].aarg_tipoAcaExt+'","aca_ext_mat_valor": "'+aaca_extCodigo[i].aarg_materialAcaExt+'"}';
          abloqueFiguraProtec = (abloqueFiguraProtec.concat(',').concat(agrillaFigura));
        }
        abloqueFiguraProtec = abloqueFiguraProtec.concat(']');
        var agrillaAcabadoEx = abloqueFiguraProtec;
        
        //------actualizar acabados muros interiores--------
      var contAMIntAct=0;
      arrDibujoAMIact.forEach(function (item, index, array) 
      {
        aaca_intCodigo[contAMIntAct].aarg_estadoAcaInt = $("#aestadoAcaInt"+item.id+"").val();
        aaca_intCodigo[contAMIntAct].aarg_integridadAcaInt = $("#aintegridadAcaInt"+item.id+"").val();
        aaca_intCodigo[contAMIntAct].aarg_tipoAcaInt = $("#atipoAcaInt"+item.id+"").val();
        aaca_intCodigo[contAMIntAct].aarg_materialAcaInt = $("#amaterialAcaInt"+item.id+"").val();
        contAMIntAct++;

      });
      var abloqueFiguraProtec = '[{"tipo":"GRD","campos":"aca_int_estado|aca_int_inte|aca_int_tipo|aca_int_mat","titulos":"Estado de Conservación|Integridad|Tipo|Materiales"}';
      for (var i = 0; i<arrDibujoAMIact.length;i++)
      {
      var agrillaFigura = '{"aca_int_estado":"'+aaca_intCodigo[i].aarg_estadoAcaInt+'","aca_int_inte":"'+aaca_intCodigo[i].aarg_integridadAcaInt+'","aca_int_tipo":"'+aaca_intCodigo[i].aarg_tipoAcaInt+'","aca_int_mat":"'+aaca_intCodigo[i].aarg_materialAcaInt+'","aca_int_estado_valor": "'+aaca_intCodigo[i].aarg_estadoAcaInt+'","aca_int_inte_valor": "'+aaca_intCodigo[i].aarg_integridadAcaInt+'","aca_int_tipo_valor": "'+aaca_intCodigo[i].aarg_tipoAcaInt+'","aca_int_mat_valor": "'+aaca_intCodigo[i].aarg_materialAcaInt+'"}';
      abloqueFiguraProtec = (abloqueFiguraProtec.concat(',').concat(agrillaFigura));
      }
      abloqueFiguraProtec = abloqueFiguraProtec.concat(']');
      var agrillaAcabadoInt = abloqueFiguraProtec; 

      //-----actualizar elementos artísticos compositivos---
       var contECAactual=0;
       arrDibujoECAact.forEach(function (item, index, array) 
            {
              aarrCodigoAR[contECAactual].aartisticosC1 = $("#aartisticosC1"+item.id+"").val();
              aarrCodigoAR[contECAactual].aartisticosC2 = $("#aartisticosC2"+item.id+"").val();
              aarrCodigoAR[contECAactual].aartisticosC3 = $("#aartisticosC3"+item.id+"").val();
              contECAactual++;  
            });
        var abloqueFiguraProtecAR = '[{"tipo":"GRD","campos":"elem_art_estado|elem_art_inte|elem_art_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
        for (var i = 0; i<arrDibujoECAact.length;i++)
        {
        var agrillaFiguraAR = '{"elem_art_estado":"'+aarrCodigoAR[i].aartisticosC1+'", "elem_art_estado_valor":"'+aarrCodigoAR[i].aartisticosC1+'", "elem_art_inte":"'+aarrCodigoAR[i].aartisticosC2+'", "elem_art_inte_valor":"'+aarrCodigoAR[i].aartisticosC2+'", "elem_art_tipo":"'+aarrCodigoAR[i].aartisticosC3+'", "elem_art_tipo_valor":"'+aarrCodigoAR[i].aartisticosC3+'"}';
        abloqueFiguraProtecAR = (abloqueFiguraProtecAR.concat(',').concat(agrillaFiguraAR));
        }
          abloqueFiguraProtecAR = abloqueFiguraProtecAR.concat(']');
          var agrillaFiguraProtecAR = abloqueFiguraProtecAR;
           //----actualizar elementos ornamentales------
         var cont=0;
         arrDibujoEOact.forEach(function (item, index, array) 
              {
                aarrCodigoOR[cont].aornamentales1 = $("#aornamentales1"+item.id+"").val();
                aarrCodigoOR[cont].aornamentales2 = $("#aornamentales2"+item.id+"").val();
                aarrCodigoOR[cont].aornamentales3 = $("#aornamentales3"+item.id+"").val();
                cont++;         
              });
          var abloqueFiguraProtecOR = '[{"tipo":"GRD","campos":"elem_orn_estado|elem_orn_inte|elem_orn_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
          for (var i = 0; i<arrDibujoEOact.length;i++)
          {
        
          var agrillaFiguraOR = '{"elem_orn_estado":"'+aarrCodigoOR[i].aornamentales1+'", "elem_orn_estado_valor":"'+aarrCodigoOR[i].aornamentales1+'", "elem_orn_inte":"'+aarrCodigoOR[i].aornamentales2+'", "elem_orn_inte_valor":"'+aarrCodigoOR[i].aornamentales2+'", "elem_orn_tipo":"'+aarrCodigoOR[i].aornamentales3+'", "elem_orn_tipo_valor":"'+aarrCodigoOR[i].aornamentales3+'"}';
          abloqueFiguraProtecOR = (abloqueFiguraProtecOR.concat(',').concat(agrillaFiguraOR));   
          }
          abloqueFiguraProtecOR = abloqueFiguraProtecOR.concat(']');
          var agrillaFiguraProtecOR = abloqueFiguraProtecOR;
      
        //--------actualizar elementos tipologicos compositivos -----------
          var contETCactual=0;
          arrDibujoETComAct.forEach(function (item, index, array) 
          {
            aarrCodigoTIC[contETCactual].atipologicos1 = $("#atipologicos1"+item.id+"").val();
            aarrCodigoTIC[contETCactual].atipologicos2 = $("#atipologicos2"+item.id+"").val();
            aarrCodigoTIC[contETCactual].atipologicos3 = $("#atipologicos3"+item.id+"").val();
            contETCactual++;
           
          });
      
          var abloqueFiguraProtecTIC = '[{"tipo":"GRD","campos":"elem_art_estado|elem_art_inte|elem_art_tipo","titulos":"Estado de Conservación|Integridad|Tipo"}';
          for (var i = 0; i<arrDibujoETComAct.length;i++)
          {
        
          var agrillaFiguraTIC = '{"elem_art_estado":"'+aarrCodigoTIC[i].atipologicos1+'", "elem_art_estado_valor":"'+aarrCodigoTIC[i].atipologicos1+'", "elem_art_inte":"'+aarrCodigoTIC[i].atipologicos2+'", "elem_art_inte_valor":"'+aarrCodigoTIC[i].atipologicos2+'", "elem_art_tipo":"'+aarrCodigoTIC[i].atipologicos3+'", "elem_art_tipo_valor":"'+aarrCodigoTIC[i].atipologicos3+'"}';

          abloqueFiguraProtecTIC = (abloqueFiguraProtecTIC.concat(',').concat(agrillaFiguraTIC));     
          }
          abloqueFiguraProtecTIC = abloqueFiguraProtecTIC.concat(']');
          var agrillaFiguraProtecTIC = abloqueFiguraProtecTIC;

        //---------actualizar hogar--------
         var cont=0;
         arrDibujoHogAct.forEach(function (item, index, array) 
          {
            aarrCodigoHogar[cont].ahogar1 = $("#ahogar1"+item.id+"").val();
            aarrCodigoHogar[cont].ahogar2 = $("#ahogar2"+item.id+"").val();
            aarrCodigoHogar[cont].ahogar3 = document.getElementById("ahogar3"+item.id+"").checked;
            aarrCodigoHogar[cont].ahogar4 = document.getElementById("ahogar4"+item.id+"").checked;
            aarrCodigoHogar[cont].ahogar5 = $("#ahogar5"+item.id+"").val();
            aarrCodigoHogar[cont].ahogar6 = $("#ahogar6"+item.id+"").val();
            cont++;         
          });
          var abloqueFiguraProtecHogar = '[{"tipo":"GRD","campos":"hog_desc|edad_hog|hom_hog|muj_hog|ten_hog|der_hog","titulos":"Hogar Des.|Edad|Hombre|Mujer|Tendencia|Derecho Propietario"}';
          for (var i = 0; i<arrDibujoHogAct.length;i++)
          {
          var agrillaFiguraHogar = '{"hog_desc":"'+aarrCodigoHogar[i].ahogar1+'","edad_hog":"'+aarrCodigoHogar[i].ahogar2+'", "edad_hog_valor":"'+aarrCodigoHogar[i].ahogar2+'","hom_hog":"'+ cambiarDatosBool(aarrCodigoHogar[i].ahogar3)+'","muj_hog":"'+ cambiarDatosBool(aarrCodigoHogar[i].ahogar4)+'","ten_hog":"'+aarrCodigoHogar[i].ahogar5+'","ten_hog_valor":"'+aarrCodigoHogar[i].ahogar5+'","der_hog":"'+aarrCodigoHogar[i].ahogar6+'","der_hog_valor":"'+aarrCodigoHogar[i].ahogar6+'"}';
          abloqueFiguraProtecHogar = (abloqueFiguraProtecHogar.concat(',').concat(agrillaFiguraHogar));    
          }
          abloqueFiguraProtecHogar = abloqueFiguraProtecHogar.concat(']');
          var agrillaFiguraProtecHogar = abloqueFiguraProtecHogar;

  var cas_data_in ='{"g_tipo":"'+g_tipo+'","PTR_CREA":"'+acreador_in+'","PTR_FEC_CREA":"'+afecha_creador_in+'","PTR_ACTUALIZA":"'+amodificador_in+'","PTR_FEC_ACT":"'+afecha_modificador_in+'","PTR_DEP":"'+adepartamento_in+'","PTR_CIU":"'+aciudad_in+'","PTR_MUNI":"'+amunicipio_in+'","PTR_COD_CAT":"'+acodCatastral_in+'","PTR_COD_CAT_HIS1":"'+acodCatastral1_in+'","PTR_OR":"'+aoriginal_in+'","PTR_TRAD":"'+atradicional_in1+'","PTR_ACT":"'+aActual_in+'","PTR_MACRO":"'+aMacrodistrito_in+'","PTR_TIP_VIAS":"'+atipoViaLocalizacion_in+'","PTR_CALL_ESQ":"'+acalleEsquina_in+'","PTR_DIRE":"'+adireccion_in+'","PTR_ZONA":"'+azona_in+'","PTR_NUM1":"'+anumero_in+'","PTR_NUM2":"'+anumero_in2+'","PTR_NUM3":"'+anumero_in3+'","PTR_NUM4":"'+anumero_in4+'","PTR_NUM5":"'+anumero_in5+'","PTR_ARE_PRED":"'+aareaPredio_in+'","PTR_ANCH_FREN":"'+aanchoFrente_in+'","PTR_FOND_PRED":"'+afondoPredio_in+'","PTR_ANCH_MUR":"'+aanchoMuro_in+'","PTR_ALT_PROM":"'+aalturaInterior_in+'","PTR_ALT_FACH":"'+aalturaFachada_in+'","PTR_ALT_EDIF":"'+aalturaMaxima_in+'","PTR_USO_TRAD":'+atradicional_check+',"PTR_USO_ACT":'+aactual_check+',"PTR_USO_TEN":'+atendencia_check+',"PTR_DES_PRED":"'+adescripcion_in+'","PTR_UBIC_MAN":"'+aubicacionManzana_in+'","PTR_LIN_CONS":"'+alineaConstruccion_in+'","PTR_TRAZ_MAN":"'+atrazoManzana_in+'","PTR_TIP_ARQ":"'+atipologiaArqui_in+'","PTR_AER_EDIF":"'+aareaEdificable_in+'","PTR_MAT_VIA":"'+amaterialVia_in+'","PTR_TIP_VIA":"'+atipoVia_in+'","PTR_BLOQ_INV":"'+abloqueInventario_in+'","PTR_BLOQ_REAL":"'+abloquesReales_in+'","PTR_HOG_INV":"'+ahogaresInventario_in+'","PTR_HOG_REAL":"'+ahogaresReales_in+'","PTR_EST_GEN":"'+aestiloGeneral_in+'","PTR_RAN_EPO":"'+arangoEpoca_in+'","PTR_EST_CON":"'+aestadoConserva_in+'","PTR_FECH_CONS":"'+afechaConstruccion_in+'","PTR_AUT_CON":"'+aautor_in+'","PTR_PREEX_EDIF":"'+apreexistencia_in+'","PTR_PROP_ANT":"'+apropietarios_in+'","PTR_USO_ORG":"'+ausoOriginal_in+'","PTR_HEC_HIST":"'+ahechoHistorico_in+'","PTR_FIE_REL":"'+afiestaReligiosa_in+'","PTR_DAT_HIST_CONJ":"'+adatoHistorico_in+'","PTR_FUENT_DOC":"'+afuente_in+'","PTR_FEC_CONST_DOC":"'+afechaConstruccion_in3+'","PTR_AUT_CONS":"'+aautor_in3+'","PTR_PREEX_INTER":"'+apreexistencia_in3+'","PTR_PROP_ANT_DOC":"'+apropietarios_in3+'","PTR_USOS_ORG_DOC":"'+ausoOriginal_in3+'","PTR_HEC_HIS_DOC":"'+ahechoHistorico_in3+'","PTR_FIE_REL_DOC":"'+afiestaReligiosa_in3+'","PTR_DAT_HIS_DOC":"'+adatoHistorico_in3+'","PTR_FUENTE_DOC":"'+afuente_in3+'","PTR_INSCRIP":"'+ainscripcion_in+'","PTR_EXIST":"'+aexistePatrimonio_in+'","PTR_EXIST_AR":"'+aexisteResto_in+'","PTR_EXIST_ELEM":"'+aexisteElemento_in+'","PTR_EXIST_RITO":"'+aritosMitos_in+'","PTR_CONJUNTO":"'+aconjunto_in+'","PTR_EST_VERF":"'+aestadoVerifica_in+'","PTR_VAL_CAT":"'+apuntuacion_in+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'","PTR_DES_BLOQ1":"'+PTR_DES_BLOQ1+'","PTR_RANG_EPO1":"'+PTR_RANG_EPO1+'","PTR_EST_BLOQ1":"'+PTR_EST_BLOQ1+'","PTR_EPO_EST1":'+PTR_EPO_EST1+',"PTR_SIS_CONST1":'+PTR_SIS_CONST1+',"PTR_SIS_CONSTRUC1":"'+PTR_SIS_CONSTRUC1+'","PTR_NRO_VIV1":"'+PTR_NRO_VIV1+'","PTR_NRO_HOG1":"'+PTR_NRO_HOG1+'","PTR_ACAP1":'+PTR_ACAP1+',"PTR_VAL_HIST1":'+PTR_VAL_HIST1+',"PTR_VAL_ART1":'+PTR_VAL_ART1+', "PTR_VAL_ARQ1":'+PTR_VAL_ARQ1+',"PTR_VAL_TEC1":'+PTR_VAL_TEC1+',"PTR_VAL_INTE1":'+PTR_VAL_INTE1+',"PTR_VAL_URB1":'+PTR_VAL_URB1+',"PTR_VALORES_1":'+PTR_VALORES_1+',"PTR_VAL_HIS_DESC1":"'+PTR_VAL_HIS_DESC1+'","PTR_VAL_ART_DESC1":"'+PTR_VAL_ART_DESC1+'","PTR_VAL_ARQ_DESC1":"'+PTR_VAL_ARQ_DESC1+'","PTR_VAL_TEC_DESC1":"'+PTR_VAL_TEC_DESC1+'","PTR_VAL_INTE_DESC1":"'+PTR_VAL_INTE_DESC1+'","PTR_VAL_URB_DESC1":"'+PTR_VAL_URB_DESC1+'","PTR_VAL_INMAT_DESC1":"'+PTR_VAL_INMAT_DESC1+'","PTR_VAL_SIMB_DESC1":"'+PTR_VAL_SIMB_DESC1+'","PTR_VAL_INT_DESC1":"'+PTR_VAL_INT_DESC1+'","PTR_VAL_EXT_DESC1":"'+PTR_VAL_EXT_DESC1+'","PTR_CATEGORIA1":"'+PTR_CATEGORIA1+'","PTR_PUNTO":"'+PTR_PUNTO+'","PTR_ID_BLOQ1":"'+PTR_ID_BLOQ1+'","PTR_UBIC_MANC":'+PTR_UBIC_MANC+',"PTR_LIN_CONSC":'+PTR_LIN_CONSC+',"PTR_TRAZ_MANC":'+PTR_TRAZ_MANC+',"PTR_TIP_ARQC":'+PTR_TIP_ARQC+',"PTR_AER_EDIFC":'+PTR_AER_EDIFC+',"PTR_FECH_CONSC":'+PTR_FECH_CONSC+',"PTR_AUT_CONS_C":'+PTR_AUT_CONS_C+',"PTR_FEC_CONST_C":'+PTR_FEC_CONST_C+',"PTR_AUT_CONSC":'+PTR_AUT_CONSC+',"PTR_EXIST_PAT":'+PTR_EXIST_PAT+',"PTR_EXIST_ARQ":'+PTR_EXIST_ARQ+',"PTR_EXIST_ELEMNAT":'+PTR_EXIST_ELEMNAT+',"PTR_EXIST_RITOS":'+PTR_EXIST_RITOS+',"PTR_REG_PROPI":'+agrillaRegimenPropietario+',"PTR_FIG_PROT":'+agrillaFiguraProtec+',"PTR_REF_HIST":'+agrillaRefHist+',"PTR_PEL_POT1":'+apeligro_potencial_grilla+',"PTR_PAT_EDIF1":'+apatologia_edif_grilla+',"PTR_INST_EST1":'+ainstalaciones_estado_grilla+',"PTR_SERVICIOS1":'+aservios_grilla+',"PTR_VAN_VENT":'+avan_venta_int+',"PTR_VENT_INT":'+aventana_interior+',"PTR_VAN_VENT_EXT":'+avan_vent_exterior+',"PTR_VENT_EXT":'+aventana_exterior+',"PTR_VAN_PUE_INT":'+avan_puert_int+',"PTR_VAN_PUE_EXT":'+avan_puert_ext+',"PTR_PUE_INT":'+apuerta_ineterior+',"PTR_PUE_EXT":'+apuerta_externa+',"PTR_REJA":'+arejas_grila+',"PTR_PINT_MUR_EXT":'+agrillaMuroExt+',"PTR_PINT_MUR_INT":'+agrillaMuroInt+',"PTR_ACA_MUR_EXT":'+agrillaAcabadoEx+',"PTR_ACA_MUR_INT":'+agrillaAcabadoInt+',"PTR_PAR_EXT":'+agrillaParedExterna+',"PTR_COB_TEC":'+agrillaCoberturaTecho+',"PTR_PAR_INT":'+agrillaParedInterior+',"PTR_ENT_PIS":'+agrillaEntrePisos+',"PTR_ESC":'+agrillaEscalera+',"PTR_CIE":'+agrillaCielo+',"PTR_BAL":'+agrillaBalcon+',"PTR_EST_CUB":'+agrillaEstructuraCubierta+',"PTR_PISO":'+agrillaPiso+',"PTR_ELEM_ART_COMP":'+agrillaFiguraProtecAR+',"PTR_ELEM_ORN_DEC":'+agrillaFiguraProtecOR+',"PTR_ELEM_TIP_COMP":'+agrillaFiguraProtecTIC+',"PTR_AG_HOGARES":'+agrillaFiguraProtecHogar+'}';
  //console.log("DATOS DEL JSONNNNNN",cas_data_in);

    var jdata_in = JSON.stringify(cas_data_in);
    var xcas_act_id = 137;
    var xcas_usr_actual_id = 1155;
    var xcas_estado_paso = 'Recibido';
    var xcas_nodo_id = 1;
    var xcas_usr_id = 1;
    var xcas_ws_id = 1;
    var xcas_asunto = 'asunto';
    var xcas_tipo_hr = 'hora';
    var xcas_id_padre = 1;
    var xcampo_f ='campo'; 

    var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_in+',"xcas_nro_caso":'+anroficha_in+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_in+',"xcas_nombre_caso":"'+acodPatrimonio_in+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
   
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: { 
       'authorization': 'Bearer' + token,
     },
     success: function(data){
      console.log("dataaaaa",data);
      $( "#cpuntuacion_in" ).val(0); 
      buscarPatrimonioPorTipo(1);
      swal( "Exíto..!", "Se actualizó correctamente el registro del Bienes Inmuebles...!", "success" );
      console.log("acasoId_in",acasoId_in);
      console.log("anroficha_in",anroficha_in);
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', acasoId_in);
        url = url.replace('numeroficha1', anroficha_in);
        location.href = url;
      }, 1000); 

    },
    error: function(result) {
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });         
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});  
});

function generarProforma(id_cas){
    setTimeout(function(){
     var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerar',['datos'=>'d1']) }}"; 
     urlPdf = urlPdf.replace('d1', id_cas);
     //console.log("urlPdf",urlPdf);
     window.open(urlPdf);
   }, 500);
  }
function imprimirArchivoFotografico(id_cas,nroFicha){
  console.log("id_cas",id_cas);
     setTimeout(function(){
    revisarImagenesDuplicadas(id_cas,nroFicha);
     /*var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerarArchFoto',['datos'=>'d1']) }}";   
     urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); */
   }, 500);
  }

function imprimirFichaTodo(id_cas){
     setTimeout(function(){
     var urlFicha = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     urlFicha = urlFicha.replace('d1', id_cas);
    ////console.log("urlFicha",urlFicha);
    window.open(urlFicha); 
  }, 500);
  }

function revisarImagenesDuplicadas(idCaso,numeroficha ){
   $.ajax(
  {
    type        : 'GET',
    url         : '/prueba/VALLE/public/v0.0/getToken',
    data        : '',
    success: function(token)
    {

      var formData = {"identificador": "SERVICIO_PATRIMONIO-944","parametros": '{"xid_doccasos":'+numeroficha+',"xg_tipo":"PTR_CBI"}'};
      
    console.log("formData",formData);
      $.ajax(
      {
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: 
        {
          'authorization': 'Bearer '+token,
        },
        success: function( dataIN )
        {           
         var tam = dataIN.length;
         var posicion = [5,6,7,8,9,10];
         var sw = 0;
         var cont = 0;
         for (var i = 0; i < posicion.length; i++){
           for (var j = 0; j < dataIN.length; j++){
            if(posicion[i] == parseInt(dataIN[j].xdoc_correlativo)){
              cont++;
              //console.log("la posicion"+posicion[i]+"se repite"+cont); 
            }
          }
          if(cont > 1){
            sw = 1;
            break; 
           }
          cont=0;
         }
         if(sw == 1){
          swal( "Revisar..!", "Hay posiciones duplicadas", "warning" );
         }
         else{
           var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienInmueble@getGenerarArchFoto',['datos'=>'d1']) }}";
           urlPdf = urlPdf.replace('d1', idCaso);
          window.open(urlPdf); 
         }
        },
        error: function( result ) 
        {
          console.log("Error al recuperar registro de Imagenes");
        }
      });

    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo recuperar los datos", "error" );
    },
  });
}


/*----------------------------------BEGIN GRILLAS------------------------------*/
//------------------------------BEGIN GRILLA FIGURA DE PROTECCION------------------------------------------//

var contCodigo=2;
var arrCodigo=[];
var arrDibujo=[];

htmlARG='Figura de Protección Legal:</label>'+
'<select class="form-control" id="tipoarg1" name="tipoarg1">'+
'<option value="N" selected="selected">--Seleccione--</option>'+
'<option value="Nivel Internacional">Nivel Internacional</option>'+
'<option value="Nivel Municipal">Nivel Municipal</option>'+
'<option value="Nivel Nacional">Nivel Nacional</option>'+
'<option value="Nivel Regional">Nivel Regional</option>'+
'<option value="Con Ordenanza General o de Conjunto">Con Ordenanza General o de Conjunto</option>'+
'<option value="Con instrumento de protección general o de conjunto">Con instrumento de protección general o de conjunto</option>'+
'<option value="Sin declaratoria específicafica">Sin declaratoria específica</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-7">'+
'<label class="control-label">Observaciones:</label>'+
'<input name="textarea" id="valor1" rows="5" cols="55" class="form-control "></input>'+
'</div><div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
'</a></div>';
var cadena = '';  
arrCodigo.push({arg_valor:'', arg_tipo:'N'});
arrDibujo.push({scd_codigo:htmlARG,id: 1});
////console.log("arrCodigo",arrCodigo);

cadena='<div class="form-group col-md-4"><label class="control-label">1. '+arrDibujo[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#argumentopintarCrear').html(cadena);
    $("#valor1").val(arrCodigo[0].arg_valor);
    $("#tipoarg1").val(arrCodigo[0].arg_tipo);



    function crearArgumento(){
      var htmlARG;
      htmlARG='Figura de Protección Legal:</label>'+
'<select class="form-control"id="tipoarg'+contCodigo+'" name="tipoarg'+contCodigo+'">'+
'<option value="N" selected="selected">--Seleccione--</option>'+
'<option value="Nivel Internacional">Nivel Internacional</option>'+
'<option value="Nivel Municipal">Nivel Municipal</option>'+
'<option value="Nivel Nacional">Nivel Nacional</option>'+
'<option value="Nivel Regional">Nivel Regional</option>'+
'<option value="Con Ordenanza General o de Conjunto">Con Ordenanza General o de Conjunto</option>'+
'<option value="Con instrumento de protección general o de conjunto">Con instrumento de protección general o de conjunto</option>'+
'<option value="Sin declaratoria específicafica">Sin declaratoria específica</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-7">'+
'<label class="control-label">Observaciones:</label>'+
'<input name="textarea" id="valor'+contCodigo+'" name="valor'+contCodigo+'" rows="5" cols="55" class="form-control "></input>'+
'</div><div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea('+contCodigo+');"></i>'+
'</a></div>';
      var cadena='';
      var cont=0;
      arrDibujo.forEach(function (item, index, array) 
      {
        arrCodigo[cont].arg_valor = $("#valor"+item.id+"").val();
        arrCodigo[cont].arg_tipo = $("#tipoarg"+item.id+"").val();
        cont++;
      });
      arrCodigo.push({arg_valor:'', arg_tipo:'N'});
      arrDibujo.push({scd_codigo:htmlARG,id: contCodigo});
      contCodigo++;
      cont=1;
      for (var i=0; i<arrDibujo.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+cont+'. '+arrDibujo[i].scd_codigo;
        cont++;
      }
        //////console.log("cadenaaaa",cadena);
        $('#argumentopintarCrear').html(cadena); 
        var conto=0;
        arrDibujo.forEach(function (item, index, array) 
        { 
          $("#valor"+item.id+"").val(arrCodigo[conto].arg_valor);
          $("#tipoarg"+item.id+"").val(arrCodigo[conto].arg_tipo);
          conto=conto+1;
        });
        
      }


      function menosLinea($id){
        if(contCodigo>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujo.forEach(function (item, index, array) 
            {
              arrCodigo[cont].arg_valor=$("#valor"+item.id+"").val();
              arrCodigo[cont].arg_tipo=$("#tipoarg"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujo.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrCodigo.splice(pos,1);
            arrDibujo.splice(pos,1);
            ////console.log("arrCodigo",arrCodigo);

            cont=1;
            for (var i=0; i<arrDibujo.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+cont+arrDibujo[i].scd_codigo;
              cont++;
            }

            $('#argumentopintarCrear').html(cadena);

            cont=0;
            arrDibujo.forEach(function (item, index, array) 
            {
              $("#valor"+item.id+"").val(arrCodigo[cont].arg_valor);
                    $("#tipoarg"+item.id+"").val(arrCodigo[cont].arg_tipo);//document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
                    cont=cont+1;
                  });
          });
        }
        ////console.log("arrCodigoFinal",arrCodigo);
      }
     var arrDibujoA=[];
     var arrCodigoA=[];
     var contCodigoAct = 0; 

    function dibujoFiguraProteccion(longitudGrilla){
      var contCodigoA=1;
      arrDibujoA=[];
      var htmlARGA;

      for (var i=1; i<longitudGrilla;i++)
            {
          htmlARGA='Figura de Protección Legal:</label>'+
      '<select class="form-control"id="tipoargA'+contCodigoA+'">'+
      '<option value="N" selected="selected">--Seleccione--</option>'+
      '<option value="Nivel Internacional">Nivel Internacional</option>'+
      '<option value="Nivel Municipal">Nivel Municipal</option>'+
      '<option value="Nivel Nacional">Nivel Nacional</option>'+
      '<option value="Nivel Regional">Nivel Regional</option>'+
      '<option value="Con Ordenanza General o de Conjunto">Con Ordenanza General o de Conjunto</option>'+
      '<option value="Con instrumento de protección general o de conjunto">Con instrumento de protección general o de conjunto</option>'+
      '<option value="Sin declaratoria específicafica">Sin declaratoria específica</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-7">'+
      '<label class="control-label">Observaciones:</label>'+
      '<input name="textarea" id="valorA'+contCodigoA+'" rows="5" cols="55" class="form-control "></input>'+
      '</div><div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarFiguraProtec('+contCodigoA+');"></i>'+
      '</a></div>';
           arrDibujoA.push({scd_codigo:htmlARGA,id: contCodigoA});
            contCodigoA++;
            }
            var cadenaA='';
            var cont=1;
            for (var i=0; i<arrDibujoA.length;i++)
            {
              cadenaA=cadenaA + '<div class="form-group col-md-4"><label class="control-label">'+cont+'. '+arrDibujoA[i].scd_codigo;
              cont++;
            }
            ////////console.log("cadenaaaa",cadena);
            $('#afiguraProteccion').html(cadenaA);
      }
  
  
  function ActualizarGrillaFiguraProt(){

      var htmlARGA;
      htmlARGA = 'Figura de Protección Legal:</label>'+
      '<select class="form-control"id="tipoargA'+contCodigoAct+'" >'+
      '<option value="N" selected="selected">--Seleccione--</option>'+
      '<option value="Nivel Internacional">Nivel Internacional</option>'+
      '<option value="Nivel Municipal">Nivel Municipal</option>'+
      '<option value="Nivel Nacional">Nivel Nacional</option>'+
      '<option value="Nivel Regional">Nivel Regional</option>'+
      '<option value="Con Ordenanza General o de Conjunto">Con Ordenanza General o de Conjunto</option>'+
      '<option value="Con instrumento de protección general o de conjunto">Con instrumento de protección general o de conjunto</option>'+
      '<option value="Sin declaratoria específicafica">Sin declaratoria específica</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-7">'+
      '<label class="control-label">Observaciones:</label>'+
      '<input name="textarea" id="valorA'+contCodigoAct+'" rows="5" cols="55" class="form-control "></input>'+
      '</div><div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarFiguraProtec('+contCodigoAct+');"></i>'+
      '</a></div>';

      var cadenaA = '';
      var cont = 0;
      arrDibujoA.forEach(function (item, index, array) 
      {
        arrCodigoA[cont].argtipo = $("#tipoargA"+item.id+"").val();
        arrCodigoA[cont].argvalor = $("#valorA"+item.id+"").val();
       
        cont++;
      });

      arrCodigoA.push({argvalor:'', argtipo:'N'});
      arrDibujoA.push({scd_codigo:htmlARGA,id: contCodigoAct});
      contCodigoAct++;
      cont=1;
      for (var i=0; i<arrDibujoA.length;i++)
      {
        cadenaA = cadenaA + '<div class="form-group col-md-4"><label class="control-label">'+cont+'. '+arrDibujoA[i].scd_codigo;
        cont++;
      }
      ////console.log("arrDibujoActualizar",arrDibujoA);
        $('#afiguraProteccion').html(cadenaA); 
        var conto=0;
        arrDibujoA.forEach(function (item, index, array) 
        { 
          $("#valorA"+item.id+"").val(arrCodigoA[conto].argvalor);
          $("#tipoargA"+item.id+"").val(arrCodigoA[conto].argtipo);
          conto=conto+1;
        });
        ////console.log("arrCodigoActualizar",arrCodigoA);
     
     }

      function eliminarFiguraProtec($id){
        if(contCodigoAct>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaA='';
            var cont=0;
            var pos=-1;
            arrDibujoA.forEach(function (item, index, array) 
            {
               arrCodigoA[cont].argtipo = $("#tipoargA"+item.id+"").val();
               arrCodigoA[cont].argvalor = $("#valorA"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoA.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrCodigoA.splice(pos,1);
            arrDibujoA.splice(pos,1);
            ////console.log("arrCodigo",arrCodigo);

            cont=1;
            for (var i=0; i<arrDibujoA.length;i++)
            {
              cadenaA = cadenaA + '<div class="form-group col-md-4"><label class="control-label">'+cont+arrDibujoA[i].scd_codigo;
              cont++;
            }

            $('#afiguraProteccion').html(cadenaA);

            cont=0;
            arrDibujoA.forEach(function (item, index, array) 
            {
               $("#valorA"+item.id+"").val(arrCodigoA[cont].argvalor);
               $("#tipoargA"+item.id+"").val(arrCodigoA[cont].argtipo);
               cont=cont+1;
                  });
          });
        }
        ////console.log("arrCodigoFinal",arrCodigo);
      }
//------------------------ FIN GRILLA FIGURA DE PROTECCION ------------------------//

//--------------------------- Inicio Grilla 1 --------------------------------------------------------------------
var contCodigoRep=2;
var reg_proCodigo=[];
var reg_proDibujo=[];

htmlREP='Nombre Propietario:</label>'+
'<div class="col-md-3">'+
' <input id="cnombrePropietario_in1" type="text" class="form-control"  placeholder="Descripcion nombre"> '+
'</div>'+
'<label class="col-md-2" for="">Tipo de propiedad:</label>'+
'<div class="col-md-3">'+
'<select class="form-control" id="tipoPropiedad_in1" >'+
'<option value="N" selected="selected">Seleccionar...</option>'+
'<option value="Privado Institucional">Privado Institucional</option>'+ 
'<option value="Privado Particular">Privado Particular</option>'+
'<option value="Privado Religioso">Privado Religioso</option>'+
'<option value="Privado Estatal">Privado Estatal</option>'+  
'<option value="Publico Municipal">Publico Municipal</option>'+  
'</select> '+
'</div><div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMe(1);"></i>'+
'</a></div></div>';
var cadena = '';  
reg_proCodigo.push({arg_propietario:'', arg_tipoP:'N'});
reg_proDibujo.push({scd_codigo:htmlREP,id: 1});

cadena='<div class="form-group"><label class="col-md-3">1. '+reg_proDibujo[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#regimenPropietario').html(cadena);
    $("#cnombrePropietario_in1").val(reg_proCodigo[0].arg_propietario);
    $("#tipoPropiedad_in1").val(reg_proCodigo[0].arg_tipoP);
    

function crearRegimenPropiedad(){
      var htmlREP;
      htmlREP='Nombre Propietario:</label>'+
      '<div class="col-md-3">'+
      ' <input id="cnombrePropietario_in'+contCodigoRep+'" type="text" class="form-control"  placeholder="Descripcion nombre"> '+
      '</div>'+
      '<label class="col-md-2" for="">Tipo de propiedad:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="tipoPropiedad_in'+contCodigoRep+'" >'+
      '<option value="N" selected="selected">Seleccionar...</option>'+
      '<option value="Privado Institucional">Privado Institucional</option>'+ 
      '<option value="Privado Particular">Privado Particular</option>'+
      '<option value="Privado Religioso">Privado Religioso</option>'+
      '<option value="Privado Estatal">Privado Estatal</option>'+  
      '<option value="Publico Municipal">Publico Municipal</option>'+  
      '</select> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMe('+contCodigoRep+');"></i>'+
      '</a></div></div>';
      var cadena='';
      var cont=0;
      reg_proDibujo.forEach(function (item, index, array) 
      {
        reg_proCodigo[cont].arg_propietario = $("#cnombrePropietario_in"+item.id+"").val();
        reg_proCodigo[cont].arg_tipoP = $("#tipoPropiedad_in"+item.id+"").val();
        cont++;
      });
      reg_proCodigo.push({arg_propietario:'', arg_tipoP:'N'});
      reg_proDibujo.push({scd_codigo:htmlREP,id: contCodigoRep});
      contCodigoRep++;
      cont=1;
      for (var i=0; i<reg_proDibujo.length;i++)
      {
        cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+'. '+reg_proDibujo[i].scd_codigo;
        cont++;
      }
      $('#regimenPropietario').html(cadena); 
      var conto=0;
      reg_proDibujo.forEach(function (item, index, array) 
      { 
        $("#cnombrePropietario_in"+item.id+"").val(reg_proCodigo[conto].arg_propietario);
        $("#tipoPropiedad_in"+item.id+"").val(reg_proCodigo[conto].arg_tipoP);
        conto=conto+1;
      });
      
    }


//------------------------------------------------------------------------------------------

    function menosLineaMe($id){
      if(contCodigoRep>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el registro seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          reg_proDibujo.forEach(function (item, index, array) 
          {
            reg_proCodigo[cont].arg_propietario=$("#cnombrePropietario_in"+item.id+"").val();
            reg_proCodigo[cont].arg_tipoP=$("#tipoPropiedad_in"+item.id+"").val();
            cont=cont+1;
          });
          pos = reg_proDibujo.map(function(e) {return e.id;}).indexOf($id);
          ////console.log("poos",pos);
          reg_proCodigo.splice(pos,1);
          reg_proDibujo.splice(pos,1);
          ////console.log("arrCodigo",arrCodigo);
          cont=1;
          for (var i=0; i<reg_proDibujo.length;i++)
          {
            cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+reg_proDibujo[i].scd_codigo;
            cont++;
          }
          
          $('#regimenPropietario').html(cadena);
          cont=0;
          reg_proDibujo.forEach(function (item, index, array) 
          {
            $("#cnombrePropietario_in"+item.id+"").val(reg_proCodigo[cont].arg_propietario);
              $("#tipoPropiedad_in"+item.id+"").val(reg_proCodigo[cont].arg_tipoP);//document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
        });
      }
    }

  var arrDibujoRP=[];
  var arrCodigoRP=[];
  var contCodigoRepA = 0;

  function editarRegimenPropiedad(longitudGrilla){
  var contRP=1;
  arrDibujoRP=[];
  var htmlRP;

  for (var i=1; i<longitudGrilla; i++)
        {
        htmlRP='Nombre Propietario:</label>'+
      '<div class="col-md-3">'+
      ' <input id="nombreprpRP'+contRP+'" type="text" class="form-control"  placeholder="Descripcion nombre"> '+
      '</div>'+
      '<label class="col-md-2" for="">Tipo de propiedad:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="tpropiedaRP'+contRP+'" >'+
      '<option value="N" selected="selected">Seleccionar...</option>'+
      '<option value="Privado Institucional">Privado Institucional</option>'+ 
      '<option value="Privado Particular">Privado Particular</option>'+
      '<option value="Privado Religioso">Privado Religioso</option>'+
      '<option value="Privado Estatal">Privado Estatal</option>'+  
      '<option value="Publico Municipal">Publico Municipal</option>'+  
      '</select> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarRegimenProp('+contRP+');"></i>'+
      '</a></div></div>';
       arrDibujoRP.push({scd_codigo:htmlRP,id: contRP});
        contRP++;
        }
        var cadenaRP='';
        var cont=1;
        for (var i=0; i<arrDibujoRP.length;i++)
        {
          cadenaRP=cadenaRP + '<br><div class="form-group"><label class="col-md-3">'+cont+'. '+arrDibujoRP[i].scd_codigo;
          cont++;
        }
        $('#aregimenPropietario').html(cadenaRP);
       }

  function actualizarGrillaRegimenProp(){
      var htmlREP;
      htmlREP='Nombre Propietario:</label>'+
      '<div class="col-md-3">'+
      '<input id="nombreprpRP'+contCodigoRepA+'" type="text" class="form-control"  placeholder="Descripcion nombre"> '+
      '</div>'+
      '<label class="col-md-2" for="">Tipo de propiedad:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="tpropiedaRP'+contCodigoRepA+'" >'+
      '<option value="N" selected="selected">Seleccionar...</option>'+
      '<option value="Privado Institucional">Privado Institucional</option>'+ 
      '<option value="Privado Particular">Privado Particular</option>'+
      '<option value="Privado Religioso">Privado Religioso</option>'+
      '<option value="Privado Estatal">Privado Estatal</option>'+  
      '<option value="Publico Municipal">Publico Municipal</option>'+  
      '</select> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarRegimenProp('+contCodigoRepA+');"></i>'+
      '</a></div></div>';
      var cadena='';
      var cont=0;
      arrDibujoRP.forEach(function (item, index, array) 
      {
        arrCodigoRP[cont].arg_propietario = $("#nombreprpRP"+item.id+"").val();
        arrCodigoRP[cont].arg_tipoP = $("#tpropiedaRP"+item.id+"").val();
        cont++;
      });
      arrCodigoRP.push({arg_propietario:'', arg_tipoP:'N'});
      arrDibujoRP.push({scd_codigo:htmlREP,id: contCodigoRepA});
      contCodigoRepA++;
      cont=1;
      for (var i=0; i<arrDibujoRP.length;i++)
      {
        cadena=cadena + '<br><div class="form-group"><label class="col-md-3">'+cont+'. '+arrDibujoRP[i].scd_codigo;
        cont++;
      }
      $('#aregimenPropietario').html(cadena); 
      var conto=0;
      arrDibujoRP.forEach(function (item, index, array) 
      { 
        $("#nombreprpRP"+item.id+"").val(arrCodigoRP[conto].arg_propietario);
        $("#tpropiedaRP"+item.id+"").val(arrCodigoRP[conto].arg_tipoP);
        conto=conto+1;
      });    
  }
  function eliminarRegimenProp($id){
    console.log("aui eliminar");
      if(contCodigoRepA>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el registro seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          arrDibujoRP.forEach(function (item, index, array) 
          {
            arrCodigoRP[cont].arg_propietario = $("#nombreprpRP"+item.id+"").val();
            arrCodigoRP[cont].arg_tipoP = $("#tpropiedaRP"+item.id+"").val();
            cont=cont+1;
          });
          pos = arrDibujoRP.map(function(e) {return e.id;}).indexOf($id);
          ////console.log("poos",pos);
          arrCodigoRP.splice(pos,1);
          arrDibujoRP.splice(pos,1);
          ////console.log("arrCodigo",arrCodigo);
          cont=1;
          for (var i=0; i<arrDibujoRP.length;i++)
          {
            cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+arrDibujoRP[i].scd_codigo;
            cont++;
          }
          
          $('#aregimenPropietario').html(cadena);
          cont=0;
          arrDibujoRP.forEach(function (item, index, array) 
          {
            $("#nombreprpRP"+item.id+"").val(arrCodigoRP[cont].arg_propietario);
            $("#tpropiedaRP"+item.id+"").val(arrCodigoRP[cont].arg_tipoP);
              cont=cont+1;
            });
          console.log("arrCodigoRP",arrCodigoRP);
        });
      }
    }
//--------------------------- Fin Grilla 1 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 2 --------------------------------------------------------------------
var contCodigoref=2;
var ref_hisCodigo=[];
var ref_hisDibujo=[];
htmlREF='Referencias Históricas:</label>'+
'<div class="col-md-3">'+
'<select class="form-control" id="tipoRefrencia1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Nivel Internacional">Nivel Internacional</option>'+ 
'<option value="Nivel Local"> Nivel Local</option>'+
'<option value="Nivel Nacional"> Nivel Nacional</option>'+
'<option value="Nivel Regional"> Nivel Regional</option>'+  
'</select> '+
'</div>'+
'<label class="col-md-2" for="">Observacion:</label>'+
'<div class="col-md-3">'+
' <input id="observacion1" type="text" class="form-control"  placeholder="Descripcion Observación"> '+
'</div><div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMi(1);"></i>'+
'</a></div></div>';
var cadena = '';  
ref_hisCodigo.push({arg_tipoRef:'N',arg_observacion:''});
ref_hisDibujo.push({scd_codigo:htmlREF,id: 1});

cadena='<div class="form-group"><label class="col-md-3">1. '+ref_hisDibujo[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#referenciaHistorica').html(cadena);
    $("#tipoRefrencia1").val(ref_hisCodigo[0].arg_tipoRef);
    $("#observacion1").val(ref_hisCodigo[0].arg_observacion);
    

    function crearReferenciaHistorica(){
      var htmlREF;
      htmlREF='Referencias Históricas:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="tipoRefrencia'+contCodigoref+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Nivel Internacional">Nivel Internacional</option>'+ 
      '<option value="Nivel Local"> Nivel Local</option>'+
      '<option value="Nivel Nacional"> Nivel Nacional</option>'+
      '<option value="Nivel Regional"> Nivel Regional</option>'+  
      '</select> '+
      '</div>'+
      '<label class="col-md-2" for="">Observacion:</label>'+
      '<div class="col-md-3">'+
      ' <input id="observacion'+contCodigoref+'" type="text" class="form-control"  placeholder="Descripcion Observación"> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMi('+contCodigoref+');"></i>'+
      '</a></div></div>';
      var cadena='';
      var cont=0;
      ref_hisDibujo.forEach(function (item, index, array) 
      {
        ref_hisCodigo[cont].arg_tipoRef = $("#tipoRefrencia"+item.id+"").val();
        ref_hisCodigo[cont].arg_observacion = $("#observacion"+item.id+"").val();
        cont++;
      });
      ref_hisCodigo.push({arg_tipoRef:'N',arg_observacion:''});
      ref_hisDibujo.push({scd_codigo:htmlREF,id: contCodigoref});
      contCodigoref++;
      cont=1;
      for (var i=0; i<ref_hisDibujo.length;i++)
      {
        cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+'. '+ref_hisDibujo[i].scd_codigo;
        cont++;
      }
      $('#referenciaHistorica').html(cadena); 
      var conto=0;
      ref_hisDibujo.forEach(function (item, index, array) 
      { 
        $("#tipoRefrencia"+item.id+"").val(ref_hisCodigo[conto].arg_tipoRef);
        $("#observacion"+item.id+"").val(ref_hisCodigo[conto].arg_observacion);
        conto=conto+1;
      });
      
    }

    function menosLineaMi($id){
      if(contCodigoref>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el registro seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          ref_hisDibujo.forEach(function (item, index, array) 
          {
            ref_hisCodigo[cont].arg_tipoRef=$("#tipoRefrencia"+item.id+"").val();
            ref_hisCodigo[cont].arg_observacion=$("#observacion"+item.id+"").val();
            cont=cont+1;
          });
          pos = ref_hisDibujo.map(function(e) {return e.id;}).indexOf($id);
          ////console.log("poos",pos);
          ref_hisCodigo.splice(pos,1);
          ref_hisDibujo.splice(pos,1);
          cont=1;
          for (var i=0; i<ref_hisDibujo.length;i++)
          {
            cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+ref_hisDibujo[i].scd_codigo;
            cont++;
          }
          
          $('#referenciaHistorica').html(cadena);
          
          cont=0;
          ref_hisDibujo.forEach(function (item, index, array) 
          {
            $("#tipoRefrencia"+item.id+"").val(ref_hisCodigo[cont].arg_tipoRef);
              $("#observacion"+item.id+"").val(ref_hisCodigo[cont].arg_observacion);//document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
        });
      }
    }

  var arrDibujoRH = [];
  var arrCodigoRH = [];
  var contCodigorefA= 0; 

  function editarReferenciaHistorica(longitudGrilla){
  var contRH = 1;
  arrDibujoRH = [];
  var htmlRHIS;
  for (var i=1; i<longitudGrilla;i++)
        {
      htmlRHIS='Referencias Históricas:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="treferenciaRH'+contRH+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Nivel Internacional">Nivel Internacional</option>'+ 
      '<option value="Nivel Local"> Nivel Local</option>'+
      '<option value="Nivel Nacional"> Nivel Nacional</option>'+
      '<option value="Nivel Regional"> Nivel Regional</option>'+  
      '</select> '+
      '</div>'+
      '<label class="col-md-2" for="">Observacion:</label>'+
      '<div class="col-md-3">'+
      ' <input id="observacionRH'+contRH+'" type="text" class="form-control"  placeholder="Descripcion Observación"> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarRefenciasHistoricas('+contRH+');"></i>'+
      '</a></div></div>';
       arrDibujoRH.push({scd_codigo:htmlRHIS,id: contRH});
        contRH++;
        }
        var cadenaRH = '';
        var cont=1;
        for (var i=0; i<arrDibujoRH.length;i++)
        {
          cadenaRH=cadenaRH + '<br><div class="form-group"><label class="col-md-3">'+cont+'. '+arrDibujoRH[i].scd_codigo;
          cont++;
        }
        $('#areferenciaHistorica2').html(cadenaRH);
    }

var contCodigorefA= 0;   
function actualizarGrillaReferenciaHist(){
   var htmlREF;
      htmlREF='Referencias Históricas:</label>'+
      '<div class="col-md-3">'+
      '<select class="form-control" id="treferenciaRH'+contCodigorefA+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Nivel Internacional">Nivel Internacional</option>'+ 
      '<option value="Nivel Local"> Nivel Local</option>'+
      '<option value="Nivel Nacional"> Nivel Nacional</option>'+
      '<option value="Nivel Regional"> Nivel Regional</option>'+  
      '</select> '+
      '</div>'+
      '<label class="col-md-2" for="">Observacion:</label>'+
      '<div class="col-md-3">'+
      ' <input id="observacionRH'+contCodigorefA+'" type="text" class="form-control"  placeholder="Descripcion Observación"> '+
      '</div><div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarRefenciasHistoricas('+contCodigorefA+');"></i>'+
      '</a></div></div>';
      var cadena='';
      var cont=0;
      arrDibujoRH.forEach(function (item, index, array) 
      {
        arrCodigoRH[cont].arg_tipoRef = $("#treferenciaRH"+item.id+"").val();
        arrCodigoRH[cont].arg_observacion = $("#observacionRH"+item.id+"").val();
        cont++;
      });
      arrCodigoRH.push({arg_tipoRef:'N',arg_observacion:''});
      arrDibujoRH.push({scd_codigo:htmlREF,id: contCodigorefA});
      contCodigorefA++;
      cont=1;
      for (var i=0; i<arrDibujoRH.length;i++)
      {
        cadena=cadena + '<br><div class="form-group"><label class="col-md-3">'+cont+'. '+arrDibujoRH[i].scd_codigo;
        cont++;
      }
      $('#areferenciaHistorica2').html(cadena); 
      var conto=0;
      arrDibujoRH.forEach(function (item, index, array) 
      { 
        $("#treferenciaRH"+item.id+"").val(arrCodigoRH[conto].arg_tipoRef);
        $("#observacionRH"+item.id+"").val(arrCodigoRH[conto].arg_observacion);
        conto=conto+1;
      });
}
 function eliminarRefenciasHistoricas($id){

      if(contCodigorefA>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el registro seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          arrDibujoRH.forEach(function (item, index, array) 
          {
            arrCodigoRH[cont].arg_tipoRef = $("#treferenciaRH"+item.id+"").val();
            arrCodigoRH[cont].arg_observacion = $("#observacionRH"+item.id+"").val();
            cont=cont+1;
          });
          pos = arrDibujoRH.map(function(e) {return e.id;}).indexOf($id);
          ////console.log("poos",pos);
          arrCodigoRH.splice(pos,1);
          arrDibujoRH.splice(pos,1);
          cont=1;
          for (var i=0; i<arrDibujoRH.length;i++)
          {
            cadena=cadena + '<br><div class="form-group"><label class="col-md-3">'+cont+arrDibujoRH[i].scd_codigo;
            cont++;
          }
          console.log("arrDibujoRH",arrDibujoRH);
          
          $('#areferenciaHistorica2').html(cadena);
          
          cont=0;
          arrDibujoRH.forEach(function (item, index, array) 
          {
            $("#treferenciaRH"+item.id+"").val(arrCodigoRH[cont].arg_tipoRef);
            $("#observacionRH"+item.id+"").val(arrCodigoRH[cont].arg_observacion);
              cont=cont+1;
            });
          
        });
      }
    }

//--------------------------- Fin Grilla 2 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 3 --------------------------------------------------------------------
var contCodigo1=2;
var muro_extCodigo=[];
var muro_extDibujo=[];
htmlMurExt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estado1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridad1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipo1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Policromia">Policromia</option>'+ 
'<option value="Bicromia"> Bicromia</option>'+
'<option value="Monocromia">Monocromia</option>'+
'<option value="Pintura Mural">Pintura Mural</option>'+
'<option value="Esgrafiado">Esgrafiado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="material1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Cal">Cal</option>'+ 
'<option value="Latex"> Latex</option>'+
'<option value="Ocre">Ocre</option>'+
'<option value="Oleo">Oleo</option>'+
'<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroExt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
muro_extCodigo.push({arg_estado:'N',arg_integridad:'N',arg_tipo:'N',arg_material:'N'});
muro_extDibujo.push({scd_codigo:htmlMurExt,id: 1});

cadena=muro_extDibujo[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#divacaPinturasMurosExteriores').html(cadena);
    $("#estado1").val(muro_extCodigo[0].arg_estado);
    $("#integridad1").val(muro_extCodigo[0].arg_integridad);
    $("#tipo1").val(muro_extCodigo[0].arg_tipo);
    $("#material1").val(muro_extCodigo[0].arg_material);
    

    function acaPinturasMurosExteriores(){
      var htmlMurExt;
      htmlMurExt=
      '<div class="col-md-4"><br>'+
      '<select class="form-control" id="estado'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Bueno">Bueno</option>'+ 
      '<option value="Regular"> Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-3"><br>'+
      '<select class="form-control" id="integridad'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Original">Original</option>'+ 
      '<option value="Nuevo"> Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2"><br>'+
      '<select class="form-control" id="tipo'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Policromia">Policromia</option>'+ 
      '<option value="Bicromia"> Bicromia</option>'+
      '<option value="Monocromia">Monocromia</option>'+
      '<option value="Pintura Mural">Pintura Mural</option>'+
      '<option value="Esgrafiado">Esgrafiado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2"><br>'+
      '<select class="form-control" id="material'+contCodigo1+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Cal">Cal</option>'+ 
      '<option value="Latex"> Latex</option>'+
      '<option value="Ocre">Ocre</option>'+
      '<option value="Oleo">Oleo</option>'+
      '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroExt('+contCodigo1+');"></i>'+
      '</a></div><br>';
      var cadena='';
      var cont=0;
      muro_extDibujo.forEach(function (item, index, array) 
      {
        muro_extCodigo[cont].arg_estado = $("#estado"+item.id+"").val();
        muro_extCodigo[cont].arg_integridad = $("#integridad"+item.id+"").val();
        muro_extCodigo[cont].arg_tipo = $("#tipo"+item.id+"").val();
        muro_extCodigo[cont].arg_material = $("#material"+item.id+"").val();

        cont++;
      });
      muro_extCodigo.push({arg_estado:'N',arg_integridad:'N',arg_tipo:'N',arg_material:'N'});
      muro_extDibujo.push({scd_codigo:htmlMurExt,id: contCodigo1});
      contCodigo1++;
      cont=1;
      for (var i=0; i<muro_extDibujo.length;i++)
      {
        cadena=cadena + muro_extDibujo[i].scd_codigo;
        cont++;
      }
      $('#divacaPinturasMurosExteriores').html(cadena); 
      var conto=0;
      muro_extDibujo.forEach(function (item, index, array) 
      { 
        $("#estado"+item.id+"").val(muro_extCodigo[conto].arg_estado);
        $("#integridad"+item.id+"").val(muro_extCodigo[conto].arg_integridad);
        $("#tipo"+item.id+"").val(muro_extCodigo[conto].arg_tipo);
        $("#material"+item.id+"").val(muro_extCodigo[conto].arg_material);
        conto=conto+1;
      });
    }

    function menosLineaMuroExt($id){
      if(contCodigo1>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el registro seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          muro_extDibujo.forEach(function (item, index, array) 
          {
            muro_extCodigo[cont].arg_estado=$("#estado"+item.id+"").val();
            muro_extCodigo[cont].arg_integridad=$("#integridad"+item.id+"").val();
            muro_extCodigo[cont].arg_tipo=$("#tipo"+item.id+"").val();
            muro_extCodigo[cont].arg_material=$("#material"+item.id+"").val();
            cont=cont+1;
          });
          pos = muro_extDibujo.map(function(e) {return e.id;}).indexOf($id);
          ////console.log("poos",pos);
          muro_extCodigo.splice(pos,1);
          muro_extDibujo.splice(pos,1);
          cont=1;
          for (var i=0; i<muro_extDibujo.length;i++)
          {
            cadena=cadena + muro_extDibujo[i].scd_codigo;
            cont++;
          }
          
          $('#divacaPinturasMurosExteriores').html(cadena);
          
          cont=0;
          muro_extDibujo.forEach(function (item, index, array) 
          {
            $("#estado"+item.id+"").val(muro_extCodigo[cont].arg_estado);
            $("#integridad"+item.id+"").val(muro_extCodigo[cont].arg_integridad);
            $("#tipo"+item.id+"").val(muro_extCodigo[cont].arg_tipo);
            $("#material"+item.id+"").val(muro_extCodigo[cont].arg_material);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
        });
      }
    }
//--------------------------- Fin Grilla 3 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 4 --------------------------------------------------------------------
var contCodigo2=2;
var muro_intCodigo=[];
var muro_intDibujo=[];
htmlMurInt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estadoInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridadInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipoInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Policromia">Policromia</option>'+ 
'<option value="Bicromia"> Bicromia</option>'+
'<option value="Monocromia">Monocromia</option>'+
'<option value="Pintura Mural">Pintura Mural</option>'+
'<option value="Esgrafiado">Esgrafiado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="materialInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Cal">Cal</option>'+ 
'<option value="Latex"> Latex</option>'+
'<option value="Ocre">Ocre</option>'+
'<option value="Oleo">Oleo</option>'+
'<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroInt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
muro_intCodigo.push({arg_estadoInt:'N',arg_integridadInt:'N',arg_tipoInt:'N',arg_materialInt:'N'});
muro_intDibujo.push({scd_codigo:htmlMurInt,id: 1});

cadena=muro_intDibujo[0].scd_codigo;
//////console.log("cadenaaaa",cadena);
$('#acaPinturasMurosInteriores').html(cadena);
$("#estadoInt1").val(muro_intCodigo[0].arg_estadoInt);
$("#integridadInt1").val(muro_intCodigo[0].arg_integridadInt);
$("#tipoInt1").val(muro_intCodigo[0].arg_tipoInt);
$("#materialInt1").val(muro_intCodigo[0].arg_materialInt);


function acaPinturasMurosInteriores(){
  var htmlMurInt;
  htmlMurInt=
  '<div class="col-md-4"><br>'+
  '<select class="form-control" id="estadoInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="integridadInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="tipoInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Policromia">Policromia</option>'+ 
  '<option value="Bicromia"> Bicromia</option>'+
  '<option value="Monocromia">Monocromia</option>'+
  '<option value="Pintura Mural">Pintura Mural</option>'+
  '<option value="Esgrafiado">Esgrafiado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="materialInt'+contCodigo2+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Cal">Cal</option>'+ 
  '<option value="Latex"> Latex</option>'+
  '<option value="Ocre">Ocre</option>'+
  '<option value="Oleo">Oleo</option>'+
  '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaMuroInt('+contCodigo2+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  muro_intDibujo.forEach(function (item, index, array) 
  {
    muro_intCodigo[cont].arg_estadoInt = $("#estadoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_integridadInt = $("#integridadInt"+item.id+"").val();
    muro_intCodigo[cont].arg_tipoInt = $("#tipoInt"+item.id+"").val();
    muro_intCodigo[cont].arg_materialInt = $("#materialInt"+item.id+"").val();

    cont++;
  });
  muro_intCodigo.push({arg_estadoInt:'N',arg_integridadInt:'N',arg_tipoInt:'N',arg_materialInt:'N'});
  muro_intDibujo.push({scd_codigo:htmlMurInt,id: contCodigo2});
  contCodigo2++;
  cont=1;
  for (var i=0; i<muro_intDibujo.length;i++)
  {
    cadena=cadena + muro_intDibujo[i].scd_codigo;
    cont++;
  }
  $('#acaPinturasMurosInteriores').html(cadena); 
  var conto=0;
  muro_intDibujo.forEach(function (item, index, array) 
  { 
    $("#estadoInt"+item.id+"").val(muro_intCodigo[conto].arg_estadoInt);
    $("#integridadInt"+item.id+"").val(muro_intCodigo[conto].arg_integridadInt);
    $("#tipoInt"+item.id+"").val(muro_intCodigo[conto].arg_tipoInt);
    $("#materialInt"+item.id+"").val(muro_intCodigo[conto].arg_materialInt);
    conto=conto+1;
  });
}

function menosLineaMuroInt($id){
  if(contCodigo2>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el registro seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      muro_intDibujo.forEach(function (item, index, array) 
      {
        muro_intCodigo[cont].arg_estadoInt=$("#estadoInt"+item.id+"").val();
        muro_intCodigo[cont].arg_integridadInt=$("#integridadInt"+item.id+"").val();
        muro_intCodigo[cont].arg_tipoInt=$("#tipoInt"+item.id+"").val();
        muro_intCodigo[cont].arg_materialInt=$("#materialInt"+item.id+"").val();
        cont=cont+1;
      });
      pos = muro_intDibujo.map(function(e) {return e.id;}).indexOf($id);
      ////console.log("poos",pos);
      muro_intCodigo.splice(pos,1);
      muro_intDibujo.splice(pos,1);
      cont=1;
      for (var i=0; i<muro_intDibujo.length;i++)
      {
        cadena=cadena + muro_intDibujo[i].scd_codigo;
        cont++;
      }

      $('#acaPinturasMurosInteriores').html(cadena);
      cont=0;
      muro_intDibujo.forEach(function (item, index, array) 
      {
        $("#estadoInt"+item.id+"").val(muro_intCodigo[cont].arg_estadoInt);
        $("#integridadInt"+item.id+"").val(muro_intCodigo[cont].arg_integridadInt);
        $("#tipoInt"+item.id+"").val(muro_intCodigo[cont].arg_tipoInt);
        $("#materialInt"+item.id+"").val(muro_intCodigo[cont].arg_materialInt);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
    });
  }
}
//--------------------------- Fin Grilla 4 --------------------------------------------------------------------

//--------------------------- Inicio Grilla 5 --------------------------------------------------------------------
var contCodigo3=2;
var aca_extCodigo=[];
var aca_extDibujo=[];
htmlAcaExt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estadoAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridadAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipoAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Revoque Grueso">Revoque Grueso</option>'+ 
'<option value="Revoque Fino"> Revoque Fino</option>'+
'<option value="Material Visto">Material Visto</option>'+
'<option value="Texturizado">Texturizado</option>'+
'<option value="Enchape">Enchape</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="materialAcaExt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Piedra">Piedra</option>'+ 
'<option value="Madera"> Madera</option>'+
'<option value="Ladrillo">Ladrillo</option>'+
'<option value="Cerámica">Cerámica</option>'+
'<option value="Marmol">Marmol</option>'+
'<option value="Azulejo">Azulejo</option>'+
'<option value="Cal y Arena">Cal y Arena</option>'+
'<option value="Barro y Paja">Barro y Paja</option>'+
'<option value="Cemento y Arena">Cemento y Arena</option>'+
'<option value="Yeso">Yeso</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoExt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
aca_extCodigo.push({arg_estadoAcaExt:'N',arg_integridadAcaExt:'N',arg_tipoAcaExt:'N',arg_materialAcaExt:'N'});
aca_extDibujo.push({scd_codigo:htmlAcaExt,id: 1});

cadena=aca_extDibujo[0].scd_codigo;
//////console.log("cadenaaaa",cadena);
$('#divacaAcabadosMurosExteriores').html(cadena);
$("#estadoAcaExt1").val(aca_extCodigo[0].arg_estadoAcaExt);
$("#integridadAcaExt1").val(aca_extCodigo[0].arg_integridadAcaExt);
$("#tipoAcaExt1").val(aca_extCodigo[0].arg_tipoAcaExt);
$("#materialAcaExt1").val(aca_extCodigo[0].arg_materialAcaExt);


function acaAcabadosMurosExteriores(){
  var htmlAcaExt;
  htmlAcaExt=
  '<div class="col-md-4"><br>'+
  '<select class="form-control" id="estadoAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="integridadAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="tipoAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="materialAcaExt'+contCodigo3+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoExt('+contCodigo3+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  aca_extDibujo.forEach(function (item, index, array) 
  {
    aca_extCodigo[cont].arg_estadoAcaExt = $("#estadoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_integridadAcaExt = $("#integridadAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_tipoAcaExt = $("#tipoAcaExt"+item.id+"").val();
    aca_extCodigo[cont].arg_materialAcaExt = $("#materialAcaExt"+item.id+"").val();

    cont++;
  });
  aca_extCodigo.push({arg_estadoAcaExt:'N',arg_integridadAcaExt:'N',arg_tipoAcaExt:'N',arg_materialAcaExt:'N'});
  aca_extDibujo.push({scd_codigo:htmlAcaExt,id: contCodigo3});
  contCodigo3++;
  cont=1;
  for (var i=0; i<aca_extDibujo.length;i++)
  {
    cadena=cadena + aca_extDibujo[i].scd_codigo;
    cont++;
  }
  $('#divacaAcabadosMurosExteriores').html(cadena); 
  var conto=0;
  aca_extDibujo.forEach(function (item, index, array) 
  { 
    $("#estadoAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_estadoAcaExt);
    $("#integridadAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_integridadAcaExt);
    $("#tipoAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_tipoAcaExt);
    $("#materialAcaExt"+item.id+"").val(aca_extCodigo[conto].arg_materialAcaExt);
    conto=conto+1;
  });
}
function menosLineaAcabadoExt($id){
  if(contCodigo3>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el registro seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      aca_extDibujo.forEach(function (item, index, array) 
      {
        aca_extCodigo[cont].arg_estadoAcaExt=$("#estadoAcaExt"+item.id+"").val();
        aca_extCodigo[cont].arg_integridadAcaExt=$("#integridadAcaExt"+item.id+"").val();
        aca_extCodigo[cont].arg_tipoAcaExt=$("#tipoAcaExt"+item.id+"").val();
        aca_extCodigo[cont].arg_materialAcaExt=$("#materialAcaExt"+item.id+"").val();
        cont=cont+1;
      });
      pos = aca_extDibujo.map(function(e) {return e.id;}).indexOf($id);
      ////console.log("poos",pos);
      aca_extCodigo.splice(pos,1);
      aca_extDibujo.splice(pos,1);
      cont=1;
      for (var i=0; i<aca_extDibujo.length;i++)
      {
        cadena=cadena + aca_extDibujo[i].scd_codigo;
        cont++;
      }

      $('#divacaAcabadosMurosExteriores').html(cadena);
      cont=0;
      aca_extDibujo.forEach(function (item, index, array) 
      {
        $("#estadoAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_estadoAcaExt);
        $("#integridadAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_integridadAcaExt);
        $("#tipoAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_tipoAcaExt);
        $("#materialAcaExt"+item.id+"").val(aca_extCodigo[cont].arg_materialAcaExt);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
    });
  }
}
//--------------------------- Fin Grilla 5 --------------------------------------------------------------------


//--------------------------- Inicio Grilla 6 --------------------------------------------------------------------
var contCodigo4=2;
var aca_intCodigo=[];
var aca_intDibujo=[];
htmlAcaInt=    
'<div class="col-md-4">'+
'<select class="form-control" id="estadoAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Bueno">Bueno</option>'+ 
'<option value="Regular"> Regular</option>'+
'<option value="Malo">Malo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-3">'+
'<select class="form-control" id="integridadAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Original">Original</option>'+ 
'<option value="Nuevo"> Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="tipoAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Revoque Grueso">Revoque Grueso</option>'+ 
'<option value="Revoque Fino"> Revoque Fino</option>'+
'<option value="Material Visto">Material Visto</option>'+
'<option value="Texturizado">Texturizado</option>'+
'<option value="Enchape">Enchape</option>'+
'</select> '+
'</div>'+
'<div class="col-md-2">'+
'<select class="form-control" id="materialAcaInt1" >'+
'<option value="N" selected="selected"> Seleccionar...</option>'+
'<option value="Piedra">Piedra</option>'+ 
'<option value="Madera"> Madera</option>'+
'<option value="Ladrillo">Ladrillo</option>'+
'<option value="Cerámica">Cerámica</option>'+
'<option value="Marmol">Marmol</option>'+
'<option value="Azulejo">Azulejo</option>'+
'<option value="Cal y Arena">Cal y Arena</option>'+
'<option value="Barro y Paja">Barro y Paja</option>'+
'<option value="Cemento y Arena">Cemento y Arena</option>'+
'<option value="Yeso">Yeso</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoInt(1);"></i>'+
'</a></div><br>';
var cadena = '';  
aca_intCodigo.push({arg_estadoAcaInt:'N',arg_integridadAcaInt:'N',arg_tipoAcaInt:'N',arg_materialAcaInt:'N'});
aca_intDibujo.push({scd_codigo:htmlAcaInt,id: 1});

cadena=aca_intDibujo[0].scd_codigo;
//////console.log("cadenaaaa",cadena);
$('#divacaAcabadosMurosInteriores').html(cadena);
$("#estadoAcaInt1").val(aca_intCodigo[0].arg_estadoAcaInt);
$("#integridadAcaInt1").val(aca_intCodigo[0].arg_integridadAcaInt);
$("#tipoAcaInt1").val(aca_intCodigo[0].arg_tipoAcaInt);
$("#materialAcaInt1").val(aca_intCodigo[0].arg_materialAcaInt);


function acaAcabadosMurosInteriores(){
  var htmlAcaInt;
  htmlAcaInt=
  '<div class="col-md-4"><br>'+
  '<select class="form-control" id="estadoAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="integridadAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="tipoAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="materialAcaInt'+contCodigo4+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAcabadoInt('+contCodigo4+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  aca_intDibujo.forEach(function (item, index, array) 
  {
    aca_intCodigo[cont].arg_estadoAcaInt = $("#estadoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_integridadAcaInt = $("#integridadAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_tipoAcaInt = $("#tipoAcaInt"+item.id+"").val();
    aca_intCodigo[cont].arg_materialAcaInt = $("#materialAcaInt"+item.id+"").val();

    cont++;
  });
  aca_intCodigo.push({arg_estadoAcaInt:'N',arg_integridadAcaInt:'N',arg_tipoAcaInt:'N',arg_materialAcaInt:'N'});
  aca_intDibujo.push({scd_codigo:htmlAcaInt,id: contCodigo4});
  contCodigo4++;
  cont=1;
  for (var i=0; i<aca_intDibujo.length;i++)
  {
    cadena=cadena + aca_intDibujo[i].scd_codigo;
    cont++;
  }
  $('#divacaAcabadosMurosInteriores').html(cadena); 
  var conto=0;
  aca_intDibujo.forEach(function (item, index, array) 
  { 
    $("#estadoAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_estadoAcaInt);
    $("#integridadAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_integridadAcaInt);
    $("#tipoAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_tipoAcaInt);
    $("#materialAcaInt"+item.id+"").val(aca_intCodigo[conto].arg_materialAcaInt);
    conto=conto+1;
  });
}

function menosLineaAcabadoInt($id){
  if(contCodigo4>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el registro seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      aca_intDibujo.forEach(function (item, index, array) 
      {
        aca_intCodigo[cont].arg_estadoAcaInt=$("#estadoAcaInt"+item.id+"").val();
        aca_intCodigo[cont].arg_integridadAcaInt=$("#integridadAcaInt"+item.id+"").val();
        aca_intCodigo[cont].arg_tipoAcaInt=$("#tipoAcaInt"+item.id+"").val();
        aca_intCodigo[cont].arg_materialAcaInt=$("#materialAcaInt"+item.id+"").val();
        cont=cont+1;
      });
      pos = aca_intDibujo.map(function(e) {return e.id;}).indexOf($id);
      ////console.log("poos",pos);
      aca_intCodigo.splice(pos,1);
      aca_intDibujo.splice(pos,1);
      cont=1;
      for (var i=0; i<aca_intDibujo.length;i++)
      {
        cadena=cadena + aca_intDibujo[i].scd_codigo;
        cont++;
      }

      $('#divacaAcabadosMurosInteriores').html(cadena);
      cont=0;
      aca_intDibujo.forEach(function (item, index, array) 
      {
        $("#estadoAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_estadoAcaInt);
        $("#integridadAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_integridadAcaInt);
        $("#tipoAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_tipoAcaInt);
        $("#materialAcaInt"+item.id+"").val(aca_intCodigo[cont].arg_materialAcaInt);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
    });
  }
}
//--------------------------- Fin Grilla 6 --------------------------------------------------------------------

 
    //--------------------GRILLA Detalles Artisitcos--------------------//
    var contCodigoDetaArtistoco = 2;
    var arrCodigoDetaArtistoco = [];
    var arrDibujoDetaArtistoco = [];
    htmlDetaArtistoco='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
    '<select id="descripcionDetaArtistoco1" class="form-control">'+
    '<option value="N" selected="selected">..Seleccionar..</option>'+
    '<option value="1">Pinturas Mural</option>'+
    '<option value="2">Esculuturas</option>'+
    '<option value="3">Caracol</option>'+
    '<option value="4">Enchapes de Ceramica</option>'+
    '<option value="5">Herrajes</option>'+
    '<option value="6">Portada</option>'+
    '</select> '+
    '</div>'+
    '<div class="form-group col-md-5">'+
    '<select id="integridDetaArtistoco1" class="form-control">'+
    '<option value="N" selected="selected">....Seleccionar...</option>'+
    '<option value="1">Original</option>'+
    '<option value="2">Nuevo</option>'+
    '<option value="3">Modificado</option>'+
    '<option value="4">Original y Nuevo</option>'+
    '</select> '+
    '</div>'+
    '<div class="col-md-2">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaDetaArtistoco(1);"></i>'+
    '</a></div>'+
    '</div>';
    var cadenaDetaArtistoco = '';  
    arrCodigoDetaArtistoco.push({descripcionDetaArtistoco: 'N',integridadDetaArtistoco: ''});
    arrDibujoDetaArtistoco.push({scd_codigo:htmlDetaArtistoco,id: 1});
    //////console.log("arrCodigo",arrCodigoUso);
    cadenaDetaArtistoco=arrDibujoDetaArtistoco[0].scd_codigo;
    ////console.log("cadenaDetaArtistoco",cadenaDetaArtistoco);
    $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco);
    $("#integridDetaArtistoco1").val(arrCodigoDetaArtistoco[0].integridadDetaArtistoco);

    function grillaDetaArtistoco(){
      var htmlDetaArtistoco;
      htmlDetaArtistoco='<div class="col-md-12">'+
      '<div class="form-group col-md-5"><br>'+
      '<select id="descripcionDetaArtistoco'+contCodigoDetaArtistoco+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="1">Pinturas Mural</option>'+
      '<option value="2">Esculuturas</option>'+
      '<option value="3">Caracol</option>'+
      '<option value="4">Enchapes de Ceramica</option>'+
      '<option value="5">Herrajes</option>'+
      '<option value="6">Portada</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-5"><br>'+
      '<select id="integridDetaArtistoco'+contCodigoDetaArtistoco+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="1">Original</option>'+
      '<option value="2">Nuevo</option>'+
      '<option value="3">Modificado</option>'+
      '<option value="4">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaDetaArtistoco('+contCodigoDetaArtistoco+');"></i>'+
      '</a></div>'+
      '</div>';
      var cadenaDetaArtistoco = '';
      var cont = 0;
      arrDibujoDetaArtistoco.forEach(function (item, index, array) 
      {
        arrCodigoDetaArtistoco[cont].descripcionDetaArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
        arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
        cont++;
      });

      arrCodigoDetaArtistoco.push({descripcionDetaArtistoco: 'N', integridadDetaArtistoco: ''});
      arrDibujoDetaArtistoco.push({scd_codigo:htmlDetaArtistoco, id: contCodigoDetaArtistoco});
      contCodigoDetaArtistoco++;

      cont=1;
      for (var i=0; i<arrDibujoDetaArtistoco.length;i++)
      {
        cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDetaArtistoco[i].scd_codigo;
        cont++;
      }
      
      $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco); 

      var conto=0;
      arrDibujoDetaArtistoco.forEach(function (item, index, array) 
      { 
        $("#descripcionDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].descripcionDetaArtistoco); 
        $("#integridDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].integridadDetaArtistoco); 

        conto=conto+1;
      });

        //////console.log("arrCodigoDetaArtistoco",arrCodigoDetaArtistoco);
      }


      function eliminarGrillaDetaArtistoco($id){
        if(contCodigoDetaArtistoco>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaDetaArtistoco ='';
            var pos=-1;
            var cont = 0;
            arrDibujoDetaArtistoco.forEach(function (item, index, array) 
            {
              arrCodigoDetaArtistoco[cont].descripcionDetaArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
              arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
              cont++;
            });
            pos = arrDibujoDetaArtistoco.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrCodigoDetaArtistoco.splice(pos,1);
            arrDibujoDetaArtistoco.splice(pos,1);
            ////console.log("arrCodigo",arrCodigoDetaArtistoco);

            cont=1;
            for (var i=0; i<arrDibujoDetaArtistoco.length;i++)
            {
              cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDetaArtistoco[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco);
            var conto=0;
            arrDibujoDetaArtistoco.forEach(function (item, index, array) 
            { 
              $("#descripcionDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].descripcionDetaArtistoco); 
              $("#integridDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].integridadDetaArtistoco); 
              conto=conto+1;
            });
          });
        }
        /////////console.log("arrCodigoFinal",arrCodigoAcab);
      }

    //--------------------GRILLA Detalles Artisitcos-------------------//
     //GRILLA TIPOLOGICOS COMPOSITIVOS 
    var contCodigoTIC=2;
    var arrCodigoTIC=[];
    var arrDibujoTIC=[];    
      htmlTIC='<div class="col-md-12">'+                     
                '<div class="col-md-4">'+          
                    '<select class="form-control" id="tipologicos11" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+            
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4">'+                
                    '<select class="form-control" id="tipologicos21" >'+
                       '<option value="N" selected="selected">--Seleccione--</option>'+
                       '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+ 
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="tipologicos31" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="atrio">Atrio</option>'+
                        '<option value="capilla">Capilla</option>'+
                        '<option value="chifon">Chifon</option>'+
                        '<option value="claustro">Claustro</option>'+   
                        '<option value="corredor">Corredor</option>'+
                        '<option value="corredor ardiente">Corredor ardiente</option>'+
                        '<option value="crujias">Crujias</option>'+
                        '<option value="enfarolado">Enfarolado</option>'+
                        '<option value="estacionamiento">Estacionamiento</option>'+
                        '<option value="galeria">Galeria</option>'+
                        '<option value="jardin interior">Jardin interior</option>'+
                        '<option value="hall">Hall</option>'+
                        '<option value="nave unica">Nave unica</option>'+
                        '<option value="naves">Naves</option>'+
                        '<option value="patio primero">Patio primero</option>'+
                        '<option value="patio segundo">Patio segundo</option>'+
                        '<option value="peribolo">Peribolo</option>'+
                        '<option value="planta basilical">planta basilical</option>'+
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaTIC = '';  
    arrCodigoTIC.push({tipologicos1:'N', tipologicos2:'N',tipologicos3:'N'});
    arrDibujoTIC.push({scd_codigo:htmlTIC,id: 1});        
    //cadenaTIC=arrDibujoTIC[0].scd_codigo;    
    cadenaTIC='<div class="form-group"><label class="col-md-12">'+arrDibujoTIC[0].scd_codigo;
    $('#grillaTipologicosC').html(cadenaTIC);
    $("#tipologicos11").val(arrCodigoTIC[0].tipologicos1);
    $("#tipologicos21").val(arrCodigoTIC[0].tipologicos2);
    $("#tipologicos31").val(arrCodigoTIC[0].tipologicos3);

function crearTIC(){
    var htmlTIC;
        htmlTIC='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+          
                    '<select class="form-control" id="tipologicos1'+contCodigoTIC+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+                
                    '<select class="form-control" id="tipologicos2'+contCodigoTIC+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+          
                    '<select class="form-control" id="tipologicos3'+contCodigoTIC+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="atrio">Atrio</option>'+
                        '<option value="capilla">Capilla</option>'+
                        '<option value="chifon">Chifon</option>'+
                        '<option value="claustro">Claustro</option>'+   
                        '<option value="corredor">Corredor</option>'+
                        '<option value="corredor ardiente">Corredor ardiente</option>'+
                        '<option value="crujias">Crujias</option>'+
                        '<option value="enfarolado">Enfarolado</option>'+
                        '<option value="estacionamiento">Estacionamiento</option>'+
                        '<option value="galeria">Galeria</option>'+
                        '<option value="jardin interior">Jardin interior</option>'+
                        '<option value="hall">Hall</option>'+
                        '<option value="nave unica">Nave unica</option>'+
                        '<option value="naves">Naves</option>'+
                        '<option value="patio primero">Patio primero</option>'+
                        '<option value="patio segundo">Patio segundo</option>'+
                        '<option value="peribolo">Peribolo</option>'+
                        '<option value="planta basilical">planta basilical</option>'+
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaTIC('+contCodigoTIC+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaTIC='';
        var cont=0;
        arrDibujoTIC.forEach(function (item, index, array) 
        {
          arrCodigoTIC[cont].tipologicos1 = $("#tipologicos1"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos2 = $("#tipologicos2"+item.id+"").val();
          arrCodigoTIC[cont].tipologicos3 = $("#tipologicos3"+item.id+"").val();
          cont++;
        });
        arrCodigoTIC.push({tipologicos1:'N', tipologicos2:'N',tipologicos3:'N'});
        arrDibujoTIC.push({scd_codigo:htmlTIC,id: contCodigoTIC});
        contCodigoTIC++;
        cont=1;
        for (var i=0; i<arrDibujoTIC.length;i++)
        {
          cadenaTIC=cadenaTIC + arrDibujoTIC[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#grillaTipologicosC').html(cadenaTIC); 
        var conto=0;
        arrDibujoTIC.forEach(function (item, index, array) 
        { 
          $("#tipologicos1"+item.id+"").val(arrCodigoTIC[conto].tipologicos1);
          $("#tipologicos2"+item.id+"").val(arrCodigoTIC[conto].tipologicos2);
          $("#tipologicos3"+item.id+"").val(arrCodigoTIC[conto].tipologicos3);
          conto=conto+1;
        });
        ////console.log("arrCodigoTIC",arrCodigoTIC);
        
    }
        function menosLineaTIC($id){
        if(contCodigoTIC>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaTIC='';
                var cont=0;
                var pos=-1;
                arrDibujoTIC.forEach(function (item, index, array) 
                {
                    arrCodigoTIC[cont].tipologicos1 = $("#tipologicos1"+item.id+"").val();
                    arrCodigoTIC[cont].tipologicos2 = $("#tipologicos2"+item.id+"").val();
                    arrCodigoTIC[cont].tipologicos3 = $("#tipologicos3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoTIC.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigoTIC.splice(pos,1);
                arrDibujoTIC.splice(pos,1);
                ////console.log("arrCodigoTIC",arrCodigoTIC);
                
                cont=1;
                for (var i=0; i<arrDibujoTIC.length;i++)
                {
                    cadenaTIC=cadenaTIC +arrDibujoTIC[i].scd_codigo;
                    cont++;
                }                
                $('#grillaTipologicosC').html(cadenaTIC);
                
                cont=0;
                arrDibujoTIC.forEach(function (item, index, array) 
                {
                    $("#tipologicos1"+item.id+"").val(arrCodigoTIC[cont].tipologicos1);
                    $("#tipologicos2"+item.id+"").val(arrCodigoTIC[cont].tipologicos2);
                    $("#tipologicos3"+item.id+"").val(arrCodigoTIC[cont].tipologicos3);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigoFinal",arrCodigoTIC);
    }
    //GRILLA ELEMENTOS ORNAMENTALES
    var contCodigoOR=2;
    var arrCodigoOR=[];
    var arrDibujoOR=[];    
      htmlOR='<div class="col-md-12">'+                     
                '<div class="col-md-4">'+          
                    '<select class="form-control" id="ornamentales11" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+            
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4">'+                
                    '<select class="form-control" id="ornamentales21" >'+
                       '<option value="N" selected="selected">--Seleccione--</option>'+
                       '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+ 
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="ornamentales31" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alero con canales vistos">Alero con canales vistos</option>'+
                        '<option value="alero encajonado">Alejo encajonado</option>'+
                        '<option value="alfiz">Alfiz</option>'+
                        '<option value="avitolado">Avitolado</option>'+
                        '<option value="alto relieve">Alto relieve</option>'+   
                        '<option value="hojas de acanto">Hojas de acanto</option>'+
                        '<option value="adaraja">Adaraja</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="alfeizar">Alfeizar</option>'+
                        '<option value="almohadillado">Almohadillado</option>'+  
                        '<option value="balustre">Balustre</option>'+
                        '<option value=canesillos">Canesillos</option>'+
                        '<option value="cancel">cancel</option>'+
                        '<option value="cartela">Cartela</option>'+
                        '<option value="columna adosada">Columna adosada</option>'+   
                        '<option value="columna esquinera">Columna esquinera</option>'+
                        '<option value="columna exenta">Columna exenta</option>'+
                        '<option value="corniza">Corniza</option>'+
                        '<option value="denticulos">Denticulos</option>'+                                
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaOR = '';  
    arrCodigoOR.push({ornamentales1:'N', ornamentales2:'N',ornamentales3:'N'});
    arrDibujoOR.push({scd_codigo:htmlOR,id: 1}); 
    cadenaOR=arrDibujoOR[0].scd_codigo;   
    $('#grillaOrnamentalesA').html(cadenaOR);
    $("#ornamentales11").val(arrCodigoOR[0].ornamentales1);
    $("#ornamentales21").val(arrCodigoOR[0].ornamentales2);
    $("#ornamentales31").val(arrCodigoOR[0].ornamentales3);

function crearORA(){
    var htmlOR;
        htmlOR='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+          
                    '<select class="form-control" id="ornamentales1'+contCodigoOR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+                
                    '<select class="form-control" id="ornamentales2'+contCodigoOR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+          
                    '<select class="form-control" id="ornamentales3'+contCodigoOR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alero con canales vistos">Alero con canales vistos</option>'+
                        '<option value="alero encajonado">Alejo encajonado</option>'+
                        '<option value="alfiz">Alfiz</option>'+
                        '<option value="avitolado">Avitolado</option>'+
                        '<option value="alto relieve">Alto relieve</option>'+   
                        '<option value="hojas de acanto">Hojas de acanto</option>'+
                        '<option value="adaraja">Adaraja</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="alfeizar">Alfeizar</option>'+
                        '<option value="almohadillado">Almohadillado</option>'+  
                        '<option value="balustre">Balustre</option>'+
                        '<option value=canesillos">Canesillos</option>'+
                        '<option value="cancel">cancel</option>'+
                        '<option value="cartela">Cartela</option>'+
                        '<option value="columna adosada">Columna adosada</option>'+   
                        '<option value="columna esquinera">Columna esquinera</option>'+
                        '<option value="columna exenta">Columna exenta</option>'+
                        '<option value="corniza">Corniza</option>'+
                        '<option value="denticulos">Denticulos</option>'+                                
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaOR('+contCodigoOR+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaOR='';
        var cont=0;
        arrDibujoOR.forEach(function (item, index, array) 
        {
          arrCodigoOR[cont].ornamentales1 = $("#ornamentales1"+item.id+"").val();
          arrCodigoOR[cont].ornamentales2 = $("#ornamentales2"+item.id+"").val();
          arrCodigoOR[cont].ornamentales3 = $("#ornamentales3"+item.id+"").val();
          cont++;
        });
        arrCodigoOR.push({ornamentales1:'N', ornamentales2:'N',ornamentales3:'N'});
        arrDibujoOR.push({scd_codigo:htmlOR,id: contCodigoOR});
        contCodigoOR++;
        cont=1;
        for (var i=0; i<arrDibujoOR.length;i++)
        {
          cadenaOR=cadenaOR + arrDibujoOR[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#grillaOrnamentalesA').html(cadenaOR); 
        var conto=0;
        arrDibujoOR.forEach(function (item, index, array) 
        { 
          $("#ornamentales1"+item.id+"").val(arrCodigoOR[conto].ornamentales1);
          $("#ornamentales2"+item.id+"").val(arrCodigoOR[conto].ornamentales2);
          $("#ornamentales3"+item.id+"").val(arrCodigoOR[conto].ornamentales3);
          conto=conto+1;
        });
        ////console.log("arrCodigoOR",arrCodigoOR);
        
    }
    function menosLineaOR($id){
        if(contCodigoOR>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaOR='';
                var cont=0;
                var pos=-1;
                arrDibujoOR.forEach(function (item, index, array) 
                {
                    arrCodigoOR[cont].ornamentales1 = $("#ornamentales1"+item.id+"").val();
                    arrCodigoOR[cont].ornamentales2 = $("#ornamentales2"+item.id+"").val();
                    arrCodigoOR[cont].ornamentales3 = $("#ornamentales3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoOR.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigoOR.splice(pos,1);
                arrDibujoOR.splice(pos,1);
                ////console.log("arrCodigoOR",arrCodigoOR);
                
                cont=1;
                for (var i=0; i<arrDibujoOR.length;i++)
                {
                    cadenaOR=cadenaOR +arrDibujoOR[i].scd_codigo;
                    cont++;
                }                
                $('#grillaOrnamentalesA').html(cadenaOR);
                
                cont=0;
                arrDibujoOR.forEach(function (item, index, array) 
                {
                    $("#ornamentales1"+item.id+"").val(arrCodigoOR[cont].ornamentales1);
                    $("#ornamentales2"+item.id+"").val(arrCodigoOR[cont].ornamentales2);
                    $("#ornamentales3"+item.id+"").val(arrCodigoOR[cont].ornamentales3);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigoFinal",arrCodigoOR);
    }
    //GRILLA ARTISITICOS COMPOSITIVOS
    var contCodigoAR=2;
    var arrCodigoAR=[];
    var arrDibujoAR=[];    
      htmlAR='<div class="col-md-12">'+                     
                '<div class="col-md-4">'+          
                    '<select class="form-control" id="artisticosC11" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+            
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4">'+                
                    '<select class="form-control" id="artisticosC21" >'+
                       '<option value="N" selected="selected">--Seleccione--</option>'+
                       '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+ 
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3">'+          
                    '<select class="form-control" id="artisticosC31" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="Pintura en cielo(plafon)">Pintura en cielo(plafon)</option>'+
                        '<option value="pinturas murales">Pinturas murales</option>'+
                        '<option value="moldura de cielos">Moldura de cielos</option>'+
                        '<option value="Enchapes de ceramica vidriada">Enchapes de ceramica vidriada</option>'+
                        '<option value="chapas">Chapas</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAR(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaAR = '';  
    arrCodigoAR.push({artisticosC1:'N', artisticosC2:'N',artisticosC3:'N'});
    arrDibujoAR.push({scd_codigo:htmlAR,id: 1});  
    //cadenaAR=arrDibujoAR[0].scd_codigo;    
    cadenaAR='<div class="form-group"><label class="col-md-12">'+arrDibujoAR[0].scd_codigo;
    $('#grillaArtisiticoC').html(cadenaAR);
    $("#artisticosC11").val(arrCodigoAR[0].artisticosC1);
    $("#artisticosC21").val(arrCodigoAR[0].artisticosC2);
    $("#artisticosC31").val(arrCodigoAR[0].artisticosC3);

function crearART(){
    var htmlAR;
        htmlAR='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+          
                    '<select class="form-control" id="artisticosC1'+contCodigoAR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+                
                    '<select class="form-control" id="artisticosC2'+contCodigoAR+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+          
                    '<select class="form-control" id="artisticosC3'+contCodigoAR+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="Pintura en cielo(plafon)">Pintura en cielo(plafon)</option>'+
                        '<option value="pinturas murales">Pinturas murales</option>'+
                        '<option value="moldura de cielos">Moldura de cielos</option>'+
                        '<option value="Enchapes de ceramica vidriada">Enchapes de ceramica vidriada</option>'+
                        '<option value="chapas">Chapas</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaAR('+contCodigoAR+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaAR='';
        var cont=0;
        arrDibujoAR.forEach(function (item, index, array) 
        {
          arrCodigoAR[cont].artisticosC1 = $("#artisticosC1"+item.id+"").val();
          arrCodigoAR[cont].artisticosC2 = $("#artisticosC2"+item.id+"").val();
          arrCodigoAR[cont].artisticosC3 = $("#artisticosC3"+item.id+"").val();
          cont++;
        });
        arrCodigoAR.push({artisticosC1:'N', artisticosC2:'N',artisticosC3:'N'});
        arrDibujoAR.push({scd_codigo:htmlAR,id: contCodigoAR});
        contCodigoAR++;
        cont=1;
        for (var i=0; i<arrDibujoAR.length;i++)
        {
          cadenaAR=cadenaAR + arrDibujoAR[i].scd_codigo;
          cont++;
        }       
        $('#grillaArtisiticoC').html(cadenaAR); 
        var conto=0;
        arrDibujoAR.forEach(function (item, index, array) 
        { 
          $("#artisticosC1"+item.id+"").val(arrCodigoAR[conto].artisticosC1);
          $("#artisticosC2"+item.id+"").val(arrCodigoAR[conto].artisticosC2);
          $("#artisticosC3"+item.id+"").val(arrCodigoAR[conto].artisticosC3);
          conto=conto+1;
          ////console.log(arrCodigoAR, "arrCodigoAR");
        });
        console.log("vectorelementoscompositivos",arrCodigoAR);
    }
    function menosLineaAR($id){
        if(contCodigoAR>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaAR='';
                var cont=0;
                var pos=-1;
                arrDibujoAR.forEach(function (item, index, array) 
                {
                    arrCodigoAR[cont].artisticosC1 = $("#artisticosC1"+item.id+"").val();
                    arrCodigoAR[cont].artisticosC2 = $("#artisticosC2"+item.id+"").val();
                    arrCodigoAR[cont].artisticosC3 = $("#artisticosC3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoAR.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigoAR.splice(pos,1);
                arrDibujoAR.splice(pos,1);
                ////console.log("arrCodigoAR",arrCodigoAR);
                
                cont=1;
                for (var i=0; i<arrDibujoAR.length;i++)
                {
                    cadenaAR=cadenaAR +arrDibujoAR[i].scd_codigo;
                    cont++;
                }                
                $('#grillaArtisiticoC').html(cadenaAR);
                
                cont=0;
                arrDibujoAR.forEach(function (item, index, array) 
                {
                    $("#artisticosC1"+item.id+"").val(arrCodigoAR[cont].artisticosC1);
                    $("#artisticosC2"+item.id+"").val(arrCodigoAR[cont].artisticosC2);
                    $("#artisticosC3"+item.id+"").val(arrCodigoAR[cont].artisticosC3);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigoFinal",arrCodigoAR);
    }
    //GRILLA HOGARES 
    var contCodigoH=2;
    var arrCodigoHogar=[];
    var arrDibujoHogar=[];

    
      htmlHogar='<div class="col-md-12">'+
                '<div class="col-md-2">'+
                    '<input class="form-control" type="text" id="hogar11"  placeholder="hogar Descripcion"></input>'+
                '</div>'+      
                '<div class="col-md-2">'+          
                    '<select class="form-control" id="hogar21" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="0-4">0  - 4</option>'+
                        '<option value="5-10">5  - 10</option>'+
                        '<option value="11-20">11  - 20</option>'+
                        '<option value="21-40">21  - 40</option>'+
                        '<option value="41-65">41  - 65</option>'+
                        '<option value="65 mas">65 mas</option>'+              
                    '</select>'+
                '</div>'+ 
                '<div class="col-md-2">'+
                '<input type="checkbox" id="hogar31"> -   </input>'+       
                '<input type="checkbox" id="hogar41"> </input>'+ 
                '</div>'+  
                '<div class="col-md-2">'+                
                    '<select class="form-control" id="hogar51" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alquiler">Alquiler</option>'+
                        '<option value="anticresis">Anticresis</option>'+ 
                        '<option value="desconocida">Desconocida</option>'+                       
                        '<option value="prestamo">Prestamo</option>'+
                        '<option value="propia">Propia</option>'+
                        '<option value="usucapion">Usucapion</option>'+
                    '</select>'+
                '</div>'+     
                '<div class="col-md-2">'+          
                    '<select class="form-control" id="hogar61" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="ninguno">Ninguno</option>'+
                        '<option value="desconocido">Desconocido</option>'+
                        '<option value="litigio">En litigio</option>'+
                        '<option value="compartida">Prop. Compartida</option>'+
                        '<option value="saneado">Saneado</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea(1);"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
    var cadenaHogar = '';  
     arrCodigoHogar.push({hogar1:'', hogar2:'N',hogar3:'',hogar4:'',hogar5:'N',hogar6:'N'});
    arrDibujoHogar.push({scd_codigo:htmlHogar,id: 1});
    ////console.log("arrCodigo",arrCodigo);
    
    cadenaHogar=arrDibujoHogar[0].scd_codigo;
    ////console.log("cadenaaaa",cadenaHogar);
    $('#grillaHogares').html(cadenaHogar);
    $("#hogar11").val(arrCodigoHogar[0].hogar1);
    $("#hogar21").val(arrCodigoHogar[0].hogar2);
    //$("#hogar31").val(arrCodigoHogar[0].hogar3);
    //$("#hogar41").val(arrCodigoHogar[0].hogar4);
    document.getElementById("hogar31").checked =arrCodigoHogar[0].hogar3 ;
    document.getElementById("hogar41").checked =arrCodigoHogar[0].hogar4 ;
    $("#hogar51").val(arrCodigoHogar[0].hogar5);
    $("#hogar61").val(arrCodigoHogar[0].hogar6);

    

function crearhogar(){
    var htmlHogar;
        htmlHogar='<br><div class="col-md-12">'+
                '<div class="col-md-2"><br>'+
                    '<input class="form-control" type="text" id="hogar1'+contCodigoH+'"  placeholder="hogar Descripcion"></input>'+
                '</div>'+      
                '<div class="col-md-2"><br>'+          
                    '<select class="form-control" id="hogar2'+contCodigoH+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="0-4">0  - 4</option>'+
                        '<option value="5-10">5  - 10</option>'+
                        '<option value="11-20">11  - 20</option>'+
                        '<option value="21-40">21  - 40</option>'+
                        '<option value="41-65">41  - 65</option>'+
                        '<option value="65 mas">65 mas</option>'+              
                    '</select>'+
                '</div>'+ 
                '<div class="col-md-2"><br>'+
                '<input type="checkbox" id="hogar3'+contCodigoH+'"> -   </input>'+       
                '<input type="checkbox" id="hogar4'+contCodigoH+'"> </input>'+ 
                '</div>'+  
                '<div class="col-md-2"><br>'+                
                    '<select class="form-control" id="hogar5'+contCodigoH+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alquiler">Alquiler</option>'+
                        '<option value="anticresis">Anticresis</option>'+  
                        '<option value="desconocida">Desconocida</option>'+                       
                        '<option value="prestamo">Prestamo</option>'+
                        '<option value="propia">Propia</option>'+
                        '<option value="usucapion">Usucapion</option>'+
                    '</select>'+
                '</div>'+     
                '<div class="col-md-2"><br>'+          
                    '<select class="form-control" id="hogar6'+contCodigoH+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="ninguno">Ninguno</option>'+ 
                        '<option value="desconocido">Desconocido</option>'+                       
                        '<option value="litigio">En litigio</option>'+
                        '<option value="compartida">Prop. Compartida</option>'+
                        '<option value="saneado">Saneado</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaHogar('+contCodigoH+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadenaHogar='';
        var cont=0;
        arrDibujoHogar.forEach(function (item, index, array) 
        {
          arrCodigoHogar[cont].hogar1 = $("#hogar1"+item.id+"").val();
          arrCodigoHogar[cont].hogar2 = $("#hogar2"+item.id+"").val();
          arrCodigoHogar[cont].hogar3 = document.getElementById("hogar3"+item.id+"").checked;
          arrCodigoHogar[cont].hogar4 = document.getElementById("hogar4"+item.id+"").checked;
          arrCodigoHogar[cont].hogar5 = $("#hogar5"+item.id+"").val();
          arrCodigoHogar[cont].hogar6 = $("#hogar6"+item.id+"").val();
          cont++;
        });
        arrCodigoHogar.push({hogar1:'', hogar2:'N',hogar3:'',hogar4:'',hogar5:'N',hogar6:'N'});
        arrDibujoHogar.push({scd_codigo:htmlHogar,id: contCodigoH});
        contCodigoH++;
        cont=1;
        for (var i=0; i<arrDibujoHogar.length;i++)
        {
          cadenaHogar=cadenaHogar + arrDibujoHogar[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#grillaHogares').html(cadenaHogar); 
        var conto=0;
        arrDibujoHogar.forEach(function (item, index, array) 
        { 
          $("#hogar1"+item.id+"").val(arrCodigoHogar[conto].hogar1);
          $("#hogar2"+item.id+"").val(arrCodigoHogar[conto].hogar2);        
          document.getElementById("hogar3"+item.id+"").checked =arrCodigoHogar[conto].hogar3 ;
          document.getElementById("hogar4"+item.id+"").checked =arrCodigoHogar[conto].hogar4 ;
          $("#hogar5"+item.id+"").val(arrCodigoHogar[conto].hogar5);
          $("#hogar6"+item.id+"").val(arrCodigoHogar[conto].hogar6);
          conto=conto+1;
        });
        ////console.log("arrCodigoHogar",arrCodigoHogar);
    }
   function menosLineaHogar($id){
        if(contCodigoH>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaHogar='';
                var cont=0;
                var pos=-1;
                arrDibujoHogar.forEach(function (item, index, array) 
                {
                    arrCodigoHogar[cont].hogar1 = $("#hogar1"+item.id+"").val();
                    arrCodigoHogar[cont].hogar2 = $("#hogar2"+item.id+"").val();
                    arrCodigoHogar[cont].hogar3 = document.getElementById("hogar3"+item.id+"").checked;
                    arrCodigoHogar[cont].hogar4 = document.getElementById("hogar4"+item.id+"").checked;
                    arrCodigoHogar[cont].hogar5 = $("#hogar5"+item.id+"").val();
                    arrCodigoHogar[cont].hogar6 = $("#hogar6"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoHogar.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigoHogar.splice(pos,1);
                arrDibujoHogar.splice(pos,1);
                ////console.log("arrCodigoHogar",arrCodigoHogar);
                
                cont=1;
                for (var i=0; i<arrDibujoHogar.length;i++)
                {
                    cadenaHogar=cadenaHogar +arrDibujoHogar[i].scd_codigo;
                    cont++;
                }                
                $('#grillaHogares').html(cadenaHogar);
                
                cont=0;
                arrDibujoHogar.forEach(function (item, index, array) 
                {
                    $("#hogar1"+item.id+"").val(arrCodigoHogar[cont].hogar1);
                    $("#hogar2"+item.id+"").val(arrCodigoHogar[cont].hogar2);        
                    document.getElementById("hogar3"+item.id+"").checked =arrCodigoHogar[cont].hogar3 ;
                    document.getElementById("hogar4"+item.id+"").checked =arrCodigoHogar[cont].hogar4 ;
                    $("#hogar5"+item.id+"").val(arrCodigoHogar[cont].hogar5);
                    $("#hogar6"+item.id+"").val(arrCodigoHogar[cont].hogar6);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigoFinal",arrCodigoHogar);
    }

    /////////GRILLA GISEL///////////////// OPCION VER ELEMENTOS ESTRUCTURALES

  /////////GRILLA GISEL///////////////// OPCION VER ELEMENTOS ESTRUCTURALES bienes inmuebles

  var contPEX=2;
  var arrPEX=[];
  var arrDibujoPEX=[];

    htmlPEX='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPEX1" name="estadocPEX1">'+
            '<option value="EC" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPEX1" name="integridadPEX1">'+
            '<option value="I" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPEX1" name="tipoPEX1">'+
             '<option value="T" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPEX1" name="materialPEX1">'+
            '<option value="M" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado<">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Exteriores" onclick="darBajaPEX(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrPEX.push({val_est_con:'EC', val_int:'I', val_tipo:'T', val_mat:'M'});
    arrDibujoPEX.push({scd_codigo:htmlPEX,id: 1});
    ////console.log("arrPEX",arrPEX);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoPEX[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cparedesexteriores').html(cadena);
    $("#estadocPEX1").val(arrPEX[0].val_est_con);
    $("#integridadPEX1").val(arrPEX[0].val_int);
    $("#tipoPEX1").val(arrPEX[0].val_tipo);
    $("#materialPEX1").val(arrPEX[0].val_mat);


function crearParedesExteriores(){
    var htmlPEX;
        htmlPEX='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPEX'+contPEX+'" name="estadocPEX'+contPEX+'">'+
            '<option value="EC" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPEX'+contPEX+'" name="integridadPEX'+contPEX+'">'+
            '<option value="I" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPEX'+contPEX+'" name="tipoPEX'+contPEX+'">'+
             '<option value="T" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPEX'+contPEX+'" name="materialPEX'+contPEX+'">'+
            '<option value="M" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado<">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Exteriores" onclick="darBajaPEX('+contPEX+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPEX.forEach(function (item, index, array) 
        {
          arrPEX[cont].val_est_con = $("#estadocPEX"+item.id+"").val();
          arrPEX[cont].val_int = $("#integridadPEX"+item.id+"").val();
          arrPEX[cont].val_tipo = $("#tipoPEX"+item.id+"").val();
          arrPEX[cont].val_mat = $("#materialPEX"+item.id+"").val();
          cont++;
        });
        arrPEX.push({val_est_con:'EC', val_int:'I', val_tipo:'T', val_mat:'M'});
        arrDibujoPEX.push({scd_codigo:htmlPEX,id: contPEX});
        contPEX++;
        cont=1;
        for (var i=0; i<arrDibujoPEX.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPEX[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#cparedesexteriores').html(cadena); 
        var conto=0;
        arrDibujoPEX.forEach(function (item, index, array) 
        { 
          $("#estadocPEX"+item.id+"").val(arrPEX[conto].val_est_con);
          $("#integridadPEX"+item.id+"").val(arrPEX[conto].val_int);
          $("#tipoPEX"+item.id+"").val(arrPEX[conto].val_tipo);
          $("#materialPEX"+item.id+"").val(arrPEX[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaPEX($id){
        if(contPEX>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoPEX.forEach(function (item, index, array) 
                {
                    arrPEX[cont].val_est_con=$("#estadocPEX"+item.id+"").val();
                    arrPEX[cont].val_int=$("#integridadPEX"+item.id+"").val();
                    arrPEX[cont].val_tipo=$("#tipoPEX"+item.id+"").val();
                    arrPEX[cont].val_mat=$("#materialPEX"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoPEX.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrPEX.splice(pos,1);
                arrDibujoPEX.splice(pos,1);
                ////console.log("arrPEX",arrPEX);
                
                cont=1;
                for (var i=0; i<arrDibujoPEX.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPEX[i].scd_codigo;
                    cont++;
                }
                
                $('#cparedesexteriores').html(cadena);
                
                cont=0;
                arrDibujoPEX.forEach(function (item, index, array) 
                {
                    $("#estadocPEX"+item.id+"").val(arrPEX[cont].val_est_con);
                    $("#integridadPEX"+item.id+"").val(arrPEX[cont].val_int);
                    $("#tipoPEX"+item.id+"").val(arrPEX[cont].val_tipo);
                    $("#materialPEX"+item.id+"").val(arrPEX[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
//------Inicio grilla paredes exteriores-----------------------
var arrDibujoPExt =[];
var arrCodigoPExt =[];
var contParedes = 0;

function editarParedesExteriores(longitudGrilla){
    var contPExt = 1;
    arrDibujoPExt = [];
    var htmPExt;
    for (var i=1; i<longitudGrilla;i++){
            htmPExt='Estado de conservación:</label>'+
            '<select class="form-control" id="estadPEx'+contPExt+'" name="estadocPEx">'+
              '<option value="EC" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="intePEx'+contPExt+'" name="integridadPEx">'+
              '<option value="I" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipPEx'+contPExt+'" name="tipoPEx">'+
               '<option value="T" selected="selected">--Seleccione--</option>'+
                '<option value="Aislado">Aislado</option>'+
                '<option value="Portante">Portante</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="matPEx'+contPExt+'" name="materialPEx">'+
              '<option value="M" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Tapial">Tapial</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Prefabricados">Prefabricados</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Hormigon Armado<">Hormigon Armado</option>'+
              '<option value="Aceite">Aceite</option>'+
              '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
              '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
              '<option value="Quincha">Quincha</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Exteriores" onclick="eliminarParedesExteriores('+contPEX+');"></i>'+
        '</a></div>';
            arrDibujoPExt.push({scd_codigo:htmPExt,id: contPExt});
            contPExt++;
          }
          var cadena = '';
          var cont=1;
          for (var i=0; i<arrDibujoPExt.length;i++)
          {
            cadena = cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPExt[i].scd_codigo;
            cont++;
          }
          //////console.log("cadenaPExaa",cadena);
          $('#aparedesexteriores').html(cadena);
       }

function actualizarParedesExteriores(){
    var htmlPEX;
        htmlPEX='Estado de conservación:</label>'+
          '<select class="form-control" id="estadPEx'+contParedes+'" >'+
            '<option value="EC" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="intePEx'+contParedes+'">'+
            '<option value="I" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipPEx'+contParedes+'">'+
             '<option value="T" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="matPEx'+contParedes+'" >'+
            '<option value="M" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado<">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Exteriores" onclick="eliminarParedesExteriores('+contParedes+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPExt.forEach(function (item, index, array) 
        {
          arrCodigoPExt[cont].val_est_con = $("#estadPEx"+item.id+"").val();
          arrCodigoPExt[cont].val_int = $("#intePEx"+item.id+"").val();
          arrCodigoPExt[cont].val_tipo = $("#tipPEx"+item.id+"").val();
          arrCodigoPExt[cont].val_mat = $("#matPEx"+item.id+"").val();
          cont++;
        });
        arrCodigoPExt.push({val_est_con:'EC', val_int:'I', val_tipo:'T', val_mat:'M'});
        arrDibujoPExt.push({scd_codigo:htmlPEX,id: contParedes});
        contParedes++;
        cont=1;
        for (var i=0; i<arrDibujoPExt.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPExt[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#aparedesexteriores').html(cadena); 
        var conto=0;
        arrDibujoPExt.forEach(function (item, index, array) 
        { 
          $("#estadPEx"+item.id+"").val(arrCodigoPExt[conto].val_est_con);
          $("#intePEx"+item.id+"").val(arrCodigoPExt[conto].val_int);
          $("#tipPEx"+item.id+"").val(arrCodigoPExt[conto].val_tipo);
          $("#matPEx"+item.id+"").val(arrCodigoPExt[conto].val_mat);
          conto=conto+1;
        });
        console.log("arrCodigoPExtDentroAct",arrCodigoPExt);
}

function eliminarParedesExteriores($id){
   if(contParedes>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoPExt.forEach(function (item, index, array) 
                {
                  arrCodigoPExt[cont].val_est_con = $("#estadPEx"+item.id+"").val();
                  arrCodigoPExt[cont].val_int = $("#intePEx"+item.id+"").val();
                  arrCodigoPExt[cont].val_tipo = $("#tipPEx"+item.id+"").val();
                  arrCodigoPExt[cont].val_mat = $("#matPEx"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoPExt.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoPExt.splice(pos,1);
                arrDibujoPExt.splice(pos,1);
                //////console.log("arrPEX",arrPEX);
                
                cont=1;
                for (var i=0; i<arrDibujoPExt.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPExt[i].scd_codigo;
                    cont++;
                }
                $('#aparedesexteriores').html(cadena); 
               var conto=0;
                arrDibujoPExt.forEach(function (item, index, array) 
                { 
                  $("#estadPEx"+item.id+"").val(arrCodigoPExt[conto].val_est_con);
                  $("#intePEx"+item.id+"").val(arrCodigoPExt[conto].val_int);
                  $("#tipPEx"+item.id+"").val(arrCodigoPExt[conto].val_tipo);
                  $("#matPEx"+item.id+"").val(arrCodigoPExt[conto].val_mat);
                  conto=conto+1;
                });
            });
        }
}
//-----fin grilla paredes exteriores---------------------------------
//-------------COBERTURA DEL TECHO-------------------------
    
 var contCT=2;
  var arrCT=[];
  var arrDibujoCT=[];

    htmlCT='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCT1" name="estadocCT1">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCT1" name="integridadCT1">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCT1" name="tipoCT1">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Escama de Pez">Escama de Pez</option>'+
              '<option value="Enchape">Enchape</option>'+
              '<option value="Techumbre">Techumbre</option>'+
              '<option value="Acabado">Acabado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCT1" name="materialCT1">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Calamina Plástica">Calamina Plástica</option>'+
            '<option value="Paja">Paja</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Policarbonato">Policarbonato</option>'+
            '<option value="Aceite<">Aceite</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Barro">Barro</option>'+
            '<option value="Fibro Cemento">Fibro Cemento</option>'+
            '<option value="Placa de Zinc">Placa de Zinc</option>'+
            '<option value="Hojas de Palma">Hojas de Palma</option>'+
            '<option value="Placa de Cobre">Placa de Cobre</option>'+
            '<option value="Teja Artesanal">Teja Artesanal</option>'+
            '<option value="Calamina Metálica">Calamina Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cobertura del Techo" onclick="darBajaCT(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrCT.push({val_est_con:'CE', val_inte:'CI', val_tipo:'CT', val_mate:'CM'});
    arrDibujoCT.push({scd_codigo:htmlCT,id: 1});
    ////console.log("arrCT",arrCT);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoCT[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#ccoberturadeltecho').html(cadena);
    $("#estadocCT1").val(arrCT[0].val_est_con);
    $("#integridadCT1").val(arrCT[0].val_inte);
    $("#tipoCT1").val(arrCT[0].val_tipo);
    $("#materialCT1").val(arrCT[0].val_mate);

  function crearCoberturaTecho(){
    var htmlCT;
        htmlCT='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCT'+contCT+'" name="estadocCT'+contCT+'">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCT'+contCT+'" name="integridadCT'+contCT+'">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCT'+contCT+'" name="tipoCT'+contCT+'">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Escama de Pez">Escama de Pez</option>'+
              '<option value="Enchape">Enchape</option>'+
              '<option value="Techumbre">Techumbre</option>'+
              '<option value="Acabado">Acabado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCT'+contCT+'" name="materialCT'+contCT+'">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Calamina Plástica">Calamina Plástica</option>'+
            '<option value="Paja">Paja</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Policarbonato">Policarbonato</option>'+
            '<option value="Aceite<">Aceite</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Barro">Barro</option>'+
            '<option value="Fibro Cemento">Fibro Cemento</option>'+
            '<option value="Placa de Zinc">Placa de Zinc</option>'+
            '<option value="Hojas de Palma">Hojas de Palma</option>'+
            '<option value="Placa de Cobre">Placa de Cobre</option>'+
            '<option value="Teja Artesanal">Teja Artesanal</option>'+
            '<option value="Calamina Metálica">Calamina Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cobertura del Techo" onclick="darBajaCT('+contCT+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoCT.forEach(function (item, index, array) 
        {
          arrCT[cont].val_est_con = $("#estadocCT"+item.id+"").val();
          arrCT[cont].val_inte = $("#integridadCT"+item.id+"").val();
          arrCT[cont].val_tipo = $("#tipoCT"+item.id+"").val();
          arrCT[cont].val_mate = $("#materialCT"+item.id+"").val();
          cont++;
        });
        arrCT.push({val_est_con:'CE', val_inte:'CI', val_tipo:'CT', val_mate:'CM'});
        arrDibujoCT.push({scd_codigo:htmlCT,id: contCT});
        contCT++;
        cont=1;
        for (var i=0; i<arrDibujoCT.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoCT[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#ccoberturadeltecho').html(cadena); 
        var conto=0;
        arrDibujoCT.forEach(function (item, index, array) 
        { 
          $("#estadocCT"+item.id+"").val(arrCT[conto].val_est_con);
          $("#integridadCT"+item.id+"").val(arrCT[conto].val_inte);
          $("#tipoCT"+item.id+"").val(arrCT[conto].val_tipo);
          $("#materialCT"+item.id+"").val(arrCT[conto].val_mate);
          conto=conto+1;
        });
        
    }

function darBajaCT($id){
        if(contCT>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoCT.forEach(function (item, index, array) 
                {
                    arrCT[cont].val_est_con=$("#estadocCT"+item.id+"").val();
                    arrCT[cont].val_inte=$("#integridadCT"+item.id+"").val();
                    arrCT[cont].val_tipo=$("#tipoCT"+item.id+"").val();
                    arrCT[cont].val_mate=$("#materialCT"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoCT.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCT.splice(pos,1);
                arrDibujoCT.splice(pos,1);
                ////console.log("arrCT",arrCT);
                
                cont=1;
                for (var i=0; i<arrDibujoCT.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoCT[i].scd_codigo;
                    cont++;
                }
                
                $('#ccoberturadeltecho').html(cadena);
                
                cont=0;
                arrDibujoCT.forEach(function (item, index, array) 
                {
                    $("#estadocCT"+item.id+"").val(arrCT[cont].val_est_con);
                    $("#integridadCT"+item.id+"").val(arrCT[cont].val_inte);
                    $("#tipoCT"+item.id+"").val(arrCT[cont].val_tipo);
                    $("#materialCT"+item.id+"").val(arrCT[cont].val_mate);
                    cont=cont+1;
                });
            });
        }
        
    }
//-----------GRILLA PARA PAREDES INTERIORES-----------

var contPI=2;
  var arrPI=[];
  var arrDibujoPI=[];

    htmlPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPI1" name="estadocPI1">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPI1" name="integridadPI1">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPI1" name="tipoPI1">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPI1" name="materialPI1">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Interiores" onclick="darBajaPI(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrPI.push({val_est_conser:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
    arrDibujoPI.push({scd_codigo:htmlPI,id: 1});
    ////console.log("arrPI",arrPI);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoPI[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cparedesinteriores').html(cadena);
    $("#estadocPI1").val(arrPI[0].val_est_conser);
    $("#integridadPI1").val(arrPI[0].val_int);
    $("#tipoPI1").val(arrPI[0].val_tipo);
    $("#materialPI1").val(arrPI[0].val_mat);

function crearParedesInteriores(){
    var htmlPI;
        htmlPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPI'+contPI+'" name="estadocPI'+contPI+'">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPI'+contPI+'" name="integridadPI'+contPI+'">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPI'+contPI+'" name="tipoPI'+contPI+'">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPI'+contPI+'" name="materialPI'+contPI+'">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Interiores" onclick="darBajaPI('+contPI+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPI.forEach(function (item, index, array) 
        {
          arrPI[cont].val_est_conser = $("#estadocPI"+item.id+"").val();
          arrPI[cont].val_int = $("#integridadPI"+item.id+"").val();
          arrPI[cont].val_tipo = $("#tipoPI"+item.id+"").val();
          arrPI[cont].val_mat = $("#materialPI"+item.id+"").val();
          cont++;
        });
        arrPI.push({val_est_conser:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
        arrDibujoPI.push({scd_codigo:htmlPI,id: contPI});
        contPI++;
        cont=1;
        for (var i=0; i<arrDibujoPI.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPI[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#cparedesinteriores').html(cadena); 
        var conto=0;
        arrDibujoPI.forEach(function (item, index, array) 
        { 
          $("#estadocPI"+item.id+"").val(arrPI[conto].val_est_conser);
          $("#integridadPI"+item.id+"").val(arrPI[conto].val_int);
          $("#tipoPI"+item.id+"").val(arrPI[conto].val_tipo);
          $("#materialPI"+item.id+"").val(arrPI[conto].val_mat);
          conto=conto+1;
        });
        
    }



function darBajaPI($id){
        if(contPI>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoPI.forEach(function (item, index, array) 
                {
                    arrPI[cont].val_est_conser=$("#estadocPI"+item.id+"").val();
                    arrPI[cont].val_int=$("#integridadPI"+item.id+"").val();
                    arrPI[cont].val_tipo=$("#tipoPI"+item.id+"").val();
                    arrPI[cont].val_mat=$("#materialPI"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoPI.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrPI.splice(pos,1);
                arrDibujoPI.splice(pos,1);
                ////console.log("arrPI",arrPI);
                
                cont=1;
                for (var i=0; i<arrDibujoPI.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPI[i].scd_codigo;
                    cont++;
                }
                
                $('#cparedesinteriores').html(cadena);
                
                cont=0;
                arrDibujoPI.forEach(function (item, index, array) 
                {
                    $("#estadocPI"+item.id+"").val(arrPI[cont].val_est_conser);
                    $("#integridadPI"+item.id+"").val(arrPI[cont].val_int);
                    $("#tipoPI"+item.id+"").val(arrPI[cont].val_tipo);
                    $("#materialPI"+item.id+"").val(arrPI[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
var arrDibujoPInt = [];
var arrCodigoPInt = [];
var contPInt = 0;


function editarParedesInteriores(longitudGrilla){
  contPInt = 1;
  arrDibujoPInt=[];
  var htmlPI;
  for (var i=1; i<longitudGrilla;i++){
          htmlPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPIn'+contPInt+'" >'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPIn'+contPInt+'">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPIn'+contPInt+'" >'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPIn'+contPInt+'" >'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Interiores" onclick="eliminarParedesInteriores('+contPInt+');"></i>'+
      '</a></div>';
        arrDibujoPInt.push({scd_codigo:htmlPI,id:contPInt});
        }
        contPInt++;
        var cadena = '';
        var cont = 1;
        for (var i = 0; i<arrDibujoPInt.length;i++)
        {
          cadena =cadena + '<div class="form-group col-md-3"><label class="control-label">' + cont + '. '+arrDibujoPInt[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaCTecaa",cadena);
        $('#aparedesinteriores').html(cadena);
}

function actualizarParedesInteriores(){
      var htmlPI;
        htmlPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocPIn'+contPInt+'" >'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadPIn'+contPInt+'" >'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoPIn'+contPInt+'" >'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Aislado">Aislado</option>'+
              '<option value="Portante">Portante</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialPIn'+contPInt+'" >'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Tapial">Tapial</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Aceite">Aceite</option>'+
            '<option value="Ladrillo Hueco">Ladrillo Hueco</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Quincha">Quincha</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Paredes Interiores" onclick="eliminarParedesInteriores('+contPInt+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPInt.forEach(function (item, index, array) 
        {
          arrCodigoPInt[cont].val_est_conser = $("#estadocPIn"+item.id+"").val();
          arrCodigoPInt[cont].val_int = $("#integridadPIn"+item.id+"").val();
          arrCodigoPInt[cont].val_tipo = $("#tipoPIn"+item.id+"").val();
          arrCodigoPInt[cont].val_mat = $("#materialPIn"+item.id+"").val();
          cont++;
        });
        arrCodigoPInt.push({val_est_conser:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
        arrDibujoPInt.push({scd_codigo:htmlPI,id: contPInt});
        contPInt++;
        cont=1;
        for (var i=0; i<arrDibujoPInt.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPInt[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#aparedesinteriores').html(cadena); 
        var conto=0;
        arrDibujoPInt.forEach(function (item, index, array) 
        { 

          $("#estadocPIn"+item.id+"").val(arrCodigoPInt[conto].val_est_conser);
          $("#integridadPIn"+item.id+"").val(arrCodigoPInt[conto].val_int);
          $("#tipoPIn"+item.id+"").val(arrCodigoPInt[conto].val_tipo);
          $("#materialPIn"+item.id+"").val(arrCodigoPInt[conto].val_mat);
          conto=conto+1;
        });

}

function eliminarParedesInteriores($id){
        if(contPInt>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var pos=-1;
                var cont=0;
                arrDibujoPInt.forEach(function (item, index, array) 
                {
                  arrCodigoPInt[cont].val_est_conser = $("#estadocPIn"+item.id+"").val();
                  arrCodigoPInt[cont].val_int = $("#integridadPIn"+item.id+"").val();
                  arrCodigoPInt[cont].val_tipo = $("#tipoPIn"+item.id+"").val();
                  arrCodigoPInt[cont].val_mat = $("#materialPIn"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoPInt.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoPInt.splice(pos,1);
                arrDibujoPInt.splice(pos,1);
                //////console.log("arrPI",arrPI);
                
                cont=1;
                for (var i=0; i<arrDibujoPInt.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPInt[i].scd_codigo;
                    cont++;
                }
                
                $('#aparedesinteriores').html(cadena);
                
                var conto=0;
                arrDibujoPInt.forEach(function (item, index, array) 
                { 
                  $("#estadocPIn"+item.id+"").val(arrCodigoPInt[conto].val_est_conser);
                  $("#integridadPIn"+item.id+"").val(arrCodigoPInt[conto].val_int);
                  $("#tipoPIn"+item.id+"").val(arrCodigoPInt[conto].val_tipo);
                  $("#materialPIn"+item.id+"").val(arrCodigoPInt[conto].val_mat);
                  conto=conto+1;
                });
            });
        }    
    }


//--------fin grilla paredes interiores-----------------------------
//-----Inicio grilla cobertura techo---------------------------------

var arrDibujoCTec=[];
var arrCodigoCTec=[];
var contCT = 0;
function editarCoberturaTecho(longitudGrilla){
    var contCTec=1;
    arrDibujoCTec=[];
    var htmCTec;
    for (var i=1; i<longitudGrilla;i++){
            htmCTec='Estado de conservación:</label>'+
            '<select class="form-control" id="estadocCTe'+contCTec+'" name="estadocCTe'+contCTec+'">'+
              '<option value="CE" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="integridadCTec'+contCTec+'" name="integridadCTec'+contCTec+'">'+
              '<option value="CI" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipoCTec'+contCTec+'" name="tipoCTec'+contCTec+'">'+
               '<option value="CT" selected="selected">--Seleccione--</option>'+
                '<option value="Escama de Pez">Escama de Pez</option>'+
                '<option value="Enchape">Enchape</option>'+
                '<option value="Techumbre">Techumbre</option>'+
                '<option value="Acabado">Acabado</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="materialCTec'+contCTec+'" name="materialCTec'+contCTec+'">'+
              '<option value="CM" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Calamina Plástica">Calamina Plástica</option>'+
              '<option value="Paja">Paja</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Aceite<">Aceite</option>'+
              '<option value="Losa">Losa</option>'+
              '<option value="Barro">Barro</option>'+
              '<option value="Fibro Cemento">Fibro Cemento</option>'+
              '<option value="Placa de Zinc">Placa de Zinc</option>'+
              '<option value="Hojas de Palma">Hojas de Palma</option>'+
              '<option value="Placa de Cobre">Placa de Cobre</option>'+
              '<option value="Teja Artesanal">Teja Artesanal</option>'+
              '<option value="Calamina Metálica">Calamina Metálica</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cobertura del Techo" onclick="eliminarCoberturaTecho('+contCT+');"></i>'+
        '</a></div>';
            arrDibujoCTec.push({scd_codigo:htmCTec,id: contCTec});
            contCTec++;
          }
          var cadena='';
          var cont=1;
          for (var i=0; i<arrDibujoCTec.length;i++)
          {
            cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">' + cont + '. '+arrDibujoCTec[i].scd_codigo;
            cont++;
          }
          //////console.log("cadenaCTecaa",cadena);
          $('#acoberturadeltecho').html(cadena);
}
function actualizarCoberturaTecho(){
    var htmlCT;
        htmlCT='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCTe'+contCT+'" >'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCTec'+contCT+'">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCTec'+contCT+'" >'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Escama de Pez">Escama de Pez</option>'+
              '<option value="Enchape">Enchape</option>'+
              '<option value="Techumbre">Techumbre</option>'+
              '<option value="Acabado">Acabado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCTec'+contCT+'" >'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Calamina Plástica">Calamina Plástica</option>'+
            '<option value="Paja">Paja</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Policarbonato">Policarbonato</option>'+
            '<option value="Aceite<">Aceite</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Barro">Barro</option>'+
            '<option value="Fibro Cemento">Fibro Cemento</option>'+
            '<option value="Placa de Zinc">Placa de Zinc</option>'+
            '<option value="Hojas de Palma">Hojas de Palma</option>'+
            '<option value="Placa de Cobre">Placa de Cobre</option>'+
            '<option value="Teja Artesanal">Teja Artesanal</option>'+
            '<option value="Calamina Metálica">Calamina Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cobertura del Techo" onclick="eliminarCoberturaTecho('+contCT+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoCTec.forEach(function (item, index, array) 
        {
          arrCodigoCTec[cont].val_est_con = $("#estadocCTe"+item.id+"").val();
          arrCodigoCTec[cont].val_inte = $("#integridadCTec"+item.id+"").val();
          arrCodigoCTec[cont].val_tipo = $("#tipoCTec"+item.id+"").val();
          arrCodigoCTec[cont].val_mate = $("#materialCTec"+item.id+"").val();
          cont++;
        });
        arrCodigoCTec.push({val_est_con:'CE', val_inte:'CI', val_tipo:'CT', val_mate:'CM'});
        arrDibujoCTec.push({scd_codigo:htmlCT,id: contCT});
        contCT++;
        cont=1;
        for (var i=0; i<arrDibujoCTec.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoCTec[i].scd_codigo;
          cont++;
        }
        $('#acoberturadeltecho').html(cadena); 
        var conto=0;
        arrDibujoCTec.forEach(function (item, index, array) 
        { 
          $("#estadocCTe"+item.id+"").val(arrCodigoCTec[conto].val_est_con);
          $("#integridadCTec"+item.id+"").val(arrCodigoCTec[conto].val_inte);
          $("#tipoCTec"+item.id+"").val(arrCodigoCTec[conto].val_tipo);
          $("#materialCTec"+item.id+"").val(arrCodigoCTec[conto].val_mate);
          conto=conto+1;
        });
}
function eliminarCoberturaTecho($id){
    if(contCT>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                 var cont=0;
                arrDibujoCTec.forEach(function (item, index, array) 
                {
                  arrCodigoCTec[cont].val_est_con = $("#estadocCTe"+item.id+"").val();
                  arrCodigoCTec[cont].val_inte = $("#integridadCTec"+item.id+"").val();
                  arrCodigoCTec[cont].val_tipo = $("#tipoCTec"+item.id+"").val();
                  arrCodigoCTec[cont].val_mate = $("#materialCTec"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoCTec.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoCTec.splice(pos,1);
                arrDibujoCTec.splice(pos,1);
                //////console.log("arrCT",arrCT);
                
                cont=1;
                for (var i=0; i<arrDibujoCTec.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoCTec[i].scd_codigo;
                    cont++;
                }
                
                $('#acoberturadeltecho').html(cadena);
                
                 var conto=0;
                arrDibujoCTec.forEach(function (item, index, array) 
                { 
                  $("#estadocCT"+item.id+"").val(arrCodigoCTec[conto].val_est_con);
                  $("#integridadCTec"+item.id+"").val(arrCodigoCTec[conto].val_inte);
                  $("#tipoCTec"+item.id+"").val(arrCodigoCTec[conto].val_tipo);
                  $("#materialCTec"+item.id+"").val(arrCodigoCTec[conto].val_mate);
                  conto=conto+1;
                });
            });
        }

}

//------fin grilla cobertura techo------------------------------
//-------------GRILLA ENTRE PISOS---------------------------

var contEPI=2;
  var arrEPI=[];
  var arrDibujoEPI=[];

    htmlEPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEPI1" name="estadocEPI1">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEPI1" name="integridadEPI1">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEPI1" name="tipoEPI1">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Envigado">Envigado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEPI1" name="materialEPI1">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Piso Técnico">Piso Técnico</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Perfil Metálico">Perfil Metálico</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Entre Pisos" onclick="darBajaEPI(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrEPI.push({val_esta:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
    arrDibujoEPI.push({scd_codigo:htmlEPI,id: 1});
    ////console.log("arrEPI",arrEPI);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoEPI[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#centrepisos').html(cadena);
    $("#estadocEPI1").val(arrEPI[0].val_esta);
    $("#integridadEPI1").val(arrEPI[0].val_int);
    $("#tipoEPI1").val(arrEPI[0].val_tipo);
    $("#materialEPI1").val(arrEPI[0].val_mat);



function crearEntrePisos(){
    var htmlEPI;
        htmlEPI='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEPI'+contEPI+'" name="estadocEPI'+contEPI+'">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEPI'+contEPI+'" name="integridadEPI'+contEPI+'">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEPI'+contEPI+'" name="tipoEPI'+contEPI+'">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Envigado">Envigado</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEPI'+contEPI+'" name="materialEPI'+contEPI+'">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Prefabricados">Prefabricados</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Piso Técnico">Piso Técnico</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Losa">Losa</option>'+
            '<option value="Perfil Metálico">Perfil Metálico</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Entre Pisos" onclick="darBajaEPI('+contEPI+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoEPI.forEach(function (item, index, array) 
        {
          arrEPI[cont].val_esta = $("#estadocEPI"+item.id+"").val();
          arrEPI[cont].val_int = $("#integridadEPI"+item.id+"").val();
          arrEPI[cont].val_tipo = $("#tipoEPI"+item.id+"").val();
          arrEPI[cont].val_mat = $("#materialEPI"+item.id+"").val();
          cont++;
        });
        arrEPI.push({val_esta:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoEPI.push({scd_codigo:htmlEPI,id: contEPI});
        contEPI++;
        cont=1;
        for (var i=0; i<arrDibujoEPI.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEPI[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#centrepisos').html(cadena); 
        var conto=0;
        arrDibujoEPI.forEach(function (item, index, array) 
        { 
          $("#estadocEPI"+item.id+"").val(arrEPI[conto].val_esta);
          $("#integridadEPI"+item.id+"").val(arrEPI[conto].val_int);
          $("#tipoEPI"+item.id+"").val(arrEPI[conto].val_tipo);
          $("#materialEPI"+item.id+"").val(arrEPI[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaEPI($id){
        if(contEPI>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoEPI.forEach(function (item, index, array) 
                {
                    arrEPI[cont].val_esta=$("#estadocEPI"+item.id+"").val();
                    arrEPI[cont].val_int=$("#integridadEPI"+item.id+"").val();
                    arrEPI[cont].val_tipo=$("#tipoEPI"+item.id+"").val();
                    arrEPI[cont].val_mat=$("#materialEPI"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoEPI.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrEPI.splice(pos,1);
                arrDibujoEPI.splice(pos,1);
                ////console.log("arrEPI",arrEPI);
                
                cont=1;
                for (var i=0; i<arrDibujoEPI.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEPI[i].scd_codigo;
                    cont++;
                }
                
                $('#centrepisos').html(cadena);
                
                cont=0;
                arrDibujoEPI.forEach(function (item, index, array) 
                {
                    $("#estadocEPI"+item.id+"").val(arrEPI[cont].val_esta);
                    $("#integridadEPI"+item.id+"").val(arrEPI[cont].val_int);
                    $("#tipoEPI"+item.id+"").val(arrEPI[cont].val_tipo);
                    $("#materialEPI"+item.id+"").val(arrEPI[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
//-------inicio grilla entre pisos----------------------------------

var arrDibujoEP=[];
var arrCodigoEP=[];
var contEPisos=0;

function editarEntrePisos(longitudGrilla){
    contEPisos=1;
    arrDibujoEP=[];
    var htmlEPI;
    for (var i=1; i<longitudGrilla;i++)
          {
        htmlEPI='Estado de conservación:</label>'+
            '<select class="form-control" id="estadocEnP'+contEPisos+'" name="estadocEnP'+contEPisos+'">'+
              '<option value="EE" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="integridadEnP'+contEPisos+'" name="integridadEnP'+contEPisos+'">'+
              '<option value="EI" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipoEnP'+contEPisos+'" name="tipoEnP'+contEPisos+'">'+
               '<option value="ET" selected="selected">--Seleccione--</option>'+
                '<option value="Bovedilla">Bovedilla</option>'+
                '<option value="Bóveda">Bóveda</option>'+
                '<option value="Envigado">Envigado</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="materialEnP'+contEPisos+'" name="materialEnP'+contEPisos+'">'+
              '<option value="EM" selected="selected">--Seleccione--</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Prefabricados">Prefabricados</option>'+
              '<option value="Madera Rolliza">Madera Rolliza</option>'+
              '<option value="Madera Escuadria">Madera Escuadria</option>'+
              '<option value="Piso Técnico">Piso Técnico</option>'+
              '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
              '<option value="Losa">Losa</option>'+
              '<option value="Perfil Metálico">Perfil Metálico</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Entre Pisos" onclick="eliminarEntrePisos('+contEPisos+');"></i>'+
        '</a></div>';
          arrDibujoEP.push({scd_codigo:htmlEPI,id: contEPisos});
          contEPisos++;
          }
          var cadena = '';
          var cont = 1;
          for (var i=0; i<arrDibujoEP.length;i++)
          {
            cadena = cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEP[i].scd_codigo;
            cont++;
          }
          //////console.log("cadenaEPaa",cadena);
          $('#aentrepisos').html(cadena);
     }

    function actualizarEntrePisos(){
      var htmlEPI;
       htmlEPI='Estado de conservación:</label>'+
            '<select class="form-control" id="estadocEnP'+contEPisos+'" name="estadocEnP'+contEPisos+'">'+
              '<option value="EE" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="integridadEnP'+contEPisos+'" name="integridadEnP'+contEPisos+'">'+
              '<option value="EI" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipoEnP'+contEPisos+'" name="tipoEnP'+contEPisos+'">'+
               '<option value="ET" selected="selected">--Seleccione--</option>'+
                '<option value="Bovedilla">Bovedilla</option>'+
                '<option value="Bóveda">Bóveda</option>'+
                '<option value="Envigado">Envigado</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="materialEnP'+contEPisos+'" name="materialEnP'+contEPisos+'">'+
              '<option value="EM" selected="selected">--Seleccione--</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Prefabricados">Prefabricados</option>'+
              '<option value="Madera Rolliza">Madera Rolliza</option>'+
              '<option value="Madera Escuadria">Madera Escuadria</option>'+
              '<option value="Piso Técnico">Piso Técnico</option>'+
              '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
              '<option value="Losa">Losa</option>'+
              '<option value="Perfil Metálico">Perfil Metálico</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Entre Pisos" onclick="eliminarEntrePisos('+contEPisos+');"></i>'+
        '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoEP.forEach(function (item, index, array) 
        {
          arrCodigoEP[cont].val_esta = $("#estadocEnP"+item.id+"").val();
          arrCodigoEP[cont].val_int = $("#integridadEnP"+item.id+"").val();
          arrCodigoEP[cont].val_tipo = $("#tipoEnP"+item.id+"").val();
          arrCodigoEP[cont].val_mat = $("#materialEnP"+item.id+"").val();
          cont++;
        });
        arrCodigoEP.push({val_esta:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoEP.push({scd_codigo:htmlEPI,id: contEPisos});
        contEPisos++;
        cont=1;
        for (var i=0; i<arrDibujoEP.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEP[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#aentrepisos').html(cadena); 
        var conto=0;
        arrDibujoEP.forEach(function (item, index, array) 
        { 
          $("#estadocEnP"+item.id+"").val(arrCodigoEP[conto].val_esta);
          $("#integridadEnP"+item.id+"").val(arrCodigoEP[conto].val_int);
          $("#tipoEnP"+item.id+"").val(arrCodigoEP[conto].val_tipo);
          $("#materialEnP"+item.id+"").val(arrCodigoEP[conto].val_mat);
          conto=conto+1;
        });

    }
    function eliminarEntrePisos($id){
        if(contEPisos>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var pos=-1;
                var cont=0;
                arrDibujoEP.forEach(function (item, index, array) 
                {
                  arrCodigoEP[cont].val_esta = $("#estadocEnP"+item.id+"").val();
                  arrCodigoEP[cont].val_int = $("#integridadEnP"+item.id+"").val();
                  arrCodigoEP[cont].val_tipo = $("#tipoEnP"+item.id+"").val();
                  arrCodigoEP[cont].val_mat = $("#materialEnP"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoEP.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoEP.splice(pos,1);
                arrDibujoEP.splice(pos,1);
                //////console.log("arrEPI",arrEPI);
                
                cont=1;
                for (var i=0; i<arrDibujoEP.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEP[i].scd_codigo;
                    cont++;
                }
                $('#aentrepisos').html(cadena);
                var conto=0;
                arrDibujoEP.forEach(function (item, index, array) 
                { 
                  $("#estadocEnP"+item.id+"").val(arrCodigoEP[conto].val_esta);
                  $("#integridadEnP"+item.id+"").val(arrCodigoEP[conto].val_int);
                  $("#tipoEnP"+item.id+"").val(arrCodigoEP[conto].val_tipo);
                  $("#materialEnP"+item.id+"").val(arrCodigoEP[conto].val_mat);
                  conto=conto+1;
                });
            });
        }
        
    }
//------fin grilla entre pisos--------------------------------------
//---------GRILLA ESCALERAS------------------------

var contES=2;
  var arrES=[];
  var arrDibujoES=[];

    htmlES='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocES1" name="estadocES1">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadES1" name="integridadES1">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoES1" name="tipoES1">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Recta">Recta</option>'+
              '<option value="Imperial">Imperial</option>'+
              '<option value="Caracol">Caracol</option>'+
              '<option value="U">U</option>'+
              '<option value="L">L</option>'+
              '<option value="Elicodal">Elicodal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialES1" name="materialES1">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
            '<option value="Marmol">Marmol</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaES(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrES.push({val_estado:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
    arrDibujoES.push({scd_codigo:htmlES,id: 1});
    ////console.log("arrES",arrES);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoES[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cescaleras').html(cadena);
    $("#estadocES1").val(arrES[0].val_estado);
    $("#integridadES1").val(arrES[0].val_int);
    $("#tipoES1").val(arrES[0].val_tipo);
    $("#materialES1").val(arrES[0].val_mat);

function crearEscalera(){
    var htmlES;
        htmlES='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocES'+contES+'" name="estadocES'+contES+'">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadES'+contES+'" name="integridadES'+contES+'">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoES'+contES+'" name="tipoES'+contES+'">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Recta">Recta</option>'+
              '<option value="Imperial">Imperial</option>'+
              '<option value="Caracol">Caracol</option>'+
              '<option value="U">U</option>'+
              '<option value="L">L</option>'+
              '<option value="Elicodal">Elicodal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialES'+contES+'" name="materialES'+contES+'">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
            '<option value="Marmol">Marmol</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaES('+contES+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoES.forEach(function (item, index, array) 
        {
          arrES[cont].val_estado = $("#estadocES"+item.id+"").val();
          arrES[cont].val_int = $("#integridadES"+item.id+"").val();
          arrES[cont].val_tipo = $("#tipoES"+item.id+"").val();
          arrES[cont].val_mat = $("#materialES"+item.id+"").val();
          cont++;
        });
        arrES.push({val_estado:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoES.push({scd_codigo:htmlES,id: contES});
        contES++;
        cont=1;
        for (var i=0; i<arrDibujoES.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoES[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#cescaleras').html(cadena); 
        var conto=0;
        arrDibujoES.forEach(function (item, index, array) 
        { 
          $("#estadocES"+item.id+"").val(arrES[conto].val_estado);
          $("#integridadES"+item.id+"").val(arrES[conto].val_int);
          $("#tipoES"+item.id+"").val(arrES[conto].val_tipo);
          $("#materialES"+item.id+"").val(arrES[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaES($id){
        if(contES>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoES.forEach(function (item, index, array) 
                {
                    arrES[cont].val_estado=$("#estadocES"+item.id+"").val();
                    arrES[cont].val_int=$("#integridadES"+item.id+"").val();
                    arrES[cont].val_tipo=$("#tipoES"+item.id+"").val();
                    arrES[cont].val_mat=$("#materialES"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoES.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrES.splice(pos,1);
                arrDibujoES.splice(pos,1);
                ////console.log("arrES",arrES);
                
                cont=1;
                for (var i=0; i<arrDibujoES.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoES[i].scd_codigo;
                    cont++;
                }
                
                $('#cescaleras').html(cadena);
                
                cont=0;
                arrDibujoES.forEach(function (item, index, array) 
                {
                    $("#estadocES"+item.id+"").val(arrES[cont].val_estado);
                    $("#integridadES"+item.id+"").val(arrES[cont].val_int);
                    $("#tipoES"+item.id+"").val(arrES[cont].val_tipo);
                    $("#materialES"+item.id+"").val(arrES[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
 //------Inicio grilla escalares-------------------------------------
      var contEsca = 0;
      var arrDibujoEsc =[];
      var arrCodigoEsc =[];

function editarEscaleras(longitudGrilla){
      contEsca=1;
      arrDibujoEsc=[];
      var htmlES;

      for (var i=1; i<longitudGrilla;i++)
            {
          htmlES ='Estado de conservación:</label>'+
              '<select class="form-control" id="estadocEs'+contEsca+'" name="estadocEs'+contEsca+'">'+
                '<option value="EE" selected="selected">--Seleccione--</option>'+
                '<option value="Bueno">Bueno</option>'+
                '<option value="Regular">Regular</option>'+
                '<option value="Malo">Malo</option>'+
              '</select>'+
          '</div>'+
          '<div class="form-group col-md-3">'+
            '<label class="control-label">Integridad:</label>'+
              '<select class="form-control" id="integridadEs'+contEsca+'" name="integridadEs'+contEsca+'">'+
                '<option value="EI" selected="selected">--Seleccione--</option>'+
                '<option value="Original">Original</option>'+
                '<option value="Nuevo">Nuevo</option>'+
                '<option value="Modificado">Modificado</option>'+
              '</select>'+
            '</div>'+
          '<div class="form-group col-md-3">'+
           '<label class="control-label">Tipo:</label>'+
              '<select class="form-control" id="tipoEs'+contEsca+'" name="tipoEs'+contEsca+'">'+
                 '<option value="ET" selected="selected">--Seleccione--</option>'+
                  '<option value="Recta">Recta</option>'+
                  '<option value="Imperial">Imperial</option>'+
                  '<option value="Caracol">Caracol</option>'+
                  '<option value="U">U</option>'+
                  '<option value="L">L</option>'+
                  '<option value="Elicodal">Elicodal</option>'+
              '</select>'+
          '</div>'+
           '<div class="form-group col-md-2">'+
            '<label class="control-label">Materiales:</label>'+
              '<select class="form-control" id="materialEs'+contEsca+'" name="materialEs'+contEsca+'">'+
                '<option value="EM" selected="selected">--Seleccione--</option>'+
                '<option value="Piedra">Piedra</option>'+
                '<option value="Madera">Madera</option>'+
                '<option value="Ladrillo">Ladrillo</option>'+
                '<option value="Hormigon Armado">Hormigon Armado</option>'+
                '<option value="Concreto">Concreto</option>'+
                '<option value="Metal">Metal</option>'+
                '<option value="Marmol">Marmol</option>'+
              '</select>'+
            '</div>'+
          '<div class="form-group col-md-1">'+
          '<a style="cursor:pointer;" type="button">'+
          '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="eliminarEscaleras('+contEsca+');"></i>'+
          '</a></div>';
           arrDibujoEsc.push({scd_codigo:htmlES,id: contEsca});
            contEsca++;
            }
            var cadena='';
            var cont=1;
            for (var i=0; i<arrDibujoEsc.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEsc[i].scd_codigo;
              cont++;
            }
            //////console.log("cadenaEscaa",cadena);
            $('#aescaleras').html(cadena);
      }
    //--------fin grilla escalares-------------------------------

    function actualizarEscaleras(){
      var htmlES;
          htmlES='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEs'+contEsca+'" >'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEs'+contEsca+'" >'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEs'+contEsca+'" >'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Recta">Recta</option>'+
              '<option value="Imperial">Imperial</option>'+
              '<option value="Caracol">Caracol</option>'+
              '<option value="U">U</option>'+
              '<option value="L">L</option>'+
              '<option value="Elicodal">Elicodal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEs'+contEsca+'" >'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
            '<option value="Marmol">Marmol</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="eliminarEscaleras('+contEsca+');"></i>'+
      '</a></div>';
       
        var cadena='';
        var cont=0;
        arrDibujoEsc.forEach(function (item, index, array) 
        {
          arrCodigoEsc[cont].val_estado = $("#estadocEs"+item.id+"").val();
          arrCodigoEsc[cont].val_int = $("#integridadEs"+item.id+"").val();
          arrCodigoEsc[cont].val_tipo = $("#tipoEs"+item.id+"").val();
          arrCodigoEsc[cont].val_mat = $("#materialEs"+item.id+"").val();
          cont++;
        });
        arrCodigoEsc.push({val_estado:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoEsc.push({scd_codigo:htmlES,id: contEsca});
        contEsca++;
        cont=1;
        for (var i=0; i<arrDibujoEsc.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEsc[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#aescaleras').html(cadena); 
        var conto=0;
        arrDibujoEsc.forEach(function (item, index, array) 
        { 
          $("#estadocEs"+item.id+"").val(arrCodigoEsc[conto].val_estado);
          $("#integridadEs"+item.id+"").val(arrCodigoEsc[conto].val_int);
          $("#tipoEs"+item.id+"").val(arrCodigoEsc[conto].val_tipo);
          $("#materialEs"+item.id+"").val(arrCodigoEsc[conto].val_mat);
          conto=conto+1;
        });
    }
function eliminarEscaleras($id){
        if(contEsca>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var pos=-1;
                var cont=0;
                arrDibujoEsc.forEach(function (item, index, array) 
                {
                  arrCodigoEsc[cont].val_estado = $("#estadocEs"+item.id+"").val();
                  arrCodigoEsc[cont].val_int = $("#integridadEs"+item.id+"").val();
                  arrCodigoEsc[cont].val_tipo = $("#tipoEs"+item.id+"").val();
                  arrCodigoEsc[cont].val_mat = $("#materialEs"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoEsc.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoEsc.splice(pos,1);
                arrDibujoEsc.splice(pos,1);
                //////console.log("arrES",arrES);
                
                cont=1;
                for (var i=0; i<arrDibujoEsc.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEsc[i].scd_codigo;
                    cont++;
                }
                
                $('#aescaleras').html(cadena); 
                var conto=0;
                arrDibujoEsc.forEach(function (item, index, array) 
                { 
                  $("#estadocEs"+item.id+"").val(arrCodigoEsc[conto].val_estado);
                  $("#integridadEs"+item.id+"").val(arrCodigoEsc[conto].val_int);
                  $("#tipoEs"+item.id+"").val(arrCodigoEsc[conto].val_tipo);
                  $("#materialEs"+item.id+"").val(arrCodigoEsc[conto].val_mat);
                  conto=conto+1;
                });
            });
        }
        
    }
//---------------GRILLA CIELO-------------------------------
var contCIE=2;
  var arrCIE=[];
  var arrDibujoCIE=[];

    htmlCIE='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCIE1" name="estadocCIE1">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCIE1" name="integridadCIE1">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCIE1" name="tipoCIE1">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Razo">Razo</option>'+
              '<option value="Falso">Falso</option>'+
              '<option value="Vitral">Vitral</option>'+
              '<option value="Viga Vista">Viga Vista</option>'+
              '<option value="Entraquillado">Entraquillado</option>'+
              '<option value="Tumbadillo">Tumbadillo</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCIE1" name="materialCIE1">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Tela">Tela</option>'+
            '<option value="Panel">Panel</option>'+
            '<option value="Plástico">Plástico</option>'+
            '<option value="Caña hueca entortada">Caña hueca entortada</option>'+
            '<option value="Caña hueca Encalada">Caña hueca Encalada</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Madera y Estuco">Madera y Estuco</option>'+
            '<option value="Malla Metálica">Malla Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cielo" onclick="darBajaCIE(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrCIE.push({val_est:'CE', val_int:'CI', val_tipo:'CT', val_mat:'CM'});
    arrDibujoCIE.push({scd_codigo:htmlCIE,id: 1});
    ////console.log("arrCIE",arrCIE);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoCIE[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#ccielo').html(cadena);
    $("#estadocCIE1").val(arrCIE[0].val_est);
    $("#integridadCIE1").val(arrCIE[0].val_int);
    $("#tipoCIE1").val(arrCIE[0].val_tipo);
    $("#materialCIE1").val(arrCIE[0].val_mat);


function crearCielo(){
    var htmlCIE;
        htmlCIE='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocCIE'+contCIE+'" name="estadocCIE'+contCIE+'">'+
            '<option value="CE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadCIE'+contCIE+'" name="integridadCIE'+contCIE+'">'+
            '<option value="CI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoCIE'+contCIE+'" name="tipoCIE'+contCIE+'">'+
             '<option value="CT" selected="selected">--Seleccione--</option>'+
              '<option value="Razo">Razo</option>'+
              '<option value="Falso">Falso</option>'+
              '<option value="Vitral">Vitral</option>'+
              '<option value="Viga Vista">Viga Vista</option>'+
              '<option value="Entraquillado">Entraquillado</option>'+
              '<option value="Tumbadillo">Tumbadillo</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Bovedilla">Bovedilla</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialCIE'+contCIE+'" name="materialCIE'+contCIE+'">'+
            '<option value="CM" selected="selected">--Seleccione--</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Tela">Tela</option>'+
            '<option value="Panel">Panel</option>'+
            '<option value="Plástico">Plástico</option>'+
            '<option value="Caña hueca entortada">Caña hueca entortada</option>'+
            '<option value="Caña hueca Encalada">Caña hueca Encalada</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Madera y Estuco">Madera y Estuco</option>'+
            '<option value="Malla Metálica">Malla Metálica</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cielo" onclick="darBajaCIE('+contCIE+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoCIE.forEach(function (item, index, array) 
        {
          arrCIE[cont].val_est = $("#estadocCIE"+item.id+"").val();
          arrCIE[cont].val_int = $("#integridadCIE"+item.id+"").val();
          arrCIE[cont].val_tipo = $("#tipoCIE"+item.id+"").val();
          arrCIE[cont].val_mat = $("#materialCIE"+item.id+"").val();
          cont++;
        });
        arrCIE.push({val_est:'CE', val_int:'CI', val_tipo:'CT', val_mat:'CM'});
        arrDibujoCIE.push({scd_codigo:htmlCIE,id: contCIE});
        contCIE++;
        cont=1;
        for (var i=0; i<arrDibujoCIE.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoCIE[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#ccielo').html(cadena); 
        var conto=0;
        arrDibujoCIE.forEach(function (item, index, array) 
        { 
          $("#estadocCIE"+item.id+"").val(arrCIE[conto].val_est);
          $("#integridadCIE"+item.id+"").val(arrCIE[conto].val_int);
          $("#tipoCIE"+item.id+"").val(arrCIE[conto].val_tipo);
          $("#materialCIE"+item.id+"").val(arrCIE[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaCIE($id){
        if(contCIE>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoCIE.forEach(function (item, index, array) 
                {
                    arrCIE[cont].val_est=$("#estadocCIE"+item.id+"").val();
                    arrCIE[cont].val_int=$("#integridadCIE"+item.id+"").val();
                    arrCIE[cont].val_tipo=$("#tipoCIE"+item.id+"").val();
                    arrCIE[cont].val_mat=$("#materialCIE"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoCIE.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCIE.splice(pos,1);
                arrDibujoCIE.splice(pos,1);
                ////console.log("arrCIE",arrCIE);
                
                cont=1;
                for (var i=0; i<arrDibujoCIE.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoCIE[i].scd_codigo;
                    cont++;
                }
                
                $('#ccielo').html(cadena);
                
                cont=0;
                arrDibujoCIE.forEach(function (item, index, array) 
                {
                    $("#estadocCIE"+item.id+"").val(arrCIE[cont].val_est);
                    $("#integridadCIE"+item.id+"").val(arrCIE[cont].val_int);
                    $("#tipoCIE"+item.id+"").val(arrCIE[cont].val_tipo);
                    $("#materialCIE"+item.id+"").val(arrCIE[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
var arrDibujoCielo=[];
    var arrCodigoCielo=[];
    var contCielo=0;
    function editarCielo(longitudGrilla){
    contCielo=1;
    arrDibujoCielo=[];
    var htmlCie;

      for (var i=1; i<longitudGrilla;i++)
            {
          htmlCie='Estado de conservación:</label>'+
              '<select class="form-control" id="estadocCi'+contCielo+'" name="estadocCi'+contCielo+'">'+
                '<option value="CE" selected="selected">--Seleccione--</option>'+
                '<option value="Bueno">Bueno</option>'+
                '<option value="Regular">Regular</option>'+
                '<option value="Malo">Malo</option>'+
              '</select>'+
          '</div>'+
          '<div class="form-group col-md-3">'+
            '<label class="control-label">Integridad:</label>'+
              '<select class="form-control" id="integridadCi'+contCielo+'" name="integridadCi'+contCielo+'">'+
                '<option value="CI" selected="selected">--Seleccione--</option>'+
                '<option value="Original">Original</option>'+
                '<option value="Nuevo">Nuevo</option>'+
                '<option value="Modificado">Modificado</option>'+
              '</select>'+
            '</div>'+
          '<div class="form-group col-md-3">'+
           '<label class="control-label">Tipo:</label>'+
              '<select class="form-control" id="tipoCi'+contCielo+'" name="tipoCi'+contCielo+'">'+
                 '<option value="CT" selected="selected">--Seleccione--</option>'+
                  '<option value="Razo">Razo</option>'+
                  '<option value="Falso">Falso</option>'+
                  '<option value="Vitral">Vitral</option>'+
                  '<option value="Viga Vista">Viga Vista</option>'+
                  '<option value="Entraquillado">Entraquillado</option>'+
                  '<option value="Tumbadillo">Tumbadillo</option>'+
                  '<option value="Bóveda">Bóveda</option>'+
                  '<option value="Bovedilla">Bovedilla</option>'+
              '</select>'+
          '</div>'+
           '<div class="form-group col-md-2">'+
            '<label class="control-label">Materiales:</label>'+
              '<select class="form-control" id="materialCi'+contCielo+'" name="materialCi'+contCielo+'">'+
                '<option value="CM" selected="selected">--Seleccione--</option>'+
                '<option value="Madera">Madera</option>'+
                '<option value="Vidrio">Vidrio</option>'+
                '<option value="Concreto">Concreto</option>'+
                '<option value="Tela">Tela</option>'+
                '<option value="Panel">Panel</option>'+
                '<option value="Plástico">Plástico</option>'+
                '<option value="Caña hueca entortada">Caña hueca entortada</option>'+
                '<option value="Caña hueca Encalada">Caña hueca Encalada</option>'+
                '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
                '<option value="Madera y Estuco">Madera y Estuco</option>'+
                '<option value="Malla Metálica">Malla Metálica</option>'+
              '</select>'+
            '</div>'+
          '<div class="form-group col-md-1">'+
          '<a style="cursor:pointer;" type="button">'+
          '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cielo" onclick="eliminarCielo('+contCielo+');"></i>'+
          '</a></div>';
           
           arrDibujoCielo.push({scd_codigo:htmlCie,id: contCielo});
            contCielo++;
            }
            var cadena='';
            var cont=1;
            for (var i=0; i<arrDibujoCielo.length;i++)
            {
              cadena =cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoCielo[i].scd_codigo;
              cont++;
            }
            //////console.log("cadenaCieloaa",cadena);
        $('#acielo').html(cadena);
        }

  function actualizarCielo(){
    var htmlCie;
    htmlCie='Estado de conservación:</label>'+
              '<select class="form-control" id="estadocCi'+contCielo+'" name="estadocCi'+contCielo+'">'+
                '<option value="CE" selected="selected">--Seleccione--</option>'+
                '<option value="Bueno">Bueno</option>'+
                '<option value="Regular">Regular</option>'+
                '<option value="Malo">Malo</option>'+
              '</select>'+
          '</div>'+
          '<div class="form-group col-md-3">'+
            '<label class="control-label">Integridad:</label>'+
              '<select class="form-control" id="integridadCi'+contCielo+'" name="integridadCi'+contCielo+'">'+
                '<option value="CI" selected="selected">--Seleccione--</option>'+
                '<option value="Original">Original</option>'+
                '<option value="Nuevo">Nuevo</option>'+
                '<option value="Modificado">Modificado</option>'+
              '</select>'+
            '</div>'+
          '<div class="form-group col-md-3">'+
           '<label class="control-label">Tipo:</label>'+
              '<select class="form-control" id="tipoCi'+contCielo+'" name="tipoCi'+contCielo+'">'+
                 '<option value="CT" selected="selected">--Seleccione--</option>'+
                  '<option value="Razo">Razo</option>'+
                  '<option value="Falso">Falso</option>'+
                  '<option value="Vitral">Vitral</option>'+
                  '<option value="Viga Vista">Viga Vista</option>'+
                  '<option value="Entraquillado">Entraquillado</option>'+
                  '<option value="Tumbadillo">Tumbadillo</option>'+
                  '<option value="Bóveda">Bóveda</option>'+
                  '<option value="Bovedilla">Bovedilla</option>'+
              '</select>'+
          '</div>'+
           '<div class="form-group col-md-2">'+
            '<label class="control-label">Materiales:</label>'+
              '<select class="form-control" id="materialCi'+contCielo+'" name="materialCi'+contCielo+'">'+
                '<option value="CM" selected="selected">--Seleccione--</option>'+
                '<option value="Madera">Madera</option>'+
                '<option value="Vidrio">Vidrio</option>'+
                '<option value="Concreto">Concreto</option>'+
                '<option value="Tela">Tela</option>'+
                '<option value="Panel">Panel</option>'+
                '<option value="Plástico">Plástico</option>'+
                '<option value="Caña hueca entortada">Caña hueca entortada</option>'+
                '<option value="Caña hueca Encalada">Caña hueca Encalada</option>'+
                '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
                '<option value="Madera y Estuco">Madera y Estuco</option>'+
                '<option value="Malla Metálica">Malla Metálica</option>'+
              '</select>'+
            '</div>'+
          '<div class="form-group col-md-1">'+
          '<a style="cursor:pointer;" type="button">'+
          '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Cielo" onclick="eliminarCielo('+contCielo+');"></i>'+
          '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoCielo.forEach(function (item, index, array) 
        {
          arrCodigoCielo[cont].val_est = $("#estadocCi"+item.id+"").val();
          arrCodigoCielo[cont].val_int = $("#integridadCi"+item.id+"").val();
          arrCodigoCielo[cont].val_tipo = $("#tipoCi"+item.id+"").val();
          arrCodigoCielo[cont].val_mat = $("#materialCi"+item.id+"").val();
          cont++;
        });
        arrCodigoCielo.push({val_est:'CE', val_int:'CI', val_tipo:'CT', val_mat:'CM'});
        arrDibujoCielo.push({scd_codigo:htmlCie,id: contCielo});
        contCielo++;
        cont=1;
        for (var i=0; i<arrDibujoCielo.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoCielo[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#acielo').html(cadena); 
        var conto=0;
        arrDibujoCielo.forEach(function (item, index, array) 
        { 
          $("#estadocCi"+item.id+"").val(arrCodigoCielo[conto].val_est);
          $("#integridadCi"+item.id+"").val(arrCodigoCielo[conto].val_int);
          $("#tipoCi"+item.id+"").val(arrCodigoCielo[conto].val_tipo);
          $("#materialCi"+item.id+"").val(arrCodigoCielo[conto].val_mat);
          conto=conto+1;
        });
  }

function eliminarCielo($id){
        if(contCielo>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var pos=-1;
                var cont=0;
                arrDibujoCielo.forEach(function (item, index, array) 
                {
                  arrCodigoCielo[cont].val_est = $("#estadocCi"+item.id+"").val();
                  arrCodigoCielo[cont].val_int = $("#integridadCi"+item.id+"").val();
                  arrCodigoCielo[cont].val_tipo = $("#tipoCi"+item.id+"").val();
                  arrCodigoCielo[cont].val_mat = $("#materialCi"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoCielo.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoCielo.splice(pos,1);
                arrDibujoCielo.splice(pos,1);
                //////console.log("arrCIE",arrCIE);
                
                cont=1;
                for (var i=0; i<arrDibujoCielo.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoCielo[i].scd_codigo;
                    cont++;
                }
                
                $('#acielo').html(cadena);
                
                 var conto=0;
                arrDibujoCielo.forEach(function (item, index, array) 
                { 
                  $("#estadocCi"+item.id+"").val(arrCodigoCielo[conto].val_est);
                  $("#integridadCi"+item.id+"").val(arrCodigoCielo[conto].val_int);
                  $("#tipoCi"+item.id+"").val(arrCodigoCielo[conto].val_tipo);
                  $("#materialCi"+item.id+"").val(arrCodigoCielo[conto].val_mat);
                  conto=conto+1;
                });
            });
        }
        
    }

//---------fin grilla cielo-------------------------------------

//--------------GRILLA BALCONES----------------------------
var contBAL=2;
  var arrBAL=[];
  var arrDibujoBAL=[];

    htmlBAL='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocBAL1" name="estadocBAL1">'+
            '<option value="BE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadBAL1" name="integridadBAL1">'+
            '<option value="BI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoBAL1" name="tipoBAL1">'+
             '<option value="BT" selected="selected">--Seleccione--</option>'+
              '<option value="De cajon">De cajon</option>'+
              '<option value="Abierto">Abierto</option>'+
              '<option value="Corrido">Corrido</option>'+
              '<option value="Balconcillo">Balconcillo</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialBAL1" name="materialBAL1">'+
            '<option value="BM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Balcones" onclick="darBajaBAL(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrBAL.push({val_est:'BE', val_inte:'BI', val_tipo:'BT', val_mat:'BM'});
    arrDibujoBAL.push({scd_codigo:htmlBAL,id: 1});
    ////console.log("arrBAL",arrBAL);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoBAL[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cbalcones').html(cadena);
    $("#estadocBAL1").val(arrBAL[0].val_est);
    $("#integridadBAL1").val(arrBAL[0].val_inte);
    $("#tipoBAL1").val(arrBAL[0].val_tipo);
    $("#materialBAL1").val(arrBAL[0].val_mat);

function crearBalcones(){
    var htmlBAL;
        htmlBAL='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocBAL'+contBAL+'" name="estadocBAL'+contBAL+'">'+
            '<option value="BE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadBAL'+contBAL+'" name="integridadBAL'+contBAL+'">'+
            '<option value="BI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoBAL'+contBAL+'" name="tipoBAL'+contBAL+'">'+
             '<option value="BT" selected="selected">--Seleccione--</option>'+
              '<option value="De cajon">De cajon</option>'+
              '<option value="Abierto">Abierto</option>'+
              '<option value="Corrido">Corrido</option>'+
              '<option value="Balconcillo">Balconcillo</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialBAL'+contBAL+'" name="materialBAL'+contBAL+'">'+
            '<option value="BM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Balcones" onclick="darBajaBAL('+contBAL+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoBAL.forEach(function (item, index, array) 
        {
          arrBAL[cont].val_est = $("#estadocBAL"+item.id+"").val();
          arrBAL[cont].val_inte = $("#integridadBAL"+item.id+"").val();
          arrBAL[cont].val_tipo = $("#tipoBAL"+item.id+"").val();
          arrBAL[cont].val_mat = $("#materialBAL"+item.id+"").val();
          cont++;
        });
        arrBAL.push({val_est:'BE', val_inte:'BI', val_tipo:'BT', val_mat:'BM'});
        arrDibujoBAL.push({scd_codigo:htmlBAL,id: contBAL});
        contBAL++;
        cont=1;
        for (var i=0; i<arrDibujoBAL.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoBAL[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#cbalcones').html(cadena); 
        var conto=0;
        arrDibujoBAL.forEach(function (item, index, array) 
        { 
          $("#estadocBAL"+item.id+"").val(arrBAL[conto].val_est);
          $("#integridadBAL"+item.id+"").val(arrBAL[conto].val_inte);
          $("#tipoBAL"+item.id+"").val(arrBAL[conto].val_tipo);
          $("#materialBAL"+item.id+"").val(arrBAL[conto].val_mat);
          conto=conto+1;
        });
        
    }


function darBajaBAL($id){
        if(contBAL>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoBAL.forEach(function (item, index, array) 
                {
                    arrBAL[cont].val_est=$("#estadocBAL"+item.id+"").val();
                    arrBAL[cont].val_inte=$("#integridadBAL"+item.id+"").val();
                    arrBAL[cont].val_tipo=$("#tipoBAL"+item.id+"").val();
                    arrBAL[cont].val_mat=$("#materialBAL"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoBAL.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrBAL.splice(pos,1);
                arrDibujoBAL.splice(pos,1);
                ////console.log("arrBAL",arrBAL);
                
                cont=1;
                for (var i=0; i<arrDibujoBAL.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoBAL[i].scd_codigo;
                    cont++;
                }
                
                $('#cbalcones').html(cadena);
                
                cont=0;
                arrDibujoBAL.forEach(function (item, index, array) 
                {
                    $("#estadocBAL"+item.id+"").val(arrBAL[cont].val_est);
                    $("#integridadBAL"+item.id+"").val(arrBAL[cont].val_inte);
                    $("#tipoBAL"+item.id+"").val(arrBAL[cont].val_tipo);
                    $("#materialBAL"+item.id+"").val(arrBAL[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
     var contBal=0;
    var arrDibujoBal=[];
    var arrCodigoBal=[];

function editarBalcones(longitudGrilla){
    
    contBal=1;
    arrDibujoBal=[];
    var htmBal;
    for (var i=1; i<longitudGrilla;i++)
          {
        htmBal='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocBal'+contBal+'" name="estadocBal'+contBal+'">'+
            '<option value="BE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadBal'+contBal+'" name="integridadBal'+contBal+'">'+
            '<option value="BI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoBal'+contBal+'" name="tipoBal'+contBal+'">'+
             '<option value="BT" selected="selected">--Seleccione--</option>'+
              '<option value="De cajon">De cajon</option>'+
              '<option value="Abierto">Abierto</option>'+
              '<option value="Corrido">Corrido</option>'+
              '<option value="Balconcillo">Balconcillo</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialBal'+contBal+'" name="materialBal'+contBal+'">'+
            '<option value="BM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Balcones" onclick="eliminarBalcones('+contBal+');"></i>'+
      '</a></div>';
         arrDibujoBal.push({scd_codigo:htmBal,id: contBal});
          contBal++;
          }
          var cadenaBal='';
          var cont=1;
          for (var i=0; i<arrDibujoBal.length;i++)
          {
            cadenaBal=cadenaBal + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoBal[i].scd_codigo;
            cont++;
          }
          //////console.log("cadenaBalaa",cadena);
          $('#abalcones').html(cadenaBal);
    }
    
function actualizarBalcones(){
      var htmBal;
     htmBal='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocBal'+contBal+'" name="estadocBal'+contBal+'">'+
            '<option value="BE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadBal'+contBal+'" name="integridadBal'+contBal+'">'+
            '<option value="BI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoBal'+contBal+'" name="tipoBal'+contBal+'">'+
             '<option value="BT" selected="selected">--Seleccione--</option>'+
              '<option value="De cajon">De cajon</option>'+
              '<option value="Abierto">Abierto</option>'+
              '<option value="Corrido">Corrido</option>'+
              '<option value="Balconcillo">Balconcillo</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialBal'+contBal+'" name="materialBal'+contBal+'">'+
            '<option value="BM" selected="selected">--Seleccione--</option>'+
            '<option value="Adobe">Adobe</option>'+
            '<option value="Madera">Madera</option>'+
            '<option value="Ladrillo">Ladrillo</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Vidrio">Vidrio</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Metal">Metal</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Balcones" onclick="eliminarBalcones('+contBal+');"></i>'+
      '</a></div>';

        var cadena='';
        var cont=0;
        arrDibujoBal.forEach(function (item, index, array) 
        {
          arrCodigoBal[cont].val_est = $("#estadocBal"+item.id+"").val();
          arrCodigoBal[cont].val_inte = $("#integridadBal"+item.id+"").val();
          arrCodigoBal[cont].val_tipo = $("#tipoBal"+item.id+"").val();
          arrCodigoBal[cont].val_mat = $("#materialBal"+item.id+"").val();
          cont++;
        });
        arrCodigoBal.push({val_est:'BE', val_inte:'BI', val_tipo:'BT', val_mat:'BM'});
        arrDibujoBal.push({scd_codigo:htmBal,id: contBal});
        contBal++;
        cont=1;
        for (var i=0; i<arrDibujoBal.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoBal[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#abalcones').html(cadena); 
        var conto=0;
        arrDibujoBal.forEach(function (item, index, array) 
        { 
          $("#estadocBal"+item.id+"").val(arrCodigoBal[conto].val_est);
          $("#integridadBal"+item.id+"").val(arrCodigoBal[conto].val_inte);
          $("#tipoBal"+item.id+"").val(arrCodigoBal[conto].val_tipo);
          $("#materialBal"+item.id+"").val(arrCodigoBal[conto].val_mat);
          conto=conto+1;
        });
}
function eliminarBalcones($id){
        if(contBal>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                
                var pos=-1;
                var cont=0;
                arrDibujoBal.forEach(function (item, index, array) 
                {
                  arrCodigoBal[cont].val_est = $("#estadocBal"+item.id+"").val();
                  arrCodigoBal[cont].val_inte = $("#integridadBal"+item.id+"").val();
                  arrCodigoBal[cont].val_tipo = $("#tipoBal"+item.id+"").val();
                  arrCodigoBal[cont].val_mat = $("#materialBal"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoBal.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoBal.splice(pos,1);
                arrDibujoBal.splice(pos,1);
                //////console.log("arrBAL",arrBAL);
                
                cont=1;
                for (var i=0; i<arrDibujoBal.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoBal[i].scd_codigo;
                    cont++;
                }
                
                $('#abalcones').html(cadena);
                
                var conto=0;
                arrDibujoBal.forEach(function (item, index, array) 
                { 
                  $("#estadocBal"+item.id+"").val(arrCodigoBal[conto].val_est);
                  $("#integridadBal"+item.id+"").val(arrCodigoBal[conto].val_inte);
                  $("#tipoBal"+item.id+"").val(arrCodigoBal[conto].val_tipo);
                  $("#materialBal"+item.id+"").val(arrCodigoBal[conto].val_mat);
                  conto=conto+1;
                });
            });
        }
        
    }

//----fin grilla balcones-----------------------------
//--------------ESTRUCTURA CUBIERTA----------------------

var contEC=2;
  var arrEC=[];
  var arrDibujoEC=[];

    htmlECU='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEC1" name="estadocEC1">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEC1" name="integridadEC1">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEC1" name="tipoEC1">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha y Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEC1" name="materialEC1">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Perfil de Acero">Perfil de Acero</option>'+
            '<option value="Perfil de Aluminio">Perfil de Aluminio</option>'+
            '<option value="Obra de Ladrillo">Obra de Ladrillo</option>'+
            '<option value="Obra de Adobe">Obra de Adobe</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Estructura Cubierta" onclick="darBajaEC(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrEC.push({val_est:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
    arrDibujoEC.push({scd_codigo:htmlECU,id: 1});
    ////console.log("arrEC",arrEC);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoEC[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cestructuracubierta').html(cadena);
    $("#estadocEC1").val(arrEC[0].val_est);
    $("#integridadEC1").val(arrEC[0].val_int);
    $("#tipoEC1").val(arrEC[0].val_tipo);
    $("#materialEC1").val(arrEC[0].val_mat);


function crearEstructuraCubierta(){
    var htmlECU;
        htmlECU='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocEC'+contEC+'" name="estadocEC'+contEC+'">'+
            '<option value="EE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadEC'+contEC+'" name="integridadEC'+contEC+'">'+
            '<option value="EI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoEC'+contEC+'" name="tipoEC'+contEC+'">'+
             '<option value="ET" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha y Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialEC'+contEC+'" name="materialEC'+contEC+'">'+
            '<option value="EM" selected="selected">--Seleccione--</option>'+
            '<option value="Hormigon Armado">Hormigon Armado</option>'+
            '<option value="Madera Rolliza">Madera Rolliza</option>'+
            '<option value="Madera Escuadria">Madera Escuadria</option>'+
            '<option value="Perfil de Acero">Perfil de Acero</option>'+
            '<option value="Perfil de Aluminio">Perfil de Aluminio</option>'+
            '<option value="Obra de Ladrillo">Obra de Ladrillo</option>'+
            '<option value="Obra de Adobe">Obra de Adobe</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Estructura Cubierta" onclick="darBajaEC('+contEC+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoEC.forEach(function (item, index, array) 
        {
          arrEC[cont].val_est = $("#estadocEC"+item.id+"").val();
          arrEC[cont].val_int = $("#integridadEC"+item.id+"").val();
          arrEC[cont].val_tipo = $("#tipoEC"+item.id+"").val();
          arrEC[cont].val_mat = $("#materialEC"+item.id+"").val();
          cont++;
        });
        arrEC.push({val_est:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoEC.push({scd_codigo:htmlECU,id: contEC});
        contEC++;
        cont=1;
        for (var i=0; i<arrDibujoEC.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEC[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#cestructuracubierta').html(cadena); 
        var conto=0;
        arrDibujoEC.forEach(function (item, index, array) 
        { 
          $("#estadocEC"+item.id+"").val(arrEC[conto].val_est);
          $("#integridadEC"+item.id+"").val(arrEC[conto].val_int);
          $("#tipoEC"+item.id+"").val(arrEC[conto].val_tipo);
          $("#materialEC"+item.id+"").val(arrEC[conto].val_mat);
          conto=conto+1;
        });
        
    }

function darBajaEC($id){
        if(contEC>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoEC.forEach(function (item, index, array) 
                {
                    arrEC[cont].val_est=$("#estadocEC"+item.id+"").val();
                    arrEC[cont].val_int=$("#integridadEC"+item.id+"").val();
                    arrEC[cont].val_tipo=$("#tipoEC"+item.id+"").val();
                    arrEC[cont].val_mat=$("#materialEC"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoEC.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrEC.splice(pos,1);
                arrDibujoEC.splice(pos,1);
                ////console.log("arrEC",arrEC);
                
                cont=1;
                for (var i=0; i<arrDibujoEC.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEC[i].scd_codigo;
                    cont++;
                }
                
                $('#cestructuracubierta').html(cadena);
                
                cont=0;
                arrDibujoEC.forEach(function (item, index, array) 
                {
                    $("#estadocEC"+item.id+"").val(arrEC[cont].val_est);
                    $("#integridadEC"+item.id+"").val(arrEC[cont].val_int);
                    $("#tipoEC"+item.id+"").val(arrEC[cont].val_tipo);
                    $("#materialEC"+item.id+"").val(arrEC[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
    var contEsC=0;
      var arrDibujoEsC=[];
      var arrCodigoEsC=[];
function editarEstructuraCubierta(longitudGrilla){
      contEsC=1;
      arrDibujoEsC=[];
      var htmEsC;
      for (var i=1; i<longitudGrilla;i++)
            {
          htmEsC='Estado de conservación:</label>'+
            '<select class="form-control" id="estadocEsC'+contEsC+'" name="estadocEsC'+contEsC+'">'+
              '<option value="EE" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="integridadEsC'+contEsC+'" name="integridadEsC'+contEsC+'">'+
              '<option value="EI" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipoEsC'+contEsC+'" name="tipoEsC'+contEsC+'">'+
               '<option value="ET" selected="selected">--Seleccione--</option>'+
                '<option value="Con Caidas">Con Caidas</option>'+
                '<option value="Bóveda">Bóveda</option>'+
                '<option value="Cúpula">Cúpula</option>'+
                '<option value="Piramidal">Piramidal</option>'+
                '<option value="Mansarda">Mansarda</option>'+
                '<option value="Azotea Plana">Azotea Plana</option>'+
                '<option value="Par y Nudillo">Par y Nudillo</option>'+
                '<option value="Cercha y Piramidal">Cercha y Piramidal</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="materialEsC'+contEsC+'" name="materialEsC'+contEsC+'">'+
              '<option value="EM" selected="selected">--Seleccione--</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Madera Rolliza">Madera Rolliza</option>'+
              '<option value="Madera Escuadria">Madera Escuadria</option>'+
              '<option value="Perfil de Acero">Perfil de Acero</option>'+
              '<option value="Perfil de Aluminio">Perfil de Aluminio</option>'+
              '<option value="Obra de Ladrillo">Obra de Ladrillo</option>'+
              '<option value="Obra de Adobe">Obra de Adobe</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Estructura Cubierta" onclick="eliminarEstructura('+contEsC+');"></i>'+
        '</a></div>';
           arrDibujoEsC.push({scd_codigo:htmEsC,id: contEsC});
            contEsC++;
            }
            var cadena='';
            var cont=1;
            for (var i=0; i<arrDibujoEsC.length;i++)
            {
              cadena = cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEsC[i].scd_codigo;
              cont++;
            }
            //////console.log("cadenaEsCaa",cadena);
            $('#aestructuracubierta').html(cadena);
        }
function actualizarEstructuraCubierta(){
  var htmEsC;
  htmEsC='Estado de conservación:</label>'+
            '<select class="form-control" id="estadocEsC'+contEsC+'" name="estadocEsC'+contEsC+'">'+
              '<option value="EE" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="integridadEsC'+contEsC+'" name="integridadEsC'+contEsC+'">'+
              '<option value="EI" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipoEsC'+contEsC+'" name="tipoEsC'+contEsC+'">'+
               '<option value="ET" selected="selected">--Seleccione--</option>'+
                '<option value="Con Caidas">Con Caidas</option>'+
                '<option value="Bóveda">Bóveda</option>'+
                '<option value="Cúpula">Cúpula</option>'+
                '<option value="Piramidal">Piramidal</option>'+
                '<option value="Mansarda">Mansarda</option>'+
                '<option value="Azotea Plana">Azotea Plana</option>'+
                '<option value="Par y Nudillo">Par y Nudillo</option>'+
                '<option value="Cercha y Piramidal">Cercha y Piramidal</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="materialEsC'+contEsC+'" name="materialEsC'+contEsC+'">'+
              '<option value="EM" selected="selected">--Seleccione--</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Madera Rolliza">Madera Rolliza</option>'+
              '<option value="Madera Escuadria">Madera Escuadria</option>'+
              '<option value="Perfil de Acero">Perfil de Acero</option>'+
              '<option value="Perfil de Aluminio">Perfil de Aluminio</option>'+
              '<option value="Obra de Ladrillo">Obra de Ladrillo</option>'+
              '<option value="Obra de Adobe">Obra de Adobe</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Estructura Cubierta" onclick="eliminarEstructura('+contEsC+');"></i>'+
        '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoEsC.forEach(function (item, index, array) 
        {
          arrCodigoEsC[cont].val_est = $("#estadocEsC"+item.id+"").val();
          arrCodigoEsC[cont].val_int = $("#integridadEsC"+item.id+"").val();
          arrCodigoEsC[cont].val_tipo = $("#tipoEsC"+item.id+"").val();
          arrCodigoEsC[cont].val_mat = $("#materialEsC"+item.id+"").val();
          cont++;
        });
        arrCodigoEsC.push({val_est:'EE', val_int:'EI', val_tipo:'ET', val_mat:'EM'});
        arrDibujoEsC.push({scd_codigo:htmEsC,id: contEsC});
        contEsC++;
        cont=1;
        for (var i=0; i<arrDibujoEsC.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoEsC[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#aestructuracubierta').html(cadena); 
        var conto=0;
        arrDibujoEsC.forEach(function (item, index, array) 
        { 
          $("#estadocEsC"+item.id+"").val(arrCodigoEsC[conto].val_est);
          $("#integridadEsC"+item.id+"").val(arrCodigoEsC[conto].val_int);
          $("#tipoEsC"+item.id+"").val(arrCodigoEsC[conto].val_tipo);
          $("#materialEsC"+item.id+"").val(arrCodigoEsC[conto].val_mat);
          conto=conto+1;
        });
}

function eliminarEstructura($id){
        if(contEsC>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var pos=-1;
                var cont=0;
                arrDibujoEsC.forEach(function (item, index, array) 
                {
                  arrCodigoEsC[cont].val_est = $("#estadocEsC"+item.id+"").val();
                  arrCodigoEsC[cont].val_int = $("#integridadEsC"+item.id+"").val();
                  arrCodigoEsC[cont].val_tipo = $("#tipoEsC"+item.id+"").val();
                  arrCodigoEsC[cont].val_mat = $("#materialEsC"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoEsC.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoEsC.splice(pos,1);
                arrDibujoEsC.splice(pos,1);
                //////console.log("arrEC",arrEC);
                
                cont=1;
                for (var i=0; i<arrDibujoEsC.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEsC[i].scd_codigo;
                    cont++;
                }
                
                $('#aestructuracubierta').html(cadena);
                
              var conto=0;
              arrDibujoEsC.forEach(function (item, index, array) 
              { 
                $("#estadocEsC"+item.id+"").val(arrCodigoEsC[conto].val_est);
                $("#integridadEsC"+item.id+"").val(arrCodigoEsC[conto].val_int);
                $("#tipoEsC"+item.id+"").val(arrCodigoEsC[conto].val_tipo);
                $("#materialEsC"+item.id+"").val(arrCodigoEsC[conto].val_mat);
                conto=conto+1;
              });
            });
        }
        
    }

//-------fin grilla estructura cubierta------------------
      
//------------------GRILLA PISOS-----------------------    
var contP=2;
  var arrP=[];
  var arrDibujoP=[];

    htmlPISO='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocP1" name="estadocP1">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadP1" name="integridadP1">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoP1" name="tipoP1">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha o Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialP1" name="materialP1">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Mosaico">Mosaico</option>'+
            '<option value="Loseta">Loseta</option>'+
            '<option value="Piedra Huevillo">Piedra Huevillo</option>'+
            '<option value="Piedra Manzana">Piedra Manzana</option>'+
            '<option value="Céramica">Céramica</option>'+
            '<option value="Obra de Adobe">Marmol</option>'+
            '<option value="Marmol">Madera Machiembre</option>'+
            '<option value="Madera Entablonado">Madera Entablonado</option>'+
            '<option value="Madera Parquet">Madera Parquet</option>'+
            '<option value="Revestimiento de Porcelanato">Revestimiento de Porcelanato</option>'+
            '<option value="Revestimiento de Vinil">Revestimiento de Vinil</option>'+
            '<option value="Revestimiento de Alfombra">Revestimiento de Alfombra</option>'+
            '<option value="Vitroblok">Vitroblok</option>'+
            '<option value="Azulejo">Azulejo</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Piedra Ormamental">Piedra Ormamental</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Pisos" onclick="darBajaPisos(1);"></i>'+
      '</a></div>';
    var cadena = '';  
    arrP.push({val_est:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
    arrDibujoP.push({scd_codigo:htmlPISO,id: 1});
    ////console.log("arrP",arrP);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1. '+arrDibujoP[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cpisos').html(cadena);
    $("#estadocP1").val(arrP[0].val_est);
    $("#integridadP1").val(arrP[0].val_int);
    $("#tipoP1").val(arrP[0].val_tipo);
    $("#materialP1").val(arrP[0].val_mat);


function crearPisos(){
    var htmlPISO;
        htmlPISO='Estado de conservación:</label>'+
          '<select class="form-control" id="estadocP'+contP+'" name="estadocP'+contP+'">'+
            '<option value="PE" selected="selected">--Seleccione--</option>'+
            '<option value="Bueno">Bueno</option>'+
            '<option value="Regular">Regular</option>'+
            '<option value="Malo">Malo</option>'+
          '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
          '<select class="form-control" id="integridadP'+contP+'" name="integridadP'+contP+'">'+
            '<option value="PI" selected="selected">--Seleccione--</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-3">'+
       '<label class="control-label">Tipo:</label>'+
          '<select class="form-control" id="tipoP'+contP+'" name="tipoP'+contP+'">'+
             '<option value="PT" selected="selected">--Seleccione--</option>'+
              '<option value="Con Caidas">Con Caidas</option>'+
              '<option value="Bóveda">Bóveda</option>'+
              '<option value="Cúpula">Cúpula</option>'+
              '<option value="Piramidal">Piramidal</option>'+
              '<option value="Mansarda">Mansarda</option>'+
              '<option value="Azotea Plana">Azotea Plana</option>'+
              '<option value="Par y Nudillo">Par y Nudillo</option>'+
              '<option value="Cercha y Piramidal">Cercha o Piramidal</option>'+
          '</select>'+
      '</div>'+
       '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
          '<select class="form-control" id="materialP'+contP+'" name="materialP'+contP+'">'+
            '<option value="PM" selected="selected">--Seleccione--</option>'+
            '<option value="Piedra">Piedra</option>'+
            '<option value="Concreto">Concreto</option>'+
            '<option value="Mosaico">Mosaico</option>'+
            '<option value="Loseta">Loseta</option>'+
            '<option value="Piedra Huevillo">Piedra Huevillo</option>'+
            '<option value="Piedra Manzana">Piedra Manzana</option>'+
            '<option value="Céramica">Céramica</option>'+
            '<option value="Obra de Adobe">Marmol</option>'+
            '<option value="Marmol">Madera Machiembre</option>'+
            '<option value="Madera Entablonado">Madera Entablonado</option>'+
            '<option value="Madera Parquet">Madera Parquet</option>'+
            '<option value="Revestimiento de Porcelanato">Revestimiento de Porcelanato</option>'+
            '<option value="Revestimiento de Vinil">Revestimiento de Vinil</option>'+
            '<option value="Revestimiento de Alfombra">Revestimiento de Alfombra</option>'+
            '<option value="Vitroblok">Vitroblok</option>'+
            '<option value="Azulejo">Azulejo</option>'+
            '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
            '<option value="Piedra Ormamental">Piedra Ormamental</option>'+
          '</select>'+
        '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Pisos" onclick="darBajaPisos('+contP+');"></i>'+
      '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoP.forEach(function (item, index, array) 
        {
          arrP[cont].val_est = $("#estadocP"+item.id+"").val();
          arrP[cont].val_int = $("#integridadP"+item.id+"").val();
          arrP[cont].val_tipo = $("#tipoP"+item.id+"").val();
          arrP[cont].val_mat = $("#materialP"+item.id+"").val();
          cont++;
        });
        arrP.push({val_est:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
        arrDibujoP.push({scd_codigo:htmlPISO,id: contP});
        contP++;
        cont=1;
        for (var i=0; i<arrDibujoP.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoP[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#cpisos').html(cadena); 
        var conto=0;
        arrDibujoP.forEach(function (item, index, array) 
        { 
          $("#estadocP"+item.id+"").val(arrP[conto].val_est);
          $("#integridadP"+item.id+"").val(arrP[conto].val_int);
          $("#tipoP"+item.id+"").val(arrP[conto].val_tipo);
          $("#materialP"+item.id+"").val(arrP[conto].val_mat);
          conto=conto+1;
        });
        
    }
function darBajaPisos($id){
        if(contP>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoP.forEach(function (item, index, array) 
                {
                    arrP[cont].val_est=$("#estadocP"+item.id+"").val();
                    arrP[cont].val_int=$("#integridadP"+item.id+"").val();
                    arrP[cont].val_tipo=$("#tipoP"+item.id+"").val();
                    arrP[cont].val_mat=$("#materialP"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoP.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrP.splice(pos,1);
                arrDibujoP.splice(pos,1);
                ////console.log("arrP",arrP);
                
                cont=1;
                for (var i=0; i<arrDibujoP.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoP[i].scd_codigo;
                    cont++;
                }
                
                $('#cpisos').html(cadena);
                
                cont=0;
                arrDibujoP.forEach(function (item, index, array) 
                {
                    $("#estadocP"+item.id+"").val(arrP[cont].val_est);
                    $("#integridadP"+item.id+"").val(arrP[cont].val_int);
                    $("#tipoP"+item.id+"").val(arrP[cont].val_tipo);
                    $("#materialP"+item.id+"").val(arrP[cont].val_mat);
                    cont=cont+1;
                });
            });
        }
        
    }
 var contPisos = 0;
      var arrDibujoPiso = [];
      var arrCodigoPiso = [];

function editarPisos(longitudGrilla){
      contPisos=1;
      arrDibujoPiso=[];
      var htmPiso;
      for (var i=1; i<longitudGrilla;i++)
            {
          htmPiso='Estado de conservación:</label>'+
            '<select class="form-control" id="estadoPi'+contPisos+'" name="estadoPi'+contPisos+'">'+
              '<option value="PE" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="integridadPi'+contPisos+'" name="integridadPi'+contPisos+'">'+
              '<option value="PI" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipoPi'+contPisos+'" name="tipoPi'+contPisos+'">'+
               '<option value="PT" selected="selected">--Seleccione--</option>'+
                '<option value="Con Caidas">Con Caidas</option>'+
                '<option value="Bóveda">Bóveda</option>'+
                '<option value="Cúpula">Cúpula</option>'+
                '<option value="Piramidal">Piramidal</option>'+
                '<option value="Mansarda">Mansarda</option>'+
                '<option value="Azotea Plana">Azotea Plana</option>'+
                '<option value="Par y Nudillo">Par y Nudillo</option>'+
                '<option value="Cercha y Piramidal">Cercha o Piramidal</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="materialPi'+contPisos+'" name="materialPi'+contPisos+'">'+
              '<option value="PM" selected="selected">--Seleccione--</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Mosaico">Mosaico</option>'+
              '<option value="Loseta">Loseta</option>'+
              '<option value="Piedra Huevillo">Piedra Huevillo</option>'+
              '<option value="Piedra Manzana">Piedra Manzana</option>'+
              '<option value="Céramica">Céramica</option>'+
              '<option value="Obra de Adobe">Marmol</option>'+
              '<option value="Marmol">Madera Machiembre</option>'+
              '<option value="Madera Entablonado">Madera Entablonado</option>'+
              '<option value="Madera Parquet">Madera Parquet</option>'+
              '<option value="Revestimiento de Porcelanato">Revestimiento de Porcelanato</option>'+
              '<option value="Revestimiento de Vinil">Revestimiento de Vinil</option>'+
              '<option value="Revestimiento de Alfombra">Revestimiento de Alfombra</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Azulejo">Azulejo</option>'+
              '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
              '<option value="Piedra Ormamental">Piedra Ormamental</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Pisos" onclick="eliminarPisos('+contPisos+');"></i>'+
        '</a></div>';
           arrDibujoPiso.push({scd_codigo:htmPiso,id: contPisos});
            contPisos++;
            }
            var cadena='';
            var cont=1;
            for (var i=0; i<arrDibujoPiso.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPiso[i].scd_codigo;
              cont++;
            }
            //////console.log("cadenaPisosaa",cadena);
            $('#apisos').html(cadena);
        }

         
  function actualizarPisos(){

    var htmPiso;
      htmPiso='Estado de conservación:</label>'+
            '<select class="form-control" id="estadoPi'+contPisos+'" name="estadoPi'+contPisos+'">'+
              '<option value="PE" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+
            '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<label class="control-label">Integridad:</label>'+
            '<select class="form-control" id="integridadPi'+contPisos+'" name="integridadPi'+contPisos+'">'+
              '<option value="PI" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-3">'+
         '<label class="control-label">Tipo:</label>'+
            '<select class="form-control" id="tipoPi'+contPisos+'" name="tipoPi'+contPisos+'">'+
               '<option value="PT" selected="selected">--Seleccione--</option>'+
                '<option value="Con Caidas">Con Caidas</option>'+
                '<option value="Bóveda">Bóveda</option>'+
                '<option value="Cúpula">Cúpula</option>'+
                '<option value="Piramidal">Piramidal</option>'+
                '<option value="Mansarda">Mansarda</option>'+
                '<option value="Azotea Plana">Azotea Plana</option>'+
                '<option value="Par y Nudillo">Par y Nudillo</option>'+
                '<option value="Cercha y Piramidal">Cercha o Piramidal</option>'+
            '</select>'+
        '</div>'+
         '<div class="form-group col-md-2">'+
          '<label class="control-label">Materiales:</label>'+
            '<select class="form-control" id="materialPi'+contPisos+'" name="materialPi'+contPisos+'">'+
              '<option value="PM" selected="selected">--Seleccione--</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Mosaico">Mosaico</option>'+
              '<option value="Loseta">Loseta</option>'+
              '<option value="Piedra Huevillo">Piedra Huevillo</option>'+
              '<option value="Piedra Manzana">Piedra Manzana</option>'+
              '<option value="Céramica">Céramica</option>'+
              '<option value="Obra de Adobe">Marmol</option>'+
              '<option value="Marmol">Madera Machiembre</option>'+
              '<option value="Madera Entablonado">Madera Entablonado</option>'+
              '<option value="Madera Parquet">Madera Parquet</option>'+
              '<option value="Revestimiento de Porcelanato">Revestimiento de Porcelanato</option>'+
              '<option value="Revestimiento de Vinil">Revestimiento de Vinil</option>'+
              '<option value="Revestimiento de Alfombra">Revestimiento de Alfombra</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Azulejo">Azulejo</option>'+
              '<option value="Ladrillo Prensado">Ladrillo Prensado</option>'+
              '<option value="Piedra Ormamental">Piedra Ormamental</option>'+
            '</select>'+
          '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Pisos" onclick="eliminarPisos('+contPisos+');"></i>'+
        '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPiso.forEach(function (item, index, array) 
        {
          arrCodigoPiso[cont].val_est = $("#estadoPi"+item.id+"").val();
          arrCodigoPiso[cont].val_int = $("#integridadPi"+item.id+"").val();
          arrCodigoPiso[cont].val_tipo = $("#tipoPi"+item.id+"").val();
          arrCodigoPiso[cont].val_mat = $("#materialPi"+item.id+"").val();
          cont++;
        });
        arrCodigoPiso.push({val_est:'PE', val_int:'PI', val_tipo:'PT', val_mat:'PM'});
        arrDibujoPiso.push({scd_codigo:htmPiso,id: contPisos});
        contPisos++;
        cont=1;
        for (var i=0; i<arrDibujoPiso.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPiso[i].scd_codigo;
          cont++;
        }
        ////////console.log("cadenaaaa",cadena);
        $('#apisos').html(cadena); 
        var conto=0;
        arrDibujoPiso.forEach(function (item, index, array) 
        { 
          $("#estadoPi"+item.id+"").val(arrCodigoPiso[conto].val_est);
          $("#integridadPi"+item.id+"").val(arrCodigoPiso[conto].val_int);
          $("#tipoPi"+item.id+"").val(arrCodigoPiso[conto].val_tipo);
          $("#materialPi"+item.id+"").val(arrCodigoPiso[conto].val_mat);
          conto=conto+1;
        });
  }

  function eliminarPisos($id){
        if(contPisos>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var pos=-1;
                var cont=0;
                arrDibujoPiso.forEach(function (item, index, array) 
                {
                  arrCodigoPiso[cont].val_est = $("#estadoPi"+item.id+"").val();
                  arrCodigoPiso[cont].val_int = $("#integridadPi"+item.id+"").val();
                  arrCodigoPiso[cont].val_tipo = $("#tipoPi"+item.id+"").val();
                  arrCodigoPiso[cont].val_mat = $("#materialPi"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoPiso.map(function(e) {return e.id;}).indexOf($id);
                //////console.log("poos",pos);
                arrCodigoPiso.splice(pos,1);
                arrDibujoPiso.splice(pos,1);
                //////console.log("arrP",arrP);
                
                cont=1;
                for (var i=0; i<arrDibujoPiso.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPiso[i].scd_codigo;
                    cont++;
                }
                
                $('#apisos').html(cadena);
                
              var conto=0;
              arrDibujoPiso.forEach(function (item, index, array) 
              { 
                $("#estadoPi"+item.id+"").val(arrCodigoPiso[conto].val_est);
                $("#integridadPi"+item.id+"").val(arrCodigoPiso[conto].val_int);
                $("#tipoPi"+item.id+"").val(arrCodigoPiso[conto].val_tipo);
                $("#materialPi"+item.id+"").val(arrCodigoPiso[conto].val_mat);
                conto=conto+1;
              });
            });
        }
        
    }
//-------fin grilla pisos-----------------------
//-------FIN GRILLAS PARA BIENES INMUEBLES BLOQUES----------
//INICIO HTML TANYA
/******************-------Vanos Ventanas Interiores**********************/
    var contCodigoEleComVanosVenInteriores=2;
    var arrCodigoEleComVanosVenInteriores=[];
    var arrDibujoEleComVanosVenInteriores=[];

     htmlEleComVanosVenInteriores='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_int_estado1" name="vent_int_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_int_inte1" name="vent_int_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_int_tipo1" name="vent_int_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_int_mat1" name="vent_int_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ven_int(1);"></i>'+
    '</a></div>';
    var cadenaEleComVanosVenInteriores = '';  

    arrCodigoEleComVanosVenInteriores.push({arg_vent_int_estado:'N', arg_vent_int_inte:'N', arg_vent_int_tipo:'N', arg_vent_int_mat:'N'});  
    arrDibujoEleComVanosVenInteriores.push({scd_codigo:htmlEleComVanosVenInteriores,id: 1});
    ////console.log("arrCodigo",arrCodigoEleComVanosVenInteriores);
        
    cadenaEleComVanosVenInteriores='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujoEleComVanosVenInteriores[0].scd_codigo;
    //////console.log("cadenaaaa",cadenaEleComVanosVenInteriores);
    $('#divelcoVanosVentanasInteriores').html(cadenaEleComVanosVenInteriores);
    $("#vent_int_estado1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_estado);
    $("#vent_int_inte1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_inte);
    $("#vent_int_tipo1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_tipo);
    $("#vent_int_mat1").val(arrCodigoEleComVanosVenInteriores[0].arg_vent_int_mat);
    //$('#grillaPrueba').val(JSON.stringify(arrCodigoEleComVanosVenInteriores));

    function elcoVanosVentanasInteriores(){
    
    var htmlEleComVanosVenInteriores;
    htmlEleComVanosVenInteriores='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_int_estado'+contCodigoEleComVanosVenInteriores+'" name="vent_int_estado'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_int_inte'+contCodigoEleComVanosVenInteriores+'" name="vent_int_inte'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_int_tipo'+contCodigoEleComVanosVenInteriores+'" name="vent_int_tipo'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_int_mat'+contCodigoEleComVanosVenInteriores+'" name="vent_int_mat'+contCodigoEleComVanosVenInteriores+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ven_int('+contCodigoEleComVanosVenInteriores+');"></i>'+
    '</a></div>';
        //RECUPERA VALORES EN LA GRILLA
        var cadenaEleComVanosVenInteriores='';
        var cont=0;
        arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
        {
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_estado = $("#vent_int_estado"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_inte = $("#vent_int_inte"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_tipo = $("#vent_int_tipo"+item.id+"").val();
          arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_mat = $("#vent_int_mat"+item.id+"").val();
          cont++;
        });

        arrCodigoEleComVanosVenInteriores.push({arg_vent_int_estado:'N', arg_vent_int_inte:'N', arg_vent_int_tipo:'N', arg_vent_int_mat:'N'});  
        arrDibujoEleComVanosVenInteriores.push({scd_codigo:htmlEleComVanosVenInteriores,id: contCodigoEleComVanosVenInteriores});
     
        contCodigoEleComVanosVenInteriores++;
        cont=1;
        //CONCATENA HTML GRILLA EN LA VISTA
        ////console.log(arrDibujoEleComVanosVenInteriores.length);
        for (var i=0; i<arrDibujoEleComVanosVenInteriores.length;i++)
        {
          cadenaEleComVanosVenInteriores=cadenaEleComVanosVenInteriores +'<div class="form-group col-md-3"><label class="control-label">'+cont+'.'+arrDibujoEleComVanosVenInteriores[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadenaEleComVanosVenInteriores);
        $('#divelcoVanosVentanasInteriores').html(cadenaEleComVanosVenInteriores);

        //MUESTRA VALORES EN LA GRILLA
        var conto=0;
        arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
        { 
          $("#vent_int_estado"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_estado);
          $("#vent_int_inte"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_inte);
          $("#vent_int_tipo"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_tipo);
          $("#vent_int_mat"+item.id+"").val(arrCodigoEleComVanosVenInteriores[conto].arg_vent_int_mat);
          conto=conto+1;
        });
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo));
    }
    function menosLinea_ven_int($id){
        if(contCodigoEleComVanosVenInteriores>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaEleComVanosVenInteriores='';
                var cont=0;
                var pos=-1;
                arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
                {
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_estado = $("#vent_int_estado"+item.id+"").val();
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_inte = $("#vent_int_inte"+item.id+"").val();
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_tipo = $("#vent_int_tipo"+item.id+"").val();
                    arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_mat = $("#vent_int_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoEleComVanosVenInteriores.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigoEleComVanosVenInteriores.splice(pos,1);
                arrDibujoEleComVanosVenInteriores.splice(pos,1);
                //////console.log("arrCodigo",arrCodigoEleComVanosVenInteriores);
                cont=1;
                for (var i=0; i<arrDibujoEleComVanosVenInteriores.length;i++)
                {
                    cadenaEleComVanosVenInteriores=cadenaEleComVanosVenInteriores + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoEleComVanosVenInteriores[i].scd_codigo;
                    cont++;
                }                
                $('#divelcoVanosVentanasInteriores').html(cadenaEleComVanosVenInteriores);
                cont=0;
                arrDibujoEleComVanosVenInteriores.forEach(function (item, index, array) 
                {
                    $("#vent_int_estado"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_estado);
                    $("#vent_int_inte"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_inte);
                    $("#vent_int_tipo"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_tipo);
                    $("#vent_int_mat"+item.id+"").val(arrCodigoEleComVanosVenInteriores[cont].arg_vent_int_mat);
                    //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigoFinal",arrCodigoEleComVanosVenInteriores);
    }

/******************-------Ventanas Interiores**********************/

/******************-------Ventanas Interiores**********************/
    var contCodigovent_inte_es_in=2;
    var arrCodigovent_inte_es_in=[];
    var arrDibujovent_inte_es_in=[];

    htmlARGvent_inte_es_in='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_inte_estado1" name="vent_inte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_inte_inte1" name="vent_inte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_inte_tipo1" name="vent_inte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+                     
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_inte_mat1" name="vent_inte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineavent_inte_es_in(1);"></i>'+
    '</a></div>';
    var cadenavent_inte_es_in = '';  
    arrCodigovent_inte_es_in.push({arg_vent_inte_estado:'N', arg_vent_inte_inte:'N', arg_vent_inte_tipo:'N', arg_vent_inte_mat:'N'});
    arrDibujovent_inte_es_in.push({scd_codigo:htmlARGvent_inte_es_in,id: 1});
    ////console.log("arrCodigovent_inte_es_in",arrCodigovent_inte_es_in);
    
    cadenavent_inte_es_in='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujovent_inte_es_in[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#divelcoVentanasInteriores').html(cadenavent_inte_es_in);
    $("#vent_inte_estado1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_estado);
    $("#vent_inte_inte1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_inte);
    $("#vent_inte_tipo1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_tipo);
    $("#vent_inte_mat1").val(arrCodigovent_inte_es_in[0].arg_vent_inte_mat);

    function elcoVentanasInteriores(){
    var htmlARGvent_inte_es_in;
        htmlARGvent_inte_es_in='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_inte_estado'+contCodigovent_inte_es_in+'" name="vent_inte_estado'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_inte_inte'+contCodigovent_inte_es_in+'" name="vent_inte_inte'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_inte_tipo'+contCodigovent_inte_es_in+'" name="vent_inte_tipo'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+                     
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_inte_mat'+contCodigovent_inte_es_in+'" name="vent_inte_mat'+contCodigovent_inte_es_in+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineavent_inte_es_in('+contCodigovent_inte_es_in+');"></i>'+
    '</a></div>';
        var cadenavent_inte_es_in='';
        var cont=0;
        arrDibujovent_inte_es_in.forEach(function (item, index, array) 
        {
          arrCodigovent_inte_es_in[cont].arg_vent_inte_estado = $("#vent_inte_estado"+item.id+"").val();
          arrCodigovent_inte_es_in[cont].arg_vent_inte_inte = $("#vent_inte_inte"+item.id+"").val();
          arrCodigovent_inte_es_in[cont].arg_vent_inte_tipo = $("#vent_inte_tipo"+item.id+"").val();
          arrCodigovent_inte_es_in[cont].arg_vent_inte_mat = $("#vent_inte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigovent_inte_es_in.push({arg_vent_inte_estado:'N', arg_vent_inte_inte:'N', arg_vent_inte_tipo:'N', arg_vent_inte_mat:'N'});
        arrDibujovent_inte_es_in.push({scd_codigo:htmlARGvent_inte_es_in,id: contCodigovent_inte_es_in});
        contCodigovent_inte_es_in++;
        cont=1;
        for (var i=0; i<arrDibujovent_inte_es_in.length; i++)
        {
          cadenavent_inte_es_in=cadenavent_inte_es_in + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujovent_inte_es_in[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        
         $('#divelcoVentanasInteriores').html(cadenavent_inte_es_in); 
        var conto=0;
        arrDibujovent_inte_es_in.forEach(function (item, index, array) 
        { 
          $("#vent_inte_estado"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_estado);
          $("#vent_inte_inte"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_inte);
          $("#vent_inte_tipo"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_tipo);
          $("#vent_inte_mat"+item.id+"").val(arrCodigovent_inte_es_in[conto].arg_vent_inte_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigovent_inte_es_in));
    }

 
    function menosLineavent_inte_es_in($id){
      alert(contCodigovent_inte_es_in);
        if(contCodigovent_inte_es_in>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenavent_inte_es_in='';
                var cont=0;
                var pos=-1;
                arrDibujovent_inte_es_in.forEach(function (item, index, array) 
                {
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_estado = $("#vent_inte_estado"+item.id+"").val();
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_inte = $("#vent_inte_inte"+item.id+"").val();
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_tipo = $("#vent_inte_tipo"+item.id+"").val();
                    arrCodigovent_inte_es_in[cont].arg_vent_inte_mat = $("#vent_inte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujovent_inte_es_in.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigovent_inte_es_in.splice(pos,1);
                arrDibujovent_inte_es_in.splice(pos,1);
                ////console.log("arrCodigovent_inte_es_in",arrCodigovent_inte_es_in);
                
                cont=1;
                for (var i=0; i<arrDibujovent_inte_es_in.length;i++)
                {
                    cadenavent_inte_es_in=cadenavent_inte_es_in + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujovent_inte_es_in[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVentanasInteriores').html(cadenavent_inte_es_in);
                
                cont=0;
                arrDibujovent_inte_es_in.forEach(function (item, index, array) 
                {   
                    $("#vent_inte_estado"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_estado);
                    $("#vent_inte_inte"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_inte);
                    $("#vent_inte_tipo"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_tipo);
                    $("#vent_inte_mat"+item.id+"").val(arrCodigovent_inte_es_in[cont].arg_vent_inte_mat);
                   ;//document.getElementById("tipoarg"+item.id+"").value(arrCodigovent_inte_es_in[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigovent_inte_es_inFinal",arrCodigovent_inte_es_in);
    }
 

 /******************-------Vanos Ventanas Exteriores**********************/
//PTR_VAN_VENT_EXT
 var contCodigoVanVenExt_es_int=2;
    var arrCodigoVanVenExt_es_int=[];
    var arrDibujoVanVenExt_es_int=[];
    htmlARGVanVenExt_es_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_ext_estado1" name="vent_ext_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_ext_inte1" name="vent_ext_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_ext_tipo1" name="vent_ext_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_ext_mat1" name="vent_ext_mat">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaVanVenExt_es_int(1);"></i>'+
    '</a></div>';
    var cadenaVanVenExt_es_int= '';  
    arrCodigoVanVenExt_es_int.push({arg_vent_ext_estado:'N', arg_vent_ext_inte:'N', arg_vent_ext_tipo:'N', arg_vent_ext_mat:'N'});
    arrDibujoVanVenExt_es_int.push({scd_codigo:htmlARGVanVenExt_es_int,id: 1});
    ////console.log("arrCodigoVanVenExt_es_int",arrCodigoVanVenExt_es_int);
    
    cadenaVanVenExt_es_int='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujoVanVenExt_es_int[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#divelcoVanosVentanasExteriores').html(cadenaVanVenExt_es_int);

    $("#vent_ext_estado1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_estado);
    $("#vent_ext_inte1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_inte);
    $("#vent_ext_tipo1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_tipo);
    $("#vent_ext_mat1").val(arrCodigoVanVenExt_es_int[0].arg_vent_ext_mat);
   
    $('#grillaPrueba').val(JSON.stringify(arrCodigoVanVenExt_es_int));




    function elcoVanosVentanasExteriores(){

        var htmlARGVanVenExt_es_int;
        htmlARGVanVenExt_es_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_ext_estado'+contCodigoVanVenExt_es_int+'" name="vent_ext_estado'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_ext_inte'+contCodigoVanVenExt_es_int+'" name="vent_ext_inte'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_ext_tipo'+contCodigoVanVenExt_es_int+'" name="vent_ext_tipo'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+   
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_ext_mat'+contCodigoVanVenExt_es_int+'" name="vent_ext_mat'+contCodigoVanVenExt_es_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLineaVanVenExt_es_int('+contCodigoVanVenExt_es_int+');"></i>'+
    '</a></div>';
        var cadenaVanVenExt_es_int='';
        var cont=0;
        arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
        {
          arrCodigoVanVenExt_es_int[cont].arg_vent_ext_estado = $("#vent_ext_estado"+item.id+"").val();
          arrCodigoVanVenExt_es_int[cont].arg_vent_ext_inte = $("#vent_ext_inte"+item.id+"").val();
          arrCodigoVanVenExt_es_int[cont].arg_vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
          arrCodigoVanVenExt_es_int[cont].arg_vent_ext_mat = $("#vent_ext_mat"+item.id+"").val();

          cont++;
        });
        arrCodigoVanVenExt_es_int.push({arg_vent_ext_estado:'N', arg_vent_ext_inte:'N', arg_vent_ext_tipo:'N', arg_vent_ext_mat:'N'});
        arrDibujoVanVenExt_es_int.push({scd_codigo:htmlARGVanVenExt_es_int,id: contCodigoVanVenExt_es_int});
        contCodigoVanVenExt_es_int++;
        cont=1;
        for (var i=0; i<arrDibujoVanVenExt_es_int.length;i++)
        {
          cadenaVanVenExt_es_int=cadenaVanVenExt_es_int + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVanVenExt_es_int[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#divelcoVanosVentanasExteriores').html(cadenaVanVenExt_es_int); 
        var conto=0;
        arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
        { 
          $("#vent_ext_estado"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].arg_vent_ext_estado);
          $("#vent_ext_inte"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].arg_vent_ext_inte);
          $("#vent_ext_tipo"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].arg_vent_ext_tipo);
          $("#vent_ext_mat"+item.id+"").val(arrCodigoVanVenExt_es_int[conto].arg_vent_ext_mat);
   
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigoVanVenExt_es_int));
    }


 
    function menosLineaVanVenExt_es_int($id){
        if(contCodigoVanVenExt_es_int>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
                {
                  
                    arrCodigoVanVenExt_es_int[cont].arg_vent_ext_estado = $("#vent_ext_estado"+item.id+"").val();
                    arrCodigoVanVenExt_es_int[cont].arg_vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
                    arrCodigoVanVenExt_es_int[cont].vent_ext_tipo = $("#vent_ext_tipo"+item.id+"").val();
                    arrCodigoVanVenExt_es_int[cont].vent_ext_mat = $("#vent_ext_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoVanVenExt_es_int.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigoVanVenExt_es_int.splice(pos,1);
                arrDibujoVanVenExt_es_int.splice(pos,1);
                ////console.log("arrCodigoVanVenExt_es_int",arrCodigoVanVenExt_es_int);
                
                cont=1;
                for (var i=0; i<arrDibujoVanVenExt_es_int.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVanVenExt_es_int[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVanosVentanasExteriores').html(cadena);
                
                cont=0;
                arrDibujoVanVenExt_es_int.forEach(function (item, index, array) 
                {
                   
                     $("#vent_ext_estado"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].arg_vent_ext_estado);
                      $("#vent_ext_tipo"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].arg_vent_ext_tipo);
                      $("#vent_ext_tipo"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].vent_ext_tipo);
                      $("#vent_ext_mat"+item.id+"").val(arrCodigoVanVenExt_es_int[cont].vent_ext_mat);//document.getElementById("tipoarg"+item.id+"").value(arrCodigoVanVenExt_es_int[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigoVanVenExt_es_intFinal",arrCodigoVanVenExt_es_int);
    }

    
/******************-------Ventanas Exteriores**********************/
/******************-------Ventanas Exteriores**********************/
//no
    var contCodigo_ptr_vent_ext=2;
    var arrCodigo_ptr_vent_ext=[];
    var arrDibujo_ptr_vent_ext=[];

    htmlARG_ptr_vent_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_exte_estado1" name="vent_exte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_exte_inte1" name="vent_exte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_exte_tipo1" name="vent_exte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_exte_mat1" name="vent_exte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio Claro">Vidrio Claro</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_vent_ext(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_vent_ext.push({arg_vent_exte_estado:'N', arg_vent_exte_inte:'N',arg_vent_exte_tipo:'N',arg_vent_exte_mat:'N' });
    arrDibujo_ptr_vent_ext.push({scd_codigo:htmlARG_ptr_vent_ext,id: 1});
    ////console.log("arrCodigo_ptr_vent_ext",arrCodigo_ptr_vent_ext);
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_vent_ext[0].scd_codigo;
    
    //////console.log("cadenaaaa",cadena);
    $('#divelcoVentanasExteriores').html(cadena);
    $("#vent_exte_estado1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_estado);
    $("#vent_exte_inte1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_inte);
    $("#vent_exte_tipo1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_tipo);
    $("#vent_exte_mat1").val(arrCodigo_ptr_vent_ext[0].arg_vent_exte_mat);


    function elcoVentanasExteriores(){
    var htmlARG_ptr_vent_ext;
        htmlARG_ptr_vent_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="vent_exte_estado'+contCodigo_ptr_vent_ext+'" name="vent_exte_estado'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="vent_exte_inte'+contCodigo_ptr_vent_ext+'" name="vent_exte_inte'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="vent_exte_tipo'+contCodigo_ptr_vent_ext+'" name="vent_exte_tipo'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="vent_exte_mat'+contCodigo_ptr_vent_ext+'" name="vent_exte_mat'+contCodigo_ptr_vent_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio Claro">Vidrio Claro</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_vent_ext('+contCodigo_ptr_vent_ext+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_estado = $("#vent_exte_estado"+item.id+"").val();
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_inte = $("#vent_exte_inte"+item.id+"").val();
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_tipo = $("#vent_exte_tipo"+item.id+"").val();
          arrCodigo_ptr_vent_ext[cont].arg_vent_exte_mat = $("#vent_exte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_vent_ext.push({arg_vent_exte_estado:'N', arg_vent_exte_inte:'N',arg_vent_exte_tipo:'N',arg_vent_exte_mat:'N' });
        arrDibujo_ptr_vent_ext.push({scd_codigo:htmlARG_ptr_vent_ext,id: contCodigo_ptr_vent_ext});
        contCodigo_ptr_vent_ext++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_vent_ext.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_vent_ext[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#divelcoVentanasExteriores').html(cadena); 
        var conto=0;
        arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
        { 
          $("#vent_exte_estado"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_estado);
          $("#vent_exte_inte"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_inte);
          $("#vent_exte_tipo"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_tipo);
          $("#vent_exte_mat"+item.id+"").val(arrCodigo_ptr_vent_ext[conto].arg_vent_exte_mat);

          conto=conto+1;
        });
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_vent_ext));
    }

 
    function menosLinea_ptr_vent_ext($id){
        if(contCodigo_ptr_vent_ext>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_estado = $("#vent_exte_estado"+item.id+"").val();
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_inte = $("#vent_exte_inte"+item.id+"").val();
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_tipo = $("#vent_exte_tipo"+item.id+"").val();
                    arrCodigo_ptr_vent_ext[cont].arg_vent_exte_mat = $("#vent_exte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_vent_ext.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigo_ptr_vent_ext.splice(pos,1);
                arrDibujo_ptr_vent_ext.splice(pos,1);
                ////console.log("arrCodigo_ptr_vent_ext",arrCodigo_ptr_vent_ext);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_vent_ext.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_vent_ext[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVentanasExteriores').html(cadena);
                
                cont=0;
                arrDibujo_ptr_vent_ext.forEach(function (item, index, array) 
                {
                    $("#vent_exte_estado"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_estado);
                    $("#vent_exte_inte"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_inte);
                    $("#vent_exte_tipo"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_tipo);
                    $("#vent_exte_mat"+item.id+"").val(arrCodigo_ptr_vent_ext[cont].arg_vent_exte_mat);
                    

                    cont=cont+1;
                });
            });
        }
        //////console.log("arrCodigo_ptr_vent_extFinal",arrCodigo_ptr_vent_ext);
    }

    ///////////////////////////////

/******************-------Vanos Puertas Interiores**********************/
//no
    var contCodigo_ptr_van_pue_int=2;
    var arrCodigo_ptr_van_pue_int=[];
    var arrDibujo_ptr_van_pue_int=[];

   htmlARG_ptr_van_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_int_estado1" name="pue_int_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_int_inte1" name="pue_int_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_int_tipo1" name="pue_int_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajad">Arco Rebajad</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_int_mat1" name="pue_int_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_int(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_van_pue_int.push({arg_pue_int_estado:'N',arg_pue_int_inte:'N',arg_pue_int_tipo:'N',arg_pue_int_mat:'N'});
    arrDibujo_ptr_van_pue_int.push({scd_codigo:htmlARG_ptr_van_pue_int,id: 1});
    ////console.log("arrCodigo",arrCodigo_ptr_van_pue_int);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_van_pue_int[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#divelcoVanosPuertasInteriores').html(cadena);
    $("#pue_int_estado1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_estado);
    $("#pue_int_inte1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_inte);
    $("#pue_int_tipo1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_tipo);
    $("#pue_int_mat1").val(arrCodigo_ptr_van_pue_int[0].arg_pue_int_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_van_pue_int));

    function elcoVanosPuertasInteriores(){
    var htmlARG_ptr_van_pue_int;
        htmlARG_ptr_van_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_int_estado'+contCodigo_ptr_van_pue_int+'" name="pue_int_estado'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_int_inte'+contCodigo_ptr_van_pue_int+'" name="pue_int_inte'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_int_tipo'+contCodigo_ptr_van_pue_int+'" name="pue_int_tipo'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajad">Arco Rebajad</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_int_mat'+contCodigo_ptr_van_pue_int+'" name="pue_int_mat'+contCodigo_ptr_van_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_int('+contCodigo_ptr_van_pue_int+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_estado = $("#pue_int_estado"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_inte = $("#pue_int_inte"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_tipo = $("#pue_int_tipo"+item.id+"").val();
          arrCodigo_ptr_van_pue_int[cont].arg_pue_int_mat = $("#pue_int_mat"+item.id+"").val();
          cont++;
        });
      
        arrCodigo_ptr_van_pue_int.push({arg_pue_int_estado:'N',arg_pue_int_inte:'N',arg_pue_int_tipo:'N',arg_pue_int_mat:'N'});
        arrDibujo_ptr_van_pue_int.push({scd_codigo:htmlARG_ptr_van_pue_int,id: contCodigo_ptr_van_pue_int});
        contCodigo_ptr_van_pue_int++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_van_pue_int.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_van_pue_int[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#divelcoVanosPuertasInteriores').html(cadena); 
        var conto=0;
        arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
        { 
          $("#pue_int_estado"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_estado);
          $("#pue_int_inte"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_inte);
          $("#pue_int_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_tipo);
          $("#pue_int_mat"+item.id+"").val(arrCodigo_ptr_van_pue_int[conto].arg_pue_int_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_van_pue_int));
    }

 
    function menosLinea_ptr_van_pue_int($id){
        if(contCodigo_ptr_van_pue_int>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_estado = $("#pue_int_estado"+item.id+"").val();
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_inte = $("#pue_int_inte"+item.id+"").val();
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_tipo = $("#pue_int_tipo"+item.id+"").val();
                    arrCodigo_ptr_van_pue_int[cont].arg_pue_int_mat = $("#pue_int_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_van_pue_int.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigo_ptr_van_pue_int.splice(pos,1);
                arrDibujo_ptr_van_pue_int.splice(pos,1);
                ////console.log("arrCodigo_ptr_van_pue_int",arrCodigo_ptr_van_pue_int);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_van_pue_int.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_van_pue_int[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVanosPuertasInteriores').html(cadena); 
                
                cont=0;
                arrDibujo_ptr_van_pue_int.forEach(function (item, index, array) 
                {
                    $("#pue_int_estado"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_estado);
                    $("#pue_int_inte"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_inte);
                    $("#pue_int_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_tipo);
                    $("#pue_int_mat"+item.id+"").val(arrCodigo_ptr_van_pue_int[cont].arg_pue_int_mat);
                    
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigo_ptr_van_pue_intFinal",arrCodigo_ptr_van_pue_int);
    }
//////////

/******************-------Vanos Puertas Exteriores**********************/
var contCodigo_ptr_van_pue_ext=2;
    var arrCodigo_ptr_van_pue_ext=[];
    var arrDibujo_ptr_van_pue_ext=[];

    htmlARG_ptr_van_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_ext_estado1" name="pue_ext_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_ext_inte1" name="pue_ext_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_ext_tipo1" name="pue_ext_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+         
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_ext_mat1" name="pue_ext_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_ext(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_van_pue_ext.push({arg_pue_ext_estado:'N', arg_pue_ext_inte:'N', arg_pue_ext_tipo:'N', arg_pue_ext_mat:'N'});
    arrDibujo_ptr_van_pue_ext.push({scd_codigo:htmlARG_ptr_van_pue_ext,id: 1});
    ////console.log("arrCodigo",arrCodigo);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_van_pue_ext[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#divelcoVanosPuertasExteriores').html(cadena);
    $("#pue_ext_estado1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_estado);
    $("#pue_ext_inte1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_inte);
    $("#pue_ext_tipo1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_tipo);
    $("#pue_ext_mat1").val(arrCodigo_ptr_van_pue_ext[0].arg_pue_ext_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_van_pue_ext));


  function elcoVanosPuertasExteriores(){
    var htmlARG_ptr_van_pue_ext;
        htmlARG_ptr_van_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_ext_estado'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_estado'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_ext_inte'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_inte'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_ext_tipo'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_tipo'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+         
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_ext_mat'+contCodigo_ptr_van_pue_ext+'" name="pue_ext_mat'+contCodigo_ptr_van_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_van_pue_ext('+contCodigo_ptr_van_pue_ext+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_estado = $("#pue_ext_estado"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_inte = $("#pue_ext_inte"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_tipo = $("#pue_ext_tipo"+item.id+"").val();
          arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_mat = $("#pue_ext_mat"+item.id+"").val();
          cont++;
        });
      
        arrCodigo_ptr_van_pue_ext.push({arg_pue_ext_estado:'N',arg_pue_ext_inte:'N',arg_pue_ext_tipo:'N',arg_pue_ext_mat:'N'});
        arrDibujo_ptr_van_pue_ext.push({scd_codigo:htmlARG_ptr_van_pue_ext,id: contCodigo_ptr_van_pue_ext});
        contCodigo_ptr_van_pue_ext++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_van_pue_ext.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">' + cont+'. '+arrDibujo_ptr_van_pue_ext[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#divelcoVanosPuertasExteriores').html(cadena); 
        var conto=0;
        arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
        { 
          $("#pue_ext_estado"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_estado);
          $("#pue_ext_inte"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_inte);
          $("#pue_ext_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_tipo);
          $("#pue_ext_mat"+item.id+"").val(arrCodigo_ptr_van_pue_ext[conto].arg_pue_ext_mat);
          conto=conto+1;
        });
       
    }
 
    function menosLinea_ptr_van_pue_ext($id){

        if(contCodigo_ptr_van_pue_ext>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
                {
                arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_estado = $("#pue_ext_estado"+item.id+"").val();
                arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_inte = $("#pue_ext_inte"+item.id+"").val();
                arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_tipo = $("#pue_ext_tipo"+item.id+"").val();
                arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_mat = $("#pue_ext_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_van_pue_ext.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigo_ptr_van_pue_ext.splice(pos,1);
                arrDibujo_ptr_van_pue_ext.splice(pos,1);
                ////console.log("arrCodigo_ptr_van_pue_ext",arrCodigo_ptr_van_pue_ext);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_van_pue_ext.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_van_pue_ext[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcoVanosPuertasExteriores').html(cadena);
                
                cont=0;
                arrDibujo_ptr_van_pue_ext.forEach(function (item, index, array) 
                {
                $("#pue_ext_estado"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_estado);
                $("#pue_ext_inte"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_inte);
                $("#pue_ext_tipo"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_tipo);
                $("#pue_ext_mat"+item.id+"").val(arrCodigo_ptr_van_pue_ext[cont].arg_pue_ext_mat);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigo_ptr_van_pue_extFinal",arrCodigo_ptr_van_pue_ext);
    }
////////6

/******************-------Puertas Interiores**********************/
  var contCodigo_ptr_pue_int=2;
  var arrCodigo_ptr_pue_int=[];
  var arrDibujo_ptr_pue_int=[];

   htmlARG_ptr_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_inte_estado1" name="pue_inte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_inte_inte1" name="pue_inte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_inte_tipo1" name="pue_inte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+           
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_inte_mat1" name="pue_inte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+        
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_int(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_pue_int.push({arg_pue_inte_estado:'N', arg_pue_inte_inte:'N', arg_pue_inte_tipo:'N', arg_pue_inte_mat:'N'});
    arrDibujo_ptr_pue_int.push({scd_codigo:htmlARG_ptr_pue_int,id: 1});
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_pue_int[0].scd_codigo;
    $('#divelcpPuertasInteriores').html(cadena);
    $("#pue_inte_estado1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_estado);
    $("#pue_inte_inte1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_inte);
    $("#pue_inte_tipo1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_tipo);
    $("#pue_inte_mat1").val(arrCodigo_ptr_pue_int[0].arg_pue_inte_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_pue_int));


    function elcpPuertasInteriores(){
    var htmlARG_ptr_pue_int;
        htmlARG_ptr_pue_int='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_inte_estado'+contCodigo_ptr_pue_int+'" name="pue_inte_estado'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_inte_inte'+contCodigo_ptr_pue_int+'" name="pue_inte_inte'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="pue_inte_tipo'+contCodigo_ptr_pue_int+'" name="pue_inte_tipo'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+  
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="pue_inte_mat'+contCodigo_ptr_pue_int+'" name="pue_inte_mat'+contCodigo_ptr_pue_int+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_int('+contCodigo_ptr_pue_int+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_estado = $("#pue_inte_estado"+item.id+"").val();
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_inte = $("#pue_inte_inte"+item.id+"").val();
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_tipo = $("#pue_inte_tipo"+item.id+"").val();
          arrCodigo_ptr_pue_int[cont].arg_pue_inte_mat = $("#pue_inte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_pue_int.push({arg_pue_inte_estado:'N', arg_pue_inte_inte:'N', arg_pue_inte_tipo:'N',arg_pue_inte_mat:'N'});
        arrDibujo_ptr_pue_int.push({scd_codigo:htmlARG_ptr_pue_int,id: contCodigo_ptr_pue_int});
        contCodigo_ptr_pue_int++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_pue_int.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_pue_int[i].scd_codigo;
          cont++;
        }
        $('#divelcpPuertasInteriores').html(cadena); 
        var conto=0;
        arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
        { 
          $("#pue_inte_estado"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_estado);
          $("#pue_inte_inte"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_inte);
          $("#pue_inte_tipo"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_tipo);
          $("#pue_inte_mat"+item.id+"").val(arrCodigo_ptr_pue_int[conto].arg_pue_inte_mat);
          conto=conto+1;
        });
        
    }

 
    function menosLinea_ptr_pue_int($id){
        if(contCodigo_ptr_pue_int>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_estado = $("#pue_inte_estado"+item.id+"").val();
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_inte = $("#pue_inte_inte"+item.id+"").val();
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_tipo = $("#pue_inte_tipo"+item.id+"").val();
                    arrCodigo_ptr_pue_int[cont].arg_pue_inte_mat = $("#pue_inte_mat"+item.id+"").val();

                    cont=cont+1;
                });
                pos = arrDibujo_ptr_pue_int.map(function(e) {return e.id;}).indexOf($id);
                arrCodigo_ptr_pue_int.splice(pos,1);
                arrDibujo_ptr_pue_int.splice(pos,1);
                cont=1;
                for (var i=0; i<arrDibujo_ptr_pue_int.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_pue_int[i].scd_codigo;
                    cont++;
                }
                
                $('#divelcpPuertasInteriores').html(cadena); 
                
                cont=0;
                arrDibujo_ptr_pue_int.forEach(function (item, index, array) 
                {
                    $("#pue_inte_estado"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_estado);
                    $("#pue_inte_inte"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_inte);
                    $("#pue_inte_tipo"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_tipo);
                    $("#pue_inte_mat"+item.id+"").val(arrCodigo_ptr_pue_int[cont].arg_pue_inte_mat);

                    cont=cont+1;
                });
            });
        }
    }
/////////7

/******************-------Puertas Exteriores**********************/
 var contCodigo_ptr_pue_ext=2;
    var arrCodigo_ptr_pue_ext=[];
    var arrDibujo_ptr_pue_ext=[];

   htmlARG_ptr_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_exte_estado1" name="pue_exte_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_exte_inte1" name="pue_exte_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
       '<select class="form-control" id="pue_exte_tipo1" name="pue_exte_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
     '<select class="form-control" id="pue_exte_mat1" name="pue_exte_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_ext(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_pue_ext.push({arg_pue_exte_estado:'N',arg_pue_exte_inte:'N', arg_pue_exte_tipo:'N',arg_pue_exte_mat:'N'});
    arrDibujo_ptr_pue_ext.push({scd_codigo:htmlARG_ptr_pue_ext,id: 1});
    ////console.log("arrCodigo",arrCodigo_ptr_pue_ext);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_pue_ext[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#divelcpPuertasExteriores').html(cadena);
    $("#pue_exte_estado1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_estado);
    $("#pue_exte_inte1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_inte);
    $("#pue_exte_tipo1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_tipo);
    $("#pue_exte_mat1").val(arrCodigo_ptr_pue_ext[0].arg_pue_exte_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_pue_ext));

    function elcpPuertasExteriores(){
    var htmlARG_ptr_pue_ext;
        htmlARG_ptr_pue_ext='Estado de Conservación:</label>'+
    '<select class="form-control" id="pue_exte_estado'+contCodigo_ptr_pue_ext+'" name="pue_exte_estado'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="pue_exte_inte'+contCodigo_ptr_pue_ext+'" name="pue_exte_inte'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
       '<select class="form-control" id="pue_exte_tipo'+contCodigo_ptr_pue_ext+'" name="pue_exte_tipo'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+
         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
     '<select class="form-control" id="pue_exte_mat'+contCodigo_ptr_pue_ext+'" name="pue_exte_mat'+contCodigo_ptr_pue_ext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_pue_ext_ptr_pue_ext('+contCodigo_ptr_pue_ext+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_estado = $("#pue_exte_estado"+item.id+"").val();
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_inte = $("#pue_exte_inte"+item.id+"").val();
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_tipo = $("#pue_exte_tipo"+item.id+"").val();
          arrCodigo_ptr_pue_ext[cont].arg_pue_exte_mat = $("#pue_exte_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_pue_ext.push({arg_pue_exte_estado:'N',arg_pue_exte_inte:'N', arg_pue_exte_tipo:'N',arg_pue_exte_mat:'N'});
        arrDibujo_ptr_pue_ext.push({scd_codigo:htmlARG_ptr_pue_ext,id: contCodigo_ptr_pue_ext});
        contCodigo_ptr_pue_ext++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_pue_ext.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujo_ptr_pue_ext[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#divelcpPuertasExteriores').html(cadena);
        var conto=0;
        arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
        { 
          $("#pue_exte_estado"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_estado);
          $("#pue_exte_inte"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_inte);
          $("#pue_exte_tipo"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_tipo);
          $("#pue_exte_mat"+item.id+"").val(arrCodigo_ptr_pue_ext[conto].arg_pue_exte_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_pue_ext));
    }

 
    function menosLinea_ptr_pue_ext_ptr_pue_ext($id){
        if(contCodigo_ptr_pue_ext>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_estado = $("#pue_exte_estado"+item.id+"").val();
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_inte = $("#pue_exte_inte"+item.id+"").val();
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_tipo = $("#pue_exte_tipo"+item.id+"").val();
                    arrCodigo_ptr_pue_ext[cont].arg_pue_exte_mat = $("#pue_exte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujo_ptr_pue_ext.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigo_ptr_pue_ext.splice(pos,1);
                arrDibujo_ptr_pue_ext.splice(pos,1);
                ////console.log("arrCodigo_ptr_pue_ext",arrCodigo_ptr_pue_ext);
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_pue_ext.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujo_ptr_pue_ext[i].scd_codigo;
                    cont++;
                }
                
                 $('#divelcpPuertasExteriores').html(cadena);
                
                cont=0;
                arrDibujo_ptr_pue_ext.forEach(function (item, index, array) 
                {
                      $("#pue_exte_estado"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_estado);
                      $("#pue_exte_inte"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_inte);
                      $("#pue_exte_tipo"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_tipo);
                      $("#pue_exte_mat"+item.id+"").val(arrCodigo_ptr_pue_ext[cont].arg_pue_exte_mat);
                    
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigo_ptr_pue_extFinal",arrCodigo_ptr_pue_ext);
    }

var contSERV=2;
var arrSERV=[];
var arrDibujoSERV=[];

htmlSERV='Descripción:</label>'+
'<select class="form-control" id="descripcionSERV1" name="descripcionSERV1">'+
'<option value="D" selected="selected">--Seleccione--</option>'+
'<option value="Agua">Agua</option>'+
'<option value="Energia Eléctrica">Energia Eléctrica</option>'+
'<option value="Gas">Gas</option>'+
'<option value="Recolección de Basura">Recolección de Basura</option>'+
'<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
'<option value="Seguridad">Seguridad</option>'+
'<option value="Otros Servicios">Otros Servicios</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-5">'+
'<label class="control-label">Proveedor:</label>'+
'<select class="form-control" id="proveedorSERV1" name="proveedorSERV1">'+
'<option value="P" selected="selected">--Seleccione--</option>'+
'<option value="Red Pública">Red Pública</option>'+
'<option value="Sistema Propio">Sistema Propio</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaSERV(1);"></i>'+
'</a></div>';
var cadena = '';  
arrSERV.push({val_desc:'D', val_prov:'P'});
arrDibujoSERV.push({scd_codigo:htmlSERV,id: 1});
////console.log("arrSERV",arrSERV);

cadena='<div class="form-group col-md-6"><label class="control-label">1. '+arrDibujoSERV[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cservicios').html(cadena);
    $("#descripcionSERV1").val(arrSERV[0].val_desc);
    $("#proveedorSERV1").val(arrSERV[0].val_prov);

    function crearServicios(){
      var htmlSERV;
      htmlSERV='Descripción:</label>'+
      '<select class="form-control" id="descripcionSERV'+contSERV+'" name="descripcionSERV'+contSERV+'">'+
      '<option value="D" selected="selected">--Seleccione--</option>'+
      '<option value="Agua">Agua</option>'+
      '<option value="Energia Eléctrica">Energia Eléctrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Recolección de Basura">Recolección de Basura</option>'+
      '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
      '<option value="Seguridad">Seguridad</option>'+
      '<option value="Otros Servicios">Otros Servicios</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<label class="control-label">Proveedor:</label>'+
      '<select class="form-control" id="proveedorSERV'+contSERV+'" name="proveedorSERV'+contSERV+'">'+
      '<option value="P" selected="selected">--Seleccione--</option>'+
      '<option value="Red Pública">Red Pública</option>'+
      '<option value="Sistema Propio">Sistema Propio</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Servicios" onclick="darBajaSERV('+contSERV+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoSERV.forEach(function (item, index, array) 
      {
        arrSERV[cont].val_desc = $("#descripcionSERV"+item.id+"").val();
        arrSERV[cont].val_prov = $("#proveedorSERV"+item.id+"").val();
        cont++;
      });
      arrSERV.push({val_desc:'D', val_prov:'P'});
      arrDibujoSERV.push({scd_codigo:htmlSERV,id: contSERV});
      contSERV++;
      cont=1;
      for (var i=0; i<arrDibujoSERV.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoSERV[i].scd_codigo;
        cont++;
      }
        console.log("arrDibujoSERV",arrDibujoSERV);
        $('#cservicios').html(cadena); 
        var conto=0;
        arrDibujoSERV.forEach(function (item, index, array) 
        { 
          $("#descripcionSERV"+item.id+"").val(arrSERV[conto].val_desc);
          $("#proveedorSERV"+item.id+"").val(arrSERV[conto].val_prov);
          conto=conto+1;
        });
        console.log("arrSERV",arrSERV);
      }

      function darBajaSERV($id){
        if(contSERV>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoSERV.forEach(function (item, index, array) 
            {
              arrSERV[cont].val_desc=$("#descripcionSERV"+item.id+"").val();
              arrSERV[cont].val_prov=$("#proveedorSERV"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoSERV.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrSERV.splice(pos,1);
            arrDibujoSERV.splice(pos,1);
            ////console.log("arrSERV",arrSERV);

            cont=1;
            for (var i=0; i<arrDibujoSERV.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+arrDibujoSERV[i].scd_codigo;
              cont++;
            }

            $('#cservicios').html(cadena);

            cont=0;
            arrDibujoSERV.forEach(function (item, index, array) 
            {
              $("#descripcionSERV"+item.id+"").val(arrSERV[cont].val_desc);
              $("#proveedorSERV"+item.id+"").val(arrSERV[cont].val_prov);
              cont=cont+1;
            });
          });
        }
        
      }
//--------------------------grilla peligros potenciales---------------------------//
var contPP=2;
var arrPP=[];
var arrDibujoPP=[];

htmlPP='Figura de Peligros Potenciales:</label>'+
'<select class="form-control" id="peligropPP1" name="peligropPP1">'+
'<option value="PP" selected="selected">--Seleccione--</option>'+
'<option value="Desastres Naturales">Desastres Naturales</option>'+
'<option value="Descarasterización">Descarasterización</option>'+
'<option value="Otros Riesgos">Otros Riesgos</option>'+
'<option value="Riesgos Extremos">Riesgos Extremos</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-4">'+
'<label class="control-label">Peligros Pontenciales:</label>'+
'<select class="form-control" id="peligropPPO1" name="peligropPPO1">'+
'<option value="PP" selected="selected">--Seleccione--</option>'+
'<option value="Deslizamientos">Deslizamientos</option>'+
'<option value="Hundimiento de Terreno">Hundimiento de Terreno</option>'+
'<option value="Inundación por filtración de aguas servidasl">Inundación por filtración de aguas servidas</option>'+
'<option value="Inundación por lluvias">Inundación por lluvias</option>'+
'<option value="Sismos">Sismos</option>'+
'<option value="Tormentas Eléctricasl">Tormentas Eléctricas</option>'+
'<option value="Vientos Fuertes">Vientos Fuertes</option>'+
'<option value="Cableado Aereo">Cableado Aereo</option>'+
'<option value="Comercio Informal">Comercio Informal</option>'+
'<option value="Metamorfosis urbana">Metamorfosis urbana</option>'+
'<option value="Parqueo">Parqueo</option>'+
'<option value="Publicidad Exterior inadecuada">Publicidad Exterior inadecuada</option>'+
'<option value="Explosiones">Explosiones</option>'+
'<option value="Fugas y Derrames tóxicos">Fugas y Derrames tóxicos</option>'+
'<option value="Incendios"> Incendios</option>'+
'<option value="Mitines y marchas populares">Mitines y marchas populares</option>'+
'<option value="Cables de alta Tención">Cables de alta Tención</option>'+
'<option value="Construcciones vecinas por colapsar">Construcciones vecinas por colapsar</option>'+
'<option value="Contaminación Acustica">Contaminación Acustica</option>'+
'<option value="Contaminación Ambiental">Contaminación Ambiental</option>'+
'<option value="Ductos en mal estado">Ductos en mal estado</option>'+
'<option value="Objetos ajenos que pueden caer o deslizarse">Objetos ajenos que pueden caer o deslizarse</option>'+
'<option value="Objetos propios queb pueden caer o deslizarse">Objetos propios queb pueden caer o deslizarse</option>'+
'<option value="Sustancias toxicas o inflamables">Sustancias toxicas o inflamables</option>'+
'<option value="Vibraciones">Vibraciones</option>'+
'<option value="Intervenciones no adecuadas">Intervenciones no adecuadas</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-3">'+
'<label class="control-label">Nivel de Peligro:</label>'+
'<select class="form-control" id="causasPP1" name="causasPP1">'+
'<option value="PC" selected="selected">--Seleccione--</option>'+
'<option value="Alto">Alto</option>'+
'<option value="Medio">Medio</option>'+
'<option value="Bajo">Bajo</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Peligros Potenciales" onclick="darBajaPP(1);"></i>'+
'</a></div>';
var cadena = '';  
arrPP.push({val_pe:'PP', val_pel:'PP', val_cau:'PC'});
arrDibujoPP.push({scd_codigo:htmlPP,id: 1});

cadena='<div class="form-group col-md-4"><label class="control-label">1. '+arrDibujoPP[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#peligrospotenciales').html(cadena);
    $("#peligropPP1").val(arrPP[0].val_pe);
    $("#peligropPPO1").val(arrPP[0].val_pel);
    $("#causasPP1").val(arrPP[0].val_cau);
    

   function crearPeligrosPotenciales(){
      var htmlPP;
      htmlPP='Figura de Peligros Potenciales:</label>'+
      '<select class="form-control" id="peligropPP'+contPP+'" name="peligropPP'+contPP+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Desastres Naturales">Desastres Naturales</option>'+
      '<option value="Descarasterización">Descarasterización</option>'+
      '<option value="Otros Riesgos">Otros Riesgos</option>'+
      '<option value="Riesgos Extremos">Riesgos Extremos</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-4">'+
      '<label class="control-label">Peligros Pontenciales:</label>'+
      '<select class="form-control" id="peligropPPO'+contPP+'" name="peligropPPO'+contPP+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Deslizamientos">Deslizamientos</option>'+
      '<option value="Hundimiento de Terreno">Hundimiento de Terreno</option>'+
      '<option value="Inundación por filtración de aguas servidasl">Inundación por filtración de aguas servidas</option>'+
      '<option value="Inundación por lluvias">Inundación por lluvias</option>'+
      '<option value="Sismos">Sismos</option>'+
      '<option value="Tormentas Eléctricasl">Tormentas Eléctricas</option>'+
      '<option value="Vientos Fuertes">Vientos Fuertes</option>'+
      '<option value="Cableado Aereo">Cableado Aereo</option>'+
      '<option value="Comercio Informal">Comercio Informal</option>'+
      '<option value="Metamorfosis urbana">Metamorfosis urbana</option>'+
      '<option value="Parqueo">Parqueo</option>'+
      '<option value="Publicidad Exterior inadecuada">Publicidad Exterior inadecuada</option>'+
      '<option value="Explosiones">Explosiones</option>'+
      '<option value="Fugas y Derrames tóxicos">Fugas y Derrames tóxicos</option>'+
      '<option value="Incendios"> Incendios</option>'+
      '<option value="Mitines y marchas populares">Mitines y marchas populares</option>'+
      '<option value="Cables de alta Tención">Cables de alta Tención</option>'+
      '<option value="Construcciones vecinas por colapsar">Construcciones vecinas por colapsar</option>'+
      '<option value="Contaminación Acustica">Contaminación Acustica</option>'+
      '<option value="Contaminación Ambiental">Contaminación Ambiental</option>'+
      '<option value="Ductos en mal estado">Ductos en mal estado</option>'+
      '<option value="Objetos ajenos que pueden caer o deslizarse">Objetos ajenos que pueden caer o deslizarse</option>'+
      '<option value="Objetos propios queb pueden caer o deslizarse">Objetos propios queb pueden caer o deslizarse</option>'+
      '<option value="Sustancias toxicas o inflamables">Sustancias toxicas o inflamables</option>'+
      '<option value="Vibraciones">Vibraciones</option>'+
      '<option value="Intervenciones no adecuadas">Intervenciones no adecuadas</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Nivel de Peligro:</label>'+
      '<select class="form-control" id="causasPP'+contPP+'" name="causasPP'+contPP+'">'+
      '<option value="PC" selected="selected">--Seleccione--</option>'+
      '<option value="Alto">Alto</option>'+
      '<option value="Medio">Medio</option>'+
      '<option value="Bajo">Bajo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Peligros Potenciales" onclick="darBajaPP('+contPP+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoPP.forEach(function (item, index, array) 
      {
        arrPP[cont].val_pe = $("#peligropPP"+item.id+"").val();
        arrPP[cont].val_pel = $("#peligropPPO"+item.id+"").val();
        arrPP[cont].val_cau = $("#causasPP"+item.id+"").val();
        cont++;
      });

      arrPP.push({val_pe:'PP', val_pel:'PP',val_cau:'PC'});
      arrDibujoPP.push({scd_codigo:htmlPP,id: contPP});
      contPP++;
      cont=1;
      for (var i=0; i<arrDibujoPP.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+arrDibujoPP[i].scd_codigo;
        cont++;
      }
        //////console.log("cadenaaaa",cadena);
        $('#peligrospotenciales').html(cadena); 
        var conto=0;
        arrDibujoPP.forEach(function (item, index, array) 
        { 
          ////console.log('arrPP[conto].val_pe P',arrPP[conto].val_pe);
          $("#peligropPP"+item.id+"").val(arrPP[conto].val_pe);
          $("#peligropPPO"+item.id+"").val(arrPP[conto].val_pel);
          $("#causasPP"+item.id+"").val(arrPP[conto].val_cau);
          conto=conto+1;
        });
      }

      function darBajaPP($id){
        if(contPP>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoPP.forEach(function (item, index, array) 
            {
              arrPP[cont].val_pe=$("#peligropPP"+item.id+"").val();
              arrPP[cont].val_pel=$("#peligropPPO"+item.id+"").val();
              arrPP[cont].val_cau=$("#causasPP"+item.id+"").val();
              cont=cont+1;
            });
            ////console.log('',);
            pos = arrDibujoPP.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrPP.splice(pos,1);
            arrDibujoPP.splice(pos,1);
            ////console.log("arrPP",arrPP);

            cont=1;
            for (var i=0; i<arrDibujoPP.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+cont+arrDibujoPP[i].scd_codigo;
              cont++;
            }

            $('#peligrospotenciales').html(cadena);

            cont=0;
            arrDibujoPP.forEach(function (item, index, array) 
            {
              $("#peligropPP"+item.id+"").val(arrPP[cont].val_pe);
              $("#peligropPPO"+item.id+"").val(arrPP[cont].val_pel);
              $("#causasPP"+item.id+"").val(arrPP[cont].val_cau);
              cont=cont+1;
            });
          });
        }
        ////console.log("arrPPFinal",arrPP);
      }

//-------------------------------------Grillas Patologias --------------------------------------//

var contPE=2;
var arrPE=[];
var arrDibujoPE=[];

htmlPE='Figura de Patologias Edificación:</label>'+
'<select class="form-control" id="dañosPE1" name="dañosPE1">'+
'<option value="PE" selected="selected">--Seleccione--</option>'+
'<option value="Asentamiento">Asentamiento</option>'+
'<option value="Desplomes">Desplomes</option>'+
'<option value="Desprendimientos">Desprendimientos</option>'+
'<option value="Disgregación de Material">Disgregación de Material</option>'+
'<option value="Eflorescencias">Eflorescencias</option>'+
'<option value="Grietas y/o Fisuras">Grietas y/o Fisuras</option>'+
'<option value="Podredumbre">Podredumbre</option>'+
'<option value="Suciedad Superficial">Suciedad Superficial</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-5">'+
'<label class="control-label">Causas:</label>'+
'<select class="form-control" id="causasPE1" name="causasPE1">'+
'<option value="PC" selected="selected">--Seleccione--</option>'+
'<option value="Abandono">Abandono</option>'+
'<option value="Acción del Hombre">Acción del Hombre</option>'+
'<option value="Agentes Botánicos">Agentes Botánicos</option>'+
'<option value="Catástrofe Natural">Catástrofe Natural</option>'+
'<option value="Deficiencias Constructivas">Deficiencias Constructivas</option>'+
'<option value="Falta de Mantenimiento">Falta de Mantenimiento</option>'+
'<option value="Humedad">Humedad</option>'+
'<option value="Insectos">Insectos</option>'+
'<option value="Imtemperie">Imtemperie</option>'+
'<option value="Mala Calidad de Materiales">Mala Calidad de Materiales</option>'+
'</select>'+
'</div>'+
'<div class="form-group col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Patologías Edificación" onclick="darBajaPE(1);"></i>'+
'</a></div>';
var cadena = '';  
arrPE.push({val_daños:'PE', val_causas:'PC'});
arrDibujoPE.push({scd_codigo:htmlPE,id: 1});
////console.log("arrPE",arrPE);

cadena='<div class="form-group col-md-6"><label class="control-label">'+arrDibujoPE[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#patologiaedificacion').html(cadena);
    $("#dañosPE1").val(arrPE[0].val_daños);
    $("#causasPE1").val(arrPE[0].val_causas);


    function crearPatologiaEdificacion(){
      var htmlPE;
      htmlPE='Figura de Patologias Edificación:</label>'+
      '<select class="form-control" id="dañosPE'+contPE+'" name="dañosPE'+contPE+'">'+
      '<option value="PL" selected="selected">--Seleccione--</option>'+
      '<option value="Asentamiento">Asentamiento</option>'+
      '<option value="Desplomes">Desplomes</option>'+
      '<option value="Desprendimientos">Desprendimientos</option>'+
      '<option value="Disgregación de Material">Disgregación de Material</option>'+
      '<option value="Eflorescencias">Eflorescencias</option>'+
      '<option value="Grietas y/o Fisuras">Grietas y/o Fisuras</option>'+
      '<option value="Podredumbre">Podredumbre</option>'+
      '<option value="Suciedad Superficial">Suciedad Superficial</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<label class="control-label">Causas:</label>'+
      '<select class="form-control" id="causasPE'+contPE+'" name="causasPE'+contPE+'">'+
      '<option value="C" selected="selected">--Seleccione--</option>'+
      '<option value="Abandono">Abandono</option>'+
      '<option value="Acción del Hombre">Acción del Hombre</option>'+
      '<option value="Agentes Botánicos">Agentes Botánicos</option>'+
      '<option value="Catástrofe Natural">Catástrofe Natural</option>'+
      '<option value="Deficiencias Constructivas">Deficiencias Constructivas</option>'+
      '<option value="Falta de Mantenimiento">Falta de Mantenimiento</option>'+
      '<option value="Humedad">Humedad</option>'+
      '<option value="Insectos">Insectos</option>'+
      '<option value="Imtemperie">Imtemperie</option>'+
      '<option value="Mala Calidad de Materiales">Mala Calidad de Materiales</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Patologías Edificación" onclick="darBajaPE('+contPE+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoPE.forEach(function (item, index, array) 
      {
        arrPE[cont].val_daños = $("#dañosPE"+item.id+"").val();
        arrPE[cont].val_causas = $("#causasPE"+item.id+"").val();
        cont++;
      });
      arrPE.push({val_daños:'PE', val_causas:'PC'});
      arrDibujoPE.push({scd_codigo:htmlPE,id: contPE});
      contPE++;
      cont=1;
      for (var i=0; i<arrDibujoPE.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoPE[i].scd_codigo;
        cont++;
      }
        //////console.log("cadenaaaa",cadena);
        $('#patologiaedificacion').html(cadena); 
        var conto=0;
        arrDibujoPE.forEach(function (item, index, array) 
        { 
          $("#dañosPE"+item.id+"").val(arrPE[conto].val_daños);
          $("#causasPE"+item.id+"").val(arrPE[conto].val_causas);
          conto=conto+1;
        });
        
      }

      function darBajaPE($id){
        if(contPE>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoPE.forEach(function (item, index, array) 
            {
              arrPE[cont].val_daños=$("#dañosPE"+item.id+"").val();
              arrPE[cont].val_causas=$("#causasPE"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoPE.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrPE.splice(pos,1);
            arrDibujoPE.splice(pos,1);
            ////console.log("arrPE",arrPE);

            cont=1;
            for (var i=0; i<arrDibujoPE.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+arrDibujoPE[i].scd_codigo;
              cont++;
            }

            $('#patologiaedificacion').html(cadena);

            cont=0;
            arrDibujoPE.forEach(function (item, index, array) 
            {
              $("#dañosPE"+item.id+"").val(arrPE[cont].val_daños);
              $("#causasPE"+item.id+"").val(arrPE[cont].val_causas);
              cont=cont+1;
            });
          });
        }
        
    }

 //--------------------------------GRILLA INSTALACIONES ESTADO----------------------//
    var contINE=2;
    var arrINE=[];
    var arrDibujoINE=[];

    htmlINE='Instalacones Estado:</label>'+
    '<select class="form-control" id="instalacionINE1" name="instalacionINE1">'+
    '<option value="IE" selected="selected">--Seleccione--</option>'+
    '<option value="Telefónica">Telefónica</option>'+
    '<option value="Energia Electrica">Energia Electrica</option>'+
    '<option value="Gas">Gas</option>'+
    '<option value="Sanitaria">Sanitaria</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Piso de Servidumbre:</label>'+
    '<select class="form-control" id="pisoservINE1" name="pisoservINE1">'+
    '<option value="PS" selected="selected">--Seleccione--</option>'+
    '<option value="SI">SI</option>'+
    '<option value="NO">NO</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Estado:</label>'+
    '<select class="form-control" id="estadoINE1" name="estadoINE1">'+
    '<option value="E" selected="selected">--Seleccione--</option>'+
    '<option value="Bueno">Bueno</option>'+
    '<option value="Regular">Regular</option>'+
    '<option value="Malo">Malo</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="integridadINE1" name="integridadINE1">'+
    '<option value="II" selected="selected">--Seleccione--</option>'+
    '<option value="Original">Original</option>'+
    '<option value="Nuevo">Nuevo</option>'+
    '<option value="Modificado">Modificado</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Escaleras" onclick="darBajaINE(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrINE.push({var_inst_est:'IE', var_pisose:'PS', var_est:'E', var_int:'II'});
    arrDibujoINE.push({scd_codigo:htmlINE,id: 1});
    ////console.log("arrINE",arrINE);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujoINE[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#cinstalacionesestado').html(cadena);
    $("#instalacionINE1").val(arrINE[0].var_inst_est);
    $("#pisoservINE1").val(arrINE[0].var_pisose);
    $("#estadoINE1").val(arrINE[0].var_est);
    $("#integridadINE1").val(arrINE[0].var_int);


    function crearInstalacionesEstado(){
      var htmlINE;
      htmlINE='Instalacones Estado:</label>'+
      '<select class="form-control" id="instalacionINE'+contINE+'" name="instalacionINE'+contINE+'">'+
      '<option value="IE" selected="selected">--Seleccione--</option>'+
      '<option value="Telefónica">Telefónica</option>'+
      '<option value="Energia Electrica">Energia Electrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Sanitaria">Sanitaria</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Piso de Servidumbre:</label>'+
      '<select class="form-control" id="pisoservINE'+contINE+'" name="pisoservINE'+contINE+'">'+
      '<option value="PS" selected="selected">--Seleccione--</option>'+
      '<option value="SI">SI</option>'+
      '<option value="NO">NO</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Estado:</label>'+
      '<select class="form-control" id="estadoINE'+contINE+'" name="estadoINE'+contINE+'">'+
      '<option value="E" selected="selected">--Seleccione--</option>'+
      '<option value="Bueno">Bueno</option>'+
      '<option value="Regular">Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<label class="control-label">Integridad:</label>'+
      '<select class="form-control" id="integridadINE'+contINE+'" name="integridadINE'+contINE+'">'+
      '<option value="II" selected="selected">--Seleccione--</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Instalaciones Estado" onclick="darBajaINE('+contINE+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoINE.forEach(function (item, index, array) 
      {
        arrINE[cont].var_inst_est = $("#instalacionINE"+item.id+"").val();
        arrINE[cont].var_pisose = $("#pisoservINE"+item.id+"").val();
        arrINE[cont].var_est = $("#estadoINE"+item.id+"").val();
        arrINE[cont].var_int = $("#integridadINE"+item.id+"").val();
        cont++;
      });
      arrINE.push({var_inst_est:'IE', var_pisose:'PS', var_est:'E', var_int:'II'});
      arrDibujoINE.push({scd_codigo:htmlINE,id: contINE});
      contINE++;
      cont=1;
      for (var i=0; i<arrDibujoINE.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoINE[i].scd_codigo;
        cont++;
      }
        //////console.log("cadenaaaa",cadena);
        $('#cinstalacionesestado').html(cadena); 
        var conto=0;
        arrDibujoINE.forEach(function (item, index, array) 
        { 
          $("#instalacionINE"+item.id+"").val(arrINE[conto].var_inst_est);
          $("#pisoservINE"+item.id+"").val(arrINE[conto].var_pisose);
          $("#estadoINE"+item.id+"").val(arrINE[conto].var_est);
          $("#integridadINE"+item.id+"").val(arrINE[conto].var_int);
          conto=conto+1;
        });
        
      }


      function darBajaINE($id){
        if(contINE>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoINE.forEach(function (item, index, array) 
            {
              arrINE[cont].var_inst_est=$("#instalacionINE"+item.id+"").val();
              arrINE[cont].var_pisose=$("#pisoservINE"+item.id+"").val();
              arrINE[cont].var_est=$("#estadoINE"+item.id+"").val();
              arrINE[cont].var_int=$("#integridadINE"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoINE.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrINE.splice(pos,1);
            arrDibujoINE.splice(pos,1);
            ////console.log("arrINE",arrINE);

            cont=1;
            for (var i=0; i<arrDibujoINE.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoINE[i].scd_codigo;
              cont++;
            }

            $('#cinstalacionesestado').html(cadena);

            cont=0;
            arrDibujoINE.forEach(function (item, index, array) 
            {
              $("#instalacionINE"+item.id+"").val(arrINE[cont].var_inst_est);
              $("#pisoservINE"+item.id+"").val(arrINE[cont].var_pisose);
              $("#estadoINE"+item.id+"").val(arrINE[cont].var_est);
              $("#integridadINE"+item.id+"").val(arrINE[cont].var_int);
              cont=cont+1;
            });
          });
        }
        
      }


    //------------------------- Grillas Rejas---------------------------//
    var contCodigo_ptr_reja=2;
    var arrCodigo_ptr_reja=[];
    var arrDibujo_ptr_reja=[];

    htmlARG_ptr_reja='Estado de Conservación:</label>'+
    '<select class="form-control" id="reja_estado1" name="reja_estado1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="reja_inte1" name="reja_inte1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="reja_tipo1" name="reja_tipo1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Figuras Geométricos">Figuras Geométricos</option>'+
              '<option value="Figuras Fitomarfas">Figuras Fitomarfas</option>'+          
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="reja_mat1" name="reja_mat1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Aluminio">Aluminio</option>'+
              '<option value="Hierro Fundido">Hierro Fundido</option>'+
              '<option value="Hierro Forjado">Hierro Forjado</option>'+           
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_reja(1);"></i>'+
    '</a></div>';
    var cadena = '';  
    arrCodigo_ptr_reja.push({arg_reja_estado:'N', arg_reja_inte:'N', arg_reja_tipo:'N', arg_reja_mat:'N'});
    arrDibujo_ptr_reja.push({scd_codigo:htmlARG_ptr_reja,id: 1});
    ////console.log("arrCodigo",arrCodigo_ptr_reja);
    
    cadena='<div class="form-group col-md-3"><label class="control-label">1.'+arrDibujo_ptr_reja[0].scd_codigo;
    //////console.log("cadenaaaa",cadena);
    $('#divelcpRejas').html(cadena);
    $("#reja_estado1").val(arrCodigo_ptr_reja[0].arg_reja_estado);
    $("#reja_inte1").val(arrCodigo_ptr_reja[0].arg_reja_inte);
    $("#reja_tipo1").val(arrCodigo_ptr_reja[0].arg_reja_tipo);
    $("#reja_mat1").val(arrCodigo_ptr_reja[0].arg_reja_mat);
    
    $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_reja));


    function elcpRejas(){
    var htmlARG_ptr_reja;
        htmlARG_ptr_reja='Estado de Conservación:</label>'+
    '<select class="form-control" id="reja_estado'+contCodigo_ptr_reja+'" name="reja_estado'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="reja_inte'+contCodigo_ptr_reja+'" name="reja_inte'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="reja_tipo'+contCodigo_ptr_reja+'" name="reja_tipo'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Figuras Geométricos">Figuras Geométricos</option>'+
              '<option value="Figuras Fitomarfas">Figuras Fitomarfas</option>'+

         

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="reja_mat'+contCodigo_ptr_reja+'" name="reja_mat'+contCodigo_ptr_reja+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Aluminio">Aluminio</option>'+
              '<option value="Hierro Fundido">Hierro Fundido</option>'+
              '<option value="Hierro Forjado">Hierro Forjado</option>'+
              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="menosLinea_ptr_reja('+contCodigo_ptr_reja+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujo_ptr_reja.forEach(function (item, index, array) 
        {
          arrCodigo_ptr_reja[cont].arg_reja_estado = $("#reja_estado"+item.id+"").val();
          arrCodigo_ptr_reja[cont].arg_reja_inte = $("#reja_inte"+item.id+"").val();
          arrCodigo_ptr_reja[cont].arg_reja_tipo = $("#reja_tipo"+item.id+"").val();
          arrCodigo_ptr_reja[cont].arg_reja_mat = $("#reja_mat"+item.id+"").val();
          cont++;
        });
        arrCodigo_ptr_reja.push({arg_reja_estado:'N', arg_reja_inte:'N', arg_reja_tipo:'N', arg_reja_mat:'N'});
        arrDibujo_ptr_reja.push({scd_codigo:htmlARG_ptr_reja,id: contCodigo_ptr_reja});
               
        contCodigo_ptr_reja++;
        cont=1;
        for (var i=0; i<arrDibujo_ptr_reja.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'.'+arrDibujo_ptr_reja[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#divelcpRejas').html(cadena); 
        var conto=0;
        arrDibujo_ptr_reja.forEach(function (item, index, array) 
        { 
          $("#reja_estado"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_estado);
          $("#reja_inte"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_inte);
          $("#reja_tipo"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_tipo);
          $("#reja_mat"+item.id+"").val(arrCodigo_ptr_reja[conto].arg_reja_mat);
          conto=conto+1;
        });
        $('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_reja));
    }

 
    function menosLinea_ptr_reja($id){
        if(contCodigo>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujo_ptr_reja.forEach(function (item, index, array) 
                {
                    arrCodigo_ptr_reja[cont].arg_reja_estado = $("#reja_estado"+item.id+"").val();
                    arrCodigo_ptr_reja[cont].arg_reja_inte = $("#reja_inte"+item.id+"").val();
                    arrCodigo_ptr_reja[cont].arg_reja_tipo = $("#reja_tipo"+item.id+"").val();
                    arrCodigo_ptr_reja[cont].arg_reja_mat = $("#reja_mat"+item.id+"").val();

                    cont=cont+1;
                });
                pos = arrDibujo_ptr_reja.map(function(e) {return e.id;}).indexOf($id);
          
                arrCodigo_ptr_reja.splice(pos,1);
                arrDibujo_ptr_reja.splice(pos,1);
                
                
                cont=1;
                for (var i=0; i<arrDibujo_ptr_reja.length;i++)
                {
                    cadena=cadena + '<div class="form-group"><label class="col-md-3">'+cont+arrDibujo_ptr_reja[i].scd_codigo;
                    cont++;
                }
                
                $('#grillaPrueba').html(cadena);
                
                cont=0;
                arrDibujo_ptr_reja.forEach(function (item, index, array) 
                {
                    $("#reja_estado"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_estado);
                    $("#reja_inte"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_inte);
                    $("#reja_tipo"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_tipo);
                    $("#reja_mat"+item.id+"").val(arrCodigo_ptr_reja[cont].arg_reja_mat);
                    cont=cont+1;
                });
            });
        }
    }

/*----------------------------------END GRILLA ------------------------------*/

//-----------------grillas para editar---------------------------------------
//------EDITAR PELIGRO POTENCIAL------------------------
var arrDibujoPPact = [];
var arrayPPactualizar = [];
var contPPactu = 0;
function editarPeligroPotenciales(longitudGrilla){
  var contPPac=1;
  arrDibujoPPact=[];
  var htmPPac;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmPPac='Figura de Peligros Potenciales:</label>'+
      '<select class="form-control" id="apeligropPP'+contPPac+'" name="apeligropPP'+contPPac+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Desastres Naturales">Desastres Naturales</option>'+
      '<option value="Descarasterización">Descarasterización</option>'+
      '<option value="Otros Riesgos">Otros Riesgos</option>'+
      '<option value="Riesgos Extremos">Riesgos Extremos</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-4">'+
      '<label class="control-label">Peligros Pontenciales:</label>'+
      '<select class="form-control" id="apeligropPPO'+contPPac+'" name="apeligropPPO'+contPPac+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Deslizamientos">Deslizamientos</option>'+
      '<option value="Hundimiento de Terreno">Hundimiento de Terreno</option>'+
      '<option value="Inundación por filtración de aguas servidasl">Inundación por filtración de aguas servidas</option>'+
      '<option value="Inundación por lluvias">Inundación por lluvias</option>'+
      '<option value="Sismos">Sismos</option>'+
      '<option value="Tormentas Eléctricasl">Tormentas Eléctricas</option>'+
      '<option value="Vientos Fuertes">Vientos Fuertes</option>'+
      '<option value="Cableado Aereo">Cableado Aereo</option>'+
      '<option value="Comercio Informal">Comercio Informal</option>'+
      '<option value="Metamorfosis urbana">Metamorfosis urbana</option>'+
      '<option value="Parqueo">Parqueo</option>'+
      '<option value="Publicidad Exterior inadecuada">Publicidad Exterior inadecuada</option>'+
      '<option value="Explosiones">Explosiones</option>'+
      '<option value="Fugas y Derrames tóxicos">Fugas y Derrames tóxicos</option>'+
      '<option value="Incendios"> Incendios</option>'+
      '<option value="Mitines y marchas populares">Mitines y marchas populares</option>'+
      '<option value="Cables de alta Tención">Cables de alta Tención</option>'+
      '<option value="Construcciones vecinas por colapsar">Construcciones vecinas por colapsar</option>'+
      '<option value="Contaminación Acustica">Contaminación Acustica</option>'+
      '<option value="Contaminación Ambiental">Contaminación Ambiental</option>'+
      '<option value="Ductos en mal estado">Ductos en mal estado</option>'+
      '<option value="Objetos ajenos que pueden caer o deslizarse">Objetos ajenos que pueden caer o deslizarse</option>'+
      '<option value="Objetos propios queb pueden caer o deslizarse">Objetos propios queb pueden caer o deslizarse</option>'+
      '<option value="Sustancias toxicas o inflamables">Sustancias toxicas o inflamables</option>'+
      '<option value="Vibraciones">Vibraciones</option>'+
      '<option value="Intervenciones no adecuadas">Intervenciones no adecuadas</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Nivel de Peligro:</label>'+
      '<select class="form-control" id="acausasPP'+contPPac+'" name="acausasPP'+contPPac+'">'+
      '<option value="PC" selected="selected">--Seleccione--</option>'+
      '<option value="Alto">Alto</option>'+
      '<option value="Medio">Medio</option>'+
      '<option value="Bajo">Bajo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Peligros Potenciales" onclick="eliminarPP('+contPPac+');"></i>'+
      '</a></div>';
       arrDibujoPPact.push({scd_codigo:htmPPac,id: contPPac});
        contPPac++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoPPact.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+cont+'. '+arrDibujoPPact[i].scd_codigo;
          cont++;
        }
        $('#apeligrospotenciales').html(cadena);
       }



   function actualizarPeligrosPotenciales(){
      var htmlPPactu;
      htmlPPactu='Figura de Peligros Potenciales:</label>'+
      '<select class="form-control" id="apeligropPP'+contPPactu+'" name="apeligropPP'+contPPactu+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Desastres Naturales">Desastres Naturales</option>'+
      '<option value="Descarasterización">Descarasterización</option>'+
      '<option value="Otros Riesgos">Otros Riesgos</option>'+
      '<option value="Riesgos Extremos">Riesgos Extremos</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-4">'+
      '<label class="control-label">Peligros Pontenciales:</label>'+
      '<select class="form-control" id="apeligropPPO'+contPPactu+'" name="apeligropPPO'+contPPactu+'">'+
      '<option value="PP" selected="selected">--Seleccione--</option>'+
      '<option value="Deslizamientos">Deslizamientos</option>'+
      '<option value="Hundimiento de Terreno">Hundimiento de Terreno</option>'+
      '<option value="Inundación por filtración de aguas servidasl">Inundación por filtración de aguas servidas</option>'+
      '<option value="Inundación por lluvias">Inundación por lluvias</option>'+
      '<option value="Sismos">Sismos</option>'+
      '<option value="Tormentas Eléctricasl">Tormentas Eléctricas</option>'+
      '<option value="Vientos Fuertes">Vientos Fuertes</option>'+
      '<option value="Cableado Aereo">Cableado Aereo</option>'+
      '<option value="Comercio Informal">Comercio Informal</option>'+
      '<option value="Metamorfosis urbana">Metamorfosis urbana</option>'+
      '<option value="Parqueo">Parqueo</option>'+
      '<option value="Publicidad Exterior inadecuada">Publicidad Exterior inadecuada</option>'+
      '<option value="Explosiones">Explosiones</option>'+
      '<option value="Fugas y Derrames tóxicos">Fugas y Derrames tóxicos</option>'+
      '<option value="Incendios"> Incendios</option>'+
      '<option value="Mitines y marchas populares">Mitines y marchas populares</option>'+
      '<option value="Cables de alta Tención">Cables de alta Tención</option>'+
      '<option value="Construcciones vecinas por colapsar">Construcciones vecinas por colapsar</option>'+
      '<option value="Contaminación Acustica">Contaminación Acustica</option>'+
      '<option value="Contaminación Ambiental">Contaminación Ambiental</option>'+
      '<option value="Ductos en mal estado">Ductos en mal estado</option>'+
      '<option value="Objetos ajenos que pueden caer o deslizarse">Objetos ajenos que pueden caer o deslizarse</option>'+
      '<option value="Objetos propios queb pueden caer o deslizarse">Objetos propios queb pueden caer o deslizarse</option>'+
      '<option value="Sustancias toxicas o inflamables">Sustancias toxicas o inflamables</option>'+
      '<option value="Vibraciones">Vibraciones</option>'+
      '<option value="Intervenciones no adecuadas">Intervenciones no adecuadas</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Nivel de Peligro:</label>'+
      '<select class="form-control" id="acausasPP'+contPPactu+'" name="acausasPP'+contPPactu+'">'+
      '<option value="PC" selected="selected">--Seleccione--</option>'+
      '<option value="Alto">Alto</option>'+
      '<option value="Medio">Medio</option>'+
      '<option value="Bajo">Bajo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Peligros Potenciales" onclick="eliminarPP('+contPPactu+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoPPact.forEach(function (item, index, array) 
      {
        arrayPPactualizar[cont].aval_pe = $("#apeligropPP"+item.id+"").val();
        arrayPPactualizar[cont].aval_pel = $("#apeligropPPO"+item.id+"").val();
        arrayPPactualizar[cont].aval_cau = $("#acausasPP"+item.id+"").val();
        cont++;
      });

      arrayPPactualizar.push({aval_pe:'PP', aval_pel:'PP', aval_cau:'PC'});
      console.log('ACTUALIZARRRRRRR',arrayPPactualizar)
      arrDibujoPPact.push({scd_codigo:htmlPPactu,id: contPPactu});
      contPPactu++;
      cont=1;
      for (var i=0; i<arrDibujoPPact.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+arrDibujoPPact[i].scd_codigo;
        cont++;
      }
        //////console.log("cadenaaaa",cadena);
        $('#apeligrospotenciales').html(cadena); 
        var conto=0;
        arrDibujoPPact.forEach(function (item, index, array) 
        { 
          ////console.log('arrPP[conto].val_pe P',arrPP[conto].val_pe);
          $("#apeligropPP"+item.id+"").val(arrayPPactualizar[conto].aval_pe);
          $("#apeligropPPO"+item.id+"").val(arrayPPactualizar[conto].aval_pel);
          $("#acausasPP"+item.id+"").val(arrayPPactualizar[conto].aval_cau);
          conto=conto+1;
        });
      }

//-----------
function eliminarPP($id){
        if(contPPactu>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoPPact.forEach(function (item, index, array) 
            {
              arrayPPactualizar[cont].aval_pe=$("#apeligropPP"+item.id+"").val();
              arrayPPactualizar[cont].aval_pel=$("#apeligropPPO"+item.id+"").val();
              arrayPPactualizar[cont].aval_cau=$("#acausasPP"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoPPact.map(function(e) {return e.id;}).indexOf($id);
            console.log("poos",pos);
            arrayPPactualizar.splice(pos,1);
            arrDibujoPPact.splice(pos,1);
            console.log("arrayPPactualizar",arrayPPactualizar);

            cont=1;
            for (var i=0; i<arrDibujoPPact.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-4"><label class="control-label">'+cont+arrDibujoPPact[i].scd_codigo;
              cont++;
            }

            $('#apeligrospotenciales').html(cadena);

            cont=0;
            arrDibujoPPact.forEach(function (item, index, array) 
            {
              $("#apeligropPP"+item.id+"").val(arrayPPactualizar[cont].aval_pe);
              $("#apeligropPPO"+item.id+"").val(arrayPPactualizar[cont].aval_pel);
               $("#acausasPP"+item.id+"").val(arrayPPactualizar[cont].aval_cau);
              cont=cont+1;
            });
          });
        }
      }
//-----------Fin peligro potencial-----------------------
 //--------Inicio  Patologia edificacion-----------------
 var arrDibujoPEact=[];
var arrPEAct = [];
var contPEact = 0;
function editarPatologiaEdificacion(longitudGrilla){
  var contPEdif=1;
  arrDibujoPEact=[];
  var htmPEact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmPEact='Figura de Patologias Edificación:</label>'+
          '<select class="form-control" id="dañoPEa'+contPEdif+'" name="dañoPEa'+contPEdif+'">'+
          '<option value="PL" selected="selected">--Seleccione--</option>'+
          '<option value="Asentamiento">Asentamiento</option>'+
          '<option value="Desplomes">Desplomes</option>'+
          '<option value="Desprendimientos">Desprendimientos</option>'+
          '<option value="Disgregación de MAterial">Disgregación de MAterial</option>'+
          '<option value="Eflorescencias">Eflorescencias</option>'+
          '<option value="Grietas y/o Fisuras">Grietas y/o Fisuras</option>'+
          '<option value="Podredumbre">Podredumbre</option>'+
          '<option value="Suciedad Superficial">Suciedad Superficial</option>'+
          '</select>'+
          '</div>'+
          '<div class="form-group col-md-5">'+
          '<label class="control-label">Causas:</label>'+
          '<select class="form-control" id="causaPEa'+contPEdif+'" name="causaPEa'+contPEdif+'">'+
          '<option value="C" selected="selected">--Seleccione--</option>'+
          '<option value="Abandono">Abandono</option>'+
          '<option value="Acción del Hombre">Acción del Hombre</option>'+
          '<option value="Agentes Botánicos">Agentes Botánicos</option>'+
          '<option value="Catástrofe Natural">Catástrofe Natural</option>'+
          '<option value="Deficiencias Constructivas">Deficiencias Constructivas</option>'+
          '<option value="Falta de Mantenimiento">Falta de Mantenimiento</option>'+
          '<option value="Humedad">Humedad</option>'+
          '<option value="Insectos">Insectos</option>'+
          '<option value="Imtemperie">Imtemperie</option>'+
          '<option value="Mala Calidad de Materiales">Mala Calidad de Materiales</option>'+
          '</select>'+
          '</div>'+
          '<div class="form-group col-md-1">'+
          '<a style="cursor:pointer;" type="button">'+
          '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Patologías Edificación" onclick="darBajaPEact('+contPEdif+');"></i>'+
          '</a></div>';
       arrDibujoPEact.push({scd_codigo:htmPEact,id: contPEdif});
        contPEdif++;
        }
        var cadenaPE='';
        var cont=1;
        for (var i=0; i<arrDibujoPEact.length;i++)
        {
          cadenaPE=cadenaPE + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoPEact[i].scd_codigo;
          cont++;
        }
        $('#apatologiaedificacion').html(cadenaPE);
       }


function actualizarPatologiaEdificacion(){
      var htmlPEact;
      htmlPEact='Figura de Patologias Edificación:</label>'+
      '<select class="form-control" id="dañoPEa'+contPEact+'" name="dañoPEa'+contPEact+'">'+
      '<option value="PL" selected="selected">--Seleccione--</option>'+
      '<option value="Asentamiento">Asentamiento</option>'+
      '<option value="Desplomes">Desplomes</option>'+
      '<option value="Desprendimientos">Desprendimientos</option>'+
      '<option value="Disgregación de Material">Disgregación de Material</option>'+
      '<option value="Eflorescencias">Eflorescencias</option>'+
      '<option value="Grietas y/o Fisuras">Grietas y/o Fisuras</option>'+
      '<option value="Podredumbre">Podredumbre</option>'+
      '<option value="Suciedad Superficial">Suciedad Superficial</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<label class="control-label">Causas:</label>'+
      '<select class="form-control" id="causaPEa'+contPEact+'" name="causaPEa'+contPEact+'">'+
      '<option value="C" selected="selected">--Seleccione--</option>'+
      '<option value="Abandono">Abandono</option>'+
      '<option value="Acción del Hombre">Acción del Hombre</option>'+
      '<option value="Agentes Botánicos">Agentes Botánicos</option>'+
      '<option value="Catástrofe Natural">Catástrofe Natural</option>'+
      '<option value="Deficiencias Constructivas">Deficiencias Constructivas</option>'+
      '<option value="Falta de Mantenimiento">Falta de Mantenimiento</option>'+
      '<option value="Humedad">Humedad</option>'+
      '<option value="Insectos">Insectos</option>'+
      '<option value="Imtemperie">Imtemperie</option>'+
      '<option value="Mala Calidad de Materiales">Mala Calidad de Materiales</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Patologías Edificación" onclick="darBajaPEact('+contPEact+');"></i>'+
      '</a></div>';
      var cadenaa='';
      var cont=0;
      arrDibujoPEact.forEach(function (item, index, array) 
      {
        arrPEAct[cont].aval_daños = $("#dañoPEa"+item.id+"").val();
        arrPEAct[cont].aval_causas = $("#causaPEa"+item.id+"").val();
        cont++;
      });
      arrPEAct.push({aval_daños:'PE', aval_causas:'PC'});
      arrDibujoPEact.push({scd_codigo:htmlPEact,id: contPEact});
      contPEact++;
      cont=1;
      for (var i=0; i<arrDibujoPEact.length;i++)
      {
        cadenaa=cadenaa + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoPEact[i].scd_codigo;
        cont++;
      }
        //////console.log("cadenaaaa",cadena);
        $('#apatologiaedificacion').html(cadenaa); 
        var conto=0;
        arrDibujoPEact.forEach(function (item, index, array) 
        { 
          $("#dañoPEa"+item.id+"").val(arrPEAct[conto].aval_daños);
          $("#causaPEa"+item.id+"").val(arrPEAct[conto].aval_causas);
          conto=conto+1;
        });
        
      }

function darBajaPEact($id){
        if(contPEact>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoPEact.forEach(function (item, index, array) 
            {
              arrPEAct[cont].aval_daños=$("#dañoPEa"+item.id+"").val();
              arrPEAct[cont].aval_causas=$("#causaPEa"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoPEact.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrPEAct.splice(pos,1);
            arrDibujoPEact.splice(pos,1);
            ////console.log("arrPE",arrPE);

            cont=1;
            for (var i=0; i<arrDibujoPEact.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+arrDibujoPEact[i].scd_codigo;
              cont++;
            }

            $('#apatologiaedificacion').html(cadena);

            cont=0;
            arrDibujoPEact.forEach(function (item, index, array) 
            {
              $("#dañoPEa"+item.id+"").val(arrPEAct[cont].aval_daños);
              $("#causaPEa"+item.id+"").val(arrPEAct[cont].aval_causas);
              cont=cont+1;
            });
          });
        }
      }

      
//--------fin grilla patologoa edificacion-------------------
//------Inicio instalaciones estado--------------------------
var arrINEactualiza=[];
var arrDibujoIEs = [];
var contINEact = 0;
function editarInstalacioEstado(longitudGrilla){
  var contIEst=1;
  arrDibujoIEs=[];
  var htmIEact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmIEact='Instalacones Estado:</label>'+
      '<select class="form-control" id="ainstalacionINE'+contIEst+'" name="ainstalacionINE'+contIEst+'">'+
      '<option value="IE" selected="selected">--Seleccione--</option>'+
      '<option value="Telefónica">Telefónica</option>'+
      '<option value="Energia Electrica">Energia Electrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Sanitaria">Sanitaria</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Piso de Servidumbre:</label>'+
      '<select class="form-control" id="apisoservINE'+contIEst+'" name="apisoservINE'+contIEst+'">'+
      '<option value="PS" selected="selected">--Seleccione--</option>'+
      '<option value="SI">SI</option>'+
      '<option value="NO">NO</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Estado:</label>'+
      '<select class="form-control" id="aestadoINE'+contIEst+'" name="aestadoINE'+contIEst+'">'+
      '<option value="E" selected="selected">--Seleccione--</option>'+
      '<option value="Bueno">Bueno</option>'+
      '<option value="Regular">Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<label class="control-label">Integridad:</label>'+
      '<select class="form-control" id="aintegridadINE'+contIEst+'" name="aintegridadINE'+contIEst+'">'+
      '<option value="II" selected="selected">--Seleccione--</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Instalaciones Estado" onclick="darBajaINEact('+contIEst+');"></i>'+
      '</a></div>';
       arrDibujoIEs.push({scd_codigo:htmIEact,id: contIEst});
        contIEst++;
        }
        var cadenaIEact='';
        var cont=1;
        for (var i=0; i<arrDibujoIEs.length;i++)
        {
          cadenaIEact=cadenaIEact + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoIEs[i].scd_codigo;
          cont++;
        }
        $('#acinstalacionesestado').html(cadenaIEact);
       }


    function actualizarInstalacionesEstado(){
      var htmlINEact;
      htmlINEact='Instalacones Estado:</label>'+
      '<select class="form-control" id="ainstalacionINE'+contINEact+'" name="ainstalacionINE'+contINEact+'">'+
      '<option value="IE" selected="selected">--Seleccione--</option>'+
      '<option value="Telefónica">Telefónica</option>'+
      '<option value="Energia Electrica">Energia Electrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Sanitaria">Sanitaria</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Piso de Servidumbre:</label>'+
      '<select class="form-control" id="apisoservINE'+contINEact+'" name="apisoservINE'+contINEact+'">'+
      '<option value="PS" selected="selected">--Seleccione--</option>'+
      '<option value="SI">SI</option>'+
      '<option value="NO">NO</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<label class="control-label">Estado:</label>'+
      '<select class="form-control" id="aestadoINE'+contINEact+'" name="aestadoINE'+contINEact+'">'+
      '<option value="E" selected="selected">--Seleccione--</option>'+
      '<option value="Bueno">Bueno</option>'+
      '<option value="Regular">Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<label class="control-label">Integridad:</label>'+
      '<select class="form-control" id="aintegridadINE'+contINEact+'" name="aintegridadINE'+contINEact+'">'+
      '<option value="II" selected="selected">--Seleccione--</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Instalaciones Estado" onclick="darBajaINEact('+contINEact+');"></i>'+
      '</a></div>';
      var cadena='';
      var cont=0;
      arrDibujoIEs.forEach(function (item, index, array) 
      {
        arrINEactualiza[cont].avar_inst_est = $("#ainstalacionINE"+item.id+"").val();
        arrINEactualiza[cont].avar_pisose = $("#apisoservINE"+item.id+"").val();
        arrINEactualiza[cont].avar_est = $("#aestadoINE"+item.id+"").val();
        arrINEactualiza[cont].avar_int = $("#aintegridadINE"+item.id+"").val();
        cont++;
      });
      arrINEactualiza.push({avar_inst_est:'IE', avar_pisose:'PS', avar_est:'E', avar_int:'II'});
      arrDibujoIEs.push({scd_codigo:htmlINEact,id: contINEact});
      contINEact++;
      cont=1;
      for (var i=0; i<arrDibujoIEs.length;i++)
      {
        cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoIEs[i].scd_codigo;
        cont++;
      }
        //////console.log("cadenaaaa",cadena);
        $('#acinstalacionesestado').html(cadena); 
        var conto=0;
        arrDibujoIEs.forEach(function (item, index, array) 
        { 
          $("#ainstalacionINE"+item.id+"").val(arrINEactualiza[conto].avar_inst_est);
          $("#apisoservINE"+item.id+"").val(arrINEactualiza[conto].avar_pisose);
          $("#aestadoINE"+item.id+"").val(arrINEactualiza[conto].avar_est);
          $("#aintegridadINE"+item.id+"").val(arrINEactualiza[conto].avar_int);
          conto=conto+1;
        });
        
      }

      //----------------------
       function darBajaINEact($id){
        if(contINEact>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoIEs.forEach(function (item, index, array) 
            {
              arrINEactualiza[cont].avar_inst_est=$("#ainstalacionINE"+item.id+"").val();
              arrINEactualiza[cont].avar_pisose=$("#apisoservINE"+item.id+"").val();
              arrINEactualiza[cont].avar_est=$("#aestadoINE"+item.id+"").val();
              arrINEactualiza[cont].avar_int=$("#aintegridadINE"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoIEs.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrINEactualiza.splice(pos,1);
            arrDibujoIEs.splice(pos,1);
            ////console.log("arrINE",arrINE);

            cont=1;
            for (var i=0; i<arrDibujoIEs.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoIEs[i].scd_codigo;
              cont++;
            }

            $('#acinstalacionesestado').html(cadena);

            cont=0;
            arrDibujoIEs.forEach(function (item, index, array) 
            {
              $("#ainstalacionINE"+item.id+"").val(arrINEactualiza[cont].avar_inst_est);
              $("#apisoservINE"+item.id+"").val(arrINEactualiza[cont].avar_pisose);
              $("#aestadoINE"+item.id+"").val(arrINEactualiza[cont].avar_est);
              $("#aintegridadINE"+item.id+"").val(arrINEactualiza[cont].avar_int);
              cont=cont+1;
            });
          });
        }   
      }

//--------fin instalaciones estado------------------------
//---------Inicio grilla Servicios------------------------
var arrDibujoServAct=[];
var arrSERVact = [];
var contSact = 0;
function editarServicios(longitudGrilla){
  var contServAct=1;
  arrDibujoServAct=[];
  var htmIEact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmIEact='Descripción:</label>'+
      '<select class="form-control" id="adescripcionSERV'+contServAct+'" name="adescripcionSERV'+contServAct+'">'+
      '<option value="D" selected="selected">--Seleccione--</option>'+
      '<option value="Agua">Agua</option>'+
      '<option value="Energia Eléctrica">Energia Eléctrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Recolección de Basura">Recolección de Basura</option>'+
      '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
      '<option value="Seguridad">Seguridad</option>'+
      '<option value="Otros Servicios">Otros Servicios</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<label class="control-label">Proveedor:</label>'+
      '<select class="form-control" id="aproveedorSERV'+contServAct+'" name="aproveedorSERV'+contServAct+'">'+
      '<option value="P" selected="selected">--Seleccione--</option>'+
      '<option value="Red Pública">Red Pública</option>'+
      '<option value="Sistema Propio">Sistema Propio</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Servicios" onclick="darBajaSERVact('+contServAct+');"></i>'+
      '</a></div>';
       arrDibujoServAct.push({scd_codigo:htmIEact,id: contServAct});
        contServAct++;
        }
        var cadenaSeraact='';
        var cont=1;
        for (var i=0; i<arrDibujoServAct.length;i++)
        {
          cadenaSeraact=cadenaSeraact + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoServAct[i].scd_codigo;
          cont++;
        }
        $('#aservicios').html(cadenaSeraact);
       }

//-------------------------------
 function actualizarServicios(){
      var htmlSact;
      htmlSact='Descripción:</label>'+
      '<select class="form-control" id="adescripcionSERV'+contSact+'" name="adescripcionSERV'+contSact+'">'+
      '<option value="D" selected="selected">--Seleccione--</option>'+
      '<option value="Agua">Agua</option>'+
      '<option value="Energia Eléctrica">Energia Eléctrica</option>'+
      '<option value="Gas">Gas</option>'+
      '<option value="Recolección de Basura">Recolección de Basura</option>'+
      '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
      '<option value="Seguridad">Seguridad</option>'+
      '<option value="Otros Servicios">Otros Servicios</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<label class="control-label">Proveedor:</label>'+
      '<select class="form-control" id="aproveedorSERV'+contSact+'" name="aproveedorSERV'+contSact+'">'+
      '<option value="P" selected="selected">--Seleccione--</option>'+
      '<option value="Red Pública">Red Pública</option>'+
      '<option value="Sistema Propio">Sistema Propio</option>'+
      '</select>'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar Servicios" onclick="darBajaSERVact('+contSact+');"></i>'+
      '</a></div>';
      var cadenaa='';
      var cont=0;
      arrDibujoServAct.forEach(function (item, index, array) 
      {
        arrSERVact[cont].aval_desc = $("#adescripcionSERV"+item.id+"").val();
        arrSERVact[cont].aval_prov = $("#aproveedorSERV"+item.id+"").val();
        cont++;
      });
      arrSERVact.push({aval_desc:'D', aval_prov:'P'});
      arrDibujoServAct.push({scd_codigo:htmlSact,id: contSact});
      contSact++;
      cont=1;
      for (var i=0; i<arrDibujoServAct.length;i++)
      {
        cadenaa=cadenaa + '<div class="form-group col-md-6"><label class="control-label">'+cont+'. '+arrDibujoServAct[i].scd_codigo;
        cont++;
      }
        //console.log("arrDibujoServAct",arrDibujoServAct);
        $('#aservicios').html(cadenaa); 
        var conto=0;
        arrDibujoServAct.forEach(function (item, index, array) 
        { 
          $("#adescripcionSERV"+item.id+"").val(arrSERVact[conto].aval_desc);
          $("#aproveedorSERV"+item.id+"").val(arrSERVact[conto].aval_prov);
          conto=conto+1;
        });
        console.log("arrSERV",arrSERVact);
      }

      //------------function darBajaSERV($id){
        
      function darBajaSERVact($id){
        if(contSact>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el registro seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadena='';
            var cont=0;
            var pos=-1;
            arrDibujoServAct.forEach(function (item, index, array) 
            {
              arrSERVact[cont].val_desc=$("#adescripcionSERV"+item.id+"").val();
              arrSERVact[cont].val_prov=$("#aproveedorSERV"+item.id+"").val();
              cont=cont+1;
            });
            pos = arrDibujoServAct.map(function(e) {return e.id;}).indexOf($id);
            ////console.log("poos",pos);
            arrSERVact.splice(pos,1);
            arrDibujoServAct.splice(pos,1);
            ////console.log("arrSERV",arrSERV);

            cont=1;
            for (var i=0; i<arrDibujoServAct.length;i++)
            {
              cadena=cadena + '<div class="form-group col-md-6"><label class="control-label">'+cont+arrDibujoServAct[i].scd_codigo;
              cont++;
            }

            $('#aservicios').html(cadena);

            cont=0;
            arrDibujoServAct.forEach(function (item, index, array) 
            {
              $("#adescripcionSERV"+item.id+"").val(arrSERVact[cont].val_desc);
              $("#aproveedorSERV"+item.id+"").val(arrSERVact[cont].val_prov);
              cont=cont+1;
            });
          });
        }
        
      }

//------fin grilla servicios-----------------------------------







   



//------Incio grilla vanos ventanas interiores--

var arrDibujoVVInt = [];
var arrVVIactualizar = [];
var contVVIact = 0;

  function editarVanosVentanasInt(longitudGrilla){
  var contVVint=1;
  arrDibujoVVInt=[];
  var htmVVInt;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmVVInt='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_int_estado'+contVVint+'" name="avent_int_estado'+contVVint+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_int_inte'+contVVint+'" name="avent_int_inte'+contVVint+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_int_tipo'+contVVint+'" name="avent_int_tipo'+contVVint+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+

              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_int_mat'+contVVint+'" name="avent_int_mat'+contVVint+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVVIntact('+contVVint+');"></i>'+
    '</a></div>';
       arrDibujoVVInt.push({scd_codigo:htmVVInt,id: contVVint});
        contVVint++;
        }
        var cadenaVVIact='';
        var cont=1;
        for (var i=0; i<arrDibujoVVInt.length;i++)
        {
          cadenaVVIact=cadenaVVIact + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVVInt[i].scd_codigo;
          cont++;
        }
        $('#aVanosVentanasInteriores').html(cadenaVVIact);
       }

function actualizarVanosVentanasInteriores(){
    
    var htmlVVInact;
    htmlVVInact='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_int_estado'+contVVIact+'" name="avent_int_estado'+contVVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_int_inte'+contVVIact+'" name="avent_int_inte'+contVVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_int_tipo'+contVVIact+'" name="avent_int_tipo'+contVVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+          
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_int_mat'+contVVIact+'" name="avent_int_mat'+contVVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+       
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVVIntact('+contVVIact+');"></i>'+
    '</a></div>';
        //RECUPERA VALORES EN LA GRILLA
        var cadenaVVIact='';
        var cont=0;
        arrDibujoVVInt.forEach(function (item, index, array) 
        {
          arrVVIactualizar[cont].aarg_vent_int_estado = $("#avent_int_estado"+item.id+"").val();
          arrVVIactualizar[cont].aarg_vent_int_inte = $("#avent_int_inte"+item.id+"").val();
          arrVVIactualizar[cont].aarg_vent_int_tipo = $("#avent_int_tipo"+item.id+"").val();
          arrVVIactualizar[cont].aarg_vent_int_mat = $("#avent_int_mat"+item.id+"").val();
          cont++;
        });

        arrVVIactualizar.push({aarg_vent_int_estado:'N', aarg_vent_int_inte:'N', aarg_vent_int_tipo:'N', aarg_vent_int_mat:'N'});  
        arrDibujoVVInt.push({scd_codigo:htmlVVInact,id: contVVIact});
        contVVIact++;
        cont=1;
        //CONCATENA HTML GRILLA EN LA VISTA
        ////console.log(arrDibujoEleComVanosVenInteriores.length);
        for (var i=0; i<arrDibujoVVInt.length;i++)
        {
          cadenaVVIact=cadenaVVIact +'<div class="form-group col-md-3"><label class="control-label">'+cont+'.'+arrDibujoVVInt[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadenaEleComVanosVenInteriores);
        $('#aVanosVentanasInteriores').html(cadenaVVIact);

        //MUESTRA VALORES EN LA GRILLA
        var conto=0;
        arrDibujoVVInt.forEach(function (item, index, array) 
        { 
          $("#avent_int_estado"+item.id+"").val(arrVVIactualizar[conto].aarg_vent_int_estado);
          $("#avent_int_inte"+item.id+"").val(arrVVIactualizar[conto].aarg_vent_int_inte);
          $("#avent_int_tipo"+item.id+"").val(arrVVIactualizar[conto].aarg_vent_int_tipo);
          $("#avent_int_mat"+item.id+"").val(arrVVIactualizar[conto].aarg_vent_int_mat);
          conto=conto+1;
        });
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo));
    }

    function eliminarVVIntact($id){
        if(contVVIact>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaVVIact='';
                var cont=0;
                var pos=-1;
                arrDibujoVVInt.forEach(function (item, index, array) 
                {
                    arrVVIactualizar[cont].aarg_vent_int_estado = $("#avent_int_estado"+item.id+"").val();
                    arrVVIactualizar[cont].aarg_vent_int_inte = $("#avent_int_inte"+item.id+"").val();
                    arrVVIactualizar[cont].aarg_vent_int_tipo = $("#avent_int_tipo"+item.id+"").val();
                    arrVVIactualizar[cont].aarg_vent_int_mat = $("#avent_int_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoVVInt.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrVVIactualizar.splice(pos,1);
                arrDibujoVVInt.splice(pos,1);
                //////console.log("arrCodigo",arrCodigoEleComVanosVenInteriores);
                cont=1;
                for (var i=0; i<arrDibujoVVInt.length;i++)
                {
                    cadenaVVIact=cadenaVVIact + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVVInt[i].scd_codigo;
                    cont++;
                }                
                $('#aVanosVentanasInteriores').html(cadenaVVIact);
                cont=0;
                arrDibujoVVInt.forEach(function (item, index, array) 
                {
                    $("#avent_int_estado"+item.id+"").val(arrVVIactualizar[cont].aarg_vent_int_estado);
                    $("#avent_int_inte"+item.id+"").val(arrVVIactualizar[cont].aarg_vent_int_inte);
                    $("#avent_int_tipo"+item.id+"").val(arrVVIactualizar[cont].aarg_vent_int_tipo);
                    $("#avent_int_mat"+item.id+"").val(arrVVIactualizar[cont].aarg_vent_int_mat);
                    //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
    }
//-------fin grilla vanos ventanas interiores--------------
//-------Inicio grilla ventanas interiores-----------------
var arrDibujoVIact=[];
var arrayVIactualizar = [];
var contVIac = 0;
function editarVentanasInteriores(longitudGrilla){
  var contVIact=1;
  arrDibujoVIact=[];
  var htmVIact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmVIact='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_inte_estado'+contVIact+'" name="avent_inte_estado'+contVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_inte_inte'+contVIact+'" name="avent_inte_inte'+contVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_inte_tipo'+contVIact+'" name="avent_inte_tipo'+contVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+                     
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_inte_mat'+contVIact+'" name="avent_inte_mat'+contVIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVIact('+contVIact+');"></i>'+
    '</a></div>';
       arrDibujoVIact.push({scd_codigo:htmVIact,id: contVIact});
        contVIact++;
        }
        var cadenaVIact='';
        var cont=1;
        for (var i=0; i<arrDibujoVIact.length;i++)
        {
          cadenaVIact=cadenaVIact + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVIact[i].scd_codigo;
          cont++;
        }
        $('#adivelcoVentanasInteriores').html(cadenaVIact);
       }

function actualizarVentanasInteriores(){
    var htmlVIact;
        htmlVIact='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_inte_estado'+contVIac+'" name="avent_inte_estado'+contVIac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_inte_inte'+contVIac+'" name="avent_inte_inte'+contVIac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_inte_tipo'+contVIac+'" name="avent_inte_tipo'+contVIac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+                            
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_inte_mat'+contVIac+'" name="avent_inte_mat'+contVIac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+             
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVIact('+contVIac+');"></i>'+
    '</a></div>';
        var cadenaVIact='';
        var cont=0;
        arrDibujoVIact.forEach(function (item, index, array) 
        {
          arrayVIactualizar[cont].aarg_vent_inte_estado = $("#avent_inte_estado"+item.id+"").val();
          arrayVIactualizar[cont].aarg_vent_inte_inte = $("#avent_inte_inte"+item.id+"").val();
          arrayVIactualizar[cont].aarg_vent_inte_tipo = $("#avent_inte_tipo"+item.id+"").val();
          arrayVIactualizar[cont].aarg_vent_inte_mat = $("#avent_inte_mat"+item.id+"").val();
          cont++;
        });
        arrayVIactualizar.push({aarg_vent_inte_estado:'N', aarg_vent_inte_inte:'N', aarg_vent_inte_tipo:'N', aarg_vent_inte_mat:'N'});
        arrDibujoVIact.push({scd_codigo:htmlVIact,id: contVIac});
        contVIac++;
        cont=1;
        for (var i=0; i<arrDibujoVIact.length; i++)
        {
          cadenaVIact=cadenaVIact + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVIact[i].scd_codigo;
          cont++;
        }
      
         $('#adivelcoVentanasInteriores').html(cadenaVIact); 
        var conto=0;
        arrDibujoVIact.forEach(function (item, index, array) 
        { 
          $("#avent_inte_estado"+item.id+"").val(arrayVIactualizar[conto].aarg_vent_inte_estado);
          $("#avent_inte_inte"+item.id+"").val(arrayVIactualizar[conto].aarg_vent_inte_inte);
          $("#avent_inte_tipo"+item.id+"").val(arrayVIactualizar[conto].aarg_vent_inte_tipo);
          $("#avent_inte_mat"+item.id+"").val(arrayVIactualizar[conto].aarg_vent_inte_mat);
          conto=conto+1;
        });
        
    }

function eliminarVIact($id){
        if(contVIac>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaVIact='';
                var cont=0;
                var pos=-1;
                arrDibujoVIact.forEach(function (item, index, array) 
                {
                    arrayVIactualizar[cont].aarg_vent_inte_estado = $("#avent_inte_estado"+item.id+"").val();
                    arrayVIactualizar[cont].aarg_vent_inte_inte = $("#avent_inte_inte"+item.id+"").val();
                    arrayVIactualizar[cont].aarg_vent_inte_tipo = $("#avent_inte_tipo"+item.id+"").val();
                    arrayVIactualizar[cont].aarg_vent_inte_mat = $("#avent_inte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoVIact.map(function(e) {return e.id;}).indexOf($id);
                arrayVIactualizar.splice(pos,1);
                arrDibujoVIact.splice(pos,1);
                
                cont=1;
                for (var i=0; i<arrDibujoVIact.length;i++)
                {
                    cadenaVIact=cadenaVIact + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVIact[i].scd_codigo;
                    cont++;
                }
                
                $('#adivelcoVentanasInteriores').html(cadenaVIact);
                
                cont=0;
                arrDibujoVIact.forEach(function (item, index, array) 
                {   
                    $("#avent_inte_estado"+item.id+"").val(arrayVIactualizar[cont].aarg_vent_inte_estado);
                    $("#avent_inte_inte"+item.id+"").val(arrayVIactualizar[cont].aarg_vent_inte_inte);
                    $("#avent_inte_tipo"+item.id+"").val(arrayVIactualizar[cont].aarg_vent_inte_tipo);
                    $("#avent_inte_mat"+item.id+"").val(arrayVIactualizar[cont].aarg_vent_inte_mat);
                   ;//document.getElementById("tipoarg"+item.id+"").value(arrCodigovent_inte_es_in[cont].arg_tipo);
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigovent_inte_es_inFinal",arrCodigovent_inte_es_in);
    }
 

 //------fin grilla ventanas interiores------------------
 //-----Inicio grilla vanos ventanas exteriores----------------
 var arrDibujoVVExt = [];
var arrayVVExtact = [];
var contVVExtact = 0;
function editarVanosVentanasExteriores(longitudGrilla){
  var contVVext=1;
  arrDibujoVVExt=[];
  var htmVVInt;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmVVInt='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_ext_estado'+contVVext+'" name="avent_ext_estado'+contVVext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_ext_inte'+contVVext+'" name="avent_ext_inte'+contVVext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_ext_tipo'+contVVext+'" name="avent_ext_tipo'+contVVext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+   
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_ext_mat'+contVVext+'" name="avent_ext_mat'+contVVext+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVVExtAct('+contVVext+');"></i>'+
    '</a></div>';
       arrDibujoVVExt.push({scd_codigo:htmVVInt,id: contVVext});
        contVVext++;
        }
        var cadenaVVEact='';
        var cont=1;
        for (var i=0; i<arrDibujoVVExt.length;i++)
        {
          cadenaVVEact=cadenaVVEact + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVVExt[i].scd_codigo;
          cont++;
        }
        $('#aVanosVentanasExteriores').html(cadenaVVEact);
       }


function actualuizarVanosVentanasExteriores(){

        var htmlVVExtact;
        htmlVVExtact='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_ext_estado'+contVVExtact+'" name="avent_ext_estado'+contVVExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_ext_inte'+contVVExtact+'" name="avent_ext_inte'+contVVExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_ext_tipo'+contVVExtact+'" name="avent_ext_tipo'+contVVExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+   
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_ext_mat'+contVVExtact+'" name="avent_ext_mat'+contVVExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVVExtAct('+contVVExtact+');"></i>'+
    '</a></div>';
        var cadenaVVExtcont='';
        var cont=0;
        arrDibujoVVExt.forEach(function (item, index, array) 
        {
          arrayVVExtact[cont].aarg_vent_ext_estado = $("#avent_ext_estado"+item.id+"").val();
          arrayVVExtact[cont].aarg_vent_ext_inte = $("#avent_ext_inte"+item.id+"").val();
          arrayVVExtact[cont].aarg_vent_ext_tipo = $("#avent_ext_tipo"+item.id+"").val();
          arrayVVExtact[cont].aarg_vent_ext_mat = $("#avent_ext_mat"+item.id+"").val();

          cont++;
        });
        arrayVVExtact.push({aarg_vent_ext_estado:'N', aarg_vent_ext_inte:'N', aarg_vent_ext_tipo:'N', aarg_vent_ext_mat:'N'});
        arrDibujoVVExt.push({scd_codigo:htmlVVExtact,id: contVVExtact});
        contVVExtact++;
        cont=1;
        for (var i=0; i<arrDibujoVVExt.length;i++)
        {
          cadenaVVExtcont=cadenaVVExtcont + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVVExt[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#aVanosVentanasExteriores').html(cadenaVVExtcont); 
        var conto=0;
        arrDibujoVVExt.forEach(function (item, index, array) 
        { 
          $("#avent_ext_estado"+item.id+"").val(arrayVVExtact[conto].aarg_vent_ext_estado);
          $("#avent_ext_inte"+item.id+"").val(arrayVVExtact[conto].aarg_vent_ext_inte);
          $("#avent_ext_tipo"+item.id+"").val(arrayVVExtact[conto].aarg_vent_ext_tipo);
          $("#avent_ext_mat"+item.id+"").val(arrayVVExtact[conto].aarg_vent_ext_mat);
   
          conto=conto+1;
        });
        
    }

    function eliminarVVExtAct($id){
        if(contVVExtact>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoVVExt.forEach(function (item, index, array) 
                {
                  arrayVVExtact[cont].aarg_vent_ext_estado = $("#avent_ext_estado"+item.id+"").val();
              arrayVVExtact[cont].aarg_vent_ext_inte = $("#avent_ext_inte"+item.id+"").val();
              arrayVVExtact[cont].aarg_vent_ext_tipo = $("#avent_ext_tipo"+item.id+"").val();
              arrayVVExtact[cont].aarg_vent_ext_mat = $("#avent_ext_mat"+item.id+"").val();
                  cont=cont+1;
                });
                pos = arrDibujoVVExt.map(function(e) {return e.id;}).indexOf($id);
                arrayVVExtact.splice(pos,1);
                arrDibujoVVExt.splice(pos,1); 
                cont=1;
                for (var i=0; i<arrDibujoVVExt.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVVExt[i].scd_codigo;
                    cont++;
                }
                
                $('#aVanosVentanasExteriores').html(cadena);
                
                cont=0;
                arrDibujoVVExt.forEach(function (item, index, array) 
                {  
                  $("#avent_ext_estado"+item.id+"").val(arrayVVExtact[cont].aarg_vent_ext_estado);
                  $("#avent_ext_inte"+item.id+"").val(arrayVVExtact[cont].aarg_vent_ext_inte);
                  $("#avent_ext_tipo"+item.id+"").val(arrayVVExtact[cont].aarg_vent_ext_tipo);
                  $("#avent_ext_mat"+item.id+"").val(arrayVVExtact[cont].aarg_vent_ext_mat);
                  cont=cont+1;
                });
            });
        }   
    }
//------fin grilla ventanas exteriores----------------------------
//------Inicio grilla ventanas exteriores-------------------------

  var arrDibujoVExact=[];
  var arrayVExactualizar = [];
  var contVExactualizar = 0;
  function editarVentanasExteriores(longitudGrilla){
  var contVExAct=1;
  arrDibujoVExact=[];
  var htmVExact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmVExact='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_exte_estado'+contVExAct+'" name="avent_exte_estado'+contVExAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_exte_inte'+contVExAct+'" name="avent_exte_inte'+contVExAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_exte_tipo'+contVExAct+'" name="avent_exte_tipo'+contVExAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_exte_mat'+contVExAct+'" name="avent_exte_mat'+contVExAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio Claro">Vidrio Claro</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+        
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVExactualizar('+contVExAct+');"></i>'+
    '</a></div>';
       arrDibujoVExact.push({scd_codigo:htmVExact,id: contVExAct});
        contVExAct++;
        }
        var cadenaVVIact='';
        var cont=1;
        for (var i=0; i<arrDibujoVExact.length;i++)
        {
          cadenaVVIact=cadenaVVIact + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVExact[i].scd_codigo;
          cont++;
        }
        $('#aVentanasExteriores').html(cadenaVVIact);
       }

function actualizarVentanasExteriores(){
    var htmlVEactualizar;
        htmlVEactualizar='Estado de Conservación:</label>'+
    '<select class="form-control" id="avent_exte_estado'+contVExactualizar+'" name="avent_exte_estado'+contVExactualizar+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="avent_exte_inte'+contVExactualizar+'" name="avent_exte_inte'+contVExactualizar+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="avent_exte_tipo'+contVExactualizar+'" name="avent_exte_tipo'+contVExactualizar+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Ventana abatible vertical">Ventana abatible vertical</option>'+
              '<option value="Ventana de Báscula">Ventana de Báscula</option>'+
              '<option value="Ventana de Buhardilla">Ventana de Buhardilla</option>'+
              '<option value="Ventana corredera">Ventana corredera</option>'+
              '<option value="Ventana de Pivote">Ventana de Pivote</option>'+
              '<option value="Ventana Abatible Horizontal">Ventana Abatible Horizontal</option>'+
              '<option value="Ventana Giratoria">Ventana Giratoria</option>'+
              '<option value="Ventana Guillotina">Ventana Guillotina</option>'+
              '<option value="Con Contraventana">Con Contraventana</option>'+  
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="avent_exte_mat'+contVExactualizar+'" name="avent_exte_mat'+contVExactualizar+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Vidrio Claro">Vidrio Claro</option>'+
              '<option value="Policarbonato">Policarbonato</option>'+
              '<option value="Vitroblok">Vitroblok</option>'+
              '<option value="Alabastro">Alabastro</option>'+
              '<option value="Acrílico">Acrílico</option>'+
              '<option value="Vidrio Esmerilado">Vidrio Esmerilado</option>'+    
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVExactualizar('+contVExactualizar+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoVExact.forEach(function (item, index, array) 
        {
          arrayVExactualizar[cont].aarg_vent_exte_estado = $("#avent_exte_estado"+item.id+"").val();
          arrayVExactualizar[cont].aarg_vent_exte_inte = $("#avent_exte_inte"+item.id+"").val();
          arrayVExactualizar[cont].aarg_vent_exte_tipo = $("#avent_exte_tipo"+item.id+"").val();
          arrayVExactualizar[cont].aarg_vent_exte_mat = $("#avent_exte_mat"+item.id+"").val();
          cont++;
        });
        arrayVExactualizar.push({aarg_vent_exte_estado:'N', aarg_vent_exte_inte:'N',aarg_vent_exte_tipo:'N',aarg_vent_exte_mat:'N' });
        arrDibujoVExact.push({scd_codigo:htmlVEactualizar,id: contVExactualizar});
        contVExactualizar++;
        cont=1;
        for (var i=0; i<arrDibujoVExact.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVExact[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#aVentanasExteriores').html(cadena); 
        var conto=0;
        arrDibujoVExact.forEach(function (item, index, array) 
        { 
          $("#avent_exte_estado"+item.id+"").val(arrayVExactualizar[conto].aarg_vent_exte_estado);
          $("#avent_exte_inte"+item.id+"").val(arrayVExactualizar[conto].aarg_vent_exte_inte);
          $("#avent_exte_tipo"+item.id+"").val(arrayVExactualizar[conto].aarg_vent_exte_tipo);
          $("#avent_exte_mat"+item.id+"").val(arrayVExactualizar[conto].aarg_vent_exte_mat);
          conto=conto+1;
        });
        //$('#grillaPrueba').val(JSON.stringify(arrCodigo_ptr_vent_ext));
    }

    function eliminarVExactualizar($id){
        if(contVExactualizar>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoVExact.forEach(function (item, index, array) 
                {
                arrayVExactualizar[cont].aarg_vent_exte_estado = $("#avent_exte_estado"+item.id+"").val();
                arrayVExactualizar[cont].aarg_vent_exte_inte = $("#avent_exte_inte"+item.id+"").val();
                arrayVExactualizar[cont].aarg_vent_exte_tipo = $("#avent_exte_tipo"+item.id+"").val();
                arrayVExactualizar[cont].aarg_vent_exte_mat = $("#avent_exte_mat"+item.id+"").val();
                cont=cont+1;
                });
                pos = arrDibujoVExact.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrCodigo_ptr_vent_ext.splice(pos,1);
                arrDibujoVExact.splice(pos,1);
                ////console.log("arrCodigo_ptr_vent_ext",arrCodigo_ptr_vent_ext);
                
                cont=1;
                for (var i=0; i<arrDibujoVExact.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVExact[i].scd_codigo;
                    cont++;
                }
                
                $('#aVentanasExteriores').html(cadena);
                
                cont=0;
                arrDibujoVExact.forEach(function (item, index, array) 
                {
                  $("#avent_exte_estado"+item.id+"").val(arrayVExactualizar[cont].aarg_vent_exte_estado);
                  $("#avent_exte_inte"+item.id+"").val(arrayVExactualizar[cont].aarg_vent_exte_inte);
                  $("#avent_exte_tipo"+item.id+"").val(arrayVExactualizar[cont].aarg_vent_exte_tipo);
                  $("#avent_exte_mat"+item.id+"").val(arrayVExactualizar[cont].aarg_vent_exte_mat);
                  cont=cont+1;
                });
            });
        }
        //////console.log("arrCodigo_ptr_vent_extFinal",arrCodigo_ptr_vent_ext);
    }

//------fin grilla ventanas exteriores----------------------------
//------Inicio grilla vanos puertas exteriores-------------------
var arrDibujoVPIn = [];
var arrayVPIntact = [];    
var contVPIntactua = 0;
function editarVanosPuertasInteriores(longitudGrilla){
  var contVPIact=1;
  arrDibujoVPIn=[];
  var htmVPIntact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmVPIntact='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_int_estado'+contVPIact+'" name="apue_int_estado'+contVPIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_int_inte'+contVPIact+'" name="apue_int_inte'+contVPIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="apue_int_tipo'+contVPIact+'" name="apue_int_tipo'+contVPIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajad">Arco Rebajad</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+      
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="apue_int_mat'+contVPIact+'" name="apue_int_mat'+contVPIact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+              
              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVPIntAct('+contVPIact+');"></i>'+
    '</a></div>';
       arrDibujoVPIn.push({scd_codigo:htmVPIntact,id: contVPIact});
        contVPIact++;
        }
        var cadenaVPInt='';
        var cont=1;
        for (var i=0; i<arrDibujoVPIn.length;i++)
        {
          cadenaVPInt=cadenaVPInt + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVPIn[i].scd_codigo;
          cont++;
        }
        $('#aVanosPuertasInteriores').html(cadenaVPInt);
       }

  function actualizarVanosPuertasInteriores(){
    var htmlVPIntact;
        htmlVPIntact='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_int_estado'+contVPIntactua+'" name="apue_int_estado'+contVPIntactua+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_int_inte'+contVPIntactua+'" name="apue_int_inte'+contVPIntactua+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="apue_int_tipo'+contVPIntactua+'" name="apue_int_tipo'+contVPIntactua+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajad">Arco Rebajad</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+             
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="apue_int_mat'+contVPIntactua+'" name="apue_int_mat'+contVPIntactua+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+                   
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVPIntAct('+contVPIntactua+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoVPIn.forEach(function (item, index, array) 
        {
          arrayVPIntact[cont].aarg_pue_int_estado = $("#apue_int_estado"+item.id+"").val();
          arrayVPIntact[cont].aarg_pue_int_inte = $("#apue_int_inte"+item.id+"").val();
          arrayVPIntact[cont].aarg_pue_int_tipo = $("#apue_int_tipo"+item.id+"").val();
          arrayVPIntact[cont].aarg_pue_int_mat = $("#apue_int_mat"+item.id+"").val();
          cont++;
        });
      
        arrayVPIntact.push({aarg_pue_int_estado:'N',aarg_pue_int_inte:'N',aarg_pue_int_tipo:'N',aarg_pue_int_mat:'N'});
        arrDibujoVPIn.push({scd_codigo:htmlVPIntact,id: contVPIntactua});
        contVPIntactua++;
        cont=1;
        for (var i=0; i<arrDibujoVPIn.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoVPIn[i].scd_codigo;
          cont++;
        }
        $('#aVanosPuertasInteriores').html(cadena); 
        var conto=0;
        arrDibujoVPIn.forEach(function (item, index, array) 
        { 
          $("#apue_int_estado"+item.id+"").val(arrayVPIntact[conto].aarg_pue_int_estado);
          $("#apue_int_inte"+item.id+"").val(arrayVPIntact[conto].aarg_pue_int_inte);
          $("#apue_int_tipo"+item.id+"").val(arrayVPIntact[conto].aarg_pue_int_tipo);
          $("#apue_int_mat"+item.id+"").val(arrayVPIntact[conto].aarg_pue_int_mat);
          conto=conto+1;
        });
      
    }


//----------------------------------
function eliminarVPIntAct($id){
        if(contVPIntactua>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoVPIn.forEach(function (item, index, array) 
                {
                    arrayVPIntact[cont].aarg_pue_int_estado = $("#apue_int_estado"+item.id+"").val();
                    arrayVPIntact[cont].aarg_pue_int_inte = $("#apue_int_inte"+item.id+"").val();
                    arrayVPIntact[cont].aarg_pue_int_tipo = $("#apue_int_tipo"+item.id+"").val();
                    arrayVPIntact[cont].aarg_pue_int_mat = $("#apue_int_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoVPIn.map(function(e) {return e.id;}).indexOf($id);
                ////console.log("poos",pos);
                arrayVPIntact.splice(pos,1);
                arrDibujoVPIn.splice(pos,1);
                ////console.log("arrCodigo_ptr_van_pue_int",arrCodigo_ptr_van_pue_int);
                
                cont=1;
                for (var i=0; i<arrDibujoVPIn.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVPIn[i].scd_codigo;
                    cont++;
                }
                
                $('#aVanosPuertasInteriores').html(cadena); 
                
                cont=0;
                arrDibujoVPIn.forEach(function (item, index, array) 
                {
                    $("#apue_int_estado"+item.id+"").val(arrayVPIntact[cont].aarg_pue_int_estado);
                    $("#apue_int_inte"+item.id+"").val(arrayVPIntact[cont].aarg_pue_int_inte);
                    $("#apue_int_tipo"+item.id+"").val(arrayVPIntact[cont].aarg_pue_int_tipo);
                    $("#apue_int_mat"+item.id+"").val(arrayVPIntact[cont].aarg_pue_int_mat);
                    
                    cont=cont+1;
                });
            });
        }
        ////console.log("arrCodigo_ptr_van_pue_intFinal",arrCodigo_ptr_van_pue_int);
    }




//-----fin grilla vanos puertas interiores------------------
//-----Inicio vanos puertas exteriores----------------------
  

var arrDibujoVPEact=[];
var aarrCodigo_ptr_van_pue_ext = [];
var contVPIntAct = 0;
function editarVanosPuertasExteriores(longitudGrilla){
  var contVPExac=1;
  arrDibujoVPEact=[];
  var htmVPEcact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmVPEcact='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_ext_estado'+contVPExac+'" name="apue_ext_estado'+contVPExac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_ext_inte'+contVPExac+'" name="apue_ext_inte'+contVPExac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="apue_ext_tipo'+contVPExac+'" name="apue_ext_tipo'+contVPExac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+         
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="apue_ext_mat'+contVPExac+'" name="apue_ext_mat'+contVPExac+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+                          
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVPEx('+contVPExac+');"></i>'+
    '</a></div>';
       arrDibujoVPEact.push({scd_codigo:htmVPEcact,id: contVPExac});
        contVPExac++;
        }
        var cadena = '';
        var cont=1; 
        for (var i=0; i<arrDibujoVPEact.length;i++)
        {
          cadena = cadena + '<div class="form-group col-md-3"><label class="control-label">' +cont+'. '+arrDibujoVPEact[i].scd_codigo;
          cont++;
        }
        $('#aVanosPuertasExteriores').html(cadena);
       }



function actualizarVanosPuertasExteriores(){
    var htmlVPExtAct;
        htmlVPExtAct='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_ext_estado'+contVPIntAct+'" name="apue_ext_estado'+contVPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_ext_inte'+contVPIntAct+'" name="apue_ext_inte'+contVPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="apue_ext_tipo'+contVPIntAct+'" name="apue_ext_tipo'+contVPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Arco Abocinado">Arco Abocinado</option>'+
              '<option value="Vano Adintelado">Vano Adintelado</option>'+
              '<option value="Arco Catenario">Arco Catenario</option>'+
              '<option value="Arco Ciego">Arco Ciego</option>'+
              '<option value="Arco de Medio Punto">Arco de Medio Punto</option>'+
              '<option value="Arco Escarzano">Arco Escarzano</option>'+
              '<option value="Arco Peraltado">Arco Peraltado</option>'+
              '<option value="Arco Rebajado">Arco Rebajado</option>'+
              '<option value="Arco de Herradura">Arco de Herradura</option>'+
              '<option value="Arco apuntado u ojival">Arco apuntado u ojival</option>'+
              '<option value="Arco de Cortina">Arco de Cortina</option>'+
              '<option value="Arco Polilobulado">Arco Polilobulado</option>'+
              '<option value="Arco Carpanel">Arco Carpanel</option>'+
              '<option value="Arco Conopial">Arco Conopial</option>'+
              '<option value="Vano angular">Vano angular</option>'+
              '<option value="Geminado con parteluz(ajimez)">Geminado con parteluz(ajimez)</option>'+
              '<option value="Arco mixtilíneo">Arco mixtilíneo</option>'+
              '<option value="Vano poligonal u ochavado">Vano poligonal u ochavado</option>'+         
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="apue_ext_mat'+contVPIntAct+'" name="apue_ext_mat'+contVPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Adobe">Adobe</option>'+
              '<option value="Piedra">Piedra</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Ladrillo">Ladrillo</option>'+
              '<option value="Hormigon Armado">Hormigon Armado</option>'+
              '<option value="Concreto">Concreto</option>'+
              '<option value="Metal">Metal</option>'+                     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarVPEx('+contVPIntAct+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoVPEact.forEach(function (item, index, array) 
        {
          aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_estado = $("#apue_ext_estado"+item.id+"").val();
          aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_inte = $("#apue_ext_inte"+item.id+"").val();
          aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_tipo = $("#apue_ext_tipo"+item.id+"").val();
          aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_mat = $("#apue_ext_mat"+item.id+"").val();
          cont++;
        });
      
        aarrCodigo_ptr_van_pue_ext.push({aarg_pue_ext_estado:'N',aarg_pue_ext_inte:'N',aarg_pue_ext_tipo:'N',aarg_pue_ext_mat:'N'});
        arrDibujoVPEact.push({scd_codigo:htmlVPExtAct,id: contVPIntAct});
        contVPIntAct++;
        cont=1;
        for (var i=0; i<arrDibujoVPEact.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">' + cont+'. '+arrDibujoVPEact[i].scd_codigo;
          cont++;
        }
        $('#aVanosPuertasExteriores').html(cadena); 
        var conto=0;
        arrDibujoVPEact.forEach(function (item, index, array) 
        { 
          $("#apue_ext_estado"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[conto].aarg_pue_ext_estado);
          $("#apue_ext_inte"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[conto].aarg_pue_ext_inte);
          $("#apue_ext_tipo"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[conto].aarg_pue_ext_tipo);
          $("#apue_ext_mat"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[conto].aarg_pue_ext_mat);
          conto=conto+1;
        });
       
    }
 
    function eliminarVPEx($id){

        if(contVPIntAct>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoVPEact.forEach(function (item, index, array) 
                {
                  aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_estado = $("#apue_ext_estado"+item.id+"").val();
                  aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_inte = $("#apue_ext_inte"+item.id+"").val();
                  aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_tipo = $("#apue_ext_tipo"+item.id+"").val();
                  aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_mat = $("#apue_ext_mat"+item.id+"").val();
                  cont=cont+1;
                });
                pos = arrDibujoVPEact.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigo_ptr_van_pue_ext.splice(pos,1);
                arrDibujoVPEact.splice(pos,1);   
                cont=1;
                for (var i=0; i<arrDibujoVPEact.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoVPEact[i].scd_codigo;
                    cont++;
                }
                
                $('#aVanosPuertasExteriores').html(cadena);
                
                cont=0;
                arrDibujoVPEact.forEach(function (item, index, array) 
                {
                 $("#apue_ext_estado"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_estado);
                $("#apue_ext_inte"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_inte);
                $("#apue_ext_tipo"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_tipo);
                $("#apue_ext_mat"+item.id+"").val(aarrCodigo_ptr_van_pue_ext[cont].aarg_pue_ext_mat);
                cont=cont+1;
                });
            });
        }
    }
 //---fin grilla vanos puertas exteriores-----------------
 //---Inicio grilla puertas interiores--------------------

var arrDibujoPIntAct=[];
var aarrCodigo_ptr_pue_int = [];
var contPIntAct = 0;
function editarPuertasInteriores(longitudGrilla){
  var contPIactu=1;
  arrDibujoPIntAct=[];
  var htmPInAc;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmPInAc='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_inte_estado'+contPIactu+'" name="apue_inte_estado'+contPIactu+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_inte_inte'+contPIactu+'" name="apue_inte_inte'+contPIactu+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="apue_inte_tipo'+contPIactu+'" name="apue_inte_tipo'+contPIactu+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+  
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="apue_inte_mat'+contPIactu+'" name="apue_inte_mat'+contPIactu+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPuertasInt('+contPIactu+');"></i>'+
    '</a></div>';
       arrDibujoPIntAct.push({scd_codigo:htmPInAc,id: contPIactu});
        contPIactu++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoPIntAct.length;i++)
        {
          cadena = cadena + '<div class="form-group col-md-3"><label class="control-label">' +cont+'. '+arrDibujoPIntAct[i].scd_codigo;
          cont++;
        }
        $('#aPuertasInteriores').html(cadena);
       }



    function actualizarPuertasInteriores(){
    var htmlPIntAct;
        htmlPIntAct='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_inte_estado'+contPIntAct+'" name="apue_inte_estado'+contPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_inte_inte'+contPIntAct+'" name="apue_inte_inte'+contPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="apue_inte_tipo'+contPIntAct+'" name="apue_inte_tipo'+contPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+  
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="apue_inte_mat'+contPIntAct+'" name="apue_inte_mat'+contPIntAct+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+     
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPuertasInt('+contPIntAct+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPIntAct.forEach(function (item, index, array) 
        {
          aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_estado = $("#apue_inte_estado"+item.id+"").val();
          aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_inte = $("#apue_inte_inte"+item.id+"").val();
          aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_tipo = $("#apue_inte_tipo"+item.id+"").val();
          aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_mat = $("#apue_inte_mat"+item.id+"").val();
          cont++;
        });
        aarrCodigo_ptr_pue_int.push({aarg_pue_inte_estado:'N', aarg_pue_inte_inte:'N', aarg_pue_inte_tipo:'N',aarg_pue_inte_mat:'N'});
        arrDibujoPIntAct.push({scd_codigo:htmlPIntAct,id: contPIntAct});
        contPIntAct++;
        cont=1;
        for (var i=0; i<arrDibujoPIntAct.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPIntAct[i].scd_codigo;
          cont++;
        }
        $('#aPuertasInteriores').html(cadena); 
        var conto=0;
        arrDibujoPIntAct.forEach(function (item, index, array) 
        { 
          $("#apue_inte_estado"+item.id+"").val(aarrCodigo_ptr_pue_int[conto].aarg_pue_inte_estado);
          $("#apue_inte_inte"+item.id+"").val(aarrCodigo_ptr_pue_int[conto].aarg_pue_inte_inte);
          $("#apue_inte_tipo"+item.id+"").val(aarrCodigo_ptr_pue_int[conto].aarg_pue_inte_tipo);
          $("#apue_inte_mat"+item.id+"").val(aarrCodigo_ptr_pue_int[conto].aarg_pue_inte_mat);
          conto=conto+1;
        });
        
    }


    function eliminarPuertasInt($id){
        if(contPIntAct>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoPIntAct.forEach(function (item, index, array) 
                {
                    aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_estado = $("#apue_inte_estado"+item.id+"").val();
                    aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_inte = $("#apue_inte_inte"+item.id+"").val();
                    aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_tipo = $("#apue_inte_tipo"+item.id+"").val();
                    aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_mat = $("#apue_inte_mat"+item.id+"").val();

                    cont=cont+1;
                });
                pos = arrDibujoPIntAct.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigo_ptr_pue_int.splice(pos,1);
                arrDibujoPIntAct.splice(pos,1);
                cont=1;
                for (var i=0; i<arrDibujoPIntAct.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPIntAct[i].scd_codigo;
                    cont++;
                }
                
                $('#aPuertasInteriores').html(cadena); 
                
                cont=0;
                arrDibujoPIntAct.forEach(function (item, index, array) 
                {
                    $("#apue_inte_estado"+item.id+"").val(aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_estado);
                    $("#apue_inte_inte"+item.id+"").val(aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_inte);
                    $("#apue_inte_tipo"+item.id+"").val(aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_tipo);
                    $("#apue_inte_mat"+item.id+"").val(aarrCodigo_ptr_pue_int[cont].aarg_pue_inte_mat);

                    cont=cont+1;
                });
            });
        }
    }
//------fin grilla puertas interiores---------------------
//-----Inicio grilla puertas exteriores-------------------
var arrDibujoPExact = [];
var arrcodPExtact =[];
var contPExtact = 0;
  function editarPuertasExteriores(longitudGrilla){
  var contPEact=1;
  arrDibujoPExact=[];
  var htmPExt;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmPExt='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_exte_estado'+contPEact+'" name="apue_exte_estado'+contPEact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_exte_inte'+contPEact+'" name="apue_exte_inte'+contPEact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
       '<select class="form-control" id="apue_exte_tipo'+contPEact+'" name="apue_exte_tipo'+contPEact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+       
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
     '<select class="form-control" id="apue_exte_mat'+contPEact+'" name="apue_exte_mat'+contPEact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+  
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPExtAct('+contPEact+');"></i>'+
    '</a></div>';
       arrDibujoPExact.push({scd_codigo:htmPExt,id: contPEact});
        contPEact++;
        }
        var cadenaPExt='';
        var cont=1;
        for (var i=0; i<arrDibujoPExact.length;i++)
        {
          cadenaPExt=cadenaPExt + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPExact[i].scd_codigo;
          cont++;
        }
        $('#aPuertasExteriores').html(cadenaPExt);
       }


function actualizarPuertasExteriores(){
    var htmlPExtact;
        htmlPExtact='Estado de Conservación:</label>'+
    '<select class="form-control" id="apue_exte_estado'+contPExtact+'" name="apue_exte_estado'+contPExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="apue_exte_inte'+contPExtact+'" name="apue_exte_inte'+contPExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
       '<select class="form-control" id="apue_exte_tipo'+contPExtact+'" name="apue_exte_tipo'+contPExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Contrapuerta">Contrapuerta</option>'+
              '<option value="Puerta con Contraventana">Puerta con Contraventana</option>'+
              '<option value="Puerta Postigo">Puerta Postigo</option>'+
              '<option value="Puerta corredera">Puerta corredera</option>'+
              '<option value="Puerta de Pivote">Puerta de Pivote</option>'+
              '<option value="Puerta Abatible de una Hoja">Puerta Abatible de una Hoja</option>'+
              '<option value="Puerta Abatible de doble Hoja">Puerta Abatible de doble Hoja</option>'+
              '<option value="Puerta vidriera">Puerta vidriera</option>'+
              '<option value="Puerta corrediza">Puerta corrediza</option>'+
              '<option value="Puerta acordeón">Puerta acordeón</option>'+
              '<option value="Puerta vaivén">Puerta vaivén</option>'+       
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
     '<select class="form-control" id="apue_exte_mat'+contPExtact+'" name="apue_exte_mat'+contPExtact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Madera">Madera</option>'+
              '<option value="Vidrio">Vidrio</option>'+
              '<option value="Metal">Metal</option>'+  
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPExtAct('+contCodigo_ptr_pue_ext+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibujoPExact.forEach(function (item, index, array) 
        {
          arrcodPExtact[cont].aarg_pue_exte_estado = $("#apue_exte_estado"+item.id+"").val();
          arrcodPExtact[cont].aarg_pue_exte_inte = $("#apue_exte_inte"+item.id+"").val();
          arrcodPExtact[cont].aarg_pue_exte_tipo = $("#apue_exte_tipo"+item.id+"").val();
          arrcodPExtact[cont].aarg_pue_exte_mat = $("#apue_exte_mat"+item.id+"").val();
          cont++;
        });
        arrcodPExtact.push({aarg_pue_exte_estado:'N', aarg_pue_exte_inte:'N', aarg_pue_exte_tipo:'N',aarg_pue_exte_mat:'N'});
        arrDibujoPExact.push({scd_codigo:htmlPExtact,id: contPExtact});
        contPExtact++;
        cont=1;
        for (var i=0; i<arrDibujoPExact.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibujoPExact[i].scd_codigo;
          cont++;
        }
        $('#aPuertasExteriores').html(cadena);
        var conto=0;
        arrDibujoPExact.forEach(function (item, index, array) 
        { 
          $("#apue_exte_estado"+item.id+"").val(arrcodPExtact[conto].aarg_pue_exte_estado);
          $("#apue_exte_inte"+item.id+"").val(arrcodPExtact[conto].aarg_pue_exte_inte);
          $("#apue_exte_tipo"+item.id+"").val(arrcodPExtact[conto].aarg_pue_exte_tipo);
          $("#apue_exte_mat"+item.id+"").val(arrcodPExtact[conto].aarg_pue_exte_mat);
          conto=conto+1;
        });
        
    }

    function eliminarPExtAct($id){
        if(contPExtact>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoPExact.forEach(function (item, index, array) 
                {
                    arrcodPExtact[cont].aarg_pue_exte_estado = $("#apue_exte_estado"+item.id+"").val();
                    arrcodPExtact[cont].aarg_pue_exte_inte = $("#apue_exte_inte"+item.id+"").val();
                    arrcodPExtact[cont].aarg_pue_exte_tipo = $("#apue_exte_tipo"+item.id+"").val();
                    arrcodPExtact[cont].aarg_pue_exte_mat = $("#apue_exte_mat"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoPExact.map(function(e) {return e.id;}).indexOf($id);
                arrcodPExtact.splice(pos,1);
                arrDibujoPExact.splice(pos,1); 
                cont=1;
                for (var i=0; i<arrDibujoPExact.length;i++)
                {
                    cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibujoPExact[i].scd_codigo;
                    cont++;
                }
                
                 $('#aPuertasExteriores').html(cadena);
                
                cont=0;
                arrDibujoPExact.forEach(function (item, index, array) 
                {
                      $("#apue_exte_estado"+item.id+"").val(arrcodPExtact[cont].aarg_pue_exte_estado);
                      $("#apue_exte_inte"+item.id+"").val(arrcodPExtact[cont].aarg_pue_exte_inte);
                      $("#apue_exte_tipo"+item.id+"").val(arrcodPExtact[cont].aarg_pue_exte_tipo);
                      $("#apue_exte_mat"+item.id+"").val(arrcodPExtact[cont].aarg_pue_exte_mat);
                    
                    cont=cont+1;
                });
            });
        }
    }

 //-----fin grilla puertas exteriores----------------------
 //----Inicio grilla rejas---------------------------------
 var contrejasact = 0;
 var arrDibRejas = [];
 var arrayrejasact = [];
 function editarRejas(longitudGrilla){
  var contRejas=1;
  arrDibRejas = [];
  var htmRejas;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmRejas='Estado de Conservación:</label>'+
        '<select class="form-control" id="areja_estado'+contRejas+'" name="areja_estado'+contRejas+'">'+
                  '<option value="N" selected="selected">--Seleccione--</option>'+
                  '<option value="Bueno">Bueno</option>'+
                  '<option value="Regular">Regular</option>'+
                  '<option value="Malo">Malo</option>'+              
        '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<label class="control-label">Integridad:</label>'+
        '<select class="form-control" id="areja_inte'+contRejas+'" name="areja_inte'+contRejas+'">'+
                  '<option value="N" selected="selected">--Seleccione--</option>'+
                  '<option value="Original">Original</option>'+
                  '<option value="Nuevo">Nuevo</option>'+
                  '<option value="Modificado">Modificado</option>'+              
        '</select>'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<label class="control-label">Tipo:</label>'+
        '<select class="form-control" id="areja_tipo'+contRejas+'" name="areja_tipo'+contRejas+'">'+
                  '<option value="N" selected="selected">--Seleccione--</option>'+
                  '<option value="Figuras Geométricos">Figuras Geométricos</option>'+
                  '<option value="Figuras Fitomarfas">Figuras Fitomarfas</option>'+ 
        '</select>'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<label class="control-label">Materiales:</label>'+
        '<select class="form-control" id="areja_mat'+contRejas+'" name="areja_mat'+contRejas+'">'+
                  '<option value="N" selected="selected">--Seleccione--</option>'+
                  '<option value="Aluminio">Aluminio</option>'+
                  '<option value="Hierro Fundido">Hierro Fundido</option>'+
                  '<option value="Hierro Forjado">Hierro Forjado</option>'+      
        '</select>'+
        '</div>'+
        '<div class="form-group col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminaRejasAct('+contRejas+');"></i>'+
        '</a></div>';
       arrDibRejas.push({scd_codigo:htmRejas,id: contRejas});
        contRejas++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibRejas.length;i++)
        {
          cadena = cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'. '+arrDibRejas[i].scd_codigo;
          cont++;
        }
        $('#apRejas').html(cadena);
       }

    function actualizarRejas(){
    var htmlrejasAct;
        htmlrejasAct='Estado de Conservación:</label>'+
    '<select class="form-control" id="areja_estado'+contrejasact+'" name="areja_estado'+contrejasact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Bueno">Bueno</option>'+
              '<option value="Regular">Regular</option>'+
              '<option value="Malo">Malo</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Integridad:</label>'+
    '<select class="form-control" id="areja_inte'+contrejasact+'" name="areja_inte'+contrejasact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+              
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-3">'+
    '<label class="control-label">Tipo:</label>'+
    '<select class="form-control" id="areja_tipo'+contrejasact+'" name="areja_tipo'+contrejasact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Figuras Geométricos">Figuras Geométricos</option>'+
              '<option value="Figuras Fitomarfas">Figuras Fitomarfas</option>'+ 
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<label class="control-label">Materiales:</label>'+
    '<select class="form-control" id="areja_mat'+contrejasact+'" name="areja_mat'+contrejasact+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Aluminio">Aluminio</option>'+
              '<option value="Hierro Fundido">Hierro Fundido</option>'+
              '<option value="Hierro Forjado">Hierro Forjado</option>'+      
    '</select>'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminaRejasAct('+contrejasact+');"></i>'+
    '</a></div>';
        var cadena='';
        var cont=0;
        arrDibRejas.forEach(function (item, index, array) 
        {
          arrayrejasact[cont].aarg_reja_estado = $("#areja_estado"+item.id+"").val();
          arrayrejasact[cont].aarg_reja_inte = $("#areja_inte"+item.id+"").val();
          arrayrejasact[cont].aarg_reja_tipo = $("#areja_tipo"+item.id+"").val();
          arrayrejasact[cont].aarg_reja_mat = $("#areja_mat"+item.id+"").val();
          cont++;
        });
        arrayrejasact.push({aarg_reja_estado:'N', aarg_reja_inte:'N', aarg_reja_tipo:'N', aarg_reja_mat:'N'});
        arrDibRejas.push({scd_codigo:htmlrejasAct,id: contrejasact});
               
        contrejasact++;
        cont=1;
        for (var i=0; i<arrDibRejas.length;i++)
        {
          cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+'.'+arrDibRejas[i].scd_codigo;
          cont++;
        }
        //////console.log("cadenaaaa",cadena);
        $('#apRejas').html(cadena); 
        var conto=0;
        arrDibRejas.forEach(function (item, index, array) 
        { 
          $("#areja_estado"+item.id+"").val(arrayrejasact[conto].aarg_reja_estado);
          $("#areja_inte"+item.id+"").val(arrayrejasact[conto].aarg_reja_inte);
          $("#areja_tipo"+item.id+"").val(arrayrejasact[conto].aarg_reja_tipo);
          $("#areja_mat"+item.id+"").val(arrayrejasact[conto].aarg_reja_mat);
          conto=conto+1;
        });
    }

 function eliminaRejasAct($id){
      if(contrejasact>2)
      {
          swal({title: "Esta seguro de eliminar?",
              text: "Se eliminará el registro seleccionado!",
              type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
              var cadena='';
              var cont=0;
              var pos=-1;
              arrDibRejas.forEach(function (item, index, array) 
              {
                  arrayrejasact[cont].aarg_reja_estado = $("#areja_estado"+item.id+"").val();
                  arrayrejasact[cont].aarg_reja_inte = $("#areja_inte"+item.id+"").val();
                  arrayrejasact[cont].aarg_reja_tipo = $("#areja_tipo"+item.id+"").val();
                  arrayrejasact[cont].aarg_reja_mat = $("#areja_mat"+item.id+"").val();
                  cont=cont+1;
              });
              pos = arrDibRejas.map(function(e) {return e.id;}).indexOf($id);
              arrayrejasact.splice(pos,1);
              arrDibRejas.splice(pos,1);
              cont=1;
              for (var i=0; i<arrDibRejas.length;i++)
              {
                  cadena=cadena + '<div class="form-group col-md-3"><label class="control-label">'+cont+arrDibRejas[i].scd_codigo;
                  cont++;
              }
              
              $('#apRejas').html(cadena);
              cont=0;
              arrDibRejas.forEach(function (item, index, array) 
              {
                  $("#areja_estado"+item.id+"").val(arrayrejasact[cont].aarg_reja_estado);
                  $("#areja_inte"+item.id+"").val(arrayrejasact[cont].aarg_reja_inte);
                  $("#areja_tipo"+item.id+"").val(arrayrejasact[cont].aarg_reja_tipo);
                  $("#areja_mat"+item.id+"").val(arrayrejasact[cont].aarg_reja_mat);
                  cont=cont+1;
              });
          });
      }
  }
 //-------fin grilla rejas----------------------
 //------Inicio grilla pinturas muros exteriores-----
var arrDibujoPMExact = [];
var arrayPMExtactualizar = [];
var contPMEact = 0;
function editarPinturasMurosExt(longitudGrilla){
  var contPMExtact = 1;
  arrDibujoPMExact = [];
  var htmPMExtact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmPMExtact='<div class="col-md-4"><br>'+
      '<select class="form-control" id="aestado'+contPMExtact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Bueno">Bueno</option>'+ 
      '<option value="Regular"> Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-3"><br>'+
      '<select class="form-control" id="aintegridad'+contPMExtact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Original">Original</option>'+ 
      '<option value="Nuevo"> Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2"><br>'+
      '<select class="form-control" id="atipo'+contPMExtact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Policromia">Policromia</option>'+ 
      '<option value="Bicromia"> Bicromia</option>'+
      '<option value="Monocromia">Monocromia</option>'+
      '<option value="Pintura Mural">Pintura Mural</option>'+
      '<option value="Esgrafiado">Esgrafiado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2"><br>'+
      '<select class="form-control" id="amaterial'+contPMExtact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Cal">Cal</option>'+ 
      '<option value="Latex"> Latex</option>'+
      '<option value="Ocre">Ocre</option>'+
      '<option value="Oleo">Oleo</option>'+
      '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="elimnarPMExAct('+contPMExtact+');"></i>'+
      '</a></div><br>';
       arrDibujoPMExact.push({scd_codigo:htmPMExtact,id: contPMExtact});
        contPMExtact++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoPMExact.length;i++)
        {
          cadena=cadena  + arrDibujoPMExact[i].scd_codigo;
          cont++;
        }
        $('#adivacaPinturasMurosExteriores').html(cadena);
       }


function actualizarPinturasMurosExteriores(){
      var htmlMurExte;
      htmlMurExte=
      '<div class="col-md-4"><br>'+
      '<select class="form-control" id="aestado'+contPMEact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Bueno">Bueno</option>'+ 
      '<option value="Regular"> Regular</option>'+
      '<option value="Malo">Malo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-3"><br>'+
      '<select class="form-control" id="aintegridad'+contPMEact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Original">Original</option>'+ 
      '<option value="Nuevo"> Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2"><br>'+
      '<select class="form-control" id="atipo'+contPMEact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Policromia">Policromia</option>'+ 
      '<option value="Bicromia"> Bicromia</option>'+
      '<option value="Monocromia">Monocromia</option>'+
      '<option value="Pintura Mural">Pintura Mural</option>'+
      '<option value="Esgrafiado">Esgrafiado</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2"><br>'+
      '<select class="form-control" id="amaterial'+contPMEact+'" >'+
      '<option value="N" selected="selected"> Seleccionar...</option>'+
      '<option value="Cal">Cal</option>'+ 
      '<option value="Latex"> Latex</option>'+
      '<option value="Ocre">Ocre</option>'+
      '<option value="Oleo">Oleo</option>'+
      '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="elimnarPMExAct('+contPMEact+');"></i>'+
      '</a></div><br>';
      var cadena='';
      var cont=0;
      arrDibujoPMExact.forEach(function (item, index, array) 
      {
        arrayPMExtactualizar[cont].aarg_estado = $("#aestado"+item.id+"").val();
        arrayPMExtactualizar[cont].aarg_integridad = $("#aintegridad"+item.id+"").val();
        arrayPMExtactualizar[cont].aarg_tipo = $("#atipo"+item.id+"").val();
        arrayPMExtactualizar[cont].aarg_material = $("#amaterial"+item.id+"").val();

        cont++;
      });
      arrayPMExtactualizar.push({aarg_estado:'N',aarg_integridad:'N',aarg_tipo:'N',aarg_material:'N'});
      arrDibujoPMExact.push({scd_codigo:htmlMurExte,id: contPMEact});
      contPMEact++;
      cont=1;
      for (var i=0; i<arrDibujoPMExact.length;i++)
      {
        cadena=cadena + arrDibujoPMExact[i].scd_codigo;
        cont++;
      }
      $('#adivacaPinturasMurosExteriores').html(cadena); 
      var conto=0;
      arrDibujoPMExact.forEach(function (item, index, array) 
      { 
        $("#aestado"+item.id+"").val(arrayPMExtactualizar[conto].aarg_estado);
        $("#aintegridad"+item.id+"").val(arrayPMExtactualizar[conto].aarg_integridad);
        $("#atipo"+item.id+"").val(arrayPMExtactualizar[conto].aarg_tipo);
        $("#amaterial"+item.id+"").val(arrayPMExtactualizar[conto].aarg_material);
        conto=conto+1;
      });
    }

function elimnarPMExAct($id){
      if(contPMEact>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el registro seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadena='';
          var cont=0;
          var pos=-1;
          arrDibujoPMExact.forEach(function (item, index, array) 
          {
            arrayPMExtactualizar[cont].aarg_estado=$("#aestado"+item.id+"").val();
            arrayPMExtactualizar[cont].aarg_integridad=$("#aintegridad"+item.id+"").val();
            arrayPMExtactualizar[cont].aarg_tipo=$("#atipo"+item.id+"").val();
            arrayPMExtactualizar[cont].aarg_material=$("#amaterial"+item.id+"").val();
            cont=cont+1;
          });
          pos = arrDibujoPMExact.map(function(e) {return e.id;}).indexOf($id);
          ////console.log("poos",pos);
          arrayPMExtactualizar.splice(pos,1);
          arrDibujoPMExact.splice(pos,1);
          cont=1;
          for (var i=0; i<arrDibujoPMExact.length;i++)
          {
            cadena=cadena + arrDibujoPMExact[i].scd_codigo;
            cont++;
          }
          
          $('#adivacaPinturasMurosExteriores').html(cadena);
          
          cont=0;
          arrDibujoPMExact.forEach(function (item, index, array) 
          {
            $("#aestado"+item.id+"").val(arrayPMExtactualizar[cont].aarg_estado);
            $("#aintegridad"+item.id+"").val(arrayPMExtactualizar[cont].aarg_integridad);
            $("#atipo"+item.id+"").val(arrayPMExtactualizar[cont].aarg_tipo);
            $("#amaterial"+item.id+"").val(arrayPMExtactualizar[cont].aarg_material);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
        });
      }
    }

//-----fin grilla pinturas muros exteriores---------------
//----Inicio grilla pinturas muros interiores-------------

var arrayPMIntact = [];
var arrDibujoPMInact = [];
var contPMIactua = 0;
function editarPinturasMurosInt(longitudGrilla){
  var contPMIntact = 1;
  arrDibujoPMInact = [];
  var htmPMIntact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmPMIntact='<div class="col-md-4"><br>'+
  '<select class="form-control" id="aestadoInt'+contPMIntact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="aintegridadInt'+contPMIntact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="atipoInt'+contPMIntact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Policromia">Policromia</option>'+ 
  '<option value="Bicromia"> Bicromia</option>'+
  '<option value="Monocromia">Monocromia</option>'+
  '<option value="Pintura Mural">Pintura Mural</option>'+
  '<option value="Esgrafiado">Esgrafiado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="amaterialInt'+contPMIntact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Cal">Cal</option>'+ 
  '<option value="Latex"> Latex</option>'+
  '<option value="Ocre">Ocre</option>'+
  '<option value="Oleo">Oleo</option>'+
  '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1"><br>'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPinMuroInt('+contPMIntact+');"></i>'+
  '</a></div><br>';
       arrDibujoPMInact.push({scd_codigo:htmPMIntact,id: contPMIntact});
        contPMIntact++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoPMInact.length;i++)
        {
          cadena=cadena + arrDibujoPMInact[i].scd_codigo;
          cont++;
        }
        $('#acaPinturasMurosInterioresA').html(cadena);
       }

//---------------------------------------
function actualizarPinturasMurosInteriores(){
  var htmlPMIntactu;
  htmlPMIntactu=
  '<div class="col-md-4"><br>'+
  '<select class="form-control" id="aestadoInt'+contPMIactua+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="aintegridadInt'+contPMIactua+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="atipoInt'+contPMIactua+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Policromia">Policromia</option>'+ 
  '<option value="Bicromia"> Bicromia</option>'+
  '<option value="Monocromia">Monocromia</option>'+
  '<option value="Pintura Mural">Pintura Mural</option>'+
  '<option value="Esgrafiado">Esgrafiado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="amaterialInt'+contPMIactua+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Cal">Cal</option>'+ 
  '<option value="Latex"> Latex</option>'+
  '<option value="Ocre">Ocre</option>'+
  '<option value="Oleo">Oleo</option>'+
  '<option value="Pigmentos Naturales">Pigmentos Naturales</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPinMuroInt('+contPMIactua+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  arrDibujoPMInact.forEach(function (item, index, array) 
  {
    arrayPMIntact[cont].aarg_estadoInt = $("#aestadoInt"+item.id+"").val();
    arrayPMIntact[cont].aarg_integridadInt = $("#aintegridadInt"+item.id+"").val();
    arrayPMIntact[cont].aarg_tipoInt = $("#atipoInt"+item.id+"").val();
    arrayPMIntact[cont].aarg_materialInt = $("#amaterialInt"+item.id+"").val();

    cont++;
  });
  arrayPMIntact.push({aarg_estadoInt:'N',aarg_integridadInt:'N',aarg_tipoInt:'N',aarg_materialInt:'N'});
  arrDibujoPMInact.push({scd_codigo:htmlPMIntactu,id: contPMIactua});
  contPMIactua++;
  cont=1;
  for (var i=0; i<arrDibujoPMInact.length;i++)
  {
    cadena=cadena + arrDibujoPMInact[i].scd_codigo;
    cont++;
  }
  $('#acaPinturasMurosInterioresA').html(cadena); 
  var conto=0;
  arrDibujoPMInact.forEach(function (item, index, array) 
  { 
    $("#aestadoInt"+item.id+"").val(arrayPMIntact[conto].aarg_estadoInt);
    $("#aintegridadInt"+item.id+"").val(arrayPMIntact[conto].aarg_integridadInt);
    $("#atipoInt"+item.id+"").val(arrayPMIntact[conto].aarg_tipoInt);
    $("#amaterialInt"+item.id+"").val(arrayPMIntact[conto].aarg_materialInt);
    conto=conto+1;
  });
}


//------------
function eliminarPinMuroInt($id){
  if(contPMIactua>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el registro seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      arrDibujoPMInact.forEach(function (item, index, array) 
      {
        arrayPMIntact[cont].aarg_estadoInt=$("#aestadoInt"+item.id+"").val();
        arrayPMIntact[cont].aarg_integridadInt=$("#aintegridadInt"+item.id+"").val();
        arrayPMIntact[cont].aarg_tipoInt=$("#atipoInt"+item.id+"").val();
        arrayPMIntact[cont].aarg_materialInt=$("#amaterialInt"+item.id+"").val();
        cont=cont+1;
      });
      pos = arrDibujoPMInact.map(function(e) {return e.id;}).indexOf($id);
      ////console.log("poos",pos);
      arrayPMIntact.splice(pos,1);
      arrDibujoPMInact.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoPMInact.length;i++)
      {
        cadena=cadena + arrDibujoPMInact[i].scd_codigo;
        cont++;
      }

      $('#acaPinturasMurosInterioresA').html(cadena);
      cont=0;
      arrDibujoPMInact.forEach(function (item, index, array) 
      {
        $("#aestadoInt"+item.id+"").val(arrayPMIntact[cont].aarg_estadoInt);
        $("#aintegridadInt"+item.id+"").val(arrayPMIntact[cont].aarg_integridadInt);
        $("#atipoInt"+item.id+"").val(arrayPMIntact[cont].aarg_tipoInt);
        $("#amaterialInt"+item.id+"").val(arrayPMIntact[cont].aarg_materialInt);
              //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
              cont=cont+1;
            });
    });
  }
}
//-----fin grilla pinturas muros interiores----------------

//-----Inicio grilla acabados muros exteriores-------------
var aaca_extCodigo = [];
var arrDibujoAMExAct = [];
var contAMEact = 0;
function editarAcabadosMurosExt(longitudGrilla){
  var contAMExact = 1;
  arrDibujoAMExAct = [];
  var htmAMEact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmAMEact='<div class="col-md-4"><br>'+
  '<select class="form-control" id="aestadoAcaExt'+contAMExact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="aintegridadAcaExt'+contAMExact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="atipoAcaExt'+contAMExact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="amaterialAcaExt'+contAMExact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarAME('+contAMExact+');"></i>'+
  '</a></div><br>';
       arrDibujoAMExAct.push({scd_codigo:htmAMEact,id: contAMExact});
        contAMExact++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoAMExAct.length;i++)
        {
          cadena=cadena + arrDibujoAMExAct[i].scd_codigo;
          cont++;
        }
        $('#divacaAcabadosMurosExterioresA').html(cadena);
       }

function actualizarAcabadosMurosExteriores(){
  var htmlAcaExtAc;
  htmlAcaExtAc=
  '<div class="col-md-4"><br>'+
  '<select class="form-control" id="aestadoAcaExt'+contAMEact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="aintegridadAcaExt'+contAMEact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="atipoAcaExt'+contAMEact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="amaterialAcaExt'+contAMEact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarAME('+contAMEact+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  arrDibujoAMExAct.forEach(function (item, index, array) 
  {
    aaca_extCodigo[cont].aarg_estadoAcaExt = $("#aestadoAcaExt"+item.id+"").val();
    aaca_extCodigo[cont].aarg_integridadAcaExt = $("#aintegridadAcaExt"+item.id+"").val();
    aaca_extCodigo[cont].aarg_tipoAcaExt = $("#atipoAcaExt"+item.id+"").val();
    aaca_extCodigo[cont].aarg_materialAcaExt = $("#amaterialAcaExt"+item.id+"").val();
    cont++;
  });
  aaca_extCodigo.push({aarg_estadoAcaExt:'N',aarg_integridadAcaExt:'N',aarg_tipoAcaExt:'N',aarg_materialAcaExt:'N'});
  arrDibujoAMExAct.push({scd_codigo:htmlAcaExtAc,id: contAMEact});
  contAMEact++;
  cont=1;
  for (var i=0; i<arrDibujoAMExAct.length;i++)
  {
    cadena=cadena + arrDibujoAMExAct[i].scd_codigo;
    cont++;
  }
  $('#divacaAcabadosMurosExterioresA').html(cadena); 
  var conto=0;
  arrDibujoAMExAct.forEach(function (item, index, array) 
  { 
    $("#aestadoAcaExt"+item.id+"").val(aaca_extCodigo[conto].aarg_estadoAcaExt);
    $("#aintegridadAcaExt"+item.id+"").val(aaca_extCodigo[conto].aarg_integridadAcaExt);
    $("#atipoAcaExt"+item.id+"").val(aaca_extCodigo[conto].aarg_tipoAcaExt);
    $("#amaterialAcaExt"+item.id+"").val(aaca_extCodigo[conto].aarg_materialAcaExt);
    conto=conto+1;
  });
}


function eliminarAME($id){
  if(contAMEact>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el registro seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      arrDibujoAMExAct.forEach(function (item, index, array) 
      {
        aaca_extCodigo[cont].aarg_estadoAcaExt=$("#aestadoAcaExt"+item.id+"").val();
        aaca_extCodigo[cont].aarg_integridadAcaExt=$("#aintegridadAcaExt"+item.id+"").val();
        aaca_extCodigo[cont].aarg_tipoAcaExt=$("#atipoAcaExt"+item.id+"").val();
        aaca_extCodigo[cont].aarg_materialAcaExt=$("#amaterialAcaExt"+item.id+"").val();
        cont=cont+1;
      });
      pos = arrDibujoAMExAct.map(function(e) {return e.id;}).indexOf($id);
      ////console.log("poos",pos);
      aaca_extCodigo.splice(pos,1);
      arrDibujoAMExAct.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoAMExAct.length;i++)
      {
        cadena=cadena + arrDibujoAMExAct[i].scd_codigo;
        cont++;
      }

      $('#divacaAcabadosMurosExterioresA').html(cadena);
      cont=0;
      arrDibujoAMExAct.forEach(function (item, index, array) 
      {
        $("#aestadoAcaExt"+item.id+"").val(aaca_extCodigo[cont].aarg_estadoAcaExt);
        $("#aintegridadAcaExt"+item.id+"").val(aaca_extCodigo[cont].aarg_integridadAcaExt);
        $("#atipoAcaExt"+item.id+"").val(aaca_extCodigo[cont].aarg_tipoAcaExt);
        $("#amaterialAcaExt"+item.id+"").val(aaca_extCodigo[cont].aarg_materialAcaExt);
        //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
          cont=cont+1;
        });
    });
  }
}
//---gfin grilla acabados muros exteriores----------------------
//----Inicio grilla acabados muros interiores------------------- 
var arrDibujoAMIact = [];
var aaca_intCodigo = [];
var contAMIntAct = 0;
function editarAcabadosMurosInt(longitudGrilla){
  var contAMIact = 1;
  arrDibujoAMIact = [];
  var htmAMEact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmAMEact='<div class="col-md-4"><br>'+
  '<select class="form-control" id="aestadoAcaInt'+contAMIact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="aintegridadAcaInt'+contAMIact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="atipoAcaInt'+contAMIact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="amaterialAcaInt'+contAMIact+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarAMI('+contAMIact+');"></i>'+
  '</a></div><br>';
       arrDibujoAMIact.push({scd_codigo:htmAMEact,id: contAMIact});
        contAMIact++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoAMIact.length;i++)
        {
          cadena=cadena + arrDibujoAMIact[i].scd_codigo;
          cont++;
        }
        $('#divacaAcabadosMurosInterioresA').html(cadena);
       }

function actualizarAcabadosMurosInteriores(){
  var htmlAMIntAct;
  htmlAMIntAct=
  '<div class="col-md-4"><br>'+
  '<select class="form-control" id="aestadoAcaInt'+contAMIntAct+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Bueno">Bueno</option>'+ 
  '<option value="Regular"> Regular</option>'+
  '<option value="Malo">Malo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-3"><br>'+
  '<select class="form-control" id="aintegridadAcaInt'+contAMIntAct+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Original">Original</option>'+ 
  '<option value="Nuevo"> Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="atipoAcaInt'+contAMIntAct+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Revoque Grueso">Revoque Grueso</option>'+ 
  '<option value="Revoque Fino"> Revoque Fino</option>'+
  '<option value="Material Visto">Material Visto</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-2"><br>'+
  '<select class="form-control" id="amaterialAcaInt'+contAMIntAct+'" >'+
  '<option value="N" selected="selected"> Seleccionar...</option>'+
  '<option value="Piedra">Piedra</option>'+ 
  '<option value="Madera"> Madera</option>'+
  '<option value="Ladrillo">Ladrillo</option>'+
  '<option value="Cerámica">Cerámica</option>'+
  '<option value="Marmol">Marmol</option>'+
  '<option value="Azulejo">Azulejo</option>'+
  '<option value="Cal y Arena">Cal y Arena</option>'+
  '<option value="Barro y Paja">Barro y Paja</option>'+
  '<option value="Cemento y Arena">Cemento y Arena</option>'+
  '<option value="Yeso">Yeso</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarAMI('+contAMIntAct+');"></i>'+
  '</a></div><br>';
  var cadena='';
  var cont=0;
  arrDibujoAMIact.forEach(function (item, index, array) 
  {
    aaca_intCodigo[cont].aarg_estadoAcaInt = $("#aestadoAcaInt"+item.id+"").val();
    aaca_intCodigo[cont].aarg_integridadAcaInt = $("#aintegridadAcaInt"+item.id+"").val();
    aaca_intCodigo[cont].aarg_tipoAcaInt = $("#atipoAcaInt"+item.id+"").val();
    aaca_intCodigo[cont].aarg_materialAcaInt = $("#amaterialAcaInt"+item.id+"").val();

    cont++;
  });
  aaca_intCodigo.push({aarg_estadoAcaInt:'N',aarg_integridadAcaInt:'N',aarg_tipoAcaInt:'N',aarg_materialAcaInt:'N'});
  arrDibujoAMIact.push({scd_codigo:htmlAMIntAct,id: contAMIntAct});
  contAMIntAct++;
  cont=1;
  for (var i=0; i<arrDibujoAMIact.length;i++)
  {
    cadena=cadena + arrDibujoAMIact[i].scd_codigo;
    cont++;
  }
  $('#divacaAcabadosMurosInterioresA').html(cadena); 
  var conto=0;
  arrDibujoAMIact.forEach(function (item, index, array) 
  { 
    $("#aestadoAcaInt"+item.id+"").val(aaca_intCodigo[conto].aarg_estadoAcaInt);
    $("#aintegridadAcaInt"+item.id+"").val(aaca_intCodigo[conto].aarg_integridadAcaInt);
    $("#atipoAcaInt"+item.id+"").val(aaca_intCodigo[conto].aarg_tipoAcaInt);
    $("#amaterialAcaInt"+item.id+"").val(aaca_intCodigo[conto].aarg_materialAcaInt);
    conto=conto+1;
  });
}


function eliminarAMI($id){
  if(contAMIntAct>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el registro seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena='';
      var cont=0;
      var pos=-1;
      arrDibujoAMIact.forEach(function (item, index, array) 
      {
        aaca_intCodigo[cont].aarg_estadoAcaInt=$("#aestadoAcaInt"+item.id+"").val();
        aaca_intCodigo[cont].aarg_integridadAcaInt=$("#aintegridadAcaInt"+item.id+"").val();
        aaca_intCodigo[cont].aarg_tipoAcaInt=$("#atipoAcaInt"+item.id+"").val();
        aaca_intCodigo[cont].aarg_materialAcaInt=$("#amaterialAcaInt"+item.id+"").val();
        cont=cont+1;
      });
      pos = arrDibujoAMIact.map(function(e) {return e.id;}).indexOf($id);
      ////console.log("poos",pos);
      aaca_intCodigo.splice(pos,1);
      arrDibujoAMIact.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoAMIact.length;i++)
      {
        cadena=cadena + arrDibujoAMIact[i].scd_codigo;
        cont++;
      }

      $('#divacaAcabadosMurosInterioresA').html(cadena);
      cont=0;
      arrDibujoAMIact.forEach(function (item, index, array) 
      {
        $("#aestadoAcaInt"+item.id+"").val(aaca_intCodigo[cont].aarg_estadoAcaInt);
        $("#aintegridadAcaInt"+item.id+"").val(aaca_intCodigo[cont].aarg_integridadAcaInt);
        $("#atipoAcaInt"+item.id+"").val(aaca_intCodigo[cont].aarg_tipoAcaInt);
        $("#amaterialAcaInt"+item.id+"").val(aaca_intCodigo[cont].aarg_materialAcaInt);
          //document.getElementById("tipoarg"+item.id+"").value(arrCodigo[cont].arg_tipo);
          cont=cont+1;
        });
    });
  }
}
 //---fin grilla acabados muros interiores--------------------
 //----Inicio grilla elementos artisticos compositivos-------
var arrDibujoECAact=[];
var aarrCodigoAR = [];
var contECAactual = 0;
function editarElementosComArt(longitudGrilla){
  var contECAact=1;
  arrDibujoECAact=[];
  var htmECAac;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmECAac='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+       
                    '<select class="form-control" id="aartisticosC1'+contECAact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+               
                    '<select class="form-control" id="aartisticosC2'+contECAact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+         
                    '<select class="form-control" id="aartisticosC3'+contECAact+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="Pintura en cielo(plafon)">Pintura en cielo(plafon)</option>'+
                        '<option value="pinturas murales">Pinturas murales</option>'+
                        '<option value="moldura de cielos">Moldura de cielos</option>'+
                        '<option value="Enchapes de ceramica vidriada">Enchapes de ceramica vidriada</option>'+
                        '<option value="chapas">Chapas</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarECA('+contECAact+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
       arrDibujoECAact.push({scd_codigo:htmECAac,id: contECAact});
        contECAact++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoECAact.length;i++)
        {
          cadena=cadena +arrDibujoECAact[i].scd_codigo;
          cont++;
        }
        $('#agrillaArtisiticoC').html(cadena);
       }

function actualizarEAC(){
    var htmlECAactual;
        htmlECAactual='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+           
                    '<select class="form-control" id="aartisticosC1'+contECAactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+               
                    '<select class="form-control" id="aartisticosC2'+contECAactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+         
                    '<select class="form-control" id="aartisticosC3'+contECAactual+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="Pintura en cielo(plafon)">Pintura en cielo(plafon)</option>'+
                        '<option value="pinturas murales">Pinturas murales</option>'+
                        '<option value="moldura de cielos">Moldura de cielos</option>'+
                        '<option value="Enchapes de ceramica vidriada">Enchapes de ceramica vidriada</option>'+
                        '<option value="chapas">Chapas</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarECA('+contECAactual+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadena='';
        var cont=0;
        arrDibujoECAact.forEach(function (item, index, array) 
        {
          aarrCodigoAR[cont].aartisticosC1 = $("#aartisticosC1"+item.id+"").val();
          aarrCodigoAR[cont].aartisticosC2 = $("#aartisticosC2"+item.id+"").val();
          aarrCodigoAR[cont].aartisticosC3 = $("#aartisticosC3"+item.id+"").val();
          cont++;
        });
        aarrCodigoAR.push({aartisticosC1:'N', aartisticosC2:'N',aartisticosC3:'N'});
        arrDibujoECAact.push({scd_codigo:htmlECAactual,id: contECAactual});
        contECAactual++;
        cont=1;
        for (var i=0; i<arrDibujoECAact.length;i++)
        {
          cadena=cadena + arrDibujoECAact[i].scd_codigo;
          cont++;
        }       
        $('#agrillaArtisiticoC').html(cadena); 
        var conto=0;
        arrDibujoECAact.forEach(function (item, index, array) 
        { 
          $("#aartisticosC1"+item.id+"").val(aarrCodigoAR[conto].aartisticosC1);
          $("#aartisticosC2"+item.id+"").val(aarrCodigoAR[conto].aartisticosC2);
          $("#aartisticosC3"+item.id+"").val(aarrCodigoAR[conto].aartisticosC3);
          conto=conto+1;
        });
    }


    function eliminarECA($id){
        if(contECAactual>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoECAact.forEach(function (item, index, array) 
                {
                    aarrCodigoAR[cont].aartisticosC1 = $("#aartisticosC1"+item.id+"").val();
                    aarrCodigoAR[cont].aartisticosC2 = $("#aartisticosC2"+item.id+"").val();
                    aarrCodigoAR[cont].aartisticosC3 = $("#aartisticosC3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoECAact.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoAR.splice(pos,1);
                arrDibujoECAact.splice(pos,1);
             
                cont=1;
                for (var i=0; i<arrDibujoECAact.length;i++)
                {
                    cadena=cadena +arrDibujoECAact[i].scd_codigo;
                    cont++;
                }                
                $('#agrillaArtisiticoC').html(cadena);
                
                cont=0;
                arrDibujoECAact.forEach(function (item, index, array) 
                {
                    $("#aartisticosC1"+item.id+"").val(aarrCodigoAR[cont].aartisticosC1);
                    $("#aartisticosC2"+item.id+"").val(aarrCodigoAR[cont].aartisticosC2);
                    $("#aartisticosC3"+item.id+"").val(aarrCodigoAR[cont].aartisticosC3);
                    cont=cont+1;
                });
            });
        }
    }

//------fin grilla elementos artisiticos compositivos--------------
//------Inicio grilla elementos ornamnetales-----------------------
  var arrDibujoEOact=[];
  var aarrCodigoOR = [];
  var contEOactual = 0;
  function editarElementosOrnamentales(longitudGrilla){
  var contEOact=1;
  arrDibujoEOact=[];
  var htmEOact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmEOact='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+         
                    '<select class="form-control" id="aornamentales1'+contEOact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+                 
                    '<select class="form-control" id="aornamentales2'+contEOact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+          
                    '<select class="form-control" id="aornamentales3'+contEOact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alero con canales vistos">Alero con canales vistos</option>'+
                        '<option value="alero encajonado">Alejo encajonado</option>'+
                        '<option value="alfiz">Alfiz</option>'+
                        '<option value="avitolado">Avitolado</option>'+
                        '<option value="alto relieve">Alto relieve</option>'+   
                        '<option value="hojas de acanto">Hojas de acanto</option>'+
                        '<option value="adaraja">Adaraja</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="alfeizar">Alfeizar</option>'+
                        '<option value="almohadillado">Almohadillado</option>'+  
                        '<option value="balustre">Balustre</option>'+
                        '<option value=canesillos">Canesillos</option>'+
                        '<option value="cancel">cancel</option>'+
                        '<option value="cartela">Cartela</option>'+
                        '<option value="columna adosada">Columna adosada</option>'+   
                        '<option value="columna esquinera">Columna esquinera</option>'+
                        '<option value="columna exenta">Columna exenta</option>'+
                        '<option value="corniza">Corniza</option>'+
                        '<option value="denticulos">Denticulos</option>'+                                
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarEO('+contEOact+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
       arrDibujoEOact.push({scd_codigo:htmEOact,id: contEOact});
        contEOact++;
        }
        var cadena = '';
        var cont = 1;
        for (var i=0; i<arrDibujoEOact.length;i++)
        {
          cadena = cadena + arrDibujoEOact[i].scd_codigo;
          cont++;
        }
        $('#agrillaOrnamentalesA').html(cadena);
       }


    function actualizarEOr(){
    var htmlORactual;
        htmlORactual='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+       
                    '<select class="form-control" id="aornamentales1'+contEOactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+                
                    '<select class="form-control" id="aornamentales2'+contEOactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+          
                    '<select class="form-control" id="aornamentales3'+contEOactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alero con canales vistos">Alero con canales vistos</option>'+
                        '<option value="alero encajonado">Alejo encajonado</option>'+
                        '<option value="alfiz">Alfiz</option>'+
                        '<option value="avitolado">Avitolado</option>'+
                        '<option value="alto relieve">Alto relieve</option>'+   
                        '<option value="hojas de acanto">Hojas de acanto</option>'+
                        '<option value="adaraja">Adaraja</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="alfeizar">Alfeizar</option>'+
                        '<option value="almohadillado">Almohadillado</option>'+  
                        '<option value="balustre">Balustre</option>'+
                        '<option value=canesillos">Canesillos</option>'+
                        '<option value="cancel">cancel</option>'+
                        '<option value="cartela">Cartela</option>'+
                        '<option value="columna adosada">Columna adosada</option>'+   
                        '<option value="columna esquinera">Columna esquinera</option>'+
                        '<option value="columna exenta">Columna exenta</option>'+
                        '<option value="corniza">Corniza</option>'+
                        '<option value="denticulos">Denticulos</option>'+                                
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarEO('+contEOactual+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadena='';
        var cont=0;
        arrDibujoEOact.forEach(function (item, index, array) 
        {
          aarrCodigoOR[cont].aornamentales1 = $("#aornamentales1"+item.id+"").val();
          aarrCodigoOR[cont].aornamentales2 = $("#aornamentales2"+item.id+"").val();
          aarrCodigoOR[cont].aornamentales3 = $("#aornamentales3"+item.id+"").val();
          cont++;
        });
        aarrCodigoOR.push({aornamentales1:'N', aornamentales2:'N',aornamentales3:'N'});
        arrDibujoEOact.push({scd_codigo:htmlORactual,id: contEOactual});
        contEOactual++;
        cont=1;
        for (var i=0; i<arrDibujoEOact.length;i++)
        {
          cadena=cadena + arrDibujoEOact[i].scd_codigo;
          cont++;
        }
        $('#agrillaOrnamentalesA').html(cadena); 
        var conto=0;
        arrDibujoEOact.forEach(function (item, index, array) 
        { 
          $("#aornamentales1"+item.id+"").val(aarrCodigoOR[conto].aornamentales1);
          $("#aornamentales2"+item.id+"").val(aarrCodigoOR[conto].aornamentales2);
          $("#aornamentales3"+item.id+"").val(aarrCodigoOR[conto].aornamentales3);
          conto=conto+1;
        });
     
    }

    function eliminarEO($id){
        if(contEOactual>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoEOact.forEach(function (item, index, array) 
                {
                    aarrCodigoOR[cont].aornamentales1 = $("#aornamentales1"+item.id+"").val();
                    aarrCodigoOR[cont].aornamentales2 = $("#aornamentales2"+item.id+"").val();
                    aarrCodigoOR[cont].aornamentales3 = $("#aornamentales3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoEOact.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoOR.splice(pos,1);
                arrDibujoEOact.splice(pos,1);
                cont=1;
                for (var i=0; i<arrDibujoEOact.length;i++)
                {
                    cadena=cadena +arrDibujoEOact[i].scd_codigo;
                    cont++;
                }                
                $('#agrillaOrnamentalesA').html(cadena);
                cont=0;
                arrDibujoEOact.forEach(function (item, index, array) 
                {
                    $("#aornamentales1"+item.id+"").val(aarrCodigoOR[cont].aornamentales1);
                    $("#aornamentales2"+item.id+"").val(aarrCodigoOR[cont].aornamentales2);
                    $("#aornamentales3"+item.id+"").val(aarrCodigoOR[cont].aornamentales3);
                    cont=cont+1;
                });
            });
        }
    }
  //---fin grilla elementos oramentales-------------------
  //----Inicio grilla elementos tipologicos compositivos--
 var arrDibujoETComAct = [];
 var aarrCodigoTIC = [];
 var contETCactual = 0;
  function editarElementosTipologicosComp(longitudGrilla){
  var contETCact=1;
  arrDibujoETComAct = [];
  var htmETCact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmETCact='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+         
                    '<select class="form-control" id="atipologicos1'+contETCact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+             
                    '<select class="form-control" id="atipologicos2'+contETCact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+          
                    '<select class="form-control" id="atipologicos3'+contETCact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="atrio">Atrio</option>'+
                        '<option value="capilla">Capilla</option>'+
                        '<option value="chifon">Chifon</option>'+
                        '<option value="claustro">Claustro</option>'+   
                        '<option value="corredor">Corredor</option>'+
                        '<option value="corredor ardiente">Corredor ardiente</option>'+
                        '<option value="crujias">Crujias</option>'+
                        '<option value="enfarolado">Enfarolado</option>'+
                        '<option value="estacionamiento">Estacionamiento</option>'+
                        '<option value="galeria">Galeria</option>'+
                        '<option value="jardin interior">Jardin interior</option>'+
                        '<option value="hall">Hall</option>'+
                        '<option value="nave unica">Nave unica</option>'+
                        '<option value="naves">Naves</option>'+
                        '<option value="patio primero">Patio primero</option>'+
                        '<option value="patio segundo">Patio segundo</option>'+
                        '<option value="peribolo">Peribolo</option>'+
                        '<option value="planta basilical">planta basilical</option>'+
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarETC('+contETCact+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
       arrDibujoETComAct.push({scd_codigo:htmETCact,id: contETCact});
        contETCact++;
        }
        var cadena = '';
        var cont = 1;
        for (var i=0; i<arrDibujoETComAct.length;i++)
        {
          cadena = cadena + arrDibujoETComAct[i].scd_codigo;
          cont++;
        }
        $('#agrillaTipologicosC').html(cadena);
       }


   function actualizarETC(){
    var htmlETCoAct;
        htmlETCoAct='<br><div class="col-md-12">'+                   
                '<div class="col-md-4"><br>'+         
                    '<select class="form-control" id="atipologicos1'+contETCactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="bueno">Bueno</option>'+
                        '<option value="regular">Regular</option>'+
                        '<option value="malo">Malo</option>'+                                     
                    '</select>'+
                '</div>'+                  
                '<div class="col-md-4"><br>'+               
                    '<select class="form-control" id="atipologicos2'+contETCactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="original">Original</option>'+
                        '<option value="nuevo">Nuevo</option>'+
                        '<option value="modificado">Modificado</option>'+                       
                    '</select>'+
                '</div>'+     
                '<div class="col-md-3"><br>'+        
                    '<select class="form-control" id="atipologicos3'+contETCactual+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="arqueria">Arqueria</option>'+
                        '<option value="atrio">Atrio</option>'+
                        '<option value="capilla">Capilla</option>'+
                        '<option value="chifon">Chifon</option>'+
                        '<option value="claustro">Claustro</option>'+   
                        '<option value="corredor">Corredor</option>'+
                        '<option value="corredor ardiente">Corredor ardiente</option>'+
                        '<option value="crujias">Crujias</option>'+
                        '<option value="enfarolado">Enfarolado</option>'+
                        '<option value="estacionamiento">Estacionamiento</option>'+
                        '<option value="galeria">Galeria</option>'+
                        '<option value="jardin interior">Jardin interior</option>'+
                        '<option value="hall">Hall</option>'+
                        '<option value="nave unica">Nave unica</option>'+
                        '<option value="naves">Naves</option>'+
                        '<option value="patio primero">Patio primero</option>'+
                        '<option value="patio segundo">Patio segundo</option>'+
                        '<option value="peribolo">Peribolo</option>'+
                        '<option value="planta basilical">planta basilical</option>'+
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarETC('+contETCactual+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadena='';
        var cont=0;
        arrDibujoETComAct.forEach(function (item, index, array) 
        {
          aarrCodigoTIC[cont].atipologicos1 = $("#atipologicos1"+item.id+"").val();
          aarrCodigoTIC[cont].atipologicos2 = $("#atipologicos2"+item.id+"").val();
          aarrCodigoTIC[cont].atipologicos3 = $("#atipologicos3"+item.id+"").val();
          cont++;
        });
        aarrCodigoTIC.push({atipologicos1:'N', atipologicos2:'N',atipologicos3:'N'});
        arrDibujoETComAct.push({scd_codigo:htmlETCoAct,id: contETCactual});
        contETCactual++;
        cont=1;
        for (var i=0; i<arrDibujoETComAct.length;i++)
        {
          cadena=cadena + arrDibujoETComAct[i].scd_codigo;
          cont++;
        }
        $('#agrillaTipologicosC').html(cadena); 
        var conto=0;
        arrDibujoETComAct.forEach(function (item, index, array) 
        { 
          $("#atipologicos1"+item.id+"").val(aarrCodigoTIC[conto].atipologicos1);
          $("#atipologicos2"+item.id+"").val(aarrCodigoTIC[conto].atipologicos2);
          $("#atipologicos3"+item.id+"").val(aarrCodigoTIC[conto].atipologicos3);
          conto=conto+1;
        });
      
    }
     
        function eliminarETC($id){
        if(contETCactual>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoETComAct.forEach(function (item, index, array) 
                {
                    aarrCodigoTIC[cont].atipologicos1 = $("#atipologicos1"+item.id+"").val();
                    aarrCodigoTIC[cont].atipologicos2 = $("#atipologicos2"+item.id+"").val();
                    aarrCodigoTIC[cont].atipologicos3 = $("#atipologicos3"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoETComAct.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoTIC.splice(pos,1);
                arrDibujoETComAct.splice(pos,1);        
                cont=1;
                for (var i=0; i<arrDibujoETComAct.length;i++)
                {
                    cadena=cadena +arrDibujoETComAct[i].scd_codigo;
                    cont++;
                }                
                $('#agrillaTipologicosC').html(cadena);
                
                cont=0;
                arrDibujoETComAct.forEach(function (item, index, array) 
                {
                    $("#atipologicos1"+item.id+"").val(aarrCodigoTIC[cont].atipologicos1);
                    $("#atipologicos2"+item.id+"").val(aarrCodigoTIC[cont].atipologicos2);
                    $("#atipologicos3"+item.id+"").val(aarrCodigoTIC[cont].atipologicos3);
                    cont=cont+1;
                });
            });
        }
    }
//----fin grilla elementos tipologicos compositivos------
//---Inicio grilla hogar---------------------------------

   var arrDibujoHogAct = [];
   var aarrCodigoHogar = [];
   var contHact = 0;
  function editarHogar(longitudGrilla){
  var contHoAct=1;
  arrDibujoHogAct = [];
  var htmHact;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmHact='<br><div class="col-md-12">'+
                '<div class="col-md-2"><br>'+
                    '<input class="form-control" type="text" id="ahogar1'+contHoAct+'"  placeholder="hogar Descripcion"></input>'+
                '</div>'+      
                '<div class="col-md-2"><br>'+          
                    '<select class="form-control" id="ahogar2'+contHoAct+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="0-4">0  - 4</option>'+
                        '<option value="5-10">5  - 10</option>'+
                        '<option value="11-20">11  - 20</option>'+
                        '<option value="21-40">21  - 40</option>'+
                        '<option value="41-65">41  - 65</option>'+
                        '<option value="65 mas">65 mas</option>'+              
                    '</select>'+
                '</div>'+ 
                '<div class="col-md-2"><br>'+
                '<input type="checkbox" id="ahogar3'+contHoAct+'"> -   </input>'+       
                '<input type="checkbox" id="ahogar4'+contHoAct+'"> </input>'+ 
                '</div>'+  
                '<div class="col-md-2"><br>'+                
                    '<select class="form-control" id="ahogar5'+contHoAct+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alquiler">Alquiler</option>'+
                        '<option value="anticresis">Anticresis</option>'+  
                        '<option value="desconocida">Desconocida</option>'+                       
                        '<option value="prestamo">Prestamo</option>'+
                        '<option value="propia">Propia</option>'+
                        '<option value="usucapion">Usucapion</option>'+
                    '</select>'+
                '</div>'+     
                '<div class="col-md-2"><br>'+          
                    '<select class="form-control" id="ahogar6'+contHoAct+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="ninguno">Ninguno</option>'+ 
                        '<option value="desconocido">Desconocido</option>'+                       
                        '<option value="litigio">En litigio</option>'+
                        '<option value="compartida">Prop. Compartida</option>'+
                        '<option value="saneado">Saneado</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarHogar('+contHoAct+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
       arrDibujoHogAct.push({scd_codigo:htmHact,id: contHoAct});
        contHoAct++;
        }
        var cadena = '';
        var cont = 1;
        for (var i=0; i<arrDibujoHogAct.length;i++)
        {
          cadena = cadena + arrDibujoHogAct[i].scd_codigo;
          cont++;
        }
        $('#agrillaHogares').html(cadena);
       }

 function actualizarhogar(){
    var htmlHogarAct;
        htmlHogarAct='<br><div class="col-md-12">'+
                '<div class="col-md-2"><br>'+
                    '<input class="form-control" type="text" id="ahogar1'+contHact+'"  placeholder="hogar Descripcion"></input>'+
                '</div>'+      
                '<div class="col-md-2"><br>'+          
                    '<select class="form-control" id="ahogar2'+contHact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="0-4">0  - 4</option>'+
                        '<option value="5-10">5  - 10</option>'+
                        '<option value="11-20">11  - 20</option>'+
                        '<option value="21-40">21  - 40</option>'+
                        '<option value="41-65">41  - 65</option>'+
                        '<option value="65 mas">65 mas</option>'+              
                    '</select>'+
                '</div>'+ 
                '<div class="col-md-2"><br>'+
                '<input type="checkbox" id="ahogar3'+contHact+'"> -   </input>'+       
                '<input type="checkbox" id="ahogar4'+contHact+'"> </input>'+ 
                '</div>'+  
                '<div class="col-md-2"><br>'+                
                    '<select class="form-control" id="ahogar5'+contHact+'" >'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="alquiler">Alquiler</option>'+
                        '<option value="anticresis">Anticresis</option>'+  
                        '<option value="desconocida">Desconocida</option>'+                       
                        '<option value="prestamo">Prestamo</option>'+
                        '<option value="propia">Propia</option>'+
                        '<option value="usucapion">Usucapion</option>'+
                    '</select>'+
                '</div>'+     
                '<div class="col-md-2"><br>'+          
                    '<select class="form-control" id="ahogar6'+contHact+'" name="tipoarg1">'+
                        '<option value="N" selected="selected">--Seleccione--</option>'+
                        '<option value="ninguno">Ninguno</option>'+ 
                        '<option value="desconocido">Desconocido</option>'+                       
                        '<option value="litigio">En litigio</option>'+
                        '<option value="compartida">Prop. Compartida</option>'+
                        '<option value="saneado">Saneado</option>'+              
                    '</select>'+
                '</div>'+
                '<div class="col-md-1">'+
                  '<a style="cursor:pointer;" type="button">'+
                  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarHogar('+contHact+');"></i>'+
                  '</a>'+
                '</div>'+
          '</div>';
        var cadena='';
        var cont=0;
        arrDibujoHogAct.forEach(function (item, index, array) 
        {
          aarrCodigoHogar[cont].ahogar1 = $("#ahogar1"+item.id+"").val();
          aarrCodigoHogar[cont].ahogar2 = $("#ahogar2"+item.id+"").val();
          aarrCodigoHogar[cont].ahogar3 = document.getElementById("ahogar3"+item.id+"").checked;
          aarrCodigoHogar[cont].ahogar4 = document.getElementById("ahogar4"+item.id+"").checked;
          aarrCodigoHogar[cont].ahogar5 = $("#ahogar5"+item.id+"").val();
          aarrCodigoHogar[cont].ahogar6 = $("#ahogar6"+item.id+"").val();
          cont++;
        });
        aarrCodigoHogar.push({ahogar1:'', ahogar2:'N',ahogar3:'',ahogar4:'',ahogar5:'N',ahogar6:'N'});
        arrDibujoHogAct.push({scd_codigo:htmlHogarAct,id: contHact});
        contHact++;
        cont=1;
        for (var i=0; i<arrDibujoHogAct.length;i++)
        {
          cadena=cadena + arrDibujoHogAct[i].scd_codigo;
          cont++;
        }
        $('#agrillaHogares').html(cadena); 
        var conto=0;
        arrDibujoHogAct.forEach(function (item, index, array) 
        { 
          $("#ahogar1"+item.id+"").val(aarrCodigoHogar[conto].ahogar1);
          $("#ahogar2"+item.id+"").val(aarrCodigoHogar[conto].ahogar2);        
          document.getElementById("ahogar3"+item.id+"").checked =aarrCodigoHogar[conto].ahogar3 ;
          document.getElementById("ahogar4"+item.id+"").checked =aarrCodigoHogar[conto].ahogar4 ;
          $("#ahogar5"+item.id+"").val(aarrCodigoHogar[conto].ahogar5);
          $("#ahogar6"+item.id+"").val(aarrCodigoHogar[conto].ahogar6);
          conto=conto+1;
        });
   
    }

   function eliminarHogar($id){
        if(contHact>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadena='';
                var cont=0;
                var pos=-1;
                arrDibujoHogAct.forEach(function (item, index, array) 
                {
                    aarrCodigoHogar[cont].ahogar1 = $("#ahogar1"+item.id+"").val();
                    aarrCodigoHogar[cont].ahogar2 = $("#ahogar2"+item.id+"").val();
                    aarrCodigoHogar[cont].ahogar3 = document.getElementById("ahogar3"+item.id+"").checked;
                    aarrCodigoHogar[cont].ahogar4 = document.getElementById("ahogar4"+item.id+"").checked;
                    aarrCodigoHogar[cont].ahogar5 = $("#ahogar5"+item.id+"").val();
                    aarrCodigoHogar[cont].ahogar6 = $("#ahogar6"+item.id+"").val();
                    cont=cont+1;
                });
                pos = arrDibujoHogAct.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoHogar.splice(pos,1);
                arrDibujoHogAct.splice(pos,1);
   
                cont=1;
                for (var i=0; i<arrDibujoHogAct.length;i++)
                {
                    cadena = cadena + arrDibujoHogAct[i].scd_codigo;
                    cont++;
                }                
                $('#agrillaHogares').html(cadena);
                
                cont=0;
                arrDibujoHogAct.forEach(function (item, index, array) 
                {
                    $("#ahogar1"+item.id+"").val(aarrCodigoHogar[cont].ahogar1);
                    $("#ahogar2"+item.id+"").val(aarrCodigoHogar[cont].ahogar2);        
                    document.getElementById("ahogar3"+item.id+"").checked = aarrCodigoHogar[cont].ahogar3 ;
                    document.getElementById("ahogar4"+item.id+"").checked = aarrCodigoHogar[cont].ahogar4 ;
                    $("#ahogar5"+item.id+"").val(aarrCodigoHogar[cont].ahogar5);
                    $("#ahogar6"+item.id+"").val(aarrCodigoHogar[cont].ahogar6);
                    cont=cont+1;
                });
            });
        }
    }

//----fin grilla hogar-----------------------
//----FIN EDITAR Y ACTUALIZAR GRILLAS--------


 function verPanelAcapites(){
  $("input[name=cver_acapites]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelVerAcapites").style = "display:block;";
      //console.log('si');
   }else{
        document.getElementById("panelVerAcapites").style = "display:none;";       
     //console.log('no');
   }
 });
};

function verPanelElemestructurales()
{
  $("input[name=cver_elemestructurales]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_elemestructurales").style = "display:block;";
      //console.log('si');
   }else{      
       document.getElementById("panelcver_elemestructurales").style = "display:none;";        
     //console.log('no');
   }
 });
}
function verPanelelemcompositivos()
{
  $("input[name=cver_elemcompositivos]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_elemcompositivos").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("panelcver_elemcompositivos").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelacabados(){
  $("input[name=cver_acabados]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_acabados").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("panelcver_acabados").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelelemartisticoscompo () {
  $("input[name=cver_elemartisticoscompo]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_elemartisticoscompo").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("panelcver_elemartisticoscompo").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelelemornamentales () {
  $("input[name=cver_elemornamentales]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_elemornamentales").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("panelcver_elemornamentales").style = "display:none;";
     
     //console.log('no');
   }
 });
}
function verPanelelemornamentales () {
  $("input[name=cver_elemornamentales]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_elemornamentales").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("panelcver_elemornamentales").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelelemtipocomp () {
  $("input[name=cver_elemtipocomp]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_elemtipocomp").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("panelcver_elemtipocomp").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelhogares() {
  $("input[name=cver_hogares]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("panelcver_hogares").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("panelcver_hogares").style = "display:none;";
       
     //console.log('no');
   }
 });
}
function verPanelServicios(){
    var cservicios = document.getElementById("cver_servicios").checked;
    ////console.log("cservicios",cservicios);
    if(cservicios){
      document.getElementById("panelcver_servicios").style = "display:block;";
    }
    else{
       document.getElementById("panelcver_servicios").style = "display:none;";
    }
}
/***********check actualizar grillas**************/

 function verPanelAcapitesact(){
  $("input[name=aver_acapites]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelVerAcapites").style = "display:block;";
      //console.log('si');
   }else{
        document.getElementById("apanelVerAcapites").style = "display:none;";       
     //console.log('no');
   }
 });
};

function verPanelElemestructuralesact()
{
  $("input[name=aver_elemestructurales]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_elemestructurales").style = "display:block;";
      //console.log('si');
   }else{      
       document.getElementById("apanelcver_elemestructurales").style = "display:none;";        
     //console.log('no');
   }
 });
}
function verPanelelemcompositivosact()
{
  $("input[name=aver_elemcompositivos]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_elemcompositivos").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("apanelcver_elemcompositivos").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelacabadosact(){
  $("input[name=aver_acabados]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_acabados").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("apanelcver_acabados").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelelemartisticoscompoact () {
  $("input[name=aver_elemartisticoscompo]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_elemartisticoscompo").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("apanelcver_elemartisticoscompo").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelelemornamentalesact () {
  $("input[name=aver_elemornamentales]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_elemornamentales").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("apanelcver_elemornamentales").style = "display:none;";
     
     //console.log('no');
   }
 });
}
function verPanelelemornamentalesact () {
  $("input[name=aver_elemornamentales]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_elemornamentales").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("apanelcver_elemornamentales").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelelemtipocompact() {
  $("input[name=aver_elemtipocomp]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_elemtipocomp").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("apanelcver_elemtipocomp").style = "display:none;";
     
     //console.log('no');
   }
 });
}

function verPanelhogaresact() {
  $("input[name=aver_hogares]").each(function (index) {  
   if($(this).is(':checked')){
     document.getElementById("apanelcver_hogares").style = "display:block;";
      //console.log('si');
   }else{
     document.getElementById("apanelcver_hogares").style = "display:none;";
       
     //console.log('no');
   }
 });
}
function verPanelServiciosact(){
    var cservicios = document.getElementById("aver_servicios").checked;
    ////console.log("cservicios",cservicios);
    if(cservicios){
      document.getElementById("apanelcver_servicios").style = "display:block;";
    }
    else{
       document.getElementById("apanelcver_servicios").style = "display:none;";
    }
}

/***********fin check actualizar grillas********/

////Puntuacion Inmuebles////
var puntuacionTotal1 = 0;
function capturarCheckPuntuacion(){
  
   puntuacionTotal1 = 0;
  $("input[name=puntuacion]").each(function (index) {  
    if($(this).is(':checked')){
      puntuacionTotal1 = puntuacionTotal1 + parseFloat($(this).val());
      //console.log("puntuacionTotal1",puntuacionTotal1);
    }
  });
  $("#cpuntuacion_in").val(puntuacionTotal1);
 
};
var puntuacionTotalAct= 0;
function capturarCheckPuntuacionAct(){
  
   puntuacionTotalAct = 0;
  $("input[name=apuntuacion]").each(function (index) {  
    if($(this).is(':checked')){
      puntuacionTotalAct = puntuacionTotalAct + parseFloat($(this).val());
      
    }
  });
  $("#apuntuacion_in").val(puntuacionTotalAct);
 
};


function CheckPuntuacionBloqueRegistrar(){
  var puntuacionTotal = 0;
  var puntuacionRegistrada = $("#cpuntuacion_in").val();
  //console.log("puntuacionRegistrada",puntuacionRegistrada);

  $("input[name=cpuntuacion2]").each(function (index) {  
    if($(this).is(':checked')){
      puntuacionTotal = puntuacionTotal + parseFloat($(this).val());

    }
  });

  document.getElementById('cpuntuacion').value = puntuacionTotal + parseInt(puntuacionRegistrada);
  var totalPuntuacionR = puntuacionTotal + parseInt(puntuacionRegistrada);
  //console.log("totalPuntuacionR",totalPuntuacionR);

  if(totalPuntuacionR<50){
    if(totalPuntuacionR==0){
      document.getElementById('ccategoria_inmueble').value='D';
    }else{ 
      document.getElementById('ccategoria_inmueble').value='C';
    }
  }else{
    if(totalPuntuacionR<80){
      document.getElementById('ccategoria_inmueble').value='B';
   
    }else{
      if(totalPuntuacionR<=105){
        document.getElementById('ccategoria_inmueble').value='A';
       
      }
    }
  }
};

function capturarCheckPuntuacionBloque(){
  var puntuacionTotal2 = 0;
  var puntuacionActualizada = $("#apuntuacion_in").val();
  //console.log("puntuacionActualizada",puntuacionActualizada);

  $("input[name=puntuacion2]").each(function (index) {  
    if($(this).is(':checked')){
      puntuacionTotal2 = puntuacionTotal2 + parseFloat($(this).val());

    }
  });

  document.getElementById('apuntuacion').value = puntuacionTotal2 + parseInt(puntuacionActualizada);
  var totalPuntuacion = puntuacionTotal2 + parseInt(puntuacionActualizada);
  //console.log("totalPuntuacion",totalPuntuacion);

  if(totalPuntuacion<50){
    if(totalPuntuacion==0){
      document.getElementById('acategoria_inmueble').value='D';
    }else{ 
      document.getElementById('acategoria_inmueble').value='C';
    }
  }else{
    if(totalPuntuacion<80){
      document.getElementById('acategoria_inmueble').value='B';
    }else{
      if(totalPuntuacion<=105){
        document.getElementById('acategoria_inmueble').value='A';
      }
    }
  }
};

//----------FUNCIONES PARA VALIDAR LAS PÁGINAS DE LOS MODALES
function validar1(){
  //console.log("aquiiiiii validar 1");
  var valor = document.getElementById('cbloqueInventario_in').value;
  var pag1 = document.getElementById('pagina1').value;
  var codigoCatastral = document.getElementById('ccodCatastral_in').value;
  var cpuntuacionBloque = document.getElementById('cpuntuacion_in').value;
  var crangoEpoca = document.getElementById('crangoEpoca_in').value;
  //console.log("crangoEpoca",crangoEpoca);
  var cEstiloBloque = document.getElementById('cestiloGeneral_in').value;
  $( "#ccodigo_catastral" ).val(codigoCatastral);
  $( "#cpuntuacion" ).val(cpuntuacionBloque);
  $( "#crango_epoca" ).val(crangoEpoca);
  $( "#cestilo_bloque" ).val(cEstiloBloque);


  if(valor == 0 && pag1==1){
    swal('Campo Obligatorio: Bloque Inventariado'); 
  }else{
    if(valor>0  && pag1 == 1)
    {
     $("#myCreateBienesInmueble").modal('toggle');
     $("#myCreateBienesInmueble1").modal();
   }
 }
 CheckPuntuacionBloqueRegistrar();
}
// -- pagina 2 bloques--//
function volver2()
{
  var valor = document.getElementById('cbloqueInventario_in').value;
  var pag2 = document.getElementById('pagina2').value;
  console.log("valor",valor);
  console.log("pag2",pag2);
  if(valor>0 && pag2 == 2)
  {
   $("#myCreateBienesInmueble").modal();
   $("#myCreateBienesInmueble1").modal('toggle');
 }
}
function volverUpdate(){

$("#myUpdateBienesInmueble").modal();
$("#myUpdateBienesInmueble1").modal('toggle');
}

function validar2(){
  var valor = document.getElementById('cbloqueInventario_in').value;
  var pag2 = document.getElementById('pagina2').value;
  if((valor>0 && valor<2)  && pag2 == 2)
  {
   swal('guardar 2 ');
  
 }
 else {if( valor>0 && pag2 == 2)
  {
    $("#myCreateBienesInmueble1").modal('toggle');
   
  }
}
}

function validarUpdate1(){

  var valor = document.getElementById('abloqueInventario_in').value;
  var pag1 = document.getElementById('apagina1').value;
  var acodigoCatastral = document.getElementById('acodCatastral_in').value;
  var apuntuacionBloque = document.getElementById('apuntuacion_in').value;
  var arangoEpoca = document.getElementById('arangoEpoca_in').value;
  var aEstiloBloque = document.getElementById('aestiloGeneral_in').value;
  $( "#aid_bloque" ).val(valor);
  $( "#acodigo_catastral" ).val(acodigoCatastral);
  $( "#apuntuacion" ).val(apuntuacionBloque);
  $( "#arango_epoca" ).val(arangoEpoca);
  $( "#aestilo_bloque" ).val(aEstiloBloque);

  if(valor == 1 && pag1==1){
    $("#myUpdateBienesInmueble").modal('toggle');
    $("#myUpdateBienesInmueble1").modal();
  }else{
    if(valor>0  && pag1 == 1)
    {
     $("#myUpdateBienesInmueble").modal('toggle');
     $("#myUpdateBienesInmueble1").modal();
   }
 }
 capturarCheckPuntuacionBloque();
}

function validarUpdate2(){
  var valor = document.getElementById('abloqueInventario_in').value;
  var pag2 = document.getElementById('apagina2').value;
  if((valor>0 && valor==1)  && pag2 == 2)
  {
   swal('guardar 2 ');

 }
 else {if( valor>0 && pag2 == 2)
  {
    $("#myUpdateBienesInmueble1").modal('toggle');
  }
}
}


function volverUpdate2()
{
  var valor = document.getElementById('abloqueInventario_in').value;
  var pag2 = document.getElementById('apagina2').value;
  if(valor>0 && pag2 == 2)
  {
   $("#myUpdateBienesInmueble").modal();
   $("#myUpdateBienesInmueble1").modal('toggle');
 }
}


$(document).ready(function (){

 buscarPatrimonioPorTipo(1);
 selectCampos();
 cargarDatosUsuario();
 //------------------------checkbox desplegables registrar-----------------------//
 document.getElementById("panelVerAcapites").style="display:none;";
 document.getElementById("panelVerAcapites").style = "display:none;";
 document.getElementById("panelcver_elemestructurales").style = "display:none;";
 document.getElementById("panelcver_elemcompositivos").style = "display:none;";
 document.getElementById("panelcver_acabados").style = "display:none;";
 document.getElementById("panelcver_elemartisticoscompo").style = "display:none;";
 document.getElementById("panelcver_elemornamentales").style = "display:none;";
 document.getElementById("panelcver_elemtipocomp").style = "display:none;";
 document.getElementById("panelcver_hogares").style = "display:none;";
 document.getElementById("panelcver_servicios").style = "display:none;";
 document.getElementById("listado_campo_valor").style = "display:none;";
 //------------------------checkbox desplegables actualzar-----------------------//
 document.getElementById("apanelVerAcapites").style="display:none;";
 document.getElementById("apanelVerAcapites").style = "display:none;";
 document.getElementById("apanelcver_elemestructurales").style = "display:none;";
 document.getElementById("apanelcver_elemcompositivos").style = "display:none;";
 document.getElementById("apanelcver_acabados").style = "display:none;";
 document.getElementById("apanelcver_elemartisticoscompo").style = "display:none;";
 document.getElementById("apanelcver_elemornamentales").style = "display:none;";
 document.getElementById("apanelcver_elemtipocomp").style = "display:none;";
 document.getElementById("apanelcver_hogares").style = "display:none;";
 document.getElementById("apanelcver_servicios").style = "display:none;";
 

});
</script>

<script  src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script >

  $('textarea').ckeditor();

</script>
@endpush