<div class="modal fade modal-default" id="myUpdateBienesInmueble1" data-backdrop="static" data-keyboard="false" tabindex="-1" rol="dialog" style="overflow-y: scroll;">
  <div class="modal-dialog modal-lg" role="document" rol="dialog" >
    <div class="modal-content">
    {!! Form::open(['class'=>'form-vertical','id'=>'form_resitem_update'])!!}
    <div class="modal-header">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="row">
            <div class="col-md-12">
              <section class="content-header">
                <div class="header_title">
                  <h3>
                   Actualizar registros de Bienes Inmuebles (Bloque) - <span id="acas_nombre_caso2"></span>  
                    <button type="button" class="close" onclick = "volverUpdate2();" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </h3>
                </div>
              </section>
            </div>   
          </div>
        </div>
        <div  style="padding:20px">
          <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">   
          <input type="hidden" name="acasoId_in" id="acasoId_in">
          <input type="hidden" name="anroficha_in" id="anroficha_in">
          <input type="hidden" name="apagina2" id="apagina2" value="2">
          <input type="hidden" name="arango_epoca_hidden" id="arango_epoca_hidden">
          <input type="hidden" name="aestilo_bloque_hidden" id="aestilo_bloque_hidden">
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
               1. IDENTIFICACIÓN:
             </label>
           </div>
          </div>
          <!-- FORMULARIO 1 MERITZA-->
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
               Id. Bloque:
              </label>
              <input id="aid_bloque" type="text" class="form-control" disabled> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Descripción:
              </label>
              <input id="adescripcion" type="text" class="form-control"  placeholder="Descripción"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
              1. ÉPOCA Y ESTILO:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
              Código Catastral:
              </label>
              <input id="acodigo_catastral" type="text" class="form-control"  placeholder="Código Catastral" disabled> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
              Rango de Época:
              </label>
            <select class="form-control" id="arango_epoca" disabled>
                  <option value="(Ninguno)"> No seleccionado...</option> 
                  <option value="1548-1825">1548-1825</option>
                  <option value="1826-1850">1826-1850</option> 
                  <option value="1851-1900">1851-1900</option>
                  <option value="1901-1930">1901-1930</option>  
                  <option value="1931-1960">1931-1960</option> 
                  <option value="1961-1980">1961-1980</option> 
                  <option value="1981-2000">1981-2000</option> 
                  <option value="2000 en Adelante">2000 en Adelante</option>
            </select>
            </div>
          </div>
          <div class="col-md-12">
           <div class="form-group col-md-6">
            <label class="control-label">
               Estilo Bloque:
            </label>
              <select class="form-control" id="aestilo_bloque" disabled >
                    <option value="(Ninguno)"> No seleccionado</option> 
                    <option value="Academicista">Academicista</option>
                    <option value="Arq. Industrial">Arq. Industrial</option>
                    <option value="Arq. Noveau">Arq. Noveau</option> 
                    <option value="Arte Decó">Arte Decó</option>
                    <option value="Barroco">Barroco</option>
                    <option value="Barroco Mestizo">Barroco Mestizo</option>
                    <option value="Brutalista">Brutalista</option>
                    <option value="Californiano">Californiano</option>
                    <option value="Clasicismo postmoderno">Clasicismo postmoderno</option>
                    <option value="Clasicista">Clasicista</option>
                    <option value="Colonial">Colonial</option>
                    <option value="Constructivista">Constructivista</option>
                    <option value="Contemporaneo">Contemporaneo</option>
                    <option value="Cubista">Cubista</option>
                    <option value="Ecléctico">Ecléctico</option>
                    <option value="Estilo no definido">Estilo no definido</option>
                    <option value="Expresionista">Expresionista</option>
                    <option value="Funcionalista">Funcionalista</option>
                    <option value="Georgiano">Georgiano</option>
                    <option value="Georginiano">Georginiano</option>
                    <option value="High Tech">High Tech</option>
                    <option value="Historicista">Historicista</option>
                    <option value="Internacional">Internacional</option>
                    <option value="Minimalismo">Minimalismo</option>
                    <option value="Moderno">Moderno</option>
                    <option value="Mosarable">Mosarable</option>
                    <option value="Mudéjar">Mudéjar</option>
                    <option value="Neoclásico">Neoclásico</option>
                    <option value="Neocolonial">Neocolonial</option>
                    <option value="Neogótico">Neogótico</option>
                    <option value="Neotiwanacota">Neotiwanacota</option>
                    <option value="Neovernacular">Neovernacular</option>
                    <option value="Organicista">Organicista</option>
                    <option value="Postmoderno">Postmoderno</option>
                    <option value="Racionalista">Racionalista</option>
                    <option value="Renacentista">Renacentista</option>
                    <option value="Republicano">Republicano</option>
                    <option value="Tudor">Tudor</option> 
                    <option value="Vernacular">Vernacular</option> 
                    <option value="Victoriano">Victoriano</option>
           </div>
            <div class="form-group col-md-6">
             <input type="checkbox" name="puntuacion2" id="aepocaestilocheck" value = "3" onclick="capturarCheckPuntuacionBloque()">Época y Estilo<br>
             <input type="checkbox" name="puntuacion2" id="asistemaconstructivocheck" value = "3" onclick="capturarCheckPuntuacionBloque()">Sistema Constructivo<br>
            </div>
          </div>
          <div class="col-md-12">
           <div class="form-group col-md-6">
            <label class="control-label">
              Sistema Constructivo:
             </label>
              <select class="form-control" id="asistema_constructivo" name="asistema_constructivo">
                  <option value=""> Seleccionar...</option> 
                  <option value="Sistema Independiente">Sistema Independiente</option>
                  <option value="Sistema Mixto">Sistema Mixto</option> 
                  <option value="Sistema Portante">Sistema Portante</option>
              </select>
            </div>
          </div>

          <!--INICIO GRILLAS 1-->
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <h5><center> Peligros Potenciales:</center></h5>
                <small>
                  <div class="pull-right">
                    <a   type="button" style="cursor:pointer;">
                      <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Peligros Potenciales" onclick="actualizarPeligrosPotenciales();"></i>
                    </a>
                  </div>
                </small>
               <div id="apeligrospotenciales"></div>
            </div>
          </div>
          <!--FIN GRILLAS 1-->
          <!--INICIO GRILLAS 2-->
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <label class="control-label ">
                   Patologías Edificación:
                </label>
                <small>
                  <div class="pull-right">
                    <a   type="button" style="cursor:pointer;">
                      <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Patologías Edificación" onclick="actualizarPatologiaEdificacion();"></i>
                    </a>
                  </div>
                </small>
               <div id="apatologiaedificacion"></div>
            </div>
          </div> 
           <!--FIN GRILLAS 2--> 
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
               3. DATOS ADICIONALES:
             </label>
           </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="aontrol-label">
              Nro. de Viviendas:
              </label>
              <input id="anro_viviendas" type="number" class="form-control"  placeholder="Nro. de Viviendas"> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
              Nro. de Hogares:
              </label>
              <input id="anro_hogares" type="number" class="form-control"  placeholder="Nro. de Hogares:"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
               4. RESUMEN:
             </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
                 <label class="control-label">
                  Acapites
                 </label>
            </div><br>
            <div class="form-group col-md-10">
              <input type="checkbox" id="aacapites1" name="puntuacion2" value="8" onclick="capturarCheckPuntuacionBloque()">Elementos Estructurales (8%)<br>
              <input type="checkbox" id="aacapites2" name="puntuacion2" value="6" onclick="capturarCheckPuntuacionBloque()">Elementos Compositivos (6%)<br>
              <input type="checkbox" id="aacapites3" name="puntuacion2" value="3" onclick="capturarCheckPuntuacionBloque()">Acabados (3%)<br>
              <input type="checkbox" id="aacapites4" name="puntuacion2" value="3" onclick="capturarCheckPuntuacionBloque()">Elementos Artisiticos Compositivos (3%)<br>
              <input type="checkbox" id="aacapites5" name="puntuacion2" value="4" onclick="capturarCheckPuntuacionBloque()">Elementos Ornamentales (4%)<br>
              <input type="checkbox" id="aacapites6" name="puntuacion2" value="9" onclick="capturarCheckPuntuacionBloque()">Elementos Tipologicos Compositivos (9%)<br>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
               VALORACIÓN:
              </label>
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
              Indicador Histórico (10% / 20%)
              </label>
              </div><br>
              <div class="form-group col-md-10">
                 <input type="checkbox" id="aindicador_historico1" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que posee un valor testimonial y documental, que ilustra el desarrollo político, social, religioso, cultural, económico y la forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad.<br>
                 <input type="checkbox" id="aindicador_historico2" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que desde su creación se constituye en un hito en la memoria historica.<br>
                 <input type="checkbox" id="aindicador_historico3" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que en el transcurso del tiempo se constituyo en hito en la memoria historica, al ser escenario relacionado con personas o eventos importantes, dentro del proceso Histórico,social, cultural, económico de las comunidades.<br>
                 <input type="checkbox" id="aindicador_historico4" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que forma parte de un conjunto Histórico.<br>
                 <input type="checkbox" id="aindicador_historico5" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que posee elementos arqueológicos o testimonios arquitectonicos de construcciones preexistentes que documentan su evolución y desarrollo.<br>
            </div>
          </div> 
          <div class="col-md-12">
            <div class="form-group col-md-2">
               <label class="control-label">
               Indicador Artístico (6% / 15%)
               </label>
            </div><br>
            <div class="form-group col-md-10">

                 <input type="checkbox" id="aindicador_artistico1" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que se constituye en un ejemplo sobresaliente por su singularidad arquitectónica/artística.<br>
                 <input type="checkbox" id="aindicador_artistico2" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que conserva elementos arquitectónicos y tradicionales de interés<br>
                 <input type="checkbox" id="aindicador_artistico3" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble poseedor de expresiones artísticas y decorativas de interés.<br>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
               <label class="control-label">
               Indicador Arquitectónico(4% / 15%)
               </label>
            </div><br>
            <div class="form-group col-md-10">
                 <input type="checkbox" id="aindicador_arquitectonico1" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble poseedor de características Tipológicas representativas de estilos arquitectónicos.<br>
                 <input type="checkbox" id="aindicador_arquitectonico2" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalles.<br>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
               <label class="control-label">
               Indicador Tecnológico (6% / 13%)
               </label>
               </div><br>
               <div class="form-group col-md-10">
               <input type="checkbox" id="aindicador_teclogico1" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que se constituye en un exponente de las técnicas constructivas y uso de materiales característicos de una época o región determinada.<br>
               <input type="checkbox" id="aindicador_teclogico2" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble donde la aplicación de técnicas constructivas son singulares o de especial interés.<br>
               <input type="checkbox" id="aindicador_teclogico3" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada.<br>
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
              Indicador Integridad (4% / 15%)
              </label>
              </div><br>
              <div class="form-group col-md-10">
               <input type="checkbox" id="aindicador_integridad1" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble que conserva el total de su tipología, materiales y técnicas constructivas originales.<br>
               <input type="checkbox" id="aindicador_integridad2" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">Inmueble o espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales.<br>
               <input type="checkbox" id="aindicador_integridad3" name="puntuacion2" value="0" onclick="capturarCheckPuntuacionBloque()">Inmueble que ha sufrido intervenciones irreversibles que han modificado parcialmente la integridad del mismo.<br>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
             <label class="control-label">
              Indicador Urbano (9% / 15%)
             </label>
            </div>
            <br>
            <div class="form-group col-md-10">
               <input type="checkbox" id="aindicador_urbano1"  name="puntuacion2" value="3" onclick="capturarCheckPuntuacionBloque()">Inmueble que contribuye a definir un entorno de valor por su configuración y calidad en su estructura urbanística, el paisaje y/o el espacio público.<br>
               <input type="checkbox" id="aindicador_urbano2" name="puntuacion2" value="3" onclick="capturarCheckPuntuacionBloque()">Inmueble que forma un perfil homogéneo y/ó armónico.<br>
               <input type="checkbox" id="aindicador_urbano3" name="puntuacion2" value="3" onclick="capturarCheckPuntuacionBloque()">Inmueble considerado hito de referencia por su emplazamiento.<br>
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="form-group col-md-2">
               <label class="control-label">
                Indicador Inmaterial
               </label>
            </div>
            <br>
            <div class="form-group col-md-10">
                 <input type="checkbox" id="aindicador_inmaterial1" name="puntuacion2" value="2" onclick="capturarCheckPuntuacionBloque()">INDICADOR INMATERIAL (2% / 7%) Inmueble relacionado con la organización social, forma de vida, usos, representaciones, expresiones, conocimientos y técnicas que las comunidades y grupos sociales reconocen como parte de patrimonio.<br>
                 <input type="checkbox" id="aindicador_inmaterial2" name="puntuacion2" value="1" onclick="capturarCheckPuntuacionBloque()">INDICADOR SIMBÓLICO (1% / 5%) Inmueble o espacio abierto único que expresa una significación cultural, representa identidad y pertenencia para el colectivo. <br>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
              Indicador Histórico:
              </label>
              <br>
              <textarea name="textarea" id="aindicador_historico"  rows="5" cols="100" class="control-label barraTextarea"></textarea> 
            </div>
          </div>
          <div class="col-md-12">
           <div class="form-group col-md-12">
              <label class="control-label">
              Indicador Artístico:
              </label>
              <br>
              <textarea name="textarea" id="aindicador_atistico"  rows="5" cols="100" class="control-label barraTextarea"></textarea> 
           </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
              Indicador Arquitectónico:
              </label>
              <br>
              <textarea name="textarea" id="aindicador_arquitectonico" rows="5" cols="100" class="control-label barraTextarea"></textarea> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
              Indicador Tecnológico:
              </label>
              <br>
              <textarea name="textarea" id="aindicador_teclogico"  rows="5" cols="100" class="control-label barraTextarea"></textarea> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
              Indicador de Integridad:
              </label>
              <br>
              <textarea name="textarea" id="aindicador_integrid" rows="5" cols="100" class="control-label barraTextarea"></textarea> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-12">
              <label class="control-label">
              Indicador Urbano:
              </label>
              <br>
            <textarea name="textarea" id="aindicador_urbano"  rows="5" cols="100" class="control-label barraTextarea"></textarea>
            </div>
          </div>
          <div class="col-md-12">  
            <div class="form-group col-md-12">
              <label class="control-label">
              Indicador Inmaterial:
              </label>
              <br>
              <textarea name="textarea" id="aindicador_inmaterial" rows="5" cols="100" class="control-label barraTextarea"></textarea> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
               <label class="control-label">
               Indicador Simbólico:
               </label>
               <br>
               <textarea name="textarea" id="aindicador_simbolico"  rows="5" cols="100" class="control-label barraTextarea"></textarea>
            </div>
          </div>
          <div class="col-md-12">  
            <div class="form-group col-md-12">
              <label class="control-label">
              Descripción Interior:
              </label>
              <br>
              <textarea name="textarea" id="aindicador_interior" rows="5" cols="100" class="control-label barraTextarea"></textarea> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
             <label class="control-label">
              Descripción Exterior:
             </label>
             <br>
             <textarea name="textarea" id="adescripcion_exterior"  rows="5" cols="100" class="control-label barraTextarea"></textarea> 
            </div>
          </div>


          <!--<div class="col-md-12">
            <div class="form-group col-md-6">
                 <input type="checkbox" id="aver_servicios">Ver Servicios<br>
                 <input type="checkbox" id="aver_acapites">Ver Acapites<br>
                 <input type="checkbox" id="aver_hogares">Ver Hogares<br>
                 <br>
            </div>
          </div>-->

  <div class="col-md-12">
    <div class="form-group col-md-12">
     <!--NANCY-->
      <input type="checkbox" id="aver_servicios" name="aver_servicios" onchange="verPanelServiciosact();">Ver Servicios<br>
     <!-- <label class="control-label">Ver Servicios</label>-->
      <div id="apanelcver_servicios">
         <!--INICIO GRILLAS 3 Instalaciones Estado-->
            <div class="col-md-12">
              <div class="panel-heading">
                <label class="control-label" style="color:#275c26";>
                  Instalaciones Estado
                </label>
              </div>
            </div>
            <div class="col-md-12"> 
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Instalaciones Estado" onclick="actualizarInstalacionesEstado();"></i>
                  </a>
                </div>
              </small>
             <div id="acinstalacionesestado"></div>
            </div>
          <!--FIN GRILLAS 3--> 
           <!--INICIO GRILLAS 4 Servicios-->
            <div class="col-md-12">
              <div class="panel-heading">
                <label class="control-label" style="color:#275c26";>
                 Servicios
                </label>
              </div>
            </div>
            <div class="col-md-12"> 
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Servisios" onclick="actualizarServicios();"></i>
                  </a>
                </div>
              </small>
             <div id="aservicios"></div>
            </div>
          <!--FIN GRILLAS 4-->
           
      </div>
     
      <input type="checkbox" id="aver_acapites" name="aver_acapites" onchange="verPanelAcapitesact();">Ver Características Técnicas<br>
      <div id="apanelVerAcapites">
          <input type="checkbox" id="aver_elemestructurales" name="aver_elemestructurales" onchange="verPanelElemestructuralesact();">Ver Elementos Estructurales<br>
              <div id="apanelcver_elemestructurales">
          <!--INICIO GRILLA 5-->
            <br>
            <div class="col-md-12"> 
              <div class="form-group col-md-12">
               <h4>
                <center>Paredes Exteriores</center>
               </h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Paredes Exteriores" onclick="actualizarParedesExteriores();"></i>
                  </a>
                </div>
              </small>
             <div id="aparedesexteriores"></div>             
             </div>
            </div>
          <!--FIN GRILLAS 5-->
          <!--INICIO GRILLAS 6 Cobertura del Techo-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Cobertura del Techo</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Cobertura del Techo" onclick="actualizarCoberturaTecho();"></i>
                  </a>
                </div>
              </small>
             <div id="acoberturadeltecho"></div>
            </div>
          <!--FIN GRILLAS 6-->
          <!--INICIO GRILLAS 7 Paredes Interiores-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Paredes Interiores</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Paredes Interiores" onclick="actualizarParedesInteriores();"></i>
                  </a>
                </div>
              </small>
             <div id="aparedesinteriores"></div>
            </div>
          <!--FIN GRILLAS 7-->
          <!--INICIO GRILLAS 8 Entre Pisos-->
            <br>
            <br>
            <div class="col-md-12"> 
              <strong><h4><center>Entre Pisos</center></h4>
              <hr></strong>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Entre Pisos" onclick="actualizarEntrePisos();"></i>
                  </a>
                </div>
              </small>
             <div id="aentrepisos"></div>
            </div>
          <!--FIN GRILLAS 8-->
          <!--INICIO GRILLAS 9 Escaleras-->
            <br>
            <br>
            <div class="col-md-12">
              <h4><center>Escaleras</center></h4> 
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Escaleras" onclick="actualizarEscaleras();"></i>
                  </a>
                </div>
              </small>
             <div id="aescaleras"></div>
            </div>
          <!--FIN GRILLAS 9-->
          <!--INICIO GRILLAS 10 Cielo-->
            <br>
            <br>
            <div class="col-md-12">
              <h4><center>Cielos</center></h4> 
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Cielo" onclick="actualizarCielo();"></i>
                  </a>
                </div>
              </small>
             <div id="acielo"></div>
            </div>
          <!--FIN GRILLAS 10--> 
          <!--INICIO GRILLAS 11 Balcones-->
            <br>
            <br>
            <div class="col-md-12">
              <h4><center>Balcones</center></h4> 
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Balcones" onclick="actualizarBalcones();"></i>
                  </a>
                </div>
              </small>
             <div id="abalcones"></div>
            </div>
          <!--FIN GRILLAS 11-->
          <!--INICIO GRILLAS 12 Estructura Cubierta-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Estructura Cubierta</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Estructura Cubierta" onclick="actualizarEstructuraCubierta();"></i>
                  </a>
                </div>
              </small>
             <div id="aestructuracubierta"></div>
            </div>
          <!--FIN GRILLAS 12-->
          <!--INICIO GRILLAS 13 Pisos-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Pisos</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Cielo" onclick="actualizarPisos();"></i>
                  </a>
                </div>
              </small>
             <div id="apisos"></div>
            </div>
            
            <!--FIN GRILLAS 13-->
                     
              </div>
          <input type="checkbox" id="aver_elemcompositivos" name="aver_elemcompositivos" onchange="verPanelelemcompositivosact();">Ver Elementos Composisitivos<br>
              <div id="apanelcver_elemcompositivos">                
             
               

             <!--TANYA-->
                  <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Ventanas Interiores
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarVanosVentanasInteriores();"></i>
                  </a>
                  </div>
                   </small>
                   <div id="aVanosVentanasInteriores"></div>
                    </div>
                  <!--Fin grilla-->
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Ventanas Interiores
                      </label>
                    </div>
                  </div>
                <div class="col-md-12"> 
                <small>
                <div class="pull-right">
                <a   type="button" style="cursor:pointer;">
                <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarVentanasInteriores();"></i>
                </a>
                </div>
                </small>
                <div id="adivelcoVentanasInteriores"></div>
                </div>
                <!--grilla-->
                <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Ventanas Exteriores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualuizarVanosVentanasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="aVanosVentanasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Ventanas Exteriores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarVentanasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="aVentanasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                  <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Puertas Interiores
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarVanosPuertasInteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="aVanosPuertasInteriores"></div>
                  </div>
                  <!--Fin grilla-->
                  <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Puertas Exteriores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarVanosPuertasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="aVanosPuertasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                  <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Puertas Interiores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarPuertasInteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="aPuertasInteriores"></div>
                  </div>
                  <!--Fin grilla-->
                  <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Puertas Exteriores
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarPuertasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="aPuertasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                  <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Rejas
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarRejas();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="apRejas"></div>
                  </div>
                  <!--Fin grilla-->
              </div>
          <input type="checkbox" id="aver_acabados" name="aver_acabados" onchange="verPanelacabadosact();">Ver Acabados<br>
               <div id="apanelcver_acabados">
               

                 <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Pinturas Muros Exteriores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarPinturasMurosExteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <br>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="adivacaPinturasMurosExteriores" class="form-group col-md-12"></div>
                    <br><br>
                  </div><br><br>
                  <!--Fin grilla--> 
                  <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Pinturas Muros Interiores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarPinturasMurosInteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="acaPinturasMurosInterioresA" class="form-group col-md-12"></div>
                  </div><br><br>
                  <!--Fin grilla--> 
                  <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Acabados de Muros Exteriores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarAcabadosMurosExteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="divacaAcabadosMurosExterioresA"></div>
                  </div><br><br>
                  <!--Fin grilla--> 
                  <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Acabados de Muros Interiores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarAcabadosMurosInteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="divacaAcabadosMurosInterioresA"></div>
                  </div>
                  <!--Fin grilla-->  

               </div>

          <input type="checkbox" id="aver_elemartisticoscompo" name="aver_elemartisticoscompo" onchange="verPanelelemartisticoscompoact();">Ver Elementos Artísticos Compositivos<br>
              <div id="apanelcver_elemartisticoscompo"><br>  
               <div class="col-md-12"> 
                  <small>
                  <div class="form-group col-md-3">
                    <label class="control-label">ESTADO CONSERVACION</label> 
                  </div>
                   <div class="form-group col-md-3">
                    <label class="control-label"> INTEGRIDAD:</label> 
                  </div>
                   <div class="form-group col-md-3">
                    <label class="control-label"> TIPO:</label> 
                  </div>                        
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarEAC()"></i>
                  </a>
                  </div>
                  </small>
                  <div id="agrillaArtisiticoC"></div>
                  </div>
                  <!--Fin grilla-->  
              </div>
          <input type="checkbox" id="aver_elemornamentales" name="aver_elemornamentales" onchange="verPanelelemornamentalesact();">Ver Elementos Ornamentales<br>
              <div id="apanelcver_elemornamentales"><br>
              <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="form-group col-md-3">
                    <label class="control-label">ESTADO CONSERVACION</label> 
                  </div>
                   <div class="form-group col-md-3">
                    <label class="control-label"> INTEGRIDAD:</label> 
                  </div>
                   <div class="form-group col-md-3">
                    <label class="control-label"> TIPO:</label> 
                  </div>   
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarEOr()"></i>
                  </a>
                  </div>
                  </small>
                  <div id="agrillaOrnamentalesA"></div>
                  </div>
                  <!--Fin grilla--> 
                 
              </div>
          <input type="checkbox" id="aver_elemtipocomp" name="aver_elemtipocomp" onchange="verPanelelemtipocompact();">Ver Elementos Tipologicos Compositivos<br><br>  
              <div id="apanelcver_elemtipocomp"> 
              <div class="col-md-12"> 
              <small>
              <div class="form-group col-md-3">
                <label class="control-label">ESTADO CONSERVACION</label> 
              </div>
               <div class="form-group col-md-3">
                <label class="control-label"> INTEGRIDAD:</label> 
              </div>
               <div class="form-group col-md-3">
                <label class="control-label"> TIPO:</label> 
              </div>
              <div class="pull-right">
              <a   type="button" style="cursor:pointer;">
              <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarETC()"></i>
              </a>
              </div>
              </small>
              <div id="agrillaTipologicosC"></div>
              </div>
              <!--Fin grilla-->     
              </div>
    </div>
    <input type="checkbox" id="aver_hogares" name="aver_hogares" onchange="verPanelhogaresact();">Ver Hogares<br>
              <div id="apanelcver_hogares"><br> 
                 <!--grilla-->
                  <div class="col-md-12"> 
                  <div class="form-group col-md-2">
                    <label class="control-label">  HOGAR DESC:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label"> EDAD:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label"> HOMBRE/MUJER:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label"> TENDENCIA:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label">DERECHO PROPIETAR:</label> 
                  </div>                  
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="actualizarhogar()"></i>
                  </a>
                  </div> 
                  </div>
                  <div class="col-md-12" id="agrillaHogares"></div>
                  <!--Fin grilla-->    
              </div>
    </div>
  </div> 
  
          <div class="col-md-12">
           <div class="form-group col-md-6">
             <label class="control-label">
             CATEGORÍA DEL INMUEBLE:
             </label>
             <input id="acategoria_inmueble" type="text" class="form-control" disabled=""> 
           </div>
           <div class="form-group col-md-6">
            <label class="control-label">
              PUNTUACIÓN:
            </label>
            <input id="apuntuacion" type="text" class="form-control" disabled> 
           </div>
          </div>
          <!-- fin FORMULARIO-->
          <div class="modal-footer">
            <button class="btn btn-primary"  type="button" onclick="volverUpdate2()"><i class="fa fa-close"></i> Cancelar</button>
            <button class="btn btn-primary" id="cancelar" onclick="volverUpdate()" type="button"><i class="fa fa-arrow-left" ></i> Volver</button>
            <button class="btn btn-primary" id="actualizarBienesInmuebles" type="button" onclick="validarUpdate2()" data-dismiss="modal">
              <i class="glyphicon glyphicon-pencil"></i> Modificar
            </button>
          </div> 
        </div>
      </div>
    </div>
    {!! Form::close() !!} 
    </div>
  </div>
</div>