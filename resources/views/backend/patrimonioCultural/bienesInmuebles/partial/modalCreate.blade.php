<div class="modal fade modal-default" id="myCreateBienesInmueble" data-backdrop="static" data-keyboard="false" tabindex="-1" rol="dialog" style="overflow-y: scroll;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical puestoMercado-validate','id'=>'formNuevaFicha_create'])!!}
      <div class="modal-header">
        <div class="row">
          <div class="col-md-12">
            <section class="panel panel-info">
               <div class="panel-heading">
                <h3>
                  Nueva Ficha Bienes Inmuebles
                  <small>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="refrescarPantalla()">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </small>
                </h3>
              </div>
            </section>
          </div>   
        </div>
      </div>

  <div  style="padding:20px">
    <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
    <input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">
    <input type="hidden" name="pagina1" id="pagina1" value="1">
     <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
        Creador:
      </label>
      <input id="ccreador_in" type="text" class="form-control" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        Fecha de creación:
      </label>
      <input id="cfecha_creador_in" type="text" class="form-control" disabled> 
    </div>
   </div> 
    <div class="col-md-12">
      <div class="panel-heading">
        <label class="control-label" style="color:#275c26";>
         1.IDENTIFICACIÓN:
       </label>
     </div>
   </div>
   <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
        Código Catastral:
      </label>
      <input id="ccodCatastral_in" type="text" class="form-control"  placeholder="000-0000-0000-0000"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Código Catastral Histórico 1:
     </label>
     <input id="ccodCatastral1_in" type="text" class="form-control"  placeholder="000-0000-0000-0000"> 
   </div>
 </div>
 <div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
     2.DENOMINACIÓN DEL INMUEBLE:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
    Original:
  </label>
  <input id="coriginal_in" type="text" class="form-control"  placeholder="Original"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
   Tradicional:
 </label>
 <input id="ctradicional_in" type="text" class="form-control"  placeholder="Tradicional"> 
</div>
</div>
   <div class="col-md-12">
     <div class="form-group col-md-12">
      <label class="control-label">
        Actual:
      </label>
      <input id="cActual_in" type="text" class="form-control"  placeholder="Actual"> 
    </div>
 </div>
 <div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
     3.LOCALIZACIÓN:
   </label>
 </div>
</div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Departamento:
      </label>
      <input id="cdepartamento_in" type="text" class="form-control"  value="La Paz" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Ciudad/Población:
     </label>
     <input id="cciudad_in" type="text" class="form-control"  value="La Paz" disabled> 
   </div>
</div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Municipio:
      </label>
      <input id="cmunicipio_in" type="text" class="form-control" value="La Paz" disabled> 
    </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      Macrodistrito:
    </label>
    <select class="form-control" id="cMacrodistrito_in" >
          <option value="-1"> Seleccionar...</option>
          <option value="MACRODISTRITO 1 COTAHUMA"> MACRODISTRITO 1 COTAHUMA</option> 
          <option value="MACRODISTRITO 2 MAXIMILIANO PAREDES">MACRODISTRITO 2 MAXIMILIANO PAREDES</option>
          <option value="MACRODISTRITO 3 PERIFERICA">MACRODISTRITO 3 PERIFERICA</option>
          <option value="MACRODISTRITO 4 SAN ANTONIO">MACRODISTRITO 4 SAN ANTONIO</option>  
          <option value="MACRODISTRITO 5 SUR">MACRODISTRITO 5 SUR</option>
          <option value="MACRODISTRITO 6 MALLASA">MACRODISTRITO 6 MALLASA</option>
          <option value="MACRODISTRITO 7 CENTRO">MACRODISTRITO 7 CENTRO</option>
          <option value="MACRODISTRITO 8 HAMPATURI">MACRODISTRITO 8 HAMPATURI</option>
          <option value="MACRODISTRITO 9 ZONGO">MACRODISTRITO 9 ZONGO</option>  
  </select>
  </div>
  <div class="form-group col-md-6">
    <label class="control-label">
     Tipo de Vía:
    </label>
    <select class="form-control" id="ctipoViaLocalizacion_in" >
          <option value="-1"> Seleccionar...</option>
          <option value="Avenida">Avenida</option> 
          <option value="Calle">Calle</option>
          <option value="Pasaje">Pasaje</option>
          <option value="Otro">Otro</option>
    </select>
  </div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
   Dirección:
 </label>
 <input id="cdireccion_in" type="text" class="form-control"  placeholder="Direccion"> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Entre Calles o Esquina:
 </label>
 <input id="ccalleEsquina_in" type="text" class="form-control"  placeholder="Entre Calles"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número1:
 </label>
 <input id="cnumero_in" type="text" class="form-control"  placeholder="Numero"> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   zona:
 </label>
 <input id="czona_in" type="text" class="form-control"  placeholder="Zona"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número2:
 </label>
 <input id="cnumero_in2" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número3:
 </label>
 <input id="cnumero_in3" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número4:
 </label>
 <input id="cnumero_in4" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número5:
 </label>
 <input id="cnumero_in5" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
 <div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
     4.REGIMEN DE PROPIEDAD:
   </label>
 </div>
</div>

<!-- Grilla Regimen -->
<div class="col-md-12"> 
  <small>
    <div class="pull-right">
      <a   type="button" style="cursor:pointer;">
        <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Regimen de la Propiedad" onclick="crearRegimenPropiedad();"></i>
      </a>
    </div>
  </small>
  <div id="regimenPropietario"></div>
</div>

<!-- fin Grilla -->


<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    5. MEDIDAS DEL PREDIO:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Área del Predio(m2):
 </label>
 <input id="careaPredio_in" type="text" class="form-control"  placeholder="Area"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Ancho Frente del Predio(m):
 </label>
 <input id="canchoFrente_in" type="text" class="form-control"  placeholder="Ancho"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Fondo del Predio(m):
 </label>
 <input id="cfondoPredio_in" type="text" class="form-control"  placeholder="Fondo"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Ancho Muro(m):
 </label>
 <input id="canchoMuro_in" type="text" class="form-control"  placeholder="Ancho Muro"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Altura Interior Promedio(m):
 </label>
 <input id="calturaInterior_in" type="text" class="form-control"  placeholder="Altura Interior"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Altura de Fachada Exterior (m):
 </label>
 <input id="calturaFachada_in" type="text" class="form-control"  placeholder="Altura Exterior"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
 Altura Máxima del Edificio:
 </label>
 <input id="calturaMaxima_in" type="text" class="form-control"  placeholder="Altura Maxima"> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    6. USO DE SUELO:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Tradicional:
 </label><br>
 <table width="100%">
 <tr>
 <td><input type="checkbox" id="tradicional1"> Comercio</td>
 <td><input type="checkbox" id="tradicional2"> Culto</td>
 <td><input type="checkbox" id="tradicional3"> Educación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="tradicional4"> Industria</td>
 <td><input type="checkbox" id="tradicional5"> Militar</td>
 <td><input type="checkbox" id="tradicional6"> Recreación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="tradicional7"> Salud</td>
 <td><input type="checkbox" id="tradicional8"> Servicio Público   </td>
 <td><input type="checkbox" id="tradicional9"> Sin Uso</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="tradicional10"> Socio Cultural</td>
 <td><input type="checkbox" id="tradicional11"> Taller</td>
 <td><input type="checkbox" id="tradicional12"> Vivienda</td>
 </tr>
 </table>


</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Actual:
 </label>
 <table width="100%">
 <tr>
 <td><input type="checkbox" id="actual1"> Comercio</td>
 <td><input type="checkbox" id="actual2"> Culto</td>
 <td><input type="checkbox" id="actual3"> Educación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="actual4"> Industria</td>
 <td><input type="checkbox" id="actual5"> Militar</td>
 <td><input type="checkbox" id="actual6"> Recreación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="actual7"> Salud</td>
 <td><input type="checkbox" id="actual8"> Servicio Público   </td>
 <td><input type="checkbox" id="actual9"> Sin Uso</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="actual10"> Socio Cultural</td>
 <td><input type="checkbox" id="actual11"> Taller</td>
 <td><input type="checkbox" id="actual12"> Vivienda</td>
 </tr> 
 </table>
</div>
</div>

<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Tendencia:
 </label>
 <table width="100%">
 <tr>
 <td><input type="checkbox" id="tendencia1"> Comercio</td>
 <td><input type="checkbox" id="tendencia2"> Culto</td>
 <td><input type="checkbox" id="tendencia3"> Educación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="tendencia4"> Industria</td>
 <td><input type="checkbox" id="tendencia5"> Militar</td>
 <td><input type="checkbox" id="tendencia6"> Recreación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="tendencia7"> Salud</td>
 <td><input type="checkbox" id="tendencia8"> Servicio Público   </td>
 <td><input type="checkbox" id="tendencia9"> Sin Uso</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="tendencia10"> Socio Cultural</td>
 <td><input type="checkbox" id="tendencia11"> Taller</td>
 <td><input type="checkbox" id="tendencia12"> Vivienda</td>
 </tr> 
 </table>
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    7. DESCRIPCIÓN DEL PREDIO:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Descripción:
 </label>
 <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripcion_in"></textarea> 
 
</div>

</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Ubicación de la Manzana:
 </label>
<select class="form-control" id="cubicacionManzana_in" >
          <option value="-1">Selecionar...</option> 
          <option value="Aislado"> Aislado</option> 
          <option value="Corazon de Manzana"> Corazón de Manzana</option>
          <option value="Esquinero"> Esquinero</option>
          <option value="Manzana Completa"> Manzana Completa</option>  
          <option value="Media Manzana"> Media Manzana</option>
          <option value="Medianero"> Medianero</option> 
          <option value="Solar"> Solar</option>   
  </select><br>
  <input type="checkbox" name="puntuacion" value="2" id="cpuntuacion_umcheck" onclick="capturarCheckPuntuacion()"> Ubicación de la Manzana
  <br>
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Línea de Construcción:
 </label>
<select class="form-control" id="clineaConstruccion_in" >
          <option value="-1">Selecionar...</option>  
          <option value="Alineada"> Alineada</option>
          <option value="Remetida"> Remetida</option>
          <option value="Sobresalida"> Sobresalida</option>   
  </select><br>
   <input type="checkbox" name="puntuacion" value="2" id="cpuntuacion_lccheck" onclick="capturarCheckPuntuacion()"> Linea de Construcción<br>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Trazo de Manzana:
 </label>
<select class="form-control" id="ctrazoManzana_in" >
          <option value="(Ninguno)"> Selecionar...</option> 
          <option value="Linea Municipal">Linea Municipal</option>
          <option value="Linea Patrimonial">Linea Patrimonial</option>
          
  </select><br> 
  <input type="checkbox" name="puntuacion" value="2" id="cpuntuacion_tmcheck" onclick="capturarCheckPuntuacion()"> Trazo de Manzana<br>
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Tipología Arquitectónica:
 </label>
 <select class="form-control" id="ctipologiaArqui_in" >
          <option value="(Ninguno)"> Seleccionar...</option> 
          <option value="Compacta"> Compacta</option>
          <option value="Compacta con Retiros"> Compacta con Retiros</option>
          <option value="Patio Central"> Patio Central</option>   
  </select><br> 
   <input type="checkbox" name="puntuacion" value="1" id="cpuntuacion_tacheck" onclick="capturarCheckPuntuacion()"> Tipología Arquitectónica<br>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Área Edificable de Crecimiento:
 </label>
 <select class="form-control" id="careaEdificable_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Tiene Area"> Tiene Area</option>
          <option value=" No tiene Area"> No tiene Area</option>  
  </select><br>
     <input type="checkbox" name="puntuacion" value="1" id="cpuntuacion_aeccheck" onclick="capturarCheckPuntuacion()"> Área Edificable de Crecimiento<br>
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Material Vía:
 </label>
 <select class="form-control" id="cmaterialVia_in" >
          <option value="(Ninguno)"> Seleccionar...</option> 
          <option value="Adoquín"> Adoquín</option>
          <option value="Asfalto"> Asfalto</option>
          <option value="Cemento"> Cemento</option>
          <option value="Empedrado"> Empedrado</option>
          <option value="Pavimento Rígido"> Pavimento Rígido</option>   
  </select> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Tipo de Vía:
 </label>
 <select class="form-control" id="ctipoVia_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Peatonal"> Peatonal</option>
          <option value="Vehicular"> Vehicular</option>  
  </select>
</div>
<div class="form-group col-md-6">
  <label  class="control-label">
   Bloques Inventariados:
 </label>
  <select class="form-control" id="cbloqueInventario_in" >
          <option value="1" selected> 1</option>
          <option value="2"> 2</option>
          <option value="3"> 3</option> 
          <option value="4"> 4</option> 
          <option value="5"> 5</option> 
  </select>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Bloques Reales:
 </label>
 <input id="cbloquesReales_in" type="text" class="form-control"  placeholder="Bloques Reales"> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
 Hogares Inventariados:
 </label>
 <input id="chogaresInventario_in" type="text" class="form-control"  placeholder="Hogares Inventariados"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Hogares Reales:
 </label>
 <input id="chogaresReales_in" type="text" class="form-control"  placeholder="Hogares Reales"> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Estilo General:
 </label>
 
 <select class="form-control" id="cestiloGeneral_in" >
            <option value="(Ninguno)">Seleccionar...</option> 
            <option value="Academicista">Academicista</option>
            <option value="Arq. Industrial">Arq. Industrial</option>
            <option value="Arq. Noveau">Arq. Noveau</option> 
            <option value="Arte Decó">Arte Decó</option>
            <option value="Barroco">Barroco</option>
            <option value="Barroco Mestizo">Barroco Mestizo</option>
            <option value="Brutalista">Brutalista</option>
            <option value="Californiano">Californiano</option>
            <option value="Clasicismo postmoderno">Clasicismo postmoderno</option>
            <option value="Clasicista">Clasicista</option>
            <option value="Colonial">Colonial</option>
            <option value="Constructivista">Constructivista</option>
            <option value="Contemporaneo">Contemporaneo</option>
            <option value="Cubista">Cubista</option>
            <option value="Ecléctico">Ecléctico</option>
            <option value="Estilo no definido">Estilo no definido</option>
            <option value="Expresionista">Expresionista</option>
            <option value="Funcionalista">Funcionalista</option>
            <option value="Georgiano">Georgiano</option>
            <option value="Georginiano">Georginiano</option>
            <option value="High Tech">High Tech</option>
            <option value="Historicista">Historicista</option>
            <option value="Internacional">Internacional</option>
            <option value="Minimalismo">Minimalismo</option>
            <option value="Moderno">Moderno</option>
            <option value="Mosarable">Mosarable</option>
            <option value="Mudéjar">Mudéjar</option>
            <option value="Neoclásico">Neoclásico</option>
            <option value="Neocolonial">Neocolonial</option>
            <option value="Neogótico">Neogótico</option>
            <option value="Neotiwanacota">Neotiwanacota</option>
            <option value="Neovernacular">Neovernacular</option>
            <option value="Organicista">Organicista</option>
            <option value="Postmoderno">Postmoderno</option>
            <option value="Racionalista">Racionalista</option>
            <option value="Renacentista">Renacentista</option>
            <option value="Republicano">Republicano</option>
            <option value="Tudor">Tudor</option> 
            <option value="Vernacular">Vernacular</option> 
            <option value="Victoriano">Victoriano</option> 
  </select> 

</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Rango de Época:
 </label>
 <select class="form-control" id="crangoEpoca_in" >
          <option value="(Ninguno)"> Seleccionar...</option> 
          <option value="1548-1825">1548-1825</option>
          <option value="1826-1850">1826-1850</option> 
          <option value="1851-1900">1851-1900</option>
          <option value="1901-1930">1901-1930</option>  
          <option value="1931-1960">1931-1960</option> 
          <option value="1961-1980">1961-1980</option> 
          <option value="1981-2000">1981-2000</option> 
          <option value="2000 en Adelante">2000 en Adelante</option>  
  </select>
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Estado de Conservación:
 </label>
 <select class="form-control" id="cestadoConserva_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Muy Bueno">Muy Bueno</option>
          <option value="Bueno">Bueno</option>
          <option value="Regular">Regular</option>   
          <option value="Malo">Malo</option>
          <option value="Muy Malo">Muy Malo</option>
          
  </select> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    II. DATOS ORALES Y DOCUMENTALES:
   </label>
 </div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    1. MARCO LEGAL:
   </label>
 </div>
</div>
<!--grilla-->
  <div class="col-md-12"> 
    <small>
    <div class="pull-right">
        <a   type="button" style="cursor:pointer;">
          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="crearArgumento();"></i>
        </a>
    </div>
    </small>
    <div id="argumentopintarCrear"></div><br><br>
    </div><br><br>

<!--Fin grilla-->

<!--grilla DOS-->
<br><br>
<center>
  <label class="control-label" style="color:#275c26";>
    Referencias Históricas
  </label>
</center>
<div class="col-md-12"> 
  <small>
    <div class="pull-right">
      <a   type="button" style="cursor:pointer;">
        <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nueva Referencia Historica" onclick="crearReferenciaHistorica();"></i>
      </a>
    </div>
  </small>
  <div id="referenciaHistorica"></div>
</div>
<!--Fin grilla-->

<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
   2. DATOS ORALES:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
Fecha de Construcción:
 </label>
 <input id="cfechaConstruccion_in" type="text" class="form-control" > 
  <br><input type="checkbox" name="puntuacion" value="2" id="cpuntuacion_fccheck" onclick="capturarCheckPuntuacion()"> Fecha de Construcción<br>
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Autor y/o Constructor:
 </label>
 <input id="cautor_in" type="text" class="form-control"  placeholder="Descripcion"> 
  <br><input type="checkbox" name="puntuacion" value="2" id="cpuntuacion_accheck" onclick="capturarCheckPuntuacion()"> Autor y/o Constructor<br>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Preexistencia de Edificación/ Intervenciones:
 </label>
 <textarea name="textarea" id="cpreexistencia_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Propietarios Anteriores:
 </label>
 <textarea name="textarea" id="cpropietarios_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Usos Originales:
 </label>
 <textarea name="textarea" id="cusoOriginal_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Hechos Históricos:
 </label>
 <textarea name="textarea" id="chechoHistorico_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Fiestas Religiosas::
 </label>
 <textarea name="textarea" id="cfiestaReligiosa_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Datos Históricos del Conjunto:
 </label>
 <textarea name="textarea" id="cdatoHistorico_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Fuentes:
 </label>
 <textarea name="textarea" id="cfuente_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
   3. DATOS DOCUMENTALES:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
Fecha de Construcción:
 </label>
 <input id="cfechaConstruccion_in3" type="text" class="form-control"  placeholder="Descripcion"> 
 <br>
     <input type="checkbox" name="puntuacion" value="2" id="cpuntuacion_fcdccheck" onclick="capturarCheckPuntuacion()"> Fecha de Construcción<br>
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Autor y/o Constructor:
 </label>
 <input id="cautor_in3" type="text" class="form-control"  placeholder="Descripcion"> 
 <br>
   <input type="checkbox" name="puntuacion" value="3" id="cpuntuacion_acdccheck" onclick="capturarCheckPuntuacion()"> Autor y/o Constructor<br>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Preexistencia de Edificación/ Intervenciones:
 </label>
 <textarea name="textarea" id="cpreexistencia_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Propietarios Anteriores:
 </label>
 <textarea name="textarea" id="cpropietarios_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Usos Originales:
 </label>
 <textarea name="textarea" id="cusoOriginal_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Hechos Históricos:
 </label>
 <textarea name="textarea" id="chechoHistorico_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Fiestas Religiosas::
 </label>
 <textarea name="textarea" id="cfiestaReligiosa_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Datos Históricos del Conjunto:
 </label>
 <textarea name="textarea" id="cdatoHistorico_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Fuentes:
 </label>
 <textarea name="textarea" id="cfuente_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
  4. INSCRIPCIONES:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Inscripciones:
 </label>
 <textarea name="textarea" id="cinscripcion_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Existencia de Patrimonio Mueble:
 </label>
 <textarea name="textarea" id="cexistePatrimonio_in" rows="5" cols="55" class="form-control "></textarea> 
  <br><input type="checkbox" name="puntuacion" value="1" id="cpuntuacion_epmcheck" onclick="capturarCheckPuntuacion()"> Existencia de Patrimonio Mueble<br>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Existencia de Restos Arqueológicos:
 </label>
 <textarea name="textarea" id="cexisteResto_in"  rows="5" cols="55" class="form-control "></textarea>
 <br><input type="checkbox" name="puntuacion" value="1" id="cpuntuacion_eracheck" onclick="capturarCheckPuntuacion()"> Existencia de Restos Arqueológicos<br> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Existencia de Elementos Naturales:
 </label>
 <textarea name="textarea" id="cexisteElemento_in" rows="5" cols="55" class="form-control "></textarea>
 <br><input type="checkbox" name="puntuacion" value="1" id="cpuntuacion_eencheck" onclick="capturarCheckPuntuacion()"> Existencia de Elementos Naturales<br>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Ritos, Mitos, Tradiciones y/o Otros:
 </label>
 <textarea name="textarea" id="critosMitos_in" rows="5" cols="55" class="form-control "></textarea>
 <br><input type="checkbox"  name="puntuacion" value="1" id="cpuntuacion_rmtocheck" onclick="capturarCheckPuntuacion()"> Ritos, Mitos, Tradiciones y/o Otros<br>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Conjunto:
 </label>
 <select class="form-control" id="cconjunto_in" >
          <option value="-1"> Seleccionar...</option>
          <option value="1. Conjunto Centro Historico (Macrodistrito Centro)">1. Conjunto Centro Historico (Macrodistrito Centro)</option>
          <option value="2. Conjunto San Sebastian (Macrodistrito Centro)">2. Conjunto San Sebastian (Macrodistrito Centro)</option> 
          <option value="3. Conjunto San Francisco (Macrodistrito Centro y Cotahuma)">3. Conjunto San Francisco (Macrodistrito Centro y Cotahuma)</option> 
          <option value="4. Conjunto Ismael Montes (Macrodistrito Centro y Periferica)">4. Conjunto Ismael Montes (Macrodistrito Centro y Periferica)</option>
          <option value="5. Conjunto Plaza Isabel La Catolica (Macrodistrito Centro)">5. Conjunto Plaza Isabel La Catolica (Macrodistrito Centro)</option> 
          <option value="6. Conjunto Rosendo Gutierrez (Macrodistrito Cotahuma)">6. Conjunto Rosendo Gutierrez (Macrodistrito Cotahuma)</option> 
          <option value="7. Conjunto Obrajes (Macrodistrito Sur)">7. Conjunto Obrajes (Macrodistrito Sur)</option>
          <option value="8. Conjunto San Pedro (Macrodistrito Cotahuma)">8. Conjunto San Pedro (Macrodistrito Cotahuma)</option> 
          <option value="9. Conjunto Luis Villanueva (Macrodistrito Centro)">9. Conjunto Luis Villanueva (Macrodistrito Centro)</option> 
          <option value="10. Conjunto Casas Alborta - Vickers (Macrodistrito Cotahuma)">10. Conjunto Casas Alborta - Vickers (Macrodistrito Cotahuma)</option>
          <option value="11. Conjunto Francisco Bedregal(Macrodistrito Cotahuma)">11. Conjunto Francisco Bedregal(Macrodistrito Cotahuma)</option> 
          <option value="12. Conjunto Plaza Riosinho (Macrodistrito Periferica)">12. Conjunto Plaza Riosinho (Macrodistrito Periferica)</option>
          <option value="13. Conjunto Capitan Castrillo (Macrodistrito Cotahuma)">13. Conjunto Capitan Castrillo (Macrodistrito Cotahuma)</option> 
          <option value="14. Conjunto Agustin Aspiazu (Macrodistrito Cotahuma)">14. Conjunto Agustin Aspiazu (Macrodistrito Cotahuma)</option>
          <option value="15. Conjunto Heriberto Gutierrez(Macrodistrito Centro)">15. Conjunto Heriberto Gutierrez(Macrodistrito Centro)</option> 
          <option value="16. Conjunto Monticulo (Macrodistrito Cotahuma)">16. Conjunto Monticulo (Macrodistrito Cotahuma)</option>  
          <option value="17. Cementerio General (Macrodistrito Maximiliano Paredes)">17. Cementerio General (Macrodistrito Maximiliano Paredes)</option>   
          <option value="1. Area Capitan Castrillo (Macrodistrito Cotahuma)">1. Area Capitan Castrillo (Macrodistrito Cotahuma)</option> 
          <option value="2. Area Agustin Aspiazu (Macrodistrito Cotahuma)">2. Area Agustin Aspiazu (Macrodistrito Cotahuma)</option>
          <option value="3. Area Heriberto Gutierrez(Macrodistrito Cotahuma)">3. Area Heriberto Gutierrez(Macrodistrito Cotahuma)</option> 
          <option value="4. Area Monticulo (Macrodistrito Cotahuma)">4. Area Monticulo (Macrodistrito Cotahuma)</option>  
     
  </select>
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Estado de Verificación:
 </label>
 <select class="form-control" id="cestadoVerifica_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Aprobado">Aprobado</option>
          <option value="No Verificado">No Verificado</option> 
          <option value="Observado">Observado</option>  
  </select>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
Puntuación:
 </label>
 <input id="cpuntuacion_in" type="number" class="form-control" value ="0" placeholder="0" disabled> 
</div>
</div>
<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" type="button" onclick="refrescarPantalla()"><i class="fa fa-close"></i> Cancelar</button>
<button class="btn btn-default "  type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
 <button type="button" class="btn btn-primary"  onclick="validar1()"> <i class="glyphicon fa fa-save"></i> Guardar / Continuar</button>        
</div>
</div>

{!! Form::close() !!}
</div>
</div>
</div>


<script type="text/javascript">




  function verPanelAcapites(){
 
  $("input[name=cver_acapites]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelVerAcapites").style = "display:block;";
       console.log('si');
    }else{
         document.getElementById("panelVerAcapites").style = "display:none;";
        
      console.log('no');
    }
  });
 };

function verPanelElemestructurales()
{
   $("input[name=cver_elemestructurales]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelcver_elemestructurales").style = "display:block;";
       console.log('si');
    }else{
      
        document.getElementById("panelcver_elemestructurales").style = "display:none;";
        
      console.log('no');
    }
  });
}
function verPanelelemcompositivos()
{
   $("input[name=cver_elemcompositivos]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelcver_elemcompositivos").style = "display:block;";
       console.log('si');
    }else{
      document.getElementById("panelcver_elemcompositivos").style = "display:none;";
      
      console.log('no');
    }
  });
}

function verPanelacabados(){
   $("input[name=cver_acabados]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelcver_acabados").style = "display:block;";
       console.log('si');
    }else{
      document.getElementById("panelcver_acabados").style = "display:none;";
      
      console.log('no');
    }
  });
}

function verPanelelemartisticoscompo () {
   $("input[name=cver_elemartisticoscompo]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelcver_elemartisticoscompo").style = "display:block;";
       console.log('si');
    }else{
      document.getElementById("panelcver_elemartisticoscompo").style = "display:none;";
      
      console.log('no');
    }
  });
}

function verPanelelemornamentales () {
   $("input[name=cver_elemornamentales]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelcver_elemornamentales").style = "display:block;";
       console.log('si');
    }else{
      document.getElementById("panelcver_elemornamentales").style = "display:none;";
      
      console.log('no');
    }
  });
}

function verPanelelemtipocomp () {
   $("input[name=cver_elemtipocomp]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelcver_elemtipocomp").style = "display:block;";
       console.log('si');
    }else{
      document.getElementById("panelcver_elemtipocomp").style = "display:none;";
      
      console.log('no');
    }
  });
}

function verPanelhogares() {
   $("input[name=cver_hogares]").each(function (index) {  
    if($(this).is(':checked')){
      document.getElementById("panelcver_hogares").style = "display:block;";
       console.log('si');
    }else{
      document.getElementById("panelcver_hogares").style = "display:none;";
        
      console.log('no');
    }
  });
}
  

 

</script>



