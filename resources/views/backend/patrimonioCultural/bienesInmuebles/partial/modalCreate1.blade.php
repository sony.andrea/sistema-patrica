<div class="modal fade modal-default" id="myCreateBienesInmueble1" data-backdrop="static" data-keyboard="false" tabindex="-1" rol="dialog" style="overflow-y: scroll;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical puestoMercado-validate','id'=>'formNuevaFicha_create'])!!}
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h3>
               Nueva Ficha Bienes Inmuebles (Bloque)
              <small>
                <button type="button" class="close" onclick="volver2();" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h3>
          </div>
        </section>
      </div>   
    </div>
  </div>
  <div  style="padding:20px">
    <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
    <input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">
     <input type="hidden" name="pagina2" id="pagina2" value="2">
    <div class="col-md-12">
      <div class="panel-heading">
        <label class="control-label" style="color:#275c26";>
          DESCRIPCIÓN DEL BLOQUE:
       </label>
     </div>
   </div>

<!-- FORMULARIO 2 MERITZA-->
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      Id. Bloque:
    </label>
      <input id="cid_bloque" type="text" class="form-control"  value="1" disabled> 
  </div>
<div class="form-group col-md-6">
  <label class="control-label">
    Descripción:
  </label>
    <input id="cdescripcion" type="text" class="form-control"  placeholder="Descripción"> 
</div>
  </div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    1. ÉPOCA Y ESTILO:
    </label>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      Código Catastral:
    </label>
    <input id="ccodigo_catastral" type="text" class="form-control"  placeholder="Código Catastral" disabled> 
  </div>
  <div class="form-group col-md-6">
    <label class="control-label">
    Rango de Época:
    </label>
  <select class="form-control" id="crango_epoca" disabled="">
          <option value="(Ninguno)"> No seleccionado</option> 
          <option value="1548-1825">1548-1825</option>
          <option value="1826-1850">1826-1850</option> 
          <option value="1851-1900">1851-1900</option>
          <option value="1901-1930">1901-1930</option>  
          <option value="1931-1960">1931-1960</option> 
          <option value="1961-1980">1961-1980</option> 
          <option value="1981-2000">1981-2000</option> 
          <option value="2000 en Adelante">2000 en Adelante</option>
  </select>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
     Estilo Bloque:
    </label>
    
      <select class="form-control" id="cestilo_bloque" disabled="">
            <option value="(Ninguno)"> No seleccionado</option> 
            <option value="Academicista">Academicista</option>
            <option value="Arq. Industrial">Arq. Industrial</option>
            <option value="Arq. Noveau">Arq. Noveau</option> 
            <option value="Arte Decó">Arte Decó</option>
            <option value="Barroco">Barroco</option>
            <option value="Barroco Mestizo">Barroco Mestizo</option>
            <option value="Brutalista">Brutalista</option>
            <option value="Californiano">Californiano</option>
            <option value="Clasicismo postmoderno">Clasicismo postmoderno</option>
            <option value="Clasicista">Clasicista</option>
            <option value="Colonial">Colonial</option>
            <option value="Constructivista">Constructivista</option>
            <option value="Contemporaneo">Contemporaneo</option>
            <option value="Cubista">Cubista</option>
            <option value="Ecléctico">Ecléctico</option>
            <option value="Estilo no definido">Estilo no definido</option>
            <option value="Expresionista">Expresionista</option>
            <option value="Funcionalista">Funcionalista</option>
            <option value="Georgiano">Georgiano</option>
            <option value="Georginiano">Georginiano</option>
            <option value="High Tech">High Tech</option>
            <option value="Historicista">Historicista</option>
            <option value="Internacional">Internacional</option>
            <option value="Minimalismo">Minimalismo</option>
            <option value="Moderno">Moderno</option>
            <option value="Mosarable">Mosarable</option>
            <option value="Mudéjar">Mudéjar</option>
            <option value="Neoclásico">Neoclásico</option>
            <option value="Neocolonial">Neocolonial</option>
            <option value="Neogótico">Neogótico</option>
            <option value="Neotiwanacota">Neotiwanacota</option>
            <option value="Neovernacular">Neovernacular</option>
            <option value="Organicista">Organicista</option>
            <option value="Postmoderno">Postmoderno</option>
            <option value="Racionalista">Racionalista</option>
            <option value="Renacentista">Renacentista</option>
            <option value="Republicano">Republicano</option>
            <option value="Tudor">Tudor</option> 
            <option value="Vernacular">Vernacular</option> 
            <option value="Victoriano">Victoriano</option> 
    </div>
  <div class="form-group col-md-6">
  <br>
    <input type="checkbox" name="cpuntuacion2" id="cepocaestilocheck" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Epoca y Estilo<br>
    <input type="checkbox" name="cpuntuacion2" id="csistemaconstructivo" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Sistema Constructivo<br>
     <!--<input type="checkbox" name="cpuntuacion2" id="cestadoconserva" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Estado de Conservación<br>-->     
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      Sistema Constructivo:
    </label>
    <select class="form-control" id="csistema_constructivo_insertar">
      <option value="-1">Seleccionar...</option> 
      <option value="Sistema Independiente">Sistema Independiente</option>
      <option value="Sistema Mixto">Sistema Mixto</option> 
      <option value="Sistema Portante">Sistema Portante</option>
    </select>
  </div>
</div> 
   <!--INICIO GRILLAS 1-->
  <div class="col-md-12"> 
    <div class="form-group col-md-12">
      <label class="control-label ">
          Peligros Potenciales:
        </label>
        <small>
          <div class="pull-right">
            <a   type="button" style="cursor:pointer;">
              <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Peligros Potenciales" onclick="crearPeligrosPotenciales();"></i>
            </a>
          </div>
        </small>
       <div id="peligrospotenciales"></div>
    </div>
  </div>
<!--FIN GRILLAS 1-->
<!--INICIO GRILLAS 2-->
  <div class="col-md-12"> 
    <div class="form-group col-md-12">
      <label class="control-label ">
           Patologías Edificación:
        </label>
        <small>
          <div class="pull-right">
            <a   type="button" style="cursor:pointer;">
              <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Patologías Edificación" onclick="crearPatologiaEdificacion();"></i>
            </a>
          </div>
        </small>
       <div id="patologiaedificacion"></div>
    </div>
  </div>

<!--FIN GRILLAS 2-->

<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
      3. DATOS ADICIONALES:
    </label>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      Nro. de Viviendas:
    </label>
      <input id="cnro_viviendas" type="number" class="form-control"  placeholder="Nro. de Viviendas"> 
  </div>
  <div class="form-group col-md-6">
    <label class="control-label">
      Nro. de Hogares:
    </label>
      <input id="cnro_hogares" type="number" class="form-control"  placeholder="Nro. de Hogares:"> 
  </div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
      4. RESUMEN:
    </label>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-2">
    <label class="control-label">
      Características Técnicas
    </label>
  </div><br>
  <div class="form-group col-md-10">
     <input type="checkbox" name="cpuntuacion2" id="cacapites1" value="8" onclick="CheckPuntuacionBloqueRegistrar()">Elementos Estructurantes (8%)<br>
     <input type="checkbox" name="cpuntuacion2" id="cacapites2" value="6" onclick="CheckPuntuacionBloqueRegistrar()">Elementos Compositivos (6%)<br>
     <input type="checkbox" name="cpuntuacion2" id="cacapites3" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Acabados (3%)<br>
     <input type="checkbox" name="cpuntuacion2" id="cacapites4" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Elementos Artisiticos Compositivos (3%)<br>
     <input type="checkbox" name="cpuntuacion2" id="cacapites5" value="4" onclick="CheckPuntuacionBloqueRegistrar()">Elementos Ornamentales (4%)<br>
     <input type="checkbox" name="cpuntuacion2" id="cacapites6" value="9" onclick="CheckPuntuacionBloqueRegistrar()">Elementos Tipologicos Compositivos (9%)<br>
  </div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
      VALORACIÓN:
    </label>
  </div>
</div>
<div lass="col-md-12">
  <div class="form-group col-md-2">
    <label class="control-label">
      Indicador Histórico (10% / 20%)
    </label>
    </div><br>
    <div class="form-group col-md-10">
      <input type="checkbox" name="cpuntuacion2" id="cindicador_historico1" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que posee un valor testimonial y documental, que ilustra el desarrollo político, social, religioso, cultural, económico y la forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_historico2" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que desde su creación se constituye en un hito en la memoria historica.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_historico3" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que en el transcurso del tiempo se constituyo en hito en la memoria historica, al ser escenario relacionado con personas o eventos importantes, dentro del proceso Histórico,social, cultural, económico de las comunidades.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_historico4" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que forma parte de un conjunto Histórico.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_historico5" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que posee elementos arqueológicos o testimonios arquitectonicos de construcciones preexistentes que documentan su evolución y desarrollo.<br>
    </div>
</div> 
<div class="col-md-12">
  <div class="form-group col-md-2">
    <label class="control-label">
      Indicador Artístico (6% / 15%)
    </label>
  </div><br>
  <div class="form-group col-md-10">
    <input type="checkbox" name="cpuntuacion2" id="cindicador_artistico1" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que se constituye en un ejemplo sobresaliente por su singularidad arquitectónica/artística.<br>
    <input type="checkbox" name="cpuntuacion2" id="cindicador_artistico2" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que conserva elementos arquitectónicos y tradicionales de interés<br>
    <input type="checkbox" name="cpuntuacion2" id="cindicador_artistico3" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble poseedor de expresiones artísticas y decorativas de interés.<br>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-2">
    <label class="control-label">
      Indicador Arquitectónico(4% / 15%)
    </label>
  </div><br>
  <div class="form-group col-md-10">
    <input type="checkbox" name="cpuntuacion2" id="cindicador_arquitectonico1" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble poseedor de características Tipológicas representativas de estilos arquitectónicos.<br>
    <input type="checkbox" name="cpuntuacion2" id="cindicador_arquitectonico2" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalles.<br>
  </div>
</div>
  <div class="col-md-12">
    <div class="form-group col-md-2">
      <label class="control-label">
       Indicador Tecnológico (6% / 13%)
      </label>
      </div><br>
      <div class="form-group col-md-10">
      <input type="checkbox" name="cpuntuacion2" id="cindicador_teclogico1" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que se constituye en un exponente de las técnicas constructivas y uso de materiales característicos de una época o región determinada.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_teclogico2" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble donde la aplicación de técnicas constructivas son singulares o de especial interés.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_teclogico3" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmuebles con sistemas constructivos y elementos arquitectónicos realizados por mano de obra especializada.<br>
  </div>
</div>
  <div class="col-md-12">
    <div class="form-group col-md-2">
      <label class="control-label">
        Indicador Integridad (4% / 15%)
      </label>
    </div><br>
    <div class="form-group col-md-10">
      <input type="checkbox" name="cpuntuacion2" id="cindicador_integridad1" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que conserva el total de su tipología, materiales y técnicas constructivas originales.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_integridad2" value="2" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble o espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_integridad3" value="0" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que ha sufrido intervenciones irreversibles que han modificado parcialmente la integridad del mismo.<br>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-2">
      <label class="control-label">
        Indicador Urbano (9% / 15%)
      </label>
    </div><br>
    <div class="form-group col-md-10">
      <input type="checkbox" name="cpuntuacion2" id="cindicador_urbano1" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que contribuye a definir un entorno de valor por su configuración y calidad en su estructura urbanística, el paisaje y/o el espacio público.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_urbano2" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble que forma un perfil homogéneo y/ó armónico.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_urbano3" value="3" onclick="CheckPuntuacionBloqueRegistrar()">Inmueble considerado hito de referencia por su emplazamiento.<br>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-2">
      <label class="control-label">
        Indicador Inmaterial
      </label>
    </div><br>
    <div class="form-group col-md-10">
      <input type="checkbox" name="cpuntuacion2" id="cindicador_inmaterial1" value="2" onclick="CheckPuntuacionBloqueRegistrar()">INDICADOR INMATERIAL (2% / 7%) Inmueble relacionado con la organización social, forma de vida, usos, representaciones, expresiones, conocimientos y técnicas que las comunidades y grupos sociales reconocen como parte de patrimonio.<br>
      <input type="checkbox" name="cpuntuacion2" id="cindicador_inmaterial2" value="1" onclick="CheckPuntuacionBloqueRegistrar()"> INDICADOR SIMBÓLICO (1% / 5%) Inmueble o espacio abierto único que expresa una significación cultural, representa identidad y pertenencia para el colectivo. <br>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        Indicador Histórico:
      </label>
      <br>
      <textarea name="textarea" id="cindicador_historico"  rows="5" cols="55" class="form-control barraTextarea"></textarea> 
    </div>
  </div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Indicador Artístico:
    </label>
    <textarea name="textarea" id="cindicador_atistico"  rows="5" cols="55" class="form-control barraTextarea"></textarea> 
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Indicador Arquitectónico:
    </label>
    <textarea name="textarea" id="cindicador_arquitectonico" rows="5" cols="55" class="form-control barraTextarea"></textarea> 
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Indicador Tecnológico:
    </label>
    <br>
    <textarea name="textarea" id="cindicador_teclogico" rows="5" cols="55" class="form-control barraTextarea"></textarea> 
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Indicador de Integridad:
    </label>
    <br>
    <textarea name="textarea" id="cindicador_integrid" rows="5" cols="55" class="form-control barraTextarea"></textarea> 
  </div>
</div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        Indicador Urbano:
      </label>
      <br>
      <textarea name="textarea" id="cindicador_urbano"  rows="5" cols="55" class="form-control barraTextarea"></textarea>
    </div>
  </div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
    Indicador Inmaterial:
    </label>
    <br>
    <textarea name="textarea" id="cindicador_inmaterial" rows="5" cols="55" class="form-control barraTextarea"></textarea> 
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      Indicador Simbólico:
    </label>
    <br>
    <textarea name="textarea" id="cindicador_simbolico"  rows="5" cols="55" class="form-control barraTextarea"></textarea>
  </div>   
</div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        Descripción Interior:
      </label>
      <br>
      <textarea name="textarea" id="cindicador_interior" rows="5" cols="55" class="form-control barraTextarea"></textarea> 
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        Descripción Exterior:
      </label>
      <br>
      <textarea name="textarea" id="cdescripcion_exterior"  rows="5" cols="55" class="form-control barraTextarea"></textarea> 
    </div>
  </div>
 <!-- <div class="col-md-12">f
    <div class="form-group col-md-6">
      <input type="checkbox" id="cver_servicios">Ver Servicios<br>
      <input type="checkbox" id="cver_acapites">Ver Acapites<br>
      <input type="checkbox" id="cver_hogares">Ver Hogares<br>
      <input type="checkbox" id="ccaldular" onclick="Puntuacion()">CALCULAR<br>
    </div>
  </div>-->
   <div class="col-md-12">
    <div class="form-group col-md-12">
     <!--NANCY-->
      <input type="checkbox" id="cver_servicios" name="cver_servicios" onchange="verPanelServicios();">Ver Servicios<br>
     <!-- <label class="control-label">Ver Servicios</label>-->
      <div id="panelcver_servicios">
        <!--INICIO GRILLAS 3 Instalaciones Estado-->
            <div class="col-md-12">
              <div class="panel-heading">
                <label class="control-label" style="color:#275c26";>
                  Instalaciones Estado
                </label>
              </div>
            </div>
            <div class="col-md-12"> 
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Instalaciones Estado" onclick="crearInstalacionesEstado();"></i>
                  </a>
                </div>
              </small>
             <div id="cinstalacionesestado"></div>
            </div>
          <!--FIN GRILLAS 3-->

          <!--INICIO GRILLAS 4 Servicios-->
            <div class="col-md-12">
              <div class="panel-heading">
                <label class="control-label" style="color:#275c26";>
                 Servicios
                </label>
              </div>
            </div>
            <div class="col-md-12"> 
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Servisios" onclick="crearServicios();"></i>
                  </a>
                </div>
              </small>
             <div id="cservicios"></div>
            </div>
          <!--FIN GRILLAS 4-->
          </div>
     <!--NANCY-->
      <input type="checkbox" id="cver_acapites" name="cver_acapites" onchange="verPanelAcapites();">Ver Características Técnicas<br>
      <div id="panelVerAcapites">
          <input type="checkbox" id="cver_elemestructurales" name="cver_elemestructurales" onchange="verPanelElemestructurales();">Ver Elementos Estructurales<br>
              <div id="panelcver_elemestructurales">
                  <!--grilla NANCY-->
                 <br>
            <div class="col-md-12"> 
              <div class="form-group col-md-12">
              <h4>
                <center>Paredes Exteriores</center>
              </h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Paredes Exteriores" onclick="crearParedesExteriores();"></i>
                  </a>
                </div>
              </small>
             <div id="cparedesexteriores"></div>             
             </div>
            </div>
          <!--FIN GRILLAS 5-->

           <!--INICIO GRILLAS 6 Cobertura del Techo-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Cobertura del Techo</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Cobertura del Techo" onclick="crearCoberturaTecho();"></i>
                  </a>
                </div>
              </small>
             <div id="ccoberturadeltecho"></div>
            </div>
          <!--FIN GRILLAS 6-->

          <!--INICIO GRILLAS 7 Paredes Interiores-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Paredes Interiores</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Paredes Interiores" onclick="crearParedesInteriores();"></i>
                  </a>
                </div>
              </small>
             <div id="cparedesinteriores"></div>
            </div>
          <!--FIN GRILLAS 7-->
          <!--INICIO GRILLAS 8 Entre Pisos-->
            <br>
            <br>
            <div class="col-md-12"> 
              <strong><h4><center>Entre Pisos</center></h4>
              <hr></strong>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Entre Pisos" onclick="crearEntrePisos();"></i>
                  </a>
                </div>
              </small>
             <div id="centrepisos"></div>
            </div>
          <!--FIN GRILLAS 8-->

          <!--INICIO GRILLAS 9 Escaleras-->


            <br>
            <br>
            <div class="col-md-12">
              <h4><center>Escaleras</center></h4> 
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Escaleras" onclick="crearEscalera();"></i>
                  </a>
                </div>
              </small>
             <div id="cescaleras"></div>
            </div>
          <!--FIN GRILLAS 9-->

          <!--INICIO GRILLAS 10 Cielo-->
            <br>
            <br>
            <div class="col-md-12">
              <h4><center>Cielos</center></h4> 
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Cielo" onclick="crearCielo();"></i>
                  </a>
                </div>
              </small>
             <div id="ccielo"></div>
            </div>
          <!--FIN GRILLAS 10-->
          <!--INICIO GRILLAS 11 Balcones-->
            <br>
            <br>
            <div class="col-md-12">
              <h4><center>Balcones</center></h4> 
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Balcones" onclick="crearBalcones();"></i>
                  </a>
                </div>
              </small>
             <div id="cbalcones"></div>
            </div>
          <!--FIN GRILLAS 11-->



          <!--INICIO GRILLAS 12 Estructura Cubierta-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Estructura Cubierta</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Estructura Cubierta" onclick="crearEstructuraCubierta();"></i>
                  </a>
                </div>
              </small>
             <div id="cestructuracubierta"></div>
            </div>
          <!--FIN GRILLAS 12-->

          <!--INICIO GRILLAS 13 Estructura Cubierta-->
            <br>
            <br>
            <div class="col-md-12"> 
              <h4><center>Pisos</center></h4>
              <hr>
              <small>
                <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                    <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Cielo" onclick="crearPisos();"></i>
                  </a>
                </div>
              </small>
             <div id="cpisos"></div>
            </div>

                  <!--Fin grilla NANCY-->
                  <!--Fin grilla NANCY-->
              </div>
          <input type="checkbox" id="cver_elemcompositivos" name="cver_elemcompositivos" onchange="verPanelelemcompositivos();">Ver Elementos Composisitivos<br>
              <div id="panelcver_elemcompositivos">
                
                 <!--TANYA-->
                  <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Ventanas Interiores
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcoVanosVentanasInteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcoVanosVentanasInteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Ventanas Interiores
                      </label>
                    </div>
                  </div>
                   <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcoVentanasInteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcoVentanasInteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Ventanas Exteriores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcoVanosVentanasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcoVanosVentanasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Ventanas Exteriores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcoVentanasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcoVentanasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Puertas Interiores
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcoVanosPuertasInteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcoVanosPuertasInteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Vanos Puertas Exteriores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcoVanosPuertasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcoVanosPuertasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Puertas Interiores
                      </label>
                    </div>
                  </div>
                       <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcpPuertasInteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcpPuertasInteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Puertas Exteriores
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcpPuertasExteriores();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcpPuertasExteriores"></div>
                  </div>
                  <!--Fin grilla-->
                   <div class="col-md-12">
                    <div class="panel-heading" align = "center">
                      <label class="control-label" style="color:#275c26";>
                        Rejas
                      </label>
                    </div>
                  </div>
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="elcpRejas();"></i>
                  </a>
                  </div>
                  </small>
                  <div id="divelcpRejas"></div>
                  </div>
                  <!--Fin grilla-->
                    <!--FIN TANYA-->
                
              </div>
          <input type="checkbox" id="cver_acabados" name="cver_acabados" onchange="verPanelacabados();">Ver Acabados<br>
               <div id="panelcver_acabados">
                  <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Pinturas Muros Exteriores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="acaPinturasMurosExteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <br>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="divacaPinturasMurosExteriores" class="form-group col-md-12"></div>
                    <br><br>
                  </div><br><br>
                  <!--Fin grilla--> 
                  <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Pinturas Muros Interiores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="acaPinturasMurosInteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="acaPinturasMurosInteriores" class="form-group col-md-12"></div>
                  </div><br><br>
                  <!--Fin grilla--> 
                  <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Acabados de Muros Exteriores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="acaAcabadosMurosExteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="divacaAcabadosMurosExteriores"></div>
                  </div><br><br>
                  <!--Fin grilla--> 
                  <!--grilla-->
                  <center>
                    <label class="control-label" style="color:#275c26";>
                      Acabados de Muros Interiores
                    </label>
                  </center>
                  <div class="col-md-12"> 
                    <small>
                      <div class="pull-right">
                        <a   type="button" style="cursor:pointer;">
                          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="acaAcabadosMurosInteriores();"></i>
                        </a>
                      </div>
                    </small>
                    <div class="form-group col-md-12">
                      <div class="col-md-4"> 
                        <strong>Estado de Conservación </strong> 
                      </div>
                      <div class="col-md-3">
                        <strong>Integridad </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Tipo </strong> 
                      </div>
                      <div class="col-md-2"> 
                        <strong>Materiales </strong> 
                      </div>
                      <div class="col-md-1"> 
                      </div>
                    </div>
                    <div id="divacaAcabadosMurosInteriores"></div>
                  </div>
                  <!--Fin grilla-->  
                
               </div>

          <input type="checkbox" id="cver_elemartisticoscompo" name="cver_elemartisticoscompo" onchange="verPanelelemartisticoscompo();">Ver Elementos Artísticos Compositivos<br>
              <div id="panelcver_elemartisticoscompo"><br>
                  <div class="col-md-12"> 
                  <small>
                  <div class="form-group col-md-4">
                    <label class="control-label">ESTADO CONSERVACION</label> 
                  </div>
                   <div class="form-group col-md-4">
                    <label class="control-label"> INTEGRIDAD:</label> 
                  </div>
                   <div class="form-group col-md-3">
                    <label class="control-label"> TIPO:</label> 
                  </div>                        
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="crearART()"></i>
                  </a>
                  </div>
                  </small>
                  <div id="grillaArtisiticoC"></div>
                  </div>
                  <!--Fin grilla-->
                
              </div>
          <input type="checkbox" id="cver_elemornamentales" name="cver_elemornamentales" onchange="verPanelelemornamentales();">Ver Elementos Ornamentales<br>
              <div id="panelcver_elemornamentales"><br>
                 <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="form-group col-md-4">
                    <label class="control-label">ESTADO CONSERVACION</label> 
                  </div>
                   <div class="form-group col-md-4">
                    <label class="control-label"> INTEGRIDAD:</label> 
                  </div>
                   <div class="form-group col-md-3">
                    <label class="control-label"> TIPO:</label> 
                  </div>   
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="crearORA()"></i>
                  </a>
                  </div>
                  </small>
                  <div id="grillaOrnamentalesA"></div>
                  </div>
                  <!--Fin grilla-->
                
              </div>
          <input type="checkbox" id="cver_elemtipocomp" name="cver_elemtipocomp" onchange="verPanelelemtipocomp();">Ver Elementos Tipologicos Compositivos<br><br>  
              <div id="panelcver_elemtipocomp"> 
                <!--grilla-->
                  <div class="col-md-12"> 
                  <small>
                  <div class="form-group col-md-4">
                    <label class="control-label">ESTADO CONSERVACION</label> 
                  </div>
                   <div class="form-group col-md-4">
                    <label class="control-label"> INTEGRIDAD:</label> 
                  </div>
                   <div class="form-group col-md-3">
                    <label class="control-label"> TIPO:</label> 
                  </div>
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="crearTIC()"></i>
                  </a>
                  </div>
                  </small>
                  <div id="grillaTipologicosC"></div>
                  </div>
                  <!--Fin grilla-->

                
              </div>
      </div>
      <input type="checkbox" id="cver_hogares" name="cver_hogares" onchange="verPanelhogares();">Ver Hogares<br>
              <div id="panelcver_hogares"><br> 
                  <!--grilla-->
                  <div class="col-md-12"> 
                  <div class="form-group col-md-2">
                    <label class="control-label">  HOGAR DESC:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label"> EDAD:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label"> HOMBRE/MUJER:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label"> TENDENCIA:</label> 
                  </div>
                   <div class="form-group col-md-2">
                    <label class="control-label">DERECHO PROPIETAR:</label> 
                  </div>                  
                  <div class="pull-right">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="crearhogar()"></i>
                  </a>
                  </div> 
                  </div>
                  <div class="col-md-12" id="grillaHogares"></div>
                  <!--Fin grilla--> 
                
              </div>
      <br>
    </div>
  </div> 
  

  <div class="col-md-12">
    <div class="form-group col-md-6">
      <label class="control-label">
        CATEGORÍA DEL INMUEBLE:
      </label>
      <input id="ccategoria_inmueble" value="D" type="text" class="form-control" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        PUNTUACIÓN:
      </label>
      <input id="cpuntuacion" value="0" type="text" class="form-control" placeholder = "0"  disabled> 
    </div>
  </div>
<!-- FIN FORMULARIO 2-->

<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" type="button" onclick="refrescarPantalla()"><i class="fa fa-close"></i> Cancelar</button>
<button class="btn btn-primary" id="cancelar" onclick="volver2()" type="button"><i class="fa fa-arrow-left" ></i> Volver</button>
  <button class="btn btn-default "  type="reset">
    <i class="fa fa-eraser"></i> Limpiar
  </button>
  <button type="button" class="btn btn-primary" id="registrarBienesInmuebles" onclick="validar2()" data-dismiss="modal"><i class="fa fa-save"></i></i> Guardar / Continuar</button>      
</div> 
</div>
{!! Form::close() !!}
</div>
</div>
</div>



