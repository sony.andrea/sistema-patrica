<div class="modal fade modal-default" id="myUpdateBienesInmueble" data-backdrop="static" data-keyboard="false" tabindex="-1" rol="dialog" style="overflow-y: scroll;">
  <div class="modal-dialog modal-lg" role="document" rol="dialog" >
    <div class="modal-content">
     {!! Form::open(['class'=>'form-vertical','id'=>'form_resitem_update'])!!}
     <div class="modal-header">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="row">
           <div class="col-md-12">
            <section class="content-header">
             <div class="header_title">
              <h3>
                Actualizar registros de Bienes Inmuebles - <span id="acas_nombre_caso1"></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="refrescarPantalla()">
                  <span aria-hidden="true">&times;</span>
                </button>
              </h3>
            </div>
          </section>
        </div>   
      </div>
    </div>
    <div  style="padding:20px">
      <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">   
      <input type="hidden" name="acasoId_in" id="acasoId_in">
      <input type="hidden" name="anroficha_in" id="anroficha_in">
      <input type="hidden" name="apagina1" id="apagina1" value="1">

  
       <div class="col-md-12">
           <div class="form-group col-md-6">
            <label class="control-label">
              Creador:
            </label>
            <input id="acreador_in" type="text" class="form-control" disabled> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Fecha de creación:
            </label>
            <input id="afecha_creador_in" type="text" class="form-control" disabled> 
          </div>
         </div>
          <div class="col-md-12">
           <div class="form-group col-md-6">
            <label class="control-label">
              Modificador:
            </label>
            <input id="amodificador_in" type="text" class="form-control" disabled> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Fecha de modificación:
            </label>
            <input id="afecha_modificador_in" type="text" class="form-control" disabled> 
          </div>
         </div>
    <div class="col-md-12">
      <div class="panel-heading">
        <label class="control-label" style="color:#275c26";>
         1.IDENTIFICACIÓN::
       </label>
     </div>
   </div>

   <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
        Código Catastral:
      </label>
      <input id="acodCatastral_in" type="text" class="form-control"  placeholder="000-0000-0000-0000"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Código Catastral Histórico 1::
     </label>
     <input id="acodCatastral1_in" type="text" class="form-control"  placeholder="000-0000-0000-0000"> 
   </div>
 </div>
 <div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
     2.DENOMINACIÓN DEL INMUEBLE:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
    Original:
  </label>
  <input id="aoriginal_in" type="text" class="form-control"  placeholder="Original"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
   Tradicional:
 </label>
 <input id="atradicional_in1" type="text" class="form-control"  placeholder="Tradicional"> 
</div>
</div>
   <div class="col-md-12">
     <div class="form-group col-md-12">
      <label class="control-label">
        Actual:
      </label>
      <input id="aActual_in" type="text" class="form-control"  placeholder="Actual"> 
    </div>
 </div>
 <div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
     3.LOCALIZACIÓN:
   </label>
 </div>
</div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Departamento:
      </label>
      <input id="adepartamento_in" type="text" class="form-control"  value="La Paz" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Ciudad/Población:
     </label>
     <input id="aciudad_in" type="text" class="form-control"  value="La Paz" disabled> 
   </div>
</div>
 <div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Municipio:
      </label>
      <input id="amunicipio_in" type="text" class="form-control" value="La Paz" disabled> 
    </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      Macrodistrito:
    </label>
    <select class="form-control" id="aMacrodistrito_in" >
          <option value="-1"> Seleccionar...</option>
          <option value="MACRODISTRITO 1 COTAHUMA"> MACRODISTRITO 1 COTAHUMA</option> 
          <option value="MACRODISTRITO 2 MAXIMILIANO PAREDES">MACRODISTRITO 2 MAXIMILIANO PAREDES</option>
          <option value="MACRODISTRITO 3 PERIFERICA">MACRODISTRITO 3 PERIFERICA</option>
          <option value="MACRODISTRITO 4 SAN ANTONIO">MACRODISTRITO 4 SAN ANTONIO</option>  
          <option value="MACRODISTRITO 5 SUR">MACRODISTRITO 5 SUR</option>
          <option value="MACRODISTRITO 6 MALLASA">MACRODISTRITO 6 MALLASA</option>
          <option value="MACRODISTRITO 7 CENTRO">MACRODISTRITO 7 CENTRO</option>
          <option value="MACRODISTRITO 8 HAMPATURI">MACRODISTRITO 8 HAMPATURI</option>
          <option value="MACRODISTRITO 9 ZONGO">MACRODISTRITO 9 ZONGO</option>  
  </select>
  </div>
  <div class="form-group col-md-6">
    <label class="control-label">
     Tipo de Vía:
    </label>
    <select class="form-control" id="atipoViaLocalizacion_in" >
          <option value="-1"> Seleccionar...</option>
          <option value="Avenida">Avenida</option> 
          <option value="Calle">Calle</option>
          <option value="Pasaje">Pasaje</option>
          <option value="Otro">Otro</option>
    </select>
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Entre Calles o Esquina:
 </label>
 <input id="acalleEsquina_in" type="text" class="form-control"  placeholder="Entre Calles"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
   Dirección:
 </label>
 <input id="adireccion_in" type="text" class="form-control"  placeholder="Direccion"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número1:
 </label>
 <input id="anumero_in" type="text" class="form-control"  placeholder="Numero"> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   zona:
 </label>
 <input id="azona_in" type="text" class="form-control"  placeholder="Zona"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número2:
 </label>
 <input id="anumero_in2" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número3:
 </label>
 <input id="anumero_in3" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número4:
 </label>
 <input id="anumero_in4" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Número5:
 </label>
 <input id="anumero_in5" type="text" class="form-control"  placeholder="Numero"> 
</div>
</div>
 <div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
     4.REGIMEN DE PROPIEDAD:
   </label>
 </div>
</div>

<!-- Grilla Regimen -->
<div class="col-md-12"> 
  <small>
    <div class="pull-right">
      <a   type="button" style="cursor:pointer;">
        <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Regimen de la Propiedad" onclick="actualizarGrillaRegimenProp();"></i>
      </a>
    </div>
  </small>
  <div id="aregimenPropietario"></div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    5. MEDIDAS DEL PREDIO:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Área del Predio(m2):
 </label>
 <input id="aareaPredio_in" type="text" class="form-control"  placeholder="Area"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Ancho Frente del Predio(m):
 </label>
 <input id="aanchoFrente_in" type="text" class="form-control"  placeholder="Ancho"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Fondo del Predio(m):
 </label>
 <input id="afondoPredio_in" type="text" class="form-control"  placeholder="Fondo"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Ancho Muro(m):
 </label>
 <input id="aanchoMuro_in" type="text" class="form-control"  placeholder="Ancho Muro"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Altura Interior Promedio(m):
 </label>
 <input id="aalturaInterior_in" type="text" class="form-control"  placeholder="Altura Interior"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Altura de Fachada Exterior (m):
 </label>
 <input id="aalturaFachada_in" type="text" class="form-control"  placeholder="Altura Exterior"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
 Altura Máxima del Edificio:
 </label>
 <input id="aalturaMaxima_in" type="text" class="form-control"  placeholder="Altura Maxima"> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    6. USO DE SUELO:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Tradicional:
 </label>
 <table width="100%">
 <tr>
 <td><input type="checkbox" id="atradicional1"> Comercio</td>
 <td><input type="checkbox" id="atradicional2"> Culto</td>
 <td><input type="checkbox" id="atradicional3"> Educación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="atradicional4"> Industria</td>
 <td><input type="checkbox" id="atradicional5"> Militar</td>
 <td><input type="checkbox" id="atradicional6"> Recreación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="atradicional7"> Salud</td>
 <td><input type="checkbox" id="atradicional8"> Servicio Publico   </td>
 <td><input type="checkbox" id="atradicional9"> Sin Uso</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="atradicional10"> Socio Cultural</td>
 <td><input type="checkbox" id="atradicional11"> Taller</td>
 <td><input type="checkbox" id="atradicional12"> Vivienda</td>
 </tr>
 </table>

</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Actual:
 </label>
 <table width="100%">
 <tr>
 <td><input type="checkbox" id="aactual1"> Comercio</td>
 <td><input type="checkbox" id="aactual2"> Culto</td>
 <td><input type="checkbox" id="aactual3"> Educación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="aactual4"> Industria</td>
 <td><input type="checkbox" id="aactual5"> Militar</td>
 <td><input type="checkbox" id="aactual6"> Recreación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="aactual7"> Salud</td>
 <td><input type="checkbox" id="aactual8"> Servicio Publico   </td>
 <td><input type="checkbox" id="aactual9"> Sin Uso</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="aactual10"> Socio Cultural</td>
 <td><input type="checkbox" id="aactual11"> Taller</td>
 <td><input type="checkbox" id="aactual12"> Vivienda</td>
 </tr> 
 </table>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Tendencia:
 </label>
  <table width="100%">
 <tr>
 <td><input type="checkbox" id="atendencia1"> Comercio</td>
 <td><input type="checkbox" id="atendencia2"> Culto</td>
 <td><input type="checkbox" id="atendencia3"> Educación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="atendencia4"> Industria</td>
 <td><input type="checkbox" id="atendencia5"> Militar</td>
 <td><input type="checkbox" id="atendencia6"> Recreación</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="atendencia7"> Salud</td>
 <td><input type="checkbox" id="atendencia8"> Servicio Publico   </td>
 <td><input type="checkbox" id="atendencia9"> Sin Uso</td>
 </tr>
 <tr>
 <td><input type="checkbox" id="atendencia10"> Socio Cultural</td>
 <td><input type="checkbox" id="atendencia11"> Taller</td>
 <td><input type="checkbox" id="atendencia12"> Vivienda</td>
 </tr> 
 </table>
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    7. DESCRIPCIÓN DEL PREDIO:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Descripción:
 </label>
 <textarea name="textarea" rows="5" cols="58" id="adescripcion_in" class="form-control ">Escriba aquí</textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Ubicación en la Manzana:
 </label>
<select class="form-control" id="aubicacionManzana_in" >
          <option value="-1">Selecionar...</option> 
          <option value="Aislado"> Aislado</option> 
          <option value="Corazon de Manzana"> Corazón de Manzana</option>
          <option value="Esquinero"> Esquinero</option>
          <option value="Manzana Completa"> Manzana Completa</option>  
          <option value="Media Manzana"> Media Manzana</option>
          <option value="Medianero"> Medianero</option> 
          <option value="Solar"> Solar</option>   
  </select><br>
 <input type="checkbox" name="apuntuacion" value="2" id="apuntuacion_umcheck" onclick="capturarCheckPuntuacionAct()"> Ubicación de la Manzana
  <br>
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Línea de Construcción:
 </label>
<select class="form-control" id="alineaConstruccion_in" >
          <option value="-1">Selecionar...</option>  
          <option value="Alineada">Alineada</option>
          <option value="Remetida">Remetida</option>
          <option value="Sobresalida">Sobresalida</option>   
  </select><br>
  <input type="checkbox" name="apuntuacion" value="2" id="apuntuacion_lccheck" onclick="capturarCheckPuntuacionAct()"> Linea de Construcción<br>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Trazo de Manzana:
 </label>
<select class="form-control" id="atrazoManzana_in" >
          <option value="(Ninguno)"> Selecionar...</option> 
          <option value="Linea Municipal">Linea Municipal</option>
          <option value="Linea Patrimonial">Linea Patrimonial</option>     
  </select><br>
  <input type="checkbox" name="apuntuacion" value="2" id="apuntuacion_tmcheck" onclick="capturarCheckPuntuacionAct()"> Trazo de Manzana<br> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Tipología Arquitectónica:
 </label>
 <select class="form-control" id="atipologiaArqui_in" >
          <option value="(Ninguno)"> Seleccionar...</option> 
          <option value="Compacta"> Compacta</option>
          <option value="Compacta con Retiros"> Compacta con Retiros</option>
          <option value="Patio Central"> Patio Central</option>   
  </select><br>
  <input type="checkbox" name="apuntuacion" value="1" id="apuntuacion_tacheck" onclick="capturarCheckPuntuacionAct()"> Tipología Arquitectónica<br>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Área Edificable de Crecimiento:
 </label>
 <select class="form-control" id="aareaEdificable_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Tiene Area">Tiene Area</option>
          <option value="No tiene Area">No tiene Area</option>  
  </select><br>
  <input type="checkbox" name="apuntuacion" value="1" id="apuntuacion_aeccheck" onclick="capturarCheckPuntuacionAct()"> Área Edificable de Crecimiento<br>
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Material Vía:
 </label>
 <select class="form-control" id="amaterialVia_in" >
          <option value="(Ninguno)"> Seleccionar...</option> 
          <option value="Adoquin">Adoquin</option>
          <option value="Asfalto">Asfalto</option>
          <option value="Cemento">Cemento</option>
          <option value="Empedrado">Empedrado</option>
          <option value="Pavimento Rígido">Pavimento Rígido</option>   
  </select> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Tipo de Vía:
 </label>
 <select class="form-control" id="atipoVia_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Peatonal"> Peatonal</option>
          <option value="Vehicular"> Vehicular</option>  
  </select>
</div>
<div class="form-group col-md-6">
  <label  class="control-label">
   Bloques Inventariados:
 </label>
  <select class="form-control" id="abloqueInventario_in" disabled >
          <option value="0"> Seleccionar...</option> 
          <option value="1"> 1</option>
          <option value="2"> 2</option>
          <option value="3"> 3</option> 
          <option value="4"> 4</option> 
          <option value="5"> 5</option> 
  </select>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Bloques Reales:
 </label>
 <input id="abloquesReales_in" type="text" class="form-control"  placeholder="Bloques Reales"> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
 Hogares Inventariados:
 </label>
 <input id="ahogaresInventario_in" type="text" class="form-control"  placeholder="Hogares Inventariados"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Hogares Reales:
 </label>
 <input id="ahogaresReales_in" type="text" class="form-control"  placeholder="Hogares Reales"> 
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Estilo General:
 </label>
 
 <select class="form-control" id="aestiloGeneral_in" >
            <option value="(Ninguno)">Seleccionar...</option> 
            <option value="Academicista">Academicista</option>
            <option value="Arq. Industrial">Arq. Industrial</option>
            <option value="Arq. Noveau">Arq. Noveau</option> 
            <option value="Arte Decó">Arte Decó</option>
            <option value="Barroco">Barroco</option>
            <option value="Barroco Mestizo">Barroco Mestizo</option>
            <option value="Brutalista">Brutalista</option>
            <option value="Californiano">Californiano</option>
            <option value="Clasicismo postmoderno">Clasicismo postmoderno</option>
            <option value="Clasicista">Clasicista</option>
            <option value="Colonial">Colonial</option>
            <option value="Constructivista">Constructivista</option>
            <option value="Contemporaneo">Contemporaneo</option>
            <option value="Cubista">Cubista</option>
            <option value="Ecléctico">Ecléctico</option>
            <option value="Estilo no definido">Estilo no definido</option>
            <option value="Expresionista">Expresionista</option>
            <option value="Funcionalista">Funcionalista</option>
            <option value="Georgiano">Georgiano</option>
            <option value="Georginiano">Georginiano</option>
            <option value="High Tech">High Tech</option>
            <option value="Historicista">Historicista</option>
            <option value="Internacional">Internacional</option>
            <option value="Minimalismo">Minimalismo</option>
            <option value="Moderno">Moderno</option>
            <option value="Mosarable">Mosarable</option>
            <option value="Mudéjar">Mudéjar</option>
            <option value="Neoclásico">Neoclásico</option>
            <option value="Neocolonial">Neocolonial</option>
            <option value="Neogótico">Neogótico</option>
            <option value="Neotiwanacota">Neotiwanacota</option>
            <option value="Neovernacular">Neovernacular</option>
            <option value="Organicista">Organicista</option>
            <option value="Postmoderno">Postmoderno</option>
            <option value="Racionalista">Racionalista</option>
            <option value="Renacentista">Renacentista</option>
            <option value="Republicano">Republicano</option>
            <option value="Tudor">Tudor</option> 
            <option value="Vernacular">Vernacular</option> 
            <option value="Victoriano">Victoriano</option>  
  </select> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Rango de Época:
 </label>
 <select class="form-control" id="arangoEpoca_in" >
          <option value="(Ninguno)"> Seleccionar...</option> 
          <option value="1548-1825">1548-1825</option>
          <option value="1826-1850">1826-1850</option> 
          <option value="1851-1900">1851-1900</option>
          <option value="1901-1930">1901-1930</option>  
          <option value="1931-1960">1931-1960</option> 
          <option value="1961-1980">1961-1980</option> 
          <option value="1981-2000">1981-2000</option> 
          <option value="2000 en Adelante">2000 en Adelante</option> 
  </select>
</div>
 <div class="form-group col-md-6">
  <label class="control-label">
   Estado de Conservación:
 </label>
 <select class="form-control" id="aestadoConserva_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Muy Bueno">Muy Bueno</option>
          <option value="Bueno">Bueno</option>
          <option value="Regular">Regular</option>   
          <option value="Malo">Malo</option>
          <option value="Muy Malo">Muy Malo</option>  
  </select> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    II. DATOS ORALES Y DOCUMENTALES:
   </label>
 </div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
    1. MARCO LEGAL:
   </label>
 </div>
</div>
<!--grilla Figura Proteccion-->
  <div class="col-md-12"> 
    <small>
    <div class="pull-right">
        <a   type="button" style="cursor:pointer;">
          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nuevo Argumento" onclick="ActualizarGrillaFiguraProt();"></i>
        </a>
    </div>
    </small>
    <div id="afiguraProteccion"></div><br><br>
    </div><br><br>

<!--Fin Figura Proteccion-->
<!--grilla DOS-->
<br><br>
<center>
  <label class="control-label" style="color:#275c26";>
    Referencias Históricas
  </label>
</center>
<div class="col-md-12"> 
  <small>
    <div class="pull-right">
      <a   type="button" style="cursor:pointer;">
        <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" title="Agregar Nueva Referencia Historica" onclick="actualizarGrillaReferenciaHist();"></i>
      </a>
    </div>
  </small>
  <div id="areferenciaHistorica2"></div>
</div>
<!--Fin grilla-->
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
   2. DATOS ORALES::
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
Fecha de Construcción:
 </label>
 <input id="afechaConstruccion_in" type="text" class="form-control" placeholder="Fecha de construcción" > 
  <br><input type="checkbox" name="apuntuacion" value="2" id="apuntuacion_fccheck" onclick="capturarCheckPuntuacionAct()"> Fecha de Construcción<br>
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Autor y/o Constructor:
 </label>
 <input id="aautor_in" type="text" class="form-control"  placeholder="Descripcion">
  <br><input type="checkbox" name="apuntuacion" value="2" id="apuntuacion_accheck" onclick="capturarCheckPuntuacionAct()"> Autor y/o Constructor<br>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Preexistencia de Edificación/ Intervenciones:
 </label>
 <textarea name="textarea" id="apreexistencia_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Propietarios Anteriores:
 </label>
 <textarea name="textarea" id="apropietarios_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Usos Originales:
 </label>
 <textarea name="textarea" id="ausoOriginal_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Hechos Históricos:
 </label>
 <textarea name="textarea" id="ahechoHistorico_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Fiestas Religiosas::
 </label>
 <textarea name="textarea" id="afiestaReligiosa_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Datos Históricos del Conjunto:
 </label>
 <textarea name="textarea" id="adatoHistorico_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Fuentes:
 </label>
 <textarea name="textarea" id="afuente_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
   3. DATOS DOCUMENTALES:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
Fecha de Construcción:
 </label>
 <input id="afechaConstruccion_in3" type="text" class="form-control"  placeholder="Fecha de construcción">
 <input type="checkbox" name="apuntuacion" value="2" id="apuntuacion_fcdccheck" onclick="capturarCheckPuntuacionAct()"> Fecha de Construcción<br>
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Autor y/o Constructor:
 </label>
 <input id="aautor_in3" type="text" class="form-control"  placeholder="Descripcion">
  <input type="checkbox" name="apuntuacion" value="3" id="apuntuacion_acdccheck" onclick="capturarCheckPuntuacionAct()"> Autor y/o Constructor<br>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Preexistencia de Edificación/ Intervenciones:
 </label>
 <textarea name="textarea" id="apreexistencia_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Propietarios Anteriores:
 </label>
 <textarea name="textarea" id="apropietarios_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Usos Originales:
 </label>
 <textarea name="textarea" id="ausoOriginal_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Hechos Históricos:
 </label>
 <textarea name="textarea" id="ahechoHistorico_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Fiestas Religiosas::
 </label>
 <textarea name="textarea" id="afiestaReligiosa_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Datos Históricos del Conjunto:
 </label>
 <textarea name="textarea" id="adatoHistorico_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Fuentes:
 </label>
 <textarea name="textarea" id="afuente_in3" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
  4. INSCRIPCIONES:
   </label>
 </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Inscripciones:
 </label>
 <textarea name="textarea" id="ainscripcion_in" rows="5" cols="55" class="form-control "></textarea> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Existencia de Patrimonio Mueble:
 </label>
 <textarea name="textarea" id="aexistePatrimonio_in" rows="5" cols="55" class="form-control "></textarea>
 <br><input type="checkbox" name="apuntuacion" value="1" id="apuntuacion_epmcheck" onclick="capturarCheckPuntuacionAct()"> Existencia de Patrimonio Mueble<br>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
  Existencia de Restos Arqueológicos:
 </label>
 <textarea name="textarea" id="aexisteResto_in"  rows="5" cols="55" class="form-control "></textarea>
 <br><input type="checkbox" name="apuntuacion" value="1" id="apuntuacion_eracheck" onclick="capturarCheckPuntuacionAct()"> Existencia de Restos Arqueológicos<br> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   Existencia de Elementos Naturales:
 </label>
 <textarea name="textarea" id="aexisteElemento_in" rows="5" cols="55" class="form-control "></textarea>
 <br><input type="checkbox" name="apuntuacion" value="1" id="apuntuacion_eencheck" onclick="capturarCheckPuntuacionAct()"> Existencia de Elementos Naturales<br> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Ritos, Mitos, Tradiciones y/o Otros:
 </label>
 <textarea name="textarea" id="aritosMitos_in" rows="5" cols="55" class="form-control "></textarea>
 <br><input type="checkbox"  name="apuntuacion" value="1" id="apuntuacion_rmtocheck" onclick="capturarCheckPuntuacionAct()"> Ritos, Mitos, Tradiciones y/o Otros<br>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
  Conjunto:
 </label>
 <select class="form-control" id="aconjunto_in" >
          <option value="-1">Seleccionar...</option>
          <option value="1. Conjunto Centro Historico (Macrodistrito Centro)">1. Conjunto Centro Historico (Macrodistrito Centro)</option>
          <option value="2. Conjunto San Sebastian (Macrodistrito Centro)">2. Conjunto San Sebastian (Macrodistrito Centro)</option> 
          <option value="3. Conjunto San Francisco (Macrodistrito Centro y Cotahuma)">3. Conjunto San Francisco (Macrodistrito Centro y Cotahuma)</option> 
          <option value="4. Conjunto Ismael Montes (Macrodistrito Centro y Periferica)">4. Conjunto Ismael Montes (Macrodistrito Centro y Periferica)</option>
          <option value="5. Conjunto Plaza Isabel La Catolica (Macrodistrito Centro)">5. Conjunto Plaza Isabel La Catolica (Macrodistrito Centro)</option> 
          <option value="6. Conjunto Rosendo Gutierrez (Macrodistrito Cotahuma)">6. Conjunto Rosendo Gutierrez (Macrodistrito Cotahuma)</option> 
          <option value="7. Conjunto Obrajes (Macrodistrito Sur)">7. Conjunto Obrajes (Macrodistrito Sur)</option>
          <option value="8. Conjunto San Pedro (Macrodistrito Cotahuma)">8. Conjunto San Pedro (Macrodistrito Cotahuma)</option> 
          <option value="9. Conjunto Luis Villanueva (Macrodistrito Centro)">9. Conjunto Luis Villanueva (Macrodistrito Centro)</option> 
          <option value="10. Conjunto Casas Alborta - Vickers (Macrodistrito Cotahuma)">10. Conjunto Casas Alborta - Vickers (Macrodistrito Cotahuma)</option>
          <option value="11. Conjunto Francisco Bedregal(Macrodistrito Cotahuma)">11. Conjunto Francisco Bedregal(Macrodistrito Cotahuma)</option> 
          <option value="12. Conjunto Plaza Riosinho (Macrodistrito Periferica)">12. Conjunto Plaza Riosinho (Macrodistrito Periferica)</option>
          <option value="13. Conjunto Capitan Castrillo (Macrodistrito Cotahuma)">13. Conjunto Capitan Castrillo (Macrodistrito Cotahuma)</option> 
          <option value="14. Conjunto Agustin Aspiazu (Macrodistrito Cotahuma)">14. Conjunto Agustin Aspiazu (Macrodistrito Cotahuma)</option>
          <option value="15. Conjunto Heriberto Gutierrez(Macrodistrito Centro)">15. Conjunto Heriberto Gutierrez(Macrodistrito Centro)</option> 
          <option value="16. Conjunto Monticulo (Macrodistrito Cotahuma)">16. Conjunto Monticulo (Macrodistrito Cotahuma)</option>  
          <option value="17. Cementerio General (Macrodistrito Maximiliano Paredes)">17. Cementerio General (Macrodistrito Maximiliano Paredes)</option>   
          <option value="1. Area Capitan Castrillo (Macrodistrito Cotahuma)">1. Area Capitan Castrillo (Macrodistrito Cotahuma)</option> 
          <option value="2. Area Agustin Aspiazu (Macrodistrito Cotahuma)">2. Area Agustin Aspiazu (Macrodistrito Cotahuma)</option>
          <option value="3. Area Heriberto Gutierrez(Macrodistrito Cotahuma)">3. Area Heriberto Gutierrez(Macrodistrito Cotahuma)</option> 
          <option value="4. Area Monticulo (Macrodistrito Cotahuma)">4. Area Monticulo (Macrodistrito Cotahuma)</option>  
  </select>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
  Estado de Verificación:
 </label>
 <select class="form-control" id="aestadoVerifica_in" >
          <option value="-1"> Seleccionar...</option> 
          <option value="Aprobado">Aprobado</option>
          <option value="No Verificado">No Verificado</option> 
          <option value="Observado">Observado</option> 
  </select>
</div>
<div class="form-group col-md-6">
  <label class="control-label">
Puntuación:
 </label>
 <input id="apuntuacion_in" type="text" class="form-control"  placeholder="0" disabled> 
</div>
</div>
<div class="modal-footer">
 <button class="btn btn-primary" data-dismiss="modal" type="button" onclick="refrescarPantalla()">
            <i class="fa fa-close"></i> Cancelar</button>
 <button type="button" class="btn btn-primary"  onclick="validarUpdate1()"> <i class="glyphicon fa fa-save"></i> Actualizar / Continuar</button>
</div> 
{!! Form::close() !!} 
</div>
</div>
</div>
</div>
</div>
</div>