@extends('backend.template.app')
@section('main-content')
<br>
<h3>Reporte Pivote Patrimonio Cultural</h3>		
<br>
<div class = "row" style="overflow: auto; width: auto; height: auto; overflow: auto;">
   	<div class = "col-md-12">      
       	<div id = "mio" style = "margin: 30px;"></div>
   	</div>
</div>
	  
@endsection
@push('scripts')

<script>

	function pivoteReporte(){
	    formData = {"identificador":'SERVICIO_VALLE-815',"parametros": '{"xgestion":2018}'};   
	        $.ajax({
	            type        : 'GET',
	      		url         :  urlToken,
	     		data        : '',
	            success: function(token) 
	              {
	              	console.log('token',token);
	                $.ajax({
	                	type        : 'POST',            
		                url         : urlRegla,
		                data        : formData,
		                dataType    : 'json',
		                crossDomain : true,
		                headers: {
		                	'authorization': 'Bearer' + token,
		                },
	                    success: function(dataIN) {
	                    	console.log('dataIN3333333333333333333',dataIN);	                    	
	                    	$("#mio").pivotUI(dataIN, {"rows":[], "cols":[]
	                    	});
	                    	/*
								$("#mio").pivotUI(dataIN, {"rows":["xur_descripcion"], "cols":["xpe_periodo"],vals:["xpe_monto"],aggregatorName:"Sum"
	                    	});

	                    	*/   	                 	
	                  
	                  },
	                    error: function (xhr, status, error) {
	                    	
	                 }
	                });
	              },
	              error: function(result) {
	                    swal( "Error..!", "No se puede generar pivote", "error" );
	            }
	        });	 
	   	
		   
    };


  	$(document).ready(function (){
    	pivoteReporte();
  	});

</script>

@endpush