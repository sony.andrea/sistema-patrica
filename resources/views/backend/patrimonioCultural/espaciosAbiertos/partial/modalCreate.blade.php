<style>
#tdborde{
  border: 1px solid #a3c2c2;
  border-collapse: border-bottom-width;
  padding:5;
  text-align: center;
  font-size: 12px;
  font-family: Arial;
  background: #d7dbdd;
}
#thtitle{
  padding:10;
  text-align: center;
}
#tdbody{
  border: 1px solid #a3c2c2;
  border-collapse: border-bottom-width;
  padding:5;
  font-size: 12px;
  font-family: Arial;
}

</style>
<div class="modal fade modal-default" id="myCreateEspacioAbierto" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h3>
              Nueva Ficha Espacio Abierto
              <small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h3>
          </div>
        </section>
      </div>   
    </div>
</div>
<div  style="padding:20px">
<input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
<input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">    
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       I. IDENTIFICACIÓN Y SERVICIOS:
      </label>
    </div>
</div>    
<div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Creador:
      </label>
      <input id="ccreador" type="text" class="form-control"  placeholder="Creador" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Autorizador:
     </label>
     <input id="cautorizador" type="text" class="form-control"  placeholder="Autorizador" disabled> 
   </div>
</div>
{!! Form::open(['class'=>'form-vertical puestoMercado-validate','id'=>'formNuevaFicha_create'])!!}
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       DATOS GENERALES:
      </label>
    </div>
 </div>
 <div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
    Código Catastral:
    </label>
    <input id="ccodigocatastral"  type="text" class="form-control"  placeholder="Código Catastral">
    El código catastral tiene este formato (000-0000-0000-0000) 
  </div> 
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       GEOREFERENCIA:
      </label>
    </div>
 </div>
  <div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
    Georeferencia:
    </label>
    <input id="cgeoreferencia"  type="text" class="form-control"  placeholder="Georeferencia">    
  </div> 
</div>
 <!--inicio Puntos Colineales juan-->
<div class="col-md-12"><hr> 
  <center><b> Puntos Colineales</b></center>  
  <label class="control-label col-md-12">
  <table width="100%">
    <tr>
        <td width="40%" id="tdborde" >Nro</td>
        <td width="50%" id="tdborde">Punto Colineal</td>
          <td width="10%">
           <a   type="button" style="cursor:pointer;">
          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaPuntosColineales();"></i>
          </a>
        </td>
      </tr>
    </table>
  </label>
 
</div>
<div id="crearGrillaPuntosColinealesado"></div>
 <!--Fin Puntos Colineales-->
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       DENOMINACIÓN DEL INMUEBLE:
      </label>
    </div>
 </div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Tradicional:
 </label>
 <input id="ctradicional" type="text" class="form-control"  placeholder="Tradicional"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Actual:
  </label>
  <input id="cactual" type="text" class="form-control"  placeholder="Actual"> 
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       LOCALIZACIÓN:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Departamento:
 </label>
 <input id="cdepartamento" type="text" class="form-control"  placeholder="Departamento" value="La Paz" disabled> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Provincia:
  </label>
  <input id="cprovincia" type="text" class="form-control"  placeholder="Provincia" value="Murillo" disabled> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Municipio:
 </label>
 <input id="cmunicipio" type="text" class="form-control"  placeholder="Municipio" value="La Paz" disabled> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Macro-distrito:
  </label>
  <select id="cmacrodistritoespabie" name="cmacrodistritoespabie" class="form-control">
    <option value="-1">Seleccionar...</option>
  </select>  
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Barrio:
 </label>
 <input id="cbarrio" type="text" class="form-control"  placeholder="Barrio"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Dirección calle:
  </label>
  <input id="cdireccioncalle" type="text" class="form-control"  placeholder="Dirección calle"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Esquina o entre calles 1:
 </label>
 <input id="cesquina1" type="text" class="form-control"  placeholder=" Esquina o entre calle 1"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
   esquina o entre calles 2:
  </label>
  <input id="cesquina2" type="text" class="form-control"  placeholder="esquina o entre calle 2"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Altitud(m.s.n.m.):
 </label>
 <input id="caltitud" type="text" class="form-control"  placeholder="Altitud"> 
</div>
<div class="form-group col-md-6">
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       RÉGIMEN DE PROPIEDAD:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Nombre del propietario:
 </label>
 <input id="cnombrepropietario" type="text" class="form-control"  placeholder="Nombre del propietario"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Tipo de Reg. Propiedad:
  </label>
   <select id="ctiporegpropietario" class="form-control">
    <option value="">Seleccionar...</option>
    <option value="Publico Estatal">Público Estatal</option>
    <option value="Publico Municipal">Público Municipal</option>
    <option value="Publico Particular">Público Particular</option>
    <option value="Publico Religioso">Público Religioso</option>
  </select> 
</div>
</div>
<!--Ininio form-->

        <!--grilla Uso suelo-->
        <div class="col-md-12"> 
        <center><b>Uso de Suelo</b></center>
        <small>
        <div class="pull-right">
            <a   type="button" style="cursor:pointer;">
              <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="GrillaUsoSuelo();"></i>
            </a>
        </div>
        </small>
        <div id="crearGrillaUso"></div>
        </div>
        <br>
       <!--Fin grilla Uso Suelo-->
        <!--begin grilla Servicio-->
        <div class="col-md-12"> <br>
        <center><b>Servicios</b></center>
        <small>
        <div class="pull-right">
            <a   type="button" style="cursor:pointer;">
              <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="GrillaServicio();"></i>
            </a>
        </div>
        </small>
        <div id="crearGrillaServicio"></div>
        </div><br><br>
       <!--Fin grilla Servicio-->
       
        <!--fin Grilla-->
        <div class="col-md-12"><br><br>
          <div class="form-group col-md-4">
            <label class="control-label">
              Otros Servicios
            </label> 
          </div>
          <div class="form-group col-md-6" >
            <label class="control-label">
              <input type="checkbox" id="cregprootrosservicios1">Internet<br>
              <input type="checkbox" id="cregprootrosservicios2">Teléfono<br>
              <input type="checkbox" id="cregprootrosservicios3">TV Cable 
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              II. MARCO LEGAL:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Figura de Protección Legal:
            </label>
            <select id="cmarlegfiguraproyeccion" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Monumento Internacional">Monumento Internacional</option>
              <option value="Monumento Local">Monumento Local</option>
              <option value="Monumento Nacional">Monumento Nacional</option>
              <option value="Sin declaratoría específica">Sin declaratoría específica</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              III. REFERENCIA HISTÓRICA:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Referencia Histórica:
            </label>
            <select id="crefhisreferenciahistorica" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Nivel Internacional">Nivel Internacional</option>
              <option value="Nivel Nacional">Nivel Nacional</option>
              <option value="Nivel Departamental">Nivel Departamental</option>
              <option value="Nivel Municipal">Nivel Municipal</option>
              <option value="Nivel Local">Nivel Local</option>
              <option value="Ninguna">Ninguna</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              IV. DATOS ORALES:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Fecha de Construcción:
            </label>
            <input id="cdatorafechaconstruccion" type="text" class="form-control"  placeholder="Fecha de Construcción"> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Autor o Constructor:
            </label>
            <input id="cdatorautorconstructor" type="text" class="form-control"  placeholder="Autor o Constructor">
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Preexistencia Edificación Intervenciones:
            </label>
            <textarea rows="4" cols="50" id="cdatorapreexistenciaedificacion"  class="form-control "  placeholder="Preexistencia Edificación Intervenciones"></textarea>  
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Propietario:
            </label>
            <input id="cdatorapropietario" type="text" class="form-control"  placeholder="Propietario:"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Usos Originales:
            </label>
            <textarea rows="4" cols="50" id="cdatorausosoriginales" class="form-control " placeholder="Usos Originales:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Hechos históricos Distinguidos:
            </label>
            <textarea rows="4" cols="50" id="cdatorahechoshistoricosdist" class="form-control " placeholder="Hechos Históricos Distinguidos:"></textarea>  
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fiestas Religiosas:
            </label>
            <textarea rows="4" cols="50" id="cdatorafiestasreligiosas" class="form-control " placeholder="Fiestas Religiosas" ></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Datos Históricos del Conjunto:
            </label>
            <textarea rows="4" cols="50" id="cdatoradatoshistoricos" class="form-control " placeholder="Datos Históricos del Conjunto:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fuentes/Bibliografía:
            </label>
            <textarea rows="4" cols="50" id="cdatorafuentebibliografia" type="text" class="form-control " placeholder="Fuentes/Bibliografia"></textarea>  
          </div>
        </div>
        <!--Fin Formulario 2-->
        <!--Inicio Formulacio 3-->
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              V. DATOS DOCUMENTALES:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Fecha de construcción:
            </label>
            <input id="cdatdocfechadeconstruccion" type="date" class="form-control" placeholder="Fecha de Construcción"> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Autor o Constructor:
            </label>
            <input id="cdatdocautoroconstructor" type="text" class="form-control" placeholder="Autor o Constructor">
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Preexistencia/Edificación/Intervenciones:
            </label>
            <textarea rows="4" cols="50" id="cdatdocpreexistenciainvestigacion" class="form-control " placeholder="Preexistencia/Edificación/Intervenciones"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Propietario:
            </label>
            <input id="cdatdocpropietario" type="text" class="form-control"  placeholder="Propietario:"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Usos preexistentes:
            </label>
            <textarea  rows="4" cols="50" id="cdatdocusospreexistentes"  class="form-control " placeholder="Usos preexistentes:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Hechos historicos/pers. distinguidos:
            </label>
            <textarea rows="4" cols="50" id="cdatdochechoshistoricospers"  class="form-control "  placeholder="Hechos Históricos Distinguidos:"></textarea>  
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fiestas Religiosas:
            </label>
            <textarea rows="4" cols="50" id="cdatdocfiestasreli"  class="form-control "  placeholder="Fiestas Religiosas" ></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Datos Históricos del Conjunto:
            </label>
            <textarea rows="4" cols="50" id="cdatdocdatoshistoricosconj"  class="form-control "  placeholder="Datos Históricos:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fuentes/Biografía:
            </label>
            <textarea rows="4" cols="50" id="cdatdocfuentebiblio" type="text" class="form-control " placeholder="Bibliografia"></textarea>  
          </div>
        </div>
        <!--Fin Form 3-->

        <!--inicio form 4-->
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              VI. INSCRIPCIONES:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Inscripciones:
            </label>
            <textarea name="textarea" rows="4" cols="100" class="form-control " id="cinsinscripciones"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              VII. ÉPOCA:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Época:
            </label>
            <select id="cepoepoca" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="1546-1825">1546-1825</option>
              <option value="1826-1850">1826-1850</option>
              <option value="1851-1900">1851-1900</option>
              <option value="1901-1930">1901-1930</option>
              <option value="1931-1960">1931-1960</option>
              <option value="1961-1980">1961-1980</option>
              <option value="1981-2000">1981-2000</option>
              <option value="2000 en adelante">2000 en adelante</option>
            </select> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Año de construcción:
            </label>
            <input id="cepoanoconstruccion" type="text" class="form-control"  placeholder="Año de construcción"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Año de ultima remodelación:
            </label>
            <input id="cepoanoremodelacion" type="text" class="form-control"  placeholder="Año de ultima remodelación"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              VIII. ESTILO:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estilo:
            </label>
            <select id="cestestilo" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Academicista">Academicista</option>
              <option value="Arq. Noveau">Arq. Noveau</option>
              <option value="Arte Decó">Arte Decó</option>
              <option value="Barroco">Barroco</option>
              <option value="Barroco Mestizo">Barroco Mestizo</option>
              <option value="Brutalista">Brutalista</option>
              <option value="Californiano">Californiano</option>
              <option value="Clasicismo postmoderno">Clasicismo postmoderno</option>
              <option value="Clasicista">Clasicista</option>
              <option value="Colonial">Colonial</option>
              <option value="Constructivista">Constructivista</option>
              <option value="Contemporaneo">Contemporaneo</option>
              <option value="Cubista">Cubista</option>
              <option value="Ecléctico">Ecléctico</option>
              <option value="Estilo no definido">Estilo no definido</option>
              <option value="Expresionista">Expresionista</option>
              <option value="High Tech">High Tech</option>
              <option value="Historicista">Historicista</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              IX. DESCRIPCIÓN DEL AREA:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Ubicación del espacio en el area
            </label>
            <select id="cdesareubicacionespacio" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Aisaldo">Aisaldo</option>
              <option value="Corazon de Manzana">Corazon de Manzana</option>
              <option value="Esquinero">Esquinero</option>
              <option value="Manzana Completa">Manzana Completa</option>
              <option value="Media Manzana">Media Manzana</option>
              <option value="Medianero">Medianero</option>
              <option value="Salar">Salar</option>
            </select> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Linea de construcción
            </label>
            <select id="cdesarelineaconstruccion" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Alineada">Alineada</option>
              <option value="Remetida">Remetida</option>
              <option value="Sobresalida">Sobresalida</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Tipología:
            </label>
            <select id="cdesaretipologia" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Base Colina">Base Colina</option>
              <option value="Borde de Quebrada">Borde de Quebrada</option>
              <option value="Colina Solitaria">Colina Solitaria</option>
              <option value="Enclave">Enclave</option>
              <option value="Final de Meseta">Final de Meseta</option>
              <option value="Final de Vía">Final de Vía</option>
              <option value="Incluido en el área">Incluido en el área</option>
              <option value="Puente">Puente</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              Muros de Delimitación:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-2">
            <label class="control-label">
              Material:
            </label> 
          </div>
          <div class="form-group col-md-10">
            <label class="control-label">
              <input type="checkbox" id="cdesarematerial1">Adobe<br>
              <input type="checkbox" id="cdesarematerial2">Concreto Armado<br>
              <input type="checkbox" id="cdesarematerial3">Hormigon Armado<br>
              <input type="checkbox" id="cdesarematerial4">Ladrillo de Barro<br>
              <input type="checkbox" id="cdesarematerial5">Madera<br>
              <input type="checkbox" id="cdesarematerial6">Otros<br>
              <input type="checkbox" id="cdesarematerial7">Piedra<br>
              <input type="checkbox" id="cdesarematerial8">Piedra Cal<br>
              <input type="checkbox" id="cdesarematerial9">Prefabricados<br>
              <input type="checkbox" id="cdesarematerial10">Tapial<br>
            </label>
          </div>
        </div>
        <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Otros:
              </label>
              <input id="cdesareotros"  type="text" class="form-control"  placeholder="Otros"> 
            </div>
          </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestado" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Integridad:
            </label>
            <select id="cdesareintegridad" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Modificado">Modificado</option>
              <option value="Nuevo">Nuevo</option>
              <option value="Original">Original</option>
              <option value="Original y Nuevo">Original y Nuevo</option>
            </select> 
          </div>
        </div><br>
        <!--/////////////////////Begin Grilla Rejas////////////////////-->
        <div class="col-md-12">
          <hr> 
          <center><b>Rejas</b></center> 
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a  type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaRejas();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label> 
          <br>
          <br>
        </div>
         <div id="crearGrillaRejas"></div>
        <!--/////////////////////End Grilla Rejas////////////////////-->

        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadorejas" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
          <!--begin grilla muro acabado-->
        <div class="col-md-12">
          <hr> 
          <center><b>Acabados de Muros</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
               
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaAcabadoMuro();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaAcabado"></div>
        <!--END grilla muro acabado-->

      
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadoacabadomur" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <!--///////////////////BEGIN GRILLA PINTURAS////////////-->
        <div class="col-md-12">
          <hr> 
          <center><b>Pinturas</b></center> 
          <label class="control-label col-md-12"> 
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otra descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaPinturas();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaPinturas"></div>
      <!--///////////////////END GRILLA PINTURAS////////////-->

        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadopint" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
 <!--inicio Grilla piso-->
        <div class="col-md-12">
          <hr> 
          <center><b>_Pisos</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaPiso();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaPisoado"></div>
        <!--fin Grilla piso-->
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadopisos" class="form-control">
            <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <!--inicio Grilla Puertas de Acceso-->
        <div class="col-md-12">
          <hr> 
          <center><b>Puertas de Acceso</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaPuertasAcceso();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaPuertasAcceso"></div>
    
        <!--fin Grilla Puertas de Acceso-->
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadopuertasacc" class="form-control">
               <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>

         <!--inicio Grilla Mobiliario Urbano-->
        <div class="col-md-12">
          <hr> 
          <center><b>_Mobiliario Urbano</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaMovilidadUrbano();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaMovilidadUrbanoado"></div>
    
        <!--fin Grilla Mobiliario Urbano-->
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadomoviliariourb" class="form-control">
               <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>

         <!--inicio Grilla Escaleras-->
        <div class="col-md-12">
          <hr> 
          <center><b> Escaleras</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaEscaleras();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaEscalerasado"></div>
    
        <!--fin Grilla Escaleras-->
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadoescaleras" class="form-control">
             <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
         <!--inicio Grilla Caracteristicas de la vegetacionj-->
        <div class="col-md-12">
          <hr> 
          <center><b> Caracterísiticas de la Vegetación</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="15%" id="tdborde">Descripcion</td>
                <td width="20%" id="tdborde">Cantidad</td>
                <td width="25%" id="tdborde">tipos</td>
                <td width="15%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaCarVegetacion();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaCarVegetacionado"></div>
        <!--fin Grilla Caracteristicas de la vegetacion-->

        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadocaracteristicasvege" class="form-control">
             <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>

  <!--inicio Grilla Caracteristicas de la vegetacionj-->
        <div class="col-md-12">
          <hr> 
          <center><b> Detalles Artisitcos</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                  <td width="45%" id="tdborde">Descripcion</td>
                  <td width="45%" id="tdborde">Integridad</td>
                  <td width="10%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="grillaDetaArtistoco();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <br>
        </div>
        <div id="crearGrillaDetaArtistocoado"></div>
        <!--fin Grilla Caracteristicas de la vegetacion-->

        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="cdesareestadodetalleartistico" class="form-control">
           <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <!--fin form5-->
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              X. ESQUEMAS:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Descripción:
            </label>
            <textarea name="textarea" class="form-control " id="cesqesquemas"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              XI. ANGULO VISUAL HORIZONTAL:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Orientación
            </label>
            <input id="cangvisorientacion" type="text" class="form-control"  placeholder="Orientacion"></div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Grados
              </label>
              <input id="cangvisgrados" type="text" class="form-control"  placeholder="Grados"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                Vistas:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Material:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cangvisvistas1">Vista Parcial<br>
                <input type="checkbox" id="cangvisvistas2">Vista Panorámica<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Otros:
              </label>
              <input id="cangvisotros" type="text" class="form-control"  placeholder="Otros"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XII. ELEMENTOS VISUALES:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                De izquierda a derecha:
              </label>
              <textarea name="textarea" class="form-control " id="celevisizquierda_derecha"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                A lo alto:
              </label>
              <textarea name="textarea" class="form-control " id="celivisalto"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                A lo bajo:
              </label>
              <textarea name="textarea" class="form-control " id="celivisbajo"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XIII. DESCRIPCIÓN DEL ENTORNO:
              </label>
            </div>
            <div class="form-group col-md-12">
              <label class="control-label">
                Des. entorno inmediato:
              </label>
              <textarea name="textarea" rows="4" cols="100" class="form-control " id="cdesententorno_inmediato"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XIV. DESCRIPCIÓN DEL ESPACIO:
              </label>
            </div>
            <div class="form-group col-md-12">
              <label class="control-label">
                Descripción del espacio:
              </label>
              <textarea name="textarea" rows="4" cols="100" class="form-control " id="cdesespdescrpcion_espacio"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XV. MEDIDAS:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
                Área total(m2):
              </label>
              <input id="cmedarea_total" type="text" class="form-control"  placeholder="Area Total"> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Ancho:
              </label>
              <input id="cmedancho" type="text" class="form-control"  placeholder="Ancho"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
                Largo:
              </label>
              <input id="cmedlargo" type="text" class="form-control"  placeholder="Largo"> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Ancho de muro de cerco:
              </label>
              <input id="cmedancho_muro" type="text" class="form-control"  placeholder="Ancho_Muro"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XVI. INFORMACIÓN ADICIONAL:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
                Información Adicional:
              </label>
              <select id="cinformacion_adicional" class="form-control">
                <option value="">Seleccionar...</option>
                <option value="Mueble">Mueble</option>
                <option value="Natural">Natural</option>
                <option value="Restos Arqueológicos">Restos Arqueológicos</option>
                <option value="Ritos, mitos, tradiciones y otros">Ritos, mitos, tradiciones y otros</option>
              </select> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Observaciones:
              </label>
              <input id="cinfadicobservaciones" type="text" class="form-control" placeholder="Observaciones">
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XVIII. PATOLOGIAS:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Daños:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cpatdanos1">Asentamientos<br>
                <input type="checkbox" id="cpatdanos2">Desplomes<br>
                <input type="checkbox" id="cpatdanos3">Desprendimientos<br>
                <input type="checkbox" id="cpatdanos4">Humedad<br>
                <input type="checkbox" id="cpatdanos5">Otros<br>
                <input type="checkbox" id="cpatdanos6">Podredumbre<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Otros:
              </label>
              <input id="cpatotros" type="text" class="form-control"  placeholder="otros"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Observaciones:
              </label>
              <textarea name="textarea" rows="4" cols="100" class="form-control " id="cpatobservaciones"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Causas:
              </label> 
            </div>
            <div class="form-group col-md-4">
              <label class="control-label">
                <input type="checkbox" id="cpatcausas1">Abandono<br>
                <input type="checkbox" id="cpatcausas2">Botánicos<br>
                <input type="checkbox" id="cpatcausas3">Calidad de los materiales<br>
                <input type="checkbox" id="cpatcausas4">Catastrofe Natural<br>
                <input type="checkbox" id="cpatcausas5">Deficiencias constructivas<br>
                <input type="checkbox" id="cpatcausas6">Falta de mantenimiento<br>
                <input type="checkbox" id="cpatcausas7">Otros<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Otros:
              </label>
              <input id="cpatotros" type="text" class="form-control"  placeholder="otros"> 
            </div>
          </div>
          <!--inicio form6-->
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XIX. VALORACIÓN:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Histórico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvalhistorico1">Espacio de valor testimonial y documental que ilustra el desarrollo político, social, religioso, cultural, económico y de forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad<br>
                <input type="checkbox" id="cvalhistorico2">Espacio que desde su creación se constituye en un hito en la memoria histórica<br>
                <input type="checkbox" id="cvalhistorico3">Espacio que en el trascurso histórico se constituyo en hito, al ser escenario relacionado con personas o eventos importantes, dentro del proceso histórico, social, cultural, económico de las comunidades<br>
                <input type="checkbox" id="cvalhistorico4">Espacios que forman parte de un conjunto histórico<br>
                <input type="checkbox" id="cvalhistorico5">Espacio que posee elementos arqueológicos que documentan su evolución y desarrollo<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Artistico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvalartistico1">Ejemplo sobresaliente por su singularidad<br>
                <input type="checkbox" id="cvalartistico2">Conservar elementos arquitectónicos y tradicionales de interés<br>
                <input type="checkbox" id="cvalartistico3">Espacio poseedor de manifestaciones artísticas y decorativas de interés<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Arquitectónico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvalarquitectonico1">Poseedor de caracteristicas tipológicas representativas de estilos arquitectónicos<br>
                <input type="checkbox" id="cvalarquitectonico2">Con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalle<br>
                <input type="checkbox" id="cvalarquitectonico3">Espacio poseedor de manifestaciones artísticas y decorativas de interés<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Tecnológico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvaltecnologico1">Espacio donde la aplicación de técnicas contructivas son singulares o de especial interés<br>
                <input type="checkbox" id="cvaltecnologico2">Espacio que se constituye en un exponente de las técnicas constructivas y uso de los materiales característicos de una época o región determinada<br>
                <input type="checkbox" id="cvaltecnologico3">Espacios con sistemas constructivos y elementos arquitectónicos realizados por su mano de obra especializada<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Integridad:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvalintegridad1">Espacio que conserva el total de su tipología, materiales y técnicas constructivas originales<br>
                <input type="checkbox" id="cvalintegridad2">Espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales<br>
              </label>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Urbano:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvalurbano1">Espacio que contribuye a definir un entorno de valor por su configuración y calidad en la estructura urbanística, el paisaje y/o el espacio público<br>
                <input type="checkbox" id="cvalurbano2">Considerado hito de referencia por su emplazamiento<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Intangible:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvalintangible1">Espacio relacionado con la organización social o forma de vida: usos, representacines, expresiones conocimientos y técnicas que las comunidades y grupos sociales reconozcan como parte de su patrimonio<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Simbólico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="cvalsimbolico1">Espacio o espacio abierto único que expresa representa o significa identidad y pertenencia para el colectivo<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XX. CRITERIOS DE VALORACIÓN:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Histórico:
              </label>
              <textarea name="textarea"  class="form-control " id="ccrivalhistorico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Artistico:
              </label>
              <textarea name="textarea" class="form-control " id="ccrivalartistico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Arquitectónico:
              </label>
              <textarea name="textarea"  class="form-control " id="ccrivalarquitectonico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Tecnológicos:
              </label>
              <textarea name="textarea" class="form-control " id="ccrivaltecnologicos"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Integridad:
              </label>
              <textarea name="textarea" class="form-control " id="ccrivalintegridad"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Urbano:
              </label>
              <textarea name="textarea" class="form-control " id="ccrivalurbano"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Intangible:
              </label>
              <textarea name="textarea" class="form-control " id="ccrivalintangible"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Simbólico:
              </label>
              <textarea name="textarea"  class="form-control " id="ccrivalsimbolico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                PERSONAL ENCARGADO:
              </label>
            </div>
          </div>
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <label class="control-label">Inventariador:</label>
              <input type="text" class="form-control" id="cperencinventariador" placeholder="Inventariador">
            </div>
          </div>
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <label class="control-label">Revisado:</label>
              <input type="text" class="form-control" id="cperrenrevisado" placeholder="Revisado">
            </div>
          </div>
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <label for="inputPassword3" class="control-label">Digitalizado:</label>
              <input type="text" class="form-control" id="cperencdigitalizado" placeholder="Digitalizado:">
            </div>
          </div>


<!--Fin form-->
<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
  <button class="btn btn-default"  type="reset" onClick="limpiar()">
    <i class="fa fa-eraser"></i> Limpiar
  </button>
  <a type = "button" class = "btn btn-primary"  id="guardarEspaciosAbiertos" name="guardarEspaciosAbiertos" data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
</div> 
</div>
{!! Form::close() !!}
</div>
</div>
</div>
