<style>
#tdborde{
  border: 1px solid #a3c2c2;
  border-collapse: border-bottom-width;
  padding:5;
  text-align: center;
  font-size: 12px;
  font-family: Arial;
  background: #d7dbdd;
}
#thtitle{
  padding:10;
  text-align: center;
}
#tdbody{
  border: 1px solid #a3c2c2;
  border-collapse: border-bottom-width;
  padding:5;
  font-size: 12px;
  font-family: Arial;
}

</style>
<div class="modal fade modal-default" id="myUpdateEspacioAbierto" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical espacioAbierto-validate','id'=>'formNuevaFicha_update'])!!}
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h3>
              Actualizar Ficha Espacio Abierto
              <small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h3>
          </div>
        </section>
      </div>   
    </div>
</div>
<div  style="padding:20px">
<input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
<input type="hidden" name="acas_id" id="acas_id">
<input type="hidden" name="ag_tipo" id="ag_tipo">
<input type="hidden" name="aespnumero_ficha" id="aespnumero_ficha">
<input type="hidden" name="aespcodigo_ficha" id="aespcodigo_ficha">

    
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       I. IDENTIFICACIÓN Y SERVICIOS:
      </label>
    </div>
</div>    
<div class="col-md-12">
   <div class="form-group col-md-6">
      <label class="control-label">
        Creador:
      </label>
      <input id="acreador" type="text" class="form-control"  placeholder="Creador" disabled> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       Autorizador:
     </label>
     <input id="aautorizador" type="text" class="form-control"  placeholder="Autorizador" disabled> 
   </div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       DATOS GENERALES:
      </label>
    </div>
 </div>
 <div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
    Código Catastral:
    </label>
    <input id="acodigocatastral"  type="text" class="form-control"  placeholder="Código Catastral">
    El código catastral tiene este formato (000-0000-0000-0000) 
  </div> 
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       GEOREFERENCIA:
      </label>
    </div>
 </div>
  <div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
    Georeferencia:
    </label>
    <input id="ageoreferencia"  type="text" class="form-control"  placeholder="Georeferencia">    
  </div> 
</div>
<!--inicio Puntos Colineales juan-->
<div class="col-md-12"><hr> 
  <center><b> Puntos Colineales</b></center>  
  <label class="control-label col-md-12">
  <table width="100%">
    <tr>
        <td width="47%" id="tdborde" >Nro</td>
        <td width="46%" id="tdborde">Punto Colineal</td>
          <td width="5%">
           <a type="button" style="cursor:pointer;">
          <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarPuntosColineales();"></i>
          </a>
        </td>
      </tr>
    </table>
  </label>
</div>
<div id="editarGrillaPuntosColinealesado"></div>
 <!--Fin Puntos Colineales-->

<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       DENOMINACIÓN DEL INMUEBLE:
      </label>
    </div>
 </div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Tradicional:
 </label>
 <input id="atradicional" type="text" class="form-control"  placeholder="Tradicional"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Actual:
  </label>
  <input id="aactual" type="text" class="form-control"  placeholder="Actual"> 
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       LOCALIZACIÓN:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Departamento:
 </label>
 <input id="adepartamento" type="text" class="form-control"  placeholder="Departamento" disabled> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Provincia:
  </label>
  <input id="aprovincia" type="text" class="form-control"  placeholder="Provincia" disabled> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Municipio:
 </label>
 <input id="amunicipio" type="text" class="form-control"  placeholder="Municipio" disabled> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Macro-distrito:
  </label>
  <select id="amacrodistritoespabie" name="amacrodistritoespabie" class="form-control">    
  </select> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Barrio:
 </label>
 <input id="abarrio" type="text" class="form-control"  placeholder="Barrio"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    Dirección calle:
  </label>
  <input id="adireccioncalle" type="text" class="form-control"  placeholder="Dirección calle"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Esquina o entre calles 1:
 </label>
 <input id="aesquina1" type="text" class="form-control"  placeholder=" Esquina o entre calle 1"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
   esquina o entre calles 2:
  </label>
  <input id="aesquina2" type="text" class="form-control"  placeholder="esquina o entre calle 2"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Altitud(m.s.n.m.):
 </label>
 <input id="aaltitud" type="text" class="form-control"  placeholder="Altitud"> 
</div>
<div class="form-group col-md-6">
</div>
</div>
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       RÉGIMEN DE PROPIEDAD:
      </label>
    </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  Nombre del propietario:
 </label>
 <input id="anombrepropietario" type="text" class="form-control"  placeholder="Nombre del propietario"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
  Tipo de Reg. Propiedad:
  </label>
  <select id="atiporegpropietario" class="form-control">
    <option value="">Seleccionar...</option>
    <option value="Publico Estatal">Público Estatal</option>
    <option value="Publico Municipal">Público Municipal</option>
    <option value="Publico Particular">Público Particular</option>
    <option value="Publico Religioso">Público Religioso</option>
  </select>
</div>
</div>
<!--hasta aqui-->
        <!--Inicio Form-->
        <!--Inicio Grilla-->
        <div class="col-md-12">
        <center><b>Uso de Suelo</b></center> 
          <label class="control-label col-md-12"> 
            <table width="100%">
              <tr>
                <td width="40%" id="tdborde">Descripción</td>
                <td width="30%" id="tdborde">Tradicional</td>
                <td width="30%" id="tdborde">Actual</td>
                <td width="1%">
                 <a type="button" style="cursor:pointer;">
                 <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarUsoSuelo();"></i>
                 </a>
                </td>
              </tr>
            </table>
          </label>
        </div>
      <div id="acrearGrillaUso"></div>
        <!--fin Grilla-->
       <!--inicio grilla-->
        <div class="col-md-12">
          <center><b>Servicios</b></center> 
          <label class="control-label col-md-12"> 
            <table width = "100%">
              <tr>
                <td width = "48%" id = "tdborde">Descripción</td>
                <td width = "48%" id = "tdborde">Proveedor</td>
                 <td width="2%">
                 <a   type="button" style="cursor:pointer;">
                 <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarServicio();"></i>
                 </a>
                </td>
              </tr>
            </table>
          </label>
         <div id="acrearGrillaServicio"></div>
        </div>
        <!--fin Grilla-->
        <div class="col-md-12">
          <div class="form-group col-md-4">
            <label class="control-label">
              Otros Servicios
            </label> 
          </div>
          <div class="form-group col-md-6" >
            <label class="control-label">
              <input type="checkbox" id="aregprootrosservicios1">Internet<br>
              <input type="checkbox" id="aregprootrosservicios2">Teléfono<br>
              <input type="checkbox" id="aregprootrosservicios3">TV Cable 
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              II. MARCO LEGAL:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Figura de Protección Legal:
            </label>
            <select id="amarlegfiguraproyeccion" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Monumento Internacional">Monumento Internacional</option>
              <option value="Monumento Local">Monumento Local</option>
              <option value="Monumento Nacional">Monumento Nacional</option>
              <option value="Sin declaratoría específica">Sin declaratoría específica</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              III. REFERENCIA HISTÓRICA:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Referencia Histórica:
            </label>
            <select id="arefhisreferenciahistorica" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Nivel Internacional">Nivel Internacional</option>
              <option value="Nivel Nacional">Nivel Nacional</option>
              <option value="Nivel Departamental">Nivel Departamental</option>
              <option value="Nivel Municipal">Nivel Municipal</option>
              <option value="Nivel Local">Nivel Local</option>
              <option value="Ninguna">Ninguna</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              IV. DATOS ORALES:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Fecha de Construcción:
            </label>
            <input id="adatorafechaconstruccion" type="text" class="form-control"  placeholder="Fecha de Construcción"> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Autor o Constructor:
            </label>
            <input id="adatorautorconstructor" type="text" class="form-control"  placeholder="Autor o Constructor">
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Preexistencia Edificación Intervenciones:
            </label>
            <textarea rows="4" cols="50" id="adatorapreexistenciaedificacion"  class="form-control "  placeholder="Preexistencia Edificación Intervenciones"></textarea>  
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Propietario:
            </label>
            <input id="adatorapropietario" type="text" class="form-control"  placeholder="Propietario:"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Usos Originales:
            </label>
            <textarea rows="4" cols="50" id="adatorausosoriginales" class="form-control " placeholder="Usos Originales:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Hechos históricos Distinguidos:
            </label>
            <textarea rows="4" cols="50" id="adatorahechoshistoricosdist" class="form-control " placeholder="Hechos Históricos Distinguidos:"></textarea>  
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fiestas Religiosas:
            </label>
            <textarea rows="4" cols="50" id="adatorafiestasreligiosas" class="form-control " placeholder="Fiestas Religiosas" ></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Datos Históricos del Conjunto:
            </label>
            <textarea rows="4" cols="50" id="adatoradatoshistoricos" class="form-control " placeholder="Datos Históricos del Conjunto:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fuentes/Bibliografía:
            </label>
            <textarea rows="4" cols="50" id="adatorafuentebibliografia" type="text" class="form-control " placeholder="Fuentes/Bibliografia"></textarea>  
          </div>
        </div>
        <!--Fin Formulario 2-->
        <!--Inicio Formulacio 3-->
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              V. DATOS DOCUMENTALES:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Fecha de construcción:
            </label>
            <input id="adatdocfechadeconstruccion" type="date" class="form-control" placeholder="Fecha de Construcción"> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Autor o Constructor:
            </label>
            <input id="adatdocautoroconstructor" type="text" class="form-control" placeholder="Autor o Constructor">
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Preexistencia/Edificación/Intervenciones:
            </label>
            <textarea rows="4" cols="50" id="adatdocpreexistenciainvestigacion" class="form-control " placeholder="Preexistencia/Edificación/Intervenciones"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Propietario:
            </label>
            <input id="adatdocpropietario" type="text" class="form-control"  placeholder="Propietario:"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Usos preexistentes:
            </label>
            <textarea  rows="4" cols="50" id="adatdocusospreexistentes"  class="form-control " placeholder="Usos preexistentes:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Hechos historicos/pers. distinguidos:
            </label>
            <textarea rows="4" cols="50" id="adatdochechoshistoricospers"  class="form-control "  placeholder="Hechos Históricos Distinguidos:"></textarea>  
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fiestas Religiosas:
            </label>
            <textarea rows="4" cols="50" id="adatdocfiestasreli"  class="form-control "  placeholder="Fiestas Religiosas" ></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Datos Históricos del Conjunto:
            </label>
            <textarea rows="4" cols="50" id="adatdocdatoshistoricosconj"  class="form-control "  placeholder="Datos Históricos:"></textarea> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label">
              Fuentes/Biografía:
            </label>
            <textarea rows="4" cols="50" id="adatdocfuentebiblio" type="text" class="form-control " placeholder="Bibliografia"></textarea>  
          </div>
        </div>
        <!--Fin Form 3-->

        <!--inicio form 4-->
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              VI. INSCRIPCIONES:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Inscripciones:
            </label>
            <textarea name="textarea" rows="4" cols="100" class="form-control " id="ainsinscripciones"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              VII. ÉPOCA:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Época:
            </label>
            <select id="aepoepoca" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="1546-1825">1546-1825</option>
              <option value="1826-1850">1826-1850</option>
              <option value="1851-1900">1851-1900</option>
              <option value="1901-1930">1901-1930</option>
              <option value="1931-1960">1931-1960</option>
              <option value="1961-1980">1961-1980</option>
              <option value="1981-2000">1981-2000</option>
              <option value="2000 en adelante">2000 en adelante</option>
            </select> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Año de construcción:
            </label>
            <input id="aepoanoconstruccion" type="text" class="form-control"  placeholder="Año de construcción"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Año de ultima remodelación:
            </label>
            <input id="aepoanoremodelacion" type="text" class="form-control"  placeholder="Año de ultima remodelación"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              VIII. ESTILO:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estilo:
            </label>
            <select id="aestestilo" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Academicista">Academicista</option>
              <option value="Arq. Noveau">Arq. Noveau</option>
              <option value="Arte Decó">Arte Decó</option>
              <option value="Barroco">Barroco</option>
              <option value="Barroco Mestizo">Barroco Mestizo</option>
              <option value="Brutalista">Brutalista</option>
              <option value="Californiano">Californiano</option>
              <option value="Clasicismo postmoderno">Clasicismo postmoderno</option>
              <option value="Clasicista">Clasicista</option>
              <option value="Colonial">Colonial</option>
              <option value="Constructivista">Constructivista</option>
              <option value="Contemporaneo">Contemporaneo</option>
              <option value="Cubista">Cubista</option>
              <option value="Ecléctico">Ecléctico</option>
              <option value="Estilo no definido">Estilo no definido</option>
              <option value="Expresionista">Expresionista</option>
              <option value="High Tech">High Tech</option>
              <option value="Historicista">Historicista</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              IX. DESCRIPCIÓN DEL AREA:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Ubicación del espacio en el area
            </label>
            <select id="adesareubicacionespacio" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Aisaldo">Aisaldo</option>
              <option value="Corazon de Manzana">Corazon de Manzana</option>
              <option value="Esquinero">Esquinero</option>
              <option value="Manzana Completa">Manzana Completa</option>
              <option value="Media Manzana">Media Manzana</option>
              <option value="Medianero">Medianero</option>
              <option value="Salar">Salar</option>
            </select> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Linea de construcción
            </label>
            <select id="adesarelineaconstruccion" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Alineada">Alineada</option>
              <option value="Remetida">Remetida</option>
              <option value="Sobresalida">Sobresalida</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Tipología:
            </label>
            <select id="adesaretipologia" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Base Colina">Base Colina</option>
              <option value="Borde de Quebrada">Borde de Quebrada</option>
              <option value="Colina Solitaria">Colina Solitaria</option>
              <option value="Enclave">Enclave</option>
              <option value="Final de Meseta">Final de Meseta</option>
              <option value="Final de Vía">Final de Vía</option>
              <option value="Incluido en el área">Incluido en el área</option>
              <option value="Puente">Puente</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              Muros de Delimitación:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-2">
            <label class="control-label">
              Material:
            </label> 
          </div>
          <div class="form-group col-md-10">
            <label class="control-label">
              <input type="checkbox" id="adesarematerial1" >Adobe<br>
              <input type="checkbox" id="adesarematerial2" > Concreto Armado<br>
              <input type="checkbox" id="adesarematerial3" > Hormigon Armado<br>
              <input type="checkbox" id="adesarematerial4" > Ladrillo de Barro<br>
              <input type="checkbox" id="adesarematerial5" > Madera<br>
              <input type="checkbox" id="adesarematerial6" > Otros<br>
              <input type="checkbox" id="adesarematerial7" > Piedra<br>
              <input type="checkbox" id="adesarematerial8" > Piedra Cal<br>
              <input type="checkbox" id="adesarematerial9" > Prefabricados<br>
              <input type="checkbox" id="adesarematerial10"> Tapial <br>
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Otros:
            </label>
           <input id="adesareotros"  type="text" class="form-control"  placeholder="Otros"> 
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestado" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Integridad:
            </label>
            <select id="adesareintegridad" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Modificado">Modificado</option>
              <option value="Nuevo">Nuevo</option>
              <option value="Original">Original</option>
              <option value="Original y Nuevo">Original y Nuevo</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <hr> 
          <center><b>Rejas</b></center> 
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros desc</td>
                <td width="20%" id="tdborde">Integridad</td>
                <td width="5%">
                <a  type="button" style="cursor:pointer;">
                <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarRejas();"></i>
                </a>
                </td>
              </tr>
            </table>
          </label> 
          <br>
          <br>
          <div id="acrearGrillaRejas"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadorejas" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <hr> 
          <center><b>Acabados de Muros</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarAcabadoMuro();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
         <div id="acrearGrillaAcabado"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadoacabadomur" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <hr> 
          <center><b>Pinturas</b></center> 
          <label class="control-label col-md-12"> 
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otra descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                <td width="5%">
                <a   type="button" style="cursor:pointer;">
                <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarPinturas();"></i>
                </a>
                </td>
            </table>
          </label>
          <br>
          <div id="acrearGrillaPinturas"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadopint" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
     <div class="col-md-12">
          <hr> 
          <center><b>Pisos</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarPiso();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
        <div id="acrearGrillaPiso"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadopisos" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
      <div class="col-md-12">
          <hr> 
          <center><b>Puertas de Acceso</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
                <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                <td width="5%">
                <a  type="button" style="cursor:pointer;">
                <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarPuertasAcceso();"></i>
                </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <div id="acrearGrillaPuertasAcceso"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadopuertasacc" class="form-control">
             <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
          <div class="col-md-12">
          <hr> 
          <center><b>Mobiliario Urbano</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarMobiliarioUrbano();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <div id="acrearGrillaMovilidadUrbanoado"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadomoviliariourb" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <hr> 
          <center><b>Escaleras</b></center> 
          <label class="control-label col-md-12"> 
            <table width="100%">
              <tr>
                <td width="20%" id="tdborde">Descripcion</td>
                <td width="35%" id="tdborde">Material</td>
                <td width="20%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                 <td width="5%">
                  <a   type="button" style="cursor:pointer;">
                  <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarEscaleras();"></i>
                  </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <div id="acrearGrillaEscalerasado"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadoescaleras" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
         <div class="col-md-12">
          <hr> 
          <center><b>Caracterísiticas de la Vegetación</b></center> 
          <label class="control-label col-md-12"> 
            <table width="100%">
              <tr>
                <td width="15%" id="tdborde">Descripcion</td>
                <td width="20%" id="tdborde">Cantidad</td>
                <td width="25%" id="tdborde">tipos</td>
                <td width="15%" id="tdborde">Otros descripción</td>
                <td width="20%" id="tdborde">Integridad</td>
                <td width="5%">
                <a   type="button" style="cursor:pointer;">
                <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarCarVegetacion();"></i>
                </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <div id="acrearGrillaCarVegetacionado"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadocaracteristicasvege" class="form-control">
               <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <div class="col-md-12">
          <hr> 
          <center><b>Detalles Artisitcos</b></center>  
          <label class="control-label col-md-12">
            <table width="100%">
              <tr>
                <td width="46%" id="tdborde">Descripcion</td>
                <td width="46%" id="tdborde">Integridad</td>
                <td width="5%">
                <a   type="button" style="cursor:pointer;">
                <i class="fa fa-plus-square fa-2x fa-fw" style="color:#29b6f6" onclick="actualizarDetaArtistoco();"></i>
                </a>
                </td>
              </tr>
            </table>
          </label>
          <br>
          <div id="acrearGrillaDetaArtistocoado"></div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Estado:
            </label>
            <select id="adesareestadodetalleartistico" class="form-control">
              <option value="">Seleccionar...</option>
              <option value="Bueno">Bueno</option>
              <option value="Malo">Malo</option>
              <option value="Regular">Regular</option>
            </select> 
          </div>
        </div>
        <!--fin form5-->
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              X. ESQUEMAS:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-12">
            <label class="control-label">
              Descripción:
            </label>
            <textarea name="textarea" class="form-control " id="aesqesquemas"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel-heading">
            <label class="control-label" style="color:#275c26";>
              XI. ANGULO VISUAL HORIZONTAL:
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Orientación
            </label>
            <input id="aangvisorientacion" type="text" class="form-control"  placeholder="Orientacion"></div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Grados
              </label>
              <input id="aangvisgrados" type="text" class="form-control"  placeholder="Grados"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                Vistas:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Material:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="aangvisvistas1">Vista Parcial<br>
                <input type="checkbox" id="aangvisvistas2">Vista Panorámica<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Otros:
              </label>
              <input id="aangvisotros" type="text" class="form-control"  placeholder="Otros"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XII. ELEMENTOS VISUALES:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                De izquierda a derecha:
              </label>
              <textarea name="textarea" class="form-control " id="aelevisizquierda_derecha"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                A lo alto:
              </label>
              <textarea name="textarea" class="form-control " id="aelivisalto"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                A lo bajo:
              </label>
              <textarea name="textarea" class="form-control " id="aelivisbajo"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XIII. DESCRIPCIÓN DEL ENTORNO:
              </label>
            </div>
            <div class="form-group col-md-12">
              <label class="control-label">
                Des. entorno inmediato:
              </label>
              <textarea name="textarea" rows="4" cols="100" class="form-control " id="adesententorno_inmediato"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XIV. DESCRIPCIÓN DEL ESPACIO:
              </label>
            </div>
            <div class="form-group col-md-12">
              <label class="control-label">
                Descripción del espacio:
              </label>
              <textarea name="textarea" rows="4" cols="100" class="form-control " id="adesespdescrpcion_espacio"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XV. MEDIDAS:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
                Área total(m2):
              </label>
              <input id="amedarea_total" type="text" class="form-control"  placeholder="Area Total"> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Ancho:
              </label>
              <input id="amedancho" type="text" class="form-control"  placeholder="Ancho"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
                Largo:
              </label>
              <input id="amedlargo" type="text" class="form-control"  placeholder="Largo"> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Ancho de muro de cerco:
              </label>
              <input id="amedancho_muro" type="text" class="form-control"  placeholder="Ancho_Muro"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XVI. INFORMACIÓN ADICIONAL:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="control-label">
                Información Adicional:
              </label>
              <select id="ainformacion_adicional" class="form-control">
                <option value="">Seleccionar...</option>
                <option value="Mueble">Mueble</option>
                <option value="Natural">Natural</option>
                <option value="Restos Arqueológicos">Restos Arqueológicos</option>
                <option value="Ritos, mitos, tradiciones y otros">Ritos, mitos, tradiciones y otros</option>
              </select> 
            </div>
            <div class="form-group col-md-6">
              <label class="control-label">
                Observaciones:
              </label>
              <input id="ainfadicobservaciones" type="text" class="form-control"  placeholder="Observaciones"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XVIII. PATOLOGIAS:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Daños:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="apatdanos1">Asentamientos<br>
                <input type="checkbox" id="apatdanos2">Desplomes<br>
                <input type="checkbox" id="apatdanos3">Desprendimientos<br>
                <input type="checkbox" id="apatdanos4">Humedad<br>
                <input type="checkbox" id="apatdanos5">Otros<br>
                <input type="checkbox" id="apatdanos6">Podredumbre<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Otros:
              </label>
              <input id="apatotros" type="text" class="form-control"  placeholder="otros"> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Observaciones:
              </label>
              <textarea name="textarea" rows="4" cols="100" class="form-control " id="apatobservaciones"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Causas:
              </label> 
            </div>
            <div class="form-group col-md-4">
              <label class="control-label">
                <input type="checkbox" id="apatcausas1">Abandono<br>
                <input type="checkbox" id="apatcausas2">Botánicos<br>
                <input type="checkbox" id="apatcausas3">Calidad de los materiales<br>
                <input type="checkbox" id="apatcausas4">Catastrofe Natural<br>
                <input type="checkbox" id="apatcausas5">Deficiencias constructivas<br>
                <input type="checkbox" id="apatcausas6">Falta de mantenimiento<br>
                <input type="checkbox" id="apatcausas7">Otros<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Otros:
              </label>
              <input id="apatotros" type="text" class="form-control"  placeholder="otros"> 
            </div>
          </div>
          <!--inicio form6-->
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XIX. VALORACIÓN:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Histórico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avalhistorico1">Espacio de valor testimonial y documental que ilustra el desarrollo político, social, religioso, cultural, económico y de forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad.<br>
                <input type="checkbox" id="avalhistorico2">Espacio que desde su creación se constituye en un hito en la memoria histórica.<br>
                <input type="checkbox" id="avalhistorico3">Espacio que en el trascurso histórico se constituyo en hito, al ser escenario relacionado con personas o eventos importantes, dentro del proceso histórico, social, cultural, económico de las comunidades.<br>
                <input type="checkbox" id="avalhistorico4">Espacios que forman parte de un conjunto histórico.<br>
                <input type="checkbox" id="avalhistorico5">Espacio que posee elementos arqueológicos que documentan su evolución y desarrollo.<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Artistico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avalartistico1">Ejemplo sobresaliente por su singularidad<br>
                <input type="checkbox" id="avalartistico2">Conservar elementos arquitectónicos y tradicionales de interés<br>
                <input type="checkbox" id="avalartistico3">Espacio poseedor de manifestaciones artísticas y decorativas de interés<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Arquitectónico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avalarquitectonico1">Poseedor de caracteristicas tipológicas representativas de estilos arquitectónicos.<br>
                <input type="checkbox" id="avalarquitectonico2">Con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalle<br>
                <input type="checkbox" id="avalarquitectonico3">Espacio poseedor de manifestaciones artísticas y decorativas de interés<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Tecnológico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avaltecnologico1">Espacio donde la aplicación de técnicas contructivas son singulares o de especial interés.<br>
                <input type="checkbox" id="avaltecnologico2">Espacio que se constituye en un exponente de las técnicas constructivas y uso de los materiales característicos de una época o región determinada<br>
                <input type="checkbox" id="avaltecnologico3">Espacios con sistemas constructivos y elementos arquitectónicos realizados por su mano de obra especializada.<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Integridad:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avalintegridad1"">Espacio que conserva el total de su tipología, materiales y técnicas constructivas originales.<br>
                <input type="checkbox" id="avalintegridad2">Espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Urbano:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avalurbano1">Espacio que contribuye a definir un entorno de valor por su configuración y calidad en la estructura urbanística, el paisaje y/o el espacio público.<br>
                <input type="checkbox" id="avalurbano2">Considerado hito de referencia por su emplazamiento.<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Intangible:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avalintangible1">Espacio relacionado con la organización social o forma de vida: usos, representacines, expresiones conocimientos y técnicas que las comunidades y grupos sociales reconozcan como parte de su patrimonio.<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-2">
              <label class="control-label">
                Simbólico:
              </label> 
            </div>
            <div class="form-group col-md-10">
              <label class="control-label">
                <input type="checkbox" id="avalsimbolico1">Espacio o espacio abierto único que expresa, representa, o significa identidad y pertenencia para el colectivo.<br>
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                XX. CRITERIOS DE VALORACIÓN:
              </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Histórico:
              </label>
              <textarea name="textarea"  class="form-control " id="acrivalhistorico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Artistico:
              </label>
              <textarea name="textarea" class="form-control " id="acrivalartistico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Arquitectónico:
              </label>
              <textarea name="textarea"  class="form-control " id="acrivalarquitectonico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Tecnológicos:
              </label>
              <textarea name="textarea" class="form-control " id="acrivaltecnologicos"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Integridad:
              </label>
              <textarea name="textarea" class="form-control " id="acrivalintegridad"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Urbano:
              </label>
              <textarea name="textarea" class="form-control " id="acrivalurbano"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Intangible:
              </label>
              <textarea name="textarea" class="form-control " id="acrivalintangible"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group col-md-12">
              <label class="control-label">
                Simbólico:
              </label>
              <textarea name="textarea"  class="form-control " id="acrivalsimbolico"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="panel-heading">
              <label class="control-label" style="color:#275c26";>
                PERSONAL ENCARGADO:
              </label>
            </div>
          </div>
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <label class="control-label">Inventariador:</label>
              <input type="text" class="form-control" id="aperencinventariador" placeholder="Inventariador">
            </div>
          </div>
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <label class="control-label">Revisado:</label>
              <input type="text" class="form-control" id="aperrenrevisado" placeholder="Revisado">
            </div>
          </div>
          <div class="col-md-12"> 
            <div class="form-group col-md-12">
              <label for="inputPassword3" class="control-label">Digitalizado:</label>
              <input type="text" class="form-control" id="aperencdigitalizado" placeholder="Digitalizado:">
            </div>
          </div>


<!--hasta aqui-->

<div class="modal-footer">
 <button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
  <a type = "button" class = "btn btn-primary"  id="actualizarEspaciosAbiertos" name="actualizarEspaciosAbiertos" data-dismiss="modal"><i class="fa fa-save"></i> Actualizar</a>        
</div> 
</div>
{!! Form::close() !!}
</div>
</div>
</div>
