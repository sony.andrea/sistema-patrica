@extends('backend.template.app')
@section('main-content')
@include('backend.patrimonioCultural.espaciosAbiertos.partial.modalCreate')
@include('backend.patrimonioCultural.espaciosAbiertos.partial.modalUpdate')
<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>OPCIONES DE BUSQUEDA - ESPACIOS ABIERTOS</b></div>
    <div class="panel-body">     
      <br>
      <div class="col-md-12">
       <div class="form-group col-md-4">
        <label class="control-label">
          Tipo Patrimonio Cultural:
        </label>
          <input type="hidden" name="tipoPatriCultural" id="tipoPatriCultural" value="4">
        <select class="form-control" name="tipoPatriCulturalMuestra" id="tipoPatriCulturalMuestra" disabled="">
          <option selected="">ESPACIOS ABIERTOS</option>
        </select>
        </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Campo:
        </label>
        <select class="form-control" name="camposPorPatrimonio" id="camposPorPatrimonio">
          <option value="-1">Seleccionar...</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Valor:
        </label>
        <input type="text" id="valorPatrimonio" class="form-control" name="valorPatrimonio">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="container">
        <div class="col-md-6"></div>
        <div class="col-md-6"> 
          <button type ="button" class = "btn btn-primary" onclick="buscarPatrimonioTipoCampo(1)"> <i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-primary" onclick="limpiarPrincipal()"> <i class="fa fa fa-eraser"></i> Limpiar</button>          
          <a class= "btn btn-primary" data-target= "#myCreateEspacioAbierto" data-toggle= "modal"><i class="glyphicon glyphicon-plus"></i> Nueva Ficha</a>
       </div>
   </div>
 </div>
</div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-body">
        <div id="listPatrimonio">
        </div>  
      </div>
    </div>
  </div>
</div>
</section>
@endsection
@push('scripts')
<script>
  var i = 20;
  var _usrid={{$usuarioId}};
  var macrodistrito_descripcion='0';
  var contespaciosabiertos = 1;
  var contconjuntos = 1;
  var contesculturas = 1;
  var correlativog="";
  console.log(_usrid,'usuarioId');

  function buscarPatrimonioPorTipo(paging_pagenumbe)
  { 
    document.getElementById("listado_campo_valor").style = "display:none;";
    document.getElementById("listado_principal").style = "display:block;";
    var paging_pagesize = $( "#reg" ).val(); 
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1206',"parametros": '{"paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'}; 
    var band = 'list_principal';
    buscar(formData, band); 
  }
  function buscarPatrimonioTipoCampo(paging_pagenumbe){
      document.getElementById("listado_campo_valor").style = "display:block;";
    document.getElementById("listado_principal").style = "display:none;";
    var paging_pagesize = $( "#reg_campo" ).val(); 
    var campoPatrimonio = $( '#camposPorPatrimonio' ).val();
    var valorPatrimonio = $( '#valorPatrimonio' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1205',"parametros": '{"xcampo_id":' +campoPatrimonio+ ', "xcampo_valor":"' +valorPatrimonio+ '","xpaging_pagesize":'+paging_pagesize+',"xpaging_pagenumber":'+paging_pagenumbe+'}'};  
    var band = 'list_por_campo'; 
    buscar(formData, band,444444444); 
  }

  function limpiarPrincipal()
  {
    $( '#tipoPatriCultural' ).val("");
    $( '#camposPorPatrimonio' ).val("");
    $( '#valorPatrimonio' ).val("");
  }

  /*function listarPatrimonioCultural(g_tipo){
    var tipo = 4;
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-933',"parametros": '{"xtipo_id":' +tipo+ '}'}; 
    buscar(formData);  
  }*/

  function generarProforma(id_cas, g_tipo){
    setTimeout(function(){
      var urlPdf= "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerar',['datos'=>'d1']) }}";
      urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirArchivoFotografico(id_cas, g_tipo){
    setTimeout(function(){   
     var urlPdf= "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarArchFoto',['datos'=>'d1']) }}";
     urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

  function imprimirFichaTodo(id_cas, g_tipo){
    setTimeout(function(){
   
     var urlFicha= "{{ action('reportesPatrimonioCultural\reporteEspacioAbierto@getGenerarFichaTodo',['datos'=>'d1']) }}";
     urlFicha = urlFicha.replace('d1', id_cas);
    ////console.log("urlFicha",urlFicha);
    window.open(urlFicha); 
  }, 500);
  }


  function buscar(formData,band){

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-patrimonio" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Nro de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código Catastral</th>';
    htmlListaBus += '<th align="left" class="form-natural">Denominación</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Fecha</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Adjuntos</th>' ;
    htmlListaBus += '<th width="15%" align="center" class="form-natural">Impresiones</th></tr>' ;
    htmlListaBus += '</thead>'; 
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
            'authorization': 'Bearer '+token,
          },
          success: function(dataIN) {
            var datos = dataIN;
            console.log('busquedaaaaa',dataIN);
            var tam = datos.length;
  //////console.log('tam',tam);
  if(dataIN == "[{ }]"){
    tam = 0;
  }; 
  var tipoContribuyenteEnvio = $( "#tipo-contribuyente" ).val();

  for (var i = 0; i < tam; i++){ 

    var cas_id = JSON.parse(dataIN[i].data).cas_id;
    var dataP= JSON.parse(dataIN[i].data);
    var g_tipo='PTR_ESP'; 
    var numero_ficha = JSON.parse(dataIN[i].data).cas_nro_caso;
    var codigo_ficha = JSON.parse(dataIN[i].data).cas_nombre_caso;
    var codigo_catastral = JSON.parse(dataIN[i].data).ptr_cod_cat;
    var fecha_registro = JSON.parse(dataIN[i].data).cas_registrado;
    fecha_registro = fecha_registro.split('T');
    fecha_registro = fecha_registro[0];
    var denominacion = JSON.parse(dataIN[i].data).ptr_den;
    if(denominacion == null)
    {
      denominacion='';
    }

    htmlListaBus += '<tr>';
    htmlListaBus += '<td align="left">'+(i+1)+'</td>';
    htmlListaBus += '<td align="center">';
    htmlListaBus+='<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + cas_id +',\''+ g_tipo +'\');">';
    htmlListaBus+='<i class="glyphicon glyphicon-trash"></i></button>';
    htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateEspacioAbierto" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarEspacioAbierto('+ cas_id +');"><i class="glyphicon glyphicon-pencil"></i></button>';
    htmlListaBus += '</td>';
    htmlListaBus += '<td align="left" class="form-natural">'+numero_ficha+'</td>'; 
    htmlListaBus += '<td align="left" class="form-natural">'+codigo_ficha+'</td>';                           
    htmlListaBus += '<td align="left" class="form-natural">'+codigo_catastral+'</td>'; 
    htmlListaBus += '<td align="left" class="form-natural">'+denominacion+'</td>';                           
    htmlListaBus += '<td align="left" class="form-natural">'+fecha_registro+'</td>';
    var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
    url = url.replace('casosId1', cas_id);
    url = url.replace('numeroficha1', numero_ficha);

    htmlListaBus+='<td style="text-align: center;"><a href="'+url+'" class="btncirculo btn-xs btn-default" fa fa-plus-square pull-right"  > <i class="fa fa-folder"></i></a>';
    htmlListaBus+='</td>';
    htmlListaBus+='<td align="center">';
    htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Ficha" type="button" onClick= "generarProforma(' + cas_id +',\''+ g_tipo +'\' );">';
    htmlListaBus+='<i class="glyphicon glyphicon-print"></i></button> &nbsp;';

    htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Archivo Fotografico" type="button" onClick= "imprimirArchivoFotografico(' + cas_id +',\''+ g_tipo +'\' );">';
    htmlListaBus+='<i class="glyphicon glyphicon-camera"></i></button> &nbsp;';

    htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Todo" type="button" onClick= "imprimirFichaTodo(' + cas_id +',\''+ g_tipo +'\' );">';
    htmlListaBus+='<i class="glyphicon glyphicon-list"></i></button>';

    htmlListaBus+='</td>';

    htmlListaBus += '</tr>';
  } 
  htmlListaBus += '</table></div></div>';
  htmlListaBus +='<div>';
  htmlListaBus +='<ul class="pager">';
  if(band =='list_principal'){
  htmlListaBus +='<li><a href="#" onClick="btnAnterior_prin()">Previous</a></li>';
  htmlListaBus +='<li><a href="#" onClick="btnSiguiente_prin()">Next</a></li>';
  }else{
  htmlListaBus +='<li><a href="#" onClick="btnAnterior_campo_valor()">Previous</a></li>';
  htmlListaBus +='<li><a href="#" onClick="btnSiguiente_campo_valor()">Next</a></li>';  
  }
  htmlListaBus+='</ul>';
  htmlListaBus +='</div>';
  $( '#listPatrimonio' ).html(htmlListaBus);


},
error: function(result) {
  swal( "Error..!", "Error....", "error" );
}
});
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});
};

  
function getBool (val){
  return !!JSON.parse(String(val).toLowerCase());
}

function selectCampos()
{  
  var tipoCampoPatrimonio = $('#tipoPatriCultural').val();
  document.getElementById("camposPorPatrimonio").length=1;
  //////////console.log(tipoCampoPatrimonio,333333333333);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-931',"parametros": '{"ytipoid":'+tipoCampoPatrimonio+'}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        //////////console.log("listadoCampos",dataIN);
        for(var i = 0; i < dataIN.length; i++){
         var campo_id = dataIN[i].xcampo_id;
         var campo_tipo = JSON.parse(dataIN[i].xcampo_data).campoTipo;   
         document.getElementById("camposPorPatrimonio").innerHTML += '<option value=' +campo_id+ '>' +campo_tipo+ '</option>';            
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
}

function obtenercorrelativo(tipoPatrimonio)
{
 $.ajax({
  type        : 'GET',
  url         : urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": 'SERVICIO_PATRIMONIO-936',"parametros":'{"xtipoPat":"'+tipoPatrimonio+'"}'};
    //////////console.log("formdataaa",formData);
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
      var data=dataIN;
      correlativog=dataIN[0].sp_correlativo_patrimonio4;       
    //////////console.log("correl99999999999999999ativog",correlativog);

  },     
  error: function (xhr, status, error) { }
});
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

}


/*-----------------BEGIN ESPACIOS ABIERTOS---------------------------*/
$("#guardarEspaciosAbiertos").click(function(){
  obtenercorrelativo('PTR-ESP');
  setTimeout(function(){

    var PTR_CREADOR = $( "#ccreador" ).val();
    var PTR_ACTUALIZA = $( "#cautorizador" ).val();
    var PTR_COD_CAT = $( "#ccodigocatastral" ).val();
    var PTR_GEOREF = $( "#cgeoreferencia" ).val();
    var PTR_TRAD = $( "#ctradicional" ).val();
    var PTR_ACT = $( "#cactual" ).val();
    var PTR_DEP = $( "#cdepartamento" ).val();
    var PTR_CIPO = $( "#cprovincia" ).val();
    var PTR_MUNI = $( "#cmunicipio" ).val();
    var PTR_MACRO = $( "#cmacrodistritoespabie" ).val();
    var macrodistritoTxt=document.getElementById("cmacrodistritoespabie");
    var PTR_MACRO=macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
    var PTR_BARR = $( "#cbarrio" ).val();
    var PTR_DIR = $( "#cdireccioncalle" ).val();
    var PTR_ESQ1 = $( "#cesquina1" ).val();
    var PTR_ESQ2 = $( "#cesquina2" ).val();
    var PTR_ALT = $( "#caltitud" ).val();
    var PTR_NOMPRO = $( "#cnombrepropietario" ).val();
    var PTR_REPRO = $( "#ctiporegpropietario" ).val();
  ///------------------------------

   /////GRILLA USO SUELO /////////
    
    var contU=0;
    arrDibujoUso.forEach(function (item, index, array) 
    {
      arrCodigoUso[contU].descripU = $("#descUso"+item.id+"").val();
      arrCodigoUso[contU].tradicionalU = document.getElementById("tradicionalUso"+item.id+"").checked;
      arrCodigoUso[contU].actualU = document.getElementById("actuallUso"+item.id+"").checked;
      contU++;
    });
    var bloqueUso = '[ {"tipo": "GRD","campos": "f01_emision_G_SERV_DESC|f01_emision_G_TRAD|f01_emision_G_ACT","titulos": "Descripción|Tradicional|Actual","impresiones": "undefined|undefined|undefined|" }';
   
     for (var i = 0; i<arrCodigoUso.length;i++)
    {
    var grillaUsoSuelo = '{"f01_emision_G_ACT": "'+ cambiarBoolAString(arrCodigoUso[i].actualU)+'","f01_emision_G_TRAD": "'+cambiarBoolAString(arrCodigoUso[i].tradicionalU)+'","f01_emision_G_SERV_DESC": "'+arrCodigoUso[i].descripU+'","f01_emision_G_SERV_DESC_valor": "'+arrCodigoUso[i].descripU+'"}';
    bloqueUso = (bloqueUso.concat(',').concat(grillaUsoSuelo));
    }
    bloqueUso = bloqueUso.concat(']');
    var PTR_USO = bloqueUso; 
   
/////FIN GRILLA USO SUELO ///////
   /////GRILLA SERVICIOS /////////  
   var contU=0;
    arrDibujoSer.forEach(function (item, index, array) 
    {
      arrCodigoSer[contU].descServicio = $("#descServicio"+item.id+"").val();
      arrCodigoSer[contU].proveedorServicio = $("#proveedorServicio"+item.id+"").val();
      contU++;
    });
    var bloqueServicio = '[{"tipo": "GRD","campos": "f01_emision_G_SERV_DESC|f01_emision_G_SERV_PROV","titulos": "Descripción|Proveedor","impresiones": "undefined|undefined|"}';
     for (var i = 0; i<arrCodigoSer.length;i++)
    {
    var grillaServicio = '{"f01_emision_G_SERV_DESC": "'+arrCodigoSer[i].descServicio+'","f01_emision_G_SERV_PROV": "'+arrCodigoSer[i].proveedorServicio+'","f01_emision_G_SERV_DESC_valor": "'+arrCodigoSer[i].descServicio+'","f01_emision_G_SERV_PROV_valor": "'+arrCodigoSer[i].proveedorServicio+'"}';
    bloqueServicio = (bloqueServicio.concat(',').concat(grillaServicio));
    }
    bloqueServicio = bloqueServicio.concat(']');
    var PTR_SERV = bloqueServicio;
/////FIN GRILLA SERVICIOS ///////

//var PTR_OTHER         = $("#cregprootrosservicios").val();
var ptr_other1 = document.getElementById("cregprootrosservicios1").checked;
var ptr_other2 = document.getElementById("cregprootrosservicios2").checked;
var ptr_other3 = document.getElementById("cregprootrosservicios3").checked;
var ptr_other_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_other1+', "resvalor": "Internet"},{"resid": 285, "estado": '+ptr_other2+', "resvalor": "Teléfono"},{"resid": 286, "estado": '+ptr_other3+', "resvalor": "TV Cable"}]';
////////console.log(888888888888, PTR_OTHER);
//var PTR_FIGPRO        = $("#cmarlegfiguraproyeccion").val();
var figuraproteccionTxt=document.getElementById("cmarlegfiguraproyeccion");
var PTR_FIGPRO=figuraproteccionTxt.options[figuraproteccionTxt.selectedIndex].text;

//var PTR_REFHIST       = $("#crefhisreferenciahistorica").val();
var referenciahistoricaTxt=document.getElementById("crefhisreferenciahistorica");
var PTR_REFHIST=referenciahistoricaTxt.options[referenciahistoricaTxt.selectedIndex].text;

////console.log('ffffffffffffffffffffffffff', PTR_REFHIST);

var PTR_FECCONS       = $("#cdatorafechaconstruccion").val();
var PTR_AUTOR         = $("#cdatorautorconstructor").val();
var PTR_PREEX         = $("#cdatorapreexistenciaedificacion").val();
var PTR_PREEX = caracteresEspecialesRegistrar(PTR_PREEX);

var PTR_PROP          = $("#cdatorapropietario").val();
var PTR_USOORG        = $("#cdatorausosoriginales").val();
var PTR_USOORG = caracteresEspecialesRegistrar(PTR_USOORG);

var PTR_HECHIS        = $("#cdatorahechoshistoricosdist").val();
var PTR_HECHIS = caracteresEspecialesRegistrar(PTR_HECHIS);

var PTR_FIEREL        = $("#cdatorafiestasreligiosas").val();
var PTR_FIEREL = caracteresEspecialesRegistrar(PTR_FIEREL);

var PTR_DATHISC       = $("#cdatoradatoshistoricos").val();
var PTR_DATHISC = caracteresEspecialesRegistrar(PTR_DATHISC);

var PTR_FUENTE        = $("#cdatorafuentebibliografia").val();
var PTR_FUENTE = caracteresEspecialesRegistrar(PTR_FUENTE);

//V. DATOS DOCUMENTALES  
var PTR_FECCONST      = $("#cdatdocfechadeconstruccion").val();
var PTR_AUTCONST      = $("#cdatdocautoroconstructor").val();
var PTR_PREDIN        = $("#cdatdocpreexistenciainvestigacion").val();
var PTR_PREDIN = caracteresEspecialesRegistrar(PTR_PREDIN);

var PTR_PROPIE        = $("#cdatdocpropietario").val();
var PTR_USOPREEX      = $("#cdatdocusospreexistentes").val();
var PTR_USOPREEX = caracteresEspecialesRegistrar(PTR_USOPREEX);

var PTR_HECHISD       = $("#cdatdochechoshistoricospers").val();
var PTR_HECHISD = caracteresEspecialesRegistrar(PTR_HECHISD);

var PTR_FIERELD       = $("#cdatdocfiestasreli").val();
var PTR_FIERELD = caracteresEspecialesRegistrar(PTR_FIERELD);

var PTR_DATHISTC      = $("#cdatdocdatoshistoricosconj").val();
var PTR_DATHISTC = caracteresEspecialesRegistrar(PTR_DATHISTC);

var PTR_FUENTBIO      = $("#cdatdocfuentebiblio").val();
var PTR_FUENTBIO = caracteresEspecialesRegistrar(PTR_FUENTBIO);

//VI. INSCRIPCIONES         
var PTR_INSCR         = $("#cinsinscripciones").val();
var PTR_INSCR = caracteresEspecialesRegistrar(PTR_INSCR);

//VII. ÉPOCA   
//var PTR_EPOCA         = $("#cepoepoca").val();
var epocaTxt=document.getElementById("cepoepoca");
var PTR_EPOCA=epocaTxt.options[epocaTxt.selectedIndex].text;

var PTR_ACONS         = $("#cepoanoconstruccion").val();
var PTR_AULTR         = $("#cepoanoremodelacion").val();
//VII. ESTILO    
//var PTR_ESTILO        = $("#cestestilo").val();
var estiloTxt=document.getElementById("cestestilo");
var PTR_ESTILO=estiloTxt.options[estiloTxt.selectedIndex].text;

//IX. DESCRIPCIÓN DEL AREA
//var PTR_UBICESP       = $("#cdesareubicacionespacio").val();
var ubicacionespacioareaTxt=document.getElementById("cdesareubicacionespacio");
var PTR_UBICESP=ubicacionespacioareaTxt.options[ubicacionespacioareaTxt.selectedIndex].text;

//var PTR_LINCONS       = $("#cdesarelineaconstruccion").val();
var lineacontruccionTxt=document.getElementById("cdesarelineaconstruccion");
var PTR_LINCONS=lineacontruccionTxt.options[lineacontruccionTxt.selectedIndex].text;

//var PTR_TIPOLO        = $("#cdesaretipologia").val();
var tipologiaTxt=document.getElementById("cdesaretipologia");
var PTR_TIPOLO=tipologiaTxt.options[tipologiaTxt.selectedIndex].text;
console.log(PTR_TIPOLO,'TIPOLOGIA');

//var PTR_MURMAT        = $("#cdesarematerial").val();

var ptr_murmat1 = document.getElementById("cdesarematerial1").checked;
var ptr_murmat2 = document.getElementById("cdesarematerial2").checked;
var ptr_murmat3 = document.getElementById("cdesarematerial3").checked;
var ptr_murmat4 = document.getElementById("cdesarematerial4").checked;
var ptr_murmat5 = document.getElementById("cdesarematerial5").checked;
var ptr_murmat6 = document.getElementById("cdesarematerial6").checked;
var ptr_murmat7 = document.getElementById("cdesarematerial7").checked;
var ptr_murmat8 = document.getElementById("cdesarematerial8").checked;
var ptr_murmat9 = document.getElementById("cdesarematerial9").checked;
var ptr_murmat10 = document.getElementById("cdesarematerial10").checked;

var ptr_murmat_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_murmat1+', "resvalor": "Adobe"},{"resid": 278, "estado": '+ptr_murmat2+', "resvalor": "Concreto Armado"},{"resid": 279, "estado": '+ptr_murmat3+', "resvalor": "Hormigon Armado"},{"resid": 280, "estado": '+ptr_murmat4+', "resvalor": "Ladrillo de Barro"},{"resid": 281, "estado": '+ptr_murmat5+', "resvalor": "Madera"},{"resid": 282, "estado": '+ptr_murmat6+', "resvalor": "Otros"},{"resid": 283, "estado": '+ptr_murmat7+', "resvalor": "Piedra"},{"resid": 284, "estado": '+ptr_murmat8+', "resvalor": "Piedra Cal"},{"resid": 285, "estado": '+ptr_murmat9+', "resvalor": "Prefabricados"},{"resid": 286, "estado": '+ptr_murmat10+', "resvalor": "Tapial"}]';
////console.log(123456789,ptr_murmat_check);

var PTR_MURMAT_OTROS  = $("#cdesareotros").val();
console.log('OTROSSSSSSSSS',PTR_MURMAT_OTROS);

//var PTR_MUREST        = $("#cdesareestado").val();
var estadootrosTxt=document.getElementById("cdesareestado");
var PTR_MUREST=estadootrosTxt.options[estadootrosTxt.selectedIndex].text;

//var PTR_MURINTE       = $("#cdesareintegridad").val();
var cdesareintegridadTxt=document.getElementById("cdesareintegridad");
var PTR_MURINTE=cdesareintegridadTxt.options[cdesareintegridadTxt.selectedIndex].text;
/////BEGIN GRILLA REJAS /////////
    var cont=0;
    arrDibujoRejas.forEach(function (item, index, array) 
    {
      arrCodigoRejas[cont].descripcionR = $("#descripcionRej"+item.id+"").val();
      arrCodigoRejas[cont].hierroR = document.getElementById("hierro"+item.id+"").checked;
      arrCodigoRejas[cont].tuboR = document.getElementById("tubo"+item.id+"").checked;
      arrCodigoRejas[cont].maderaR = document.getElementById("madera"+item.id+"").checked;
      arrCodigoRejas[cont].otrosR = document.getElementById("otros"+item.id+"").checked;
      arrCodigoRejas[cont].otrosDescR = $("#otrosDesc"+item.id+"").val();
      arrCodigoRejas[cont].integridadR = $("#integridRejas"+item.id+"").val();
      cont++;
    });
     var bloqueRejas = '[{"tipo": "GRD","campos": "f01_reja_DES_|f01_reja_HIERRO_|f01_reja_TUBO_|f01_reja_MAD_|f01_reja_OTRO_|f01_reja_OTRODESC_|f01_reja_INT_","titulos": "Descripción|Hierro Forjado|Tubo Industrial|Madera|Otros|Otrosdesc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
    for (var i = 0; i<arrCodigoRejas.length;i++)
    {
      var grillaRejas = '{"f01_reja_DES_": "'+arrCodigoRejas[i].descripcionR+'","f01_reja_INT_": "'+arrCodigoRejas[i].integridadR+'","f01_reja_MAD_": "'+cambiarBoolAString(arrCodigoRejas[i].maderaR)+'","f01_reja_OTRO_": "'+cambiarBoolAString(arrCodigoRejas[i].otrosR)+'","f01_reja_TUBO_": "'+ cambiarBoolAString(arrCodigoRejas[i].tuboR)+'","f01_reja_HIERRO_": "'+cambiarBoolAString(arrCodigoRejas[i].hierroR)+'","f01_reja_OTRODESC_": "'+arrCodigoRejas[i].otrosDescR+'","f01_reja_DES__valor": "'+arrCodigoRejas[i].descripcionR+'","f01_reja_INT__valor": "'+arrCodigoRejas[i].integridadR+'"}';
      bloqueRejas = (bloqueRejas.concat(',').concat(grillaRejas));
    }
    bloqueRejas = bloqueRejas.concat(']');
    var PTR_REJAS = bloqueRejas;

    //console.log("PTR_REJAS",PTR_REJAS);
/////END GRILLA REJAS/////////

/////BEGIN GRILLA ACABADO /////////
  var cont = 0;
      arrDibujoAcab.forEach(function (item, index, array) 
      {
        arrCodigoAcab[cont].descripcionA = $("#descripcionAcab"+item.id+"").val();
        arrCodigoAcab[cont].calArenaA = document.getElementById("calArena"+item.id+"").checked;
        arrCodigoAcab[cont].cementoA = document.getElementById("cemento"+item.id+"").checked;
        arrCodigoAcab[cont].piedraA = document.getElementById("piedra"+item.id+"").checked;
        arrCodigoAcab[cont].marmolA = document.getElementById("marmol"+item.id+"").checked;
        arrCodigoAcab[cont].maderaA = document.getElementById("maderamuro"+item.id+"").checked;
        arrCodigoAcab[cont].azulejoA = document.getElementById("azulejo"+item.id+"").checked;
        arrCodigoAcab[cont].ladrilloA = document.getElementById("ladrillo"+item.id+"").checked;
        arrCodigoAcab[cont].otroAcabA = document.getElementById("otroAcab"+item.id+"").checked;
        arrCodigoAcab[cont].otroDescAcabA = $("#otrosDescAcab"+item.id+"").val();
        arrCodigoAcab[cont].integridadAcabA = $("#integridAcab"+item.id+"").val();
        cont++;
      });

      //console.log("VectorAcabadoGrillas",arrCodigoAcab);
        var bloqueAcabMuros = '[{"tipo": "GRD","campos": "f01_muro_DES_|f01_muro_CAL_|f01_muro_CEM_|f01_muro_PIE_|f01_muro_MAR_|f01_muro_MAD_|f01_muro_AZU_|f01_muro_LAD_|f01_muro_OTRO_|f01_muro_OTRODESC_|f01_muro_INT_","titulos": "Descripción|Cal y Arena|Cemento y Arena|Piedra|Mármol|Madera|Azulejo|Ladrillo|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';

        for (var i = 0; i<arrCodigoAcab.length;i++)
        {
          ////console.log("grillapruebaI"+i+"",arrCodigo[i]);
          var grillaAcabMuros = '{"f01_muro_AZU_": "'+cambiarBoolAString(arrCodigoAcab[i].azulejoA)+'","f01_muro_CAL_": "'+cambiarBoolAString(arrCodigoAcab[i].calArenaA)+'","f01_muro_CEM_": "'+cambiarBoolAString(arrCodigoAcab[i].cementoA)+'","f01_muro_DES_": "'+arrCodigoAcab[i].descripcionA+'","f01_muro_INT_": "'+arrCodigoAcab[i].integridadAcabA+'","f01_muro_LAD_": "'+cambiarBoolAString(arrCodigoAcab[i].ladrilloA)+'","f01_muro_MAD_": "'+cambiarBoolAString(arrCodigoAcab[i].maderaA)+'","f01_muro_MAR_": "'+cambiarBoolAString(arrCodigoAcab[i].marmolA)+'","f01_muro_PIE_": "'+cambiarBoolAString(arrCodigoAcab[i].piedraA)+'","f01_muro_OTRO_": "'+cambiarBoolAString(arrCodigoAcab[i].otroAcabA)+'","f01_muro_OTRODESC_": "'+arrCodigoAcab[i].otroDescAcabA+'","f01_muro_DES__valor": "'+arrCodigoAcab[i].descripcionA+'","f01_muro_INT__valor": "'+arrCodigoAcab[i].integridadAcabA+'"}';

          bloqueAcabMuros = (bloqueAcabMuros.concat(',').concat(grillaAcabMuros));

        }
        bloqueAcabMuros = bloqueAcabMuros.concat(']');

      var PTR_ACAMUR = bloqueAcabMuros;
      //console.log("PTR_ACAMUR",PTR_ACAMUR);
/////END GRILLA ACABADO/////////
/////END GRILLA PINTURAS/////////

      var cont = 0;
      arrDibujoPinturas.forEach(function (item, index, array) 
      {
        arrCodigoPinturas[cont].descripcionPintu = $("#descripcionPintu"+item.id+"").val();
        arrCodigoPinturas[cont].calArenaPintu = document.getElementById("calArenaPintu"+item.id+"").checked;
        arrCodigoPinturas[cont].acrilicoPintu = document.getElementById("acrilicoPintu"+item.id+"").checked;
        arrCodigoPinturas[cont].aceitePintu = document.getElementById("aceitePintu"+item.id+"").checked;
        arrCodigoPinturas[cont].otroPintu = document.getElementById("otroPintu"+item.id+"").checked;
        arrCodigoPinturas[cont].otrosDescPintu = $("#otrosDescPintu"+item.id+"").val();
        arrCodigoPinturas[cont].integridPintu = $("#integridPintu"+item.id+"").val();
        cont++;
      });
       var bloquePinturas = '[{"tipo": "GRD","campos": "f01_pint_COD_|f01_pint_CAL_|f01_pint_ACR_|f01_pint_ACE_|f01_pint_OTRO_|f01_pint_OTRODESC_|f01_pint_INT_","titulos": "Descripción|Cal|Acrílica|Aceite|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
        for (var i = 0; i<arrCodigoPinturas.length;i++)
        {
          var grillaPinturas = '{"f01_pint_ACE_": "'+cambiarBoolAString(arrCodigoPinturas[i].aceitePintu)+'","f01_pint_ACR_": "'+cambiarBoolAString(arrCodigoPinturas[i].acrilicoPintu)+'","f01_pint_CAL_": "'+cambiarBoolAString(arrCodigoPinturas[i].calArenaPintu)+'","f01_pint_COD_": "'+arrCodigoPinturas[i].descripcionPintu+'","f01_pint_INT_": "'+arrCodigoPinturas[i].integridPintu+'","f01_pint_OTRO_": "'+cambiarBoolAString(arrCodigoPinturas[i].otroPintu)+'","f01_pint_OTRODESC_": "'+arrCodigoPinturas[i].otrosDescPintu+'","f01_pint_COD__valor": "'+arrCodigoPinturas[i].descripcionPintu+'","f01_pint_INT__valor": "'+arrCodigoPinturas[i].integridPintu+'"}';
          bloquePinturas = (bloquePinturas.concat(',').concat(grillaPinturas));
        }
        bloquePinturas = bloquePinturas.concat(']');
       var PTR_PINTURA = bloquePinturas;

/////END GRILLA PINTURAS/////////

//var PTR_REJAEST       = $("#cdesareestadorejas").val();
var cdesareestadorejasTxt=document.getElementById("cdesareestadorejas");
var PTR_REJAEST=cdesareestadorejasTxt.options[cdesareestadorejasTxt.selectedIndex].text;


//var PTR_ACAEST        = $("#cdesareestadoacabadomur").val();
var cdesareestadoacabadomurTxt=document.getElementById("cdesareestadoacabadomur");
var PTR_ACAEST=cdesareestadoacabadomurTxt.options[cdesareestadoacabadomurTxt.selectedIndex].text;


//var PTR_PINTEST       = $("#cdesareestadopint").val();
var cdesareestadopintTxt=document.getElementById("cdesareestadopint");
var PTR_PINTEST=cdesareestadopintTxt.options[cdesareestadopintTxt.selectedIndex].text;

///-- Grillas juan inicio --\\\
///--- GRILLA Puntos Colineales --- \\\
var cont = 0;
 arrDibujoPuntosColineales.forEach(function (item, index, array){
    arrCodigoPuntosColineales[cont].NroDescPuntosColineales = $("#NroDescPuntosColineales"+item.id+"").val();
    arrCodigoPuntosColineales[cont].PuntoColinealPuntosColineales = $("#PuntoColinealPuntosColineales"+item.id+"").val();
    cont++;
  });
var bloquePuntosColiniales = '[{"tipo": "GRD","campos": "f01_Nro|f01_Pto_Colineal","titulos": "Nro|Punto Colineal","impresiones": "undefined|undefined|"}';
for (var i = 0; i<arrCodigoPuntosColineales.length;i++) {
  var grillaPuntosColiniales = '  {"f01_Nro":"'+  arrCodigoPuntosColineales[i].NroDescPuntosColineales+'", "f01_Pto_Colineal":"'+  arrCodigoPuntosColineales[i].PuntoColinealPuntosColineales+'" }';
  bloquePuntosColiniales = (bloquePuntosColiniales.concat(',').concat(grillaPuntosColiniales));
}
bloquePuntosColiniales = bloquePuntosColiniales.concat(']');
var PTR_DOT_S = bloquePuntosColiniales;
///---FIN GRILLA Puntos Colineales--- \\\
///--- GRILLA PISOS --- \\\
var cont = 0;
arrDibujoPiso.forEach(function (item, index, array){
  arrCodigoPiso[cont].descripcionPisoA = $("#descripcionPiso"+item.id+"").val();     
  arrCodigoPiso[cont].baldosasPisoA = document.getElementById("baldosasPiso"+item.id+"").checked; 
  arrCodigoPiso[cont].mosaicosPisoA = document.getElementById("mosaicosPiso"+item.id+"").checked;
  arrCodigoPiso[cont].porcelanatoPisoA = document.getElementById("porcelanatoPiso"+item.id+"").checked;
  arrCodigoPiso[cont].plajaPisoA = document.getElementById("plajaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].prioPisoA = document.getElementById("prioPiso"+item.id+"").checked; 
  arrCodigoPiso[cont].ladrillosPisoA = document.getElementById("ladrillosPiso"+item.id+"").checked;
  arrCodigoPiso[cont].gramaPisoA = document.getElementById("gramaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].tierraPisoA = document.getElementById("tierraPiso"+item.id+"").checked;
  arrCodigoPiso[cont].concretoPisoA = document.getElementById("concretoPiso"+item.id+"").checked; 
  arrCodigoPiso[cont].ceramicaPisoA = document.getElementById("ceramicaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].maderaPisoA = document.getElementById("maderaPiso"+item.id+"").checked;
  arrCodigoPiso[cont].otrosPisoA = document.getElementById("otrosPiso"+item.id+"").checked;
  arrCodigoPiso[cont].otroDescPisoA = $("#otrosDescPiso"+item.id+"").val();
  arrCodigoPiso[cont].integridadPisoA = $("#integridPiso"+item.id+"").val();
  cont++;
});
//console.log("arrCodigoPiso",arrCodigoPiso);
var bloquePisos = '[{"tipo": "GRD","campos": "f01_pis_DES_|f01_pis_BAL_|f01_pis_MOS_|f01_pis_POR_|f01_pis_LAJ_|f01_pis_RIO_|f01_pis_LAD_|f01_pis_GRA_|f01_pis_TIE_|f01_pis_CON_|f01_pis_CER_|f01_pis_MAD_|f01_pis_OTRO_|f01_pis_OTRODESC_|f01_pis_INT_", "titulos": "Descripción|Baldosas|Mosaicos|Porcelanato|P.Laja|P.Rio|Ladrillos|Grama|Tierra|Concreto|Cerámica|Madera|Otros|Otros desc.|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';

for (var i = 0; i<arrCodigoPiso.length;i++) {
  var grillaPisos = '{ "f01_pis_BAL_":"'+ cambiarBoolAString(arrCodigoPiso[i].baldosasPisoA)+'","f01_pis_CER_":"'+ cambiarBoolAString(arrCodigoPiso[i].ceramicaPisoA)+'", "f01_pis_CON_":"'+ cambiarBoolAString(arrCodigoPiso[i].concretoPisoA)+'","f01_pis_DES_":"'+ arrCodigoPiso[i].descripcionPisoA+'","f01_pis_GRA_":"'+ cambiarBoolAString(arrCodigoPiso[i].gramaPisoA)+'","f01_pis_INT_":"'+ arrCodigoPiso[i].integridadPisoA+'","f01_pis_LAD_":"'+ cambiarBoolAString(arrCodigoPiso[i].ladrillosPisoA)+'","f01_pis_LAJ_":"'+ cambiarBoolAString(arrCodigoPiso[i].plajaPisoA)+'","f01_pis_MAD_":"'+ cambiarBoolAString(arrCodigoPiso[i].maderaPisoA)+'","f01_pis_MOS_":"'+cambiarBoolAString(arrCodigoPiso[i].mosaicosPisoA)+'","f01_pis_POR_":"'+ cambiarBoolAString(arrCodigoPiso[i].porcelanatoPisoA)+'", "f01_pis_RIO_":"'+ cambiarBoolAString(arrCodigoPiso[i].prioPisoA)+'","f01_pis_TIE_":"'+ cambiarBoolAString(arrCodigoPiso[i].tierraPisoA)+'","f01_pis_OTRO_":"'+ cambiarBoolAString(arrCodigoPiso[i].otrosPisoA)+'","f01_pis_OTRODESC_":"'+ arrCodigoPiso[i].otroDescPisoA+'","f01_pis_DES__valor":"0", "f01_pis_INT__valor":"0"}';
  bloquePisos = (bloquePisos.concat(',').concat(grillaPisos));
}
bloquePisos = bloquePisos.concat(']');
var PTR_PISOS = bloquePisos;

//console.log("PTR_PISOooooosss",PTR_PISOS);
///---FIN GRILLA PISOS --- \\\

///--- GRILLA puertas de acesoS --- \\\
var cont = 0;
arrDibujoPuertasAcceso.forEach(function (item, index, array) {
  arrCodigoPuertasAcceso[cont].descripcionPuertasAcceso = $("#descripcionPuertasAcceso"+item.id+"").val();     
  arrCodigoPuertasAcceso[cont].maderaPuertaAcceso = document.getElementById("maderaPuertasAcceso"+item.id+"").checked; 
  arrCodigoPuertasAcceso[cont].metalPuertaAcceso = document.getElementById("metalPuertasAcceso"+item.id+"").checked;
  arrCodigoPuertasAcceso[cont].otrosPuertaAcceso = document.getElementById("otrosPuertasAcceso"+item.id+"").checked;
  arrCodigoPuertasAcceso[cont].otroDescPuertasAccesoA = $("#otrosDescPuertasAcceso"+item.id+"").val();
  arrCodigoPuertasAcceso[cont].integridadPuertasAccesoA = $("#integridPuertasAcceso"+item.id+"").val();
  cont++;
});
var bloquePuertasAcceso = '[{ "tipo": "GRD", "campos": "f01_pue_DES_|f01_pue_MAD_|f01_pue_MET_|f01_pue_OTRO_|f01_pue_OTRODESC_|f01_pue_INT_", "titulos": "Descripción|Madera|Metal|Otros|Otros desc.|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|"}';

for (var i = 0; i<arrDibujoPuertasAcceso.length;i++) {
  var grillaPuertasAcceso = '{"f01_pue_DES_": "'+ arrCodigoPuertasAcceso[i].descripcionPuertasAcceso+'","f01_pue_INT_": "'+ arrCodigoPuertasAcceso[i].integridadPuertasAccesoA+'","f01_pue_MAD_": "'+ cambiarBoolAString(arrCodigoPuertasAcceso[i].maderaPuertaAcceso)+'","f01_pue_MET_": "'+ cambiarBoolAString(arrCodigoPuertasAcceso[i].metalPuertaAcceso)+'","f01_pue_OTRO_": "'+ cambiarBoolAString(arrCodigoPuertasAcceso[i].otrosPuertaAcceso)+'","f01_pue_OTRODESC_": "'+ arrCodigoPuertasAcceso[i].otroDescPuertasAccesoA+'","f01_pue_DES__valor":"0","f01_pue_INT__valor":"0"}'; 

  bloquePuertasAcceso = (bloquePuertasAcceso.concat(',').concat(grillaPuertasAcceso));
}
bloquePuertasAcceso = bloquePuertasAcceso.concat(']');
var PTR_PUERTA = bloquePuertasAcceso;

//console.log("boqueGrilla",bloquePuertasAcceso)
///---FIN GRILLA puertas de acesoS --- \\\

///--- GRILLA Mobiliario Urbano --- \\\
var cont = 0;
arrDibujoMovilidadUrbano.forEach(function (item, index, array) {
  arrCodigoMovilidadUrbano[cont].descripcionMovilidadUrbano = $("#descripcionMovilidadUrbano"+item.id+"").val();     
  arrCodigoMovilidadUrbano[cont].maderaMovilidadUrbano = document.getElementById("maderamobiliariourbano"+item.id+"").checked; 
  arrCodigoMovilidadUrbano[cont].metalMovilidadUrbano = document.getElementById("metal"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].barroMovilidadUrbano = document.getElementById("barro"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].calMovilidadUrbano = document.getElementById("cal"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].ladrilloMovilidadUrbano = document.getElementById("ladrillomobiliariourbano"+item.id+"").checked; 
  arrCodigoMovilidadUrbano[cont].piedraMovilidadUrbano = document.getElementById("piedramobiliariourbano"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].adobeMovilidadUrbano = document.getElementById("adobe"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].concretoMovilidadUrbano = document.getElementById("concreto"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].fcarbonMovilidadUrbano = document.getElementById("fcarbon"+item.id+"").checked; 
  arrCodigoMovilidadUrbano[cont].vidrioMovilidadUrbano = document.getElementById("vidrio"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].fibrocemMovilidadUrbano = document.getElementById("fibrocem"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].otrosMovilidadUrbano = document.getElementById("otrosmobiliariourbano"+item.id+"").checked;
  arrCodigoMovilidadUrbano[cont].otroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDescMovilidadUrbano"+item.id+"").val();
  arrCodigoMovilidadUrbano[cont].integridadMovilidadUrbanoMovilidadUrbano = $("#integridMovilidadUrbano"+item.id+"").val();
  cont++;
});
var bloqueMovilidadUrbano = '[ {"tipo": "GRD","campos": "f01_mob_DES_|f01_mob_MAD_|f01_mob_MET_|f01_mob_BARR_|f01_mob_CAL_|f01_mob_LAD_|f01_mob_PIE_|f01_mob_ADO_|f01_mob_CON_|f01_mob_CAR_|f01_mob_VID_|f01_mob_FIBR_|f01_mob_OTRO_|f01_mob_OTRODESC_|f01_mob_INT_","titulos": "Descripción|Madera|Metal|Barro|Cal|Ladrillo|Piedra|Adobe|Concreto|F. Carbono|Vidrio|Fibrocem|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
for (var i = 0; i<arrDibujoMovilidadUrbano.length;i++) {
  var grillaMovilidadUrbano = ' { "f01_mob_ADO_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].adobeMovilidadUrbano) +'", "f01_mob_CAL_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].calMovilidadUrbano) +'", "f01_mob_CAR_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].fcarbonMovilidadUrbano) +'", "f01_mob_CON_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].concretoMovilidadUrbano) +'", "f01_mob_DES_": "'+ arrCodigoMovilidadUrbano[i].descripcionMovilidadUrbano +'", "f01_mob_INT_": "'+ arrCodigoMovilidadUrbano[i].integridadMovilidadUrbanoMovilidadUrbano +'", "f01_mob_LAD_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].ladrilloMovilidadUrbano) +'", "f01_mob_MAD_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].maderaMovilidadUrbano) +'", "f01_mob_MET_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].metalMovilidadUrbano) +'", "f01_mob_PIE_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].piedraMovilidadUrbano) +'", "f01_mob_VID_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].vidrioMovilidadUrbano) +'", "f01_mob_BARR_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].barroMovilidadUrbano) +'", "f01_mob_FIBR_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].fibrocemMovilidadUrbano) +'", "f01_mob_OTRO_": "'+ cambiarBoolAString(arrCodigoMovilidadUrbano[i].otrosMovilidadUrbano) +'", "f01_mob_OTRODESC_": "'+ arrCodigoMovilidadUrbano[i].otroDescMovilidadUrbanoMovilidadUrbano +'", "f01_mob_DES__valor":"'+ arrCodigoMovilidadUrbano[i].descripcionMovilidadUrbano +'" , "f01_mob_INT__valor":"'+ arrCodigoMovilidadUrbano[i].integridadMovilidadUrbanoMovilidadUrbano +'"}';
  bloqueMovilidadUrbano = (bloqueMovilidadUrbano.concat(',').concat(grillaMovilidadUrbano));
}
bloqueMovilidadUrbano = bloqueMovilidadUrbano.concat(']');
var PTR_MOBURB = bloqueMovilidadUrbano;
///---FIN GRILLA Mobiliario Urbano --- \\\

///--- GRILLA Escaleras --- \\\
var cont = 0;
arrDibujoEscaleras.forEach(function (item, index, array) {
  arrCodigoEscaleras[cont].descripcionEscaleras = $("#descripcionEscaleras"+item.id+"").val();     
  arrCodigoEscaleras[cont].maderaEscaleras = document.getElementById("maderaEscalerass"+item.id+"").checked; 
  arrCodigoEscaleras[cont].metalEscaleras = document.getElementById("metalEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].ConcretoEscaleras = document.getElementById("ConcretoEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].PiedraEscaleras = document.getElementById("PiedraEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].ladrilloEscaleras = document.getElementById("ladrilloEscaleras"+item.id+"").checked; 
  arrCodigoEscaleras[cont].tierraEscaleras = document.getElementById("tierraEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].otrosEscaleras = document.getElementById("otrosEscaleras"+item.id+"").checked;
  arrCodigoEscaleras[cont].otroDescEscaleras = $("#otrosDescEscaleras"+item.id+"").val();
  arrCodigoEscaleras[cont].integridadEscaleras = $("#integridEscaleras"+item.id+"").val();
  cont++;
});
var bloqueEscaleras = '[ { "tipo": "GRD", "campos": "f01_esc_DES_|f01_esc_MAD_|f01_esc_MET_|f01_esc_CON_|f01_esc_PIE_|f01_esc_LAD_|f01_esc_TIE_|f01_esc_OTRO_|f01_esc_OTRODESC_|f01_esc_INT_", "titulos": "Descripción|Madera|Metal|Concreto|Piedra|Ladrillo|Tierra|Otro|Otro desc|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
for (var i = 0; i<arrDibujoEscaleras.length;i++) {
  var grillaEscaleras = '{ "f01_esc_CON_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].ConcretoEscaleras)+'", "f01_esc_DES_":"'+ arrCodigoEscaleras[i].descripcionEscaleras+'", "f01_esc_INT_":"'+ arrCodigoEscaleras[i].integridadEscaleras+'", "f01_esc_LAD_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].ladrilloEscaleras)+'", "f01_esc_MAD_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].maderaEscaleras)+'", "f01_esc_MET_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].metalEscaleras)+'", "f01_esc_PIE_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].PiedraEscaleras)+'", "f01_esc_TIE_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].tierraEscaleras)+'", "f01_esc_OTRO_":"'+ cambiarBoolAString(arrCodigoEscaleras[i].otrosEscaleras)+'", "f01_esc_OTRODESC_":"'+ arrCodigoEscaleras[i].otroDescEscaleras+'", "f01_esc_DES__valor":"'+ arrCodigoEscaleras[i].descripcionEscaleras+'", "f01_esc_INT__valor":"'+ arrCodigoEscaleras[i].integridadEscaleras+'"}';
  bloqueEscaleras = (bloqueEscaleras.concat(',').concat(grillaEscaleras));
}
bloqueEscaleras = bloqueEscaleras.concat(']');
var PTR_ESCAL = bloqueEscaleras;

///---FIN GRILLA Escaleras --- \\\

///--- GRILLA CARACTERISTICAS DE LA VEGETACION--- \\\
var cont = 0;
arrDibujoCarVegetacion.forEach(function (item, index, array) {
  arrCodigoCarVegetacion[cont].descripcionCarVegetacion = $("#descripcionCarVegetacion"+item.id+"").val();     
  arrCodigoCarVegetacion[cont].cantidadCarVegetacion = $("#cantidadCarVegetacion"+item.id+"").val();
  arrCodigoCarVegetacion[cont].sinFloracionCarVegetacion = document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked;
  arrCodigoCarVegetacion[cont].caducaCarVegetacion = document.getElementById("caducaCarVegetacion"+item.id+"").checked; 
  arrCodigoCarVegetacion[cont].perenneCarVegetacion = document.getElementById("perenneCarVegetacion"+item.id+"").checked;
  arrCodigoCarVegetacion[cont].otrosCarVegetacion = document.getElementById("otrosCarVegetacion"+item.id+"").checked;
  arrCodigoCarVegetacion[cont].otroDescCarVegetacion = $("#otrosDescCarVegetacion"+item.id+"").val();
  arrCodigoCarVegetacion[cont].integridadCarVegetacion = $("#integridCarVegetacion"+item.id+"").val();
  cont++;
});
var bloqueCarVegetacion = '[{"tipo": "GRD","campos": "f01_veg_DES_|f01_veg_CAN_|f01_veg_SFL_|f01_veg_CAD_|f01_veg_PER_|f01_veg_OTRO_|f01_veg_OTRODESC_|f01_veg_INT_","titulos": "Descripción|Cantidad|Sin Floracion|Caduca|Perenne|Otro|Otro desc|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|" }';
for (var i = 0; i<arrDibujoCarVegetacion.length;i++) {
  var grillaCarVegetacion = ' {"f01_veg_CAD_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].caducaCarVegetacion)+'","f01_veg_CAN_":"'+ arrCodigoCarVegetacion[i].cantidadCarVegetacion+'","f01_veg_DES_":"'+ arrCodigoCarVegetacion[i].descripcionCarVegetacion+'","f01_veg_INT_":"'+ arrCodigoCarVegetacion[i].integridadCarVegetacion+'","f01_veg_PER_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].perenneCarVegetacion)+'","f01_veg_SFL_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].sinFloracionCarVegetacion)+'","f01_veg_OTRO_":"'+ cambiarBoolAString(arrCodigoCarVegetacion[i].otrosCarVegetacion)+'","f01_veg_OTRODESC_":"'+ arrCodigoCarVegetacion[i].otroDescCarVegetacion+'","f01_veg_DES__valor":"'+ arrCodigoCarVegetacion[i].descripcionCarVegetacion+'", "f01_veg_INT__valor":"'+ arrCodigoCarVegetacion[i].integridadCarVegetacion+'"}';
  bloqueCarVegetacion = (bloqueCarVegetacion.concat(',').concat(grillaCarVegetacion));
}
bloqueCarVegetacion = bloqueCarVegetacion.concat(']');
var PTR_VEGET = bloqueCarVegetacion;

///---FIN GRILLA CARCATERISTICAS DE LA VEGETACION --- \\\

///--- GRILLA Detalles Artisitcos--- \\\
var cont = 0;
  arrDibujoDetaArtistoco.forEach(function (item, index, array) 
  {
    arrCodigoDetaArtistoco[cont].descripcionDetalleArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
    arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
    cont++;
    
  });
var bloqueDetaArtistoco = '[{"tipo": "GRD","campos": "f01_det_DES_|f01_det_INT_","titulos": "Descripción|Integridad", "impresiones": "undefined|undefined|"}';
for (var i = 0; i<arrDibujoDetaArtistoco.length;i++) {
var grillaDetaArt = ' { "f01_det_DES_": "'+arrCodigoDetaArtistoco[i].descripcionDetalleArtistoco+'", "f01_det_INT_": "'+arrCodigoDetaArtistoco[i].integridadDetaArtistoco+'", "f01_det_DES__valor": "2", "f01_det_INT__valor": "3"}';
  bloqueDetaArtistoco = (bloqueDetaArtistoco.concat(',').concat(grillaDetaArt));
}
bloqueDetaArtistoco = bloqueDetaArtistoco.concat(']');
var PTR_DETALLES = bloqueDetaArtistoco;
///---FIN GRILLA Detalles Artisitcos--- \\\

/// Grillas juan Fin \\\

//var PTR_PISOEST       = $("#cdesareestadopisos").val();
var cdesareestadopisosTxt=document.getElementById("cdesareestadopisos");
var PTR_PISOEST=cdesareestadopisosTxt.options[cdesareestadopisosTxt.selectedIndex].text;

//var PTR_PUERTAEST     = $("#cdesareestadopuertasacc").val();
var cdesareestadopuertasaccTxt=document.getElementById("cdesareestadopuertasacc");
var PTR_PUERTAEST=cdesareestadopuertasaccTxt.options[cdesareestadopuertasaccTxt.selectedIndex].text;

//var PTR_MOBEST        = $("#cdesareestadomoviliariourb").val();
var cdesareestadomoviliariourbTxt=document.getElementById("cdesareestadomoviliariourb");
var PTR_MOBEST=cdesareestadomoviliariourbTxt.options[cdesareestadomoviliariourbTxt.selectedIndex].text;

//var PTR_ESCEST        = $("#cdesareestadoescaleras").val();
var cdesareestadoescalerasTxt=document.getElementById("cdesareestadoescaleras");
var PTR_ESCEST=cdesareestadoescalerasTxt.options[cdesareestadoescalerasTxt.selectedIndex].text;


//var PTR_VEGEST        = $("#cdesareestadocaracteristicasvege").val();
var cdesareestadocaracteristicasvegeTxt=document.getElementById("cdesareestadocaracteristicasvege");
var PTR_VEGEST=cdesareestadocaracteristicasvegeTxt.options[cdesareestadocaracteristicasvegeTxt.selectedIndex].text;


//var PTR_DETALLEST     = $("#cdesareestadodetalleartistico").val();
var cdesareestadodetalleartisticoTxt=document.getElementById("cdesareestadodetalleartistico");
var PTR_DETALLEST=cdesareestadodetalleartisticoTxt.options[cdesareestadodetalleartisticoTxt.selectedIndex].text;

//x. ESQUEMAS
var PTR_ESQDES        =$("#cesqesquemas").val();
var PTR_ESQDES = caracteresEspecialesRegistrar(PTR_ESQDES);

//XI. ANGULO VISUAL HORIZONTAL
var PTR_ANGOR         = $("#cangvisorientacion").val();
var PTR_ANGGRA        = $("#cangvisgrados").val();
//var PTR_VISTAS        = $("#cangvisvistas").val();
var ptr_vistas1 = document.getElementById("cangvisvistas1").checked;
var ptr_vistas2 = document.getElementById("cangvisvistas2").checked;

var ptr_vistas_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_vistas1+', "resvalor": "Vista Parcial  "},{"resid": 271, "estado": '+ptr_vistas2+', "resvalor": "Vista Panorámica"}]';

var PTR_ANGOTR        = $("#cangvisotros").val();
//XII. ELEMENTOS VISUALES
var PTR_ELEMVISID     = $("#celevisizquierda_derecha").val();
var PTR_ELEMVISID = caracteresEspecialesRegistrar(PTR_ELEMVISID);

var PTR_ELEMVISAL     = $("#celivisalto").val();
var PTR_ELEMVISAL = caracteresEspecialesRegistrar(PTR_ELEMVISAL);

var PTR_ELEMVISAB     = $("#celivisbajo").val();
var PTR_ELEMVISAB = caracteresEspecialesRegistrar(PTR_ELEMVISAB);

//XIII. DESCRIPCIÓN DEL ENTORNO
var PTR_DESENTEI      = $("#cdesententorno_inmediato").val();
var PTR_DESENTEI = caracteresEspecialesRegistrar(PTR_DESENTEI);

//XIV. DESCRIPCIÓN DEL ESPACIO
var PTR_DESESPDES     = $("#cdesespdescrpcion_espacio").val();
var PTR_DESESPDES = caracteresEspecialesRegistrar(PTR_DESESPDES);

//XV. MEDIDAS 
var PTR_MEDIDAREA     = $("#cmedarea_total").val();
var PTR_MEDIDANC      = $("#cmedancho").val();
var PTR_MEDIDLAR      = $("#cmedlargo").val();
var PTR_MEDIAMC       = $("#cmedancho_muro").val();
//XVI. INFORMACIÓN ADICIONAL
//var PTR_INFADICIONAL  = $("#cinformacion_adicional").val();
var cinformacion_adicionalTxt=document.getElementById("cinformacion_adicional");
var PTR_INFADICIONAL=cinformacion_adicionalTxt.options[cinformacion_adicionalTxt.selectedIndex].text;

var PTR_INFOBS        = $("#cinfadicobservaciones").val();
var PTR_INFOBS = caracteresEspecialesRegistrar(PTR_INFOBS);

//XVIII. PATOLOGIAS  
//var PTR_PATODAN       = $("#cpatdanos").val();
var ptr_patodan1 = document.getElementById("cpatdanos1").checked;
var ptr_patodan2 = document.getElementById("cpatdanos2").checked;
var ptr_patodan3 = document.getElementById("cpatdanos3").checked;
var ptr_patodan4 = document.getElementById("cpatdanos4").checked;
var ptr_patodan5 = document.getElementById("cpatdanos5").checked;
var ptr_patodan6 = document.getElementById("cpatdanos6").checked;

var ptr_patodan_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patodan1+', "resvalor": "Asentamientos"},{"resid": 278, "estado": '+ptr_patodan2+', "resvalor": "Desplomes"},{"resid": 279, "estado": '+ptr_patodan3+', "resvalor": "Desprendimientos"},{"resid": 279, "estado": '+ptr_patodan4+', "resvalor": "Humedad"},{"resid": 280, "estado": '+ptr_patodan5+', "resvalor": "Otros"},{"resid": 281, "estado": '+ptr_patodan6+', "resvalor": "Podredumbre"}]';

var PTR_PATO_OTROS    = $("#cpatotros").val();
var PTR_PATOOBS       = $("#cpatobservaciones").val();
var PTR_PATOOBS = caracteresEspecialesRegistrar(PTR_PATOOBS);

//var PTR_PATOCAU       = $("#cpatcausas").val();
var ptr_patocau1 = document.getElementById("cpatcausas1").checked;
var ptr_patocau2 = document.getElementById("cpatcausas2").checked;
var ptr_patocau3 = document.getElementById("cpatcausas3").checked;
var ptr_patocau4 = document.getElementById("cpatcausas4").checked;
var ptr_patocau5 = document.getElementById("cpatcausas5").checked;
var ptr_patocau6 = document.getElementById("cpatcausas6").checked;
var ptr_patocau7 = document.getElementById("cpatcausas7").checked;



var ptr_patocau_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patocau1+', "resvalor": "Abandono"},{"resid": 278, "estado": '+ptr_patocau2+', "resvalor": "Botánicos"},{"resid": 279, "estado": '+ptr_patocau3+', "resvalor": "Calidad de los materiales"},{"resid": 280, "estado": '+ptr_patocau4+', "resvalor": "Catastrofe Natural"},{"resid": 281, "estado": '+ptr_patocau5+', "resvalor": "Deficiencias constructivas"},{"resid": 282, "estado": '+ptr_patocau6+', "resvalor": "Falta de mantenimiento"},{"resid": 283, "estado": '+ptr_patocau7+', "resvalor": "Otros"}]';

var PTR_PATOCAU_OTROS = $("#cpatotros ").val();
//XIX. VALORACIÓN  
//var PTR_VALORHIS      = $("#cvalhistorico").val();
var ptr_valorhis1 = document.getElementById("cvalhistorico1").checked;
var ptr_valorhis2 = document.getElementById("cvalhistorico2").checked;
var ptr_valorhis3 = document.getElementById("cvalhistorico3").checked;
var ptr_valorhis4 = document.getElementById("cvalhistorico4").checked;
var ptr_valorhis5 = document.getElementById("cvalhistorico5").checked;

var ptr_valorhis_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorhis1+', "resvalor": "Espacio de valor testimonial y documental que ilustra el desarrollo político, social, religioso, cultural, económico y de forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 278, "estado": '+ptr_valorhis2+', "resvalor": "Espacio que desde su creación se constituye en un hito en la memoria histórica"},{"resid": 279, "estado": '+ptr_valorhis3+', "resvalor": "Espacio que en el trascurso histórico se constituyo en hito, al ser escenario relacionado con personas o eventos importantes, dentro del proceso histórico, social, cultural, económico de las comunidades"},{"resid": 280, "estado": '+ptr_valorhis4+', "resvalor": "Espacios que forman parte de un conjunto histórico"},{"resid": 281, "estado": '+ptr_valorhis5+', "resvalor": "Espacio que posee elementos arqueológicos que documentan su evolución y desarrollo"}]';

//var PTR_VALORART      = $("#cvalartistico").val();
var ptr_valorart1 = document.getElementById("cvalartistico1").checked;
var ptr_valorart2 = document.getElementById("cvalartistico2").checked;
var ptr_valorart3 = document.getElementById("cvalartistico3").checked;

var ptr_valorart_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorart1+', "resvalor": "Ejemplo sobresaliente por su singularidad"},{"resid": 278, "estado": '+ptr_valorart2+', "resvalor": "Conservar elementos arquitectónicos y tradicionales de interés"},{"resid": 279, "estado": '+ptr_valorart3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';


//var PTR_VALORARQ      = $("#cvalarquitectonico").val();
var ptr_valorarq1 = document.getElementById("cvalarquitectonico1").checked;
var ptr_valorarq2 = document.getElementById("cvalarquitectonico2").checked;
var ptr_valorarq3 = document.getElementById("cvalarquitectonico3").checked;

var ptr_valorarq_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorarq1+', "resvalor": "Poseedor de caracteristicas tipológicas representativas de estilos arquitectónicos"},{"resid": 278, "estado": '+ptr_valorarq2+', "resvalor": "Con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalle"},{"resid": 279, "estado": '+ptr_valorarq3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';

//var PTR_VALORTEC      = $("#cvaltecnologico").val();
var ptr_valortec1 = document.getElementById("cvaltecnologico1").checked;
var ptr_valortec2 = document.getElementById("cvaltecnologico2").checked;
var ptr_valortec3 = document.getElementById("cvaltecnologico3").checked;

var ptr_valortec_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valortec1+', "resvalor": "Espacio donde la aplicación de técnicas contructivas son singulares o de especial interés"},{"resid": 278, "estado": '+ptr_valortec2+', "resvalor": "Espacio que se constituye en un exponente de las técnicas constructivas y uso de los materiales característicos de una época o región determinada"},{"resid": 279, "estado": '+ptr_valortec2+', "resvalor": "Espacios con sistemas constructivos y elementos arquitectónicos realizados por su mano de obra especializada"}]';



//var PTR_VALORINTG     = $("#cvalintegridad").val();
var ptr_valorintg1 = document.getElementById("cvalintegridad1").checked;
var ptr_valorintg2 = document.getElementById("cvalintegridad2").checked;

var ptr_valorintg_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorintg1+', "resvalor": "Espacio que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 278, "estado": '+ptr_valorintg2+', "resvalor": "Espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"}]';


//var PTR_VALORURB      = $("#cvalurbano").val();
var ptr_valorurb1 = document.getElementById("cvalurbano1").checked;
var ptr_valorurb2 = document.getElementById("cvalurbano2").checked;

var ptr_valorurb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorurb1+', "resvalor": "Espacio que contribuye a definir un entorno de valor por su configuración y calidad en la estructura urbanística, el paisaje y/o el espacio público"},{"resid": 278, "estado": '+ptr_valorurb2+', "resvalor": "Considerado hito de referencia por su emplazamiento"}]';

//var PTR_VALORINT      = $("#cvalintangible").val();
var ptr_valorint1 = document.getElementById("cvalintangible1").checked;

var ptr_valorint_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorint1+', "resvalor": "Espacio relacionado con la organización social o forma de vida: usos, representacines, expresiones conocimientos y técnicas que las comunidades y grupos sociales reconozcan como parte de su patrimonio"}]';

//var PTR_VALORSIMB     = $("#cvalsimbolico").val();
var ptr_valorsimb1 = document.getElementById("cvalsimbolico1").checked;

var ptr_valorsimb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorsimb1+', "resvalor": "Espacio o espacio abierto único que expresa, representa, o significa identidad y pertenencia para el colectivo"}]';

//XX. CRITERIOS DE VALORACIÓN
var PTR_CRITERHIS     = $("#ccrivalhistorico").val();
var PTR_CRITERHIS = caracteresEspecialesRegistrar(PTR_CRITERHIS);
var PTR_CRITERART     = $("#ccrivalartistico").val();
var PTR_CRITERART = caracteresEspecialesRegistrar(PTR_CRITERART);
var PTR_CRITERARQ     = $("#ccrivalarquitectonico").val();
var PTR_CRITERARQ = caracteresEspecialesRegistrar(PTR_CRITERARQ);
var PTR_CRITERTEC     = $("#ccrivaltecnologicos").val();
var PTR_CRITERTEC = caracteresEspecialesRegistrar(PTR_CRITERTEC);
var PTR_CRITERINT     = $("#ccrivalintegridad").val();
var PTR_CRITERINT = caracteresEspecialesRegistrar(PTR_CRITERINT);
var PTR_CRITERURB     = $("#ccrivalurbano").val();
var PTR_CRITERURB = caracteresEspecialesRegistrar(PTR_CRITERURB);
var PTR_CRITERINTA    = $("#ccrivalintangible").val();
var PTR_CRITERINTA = caracteresEspecialesRegistrar(PTR_CRITERINTA);
var PTR_CRITERSIMB    = $("#ccrivalsimbolico").val();
var PTR_CRITERSIMB = caracteresEspecialesRegistrar(PTR_CRITERSIMB);
//PERSONAL ENCARGADO  
var PTR_PERINV        = $("#cperencinventariador").val();
var PTR_PERREV        = $("#cperrenrevisado").val();
var PTR_PERDIG        = $("#cperencdigitalizado").val();
///-------------TEXTAREA-----------------


var g_tipo = 'PTR-ESP';
var data = '{"g_tipo":"'+g_tipo+'","PTR_CREA":"'+PTR_CREADOR+'","PTR_ACTUALIZA":"'+PTR_ACTUALIZA+'","PTR_COD_CAT":"'+PTR_COD_CAT+'","PTR_GEOREF":"'+PTR_GEOREF+'","PTR_TRAD":"'+PTR_TRAD+'","PTR_ACT":"'+PTR_ACT+'","PTR_DEP":"'+PTR_DEP+'","PTR_CIPO":"'+PTR_CIPO+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_BARR":"'+PTR_BARR+'","PTR_DIR":"'+PTR_DIR+'","PTR_ESQ1":"'+PTR_ESQ1+'","PTR_ESQ2":"'+PTR_ESQ2+'","PTR_ALT":"'+PTR_ALT+'","PTR_NOMPRO":"'+PTR_NOMPRO+'","PTR_REPRO":"'+PTR_REPRO+'","PTR_USO":'+PTR_USO+',"PTR_SERV":'+PTR_SERV+',"PTR_FIGPRO":"'+PTR_FIGPRO+'","PTR_OTHER":'+ptr_other_check+',"PTR_REFHIST":"'+PTR_REFHIST+'","PTR_FECCONS":"'+PTR_FECCONS+'","PTR_AUTOR":"'+PTR_AUTOR+'","PTR_PREEX":"'+PTR_PREEX+'","PTR_PROP":"'+PTR_PROP+'","PTR_USOORG":"'+PTR_USOORG+'","PTR_HECHIS":"'+PTR_HECHIS+'","PTR_FIEREL":"'+PTR_FIEREL+'","PTR_DATHISC":"'+PTR_DATHISC+'","PTR_FUENTE":"'+PTR_FUENTE+'","PTR_FECCONST":"'+PTR_FECCONST+'","PTR_AUTCONST":"'+PTR_AUTCONST+'","PTR_PREDIN":"'+PTR_PREDIN+'","PTR_PROPIE":"'+PTR_PROPIE+'","PTR_USOPREEX":"'+PTR_USOPREEX+'","PTR_HECHISD":"'+PTR_HECHISD+'","PTR_FIERELD":"'+PTR_FIERELD+'","PTR_DATHISTC":"'+PTR_DATHISTC+'","PTR_FUENTBIO":"'+PTR_FUENTBIO+'","PTR_INSCR":"'+PTR_INSCR+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_ACONS":"'+PTR_ACONS+'","PTR_AULTR":"'+PTR_AULTR+'","PTR_ESTILO":"'+PTR_ESTILO+'","PTR_UBICESP":"'+PTR_UBICESP+'","PTR_LINCONS":"'+PTR_LINCONS+'","PTR_TIPOLO":"'+PTR_TIPOLO+'","PTR_MURMAT":'+ptr_murmat_check+',"PTR_MURMAT_OTROS":"'+PTR_MURMAT_OTROS+'","PTR_MUREST":"'+PTR_MUREST+'","PTR_MURINTE":"'+PTR_MURINTE+'","PTR_REJAS":'+PTR_REJAS+',"PTR_REJAEST":"'+PTR_REJAEST+'","PTR_ACAMUR":'+PTR_ACAMUR+',"PTR_ACAEST":"'+PTR_ACAEST+'","PTR_PINTURA":'+PTR_PINTURA+',"PTR_PINTEST":"'+PTR_PINTEST+'","PTR_PISOS":'+PTR_PISOS+',"PTR_PISOEST":"'+PTR_PISOEST+'","PTR_PUERTA":'+PTR_PUERTA+',"PTR_PUERTAEST":"'+PTR_PUERTAEST+'","PTR_MOBURB":'+PTR_MOBURB+',"PTR_MOBEST":"'+PTR_MOBEST+'","PTR_ESCAL":'+PTR_ESCAL+',"PTR_ESCEST":"'+PTR_ESCEST+'","PTR_VEGET":'+PTR_VEGET+',"PTR_VEGEST":"'+PTR_VEGEST+'","PTR_DETALLES":'+PTR_DETALLES+',"PTR_DETALLEST":"'+PTR_DETALLEST+'","PTR_ESQDES":"'+PTR_ESQDES+'","PTR_ANGOR":"'+PTR_ANGOR+'","PTR_ANGGRA":"'+PTR_ANGGRA+'","PTR_VISTAS":'+ptr_vistas_check+',"PTR_ANGOTR":"'+PTR_ANGOTR+'","PTR_ELEMVISID":"'+PTR_ELEMVISID+'","PTR_ELEMVISAL":"'+PTR_ELEMVISAL+'","PTR_ELEMVISAB":"'+PTR_ELEMVISAB+'","PTR_DESENTEI":"'+PTR_DESENTEI+'","PTR_DESESPDES":"'+PTR_DESESPDES+'","PTR_MEDIDAREA":"'+PTR_MEDIDAREA+'","PTR_MEDIDANC":"'+PTR_MEDIDANC+'","PTR_MEDIDLAR":"'+PTR_MEDIDLAR+'","PTR_MEDIAMC":"'+PTR_MEDIAMC+'","PTR_INFADICIONAL":"'+PTR_INFADICIONAL+'","PTR_INFOBS":"'+PTR_INFOBS+'","PTR_PATODAN":'+ptr_patodan_check+',"PTR_PATO_OTROS":"'+PTR_PATO_OTROS+'","PTR_PATOOBS":"'+PTR_PATOOBS+'","PTR_PATOCAU":'+ptr_patocau_check+',"PTR_PATOCAU_OTROS":"'+PTR_PATOCAU_OTROS+'","PTR_VALORHIS":'+ptr_valorhis_check+',"PTR_VALORART":'+ptr_valorart_check+',"PTR_VALORARQ":'+ptr_valorarq_check+',"PTR_VALORTEC":'+ptr_valortec_check+',"PTR_VALORINTG":'+ptr_valorintg_check+',"PTR_VALORURB":'+ptr_valorurb_check+',"PTR_VALORINT":'+ptr_valorint_check+',"PTR_VALORSIMB":'+ptr_valorsimb_check+',"PTR_CRITERHIS":"'+PTR_CRITERHIS+'","PTR_CRITERART":"'+PTR_CRITERART+'","PTR_CRITERARQ":"'+PTR_CRITERARQ+'","PTR_CRITERTEC":"'+PTR_CRITERTEC+'","PTR_CRITERINT":"'+PTR_CRITERINT+'","PTR_CRITERURB":"'+PTR_CRITERURB+'","PTR_CRITERINTA":"'+PTR_CRITERINTA+'","PTR_CRITERSIMB":"'+PTR_CRITERSIMB+'","PTR_PERINV":"'+PTR_PERINV+'","PTR_PERREV":"'+PTR_PERREV+'","PTR_PERDIG":"'+PTR_PERDIG+'","PTR_DOT_S":'+PTR_DOT_S+'}';

  ////console.log("SOLO DATA",data);

  var dataesp = JSON.stringify(data);
  //////////console.log("dataPTR_ESP",dataesp);
  var cas_nro_caso = correlativog;
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  var cas_nombre_caso = 'PTR-ESP'+correlativog+'/2018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo'; 
  var formData = {"identificador":"SERVICIO_PATRIMONIO-941","parametros":'{"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+dataesp+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  ////console.log(222222222222,formData);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //console.log("dataaaaEspaciosAbiertos",data);
        buscarPatrimonioPorTipo(1); 
        swal( "Exíto..!", "Se registro correctamente los bienes patrimoniales...!", "success" ); 
        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
     /*   setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', cas_nro_caso);
          location.href = url;
        }, 1000);*/         
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500);
});

function darBaja(id, g_tipo)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-934","parametros": '{"xcas_id":'+id+', "xcas_usr_id":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
           buscarPatrimonioPorTipo(1); 
           swal("Patrimonio!", "Fue eliminado correctamente!", "success");

         },
         error: function( result ) 
         {
          swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};



function editarEspacioAbierto(cas_id){

    var formData = {"identificador": "SERVICIO_PATRIMONIO-1016", "parametros":'{"id_caso":'+cas_id+'}'};

    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          console.log(dataIN,999999999999999999999);
          var cas_id = JSON.parse(dataIN[0].data).cas_id;
          var numero_ficha = JSON.parse(dataIN[0].data).cas_nro_caso;
          var g_tipo= JSON.parse(dataIN[0].data).cas_datos.g_tipo;
          var codigo_ficha = JSON.parse(dataIN[0].data).cas_nombre_caso;

           var creador = JSON.parse(dataIN[0].data).cas_datos.PTR_CREA;
           var autorizador = JSON.parse(dataIN[0].data).cas_datos.PTR_ACTUALIZA;
           var codigocatastral = JSON.parse(dataIN[0].data).cas_datos.PTR_COD_CAT;
           var georeferencia = JSON.parse(dataIN[0].data).cas_datos.PTR_GEOREF;
           var tradicional = JSON.parse(dataIN[0].data).cas_datos.PTR_TRAD;
           var actual = JSON.parse(dataIN[0].data).cas_datos.PTR_ACT;
           var departamento = JSON.parse(dataIN[0].data).cas_datos.PTR_DEP;
           var provincia = JSON.parse(dataIN[0].data).cas_datos.PTR_CIPO;
           var municipio = JSON.parse(dataIN[0].data).cas_datos.PTR_MUNI;
           var macrodistrito = JSON.parse(dataIN[0].data).cas_datos.PTR_MACRO;
           var barrio = JSON.parse(dataIN[0].data).cas_datos.PTR_BARR;
           var direccioncalle = JSON.parse(dataIN[0].data).cas_datos.PTR_DIR;
           var esquina1 = JSON.parse(dataIN[0].data).cas_datos.PTR_ESQ1;
           var esquina2 = JSON.parse(dataIN[0].data).cas_datos.PTR_ESQ2;
           var altitud = JSON.parse(dataIN[0].data).cas_datos.PTR_ALT;
           var nombrepropietario = JSON.parse(dataIN[0].data).cas_datos.PTR_NOMPRO;
           var tiporegpropietario = JSON.parse(dataIN[0].data).cas_datos.PTR_REPRO;
           /**/
           //var regprousosuelo=JSON.parse(dataIN[0].data).cas_datos.PTR_USO;
           //var regproservicio=JSON.parse(dataIN[0].data).cas_datos.PTR_SERV;

           var marlegfiguraproyeccion=JSON.parse(dataIN[0].data).cas_datos.PTR_FIGPRO;
           var regprootrosservicios=JSON.parse(dataIN[0].data).cas_datos.PTR_OTHER;        
           var regprootrosservicios_check = "";
           for (var j = 1; j < regprootrosservicios.length; j++){
            var trad = regprootrosservicios[j].estado;
            regprootrosservicios_check = regprootrosservicios_check + trad + ',';
          }
          regprootrosservicios = regprootrosservicios_check;
          var crefhisreferenciahistorica=JSON.parse(dataIN[0].data).cas_datos.PTR_REFHIST;  
          
          //////console.log(crefhisreferenciahistorica,66666666666999999999);
          var cdatorafechaconstruccion=JSON.parse(dataIN[0].data).cas_datos.PTR_FECCONS;       
          var cdatorautorconstructor=JSON.parse(dataIN[0].data).cas_datos.PTR_AUTOR;          
          var cdatorapreexistenciaedificacion=JSON.parse(dataIN[0].data).cas_datos.PTR_PREEX;         
          var cdatorapropietario=JSON.parse(dataIN[0].data).cas_datos.PTR_PROP;          
          var cdatorausosoriginales=JSON.parse(dataIN[0].data).cas_datos.PTR_USOORG;        
          var cdatorahechoshistoricosdist=JSON.parse(dataIN[0].data).cas_datos.PTR_HECHIS;       
          var cdatorafiestasreligiosas=JSON.parse(dataIN[0].data).cas_datos.PTR_FIEREL;       
          var cdatoradatoshistoricos=JSON.parse(dataIN[0].data).cas_datos.PTR_DATHISC;
          var cdatorafuentebibliografia=JSON.parse(dataIN[0].data).cas_datos.PTR_FUENTE;        
          var cdatdocfechadeconstruccion=JSON.parse(dataIN[0].data).cas_datos.PTR_FECCONST;
          var cdatdocautoroconstructor=JSON.parse(dataIN[0].data).cas_datos.PTR_AUTCONST;      
          var cdatdocpreexistenciainvestigacion=JSON.parse(dataIN[0].data).cas_datos.PTR_PREDIN;       
          var cdatdocpropietario=JSON.parse(dataIN[0].data).cas_datos.PTR_PROPIE;        
          var cdatdocusospreexistentes=JSON.parse(dataIN[0].data).cas_datos.PTR_USOPREEX;
          var cdatdochechoshistoricospers=JSON.parse(dataIN[0].data).cas_datos.PTR_HECHISD;     
          var cdatdocfiestasreli=JSON.parse(dataIN[0].data).cas_datos.PTR_FIERELD;       
          var cdatdocdatoshistoricosconj=JSON.parse(dataIN[0].data).cas_datos.PTR_DATHISTC;
          var cdatdocfuentebiblio=JSON.parse(dataIN[0].data).cas_datos.PTR_FUENTBIO;      
          var cinsinscripciones=JSON.parse(dataIN[0].data).cas_datos.PTR_INSCR;     
          var cepoepoca=JSON.parse(dataIN[0].data).cas_datos.PTR_EPOCA;         
          var cepoanoconstruccion=JSON.parse(dataIN[0].data).cas_datos.PTR_ACONS;         
          var cepoanoremodelacion=JSON.parse(dataIN[0].data).cas_datos.PTR_AULTR;         
          var cestestilo=JSON.parse(dataIN[0].data).cas_datos.PTR_ESTILO;        
          var cdesareubicacionespacio=JSON.parse(dataIN[0].data).cas_datos.PTR_UBICESP;       
          var cdesarelineaconstruccion=JSON.parse(dataIN[0].data).cas_datos.PTR_LINCONS;     
          var cdesaretipologia=JSON.parse(dataIN[0].data).cas_datos.PTR_TIPOLO;      
          var cdesarematerial=JSON.parse(dataIN[0].data).cas_datos.PTR_MURMAT; 
          
          var cdesarematerial_check = ""; 
          for (var j = 1; j < cdesarematerial.length; j++){
            var trad = cdesarematerial[j].estado;
            cdesarematerial_check = cdesarematerial_check + trad + ',';
          }
          cdesarematerial=cdesarematerial_check;
          
          //////console.log(cdesareotros,'PTR_MURMAT_OTROS');
          var cdesareestado=JSON.parse(dataIN[0].data).cas_datos.PTR_MUREST;      
          var cdesareintegridad=JSON.parse(dataIN[0].data).cas_datos.PTR_MURINTE;

          //var rejasgrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_REJAS;

          var cdesareestadorejas=JSON.parse(dataIN[0].data).cas_datos.PTR_REJAEST;
          //var acabadomurosgrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_ACAMUR;
          var cdesareestadoacabadomur=JSON.parse(dataIN[0].data).cas_datos.PTR_ACAEST;

          //var pinturagrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_PINTURA;

          var cdesareestadopint=JSON.parse(dataIN[0].data).cas_datos.PTR_PINTEST;

          //var pisosgrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_PISOS;

          var cdesareestadopisos=JSON.parse(dataIN[0].data).cas_datos.PTR_PISOEST;

          //var puertadeaccesogrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_PUERTA;

          var cdesareestadopuertasacc=JSON.parse(dataIN[0].data).cas_datos.PTR_PUERTAEST;

          //var mobiliariogrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_MOBURB;

          var cdesareestadomoviliariourb=JSON.parse(dataIN[0].data).cas_datos.PTR_MOBEST;

          //var escaleragrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_ESCAL;

          var cdesareestadoescaleras=JSON.parse(dataIN[0].data).cas_datos.PTR_ESCEST;

          //var caracterivegetagrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_VEGET;

          var cdesareestadocaracteristicasvege=JSON.parse(dataIN[0].data).cas_datos.PTR_VEGEST;

         // var detalleartisticogrilla=JSON.parse(dataIN[0].data).cas_datos.PTR_DETALLES;

          var cdesareestadodetalleartistico=JSON.parse(dataIN[0].data).cas_datos.PTR_DETALLEST;
          var cesqesquemas =JSON.parse(dataIN[0].data).cas_datos.PTR_ESQDES;
          var cangvisorientacion =JSON.parse(dataIN[0].data).cas_datos.PTR_ANGOR;
          var cangvisgrados =JSON.parse(dataIN[0].data).cas_datos.PTR_ANGGRA;
          var cangvisvistas=JSON.parse(dataIN[0].data).cas_datos.PTR_VISTAS;
          var cangvisvistas_check = ""; 
          for (var j = 1; j < cangvisvistas.length; j++){
            var trad = cangvisvistas[j].estado;
            cangvisvistas_check = cangvisvistas_check + trad + ',';
          }
          cangvisvistas = cangvisvistas_check;
          var cangvisotros =JSON.parse(dataIN[0].data).cas_datos.PTR_ANGOTR;
          var celevisizquierda_derecha=JSON.parse(dataIN[0].data).cas_datos.PTR_ELEMVISID;
          var celivisalto =JSON.parse(dataIN[0].data).cas_datos.PTR_ELEMVISAL;
          var celivisbajo =JSON.parse(dataIN[0].data).cas_datos.PTR_ELEMVISAB;
          var cdesententorno_inmediato=JSON.parse(dataIN[0].data).cas_datos.PTR_DESENTEI;      
          var cdesespdescrpcion_espacio=JSON.parse(dataIN[0].data).cas_datos.PTR_DESESPDES;
          var cmedarea_total=JSON.parse(dataIN[0].data).cas_datos.PTR_MEDIDAREA;
          var cmedancho =JSON.parse(dataIN[0].data).cas_datos.PTR_MEDIDANC;
          var cmedlargo =JSON.parse(dataIN[0].data).cas_datos.PTR_MEDIDLAR;
          var cmedancho_muro=JSON.parse(dataIN[0].data).cas_datos.PTR_MEDIAMC;
          var cinformacion_adicional=JSON.parse(dataIN[0].data).cas_datos.PTR_INFADICIONAL;
          var cinfadicobservaciones=JSON.parse(dataIN[0].data).cas_datos.PTR_INFOBS;
          var cpatdanos =JSON.parse(dataIN[0].data).cas_datos.PTR_PATODAN;
          var cpatdanos_check = ""; 
          for (var j = 1; j < cpatdanos.length; j++){
            var trad = cpatdanos[j].estado;
            cpatdanos_check = cpatdanos_check + trad + ',';
          }
          cpatdanos = cpatdanos_check;
          var cpatotros=JSON.parse(dataIN[0].data).cas_datos.PTR_PATO_OTROS;
          var cpatobservaciones=JSON.parse(dataIN[0].data).cas_datos.PTR_PATOOBS;
          var cpatcausas =JSON.parse(dataIN[0].data).cas_datos.PTR_PATOCAU;
          var cpatcausas_check = ""; 
          for (var j = 1; j < cpatcausas.length; j++){
            var trad = cpatcausas[j].estado;
            cpatcausas_check = cpatcausas_check + trad + ',';
          }
          cpatcausas = cpatcausas_check;

          var cpatotros=JSON.parse(dataIN[0].data).cas_datos.PTR_PATOCAU_OTROS;
          var cvalhistorico=JSON.parse(dataIN[0].data).cas_datos.PTR_VALORHIS;
          var cvalhistorico_check = ""; 
          for (var j = 1; j < cvalhistorico.length; j++){
            var trad = cvalhistorico[j].estado;
            cvalhistorico_check = cvalhistorico_check + trad + ',';
          }
          cvalhistorico  = cvalhistorico_check;

          var cvalartistico =JSON.parse(dataIN[0].data).cas_datos.PTR_VALORART;
          var cvalartistico_check = ""; 
          for (var j = 1; j < cvalartistico.length; j++){
            var trad = cvalartistico[j].estado;
            cvalartistico_check = cvalartistico_check + trad + ',';
          }
          cvalartistico  = cvalartistico_check;

          var cvalarquitectonico =JSON.parse(dataIN[0].data).cas_datos.PTR_VALORARQ;
          var cvalarquitectonico_check = "";
          for (var j = 1; j < cvalarquitectonico.length; j++){
            var trad = cvalarquitectonico[j].estado;
            cvalarquitectonico_check = cvalarquitectonico_check + trad + ',';
          }
          cvalarquitectonico =  cvalarquitectonico_check;
          var cvaltecnologico =JSON.parse(dataIN[0].data).cas_datos.PTR_VALORTEC;
          var cvaltecnologico_check = "";
          for (var j = 1; j < cvaltecnologico.length; j++){
            var trad = cvaltecnologico[j].estado;
            cvaltecnologico_check = cvaltecnologico_check + trad + ',';
          }
          cvaltecnologico =  cvaltecnologico_check;

          var cvalintegridad =JSON.parse(dataIN[0].data).cas_datos.PTR_VALORINTG; 
          var cvalintegridad_check = "";
          for (var j = 1; j < cvalintegridad.length; j++){
            var trad = cvalintegridad[j].estado;
            cvalintegridad_check = cvalintegridad_check + trad + ',';
          }
          cvalintegridad = cvalintegridad_check;

          var cvalurbano=JSON.parse(dataIN[0].data).cas_datos.PTR_VALORURB;
          var cvalurbano_check = "";
          for (var j = 1; j < cvalurbano.length; j++){
            var trad = cvalurbano[j].estado;
            cvalurbano_check = cvalurbano_check + trad + ',';
          }
          cvalurbano = cvalurbano_check;

          var cvalintangible =JSON.parse(dataIN[0].data).cas_datos.PTR_VALORINT;
          var cvalintangible_check = "";
          for (var j = 1; j < cvalintangible.length; j++){
            var trad = cvalintangible[j].estado;
            cvalintangible_check = cvalintangible_check + trad + ',';
          }
          cvalintangible = cvalintangible_check;

          var cvalsimbolico =JSON.parse(dataIN[0].data).cas_datos.PTR_VALORSIMB; 
          var cvalsimbolico_check = "";
          for (var j = 1; j < cvalsimbolico.length; j++){
            var trad = cvalsimbolico[j].estado;
            cvalsimbolico_check = cvalsimbolico_check + trad + ',';
          }
          
          cvalsimbolico = cvalsimbolico_check;


          var ccrivalhistorico=JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERHIS;    
          var ccrivalartistico =JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERART;    
          var ccrivalarquitectonico =JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERARQ;
          var ccrivaltecnologicos=JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERTEC;
          var ccrivalintegridad =JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERINT;
          var ccrivalurbano =JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERURB;
          var ccrivalintangible =JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERINTA;
          var ccrivalsimbolico =JSON.parse(dataIN[0].data).cas_datos.PTR_CRITERSIMB;
          var cperencinventariador=JSON.parse(dataIN[0].data).cas_datos.PTR_PERINV;
          var cperrenrevisado=JSON.parse(dataIN[0].data).cas_datos.PTR_PERREV;
          var cperencdigitalizado=JSON.parse(dataIN[0].data).cas_datos.PTR_PERDIG;


          $( "#acas_id" ).val(cas_id);
          $( "#aespnumero_ficha" ).val(numero_ficha);
          $( "#aespcodigo_ficha" ).val(codigo_ficha);
          $( "#ag_tipo" ).val(g_tipo);
          $( "#acreador" ).val(creador);
          $( "#aautorizador" ).val(autorizador);
          $( "#acodigocatastral" ).val(codigocatastral);
          $( "#ageoreferencia" ).val(georeferencia);
          $( "#atradicional" ).val(tradicional);
          $( "#aactual" ).val(actual);
          $( "#adepartamento" ).val(departamento);
          $( "#aprovincia" ).val(provincia);
          $( "#amunicipio" ).val(municipio);
          document.getElementById("amacrodistritoespabie").length=0;
          document.getElementById("cmacrodistritoespabie").length=1;
          listaMacrodistritoespacioabierto(macrodistrito);
          $( "#abarrio" ).val(barrio);
          $( "#adireccioncalle" ).val(direccioncalle);
          $( "#aesquina1" ).val(esquina1);
          $( "#aesquina2" ).val(esquina2);
          $( "#aaltitud" ).val(altitud);
          $( "#anombrepropietario" ).val(nombrepropietario);
          $( "#atiporegpropietario" ).val(tiporegpropietario);
          /*-----------------*/

          //var PTR_USO           = 'Uso de suelo';
          //var PTR_SERV          = 'Servicio'
          //$("#aregprootrosservicios").val(regprootrosservicios);
          var regprootrosservicios = regprootrosservicios.split(',');
          document.getElementById("aregprootrosservicios1").checked = getBool(regprootrosservicios[0]);
          document.getElementById("aregprootrosservicios2").checked = getBool(regprootrosservicios[1]);
          document.getElementById("aregprootrosservicios3").checked = getBool(regprootrosservicios[2]);

          $("#amarlegfiguraproyeccion").val(marlegfiguraproyeccion);
          $("#arefhisreferenciahistorica").val(crefhisreferenciahistorica);
          $("#adatorafechaconstruccion").val(cdatorafechaconstruccion);
          $("#adatorautorconstructor").val(cdatorautorconstructor);
          var cdatorapreexistenciaedificacion = caracteresEspecialesActualizar(cdatorapreexistenciaedificacion);
          $("#adatorapreexistenciaedificacion").val(cdatorapreexistenciaedificacion);
          $("#adatorapropietario").val(cdatorapropietario);
          var cdatorausosoriginales = caracteresEspecialesActualizar(cdatorausosoriginales);
          $("#adatorausosoriginales").val(cdatorausosoriginales);
          var cdatorahechoshistoricosdist = caracteresEspecialesActualizar(cdatorahechoshistoricosdist);
          $("#adatorahechoshistoricosdist").val(cdatorahechoshistoricosdist);
          var cdatorafiestasreligiosas = caracteresEspecialesActualizar(cdatorafiestasreligiosas);
          $("#adatorafiestasreligiosas").val(cdatorafiestasreligiosas);
          var cdatoradatoshistoricos = caracteresEspecialesActualizar(cdatoradatoshistoricos);
          $("#adatoradatoshistoricos").val(cdatoradatoshistoricos);
          var cdatorafuentebibliografia = caracteresEspecialesActualizar(cdatorafuentebibliografia);
          $("#adatorafuentebibliografia").val(cdatorafuentebibliografia);
          //V. DATOS DOCUMENTALES  
          $("#adatdocfechadeconstruccion").val(cdatdocfechadeconstruccion);
          $("#adatdocautoroconstructor").val(cdatdocautoroconstructor);
          var cdatdocpreexistenciainvestigacion = caracteresEspecialesActualizar(cdatdocpreexistenciainvestigacion);
          $("#adatdocpreexistenciainvestigacion").val(cdatdocpreexistenciainvestigacion);
          $("#adatdocpropietario").val(cdatdocpropietario);
          var cdatdocusospreexistentes = caracteresEspecialesActualizar(cdatdocusospreexistentes);
          $("#adatdocusospreexistentes").val(cdatdocusospreexistentes);
          var cdatdochechoshistoricospers = caracteresEspecialesActualizar(cdatdochechoshistoricospers);
          $("#adatdochechoshistoricospers").val(cdatdochechoshistoricospers);
          var cdatdocfiestasreli = caracteresEspecialesActualizar(cdatdocfiestasreli);
          $("#adatdocfiestasreli").val(cdatdocfiestasreli);
          var cdatdocdatoshistoricosconj = caracteresEspecialesActualizar(cdatdocdatoshistoricosconj);
          $("#adatdocdatoshistoricosconj").val(cdatdocdatoshistoricosconj);
          var cdatdocfuentebiblio = caracteresEspecialesActualizar(cdatdocfuentebiblio);
          $("#adatdocfuentebiblio").val(cdatdocfuentebiblio);
          //VI. INSCRIPCIONES  
          var cinsinscripciones = caracteresEspecialesActualizar(cinsinscripciones);
          $("#ainsinscripciones").val(cinsinscripciones);
          //VII. ÉPOCA   
          $("#aepoepoca").val(cepoepoca);
          $("#aepoanoconstruccion").val(cepoanoconstruccion);
          $("#aepoanoremodelacion").val(cepoanoremodelacion);
          //VII. ESTILO    
          $("#aestestilo").val(cestestilo);
          //IX. DESCRIPCIÓN DEL AREA
          $("#adesareubicacionespacio").val(cdesareubicacionespacio);
          $("#adesarelineaconstruccion").val(cdesarelineaconstruccion);
          $("#adesaretipologia").val(cdesaretipologia);
          //$("#adesarematerial").val(cdesarematerial);
          var cdesarematerial = cdesarematerial.split(',');
          document.getElementById("adesarematerial1").checked = getBool(cdesarematerial[0]);
          document.getElementById("adesarematerial2").checked = getBool(cdesarematerial[1]);
          document.getElementById("adesarematerial3").checked = getBool(cdesarematerial[2]);
          document.getElementById("adesarematerial4").checked = getBool(cdesarematerial[3]);
          document.getElementById("adesarematerial5").checked = getBool(cdesarematerial[4]);
          document.getElementById("adesarematerial6").checked = getBool(cdesarematerial[5]);
          document.getElementById("adesarematerial7").checked = getBool(cdesarematerial[6]);
          document.getElementById("adesarematerial8").checked = getBool(cdesarematerial[7]);
          document.getElementById("adesarematerial9").checked = getBool(cdesarematerial[8]);
          document.getElementById("adesarematerial10").checked = getBool(cdesarematerial[9]);

          var cdesareotros = JSON.parse(dataIN[0].data).cas_datos.PTR_MURMAT_OTROS; 
          $("#adesareotros").val(cdesareotros);
          $("#adesareestado").val(cdesareestado);
          $("#adesareintegridad").val(cdesareintegridad);
          //var PTR_REJAS = 'Rejas';
          $("#adesareestadorejas").val(cdesareestadorejas);
          //var PTR_ACAMUR = 'Acabados de Muros';
          $("#adesareestadoacabadomur").val(cdesareestadoacabadomur);
          //var PTR_PINTURA = 'Pintura';
          $("#adesareestadopint").val(cdesareestadopint);
          //var PTR_PISOS = 'Pisos';
          $("#adesareestadopisos").val(cdesareestadopisos);
          //var PTR_PUERTA = 'Puertas de Acceso';
          $("#adesareestadopuertasacc").val(cdesareestadopuertasacc);
          //var PTR_MOBURB  = "Mobiliario";
          $("#adesareestadomoviliariourb").val(cdesareestadomoviliariourb);
          //var PTR_ESCAL = 'Escalera';
          $("#adesareestadoescaleras").val(cdesareestadoescaleras);
          //var PTR_VEGET = 'Caracterísiticas de la Vegetación';
          $("#adesareestadocaracteristicasvege").val(cdesareestadocaracteristicasvege);
          //var PTR_DETALLES = 'Detalle artistico';
          $("#adesareestadodetalleartistico").val(cdesareestadodetalleartistico);
          //x. ESQUEMAS
          var cesqesquemas = caracteresEspecialesActualizar(cesqesquemas);
          $("#aesqesquemas").val(cesqesquemas);
          //XI. ANGULO VISUAL HORIZONTAL
          $("#aangvisorientacion").val(cangvisorientacion);
          $("#aangvisgrados").val(cangvisgrados);
          //$("#aangvisvistas").val(cangvisvistas);
          var cangvisvistas = cangvisvistas.split(',');
          document.getElementById("aangvisvistas1").checked = getBool(cangvisvistas[0]);
          document.getElementById("aangvisvistas2").checked = getBool(cangvisvistas[1]);

          $("#aangvisotros").val(cangvisotros);
          //XII. ELEMENTOS VISUALES
          var celevisizquierda_derecha = caracteresEspecialesActualizar(celevisizquierda_derecha);
          $("#aelevisizquierda_derecha").val(celevisizquierda_derecha);
          var celivisalto = caracteresEspecialesActualizar(celivisalto);
          $("#aelivisalto").val(celivisalto);
          var celivisbajo = caracteresEspecialesActualizar(celivisbajo);
          $("#aelivisbajo").val(celivisbajo);
          //XIII. DESCRIPCIÓN DEL ENTORNO
          var cdesententorno_inmediato = caracteresEspecialesActualizar(cdesententorno_inmediato);
          $("#adesententorno_inmediato").val(cdesententorno_inmediato);
          //XIV. DESCRIPCIÓN DEL ESPACIO
          //var PTR_DESESP
          var cdesespdescrpcion_espacio = caracteresEspecialesActualizar(cdesespdescrpcion_espacio);
          $("#adesespdescrpcion_espacio").val(cdesespdescrpcion_espacio);
          //XV. MEDIDAS 
          $("#amedarea_total").val(cmedarea_total);
          $("#amedancho").val(cmedancho);
          $("#amedlargo").val(cmedlargo);
          $("#amedancho_muro").val(cmedancho_muro);
          //XVI. INFORMACIÓN ADICIONAL
          $("#ainformacion_adicional").val(cinformacion_adicional);
          var cinfadicobservaciones = caracteresEspecialesActualizar(cinfadicobservaciones);
          $("#ainfadicobservaciones").val(cinfadicobservaciones);
          //XVIII. PATOLOGIAS  
          //$("#apatdanos").val(cpatdanos);
          var cpatdanos = cpatdanos.split(',');
          ////console.log(cpatdanos,'hghhghghghgh');
          document.getElementById("apatdanos1").checked = getBool(cpatdanos[0]);
          document.getElementById("apatdanos2").checked = getBool(cpatdanos[1]);
          document.getElementById("apatdanos3").checked = getBool(cpatdanos[2]);
          document.getElementById("apatdanos4").checked = getBool(cpatdanos[3]);
          document.getElementById("apatdanos5").checked = getBool(cpatdanos[4]);
          document.getElementById("apatdanos6").checked = getBool(cpatdanos[5]);

          $("#apatotros").val(cpatotros);
          var cpatobservaciones = caracteresEspecialesActualizar(cpatobservaciones);
          $("#apatobservaciones").val(cpatobservaciones);

          ////console.log(cpatcausas,'apatcausas1 12121212121212121');
          //$("#apatcausas").val(cpatcausas);
          var cpatcausas = cpatcausas.split(',');
          ////console.log('causaseditar',cpatcausas);
          document.getElementById("apatcausas1").checked = getBool(cpatcausas[0]);
          document.getElementById("apatcausas2").checked = getBool(cpatcausas[1]);
          document.getElementById("apatcausas3").checked = getBool(cpatcausas[2]);
          document.getElementById("apatcausas4").checked = getBool(cpatcausas[3]);
          document.getElementById("apatcausas5").checked = getBool(cpatcausas[4]);
          document.getElementById("apatcausas6").checked = getBool(cpatcausas[5]);
          document.getElementById("apatcausas7").checked = getBool(cpatcausas[6]);


          $("#apatotros ").val(cpatotros);
          //XIX. VALORACIÓN  
          //$("#avalhistorico").val(cvalhistorico);
          var cvalhistorico = cvalhistorico.split(',');
          document.getElementById("avalhistorico1").checked = getBool(cvalhistorico[0]);
          document.getElementById("avalhistorico2").checked = getBool(cvalhistorico[1]);
          document.getElementById("avalhistorico3").checked = getBool(cvalhistorico[2]);
          document.getElementById("avalhistorico4").checked = getBool(cvalhistorico[3]);
          document.getElementById("avalhistorico5").checked = getBool(cvalhistorico[4]);

          //$("#avalartistico").val(cvalartistico);
          var cvalartistico = cvalartistico.split(',');
          document.getElementById("avalartistico1").checked = getBool(cvalartistico[0]);
          document.getElementById("avalartistico2").checked = getBool(cvalartistico[1]);
          document.getElementById("avalartistico3").checked = getBool(cvalartistico[2]);

          //$("#avalarquitectonico").val(cvalarquitectonico);
          var cvalarquitectonico = cvalarquitectonico.split(',');
          document.getElementById("avalarquitectonico1").checked = getBool(cvalarquitectonico[0]);
          document.getElementById("avalarquitectonico2").checked = getBool(cvalarquitectonico[1]);
          document.getElementById("avalarquitectonico3").checked = getBool(cvalarquitectonico[2]);

          //$("#avaltecnologico").val(cvaltecnologico);\
          var cvaltecnologico = cvaltecnologico.split(',');
          document.getElementById("avaltecnologico1").checked = getBool(cvaltecnologico[0]);
          document.getElementById("avaltecnologico2").checked = getBool(cvaltecnologico[1]);
          document.getElementById("avaltecnologico3").checked = getBool(cvaltecnologico[2]);



          //$("#avalintegridad").val(cvalintegridad);
          var cvalintegridad = cvalintegridad.split(',');
          document.getElementById("avalintegridad1").checked = getBool(cvalintegridad[0]);
          document.getElementById("avalintegridad2").checked = getBool(cvalintegridad[1]);

          //$("#avalurbano").val(cvalurbano);
          var cvalurbano = cvalurbano.split(',');
          document.getElementById("avalurbano1").checked = getBool(cvalurbano[0]);
          document.getElementById("avalurbano2").checked = getBool(cvalurbano[1]);

          //$("#avalintangible").val(cvalintangible);
          var cvalintangible = cvalintangible.split(',');
          document.getElementById("avalintangible1").checked = getBool(cvalintangible[0]);

          //$("#avalsimbolico").val(cvalsimbolico);
          var cvalsimbolico = cvalsimbolico.split(',');
          document.getElementById("avalsimbolico1").checked = getBool(cvalsimbolico[0]);

          //XX. CRITERIOS DE VALORACIÓN
          var ccrivalhistorico = caracteresEspecialesActualizar(ccrivalhistorico);
          $("#acrivalhistorico").val(ccrivalhistorico);
          var ccrivalartistico = caracteresEspecialesActualizar(ccrivalartistico);
          $("#acrivalartistico").val(ccrivalartistico);
          var ccrivalarquitectonico = caracteresEspecialesActualizar(ccrivalarquitectonico);
          $("#acrivalarquitectonico").val(ccrivalarquitectonico);
          var ccrivaltecnologicos = caracteresEspecialesActualizar(ccrivaltecnologicos);
          $("#acrivaltecnologicos").val(ccrivaltecnologicos);
          var ccrivalintegridad = caracteresEspecialesActualizar(ccrivalintegridad);
          $("#acrivalintegridad").val(ccrivalintegridad);
          var ccrivalurbano = caracteresEspecialesActualizar(ccrivalurbano);
          $("#acrivalurbano").val(ccrivalurbano);
          var ccrivalintangible = caracteresEspecialesActualizar(ccrivalintangible);
          $("#acrivalintangible").val(ccrivalintangible);
          var ccrivalsimbolico = caracteresEspecialesActualizar(ccrivalsimbolico);
          $("#acrivalsimbolico").val(ccrivalsimbolico);
          //PERSONAL ENCARGADO  
          $("#aperencinventariador").val(cperencinventariador);
          $("#aperrenrevisado").val(cperrenrevisado);
          $("#aperencdigitalizado").val(cperencdigitalizado);

           //-------- Inicio grilla Puntos Colineales-------------
          var grillaPuntosC = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_DOT_S !== 'undefined'){
          grillaPuntosC = JSON.parse(dataIN[0].data).cas_datos.PTR_DOT_S;
          console.log('PUNTOS COLINEALES',grillaPuntosC);
          var longitudPC  = grillaPuntosC.length;
          contPCact = longitudPC;
          editarPuntosC(longitudPC);
           for (var j = 1; j < longitudPC; j++)
            {
            $("#aNroDescPuntosColineales"+j+"").val(grillaPuntosC[j].f01_Nro);
            $("#aPuntoColinealPuntosColineales"+j+"").val(grillaPuntosC[j].f01_Pto_Colineal); 
            } 
           arrDibujoPC.forEach(function (item, index, array) 
           {
            arrarPCactual.push({var_numero:$("#NumeroPC"+item.id).val(), 
            var_puntocol:$("#puntoPC"+item.id).val()});
           });
          }
           
          //----------fin grilla puntos colineales---------------  
          $( "#atradicional" ).val(tradicional);
          $( "#aactual" ).val(actual);
          $( "#adepartamento" ).val(departamento);
          $( "#aprovincia" ).val(provincia);
          $( "#amunicipio" ).val(municipio);
          document.getElementById("amacrodistritoespabie").length=0;
          document.getElementById("cmacrodistritoespabie").length=1;
          listaMacrodistritoespacioabierto(macrodistrito);
          $( "#abarrio" ).val(barrio);
          $( "#adireccioncalle" ).val(direccioncalle);
          $( "#aesquina1" ).val(esquina1);
          $( "#aesquina2" ).val(esquina2);
          $( "#aaltitud" ).val(altitud);
          $( "#anombrepropietario" ).val(nombrepropietario);
          $( "#atiporegpropietario" ).val(tiporegpropietario);

          //----------Grilla Uso de Suelo------------------------ 
          var grillaUsoS = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_USO !== 'undefined'){
          grillaUsoS = JSON.parse(dataIN[0].data).cas_datos.PTR_USO;
          var longitudUS  = grillaUsoS.length;
          contUSact = longitudUS;
          editarUsoSuelo(longitudUS);
           for (var j = 1; j < longitudUS; j++)
            {
            $("#desccripcionUS"+j+"").val(grillaUsoS[j].f01_emision_G_SERV_DESC);
            var actual = grillaUsoS[j].f01_emision_G_ACT;
            var actual = cambiarValorBool(actual);   
            document.getElementById("actuallUS"+j+"").checked = actual;
            var tradicion = grillaUsoS[j].f01_emision_G_TRAD;
            var tradicion = cambiarValorBool(tradicion);   
            document.getElementById("tradicionUS"+j+"").checked = tradicion; 
            } 
           arrDibujoUSact.forEach(function (item, index, array) 
           {
            aarrCodigoUso.push({adescripU:$("#desccripcionUS"+item.id).val(), 
            atradicionalU:document.getElementById("actuallUS"+item.id).checked, 
            aactualU:document.getElementById("tradicionUS"+item.id).checked});
           });
          }
          //------Grilla Servicio------
          var grillaSER = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_SERV !== 'undefined'){
          grillaSER = JSON.parse(dataIN[0].data).cas_datos.PTR_SERV;
          console.log('servicio',grillaSER);
          var longitudS  = grillaSER.length;
          contSerAct = longitudS;
          editarServicio(longitudS);
           for (var j = 1; j < longitudS; j++)
            {
            $("#descripcionS"+j+"").val(grillaSER[j].f01_emision_G_SERV_DESC);
            $("#proveedorS"+j+"").val(grillaSER[j].f01_emision_G_SERV_PROV);
            } 
           arrDibujoS.forEach(function (item, index, array) 
           {
            aarrCodigoSer.push({adescServicio:$("#descripcionS"+item.id).val(), 
            aproveedorServicio:$("#proveedorS"+item.id).val()});
           });
          }
          //------Grilla Rejas------
          var grillaR = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_REJAS !== 'undefined'){
          grillaR = JSON.parse(dataIN[0].data).cas_datos.PTR_REJAS;
          var longitudR  = grillaR.length;
          contRactual = longitudR;
          editarGrillaRejas(longitudR);
           for (var j = 1; j < longitudR; j++)
            {
            $("#descripcionRE"+j+"").val(grillaR[j].f01_reja_DES_);
            $("#integridadRE"+j+"").val(grillaR[j].f01_reja_INT_);
            var madera = grillaR[j].f01_reja_MAD_;
            var madera = cambiarValorBool(madera);
            document.getElementById("maderaRE"+j+"").checked = madera;
            var otro = grillaR[j].f01_reja_OTRO_;
            var otro = cambiarValorBool(otro);
            console.log('otro',otro);
            document.getElementById("otrosRE"+j+"").checked = otro;
            var tubo = grillaR[j].f01_reja_TUBO_;
            var tubo = cambiarValorBool(tubo);
            document.getElementById("tuboRE"+j+"").checked = tubo;
            var hierro = grillaR[j].f01_reja_HIERRO_;
            var hierro = cambiarValorBool(hierro);
            document.getElementById("hierroRE"+j+"").checked = hierro;
            $("#otrosDRE"+j+"").val(grillaR[j].f01_reja_OTRODESC_); 
            } 
           arrRE.forEach(function (item, index, array) 
           {
            aarrCodigoRejas.push({adescripcionR:$("#descripcionRE"+item.id).val(), 
            ahierroR:document.getElementById("hierroRE"+item.id+"").checked, 
            atuboR:document.getElementById("tuboRE"+item.id+"").checked,
            amaderaR:document.getElementById("maderaRE"+item.id+"").checked,
            aotrosR:document.getElementById("otrosRE"+item.id+"").checked,
            aotrosDescR:$("#otrosDRE"+item.id+"").val(),
            aintegridadR:$("#integridadRE"+item.id+"").val()});
           });
          }

        //-----Grilla acabados muros---------
          var grillaAM = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_ACAMUR !== 'undefined'){
          grillaAM = JSON.parse(dataIN[0].data).cas_datos.PTR_ACAMUR;
          var longitudAM  = grillaAM.length;
          contAMact = longitudAM;
          editarAcabadoM(longitudAM);
           for (var j = 1; j < longitudAM; j++)
            {
           $("#descripcionAM"+j+"").val(grillaAM[j].f01_muro_DES_);
            var cal = grillaAM[j].f01_muro_CAL_;
            var cal = cambiarValorBool(cal);
            document.getElementById("calArenaAM"+j+"").checked = cal;
            var cemento = grillaAM[j].f01_muro_CEM_;
            var cemento = cambiarValorBool(cemento);
            document.getElementById("cementoAM"+j+"").checked = cemento;
            var piedra = grillaAM[j].f01_muro_PIE_;
            var piedra = cambiarValorBool(piedra);
            document.getElementById("piedraAM"+j+"").checked = piedra;
            var marmol = grillaAM[j].f01_muro_MAR_;
            var marmol = cambiarValorBool(marmol);
            document.getElementById("marmolAM"+j+"").checked = marmol; 
            var muro = grillaAM[j].f01_muro_MAD_;
            var muro = cambiarValorBool(muro);
            document.getElementById("maderamuroAM"+j+"").checked = muro;
            var azulejo = grillaAM[j].f01_muro_AZU_;
            var azulejo = cambiarValorBool(azulejo);
            document.getElementById("azulejoAM"+j+"").checked = azulejo; 
            var ladrillo = grillaAM[j].f01_muro_LAD_;
            var ladrillo = cambiarValorBool(ladrillo);
            document.getElementById("ladrilloAM"+j+"").checked = ladrillo; 
            var otro = grillaAM[j].f01_muro_OTRO_;
            var otro = cambiarValorBool(otro);
            document.getElementById("otroAcabAM"+j+"").checked = otro;
            $("#otrosDescAcab1"+j+"").val(grillaAM[j].f01_muro_OTRODESC_); 
            $("#integridAM"+j+"").val(grillaAM[j].f01_muro_INT_);   
            } 
           arrDibujoAM.forEach(function (item, index, array) 
           {
            aarrCodigoAcab.push({adescripcionA:$("#descripcionAM"+item.id+"").val(), 
            acalArenaA:document.getElementById("calArenaAM"+item.id+"").checked, 
            acementoA:document.getElementById("cementoAM"+item.id+"").checked,
            apiedraA:document.getElementById("piedraAM"+item.id+"").checked,
            amarmolA:document.getElementById("marmolAM"+item.id+"").checked,
            amaderaA:document.getElementById("maderamuroAM"+item.id+"").checked,
            aazulejoA:document.getElementById("azulejoAM"+item.id+"").checked,
            aladrilloA:document.getElementById("ladrilloAM"+item.id+"").checked,
            aotroAcabA:document.getElementById("otroAcabAM"+item.id+"").checked,
            aotroDescAcabA:$("#otrosDescAcab1"+item.id+"").val(),
            aintegridadAcabA:$("#integridAM"+item.id+"").val()});
           });
          }

          //------grilla pinturas------
          var grillaPIN = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_PINTURA !== 'undefined'){
          grillaPIN = JSON.parse(dataIN[0].data).cas_datos.PTR_PINTURA;
          var longitudP  = grillaPIN.length;
          contPact = longitudP;
          editarGrillaPinturas(longitudP);
           for (var j = 1; j < longitudP; j++)
            {
            $("#descripcionPIN"+j+"").val(grillaPIN[j].f01_pint_COD_);
            var cal = grillaPIN[j].f01_pint_CAL_;
            var cal = cambiarValorBool(cal);
            document.getElementById("calArenaPIN"+j+"").checked = cal;
            var acrilico = grillaPIN[j].f01_pint_ACR_;
            var acrilico = cambiarValorBool(acrilico);
            document.getElementById("acrilicoPIN"+j+"").checked = acrilico;
            var aceite = grillaPIN[j].f01_pint_ACE_;
            var aceite = cambiarValorBool(aceite);
            document.getElementById("aceitePIN"+j+"").checked = aceite;
            var otro = grillaPIN[j].f01_pint_OTRO_;
            var otro = cambiarValorBool(otro);
            document.getElementById("otroPIN"+j+"").checked = otro;
            $("#otrosDescPIN"+j+"").val(grillaPIN[j].f01_pint_OTRODESC_); 
            $("#integridPIN"+j+"").val(grillaPIN[j].f01_pint_INT_); 
            } 
           arrDibujoPIN.forEach(function (item, index, array) 
           {
            aarrCodigoPinturas.push({adescripcionPintu:$("#descripcionPIN"+item.id+"").val(), 
            acalArenaPintu:document.getElementById("calArenaPIN"+item.id+"").checked, 
            aacrilicoPintu:document.getElementById("acrilicoPIN"+item.id+"").checked,
            aaceitePintu:document.getElementById("aceitePIN"+item.id+"").checked,
            aotroPintu:document.getElementById("otroPIN"+item.id+"").checked,
            aotrosDescPintu:$("#otrosDescPIN"+item.id+"").val(),
            aintegridPintu:$("#integridPIN"+item.id+"").val()});
           });
          }
          
        //---------grilla pisos---------------
         var grillaPiso = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_PISOS !== 'undefined'){
          grillaPiso = JSON.parse(dataIN[0].data).cas_datos.PTR_PISOS;
          var longitudPi  = grillaPiso.length;
          contPactual = longitudPi;
          editarGrillaPiso(longitudPi);
           for (var j = 1; j < longitudPi; j++)
            {
            $("#descripcionPi"+j+"").val(grillaPiso[j].f01_pis_DES_);
            var baldosa = grillaPiso[j].f01_pis_BAL_;
            var baldosa = cambiarValorBool(baldosa);
            document.getElementById("baldosasPi"+j+"").checked = baldosa;
            var mosaico = grillaPiso[j].f01_pis_MOS_;
            var mosaico = cambiarValorBool(mosaico);
            document.getElementById("mosaicosPi"+j+"").checked = mosaico;
            var porcelanato = grillaPiso[j].f01_pis_POR_;
            var porcelanato = cambiarValorBool(porcelanato);
            document.getElementById("porcelanatoPi"+j+"").checked = porcelanato; 
            var plaja = grillaPiso[j].f01_pis_LAJ_;
            var plaja = cambiarValorBool(plaja);
            document.getElementById("plajaPi"+j+"").checked = plaja;
            var prio = grillaPiso[j].f01_pis_RIO_;
            var prio = cambiarValorBool(prio);
            document.getElementById("prioPi"+j+"").checked = prio;
            var ladrillo = grillaPiso[j].f01_pis_LAD_;
            var ladrillo = cambiarValorBool(ladrillo);
            document.getElementById("ladrillosPi"+j+"").checked = ladrillo;
            var grama = grillaPiso[j].f01_pis_GRA_;
            var grama = cambiarValorBool(grama);
            document.getElementById("gramaPi"+j+"").checked = grama;
            var tierra = grillaPiso[j].f01_pis_TIE_;
            var tierra = cambiarValorBool(tierra);
            document.getElementById("tierraPi"+j+"").checked =  tierra; 
            var concreto = grillaPiso[j].f01_pis_CON_;
            var concreto = cambiarValorBool(concreto);
            document.getElementById("concretoPi"+j+"").checked =  concreto;
            var ceramica = grillaPiso[j].f01_pis_CER_;
            var ceramica = cambiarValorBool(ceramica);
            document.getElementById("ceramicaPi"+j+"").checked =  ceramica; 
            var piso = grillaPiso[j].f01_pis_MAD_;
            var piso = cambiarValorBool(piso);
            document.getElementById("maderaPi"+j+"").checked =  piso;
            var otro = grillaPiso[j].f01_pis_OTRO_;
            var otro = cambiarValorBool(otro);
            document.getElementById("otrosPi"+j+"").checked =  otro;
            $("#otrosDescPi"+j+"").val(grillaPiso[j].f01_pis_OTRODESC_);
            $("#integridPi"+j+"").val(grillaPiso[j].f01_pis_INT_);
            } 
           arrDibujoP.forEach(function (item, index, array) 
           {
            aarrCodigoPiso.push({adescripcionPisoA:$("#descripcionPi"+item.id+"").val(),     
            abaldosasPisoA:document.getElementById("baldosasPi"+item.id+"").checked,
            amosaicosPisoA:document.getElementById("mosaicosPi"+item.id+"").checked,
            aporcelanatoPisoA:document.getElementById("porcelanatoPi"+item.id+"").checked,
            aplajaPisoA:document.getElementById("plajaPi"+item.id+"").checked,
            aprioPisoA:document.getElementById("prioPi"+item.id+"").checked, 
            aladrillosPisoA:document.getElementById("ladrillosPi"+item.id+"").checked,
            agramaPisoA:document.getElementById("gramaPi"+item.id+"").checked,
            atierraPisoA:document.getElementById("tierraPi"+item.id+"").checked,
            aconcretoPisoA:document.getElementById("concretoPi"+item.id+"").checked, 
            aceramicaPisoA:document.getElementById("ceramicaPi"+item.id+"").checked,
            amaderaPisoA:document.getElementById("maderaPi"+item.id+"").checked,
            aotrosPisoA:document.getElementById("otrosPi"+item.id+"").checked,
            aotroDescPisoA:$("#otrosDescPi"+item.id+"").val(),
            aintegridadPisoA:$("#integridPi"+item.id+"").val()});
           });
          }
         
         //----grilla puertas de acceso
          var grillaPax = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_PUERTA !== 'undefined'){
          grillaPax = JSON.parse(dataIN[0].data).cas_datos.PTR_PUERTA;
          var longitudPA  = grillaPax.length;
          contPAactual = longitudPA;
          editarPuertasAcceso(longitudPA);
           for (var j = 1; j < longitudPA; j++)
            {
            $("#descripcionPA"+j+"").val(grillaPax[j].f01_pue_DES_);
            var madera = grillaPax[j].f01_pue_MAD_;
            var madera = cambiarValorBool(madera);
            document.getElementById("maderaPA"+j+"").checked =  madera;
            var metal = grillaPax[j].f01_pue_MET_;
            var metal = cambiarValorBool(metal);
            document.getElementById("metalPA"+j+"").checked =  metal;
            var otros = grillaPax[j].f01_pue_OTRO_;
            var otros = cambiarValorBool(otros);
            document.getElementById("otrosPA"+j+"").checked =  otros;
            $("#otrosDescPA"+j+"").val(grillaPax[j].f01_pue_OTRODESC_);
            $("#integridPA"+j+"").val(grillaPax[j].f01_pue_INT_);  
            } 
           arrDibujoPAx.forEach(function (item, index, array) 
           {
            aarrCodigoPuertasAcceso.push({adescripcionPuertasAcceso:$("#descripcionPA"+item.id+"").val(),     
            amaderaPuertaAcceso:document.getElementById("maderaPA"+item.id+"").checked, 
            ametalPuertaAcceso:document.getElementById("metalPA"+item.id+"").checked,
            aotrosPuertaAcceso:document.getElementById("otrosPA"+item.id+"").checked,
            aotroDescPuertasAccesoA:$("#otrosDescPA"+item.id+"").val(),
            aintegridadPuertasAccesoA:$("#integridPA"+item.id+"").val()});
           });
          }

          //------grilla mobiliario urbano---------

          var grillaMUr = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_MOBURB !== 'undefined'){
          grillaMUr = JSON.parse(dataIN[0].data).cas_datos.PTR_MOBURB;
          var longitudMU  = grillaMUr.length;
          contMUactual = longitudMU;
          editarMobiliarioUrbano(longitudMU);
           for (var j = 1; j < longitudMU; j++)
            {
            $("#descMUr"+j+"").val(grillaMUr[j].f01_mob_DES_);
            var madera = grillaMUr[j].f01_mob_MAD_;
            var madera = cambiarValorBool(madera);
            document.getElementById("maderMUr"+j+"").checked =  madera;
            var metal = grillaMUr[j].f01_mob_MET_;
            var metal = cambiarValorBool(metal);
            document.getElementById("metalMUr"+j+"").checked =  metal;
            var barro = grillaMUr[j].f01_mob_BARR_;
            var barro = cambiarValorBool(barro);
            document.getElementById("barroMUr"+j+"").checked =  barro;
            var cal = grillaMUr[j].f01_mob_CAL_;
            var cal = cambiarValorBool(cal);
            document.getElementById("calMUr"+j+"").checked =  cal;
            var ladrillo = grillaMUr[j].f01_mob_LAD_;
            var ladrillo = cambiarValorBool(ladrillo);
            document.getElementById("ladrilloMUr"+j+"").checked =  ladrillo;
            var piedra = grillaMUr[j].f01_mob_PIE_;
            var piedra = cambiarValorBool(piedra);
            document.getElementById("piedraMUr"+j+"").checked =  piedra;
            var adobe = grillaMUr[j].f01_mob_ADO_;
            var adobe = cambiarValorBool(adobe);
            document.getElementById("adobeMUr"+j+"").checked =  adobe;
            var concreto = grillaMUr[j].f01_mob_CON_;
            var concreto = cambiarValorBool(concreto);
            document.getElementById("concretoMUr"+j+"").checked =  concreto;
            var carbon = grillaMUr[j].f01_mob_CAR_;
            var carbon = cambiarValorBool(carbon);
            document.getElementById("fcarbonMUr"+j+"").checked =  carbon;
            var vidrio = grillaMUr[j].f01_mob_VID_;
            var vidrio = cambiarValorBool(vidrio);
            document.getElementById("vidrioMUr"+j+"").checked =  vidrio;
            var fibra = grillaMUr[j].f01_mob_FIBR_;
            var fibra = cambiarValorBool(fibra);
            document.getElementById("fibrocemMUr"+j+"").checked =  fibra;
            var otro = grillaMUr[j].f01_mob_OTRO_;
            var otro = cambiarValorBool(otro);
            document.getElementById("otrosMUr"+j+"").checked =  otro;
            $("#otrosDesOUr"+j+"").val(grillaMUr[j].f01_mob_OTRODESC_);
            $("#integridMUr"+j+"").val(grillaMUr[j].f01_mob_INT_); 
            } 
           arrDibujoMUr.forEach(function (item, index, array) 
           {
            aarrMobiliarioUrbano.push({adescripcionMovilidadUrbano:$("#descMUr"+item.id+"").val(),     
            amaderaMovilidadUrbano:document.getElementById("maderMUr"+item.id+"").checked,
            ametalMovilidadUrbano:document.getElementById("metalMUr"+item.id+"").checked,
            abarroMovilidadUrbano:document.getElementById("barroMUr"+item.id+"").checked,
            acalMovilidadUrbano:document.getElementById("calMUr"+item.id+"").checked,
            aladrilloMovilidadUrbano:document.getElementById("ladrilloMUr"+item.id+"").checked, 
            apiedraMovilidadUrbano:document.getElementById("piedraMUr"+item.id+"").checked,
            aadobeMovilidadUrbano:document.getElementById("adobeMUr"+item.id+"").checked,
            aconcretoMovilidadUrbano:document.getElementById("concretoMUr"+item.id+"").checked,
            afcarbonMovilidadUrbano:document.getElementById("fcarbonMUr"+item.id+"").checked, 
            avidrioMovilidadUrbano:document.getElementById("vidrioMUr"+item.id+"").checked,
            afibrocemMovilidadUrbano:document.getElementById("fibrocemMUr"+item.id+"").checked,
            aotrosMovilidadUrbano:document.getElementById("otrosMUr"+item.id+"").checked,
            aotroDescMovilidadUrbanoMovilidadUrbano:$("#otrosDesOUr"+item.id+"").val(),
            aintegridadMovilidadUrbanoMovilidadUrbano:$("#integridMUr"+item.id+"").val()});
           });
          }

         //------grilla escaleras-------
          var grillaEsca = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_ESCAL !== 'undefined'){
          grillaEsca = JSON.parse(dataIN[0].data).cas_datos.PTR_ESCAL;
          var longitudE  = grillaEsca.length;
          contEactual = longitudE;
          editarGrillaEscaleras(longitudE);
           for (var j = 1; j < longitudE; j++)
            {
            $("#descEsca"+j+"").val(grillaEsca[j].f01_esc_DES_);
            var madera = grillaEsca[j].f01_esc_MAD_;
            var madera = cambiarValorBool(madera);
            document.getElementById("maderaEsca"+j+"").checked =  madera;
            var metal = grillaEsca[j].f01_esc_MET_;
            var metal = cambiarValorBool(metal);
            document.getElementById("metalEsca"+j+"").checked =  metal;
             var concreto = grillaEsca[j].f01_esc_CON_;
            var concreto = cambiarValorBool(concreto);
            document.getElementById("ConcretoEsca"+j+"").checked =  concreto;
            var piedra = grillaEsca[j].f01_esc_PIE_;
            var piedra = cambiarValorBool(piedra);
            document.getElementById("PiedraEsca"+j+"").checked =  piedra;
            var ladrillo = grillaEsca[j].f01_esc_LAD_;
            var ladrillo = cambiarValorBool(ladrillo);
            document.getElementById("ladrilloEsca"+j+"").checked =  ladrillo; 
            var tierra = grillaEsca[j].f01_esc_TIE_;
            var tierra = cambiarValorBool(tierra);
            document.getElementById("tierraEsca"+j+"").checked =  tierra;
            var otro = grillaEsca[j].f01_esc_OTRO_;
            var otro = cambiarValorBool(otro);
            document.getElementById("otrosEsca"+j+"").checked =  otro;
            $("#otrosDescEsca"+j+"").val(grillaEsca[j].f01_esc_OTRODESC_);
            $("#integridEsca"+j+"").val(grillaEsca[j].f01_esc_INT_); 
            } 
           arrDibujoEsca.forEach(function (item, index, array) 
           {
            aarrCodigoEscaleras.push({adescripcionEscaleras:$("#descEsca"+item.id+"").val(),     
                amaderaEscaleras:document.getElementById("maderaEsca"+item.id+"").checked, 
                ametalEscaleras:document.getElementById("metalEsca"+item.id+"").checked,
                aConcretoEscaleras:document.getElementById("ConcretoEsca"+item.id+"").checked,
                aPiedraEscaleras:document.getElementById("PiedraEsca"+item.id+"").checked,
                aladrilloEscaleras:document.getElementById("ladrilloEsca"+item.id+"").checked, 
                atierraEscaleras:document.getElementById("tierraEsca"+item.id+"").checked,
                aotrosEscaleras:document.getElementById("otrosEsca"+item.id+"").checked,
                aotroDescEscaleras:$("#otrosDescEsca"+item.id+"").val(),
                aintegridadEscaleras:$("#integridEsca"+item.id+"").val()});
           });
          }
          //-----grilla característica de la vegetación---------
          var grillaCVege = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_VEGET !== 'undefined'){
          grillaCVege = JSON.parse(dataIN[0].data).cas_datos.PTR_VEGET;
          var longitudCV  = grillaCVege.length;
          contVactualizar = longitudCV;
          editarGrillaCarVegetacion(longitudCV);
           for (var j = 1; j < longitudCV; j++)
            {
            $("#descCVeg"+j+"").val(grillaCVege[j].f01_veg_DES_);
         
            $("#cantidadCVeg"+j+"").val(grillaCVege[j].f01_veg_CAN_);
            var sflor = grillaCVege[j].f01_veg_SFL_;
            var sflor = cambiarValorBool(otro);
            document.getElementById("sinFlorCVeg"+j+"").checked =  sflor;
            var caduca = grillaCVege[j].f01_veg_CAD_;
            var caduca = cambiarValorBool(caduca);
            document.getElementById("caducaCVeg"+j+"").checked =  caduca; 
            var perenne = grillaCVege[j].f01_veg_PER_;
            var perenne = cambiarValorBool(perenne);
            document.getElementById("perenneCVeg"+j+"").checked =  perenne;
            var otros = grillaCVege[j].f01_veg_OTRO_;
            var otros = cambiarValorBool(otros);
            document.getElementById("otrosCVeg"+j+"").checked =  otros;
            $("#otrosDescCVeg"+j+"").val(grillaCVege[j].f01_veg_OTRODESC_); 
            $("#integridCVeg"+j+"").val(grillaCVege[j].f01_veg_INT_);
            } 
           arrDibujoCVeg.forEach(function (item, index, array) 
           {
            aarrCodigoCarVegetacion.push({adescripcionCarVegetacion:$("#descCVeg"+item.id+"").val(),     
            acantidadCarVegetacion:$("#cantidadCVeg"+item.id+"").val(),
            asinFloracionCarVegetacion:document.getElementById("sinFlorCVeg"+item.id+"").checked,
            acaducaCarVegetacion:document.getElementById("caducaCVeg"+item.id+"").checked, 
            aperenneCarVegetacion:document.getElementById("perenneCVeg"+item.id+"").checked,
            aotrosCarVegetacion:document.getElementById("otrosCVeg"+item.id+"").checked,
            aotroDescCarVegetacion:$("#otrosDescCVeg"+item.id+"").val(),
            aintegridadCarVegetacion:$("#integridCVeg"+item.id+"").val()});
           });
          }

         //------grilla detalle artistico-------------------------
          var grillaDArt = "";
          if(typeof JSON.parse(dataIN[0].data).cas_datos.PTR_DETALLES !== 'undefined'){
          grillaDArt = JSON.parse(dataIN[0].data).cas_datos.PTR_DETALLES;
          var longitudDA  = grillaDArt.length;
          contDAact = longitudDA;
          editarDetaArtistoco(longitudDA);
           for (var j = 1; j < longitudDA; j++)
            {
            $("#descripDA"+j+"").val(grillaDArt[j].f01_det_DES_);
            $("#integridadDA"+j+"").val(grillaDArt[j].f01_det_INT_);
            } 
           arrDibujoDArt.forEach(function (item, index, array) 
           {
            aarrCodigoDetaArtistoco.push({adescripcionDetalleArtistoco:$("#descripDA"+item.id+"").val(),   
           aintegridadDetaArtistoco:$("#integridadDA"+item.id+"").val()});
           });
          }
 
         
          },
        error: function(result) {
          swal( "Error..!", "Error....", "error" );
        }
      });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
      });
};


$("#actualizarEspaciosAbiertos").click(function(){


  var cas_id = $( "#acas_id" ).val();
  var cas_nro_caso = $( "#aespnumero_ficha" ).val();
  var cas_nombre_caso = $( "#aespcodigo_ficha" ).val();
  var PTR_CREA = $( "#acreador" ).val();
  var PTR_ACTUALIZA = $( "#aautorizador" ).val();
  var PTR_COD_CAT = $( "#acodigocatastral" ).val();
  var PTR_GEOREF = $( "#ageoreferencia" ).val();
  var PTR_TRAD = $( "#atradicional" ).val();
  var PTR_ACT = $( "#aactual" ).val();
  var PTR_DEP = $( "#adepartamento" ).val();
  var PTR_CIPO = $( "#aprovincia" ).val();
  var PTR_MUNI = $( "#amunicipio" ).val();
  var macrodistritoTxt=document.getElementById("amacrodistritoespabie");
  var PTR_MACRO=macrodistritoTxt.options[macrodistritoTxt.selectedIndex].text;
  console.log(PTR_MACRO,77777777777777777);
  var PTR_BARR = $( "#abarrio" ).val();
  var PTR_DIR = $( "#adireccioncalle" ).val();
  var PTR_ESQ1 = $( "#aesquina1" ).val();
  var PTR_ESQ2 = $( "#aesquina2" ).val();
  var PTR_ALT = $( "#aaltitud" ).val();
  var PTR_NOMPRO = $( "#anombrepropietario" ).val();
  var PTR_REPRO = $( "#atiporegpropietario" ).val();
  /*-----------------------------------------------*/

  //var PTR_USO           = 'Uso de suelo';
  //var PTR_SERV          = 'Servicio'
//var PTR_OTHER         = $("#aregprootrosservicios").val();
var ptr_other1 = document.getElementById("aregprootrosservicios1").checked;
var ptr_other2 = document.getElementById("aregprootrosservicios2").checked;
var ptr_other3 = document.getElementById("aregprootrosservicios3").checked;
var ptr_other_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_other1+', "resvalor": "Internet"},{"resid": 285, "estado": '+ptr_other2+', "resvalor": "Teléfono"},{"resid": 286, "estado": '+ptr_other3+', "resvalor": "TV Cable"}]';

//////////console.log(888888888888, PTR_OTHER);
//var PTR_FIGPRO        = $("#cmarlegfiguraproyeccion").val();
var PTR_FIGPRO=document.getElementById("amarlegfiguraproyeccion").value;


//var PTR_REFHIST       = $("#crefhisreferenciahistorica").val();
var PTR_REFHIST=document.getElementById("arefhisreferenciahistorica").value;




var PTR_FECCONS       = $("#adatorafechaconstruccion").val();
var PTR_AUTOR         = $("#adatorautorconstructor").val();
var PTR_PREEX         = $("#adatorapreexistenciaedificacion").val();
var PTR_PREEX = caracteresEspecialesRegistrar(PTR_PREEX);

var PTR_PROP          = $("#adatorapropietario").val();
var PTR_USOORG        = $("#adatorausosoriginales").val();
var PTR_USOORG = caracteresEspecialesRegistrar(PTR_USOORG);

var PTR_HECHIS        = $("#adatorahechoshistoricosdist").val();
var PTR_HECHIS = caracteresEspecialesRegistrar(PTR_HECHIS);

var PTR_FIEREL        = $("#adatorafiestasreligiosas").val();
var PTR_FIEREL = caracteresEspecialesRegistrar(PTR_FIEREL);

var PTR_DATHISC       = $("#adatoradatoshistoricos").val();
var PTR_DATHISC = caracteresEspecialesRegistrar(PTR_DATHISC);

var PTR_FUENTE        = $("#adatorafuentebibliografia").val();
var PTR_FUENTE = caracteresEspecialesRegistrar(PTR_FUENTE);

//V. DATOS DOCUMENTALES  
var PTR_FECCONST      = $("#adatdocfechadeconstruccion").val();
var PTR_AUTCONST      = $("#adatdocautoroconstructor").val();
var PTR_PREDIN        = $("#adatdocpreexistenciainvestigacion").val();
var PTR_PREDIN = caracteresEspecialesRegistrar(PTR_PREDIN);

var PTR_PROPIE        = $("#adatdocpropietario").val();
var PTR_USOPREEX      = $("#adatdocusospreexistentes").val();
var PTR_USOPREEX = caracteresEspecialesRegistrar(PTR_USOPREEX);

var PTR_HECHISD       = $("#adatdochechoshistoricospers").val();
var PTR_HECHISD = caracteresEspecialesRegistrar(PTR_HECHISD);

var PTR_FIERELD       = $("#adatdocfiestasreli").val();
var PTR_FIERELD = caracteresEspecialesRegistrar(PTR_FIERELD);

var PTR_DATHISTC      = $("#adatdocdatoshistoricosconj").val();
var PTR_DATHISTC = caracteresEspecialesRegistrar(PTR_DATHISTC);

var PTR_FUENTBIO      = $("#adatdocfuentebiblio").val();
var PTR_FUENTBIO = caracteresEspecialesRegistrar(PTR_FUENTBIO);

//VI. INSCRIPCIONES         
var PTR_INSCR         = $("#ainsinscripciones").val();
var PTR_INSCR = caracteresEspecialesRegistrar(PTR_INSCR);

//VII. ÉPOCA   
//var PTR_EPOCA         = $("#cepoepoca").val();
var PTR_EPOCA=document.getElementById("aepoepoca").value;


var PTR_ACONS         = $("#aepoanoconstruccion").val();
var PTR_AULTR         = $("#aepoanoremodelacion").val();
//VII. ESTILO    
//var PTR_ESTILO        = $("#cestestilo").val();
var PTR_ESTILO=document.getElementById("aestestilo").value;


//IX. DESCRIPCIÓN DEL AREA
//var PTR_UBICESP       = $("#cdesareubicacionespacio").val();
var PTR_UBICESP=document.getElementById("adesareubicacionespacio").value;


//var PTR_LINCONS       = $("#cdesarelineaconstruccion").val();
var PTR_LINCONS=document.getElementById("adesarelineaconstruccion").value;


//var PTR_TIPOLO        = $("#cdesaretipologia").val();
var PTR_TIPOLO=document.getElementById("adesaretipologia").value;


//var PTR_MURMAT        = $("#adesarematerial").val();
var ptr_murmat1 = document.getElementById("adesarematerial1").checked;
var ptr_murmat2 = document.getElementById("adesarematerial2").checked;
var ptr_murmat3 = document.getElementById("adesarematerial3").checked;
var ptr_murmat4 = document.getElementById("adesarematerial4").checked;
var ptr_murmat5 = document.getElementById("adesarematerial5").checked;
var ptr_murmat6 = document.getElementById("adesarematerial6").checked;
var ptr_murmat7 = document.getElementById("adesarematerial7").checked;
var ptr_murmat8 = document.getElementById("adesarematerial8").checked;
var ptr_murmat9 = document.getElementById("adesarematerial9").checked;
var ptr_murmat10 = document.getElementById("adesarematerial10").checked;

var ptr_murmat_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_murmat1+', "resvalor": "Adobe"},{"resid": 278, "estado": '+ptr_murmat2+', "resvalor": "Concreto Armado"},{"resid": 279, "estado": '+ptr_murmat3+', "resvalor": "Hormigon Armado"},{"resid": 280, "estado": '+ptr_murmat4+', "resvalor": "Ladrillo de Barro"},{"resid": 281, "estado": '+ptr_murmat5+', "resvalor": "Madera"},{"resid": 282, "estado": '+ptr_murmat6+', "resvalor": "Otros"},{"resid": 283, "estado": '+ptr_murmat7+', "resvalor": "Piedra"},{"resid": 284, "estado": '+ptr_murmat8+', "resvalor": "Piedra Cal"},{"resid": 285, "estado": '+ptr_murmat9+', "resvalor": "Prefabricados"},{"resid": 286, "estado": '+ptr_murmat10+', "resvalor": "Tapial"}]';


var PTR_MURMAT_OTROS  = $("#adesareotros").val();
//var PTR_MUREST        = $("#cdesareestado").val();
var PTR_MUREST=document.getElementById("adesareestado").value;


//var PTR_MURINTE       = $("#cdesareintegridad").val();
var PTR_MURINTE=document.getElementById("adesareintegridad").value;


//var PTR_REJAS = 'Rejas';

//var PTR_REJAEST       = $("#cdesareestadorejas").val();
var PTR_REJAEST=document.getElementById("adesareestadorejas").value;


//var PTR_ACAMUR = 'Acabados de Muros';

//var PTR_ACAEST        = $("#cdesareestadoacabadomur").val();
var PTR_ACAEST=document.getElementById("adesareestadoacabadomur").value;


//var PTR_PINTURA = 'Pintura';

//var PTR_PINTEST       = $("#cdesareestadopint").val();
var PTR_PINTEST=document.getElementById("adesareestadopint").value;


//var PTR_PISOS = 'Pisos';

//var PTR_PISOEST       = $("#cdesareestadopisos").val();
var PTR_PISOEST=document.getElementById("adesareestadopisos").value;

//var PTR_PUERTA = 'Puertas de Acceso';

//var PTR_PUERTAEST     = $("#cdesareestadopuertasacc").val();
var PTR_PUERTAEST=document.getElementById("adesareestadopuertasacc").value;


//var PTR_MOBURB  = "Mobiliario";

//var PTR_MOBEST        = $("#cdesareestadomoviliariourb").val();
var PTR_MOBEST=document.getElementById("adesareestadomoviliariourb").value;


//var PTR_ESCAL = 'Escalera';

//var PTR_ESCEST        = $("#cdesareestadoescaleras").val();
var PTR_ESCEST=document.getElementById("adesareestadoescaleras").value;


//var PTR_VEGET = 'Caracterísiticas de la Vegetación';

//var PTR_VEGEST        = $("#cdesareestadocaracteristicasvege").val();
var PTR_VEGEST=document.getElementById("adesareestadocaracteristicasvege").value;



//var PTR_DETALLES = 'Detalle artistico';

//var PTR_DETALLEST     = $("#cdesareestadodetalleartistico").val();
var PTR_DETALLEST = document.getElementById("adesareestadodetalleartistico").value;


//x. ESQUEMAS
var PTR_ESQDES        =$("#aesqesquemas").val();
var PTR_ESQDES = caracteresEspecialesRegistrar(PTR_ESQDES);

//XI. ANGULO VISUAL HORIZONTAL
var PTR_ANGOR         = $("#aangvisorientacion").val();
var PTR_ANGGRA        = $("#aangvisgrados").val();
//var PTR_VISTAS        = $("#aangvisvistas").val();
var ptr_vistas1 = document.getElementById("aangvisvistas1").checked;
var ptr_vistas2 = document.getElementById("aangvisvistas2").checked;
var ptr_vistas_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_vistas1+', "resvalor": "Vista Parcial  "},{"resid": 271, "estado": '+ptr_vistas2+', "resvalor": "Vista Panorámica"}]';

var PTR_ANGOTR        = $("#aangvisotros").val();
//XII. ELEMENTOS VISUALES
var PTR_ELEMVISID     = $("#aelevisizquierda_derecha").val();
var PTR_ELEMVISID = caracteresEspecialesRegistrar(PTR_ELEMVISID);

var PTR_ELEMVISAL     = $("#aelivisalto").val();
var PTR_ELEMVISAL = caracteresEspecialesRegistrar(PTR_ELEMVISAL);

var PTR_ELEMVISAB     = $("#aelivisbajo").val();
var PTR_ELEMVISAB = caracteresEspecialesRegistrar(PTR_ELEMVISAB);

//XIII. DESCRIPCIÓN DEL ENTORNO
var PTR_DESENTEI      = $("#adesententorno_inmediato").val();
var PTR_DESENTEI = caracteresEspecialesRegistrar(PTR_DESENTEI);

//XIV. DESCRIPCIÓN DEL ESPACIO
var PTR_DESESPDES     = $("#adesespdescrpcion_espacio").val();
var PTR_DESESPDES = caracteresEspecialesRegistrar(PTR_DESESPDES);

//XV. MEDIDAS 
var PTR_MEDIDAREA     = $("#amedarea_total").val();
var PTR_MEDIDANC      = $("#amedancho").val();
var PTR_MEDIDLAR      = $("#amedlargo").val();
var PTR_MEDIAMC       = $("#amedancho_muro").val();
//XVI. INFORMACIÓN ADICIONAL
//var PTR_INFADICIONAL  = $("#cinformacion_adicional").val();
var PTR_INFADICIONAL=document.getElementById("ainformacion_adicional").value;


var PTR_INFOBS        = $("#ainfadicobservaciones").val();
var PTR_INFOBS = caracteresEspecialesRegistrar(PTR_INFOBS);

//XVIII. PATOLOGIAS  
//var PTR_PATODAN       = $("#apatdanos").val();
var ptr_patodan1 = document.getElementById("apatdanos1").checked;
var ptr_patodan2 = document.getElementById("apatdanos2").checked;
var ptr_patodan3 = document.getElementById("apatdanos3").checked;
var ptr_patodan4 = document.getElementById("apatdanos4").checked;
var ptr_patodan5 = document.getElementById("apatdanos5").checked;
var ptr_patodan6 = document.getElementById("apatdanos6").checked;

var ptr_patodan_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patodan1+', "resvalor": "Asentamientos"},{"resid": 278, "estado": '+ptr_patodan2+', "resvalor": "Desplomes"},{"resid": 279, "estado": '+ptr_patodan3+', "resvalor": "Desprendimientos"},{"resid": 279, "estado": '+ptr_patodan4+', "resvalor": "Humedad"},{"resid": 280, "estado": '+ptr_patodan5+', "resvalor": "Otros"},{"resid": 281, "estado": '+ptr_patodan6+', "resvalor": "Podredumbre"}]';

var PTR_PATO_OTROS    = $("#apatotros").val();
var PTR_PATOOBS       = $("#apatobservaciones").val();
var PTR_PATOOBS = caracteresEspecialesRegistrar(PTR_PATOOBS);

//var PTR_PATOCAU       = $("#apatcausas").val();
var ptr_patocau1 = document.getElementById("apatcausas1").checked;
var ptr_patocau2 = document.getElementById("apatcausas2").checked;
var ptr_patocau3 = document.getElementById("apatcausas3").checked;
var ptr_patocau4 = document.getElementById("apatcausas4").checked;
var ptr_patocau5 = document.getElementById("apatcausas5").checked;
var ptr_patocau6 = document.getElementById("apatcausas6").checked;
var ptr_patocau7 = document.getElementById("apatcausas7").checked;



var ptr_patocau_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_patocau1+', "resvalor": "Abandono"},{"resid": 278, "estado": '+ptr_patocau2+', "resvalor": "Botánicos"},{"resid": 279, "estado": '+ptr_patocau3+', "resvalor": "Calidad de los materiales"},{"resid": 280, "estado": '+ptr_patocau4+', "resvalor": "Catastrofe Natural"},{"resid": 281, "estado": '+ptr_patocau5+', "resvalor": "Deficiencias constructivas"},{"resid": 282, "estado": '+ptr_patocau6+', "resvalor": "Falta de mantenimiento"},{"resid": 283, "estado": '+ptr_patocau7+', "resvalor": "Otros"}]';

var PTR_PATOCAU_OTROS = $("#apatotros ").val();
//XIX. VALORACIÓN  
//var PTR_VALORHIS      = $("#avalhistorico").val();
//var PTR_VALORHIS      = $("#cvalhistorico").val();
var ptr_valorhis1 = document.getElementById("avalhistorico1").checked;
var ptr_valorhis2 = document.getElementById("avalhistorico2").checked;
var ptr_valorhis3 = document.getElementById("avalhistorico3").checked;
var ptr_valorhis4 = document.getElementById("avalhistorico4").checked;
var ptr_valorhis5 = document.getElementById("avalhistorico5").checked;

var ptr_valorhis_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorhis1+', "resvalor": "Espacio de valor testimonial y documental que ilustra el desarrollo político, social, religioso, cultural, económico y de forma de vida de un periodo determinado, contribuyendo a entender mejor el desarrollo histórico de una comunidad"},{"resid": 278, "estado": '+ptr_valorhis2+', "resvalor": "Espacio que desde su creación se constituye en un hito en la memoria histórica"},{"resid": 279, "estado": '+ptr_valorhis3+', "resvalor": "Espacio que en el trascurso histórico se constituyo en hito, al ser escenario relacionado con personas o eventos importantes, dentro del proceso histórico, social, cultural, económico de las comunidades"},{"resid": 280, "estado": '+ptr_valorhis4+', "resvalor": "Espacios que forman parte de un conjunto histórico"},{"resid": 281, "estado": '+ptr_valorhis5+', "resvalor": "Espacio que posee elementos arqueológicos que documentan su evolución y desarrollo"}]';

//var PTR_VALORART      = $("#avalartistico").val();
var ptr_valorart1 = document.getElementById("avalartistico1").checked;
var ptr_valorart2 = document.getElementById("avalartistico2").checked;
var ptr_valorart3 = document.getElementById("avalartistico3").checked;

var ptr_valorart_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorart1+', "resvalor": "Ejemplo sobresaliente por su singularidad"},{"resid": 278, "estado": '+ptr_valorart2+', "resvalor": "Conservar elementos arquitectónicos y tradicionales de interés"},{"resid": 279, "estado": '+ptr_valorart3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';

//var PTR_VALORARQ      = $("#avalarquitectonico").val();
var ptr_valorarq1 = document.getElementById("avalarquitectonico1").checked;
var ptr_valorarq2 = document.getElementById("avalarquitectonico2").checked;
var ptr_valorarq3 = document.getElementById("avalarquitectonico3").checked;

var ptr_valorarq_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorarq1+', "resvalor": "Poseedor de caracteristicas tipológicas representativas de estilos arquitectónicos"},{"resid": 278, "estado": '+ptr_valorarq2+', "resvalor": "Con principios morfológicos: unidad, composición volumétrica, materiales, contraste, textura, proporción en detalle"},{"resid": 279, "estado": '+ptr_valorarq3+', "resvalor": "Espacio poseedor de manifestaciones artísticas y decorativas de interés"}]';


//var PTR_VALORTEC      = $("#avaltecnologico").val();
var ptr_valortec1 = document.getElementById("avaltecnologico1").checked;
var ptr_valortec2 = document.getElementById("avaltecnologico2").checked;
var ptr_valortec3 = document.getElementById("avaltecnologico3").checked;

var ptr_valortec_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valortec1+', "resvalor": "Espacio donde la aplicación de técnicas contructivas son singulares o de especial interés"},{"resid": 278, "estado": '+ptr_valortec2+', "resvalor": "Espacio que se constituye en un exponente de las técnicas constructivas y uso de los materiales característicos de una época o región determinada"},{"resid": 279, "estado": '+ptr_valortec2+', "resvalor": "Espacios con sistemas constructivos y elementos arquitectónicos realizados por su mano de obra especializada"}]';

//var PTR_VALORINTG     = $("#avalintegridad").val();
var ptr_valorintg1 = document.getElementById("avalintegridad1").checked;
var ptr_valorintg2 = document.getElementById("avalintegridad2").checked;

var ptr_valorintg_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorintg1+', "resvalor": "Espacio que conserva el total de su tipología, materiales y técnicas constructivas originales"},{"resid": 278, "estado": '+ptr_valorintg2+', "resvalor": "Espacio abierto que ha sufrido intervenciones que pueden ser reversibles con obras menores, para recuperar las características originales"}]';

//var PTR_VALORURB      = $("#avalurbano").val();
var ptr_valorurb1 = document.getElementById("avalurbano1").checked;
var ptr_valorurb2 = document.getElementById("avalurbano2").checked;

var ptr_valorurb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorurb1+', "resvalor": "Espacio que contribuye a definir un entorno de valor por su configuración y calidad en la estructura urbanística, el paisaje y/o el espacio público"},{"resid": 278, "estado": '+ptr_valorurb2+', "resvalor": "Considerado hito de referencia por su emplazamiento"}]';


//var PTR_VALORINT      = $("#avalintangible").val();
var ptr_valorint1 = document.getElementById("avalintangible1").checked;

var ptr_valorint_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorint1+', "resvalor": "Espacio relacionado con la organización social o forma de vida: usos, representacines, expresiones conocimientos y técnicas que las comunidades y grupos sociales reconozcan como parte de su patrimonio"}]';

//var PTR_VALORSIMB     = $("#avalsimbolico").val();
var ptr_valorsimb1 = document.getElementById("avalsimbolico1").checked;

var ptr_valorsimb_check = '[{"tipo": "CHKM"},{"resid": 277, "estado": '+ptr_valorsimb1+', "resvalor": "Espacio o espacio abierto único que expresa, representa, o significa identidad y pertenencia para el colectivo"}]';

//XX. CRITERIOS DE VALORACIÓN
var PTR_CRITERHIS     = $("#acrivalhistorico").val();
var PTR_CRITERHIS = caracteresEspecialesRegistrar(PTR_CRITERHIS);
var PTR_CRITERART     = $("#acrivalartistico").val();
var PTR_CRITERART = caracteresEspecialesRegistrar(PTR_CRITERART);
var PTR_CRITERARQ     = $("#acrivalarquitectonico").val();
var PTR_CRITERARQ = caracteresEspecialesRegistrar(PTR_CRITERARQ);
var PTR_CRITERTEC     = $("#acrivaltecnologicos").val();
var PTR_CRITERTEC = caracteresEspecialesRegistrar(PTR_CRITERTEC);
var PTR_CRITERINT     = $("#acrivalintegridad").val();
var PTR_CRITERINT = caracteresEspecialesRegistrar(PTR_CRITERINT);
var PTR_CRITERURB     = $("#acrivalurbano").val();
var PTR_CRITERURB = caracteresEspecialesRegistrar(PTR_CRITERURB);
var PTR_CRITERINTA    = $("#acrivalintangible").val();
var PTR_CRITERINTA = caracteresEspecialesRegistrar(PTR_CRITERINTA);
var PTR_CRITERSIMB    = $("#acrivalsimbolico").val();
var PTR_CRITERSIMB = caracteresEspecialesRegistrar(PTR_CRITERSIMB);
//PERSONAL ENCARGADO  
var PTR_PERINV        = $("#aperencinventariador").val();
var PTR_PERREV        = $("#aperrenrevisado").val();
var PTR_PERDIG        = $("#aperencdigitalizado").val();
///-------------TEXTAREA-----------------
//---actualizar puntos colineales----
var contPCact = 0;
 arrDibujoPC.forEach(function (item, index, array){
    arrarPCactual[contPCact].var_numero = $("#aNroDescPuntosColineales"+item.id+"").val();
    arrarPCactual[contPCact].var_puntocol = $("#aPuntoColinealPuntosColineales"+item.id+"").val();
    contPCact++;
  });
var abloquePuntosColiniales = '[{"tipo": "GRD","campos": "f01_Nro|f01_Pto_Colineal","titulos": "Nro|Punto Colineal","impresiones": "undefined|undefined|"}';
for (var i = 0; i<arrDibujoPC.length;i++) {
  var agrillaPuntosColiniales = '  {"f01_Nro":"'+  arrarPCactual[i].var_numero+'", "f01_Pto_Colineal":"'+  arrarPCactual[i].var_puntocol+'" }';
  abloquePuntosColiniales = (abloquePuntosColiniales.concat(',').concat(agrillaPuntosColiniales));
}
abloquePuntosColiniales = abloquePuntosColiniales.concat(']');
var PTR_DOT_S = abloquePuntosColiniales;

 //-----actualizar usos suelo----------
 var contUSact=0;
    arrDibujoUSact.forEach(function (item, index, array) 
    {
      aarrCodigoUso[contUSact].adescripU = $("#desccripcionUS"+item.id+"").val();
      aarrCodigoUso[contUSact].atradicionalU = document.getElementById("tradicionUS"+item.id+"").checked;
      aarrCodigoUso[contUSact].aactualU = document.getElementById("actuallUS"+item.id+"").checked;
      contUSact++;
    });
    var abloqueUso = '[ {"tipo": "GRD","campos": "f01_emision_G_SERV_DESC|f01_emision_G_TRAD|f01_emision_G_ACT","titulos": "Descripción|Tradicional|Actual","impresiones": "undefined|undefined|undefined|" }';
     for (var i = 0; i<arrDibujoUSact.length;i++)
    {
    var agrillaUsoSuelo = '{"f01_emision_G_ACT": "'+ cambiarDatosBool(aarrCodigoUso[i].aactualU)+'","f01_emision_G_TRAD": "'+cambiarDatosBool(aarrCodigoUso[i].atradicionalU)+'","f01_emision_G_SERV_DESC": "'+aarrCodigoUso[i].adescripU+'","f01_emision_G_SERV_DESC_valor": "'+aarrCodigoUso[i].adescripU+'"}';
    abloqueUso = (abloqueUso.concat(',').concat(agrillaUsoSuelo));
    }
    abloqueUso = abloqueUso.concat(']');
    var PTR_USO = abloqueUso; 

  //-----actualizar servicio--------------------
   var contSerAct=0;
    arrDibujoS.forEach(function (item, index, array) 
    {
      aarrCodigoSer[contSerAct].adescServicio = $("#descripcionS"+item.id+"").val();
      aarrCodigoSer[contSerAct].aproveedorServicio = $("#proveedorS"+item.id+"").val();
      contSerAct++;
    });
    var abloqueServicio = '[{"tipo": "GRD","campos": "f01_emision_G_SERV_DESC|f01_emision_G_SERV_PROV","titulos": "Descripción|Proveedor","impresiones": "undefined|undefined|"}';
     for (var i = 0; i<arrDibujoS.length;i++)
    {
    var agrillaServicio = '{"f01_emision_G_SERV_DESC": "'+aarrCodigoSer[i].adescServicio+'","f01_emision_G_SERV_PROV": "'+aarrCodigoSer[i].aproveedorServicio+'","f01_emision_G_SERV_DESC_valor": "'+aarrCodigoSer[i].adescServicio+'","f01_emision_G_SERV_PROV_valor": "'+aarrCodigoSer[i].aproveedorServicio+'"}';
    abloqueServicio = (abloqueServicio.concat(',').concat(agrillaServicio));
    }
    abloqueServicio = abloqueServicio.concat(']');
    var PTR_SERV = abloqueServicio;

    //---------actualizar Rejas------------
    var contRejasAct=0;
    arrRE.forEach(function (item, index, array) 
    {
      aarrCodigoRejas[contRejasAct].adescripcionR = $("#descripcionRE"+item.id+"").val();
      aarrCodigoRejas[contRejasAct].ahierroR = document.getElementById("hierroRE"+item.id+"").checked;
      aarrCodigoRejas[contRejasAct].atuboR = document.getElementById("tuboRE"+item.id+"").checked;
      aarrCodigoRejas[contRejasAct].amaderaR = document.getElementById("maderaRE"+item.id+"").checked;
      aarrCodigoRejas[contRejasAct].aotrosR = document.getElementById("otrosRE"+item.id+"").checked;
      aarrCodigoRejas[contRejasAct].aotrosDescR = $("#otrosDRE"+item.id+"").val();
      aarrCodigoRejas[contRejasAct].aintegridadR = $("#integridadRE"+item.id+"").val();
      contRejasAct++;
    });
     var abloqueRejas = '[{"tipo": "GRD","campos": "f01_reja_DES_|f01_reja_HIERRO_|f01_reja_TUBO_|f01_reja_MAD_|f01_reja_OTRO_|f01_reja_OTRODESC_|f01_reja_INT_","titulos": "Descripción|Hierro Forjado|Tubo Industrial|Madera|Otros|Otrosdesc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
    for (var i = 0; i<arrRE.length;i++)
    {
      var agrillaRejas = '{"f01_reja_DES_": "'+aarrCodigoRejas[i].adescripcionR+'","f01_reja_INT_": "'+aarrCodigoRejas[i].aintegridadR+'","f01_reja_MAD_": "'+cambiarDatosBool(aarrCodigoRejas[i].amaderaR)+'","f01_reja_OTRO_": "'+cambiarDatosBool(aarrCodigoRejas[i].aotrosR)+'","f01_reja_TUBO_": "'+ cambiarDatosBool(aarrCodigoRejas[i].atuboR)+'","f01_reja_HIERRO_": "'+cambiarDatosBool(aarrCodigoRejas[i].ahierroR)+'","f01_reja_OTRODESC_": "'+aarrCodigoRejas[i].aotrosDescR+'","f01_reja_DES__valor": "'+aarrCodigoRejas[i].adescripcionR+'","f01_reja_INT__valor": "'+aarrCodigoRejas[i].aintegridadR+'"}';
      abloqueRejas = (abloqueRejas.concat(',').concat(agrillaRejas));
    }
    abloqueRejas = abloqueRejas.concat(']');
    var PTR_REJAS = abloqueRejas;
    

   //----actualizar acabados muros--------
    var cont = 0;
      arrDibujoAM.forEach(function (item, index, array) 
      {
          aarrCodigoAcab[cont].adescripcionA = $("#descripcionAM"+item.id+"").val();
          aarrCodigoAcab[cont].acalArenaA = document.getElementById("calArenaAM"+item.id+"").checked;
          aarrCodigoAcab[cont].acementoA = document.getElementById("cementoAM"+item.id+"").checked;
          aarrCodigoAcab[cont].apiedraA = document.getElementById("piedraAM"+item.id+"").checked;
          aarrCodigoAcab[cont].amarmolA = document.getElementById("marmolAM"+item.id+"").checked;
          aarrCodigoAcab[cont].amaderaA = document.getElementById("maderamuroAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aazulejoA = document.getElementById("azulejoAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aladrilloA = document.getElementById("ladrilloAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aotroAcabA = document.getElementById("otroAcabAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aotroDescAcabA = $("#otrosDescAcab1"+item.id+"").val();
          aarrCodigoAcab[cont].aintegridadAcabA = $("#integridAM"+item.id+"").val();
        cont++;
      });

        var abloqueAcabMuros = '[{"tipo": "GRD","campos": "f01_muro_DES_|f01_muro_CAL_|f01_muro_CEM_|f01_muro_PIE_|f01_muro_MAR_|f01_muro_MAD_|f01_muro_AZU_|f01_muro_LAD_|f01_muro_OTRO_|f01_muro_OTRODESC_|f01_muro_INT_","titulos": "Descripción|Cal y Arena|Cemento y Arena|Piedra|Mármol|Madera|Azulejo|Ladrillo|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
        for (var i = 0; i<arrDibujoAM.length;i++)
        {
          var agrillaAcabMuros = '{"f01_muro_AZU_": "'+cambiarDatosBool(aarrCodigoAcab[i].aazulejoA)+'","f01_muro_CAL_": "'+cambiarDatosBool(aarrCodigoAcab[i].acalArenaA)+'","f01_muro_CEM_": "'+cambiarDatosBool(aarrCodigoAcab[i].acementoA)+'","f01_muro_DES_": "'+aarrCodigoAcab[i].adescripcionA+'","f01_muro_INT_": "'+aarrCodigoAcab[i].aintegridadAcabA+'","f01_muro_LAD_": "'+cambiarDatosBool(aarrCodigoAcab[i].aladrilloA)+'","f01_muro_MAD_": "'+cambiarDatosBool(aarrCodigoAcab[i].amaderaA)+'","f01_muro_MAR_": "'+cambiarDatosBool(aarrCodigoAcab[i].amarmolA)+'","f01_muro_PIE_": "'+cambiarDatosBool(aarrCodigoAcab[i].apiedraA)+'","f01_muro_OTRO_": "'+cambiarDatosBool(aarrCodigoAcab[i].aotroAcabA)+'","f01_muro_OTRODESC_": "'+aarrCodigoAcab[i].aotroDescAcabA+'","f01_muro_DES__valor": "'+aarrCodigoAcab[i].adescripcionA+'","f01_muro_INT__valor": "'+aarrCodigoAcab[i].aintegridadAcabA+'"}';
          abloqueAcabMuros = (abloqueAcabMuros.concat(',').concat(agrillaAcabMuros));
        }
        abloqueAcabMuros = abloqueAcabMuros.concat(']');

      var PTR_ACAMUR = abloqueAcabMuros;
     console.log('acabados muros',PTR_ACAMUR);

     //-----actualizar grilla pintura------
      var contPact = 0;
      arrDibujoPIN.forEach(function (item, index, array) 
      {
        aarrCodigoPinturas[contPact].adescripcionPintu = $("#descripcionPIN"+item.id+"").val();
        aarrCodigoPinturas[contPact].acalArenaPintu = document.getElementById("calArenaPIN"+item.id+"").checked;
        aarrCodigoPinturas[contPact].aacrilicoPintu = document.getElementById("acrilicoPIN"+item.id+"").checked;
        aarrCodigoPinturas[contPact].aaceitePintu = document.getElementById("aceitePIN"+item.id+"").checked;
        aarrCodigoPinturas[contPact].aotroPintu = document.getElementById("otroPIN"+item.id+"").checked;
        aarrCodigoPinturas[contPact].aotrosDescPintu = $("#otrosDescPIN"+item.id+"").val();
        aarrCodigoPinturas[contPact].aintegridPintu = $("#integridPIN"+item.id+"").val();
        contPact++;
      });
       var abloquePinturas = '[{"tipo": "GRD","campos": "f01_pint_COD_|f01_pint_CAL_|f01_pint_ACR_|f01_pint_ACE_|f01_pint_OTRO_|f01_pint_OTRODESC_|f01_pint_INT_","titulos": "Descripción|Cal|Acrílica|Aceite|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
        for (var i = 0; i<arrDibujoPIN.length;i++)
        {
          var agrillaPinturas = '{"f01_pint_ACE_": "'+cambiarDatosBool(aarrCodigoPinturas[i].aaceitePintu)+'","f01_pint_ACR_": "'+cambiarDatosBool(aarrCodigoPinturas[i].aacrilicoPintu)+'","f01_pint_CAL_": "'+cambiarDatosBool(aarrCodigoPinturas[i].acalArenaPintu)+'","f01_pint_COD_": "'+aarrCodigoPinturas[i].adescripcionPintu+'","f01_pint_INT_": "'+aarrCodigoPinturas[i].aintegridPintu+'","f01_pint_OTRO_": "'+cambiarDatosBool(aarrCodigoPinturas[i].aotroPintu)+'","f01_pint_OTRODESC_": "'+aarrCodigoPinturas[i].aotrosDescPintu+'","f01_pint_COD__valor": "'+aarrCodigoPinturas[i].adescripcionPintu+'","f01_pint_INT__valor": "'+aarrCodigoPinturas[i].aintegridPintu+'"}';
          abloquePinturas = (abloquePinturas.concat(',').concat(agrillaPinturas));
        }
        abloquePinturas = abloquePinturas.concat(']');
       var PTR_PINTURA = abloquePinturas;

     //---------------actualizar pisos------------------
    var contPactual = 0;
    arrDibujoP.forEach(function (item, index, array){
    aarrCodigoPiso[contPactual].adescripcionPisoA = $("#descripcionPi"+item.id+"").val();     
    aarrCodigoPiso[contPactual].abaldosasPisoA = document.getElementById("baldosasPi"+item.id+"").checked; 
    aarrCodigoPiso[contPactual].amosaicosPisoA = document.getElementById("mosaicosPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].aporcelanatoPisoA = document.getElementById("porcelanatoPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].aplajaPisoA = document.getElementById("plajaPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].aprioPisoA = document.getElementById("prioPi"+item.id+"").checked; 
    aarrCodigoPiso[contPactual].aladrillosPisoA = document.getElementById("ladrillosPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].agramaPisoA = document.getElementById("gramaPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].atierraPisoA = document.getElementById("tierraPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].aconcretoPisoA = document.getElementById("concretoPi"+item.id+"").checked; 
    aarrCodigoPiso[contPactual].aceramicaPisoA = document.getElementById("ceramicaPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].amaderaPisoA = document.getElementById("maderaPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].aotrosPisoA = document.getElementById("otrosPi"+item.id+"").checked;
    aarrCodigoPiso[contPactual].aotroDescPisoA = $("#otrosDescPi"+item.id+"").val();
    aarrCodigoPiso[contPactual].aintegridadPisoA = $("#integridPi"+item.id+"").val();
      contPactual++;
    });
  var abloquePisos = '[{"tipo": "GRD","campos": "f01_pis_DES_|f01_pis_BAL_|f01_pis_MOS_|f01_pis_POR_|f01_pis_LAJ_|f01_pis_RIO_|f01_pis_LAD_|f01_pis_GRA_|f01_pis_TIE_|f01_pis_CON_|f01_pis_CER_|f01_pis_MAD_|f01_pis_OTRO_|f01_pis_OTRODESC_|f01_pis_INT_", "titulos": "Descripción|Baldosas|Mosaicos|Porcelanato|P.Laja|P.Rio|Ladrillos|Grama|Tierra|Concreto|Cerámica|Madera|Otros|Otros desc.|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';

  for (var i = 0; i<arrDibujoP.length;i++) {
    var agrillaPisos = '{ "f01_pis_BAL_":"'+ cambiarDatosBool(aarrCodigoPiso[i].abaldosasPisoA)+'","f01_pis_CER_":"'+ cambiarDatosBool(aarrCodigoPiso[i].aceramicaPisoA)+'", "f01_pis_CON_":"'+ cambiarDatosBool(aarrCodigoPiso[i].aconcretoPisoA)+'","f01_pis_DES_":"'+ aarrCodigoPiso[i].adescripcionPisoA+'","f01_pis_GRA_":"'+ cambiarDatosBool(aarrCodigoPiso[i].agramaPisoA)+'","f01_pis_INT_":"'+ aarrCodigoPiso[i].aintegridadPisoA+'","f01_pis_LAD_":"'+ cambiarDatosBool(aarrCodigoPiso[i].aladrillosPisoA)+'","f01_pis_LAJ_":"'+ cambiarDatosBool(aarrCodigoPiso[i].aplajaPisoA)+'","f01_pis_MAD_":"'+ cambiarDatosBool(aarrCodigoPiso[i].amaderaPisoA)+'","f01_pis_MOS_":"'+cambiarDatosBool(aarrCodigoPiso[i].amosaicosPisoA)+'","f01_pis_POR_":"'+ cambiarDatosBool(aarrCodigoPiso[i].aporcelanatoPisoA)+'", "f01_pis_RIO_":"'+ cambiarDatosBool(aarrCodigoPiso[i].aprioPisoA)+'","f01_pis_TIE_":"'+ cambiarDatosBool(aarrCodigoPiso[i].atierraPisoA)+'","f01_pis_OTRO_":"'+ cambiarDatosBool(aarrCodigoPiso[i].aotrosPisoA)+'","f01_pis_OTRODESC_":"'+ aarrCodigoPiso[i].aotroDescPisoA+'","f01_pis_DES__valor":"0", "f01_pis_INT__valor":"0"}';
    abloquePisos = (abloquePisos.concat(',').concat(agrillaPisos));
  }
  abloquePisos = abloquePisos.concat(']');
  var PTR_PISOS = abloquePisos;
  //-------actualizar puertas de acceso---------

  var contPAactual = 0;
  arrDibujoPAx.forEach(function (item, index, array) {
    aarrCodigoPuertasAcceso[contPAactual].adescripcionPuertasAcceso = $("#descripcionPA"+item.id+"").val();     
      aarrCodigoPuertasAcceso[contPAactual].amaderaPuertaAcceso = document.getElementById("maderaPA"+item.id+"").checked; 
      aarrCodigoPuertasAcceso[contPAactual].ametalPuertaAcceso = document.getElementById("metalPA"+item.id+"").checked;
      aarrCodigoPuertasAcceso[contPAactual].aotrosPuertaAcceso = document.getElementById("otrosPA"+item.id+"").checked;
      aarrCodigoPuertasAcceso[contPAactual].aotroDescPuertasAccesoA = $("#otrosDescPA"+item.id+"").val();
      aarrCodigoPuertasAcceso[contPAactual].aintegridadPuertasAccesoA = $("#integridPA"+item.id+"").val();
    contPAactual++;
  });
  var abloquePuertasAcceso = '[{ "tipo": "GRD", "campos": "f01_pue_DES_|f01_pue_MAD_|f01_pue_MET_|f01_pue_OTRO_|f01_pue_OTRODESC_|f01_pue_INT_", "titulos": "Descripción|Madera|Metal|Otros|Otros desc.|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|"}';

  for (var i = 0; i<arrDibujoPAx.length;i++) {
    var agrillaPuertasAcceso = '{"f01_pue_DES_": "'+ aarrCodigoPuertasAcceso[i].adescripcionPuertasAcceso+'","f01_pue_INT_": "'+ aarrCodigoPuertasAcceso[i].aintegridadPuertasAccesoA+'","f01_pue_MAD_": "'+ cambiarBoolAString(aarrCodigoPuertasAcceso[i].amaderaPuertaAcceso)+'","f01_pue_MET_": "'+ cambiarBoolAString(aarrCodigoPuertasAcceso[i].ametalPuertaAcceso)+'","f01_pue_OTRO_": "'+ cambiarBoolAString(aarrCodigoPuertasAcceso[i].aotrosPuertaAcceso)+'","f01_pue_OTRODESC_": "'+ aarrCodigoPuertasAcceso[i].aotroDescPuertasAccesoA+'","f01_pue_DES__valor":"'+ aarrCodigoPuertasAcceso[i].adescripcionPuertasAcceso+'","f01_pue_INT__valor":"'+ aarrCodigoPuertasAcceso[i].aintegridadPuertasAccesoA+'"}'; 

    abloquePuertasAcceso = (abloquePuertasAcceso.concat(',').concat(agrillaPuertasAcceso));
  }
  abloquePuertasAcceso = abloquePuertasAcceso.concat(']');
  var PTR_PUERTA = abloquePuertasAcceso;

//-----actualizar mobiliario urbano------------
  var contMUactual = 0;
  arrDibujoMUr.forEach(function (item, index, array) {
  aarrMobiliarioUrbano[contMUactual].adescripcionMovilidadUrbano = $("#descMUr"+item.id+"").val();     
  aarrMobiliarioUrbano[contMUactual].amaderaMovilidadUrbano = document.getElementById("maderMUr"+item.id+"").checked; 
  aarrMobiliarioUrbano[contMUactual].ametalMovilidadUrbano = document.getElementById("metalMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].abarroMovilidadUrbano = document.getElementById("barroMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].acalMovilidadUrbano = document.getElementById("calMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].aladrilloMovilidadUrbano = document.getElementById("ladrilloMUr"+item.id+"").checked; 
  aarrMobiliarioUrbano[contMUactual].apiedraMovilidadUrbano = document.getElementById("piedraMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].aadobeMovilidadUrbano = document.getElementById("adobeMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].aconcretoMovilidadUrbano = document.getElementById("concretoMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].afcarbonMovilidadUrbano = document.getElementById("fcarbonMUr"+item.id+"").checked; 
  aarrMobiliarioUrbano[contMUactual].avidrioMovilidadUrbano = document.getElementById("vidrioMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].afibrocemMovilidadUrbano = document.getElementById("fibrocemMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].aotrosMovilidadUrbano = document.getElementById("otrosMUr"+item.id+"").checked;
  aarrMobiliarioUrbano[contMUactual].aotroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDesOUr"+item.id+"").val();
  aarrMobiliarioUrbano[contMUactual].aintegridadMovilidadUrbanoMovilidadUrbano = $("#integridMUr"+item.id+"").val();
  contMUactual++;
});
var abloqueMovilidadUrbano = '[ {"tipo": "GRD","campos": "f01_mob_DES_|f01_mob_MAD_|f01_mob_MET_|f01_mob_BARR_|f01_mob_CAL_|f01_mob_LAD_|f01_mob_PIE_|f01_mob_ADO_|f01_mob_CON_|f01_mob_CAR_|f01_mob_VID_|f01_mob_FIBR_|f01_mob_OTRO_|f01_mob_OTRODESC_|f01_mob_INT_","titulos": "Descripción|Madera|Metal|Barro|Cal|Ladrillo|Piedra|Adobe|Concreto|F. Carbono|Vidrio|Fibrocem|Otros|Otros desc.|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
for (var i = 0; i<arrDibujoMUr.length;i++) {
  var agrillaMovilidadUrbano = ' { "f01_mob_ADO_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].aadobeMovilidadUrbano) +'", "f01_mob_CAL_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].acalMovilidadUrbano) +'", "f01_mob_CAR_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].afcarbonMovilidadUrbano) +'", "f01_mob_CON_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].aconcretoMovilidadUrbano) +'", "f01_mob_DES_": "'+ aarrMobiliarioUrbano[i].adescripcionMovilidadUrbano +'", "f01_mob_INT_": "'+ aarrMobiliarioUrbano[i].aintegridadMovilidadUrbanoMovilidadUrbano +'", "f01_mob_LAD_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].aladrilloMovilidadUrbano) +'", "f01_mob_MAD_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].amaderaMovilidadUrbano) +'", "f01_mob_MET_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].ametalMovilidadUrbano) +'", "f01_mob_PIE_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].apiedraMovilidadUrbano) +'", "f01_mob_VID_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].avidrioMovilidadUrbano) +'", "f01_mob_BARR_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].abarroMovilidadUrbano) +'", "f01_mob_FIBR_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].afibrocemMovilidadUrbano) +'", "f01_mob_OTRO_": "'+ cambiarDatosBool(aarrMobiliarioUrbano[i].aotrosMovilidadUrbano) +'", "f01_mob_OTRODESC_": "'+ aarrMobiliarioUrbano[i].aotroDescMovilidadUrbanoMovilidadUrbano +'", "f01_mob_DES__valor":"'+ aarrMobiliarioUrbano[i].adescripcionMovilidadUrbano +'", "f01_mob_INT__valor":"'+ aarrMobiliarioUrbano[i].aintegridadMovilidadUrbanoMovilidadUrbano +'" }';
  abloqueMovilidadUrbano = (abloqueMovilidadUrbano.concat(',').concat(agrillaMovilidadUrbano));
}
abloqueMovilidadUrbano = abloqueMovilidadUrbano.concat(']');
var PTR_MOBURB = abloqueMovilidadUrbano;

  //----------actualizar escaleras---------------

  var contEactual = 0;
  arrDibujoEsca.forEach(function (item, index, array) {
  aarrCodigoEscaleras[contEactual].adescripcionEscaleras = $("#descEsca"+item.id+"").val();     
  aarrCodigoEscaleras[contEactual].amaderaEscaleras = document.getElementById("maderaEsca"+item.id+"").checked; 
  aarrCodigoEscaleras[contEactual].ametalEscaleras = document.getElementById("metalEsca"+item.id+"").checked;
  aarrCodigoEscaleras[contEactual].aConcretoEscaleras = document.getElementById("ConcretoEsca"+item.id+"").checked;
  aarrCodigoEscaleras[contEactual].aPiedraEscaleras = document.getElementById("PiedraEsca"+item.id+"").checked;
  aarrCodigoEscaleras[contEactual].aladrilloEscaleras = document.getElementById("ladrilloEsca"+item.id+"").checked; 
  aarrCodigoEscaleras[contEactual].atierraEscaleras = document.getElementById("tierraEsca"+item.id+"").checked;
  aarrCodigoEscaleras[contEactual].aotrosEscaleras = document.getElementById("otrosEsca"+item.id+"").checked;
  aarrCodigoEscaleras[contEactual].aotroDescEscaleras = $("#otrosDescEsca"+item.id+"").val();
  aarrCodigoEscaleras[contEactual].aintegridadEscaleras = $("#integridEsca"+item.id+"").val();
  contEactual++;
});
var abloqueEscaleras = '[ { "tipo": "GRD", "campos": "f01_esc_DES_|f01_esc_MAD_|f01_esc_MET_|f01_esc_CON_|f01_esc_PIE_|f01_esc_LAD_|f01_esc_TIE_|f01_esc_OTRO_|f01_esc_OTRODESC_|f01_esc_INT_", "titulos": "Descripción|Madera|Metal|Concreto|Piedra|Ladrillo|Tierra|Otro|Otro desc|Integridad", "impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|"}';
for (var i = 0; i<arrDibujoEsca.length;i++) {
  var agrillaEscaleras = '{ "f01_esc_CON_":"'+ cambiarDatosBool(aarrCodigoEscaleras[i].aConcretoEscaleras)+'", "f01_esc_DES_":"'+ aarrCodigoEscaleras[i].adescripcionEscaleras+'", "f01_esc_INT_":"'+ aarrCodigoEscaleras[i].aintegridadEscaleras+'", "f01_esc_LAD_":"'+ cambiarDatosBool(aarrCodigoEscaleras[i].aladrilloEscaleras)+'", "f01_esc_MAD_":"'+ cambiarDatosBool(aarrCodigoEscaleras[i].amaderaEscaleras)+'", "f01_esc_MET_":"'+ cambiarDatosBool(aarrCodigoEscaleras[i].ametalEscaleras)+'", "f01_esc_PIE_":"'+ cambiarDatosBool(aarrCodigoEscaleras[i].aPiedraEscaleras)+'", "f01_esc_TIE_":"'+ cambiarDatosBool(aarrCodigoEscaleras[i].atierraEscaleras)+'", "f01_esc_OTRO_":"'+ cambiarDatosBool(aarrCodigoEscaleras[i].aotrosEscaleras)+'", "f01_esc_OTRODESC_":"'+ aarrCodigoEscaleras[i].aotroDescEscaleras+'", "f01_esc_DES__valor":"'+ aarrCodigoEscaleras[i].adescripcionEscaleras+'", "f01_esc_INT__valor":"'+ aarrCodigoEscaleras[i].aintegridadEscaleras+'"}';
  abloqueEscaleras = (abloqueEscaleras.concat(',').concat(agrillaEscaleras));
}
abloqueEscaleras = abloqueEscaleras.concat(']');
var PTR_ESCAL = abloqueEscaleras;

//-----actualizar características de la vegetación-------

var contVactualizar = 0;
  arrDibujoCVeg.forEach(function (item, index, array) {
  aarrCodigoCarVegetacion[contVactualizar].adescripcionCarVegetacion = $("#descCVeg"+item.id+"").val();     
  aarrCodigoCarVegetacion[contVactualizar].acantidadCarVegetacion = $("#cantidadCVeg"+item.id+"").val();
  aarrCodigoCarVegetacion[contVactualizar].asinFloracionCarVegetacion = document.getElementById("sinFlorCVeg"+item.id+"").checked;
  aarrCodigoCarVegetacion[contVactualizar].acaducaCarVegetacion = document.getElementById("caducaCVeg"+item.id+"").checked; 
  aarrCodigoCarVegetacion[contVactualizar].aperenneCarVegetacion = document.getElementById("perenneCVeg"+item.id+"").checked;
  aarrCodigoCarVegetacion[contVactualizar].aotrosCarVegetacion = document.getElementById("otrosCVeg"+item.id+"").checked;
  aarrCodigoCarVegetacion[contVactualizar].aotroDescCarVegetacion = $("#otrosDescCVeg"+item.id+"").val();
  aarrCodigoCarVegetacion[contVactualizar].aintegridadCarVegetacion = $("#integridCVeg"+item.id+"").val();
  contVactualizar++;
});
var abloqueCarVegetacion = '[{"tipo": "GRD","campos": "f01_veg_DES_|f01_veg_CAN_|f01_veg_SFL_|f01_veg_CAD_|f01_veg_PER_|f01_veg_OTRO_|f01_veg_OTRODESC_|f01_veg_INT_","titulos": "Descripción|Cantidad|Sin Floracion|Caduca|Perenne|Otro|Otro desc|Integridad","impresiones": "undefined|undefined|undefined|undefined|undefined|undefined|undefined|undefined|" }';
for (var i = 0; i<arrDibujoCVeg.length;i++) {
  var agrillaCarVegetacion = ' {"f01_veg_CAD_":"'+ cambiarDatosBool(aarrCodigoCarVegetacion[i].acaducaCarVegetacion)+'","f01_veg_CAN_":"'+ aarrCodigoCarVegetacion[i].acantidadCarVegetacion+'","f01_veg_DES_":"'+ aarrCodigoCarVegetacion[i].adescripcionCarVegetacion+'","f01_veg_INT_":"'+ aarrCodigoCarVegetacion[i].aintegridadCarVegetacion+'","f01_veg_PER_":"'+ cambiarDatosBool(aarrCodigoCarVegetacion[i].aperenneCarVegetacion)+'","f01_veg_SFL_":"'+ cambiarDatosBool(aarrCodigoCarVegetacion[i].asinFloracionCarVegetacion)+'","f01_veg_OTRO_":"'+ cambiarDatosBool(aarrCodigoCarVegetacion[i].aotrosCarVegetacion)+'","f01_veg_OTRODESC_":"'+ aarrCodigoCarVegetacion[i].aotroDescCarVegetacion+'","f01_veg_DES__valor":"'+ aarrCodigoCarVegetacion[i].adescripcionCarVegetacion+'", "f01_veg_INT__valor":"'+ aarrCodigoCarVegetacion[i].aintegridadCarVegetacion+'"}';
  abloqueCarVegetacion = (abloqueCarVegetacion.concat(',').concat(agrillaCarVegetacion));
}
abloqueCarVegetacion = abloqueCarVegetacion.concat(']');
var PTR_VEGET = abloqueCarVegetacion;

//----grilla detalle artistico------------
var contDArt = 0;
  arrDibujoDArt.forEach(function (item, index, array) 
  {
    aarrCodigoDetaArtistoco[contDArt].adescripcionDetalleArtistoco = $("#descripDA"+item.id+"").val();     
    aarrCodigoDetaArtistoco[contDArt].aintegridadDetaArtistoco = $("#integridadDA"+item.id+"").val();
    contDArt++;
    
  });
var abloqueDetaArtistoco = '[{"tipo": "GRD","campos": "f01_det_DES_|f01_det_INT_","titulos": "Descripción|Integridad", "impresiones": "undefined|undefined|"}';
for (var i = 0; i<arrDibujoDArt.length;i++) {
var agrillaDetaArt = ' { "f01_det_DES_": "'+aarrCodigoDetaArtistoco[i].adescripcionDetalleArtistoco+'", "f01_det_INT_": "'+aarrCodigoDetaArtistoco[i].aintegridadDetaArtistoco+'", "f01_det_DES__valor": "'+aarrCodigoDetaArtistoco[i].adescripcionDetalleArtistoco+'", "f01_det_INT__valor": "'+aarrCodigoDetaArtistoco[i].aintegridadDetaArtistoco+'"}';
  abloqueDetaArtistoco = (abloqueDetaArtistoco.concat(',').concat(agrillaDetaArt));
}
abloqueDetaArtistoco = abloqueDetaArtistoco.concat(']');
var PTR_DETALLES = abloqueDetaArtistoco;

/*-----------------------------------------------*/
var g_tipo = 'PTR-ESP';
var data = '{"g_tipo":"'+g_tipo+'","PTR_CREA":"'+PTR_CREA+'","PTR_ACTUALIZA":"'+PTR_ACTUALIZA+'","PTR_COD_CAT":"'+PTR_COD_CAT+'","PTR_GEOREF":"'+PTR_GEOREF+'","PTR_TRAD":"'+PTR_TRAD+'","PTR_ACT":"'+PTR_ACT+'","PTR_DEP":"'+PTR_DEP+'","PTR_CIPO":"'+PTR_CIPO+'","PTR_MUNI":"'+PTR_MUNI+'","PTR_MACRO":"'+PTR_MACRO+'","PTR_BARR":"'+PTR_BARR+'","PTR_DIR":"'+PTR_DIR+'","PTR_ESQ1":"'+PTR_ESQ1+'","PTR_ESQ2":"'+PTR_ESQ2+'","PTR_ALT":"'+PTR_ALT+'","PTR_NOMPRO":"'+PTR_NOMPRO+'","PTR_REPRO":"'+PTR_REPRO+'","PTR_USO":'+PTR_USO+',"PTR_SERV":'+PTR_SERV+',"PTR_FIGPRO":"'+PTR_FIGPRO+'","PTR_OTHER":'+ptr_other_check+',"PTR_REFHIST":"'+PTR_REFHIST+'","PTR_FECCONS":"'+PTR_FECCONS+'","PTR_AUTOR":"'+PTR_AUTOR+'","PTR_PREEX":"'+PTR_PREEX+'","PTR_PROP":"'+PTR_PROP+'","PTR_USOORG":"'+PTR_USOORG+'","PTR_HECHIS":"'+PTR_HECHIS+'","PTR_FIEREL":"'+PTR_FIEREL+'","PTR_DATHISC":"'+PTR_DATHISC+'","PTR_FUENTE":"'+PTR_FUENTE+'","PTR_FECCONST":"'+PTR_FECCONST+'","PTR_AUTCONST":"'+PTR_AUTCONST+'","PTR_PREDIN":"'+PTR_PREDIN+'","PTR_PROPIE":"'+PTR_PROPIE+'","PTR_USOPREEX":"'+PTR_USOPREEX+'","PTR_HECHISD":"'+PTR_HECHISD+'","PTR_FIERELD":"'+PTR_FIERELD+'","PTR_DATHISTC":"'+PTR_DATHISTC+'","PTR_FUENTBIO":"'+PTR_FUENTBIO+'","PTR_INSCR":"'+PTR_INSCR+'","PTR_EPOCA":"'+PTR_EPOCA+'","PTR_ACONS":"'+PTR_ACONS+'","PTR_AULTR":"'+PTR_AULTR+'","PTR_ESTILO":"'+PTR_ESTILO+'","PTR_UBICESP":"'+PTR_UBICESP+'","PTR_LINCONS":"'+PTR_LINCONS+'","PTR_TIPOLO":"'+PTR_TIPOLO+'","PTR_MURMAT":'+ptr_murmat_check+',"PTR_MURMAT_OTROS":"'+PTR_MURMAT_OTROS+'","PTR_MUREST":"'+PTR_MUREST+'","PTR_MURINTE":"'+PTR_MURINTE+'","PTR_REJAS":'+PTR_REJAS+',"PTR_REJAEST":"'+PTR_REJAEST+'","PTR_ACAMUR":'+PTR_ACAMUR+',"PTR_ACAEST":"'+PTR_ACAEST+'","PTR_PINTURA":'+PTR_PINTURA+',"PTR_PINTEST":"'+PTR_PINTEST+'","PTR_PISOS":'+PTR_PISOS+',"PTR_PISOEST":"'+PTR_PISOEST+'","PTR_PUERTA":'+PTR_PUERTA+',"PTR_PUERTAEST":"'+PTR_PUERTAEST+'","PTR_MOBURB":'+PTR_MOBURB+',"PTR_MOBEST":"'+PTR_MOBEST+'","PTR_ESCAL":'+PTR_ESCAL+',"PTR_ESCEST":"'+PTR_ESCEST+'","PTR_VEGET":'+PTR_VEGET+',"PTR_VEGEST":"'+PTR_VEGEST+'","PTR_DETALLES":'+PTR_DETALLES+',"PTR_DETALLEST":"'+PTR_DETALLEST+'","PTR_ESQDES":"'+PTR_ESQDES+'","PTR_ANGOR":"'+PTR_ANGOR+'","PTR_ANGGRA":"'+PTR_ANGGRA+'","PTR_VISTAS":'+ptr_vistas_check+',"PTR_ANGOTR":"'+PTR_ANGOTR+'","PTR_ELEMVISID":"'+PTR_ELEMVISID+'","PTR_ELEMVISAL":"'+PTR_ELEMVISAL+'","PTR_ELEMVISAB":"'+PTR_ELEMVISAB+'","PTR_DESENTEI":"'+PTR_DESENTEI+'","PTR_DESESPDES":"'+PTR_DESESPDES+'","PTR_MEDIDAREA":"'+PTR_MEDIDAREA+'","PTR_MEDIDANC":"'+PTR_MEDIDANC+'","PTR_MEDIDLAR":"'+PTR_MEDIDLAR+'","PTR_MEDIAMC":"'+PTR_MEDIAMC+'","PTR_INFADICIONAL":"'+PTR_INFADICIONAL+'","PTR_INFOBS":"'+PTR_INFOBS+'","PTR_PATODAN":'+ptr_patodan_check+',"PTR_PATO_OTROS":"'+PTR_PATO_OTROS+'","PTR_PATOOBS":"'+PTR_PATOOBS+'","PTR_PATOCAU":'+ptr_patocau_check+',"PTR_PATOCAU_OTROS":"'+PTR_PATOCAU_OTROS+'","PTR_VALORHIS":'+ptr_valorhis_check+',"PTR_VALORART":'+ptr_valorart_check+',"PTR_VALORARQ":'+ptr_valorarq_check+',"PTR_VALORTEC":'+ptr_valortec_check+',"PTR_VALORINTG":'+ptr_valorintg_check+',"PTR_VALORURB":'+ptr_valorurb_check+',"PTR_VALORINT":'+ptr_valorint_check+',"PTR_VALORSIMB":'+ptr_valorsimb_check+',"PTR_CRITERHIS":"'+PTR_CRITERHIS+'","PTR_CRITERART":"'+PTR_CRITERART+'","PTR_CRITERARQ":"'+PTR_CRITERARQ+'","PTR_CRITERTEC":"'+PTR_CRITERTEC+'","PTR_CRITERINT":"'+PTR_CRITERINT+'","PTR_CRITERURB":"'+PTR_CRITERURB+'","PTR_CRITERINTA":"'+PTR_CRITERINTA+'","PTR_CRITERSIMB":"'+PTR_CRITERSIMB+'","PTR_PERINV":"'+PTR_PERINV+'","PTR_PERREV":"'+PTR_PERREV+'","PTR_PERDIG":"'+PTR_PERDIG+'","PTR_DOT_S":'+PTR_DOT_S+'}';
var dataesp = JSON.stringify(data);
  //console.log("dataPTR_ESP_ACTUALIZAR",dataesp);
  //var cas_nro_caso = 11 ;
  var cas_act_id = 137 ;
  var cas_usr_actual_id = 1155 ;
  //var cas_nombre_caso = 'PTR-ESP772018' ;
  var cas_estado_paso = 'RECIBIDO';
  var cas_nodo_id = 1; 
  var cas_usr_id = 1;
  var cas_ws_id = 1;  
  var cas_asunto = 'asunto';
  var cas_tipo_hr = 'hora';
  var cas_id_padre = 1;
  var campo_f = 'campo';

  var formData = {"identificador":"SERVICIO_PATRIMONIO-943","parametros":'{"xcas_id":'+cas_id+',"xcas_nro_caso":'+cas_nro_caso+',"xcas_act_id":'+cas_act_id+',"xcas_usr_actual_id":'+cas_usr_actual_id+',"xcas_datos":'+dataesp+',"xcas_nombre_caso":"'+cas_nombre_caso+'","xcas_estado_paso":"'+cas_estado_paso+'","xcas_nodo_id":'+cas_nodo_id+',"xcas_usr_id":'+cas_usr_id+',"xcas_ws_id":'+cas_ws_id+',"xcas_asunto":"'+cas_asunto+'","xcas_tipo_hr":"'+cas_tipo_hr+'","xcas_id_padre":'+cas_id_padre+',"xcampo_f":"'+campo_f+'"}'};
  console.log(formData,"formData");
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {  
     $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        //////////console.log("dataaaa",data);
        swal( "Exíto..!", "Se actualizo correctamente...!", "success" );
        buscarPatrimonioPorTipo(1);           
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
   },
   error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 
});



function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SERVICIO_PATRIMONIO-1023","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
       console.log(dataIN, 'USUARIO');
       var primerNombre = dataIN[0].vprs_nombres;
       var primerApellido = dataIN[0].vprs_paterno;
       var segundoApellido = dataIN[0].vprs_materno;
       var cedulaIdentidad = dataIN[0].vprs_ci;
       var razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
       
       $("#ccreador_p").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreadorM_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreadorS_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#cactualizador_in").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreador").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#cautorizador").val(primerNombre+' '+primerApellido+' '+segundoApellido);  

     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};


function listaMacrodistritoespacioabierto(macrodistrito_descripcion)
{ 
  $.ajax(
  {
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) 
    {         
      var formData = {"identificador": 'SISTEMA_VALLE-616',"parametros": '{}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) 
       {  
        var datos = dataIN;
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          document.getElementById("cmacrodistritoespabie").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+ '</option>';
        }
        for(var i = 0; i < datos.length; i++)
        { 
          var macrodistrito_id = datos[i].macrodistrito_id;
          var macrodistrito = JSON.parse(datos[i].macrodistrito_data).nombre;
          if (macrodistrito == macrodistrito_descripcion) 
          {       
            document.getElementById("amacrodistritoespabie").innerHTML += '<option value='+macrodistrito+' selected>'+macrodistrito+'</option>';
          }
          else
          {
            document.getElementById("amacrodistritoespabie").innerHTML += '<option value='+macrodistrito+'>' +macrodistrito+'</option>';
          } 
        }      
      },     
      error: function (xhr, status, error) { }
    });
    },
    error: function(result) {
      swal( "Error..!", "No se puede recuperar los datos", "error" );
    },
  });
};

/*-----------------END ESPACIOS ABIERTOS-----------------------*/

$(document).ready(function (){

 selectCampos();
 buscarPatrimonioPorTipo(1);
 cargarDatosUsuario();
 document.getElementById("cmacrodistritoespabie").length=1;
 document.getElementById("amacrodistritoespabie").length=0;
 listaMacrodistritoespacioabierto(macrodistrito_descripcion);

});

///--- inicio grilla  Puntos Colineales ---\\\
var contCodigoPuntosColineales = 2;
var arrCodigoPuntosColineales = [];
var arrDibujoPuntosColineales = [];

htmlPuntosColineales='<div class="col-md-12">'+
                    '<div class="form-group col-md-5">'+
                    '<input type="text" id="NroDescPuntosColineales1" class="form-control"  placeholder="Nro">'+
                    '</div>'+
                    '<div class="form-group col-md-6">'+
                    '<input type="text" id="PuntoColinealPuntosColineales1" class="form-control"  placeholder="Punto Colineal">'+
                    '</div>'+
                    '<div class="col-md-1">'+
                    '<a style="cursor:pointer;" type="button">'+
                    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuntosColineales(1);"></i>'+
                    '</a></div>'+
                    '</div>';

var cadenaPuntosColineales = '';  
arrCodigoPuntosColineales.push({NroDescPuntosColineales:'', PuntoColinealPuntosColineales: ''});
arrDibujoPuntosColineales.push({scd_codigo:htmlPuntosColineales, id: 1});
cadenaPuntosColineales=arrDibujoPuntosColineales[0].scd_codigo;

$('#crearGrillaPuntosColinealesado').html(cadenaPuntosColineales);
$("#NroDescPuntosColineales1").val(arrCodigoPuntosColineales[0].NroDescPuntosColineales); 
$("#PuntoColinealPuntosColineales1").val(arrCodigoPuntosColineales[0].PuntoColinealPuntosColineales);

function grillaPuntosColineales() {
  var htmlPuntosColineales;
  htmlPuntosColineales='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
    '<input type="text" id="NroDescPuntosColineales'+contCodigoPuntosColineales+'" class="form-control"  placeholder="Nro">'+
    '</div>'+
    '<div class="form-group col-md-6">'+
    '<input type="text" id="PuntoColinealPuntosColineales'+contCodigoPuntosColineales+'" class="form-control"  placeholder="Punto Colineal">'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuntosColineales('+contCodigoPuntosColineales+');"></i>'+
    '</a></div>'+
    '</div>';
  var cadenaPuntosColineales = '';
  var cont = 0;
  arrDibujoPuntosColineales.forEach(function (item, index, array){
    arrCodigoPuntosColineales[cont].NroDescPuntosColineales = $("#NroDescPuntosColineales"+item.id+"").val();
    arrCodigoPuntosColineales[cont].PuntoColinealPuntosColineales = $("#PuntoColinealPuntosColineales"+item.id+"").val();
    cont++;
  });
 arrCodigoPuntosColineales.push({NroDescPuntosColineales:'', PuntoColinealPuntosColineales: ''});
  arrDibujoPuntosColineales.push({scd_codigo:htmlPuntosColineales, id: contCodigoPuntosColineales});
  contCodigoPuntosColineales++;
  cont=1;
  for (var i=0; i<arrDibujoPuntosColineales.length;i++) {
    cadenaPuntosColineales = cadenaPuntosColineales + arrDibujoPuntosColineales[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaPuntosColinealesado').html(cadenaPuntosColineales); 
  var conto=0;
  arrDibujoPuntosColineales.forEach(function (item, index, array) { 
    $("#NroDescPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].NroDescPuntosColineales); 
    $("#PuntoColinealPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].PuntoColinealPuntosColineales); 
    conto=conto+1;
  });
}

function eliminarGrillaPuntosColineales($id){
  if(contCodigoPuntosColineales>2) {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaPuntosColineales ='';
      var pos=-1;
      var cont = 0;
      arrDibujoPuntosColineales.forEach(function (item, index, array) {
     arrCodigoPuntosColineales[cont].NroDescPuntosColineales = $("#NroDescPuntosColineales"+item.id+"").val();
      arrCodigoPuntosColineales[cont].PuntoColinealPuntosColineales = $("#PuntoColinealPuntosColineales"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoPuntosColineales.map(function(e) {return e.id;}).indexOf($id);
      arrCodigoPuntosColineales.splice(pos,1);
      arrDibujoPuntosColineales.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoPuntosColineales.length;i++) {
        cadenaPuntosColineales = cadenaPuntosColineales + arrDibujoPuntosColineales[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaPuntosColinealesado').html(cadenaPuntosColineales);
      var conto=0;
      arrDibujoPuntosColineales.forEach(function (item, index, array) { 
      $("#NroDescPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].NroDescPuntosColineales); 
    $("#PuntoColinealPuntosColineales"+item.id+"").val(arrCodigoPuntosColineales[conto].PuntoColinealPuntosColineales); 
        conto=conto+1;
      });
    });
  }
}

//----EDITAR Y ACTUALUIZAR PUNTOS COLINEALES-----

var arrDibujoPC=[];
var arrarPCactual = [];
var contPCact = 0;
function editarPuntosC(longitudGrilla){
  var contPC=1;
  arrDibujoPC=[];
  var htmlPC;
  for (var i=1; i<longitudGrilla; i++)
        {
        htmlPC='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
    '<input type="text" id="aNroDescPuntosColineales'+contPC+'" class="form-control"  placeholder="Nro">'+
    '</div>'+
    '<div class="form-group col-md-6">'+
    '<input type="text" id="aPuntoColinealPuntosColineales'+contPC+'" class="form-control"  placeholder="Punto Colineal">'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPuntosColineales('+contPC+');"></i>'+
    '</a></div>'+
    '</div>';
       arrDibujoPC.push({scd_codigo:htmlPC,id: contPC});
        contPC++;
        }
        var cadena='';
        var cont=1;
        for (var i=0; i<arrDibujoPC.length;i++)
        {
          cadena=cadena +arrDibujoPC[i].scd_codigo;
          cont++;
        }
        $('#editarGrillaPuntosColinealesado').html(cadena);
       }

function actualizarPuntosColineales() {
  var htmlPCact;
  htmlPCact='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
    '<input type="text" id="aNroDescPuntosColineales'+contPCact+'" class="form-control"  placeholder="Nro">'+
    '</div>'+
    '<div class="form-group col-md-6">'+
    '<input type="text" id="aPuntoColinealPuntosColineales'+contPCact+'" class="form-control"  placeholder="Punto Colineal">'+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPuntosColineales('+contPCact+');"></i>'+
    '</a></div>'+
    '</div>';
  var cadena = '';
  var cont = 0;
  arrDibujoPC.forEach(function (item, index, array){
    arrarPCactual[cont].var_numero = $("#aNroDescPuntosColineales"+item.id+"").val();
    arrarPCactual[cont].var_puntocol = $("#aPuntoColinealPuntosColineales"+item.id+"").val();
    cont++;
  });
 arrarPCactual.push({var_numero:'', var_puntocol: ''});
  arrDibujoPC.push({scd_codigo:htmlPCact, id: contPCact});
  contPCact++;
  cont=1;
  for (var i=0; i<arrDibujoPC.length;i++) {
    cadena = cadena + arrDibujoPC[i].scd_codigo;
    cont++;
  }
  $('#editarGrillaPuntosColinealesado').html(cadena); 
  var conto=0;
  arrDibujoPC.forEach(function (item, index, array) { 
    $("#aNroDescPuntosColineales"+item.id+"").val(arrarPCactual[conto].var_numero); 
    $("#aPuntoColinealPuntosColineales"+item.id+"").val(arrarPCactual[conto].var_puntocol); 
    conto=conto+1;
  });
}

function eliminarPuntosColineales($id){
  if(contPCact>2) {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadena ='';
      var pos=-1;
      var cont = 0;
      arrDibujoPC.forEach(function (item, index, array) {
     arrarPCactual[cont].var_numero = $("#aNroDescPuntosColineales"+item.id+"").val();
      arrarPCactual[cont].var_puntocol = $("#aPuntoColinealPuntosColineales"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoPC.map(function(e) {return e.id;}).indexOf($id);
      arrarPCactual.splice(pos,1);
      arrDibujoPC.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoPC.length;i++) {
        cadena = cadena + arrDibujoPC[i].scd_codigo;
        cont++;
      }
      $('#editarGrillaPuntosColinealesado').html(cadena);
      var conto=0;
      arrDibujoPC.forEach(function (item, index, array) { 
      $("#aNroDescPuntosColineales"+item.id+"").val(arrarPCactual[conto].var_numero); 
      $("#aPuntoColinealPuntosColineales"+item.id+"").val(arrarPCactual[conto].var_puntocol); 
        conto=conto+1;
      });
    });
  }
}



///--- fin grilla  Puntos Colineales ---\\\
//--------------------GRILLA USO SUELO--------------------//

    var contCodigoUso = 2;
    var arrCodigoUso = [];
    var arrDibujoUso = [];
  
    htmlUso='Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descUso1" name="tipoarg1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Recreación activa">Recreación activa</option>'+
              '<option value="Parque Infantil">Parque Infantil</option>'+
              '<option value="Parques Urbanos">Parques Urbanos</option>'+
              '<option value="Recreación">Recreación</option>'+
              '<option value="Deportivo">Deportivo</option>'+
              '<option value="Industria">Industria</option>'+
              '<option value="Artistico Cultural">Artistico Cultural</option>'+
              '<option value="Recreación Pasiva">Recreación Pasiva</option>'+ 
          '</select>'+
      '</div>'+
      '<div class="col-md-3">'+
          'Tradicional <input type="checkbox" id="tradicionalUso1" name="tradicionalUso1">'+
      '</div>'+
      '<div class="col-md-3">'+
          'Actual <input type="checkbox" id="actuallUso1">'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaUso(1);"></i>'+
      '</a></div></div>';
    var cadenaUso = '';  
    arrCodigoUso.push({descripU:'N',tradicionalU:'',actualU:''});
    arrDibujoUso.push({scd_codigo:htmlUso,id: 1});
    //////console.log("arrCodigo",arrCodigoUso);
    
    cadenaUso='<div class="form-group"><label class="col-md-2">1. '+arrDibujoUso[0].scd_codigo;
    //////console.log("cadenaaaaUso",cadenaUso);
    $('#crearGrillaUso').html(cadenaUso);
    
    $("#descUso1").val(arrCodigoUso[0].descripU); 
    document.getElementById("tradicionalUso1").checked = (arrCodigoUso[0].tradicionalU); 
    document.getElementById("actuallUso1").checked = (arrCodigoUso[0].actualU);

    function GrillaUsoSuelo(){
    var htmlUso;
        htmlUso='Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descUso'+contCodigoUso+'" name="tipoarg'+contCodigoUso+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Recreación activa">Recreación activa</option>'+
              '<option value="Parque Infantil">Parque Infantil</option>'+
              '<option value="Parques Urbanos">Parques Urbanos</option>'+
              '<option value="Recreación">Recreación</option>'+
              '<option value="Deportivo">Deportivo</option>'+
              '<option value="Industria">Industria</option>'+
              '<option value="Artistico Cultural">Artistico Cultural</option>'+
              '<option value="Recreación Pasiva">Recreación Pasiva</option>'+
          '</select>'+
      '</div>'+
      '<div class="col-md-3">'+
          'Tradicional <input type="checkbox" id="tradicionalUso'+contCodigoUso+'">'+
      '</div>'+
      '<div class="col-md-3">'+
          'Actual <input type="checkbox" id="actuallUso'+contCodigoUso+'">'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaUso('+contCodigoUso+');"></i>'+
      '</a></div></div>';
        var cadenaUso='';
        var contU=0;
        arrDibujoUso.forEach(function (item, index, array) 
        {
          arrCodigoUso[contU].descripU = $("#descUso"+item.id+"").val();
          arrCodigoUso[contU].tradicionalU = document.getElementById("tradicionalUso"+item.id+"").checked;
          arrCodigoUso[contU].actualU = document.getElementById("actuallUso"+item.id+"").checked;
          
          contU++;
        });

        arrCodigoUso.push({descripU:'N',tradicionalU:'',actualU:''});
        arrDibujoUso.push({scd_codigo:htmlUso,id: contCodigoUso});

        contCodigoUso++;
        contU=1;
        for (var i=0; i<arrDibujoUso.length;i++)
        {
          cadenaUso = cadenaUso + '<br><div class="form-group"><label class="col-md-2">'+contU+'. '+arrDibujoUso[i].scd_codigo;
          contU++;
        }
        //////console.log("cadenaaaaUso",cadenaUso);
        $('#crearGrillaUso').html(cadenaUso); 
        
        var contoUso=0;
        arrDibujoUso.forEach(function (item, index, array) 
        { 
          
          $("#descUso"+item.id+"").val(arrCodigoUso[contoUso].descripU);
          document.getElementById("tradicionalUso"+item.id+"").checked = (arrCodigoUso[contoUso].tradicionalU); 
          document.getElementById("actuallUso"+item.id+"").checked = (arrCodigoUso[contoUso].actualU);
          contoUso=contoUso+1;
          
        });
        //////console.log("vectoooorr",arrCodigoUso);     
    }

    function eliminarGrillaUso($id){
      
        if(contCodigoUso>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaUso='';
                var cont=0;
                var pos=-1;
                arrDibujoUso.forEach(function (item, index, array) 
                {
                  arrCodigoUso[cont].descripU = $("#descUso"+item.id+"").val();
                  arrCodigoUso[cont].tradicionalU = document.getElementById("tradicionalUso"+item.id+"").checked;
                  arrCodigoUso[cont].actualU = document.getElementById("actuallUso"+item.id+"").checked;
                  cont++;
                });
                pos = arrDibujo.map(function(e) {return e.id;}).indexOf($id);
                arrCodigoUso.splice(pos,1);
                arrDibujoUso.splice(pos,1);
                cont=1;
                for (var i=0; i<arrDibujoUso.length;i++)
                {
                    cadenaUso=cadenaUso + '<br><div class="form-group"><label class="col-md-2">'+cont+'. '+arrDibujoUso[i].scd_codigo;
                    cont++;
                }
                $('#crearGrillaUso').html(cadenaUso);
                var contoUso=0;
                arrDibujoUso.forEach(function (item, index, array) 
                { 
                  $("#descUso"+item.id+"").val(arrCodigoUso[contoUso].descripU);
                  document.getElementById("tradicionalUso"+item.id+"").checked = (arrCodigoUso[contoUso].tradicionalU); 
                  document.getElementById("actuallUso"+item.id+"").checked = (arrCodigoUso[contoUso].actualU);
                  contoUso=contoUso+1;
                  
                });

            });
        }
    }
 
var arrDibujoUSact=[];
var aarrCodigoUso = [];
var contUSact = 0;
function editarUsoSuelo(longitudGrilla){
  var contUS=1;
  arrDibujoUSact=[];
  var htmlUS;
  for (var i=1; i<longitudGrilla;i++){
       htmlUS = '<div class="col-md-12">'+
        '<div class="form-group col-md-5">'+
        '<select class="form-control" id="desccripcionUS'+contUS+'" name="tipoarg1'+contUS+'">'+
          '<option value="N" selected="selected">--Seleccione--</option>'+
          '<option value="Recreación activa">Recreación activa</option>'+
          '<option value="Parque Infantil">Parque Infantil</option>'+
          '<option value="Parques Urbanos">Parques Urbanos</option>'+
          '<option value="Recreación">Recreación</option>'+
          '<option value="Deportivo">Deportivo</option>'+
          '<option value="Industria">Industria</option>'+
          '<option value="Artistico Cultural">Artistico Cultural</option>'+
          '<option value="Recreación Pasiva">Recreación Pasiva</option>'+ 
        '</select>'+
        '</div>'+
        '<div class="form-group col-md-1">'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="checkbox" id="tradicionUS'+contUS+'" name="tradicionUS'+contUS+'">'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="checkbox" id="actuallUS'+contUS+'">'+
        '</div>'+
        '<div class="col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarUsoSuelo('+contUS+');"></i>'+
        '</a></div>'+
        '</div>';
       arrDibujoUSact.push({scd_codigo:htmlUS,id: contUS});
        contUS++;
      }
        var cadenaUS='';
        var cont=1;
        for (var i=0; i<arrDibujoUSact.length;i++)
        {
          cadenaUS=cadenaUS + arrDibujoUSact[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaUso').html(cadenaUS);
      } 
 

 function actualizarUsoSuelo(){
    var htmlUsoSact;
        htmlUsoSact='<div class="col-md-12">'+
        '<div class="form-group col-md-5">'+
        '<select class="form-control" id="desccripcionUS'+contUSact+'" name="tipoarg1'+contUSact+'">'+
          '<option value="N" selected="selected">--Seleccione--</option>'+
          '<option value="Recreación activa">Recreación activa</option>'+
          '<option value="Parque Infantil">Parque Infantil</option>'+
          '<option value="Parques Urbanos">Parques Urbanos</option>'+
          '<option value="Recreación">Recreación</option>'+
          '<option value="Deportivo">Deportivo</option>'+
          '<option value="Industria">Industria</option>'+
          '<option value="Artistico Cultural">Artistico Cultural</option>'+
          '<option value="Recreación Pasiva">Recreación Pasiva</option>'+ 
        '</select>'+
        '</div>'+
        '<div class="form-group col-md-1">'+
        '</div>'+
        '<div class="form-group col-md-3">'+
        '<input type="checkbox" id="tradicionUS'+contUSact+'" name="tradicionUS'+contUSact+'">'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="checkbox" id="actuallUS'+contUSact+'">'+
        '</div>'+
        '<div class="col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarUsoSuelo('+contUSact+');"></i>'+
        '</a></div>'+
        '</div>';
        var cadenaUso='';
        var contU=0;
        arrDibujoUSact.forEach(function (item, index, array) 
        {
          aarrCodigoUso[contU].adescripU = $("#desccripcionUS"+item.id+"").val();
          aarrCodigoUso[contU].atradicionalU = document.getElementById("tradicionUS"+item.id+"").checked;
          aarrCodigoUso[contU].aactualU = document.getElementById("actuallUS"+item.id+"").checked;
         contU++;
        });
        aarrCodigoUso.push({adescripU:'N',atradicionalU:'',aactualU:''});
        arrDibujoUSact.push({scd_codigo:htmlUsoSact,id: contUSact});
        contUSact++;
        contU=1;
        for (var i=0; i<arrDibujoUSact.length;i++)
        {
          cadenaUso = cadenaUso +contU+'. '+arrDibujoUSact[i].scd_codigo;
          contU++;
        }
        $('#acrearGrillaUso').html(cadenaUso); 
        var contoUso=0;
        arrDibujoUSact.forEach(function (item, index, array) 
        { 
          $("#desccripcionUS"+item.id+"").val(aarrCodigoUso[contoUso].adescripU);
          document.getElementById("tradicionUS"+item.id+"").checked = (aarrCodigoUso[contoUso].atradicionalU); 
          document.getElementById("actuallUS"+item.id+"").checked = (aarrCodigoUso[contoUso].aactualU);
          contoUso=contoUso+1;
        });  
    }


    function eliminarUsoSuelo($id){
        if(contUSact>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaUso='';
                var cont=0;
                var pos=-1;
                arrDibujoUSact.forEach(function (item, index, array) 
                {
                  aarrCodigoUso[cont].adescripU = $("#desccripcionUS"+item.id+"").val();
                  aarrCodigoUso[cont].atradicionalU = document.getElementById("tradicionUS"+item.id+"").checked;
                  aarrCodigoUso[cont].aactualU = document.getElementById("actuallUS"+item.id+"").checked;
                  cont++;
                });
                pos = arrDibujoUSact.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoUso.splice(pos,1);
                arrDibujoUSact.splice(pos,1);
                cont=1;
                for (var i=0; i<arrDibujoUSact.length;i++)
                {
                    cadenaUso=cadenaUso +arrDibujoUSact[i].scd_codigo;
                    cont++;
                }
                $('#acrearGrillaUso').html(cadenaUso);
                var contoUso=0;
                arrDibujoUSact.forEach(function (item, index, array) 
                { 
                  $("#desccripcionUS"+item.id+"").val(aarrCodigoUso[contoUso].adescripU);
                  document.getElementById("tradicionUS"+item.id+"").checked = (aarrCodigoUso[contoUso].atradicionalU); 
                  document.getElementById("actuallUS"+item.id+"").checked = (aarrCodigoUso[contoUso].aactualU);
                  contoUso=contoUso+1;  
                });

            });
        }

    }
//---------------------------- FIN GRILLA USO SUELO----------------------//


//-------------------------------begin grilla servicio--------------------//

    var contCodigoSer = 2;
    var arrCodigoSer = [];
    var arrDibujoSer = [];
    htmlServicio = 'Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descServicio1" name="tipoarg1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Agua">Agua</option>'+
              '<option value="Gas">Gas</option>'+
              '<option value="Recopilación">Recopilación</option>'+
              '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
              '<option value="Seguridad">Seguridad</option>'+
              '<option value="Otros Servicios">Otros Servicios</option>'+
          '</select>'+
      '</div>'+
      '<label class="col-md-2">Proveedor:</label>'+
      '<div class="col-md-3">'+
        '<select class="form-control" id="proveedorServicio1" name="tipoarg1">'+
            '<option value="N" selected="selected">--Seleccione--</option>'+
            '<option value="Red Publica">Red Publica</option>'+
            '<option value="Sistema Propio">Sistema Propio</option>'+
        '</select>'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaServicio(1);"></i>'+
      '</a></div></div>';
    var cadenaServicio = '';  
    arrCodigoSer.push({descServicio:'N',proveedorServicio:'N'});
    arrDibujoSer.push({scd_codigo:htmlServicio,id: 1}); 
    cadenaServicio='<div class="form-group"><label class="col-md-2">1. '+arrDibujoSer[0].scd_codigo;
    $('#crearGrillaServicio').html(cadenaServicio);
    $("#descServicio1").val(arrCodigoSer[0].descServicio); 
    $("#proveedorServicio1").val(arrCodigoSer[0].proveedorServicio); 
   
function GrillaServicio(){
    var htmlServicio;
        htmlServicio = 'Descripción:</label>'+
      '<div class="col-md-3">'+
          '<select class="form-control" id="descServicio'+contCodigoSer+'">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Agua">Agua</option>'+
              '<option value="Gas">Gas</option>'+
              '<option value="Recopilación">Recopilación</option>'+
              '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
              '<option value="Seguridad">Seguridad</option>'+
              '<option value="Otros Servicios">Otros Servicios</option>'+
          '</select>'+
      '</div>'+
      '<label class="col-md-2">Proveedor:</label>'+
      '<div class="col-md-3">'+
        '<select class="form-control" id="proveedorServicio'+contCodigoSer+'">'+
            '<option value="N" selected="selected">--Seleccione--</option>'+
            '<option value="Red Publica">Red Publica</option>'+
            '<option value="Sistema Propio">Sistema Propio</option>'+
        '</select>'+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaServicio('+contCodigoSer+');"></i>'+
      '</a></div></div>';
        var cadenaServicio='';
        var contU=0;
        arrDibujoSer.forEach(function (item, index, array) 
        {
          arrCodigoSer[contU].descServicio = $("#descServicio"+item.id+"").val();
          arrCodigoSer[contU].proveedorServicio = $("#proveedorServicio"+item.id+"").val();
          contU++;
        });
        arrCodigoSer.push({descServicio:'N',proveedorServicio:'N'});
        arrDibujoSer.push({scd_codigo:htmlServicio,id: contCodigoSer});
        contCodigoSer++;
        contU=1;
        for (var i=0; i<arrDibujoSer.length;i++)
        {
          cadenaServicio = cadenaServicio + '<br><div class="form-group"><label class="col-md-2">'+contU+'. '+arrDibujoSer[i].scd_codigo;
          contU++;
        }
        $('#crearGrillaServicio').html(cadenaServicio);
        var contoU=0;
        arrDibujoSer.forEach(function (item, index, array) 
        { 
          $("#descServicio"+item.id+"").val(arrCodigoSer[contoU].descServicio); 
          $("#proveedorServicio"+item.id+"").val(arrCodigoSer[contoU].proveedorServicio); 
          contoU=contoU+1; 
        });    
    }

    function eliminarGrillaServicio($id){
        if(contCodigoSer>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaServicio='';
                var pos=-1;
               
                var contU=0;
                arrDibujoSer.forEach(function (item, index, array) 
                {
                  arrCodigoSer[contU].descServicio = $("#descServicio"+item.id+"").val();
                  arrCodigoSer[contU].proveedorServicio = $("#proveedorServicio"+item.id+"").val();
                  contU++;
                });
                pos = arrDibujoSer.map(function(e) {return e.id;}).indexOf($id);
                arrCodigoSer.splice(pos,1);
                arrDibujoSer.splice(pos,1);    
                contU=1;
                for (var i=0; i<arrDibujoSer.length;i++)
                {
                  cadenaServicio = cadenaServicio + '<br><div class="form-group"><label class="col-md-2">'+contU+'. '+arrDibujoSer[i].scd_codigo;
                  contU++;
                }
                $('#crearGrillaServicio').html(cadenaServicio);
                var contoU=0;
                arrDibujoSer.forEach(function (item, index, array) 
                { 
                  $("#descServicio"+item.id+"").val(arrCodigoSer[contoU].descServicio); 
                  $("#proveedorServicio"+item.id+"").val(arrCodigoSer[contoU].proveedorServicio); 
                  contoU=contoU+1;
                });

            });
        }
    }
 
//-----editar y actualizar grilla servicio----------------------

var arrDibujoS = [];
var aarrCodigoSer = [];
var contSerAct = 0;
function editarServicio(longitudGrilla){
  var contS=1;
  arrDibujoS=[];
  var htmlS;
  for (var i=1; i<longitudGrilla;i++){
       htmlS = '<div class="col-md-12">'+
        '<div class="form-group col-md-5">'+
        '<select class="form-control" id="descripcionS'+contS+'" name="tipoarg1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Agua">Agua</option>'+
              '<option value="Gas">Gas</option>'+
              '<option value="Recopilación">Recopilación</option>'+
              '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
              '<option value="Seguridad">Seguridad</option>'+
              '<option value="Otros Servicios">Otros Servicios</option>'+
          '</select>'+
        '</div>'+
        '<div class="form-group col-md-6">'+
        '<select class="form-control" id="proveedorS'+contS+'" name="tipoarg1">'+
            '<option value="N" selected="selected">--Seleccione--</option>'+
            '<option value="Red Publica">Red Publica</option>'+
            '<option value="Sistema Propio">Sistema Propio</option>'+
        '</select>'+
        '</div>'+
        '<div class="col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarServicio('+contS+');"></i>'+
        '</a></div>'+
        '</div>';
       arrDibujoS.push({scd_codigo:htmlS,id: contS});
        contS++;
      }
        var cadenaS='';
        var cont=1;
        for (var i=0; i<arrDibujoS.length;i++)
        {
          cadenaS=cadenaS + arrDibujoS[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaServicio').html(cadenaS);
  }

function actualizarServicio(){
    var htmlSerAct;
        htmlSerAct = '<div class="col-md-12">'+
        '<div class="form-group col-md-5">'+
        '<select class="form-control" id="descripcionS'+contSerAct+'" name="tipoarg1">'+
              '<option value="N" selected="selected">--Seleccione--</option>'+
              '<option value="Agua">Agua</option>'+
              '<option value="Gas">Gas</option>'+
              '<option value="Recopilación">Recopilación</option>'+
              '<option value="Servicios Higiénicos">Servicios Higiénicos</option>'+
              '<option value="Seguridad">Seguridad</option>'+
              '<option value="Otros Servicios">Otros Servicios</option>'+
          '</select>'+
        '</div>'+
        '<div class="form-group col-md-6">'+
        '<select class="form-control" id="proveedorS'+contSerAct+'" name="tipoarg1">'+
            '<option value="N" selected="selected">--Seleccione--</option>'+
            '<option value="Red Publica">Red Publica</option>'+
            '<option value="Sistema Propio">Sistema Propio</option>'+
        '</select>'+
        '</div>'+
        '<div class="col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarServicio('+contSerAct+');"></i>'+
        '</a></div>'+
        '</div>';
        var cadenaServicio='';
        var contU=0;
        arrDibujoS.forEach(function (item, index, array) 
        {
          aarrCodigoSer[contU].adescServicio = $("#descripcionS"+item.id+"").val();
          aarrCodigoSer[contU].aproveedorServicio = $("#proveedorS"+item.id+"").val();
          contU++;
        });
        aarrCodigoSer.push({adescServicio:'N',aproveedorServicio:'N'});
        arrDibujoS.push({scd_codigo:htmlSerAct,id: contSerAct});
        contSerAct++;
        contU=1;
        for (var i=0; i<arrDibujoS.length;i++)
        {
          cadenaServicio = cadenaServicio + arrDibujoS[i].scd_codigo;
          contU++;
        }
        $('#acrearGrillaServicio').html(cadenaServicio);
        var contoU=0;
        arrDibujoS.forEach(function (item, index, array) 
        { 
          $("#descripcionS"+item.id+"").val(aarrCodigoSer[contoU].adescServicio); 
          $("#proveedorS"+item.id+"").val(aarrCodigoSer[contoU].aproveedorServicio); 
          contoU=contoU+1; 
        });    
    }


    function eliminarServicio($id){
        if(contSerAct>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaServicio='';
                var pos=-1;
               
                var contU=0;
                arrDibujoS.forEach(function (item, index, array) 
                {
                  aarrCodigoSer[contU].adescServicio = $("#descripcionS"+item.id+"").val();
                  aarrCodigoSer[contU].aproveedorServicio = $("#proveedorS"+item.id+"").val();
                  contU++;
                });
                pos = arrDibujoS.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoSer.splice(pos,1);
                arrDibujoS.splice(pos,1);    
                contU=1;
                for (var i=0; i<arrDibujoS.length;i++)
                {
                  cadenaServicio = cadenaServicio + arrDibujoS[i].scd_codigo;
                  contU++;
                }
                $('#acrearGrillaServicio').html(cadenaServicio);
                var contoU=0;
                arrDibujoS.forEach(function (item, index, array) 
                { 
                  $("#descripcionS"+item.id+"").val(aarrCodigoSer[contoU].adescServicio); 
                  $("#proveedorS"+item.id+"").val(aarrCodigoSer[contoU].aproveedorServicio); 
                  contoU=contoU+1;
                });

            });
        }
    }
//------------------------end grilla servicio--------------------//




//-----------------------BEGIN GRILLA REJAS-------------------- //
    var contCodigoRejas = 2;
    var arrCodigoRejas = [];
    var arrDibujoRejas = [];

     htmlRejas = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionRej1" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Dibujos geo.">Dibujos geo.</option>'+
              '<option value="Dibujos Vegetales.">Dibujos Vegetales.</option>'+
              '<option value="Concavo">Concavo</option>'+
              '<option value="Antepechado">Antepechado</option>'+
            '</select> '+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '</div>'+
        '<div class="form-group col-md-3">'+
          '<input type="checkbox" id="hierro1"> Hierro<br>'+
          '<input type="checkbox" id="tubo1"> Tubo<br>'+
          '<input type="checkbox" id="madera1"> Madera <br>'+
          '<input type="checkbox" id="otros1"> OTros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDesc1" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridRejas1" class="form-control">'+
              '<option value="" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
        '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaRejas(1);"></i>'+
      '</a></div>'+
      '</div>';

    var cadenaRejas = '';  
    arrCodigoRejas.push({descripcionR:'N',hierroR:'',tuboR:'',maderaR:'',otrosR:'',otrosDescR:'',integridadR:''});
    arrDibujoRejas.push({scd_codigo:htmlRejas,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaRejas=arrDibujoRejas[0].scd_codigo;
    ////console.log("cadenaRejas",cadenaRejas);
    $('#crearGrillaRejas').html(cadenaRejas);

    $("#descripcionRej1").val(arrCodigoRejas[0].descripcionR); 
    document.getElementById("hierro1").checked = (arrCodigoRejas[0].hierroR); 
    document.getElementById("tubo1").checked = (arrCodigoRejas[0].tuboR);
    document.getElementById("madera1").checked = (arrCodigoRejas[0].maderaR);
    document.getElementById("otros1").checked = (arrCodigoRejas[0].otrosR);
    $("#otrosDesc1").val(arrCodigoRejas[0].otrosDescR); 
    $("#integridRejas1").val(arrCodigoRejas[0].integridadR); 

   
    function grillaRejas(){
    var htmlRejas;
        htmlRejas = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionRej'+contCodigoRejas+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Dibujos geo.">Dibujos geo.</option>'+
              '<option value="Dibujos Vegetales.">Dibujos Vegetales.</option>'+
              '<option value="Concavo">Concavo</option>'+
              '<option value="Antepechado">Antepechado</option>'+
            '</select> '+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '</div>'+
        '<div class="form-group col-md-3">'+
          '<input type="checkbox" id="hierro'+contCodigoRejas+'"> Hierro<br>'+
          '<input type="checkbox" id="tubo'+contCodigoRejas+'"> Tubo<br>'+
          '<input type="checkbox" id="madera'+contCodigoRejas+'"> Madera <br>'+
          '<input type="checkbox" id="otros'+contCodigoRejas+'"> OTros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDesc'+contCodigoRejas+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridRejas'+contCodigoRejas+'" class="form-control">'+
              '<option value="" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
          '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaRejas('+contCodigoRejas+');"></i>'+
       '</a></div>'+
       '</div>';

        var cadenaRejas='';
        var cont=0;
        arrDibujoRejas.forEach(function (item, index, array) 
        {
          arrCodigoRejas[cont].descripcionR = $("#descripcionRej"+item.id+"").val();
          arrCodigoRejas[cont].hierroR = document.getElementById("hierro"+item.id+"").checked;
          arrCodigoRejas[cont].tuboR = document.getElementById("tubo"+item.id+"").checked;
          arrCodigoRejas[cont].maderaR = document.getElementById("madera"+item.id+"").checked;
          arrCodigoRejas[cont].otrosR = document.getElementById("otros"+item.id+"").checked;
          arrCodigoRejas[cont].otrosDescR = $("#otrosDesc"+item.id+"").val();
          arrCodigoRejas[cont].integridadR = $("#integridRejas"+item.id+"").val();
          cont++;
        });
     
        arrCodigoRejas.push({descripcionR:'N',hierroR:'',tuboR:'',maderaR:'',otrosR:'',otrosDescR:'',integridadR:''});
        arrDibujoRejas.push({scd_codigo:htmlRejas,id: contCodigoRejas});
        contCodigoRejas++;

        cont=1;
        for (var i=0; i<arrDibujoRejas.length;i++)
        {
          cadenaRejas = cadenaRejas + arrDibujoRejas[i].scd_codigo;
          cont++;
        }
      
        $('#crearGrillaRejas').html(cadenaRejas); 

        var conto=0;
        arrDibujoRejas.forEach(function (item, index, array) 
        { 
          $("#descripcionRej"+item.id+"").val(arrCodigoRejas[conto].descripcionR); 
          document.getElementById("hierro"+item.id+"").checked = (arrCodigoRejas[conto].hierroR); 
          document.getElementById("tubo"+item.id+"").checked = (arrCodigoRejas[conto].tuboR);
          document.getElementById("madera"+item.id+"").checked = (arrCodigoRejas[conto].maderaR);
          document.getElementById("otros"+item.id+"").checked = (arrCodigoRejas[conto].otrosR);
          $("#otrosDesc"+item.id+"").val(arrCodigoRejas[conto].otrosDescR); 
          $("#integridRejas"+item.id+"").val(arrCodigoRejas[conto].integridadR); 

          conto=conto+1;
        });

        ////console.log("arrCodigoRejas",arrCodigoRejas);
    } 

    function eliminarGrillaRejas($id){
        if(contCodigoRejas>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaRejas = '';
                var pos = -1;
                var cont = 0;

                arrDibujoRejas.forEach(function (item, index, array) 
                {
                  arrCodigoRejas[cont].descripcionR = $("#descripcionRej"+item.id+"").val();
                  arrCodigoRejas[cont].hierroR = document.getElementById("hierro"+item.id+"").checked;
                  arrCodigoRejas[cont].tuboR = document.getElementById("tubo"+item.id+"").checked;
                  arrCodigoRejas[cont].maderaR = document.getElementById("madera"+item.id+"").checked;
                  arrCodigoRejas[cont].otrosR = document.getElementById("otros"+item.id+"").checked;
                  arrCodigoRejas[cont].otrosDescR = $("#otrosDesc"+item.id+"").val();
                  arrCodigoRejas[cont].integridadR = $("#integridRejas"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoRejas.map(function(e) {return e.id;}).indexOf($id);
                //console.log("poos",pos);
                arrCodigoRejas.splice(pos,1);
                arrDibujoRejas.splice(pos,1);
                //console.log("EliminarRejas",arrCodigoRejas);
                
                cont=1;
                for (var i=0; i<arrDibujoRejas.length;i++)
                {
                  cadenaRejas = cadenaRejas + arrDibujoRejas[i].scd_codigo;
                  cont++;
                }
                
                $('#crearGrillaRejas').html(cadenaRejas);
                
                var conto=0;
                arrDibujoRejas.forEach(function (item, index, array) 
                { 
                  $("#descripcionRej"+item.id+"").val(arrCodigoRejas[conto].descripcionR); 
                  document.getElementById("hierro"+item.id+"").checked = (arrCodigoRejas[conto].hierroR); 
                  document.getElementById("tubo"+item.id+"").checked = (arrCodigoRejas[conto].tuboR);
                  document.getElementById("madera"+item.id+"").checked = (arrCodigoRejas[conto].maderaR);
                  document.getElementById("otros"+item.id+"").checked = (arrCodigoRejas[conto].otrosR);
                  $("#otrosDesc"+item.id+"").val(arrCodigoRejas[conto].otrosDescR); 
                  $("#integridRejas"+item.id+"").val(arrCodigoRejas[conto].integridadR); 

                  conto=conto+1;
                });
            });
        }
        
    }


var arrRE = [];
var aarrCodigoRejas = [];
var contRactual = 0;
function editarGrillaRejas(longitudGrilla){
  var contRejas = 1;
  arrRE=[];
  var htmlRE;
  for (var i=1; i<longitudGrilla;i++){
       htmlRE = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionRE'+contRejas+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Dibujos geo.">Dibujos geo.</option>'+
              '<option value="Dibujos Vegetales.">Dibujos Vegetales.</option>'+
              '<option value="Concavo">Concavo</option>'+
              '<option value="Antepechado">Antepechado</option>'+
            '</select> '+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '</div>'+
        '<div class="form-group col-md-3">'+
          '<input type="checkbox" id="hierroRE'+contRejas+'">Hierro<br>'+
          '<input type="checkbox" id="tuboRE'+contRejas+'">Tubo<br>'+
          '<input type="checkbox" id="maderaRE'+contRejas+'">Madera <br>'+
          '<input type="checkbox" id="otrosRE'+contRejas+'">OTros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDRE'+contRejas+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridadRE'+contRejas+'" class="form-control">'+
              '<option value="" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
        '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarRejas('+contRejas+');"></i>'+
      '</a></div>'+
      '</div>';
       arrRE.push({scd_codigo:htmlRE,id: contRejas});
        contRejas++;
        }
        var cadenaRejas='';
        var cont=1;
        for (var i=0; i<arrRE.length;i++)
        {
          cadenaRejas=cadenaRejas + arrRE[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaRejas').html(cadenaRejas);
  }


function actualizarRejas(){
    var htmlRejasAct;
        htmlRejasAct = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionRE'+contRactual+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Dibujos geo.">Dibujos geo.</option>'+
              '<option value="Dibujos Vegetales.">Dibujos Vegetales.</option>'+
              '<option value="Concavo">Concavo</option>'+
              '<option value="Antepechado">Antepechado</option>'+
            '</select> '+
          '</div>'+
           '<div class="form-group col-md-2">'+
          '</div>'+
        '<div class="form-group col-md-3">'+
          '<input type="checkbox" id="hierroRE'+contRactual+'">Hierro<br>'+
          '<input type="checkbox" id="tuboRE'+contRactual+'">Tubo<br>'+
          '<input type="checkbox" id="maderaRE'+contRactual+'">Madera <br>'+
          '<input type="checkbox" id="otrosRE'+contRactual+'">OTros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDRE'+contRactual+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridadRE'+contRactual+'" class="form-control">'+
              '<option value="" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
        '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarRejas('+contRactual+');"></i>'+
      '</a></div>'+
      '</div>';
        var cadenaRejas='';
        var cont=0;
        arrRE.forEach(function (item, index, array) 
        {
          aarrCodigoRejas[cont].adescripcionR = $("#descripcionRE"+item.id+"").val();
          aarrCodigoRejas[cont].ahierroR = document.getElementById("hierroRE"+item.id+"").checked;
          aarrCodigoRejas[cont].atuboR = document.getElementById("tuboRE"+item.id+"").checked;
          aarrCodigoRejas[cont].amaderaR = document.getElementById("maderaRE"+item.id+"").checked;
          aarrCodigoRejas[cont].aotrosR = document.getElementById("otrosRE"+item.id+"").checked;
          aarrCodigoRejas[cont].aotrosDescR = $("#otrosDRE"+item.id+"").val();
          aarrCodigoRejas[cont].aintegridadR = $("#integridadRE"+item.id+"").val();
          cont++;
        });
        aarrCodigoRejas.push({adescripcionR:'N',ahierroR:'',atuboR:'',amaderaR:'',aotrosR:'',aotrosDescR:'',aintegridadR:''});
        arrRE.push({scd_codigo:htmlRejasAct,id: contRactual});
        contRactual++;
        cont=1;
        for (var i=0; i<arrRE.length;i++)
        {
          cadenaRejas = cadenaRejas + arrRE[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaRejas').html(cadenaRejas); 
        var conto=0;
        arrRE.forEach(function (item, index, array) 
        { 
          $("#descripcionRE"+item.id+"").val(aarrCodigoRejas[conto].adescripcionR); 
          document.getElementById("hierroRE"+item.id+"").checked = (aarrCodigoRejas[conto].ahierroR); 
          document.getElementById("tuboRE"+item.id+"").checked = (aarrCodigoRejas[conto].atuboR);
          document.getElementById("maderaRE"+item.id+"").checked = (aarrCodigoRejas[conto].amaderaR);
          document.getElementById("otrosRE"+item.id+"").checked = (aarrCodigoRejas[conto].aotrosR);
          $("#otrosDRE"+item.id+"").val(aarrCodigoRejas[conto].aotrosDescR); 
          $("#integridadRE"+item.id+"").val(aarrCodigoRejas[conto].aintegridadR); 
          conto=conto+1;
        });

    }

    function eliminarRejas($id){
        if(contRactual>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el registro seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaRejas = '';
                var pos = -1;
                var cont = 0;

                arrRE.forEach(function (item, index, array) 
                {
                  aarrCodigoRejas[cont].adescripcionR = $("#descripcionRE"+item.id+"").val();
                  aarrCodigoRejas[cont].ahierroR = document.getElementById("hierroRE"+item.id+"").checked;
                  aarrCodigoRejas[cont].atuboR = document.getElementById("tuboRE"+item.id+"").checked;
                  aarrCodigoRejas[cont].amaderaR = document.getElementById("maderaRE"+item.id+"").checked;
                  aarrCodigoRejas[cont].aotrosR = document.getElementById("otrosRE"+item.id+"").checked;
                  aarrCodigoRejas[cont].aotrosDescR = $("#otrosDRE"+item.id+"").val();
                  aarrCodigoRejas[cont].aintegridadR = $("#integridadRE"+item.id+"").val();
                  cont++;
                });
                pos = arrRE.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoRejas.splice(pos,1);
                arrRE.splice(pos,1);
                cont=1;
                for (var i=0; i<arrRE.length;i++)
                {
                  cadenaRejas = cadenaRejas + arrRE[i].scd_codigo;
                  cont++;
                }    
                $('#acrearGrillaRejas').html(cadenaRejas); 
                var conto=0;
                arrRE.forEach(function (item, index, array) 
                { 
                $("#descripcionRE"+item.id+"").val(aarrCodigoRejas[conto].adescripcionR); 
                document.getElementById("hierroRE"+item.id+"").checked = (aarrCodigoRejas[conto].ahierroR); 
                document.getElementById("tuboRE"+item.id+"").checked = (aarrCodigoRejas[conto].atuboR);
                document.getElementById("maderaRE"+item.id+"").checked = (aarrCodigoRejas[conto].amaderaR);
                document.getElementById("otrosRE"+item.id+"").checked = (aarrCodigoRejas[conto].aotrosR);
                $("#otrosDRE"+item.id+"").val(aarrCodigoRejas[conto].aotrosDescR); 
                $("#integridadRE"+item.id+"").val(aarrCodigoRejas[conto].aintegridadR);
                  conto=conto+1;
                });
            });
        }  
    }

//----------------------FIN GRILLA REJAS--------------------------------- //
//-----------------------BEGIN GRILLA ACABADOS DE MURO-------------------- //
    var contCodigoAcab = 2;
    var arrCodigoAcab = [];
    var arrDibujoAcab = [];


        htmlAcab='<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionAcab1" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Revoque Grueso">Revoque Grueso</option>'+
              '<option value="Revoque Fino">Revoque Fino</option>'+
              '<option value="Estructura Aparente">Estructura Aparente</option>'+
              '<option value="Texturizado">Texturizado</option>'+
              '<option value="Enchape">Enchape</option>'+
            '</select> '+
          '</div>'+
          '<div class="form-group col-md-1">'+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArena1"> Cal y Arena<br>'+
          '<input type="checkbox" id="cemento1"> Cemento y Arena<br>'+
          '<input type="checkbox" id="piedra1"> Piedra<br>'+
          '<input type="checkbox" id="marmol1"> Mármol'+
        '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="maderamuro1"> Madera<br>'+
          '<input type="checkbox" id="azulejo1"> Azulejo<br>'+
          '<input type="checkbox" id="ladrillo1"> Ladrillo<br>'+
          '<input type="checkbox" id="otroAcab1"> Otros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDescAcab1" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridAcab1" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
       '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaAcabado(1);"></i>'+
       '</a></div>'+
        '</div>';

    var cadenaAcab = '';  
    arrCodigoAcab.push({descripcionA:'N',calArenaA:'',cementoA:'',piedraA:'',marmolA:'',maderaA:'',azulejoA:'',ladrilloA:'',otroAcabA:'',otroDescAcabA:'',integridadAcabA:''});
    arrDibujoAcab.push({scd_codigo:htmlAcab,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaAcab=arrDibujoAcab[0].scd_codigo;
    //console.log("cadenaAcab",cadenaAcab);
    $('#crearGrillaAcabado').html(cadenaAcab);

    $("#descripcionAcab1").val(arrCodigoAcab[0].descripcionA); 
    document.getElementById("calArena1").checked = (arrCodigoAcab[0].calArenaA); 
    document.getElementById("cemento1").checked = (arrCodigoAcab[0].cementoA);
    document.getElementById("piedra1").checked = (arrCodigoAcab[0].piedraA);
    document.getElementById("marmol1").checked = (arrCodigoAcab[0].marmolA);
    document.getElementById("maderamuro1").checked = (arrCodigoAcab[0].maderaA); 
    document.getElementById("azulejo1").checked = (arrCodigoAcab[0].azulejoA);
    document.getElementById("ladrillo1").checked = (arrCodigoAcab[0].ladrilloA);
    document.getElementById("otroAcab1").checked = (arrCodigoAcab[0].otroAcabA);
    $("#otrosDescAcab1").val(arrCodigoAcab[0].otroDescAcabA); 
    $("#integridAcab1").val(arrCodigoAcab[0].integridadAcabA);

    function grillaAcabadoMuro(){

    var htmlAcab;
        htmlAcab = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionAcab'+contCodigoAcab+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Revoque Grueso">Revoque Grueso</option>'+
              '<option value="Revoque Fino">Revoque Fino</option>'+
              '<option value="Estructura Aparente">Estructura Aparente</option>'+
              '<option value="Texturizado">Texturizado</option>'+
              '<option value="Enchape">Enchape</option>'+
            '</select> '+
          '</div>'+
          '<div class="form-group col-md-1">'+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArena'+contCodigoAcab+'"> Cal y Arena<br>'+
          '<input type="checkbox" id="cemento'+contCodigoAcab+'"> Cemento y Arena<br>'+
          '<input type="checkbox" id="piedra'+contCodigoAcab+'"> Piedra<br>'+
          '<input type="checkbox" id="marmol'+contCodigoAcab+'"> Mármol'+
        '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="maderamuro'+contCodigoAcab+'"> Madera<br>'+
          '<input type="checkbox" id="azulejo'+contCodigoAcab+'"> Azulejo<br>'+
          '<input type="checkbox" id="ladrillo'+contCodigoAcab+'"> Ladrillo<br>'+
          '<input type="checkbox" id="otroAcab'+contCodigoAcab+'"> Otros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDescAcab'+contCodigoAcab+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridAcab'+contCodigoAcab+'" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
       '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarGrillaAcabado('+contCodigoAcab+');"></i>'+
       '</a></div>'+
        '</div>';

        var cadenaAcab = '';
        var cont = 0;
        arrDibujoAcab.forEach(function (item, index, array) 
        {
          arrCodigoAcab[cont].descripcionA = $("#descripcionAcab"+item.id+"").val();
          arrCodigoAcab[cont].calArenaA = document.getElementById("calArena"+item.id+"").checked;
          arrCodigoAcab[cont].cementoA = document.getElementById("cemento"+item.id+"").checked;
          arrCodigoAcab[cont].piedraA = document.getElementById("piedra"+item.id+"").checked;
          arrCodigoAcab[cont].marmolA = document.getElementById("marmol"+item.id+"").checked;
          arrCodigoAcab[cont].maderaA = document.getElementById("maderamuro"+item.id+"").checked;
          arrCodigoAcab[cont].azulejoA = document.getElementById("azulejo"+item.id+"").checked;
          arrCodigoAcab[cont].ladrilloA = document.getElementById("ladrillo"+item.id+"").checked;
          arrCodigoAcab[cont].otroAcabA = document.getElementById("otroAcab"+item.id+"").checked;
          arrCodigoAcab[cont].otroDescAcabA = $("#otrosDescAcab"+item.id+"").val();
          arrCodigoAcab[cont].integridadAcabA = $("#integridAcab"+item.id+"").val();
          cont++;
        });
     
        arrCodigoAcab.push({descripcionA:'N',calArenaA:'',cementoA:'',piedraA:'',marmolA:'',maderaA:'',azulejoA:'',ladrilloA:'',otroAcabA:'',otroDescAcabA:'',integridadAcabA:''});
        arrDibujoAcab.push({scd_codigo:htmlAcab, id: contCodigoAcab});
        contCodigoAcab++;

        cont=1;
        for (var i=0; i<arrDibujoAcab.length;i++)
        {
          cadenaAcab = cadenaAcab + arrDibujoAcab[i].scd_codigo;
          cont++;
        }
      
        $('#crearGrillaAcabado').html(cadenaAcab); 

        var conto=0;
        arrDibujoAcab.forEach(function (item, index, array) 
        { 
          $("#descripcionAcab"+item.id+"").val(arrCodigoAcab[conto].descripcionA); 
          document.getElementById("calArena"+item.id+"").checked = (arrCodigoAcab[conto].calArenaA); 
          document.getElementById("cemento"+item.id+"").checked = (arrCodigoAcab[conto].cementoA);
          document.getElementById("piedra"+item.id+"").checked = (arrCodigoAcab[conto].piedraA);
          document.getElementById("marmol"+item.id+"").checked = (arrCodigoAcab[conto].marmolA);
          document.getElementById("maderamuro"+item.id+"").checked = (arrCodigoAcab[conto].maderaA); 
          document.getElementById("azulejo"+item.id+"").checked = (arrCodigoAcab[conto].azulejoA);
          document.getElementById("ladrillo"+item.id+"").checked = (arrCodigoAcab[conto].ladrilloA);
          document.getElementById("otroAcab"+item.id+"").checked = (arrCodigoAcab[conto].otroAcabA);
          $("#otrosDescAcab"+item.id+"").val(arrCodigoAcab[conto].otroDescAcabA); 
          $("#integridAcab"+item.id+"").val(arrCodigoAcab[conto].integridadAcabA); 

          conto=conto+1;
        });

        ////console.log("arrCodigoAcab",arrCodigoAcab);
    }

     function eliminarGrillaAcabado($id){
        if(contCodigoAcab>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaAcab='';
                var pos=-1;
                var cont = 0;
                arrDibujoAcab.forEach(function (item, index, array) 
                {
                  arrCodigoAcab[cont].descripcionA = $("#descripcionAcab"+item.id+"").val();
                  arrCodigoAcab[cont].calArenaA = document.getElementById("calArena"+item.id+"").checked;
                  arrCodigoAcab[cont].cementoA = document.getElementById("cemento"+item.id+"").checked;
                  arrCodigoAcab[cont].piedraA = document.getElementById("piedra"+item.id+"").checked;
                  arrCodigoAcab[cont].marmolA = document.getElementById("marmol"+item.id+"").checked;
                  arrCodigoAcab[cont].maderaA = document.getElementById("maderamuro"+item.id+"").checked;
                  arrCodigoAcab[cont].azulejoA = document.getElementById("azulejo"+item.id+"").checked;
                  arrCodigoAcab[cont].ladrilloA = document.getElementById("ladrillo"+item.id+"").checked;
                  arrCodigoAcab[cont].otroAcabA = document.getElementById("otroAcab"+item.id+"").checked;
                  arrCodigoAcab[cont].otroDescAcabA = $("#otrosDescAcab"+item.id+"").val();
                  arrCodigoAcab[cont].integridadAcabA = $("#integridAcab"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoAcab.map(function(e) {return e.id;}).indexOf($id);
                arrCodigoAcab.splice(pos,1);
                arrDibujoAcab.splice(pos,1);
             
                  cont=1;
                for (var i=0; i<arrDibujoAcab.length;i++)
                {
                  cadenaAcab = cadenaAcab + arrDibujoAcab[i].scd_codigo;
                  cont++;
                }
                $('#crearGrillaAcabado').html(cadenaAcab);
                
                var conto=0;
                arrDibujoAcab.forEach(function (item, index, array) 
                { 
                  $("#descripcionAcab"+item.id+"").val(arrCodigoAcab[conto].descripcionA); 
                  document.getElementById("calArena"+item.id+"").checked = (arrCodigoAcab[conto].calArenaA); 
                  document.getElementById("cemento"+item.id+"").checked = (arrCodigoAcab[conto].cementoA);
                  document.getElementById("piedra"+item.id+"").checked = (arrCodigoAcab[conto].piedraA);
                  document.getElementById("marmol"+item.id+"").checked = (arrCodigoAcab[conto].marmolA);
                  document.getElementById("maderamuro"+item.id+"").checked = (arrCodigoAcab[conto].maderaA); 
                  document.getElementById("azulejo"+item.id+"").checked = (arrCodigoAcab[conto].azulejoA);
                  document.getElementById("ladrillo"+item.id+"").checked = (arrCodigoAcab[conto].ladrilloA);
                  document.getElementById("otroAcab"+item.id+"").checked = (arrCodigoAcab[conto].otroAcabA);
                  $("#otrosDescAcab"+item.id+"").val(arrCodigoAcab[conto].otroDescAcabA); 
                  $("#integridAcab"+item.id+"").val(arrCodigoAcab[conto].integridadAcabA); 

                  conto=conto+1;
                });
            });
        }
   
    }

//------editar y actualizar acabados muros-------
var arrDibujoAM = [];
var aarrCodigoAcab = [];
var contAMact = 0;

function editarAcabadoM(longitudGrilla){
  var contAM=1;
  arrDibujoAM=[];
  var htmlAM;
  for (var i=1; i<longitudGrilla;i++)
        {
      htmlAM='<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionAM'+contAM+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Revoque Grueso">Revoque Grueso</option>'+
              '<option value="Revoque Fino">Revoque Fino</option>'+
              '<option value="Estructura Aparente">Estructura Aparente</option>'+
              '<option value="Texturizado">Texturizado</option>'+
              '<option value="Enchape">Enchape</option>'+
            '</select> '+
          '</div>'+
           '<div class="form-group col-md-1">'+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArenaAM'+contAM+'"> Cal y Arena<br>'+
          '<input type="checkbox" id="cementoAM'+contAM+'"> Cemento y Arena<br>'+
          '<input type="checkbox" id="piedraAM'+contAM+'"> Piedra<br>'+
          '<input type="checkbox" id="marmolAM'+contAM+'"> Mármol'+
        '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="maderamuroAM'+contAM+'"> Madera<br>'+
          '<input type="checkbox" id="azulejoAM'+contAM+'"> Azulejo<br>'+
          '<input type="checkbox" id="ladrilloAM'+contAM+'"> Ladrillo<br>'+
          '<input type="checkbox" id="otroAcabAM'+contAM+'"> Otros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDescAcab1'+contAM+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridAM'+contAM+'" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
       '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarAcabadoMuro('+contAM+');"></i>'+
       '</a></div>'+
        '</div>';
       arrDibujoAM.push({scd_codigo:htmlAM,id: contAM});
        contAM++;
        }
        var cadenaAM='';
        var cont=1;
        for (var i=0; i<arrDibujoAM.length;i++)
        {
          cadenaAM=cadenaAM + arrDibujoAM[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaAcabado').html(cadenaAM);
  }


function actualizarAcabadoMuro(){
    var htmlAcabAct;
        htmlAcabAct = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionAM'+contAMact+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Revoque Grueso">Revoque Grueso</option>'+
              '<option value="Revoque Fino">Revoque Fino</option>'+
              '<option value="Estructura Aparente">Estructura Aparente</option>'+
              '<option value="Texturizado">Texturizado</option>'+
              '<option value="Enchape">Enchape</option>'+
            '</select> '+
          '</div>'+
           '<div class="form-group col-md-1">'+
          '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="calArenaAM'+contAMact+'"> Cal y Arena<br>'+
          '<input type="checkbox" id="cementoAM'+contAMact+'"> Cemento y Arena<br>'+
          '<input type="checkbox" id="piedraAM'+contAMact+'"> Piedra<br>'+
          '<input type="checkbox" id="marmolAM'+contAMact+'"> Mármol'+
        '</div>'+
        '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="maderamuroAM'+contAMact+'"> Madera<br>'+
          '<input type="checkbox" id="azulejoAM'+contAMact+'"> Azulejo<br>'+
          '<input type="checkbox" id="ladrilloAM'+contAMact+'"> Ladrillo<br>'+
          '<input type="checkbox" id="otroAcabAM'+contAMact+'"> Otros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDescAcab1'+contAMact+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridAM'+contAMact+'" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
       '<div class="col-md-1">'+
       '<a style="cursor:pointer;" type="button">'+
       '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar registro" onclick="eliminarAcabadoMuro('+contAMact+');"></i>'+
       '</a></div>'+
        '</div>';
        var cadenaAcab = '';
        var cont = 0;
        arrDibujoAM.forEach(function (item, index, array) 
        {
          aarrCodigoAcab[cont].adescripcionA = $("#descripcionAM"+item.id+"").val();
          aarrCodigoAcab[cont].acalArenaA = document.getElementById("calArenaAM"+item.id+"").checked;
          aarrCodigoAcab[cont].acementoA = document.getElementById("cementoAM"+item.id+"").checked;
          aarrCodigoAcab[cont].apiedraA = document.getElementById("piedraAM"+item.id+"").checked;
          aarrCodigoAcab[cont].amarmolA = document.getElementById("marmolAM"+item.id+"").checked;
          aarrCodigoAcab[cont].amaderaA = document.getElementById("maderamuroAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aazulejoA = document.getElementById("azulejoAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aladrilloA = document.getElementById("ladrilloAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aotroAcabA = document.getElementById("otroAcabAM"+item.id+"").checked;
          aarrCodigoAcab[cont].aotroDescAcabA = $("#otrosDescAcab1"+item.id+"").val();
          aarrCodigoAcab[cont].aintegridadAcabA = $("#integridAM"+item.id+"").val();
          cont++;
        });
        aarrCodigoAcab.push({adescripcionA:'N',acalArenaA:'',acementoA:'',apiedraA:'',amarmolA:'',amaderaA:'',aazulejoA:'',aladrilloA:'',aotroAcabA:'',aotroDescAcabA:'',aintegridadAcabA:''});
        arrDibujoAM.push({scd_codigo:htmlAcabAct, id: contAMact});
        contAMact++;
        cont=1;
        for (var i=0; i<arrDibujoAM.length;i++)
        {
          cadenaAcab = cadenaAcab + arrDibujoAM[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaAcabado').html(cadenaAcab); 
        var conto=0;
        arrDibujoAM.forEach(function (item, index, array) 
        { 
          $("#descripcionAM"+item.id+"").val(aarrCodigoAcab[conto].adescripcionA); 
          document.getElementById("calArenaAM"+item.id+"").checked = (aarrCodigoAcab[conto].acalArenaA); 
          document.getElementById("cementoAM"+item.id+"").checked = (aarrCodigoAcab[conto].acementoA);
          document.getElementById("piedraAM"+item.id+"").checked = (aarrCodigoAcab[conto].apiedraA);
          document.getElementById("marmolAM"+item.id+"").checked = (aarrCodigoAcab[conto].amarmolA);
          document.getElementById("maderamuroAM"+item.id+"").checked = (aarrCodigoAcab[conto].amaderaA); 
          document.getElementById("azulejoAM"+item.id+"").checked = (aarrCodigoAcab[conto].aazulejoA);
          document.getElementById("ladrilloAM"+item.id+"").checked = (aarrCodigoAcab[conto].aladrilloA);
          document.getElementById("otroAcabAM"+item.id+"").checked = (aarrCodigoAcab[conto].aotroAcabA);
          $("#otrosDescAcab1"+item.id+"").val(aarrCodigoAcab[conto].aotroDescAcabA); 
          $("#integridAM"+item.id+"").val(aarrCodigoAcab[conto].aintegridadAcabA); 
          conto=conto+1;
        });

    }

     function eliminarAcabadoMuro($id){
        if(contAMact>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaAcab='';
                var pos=-1;
                var cont = 0;
                arrDibujoAM.forEach(function (item, index, array) 
                {
                  aarrCodigoAcab[cont].adescripcionA = $("#descripcionAM"+item.id+"").val();
                  aarrCodigoAcab[cont].acalArenaA = document.getElementById("calArenaAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].acementoA = document.getElementById("cementoAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].apiedraA = document.getElementById("piedraAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].amarmolA = document.getElementById("marmolAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].amaderaA = document.getElementById("maderamuroAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].aazulejoA = document.getElementById("azulejoAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].aladrilloA = document.getElementById("ladrilloAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].aotroAcabA = document.getElementById("otroAcabAM"+item.id+"").checked;
                  aarrCodigoAcab[cont].aotroDescAcabA = $("#otrosDescAcab1"+item.id+"").val();
                  aarrCodigoAcab[cont].aintegridadAcabA = $("#integridAM"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoAM.map(function(e) {return e.id;}).indexOf($id);
                aarrCodigoAcab.splice(pos,1);
                arrDibujoAM.splice(pos,1);
             
                  cont=1;
                for (var i=0; i<arrDibujoAM.length;i++)
                {
                  cadenaAcab = cadenaAcab + arrDibujoAM[i].scd_codigo;
                  cont++;
                }
                $('#acrearGrillaAcabado').html(cadenaAcab);
                var conto=0;
                arrDibujoAM.forEach(function (item, index, array) 
                { 
                  $("#descripcionAM"+item.id+"").val(aarrCodigoAcab[conto].adescripcionA); 
                  document.getElementById("calArenaAM"+item.id+"").checked = (aarrCodigoAcab[conto].acalArenaA); 
                  document.getElementById("cementoAM"+item.id+"").checked = (aarrCodigoAcab[conto].acementoA);
                  document.getElementById("piedraAM"+item.id+"").checked = (aarrCodigoAcab[conto].apiedraA);
                  document.getElementById("marmolAM"+item.id+"").checked = (aarrCodigoAcab[conto].amarmolA);
                  document.getElementById("maderamuroAM"+item.id+"").checked = (aarrCodigoAcab[conto].amaderaA); 
                  document.getElementById("azulejoAM"+item.id+"").checked = (aarrCodigoAcab[conto].aazulejoA);
                  document.getElementById("ladrilloAM"+item.id+"").checked = (aarrCodigoAcab[conto].aladrilloA);
                  document.getElementById("otroAcabAM"+item.id+"").checked = (aarrCodigoAcab[conto].aotroAcabA);
                  $("#otrosDescAcab1"+item.id+"").val(aarrCodigoAcab[conto].aotroDescAcabA); 
                  $("#integridAM"+item.id+"").val(aarrCodigoAcab[conto].aintegridadAcabA); 
                  conto=conto+1;
                });
            });
        }
   
    }



    /////BEGIN GRILLA PINTURAS////
    var contCodigoPinturas = 2;
    var arrCodigoPinturas = [];
    var arrDibujoPinturas = [];


        htmlPinturas='<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionPintu1" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Policromia">Policromia</option>'+
              '<option value="Bicromia">Bicromia</option>'+
              '<option value="Monocromia">Monocromia</option>'+
            '</select> '+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '</div>'+
        '<div class="form-group col-md-3">'+
          '<input type="checkbox" id="calArenaPintu1"> Cal<br>'+
          '<input type="checkbox" id="acrilicoPintu1"> Acrilica<br>'+
          '<input type="checkbox" id="aceitePintu1"> Aceite<br>'+
          '<input type="checkbox" id="otroPintu1"> Otros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDescPintu1" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridPintu1" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
        '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPintura(1);"></i>'+
      '</a></div>'+
        '</div>';

    var cadenaPinturas = '';  
    arrCodigoPinturas.push({descripcionPintu:'N',calArenaPintu:'',acrilicoPintu:'',aceitePintu:'',otroPintu:'',otrosDescPintu:'',integridPintu:''});
    arrDibujoPinturas.push({scd_codigo:htmlPinturas,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaPinturas=arrDibujoPinturas[0].scd_codigo;
   // //console.log("cadenaAcab",cadenaAcab);
    $('#crearGrillaPinturas').html(cadenaPinturas); 
    $("#descripcionPintu1").val(arrCodigoPinturas[0].descripcionPintu);
    document.getElementById("calArenaPintu1").checked = (arrCodigoPinturas[0].calArenaPintu); 
    document.getElementById("acrilicoPintu1").checked = (arrCodigoPinturas[0].acrilicoPintu); 
    document.getElementById("aceitePintu1").checked = (arrCodigoPinturas[0].aceitePintu);
    document.getElementById("otroPintu1").checked = (arrCodigoPinturas[0].otroPintu);
    $("#otrosDescPintu1").val(arrCodigoPinturas[0].otrosDescPintu); 
    $("#integridPintu1").val(arrCodigoPinturas[0].integridPintu);

    function grillaPinturas(){

    var htmlPinturas;
        htmlPinturas='<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionPintu'+contCodigoPinturas+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Policromia">Policromia</option>'+
              '<option value="Bicromia">Bicromia</option>'+
              '<option value="Monocromia">Monocromia</option>'+
            '</select> '+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '</div>'+
        '<div class="form-group col-md-3">'+
          '<input type="checkbox" id="calArenaPintu'+contCodigoPinturas+'"> Cal<br>'+
          '<input type="checkbox" id="acrilicoPintu'+contCodigoPinturas+'"> Acrilica<br>'+
          '<input type="checkbox" id="aceitePintu'+contCodigoPinturas+'"> Aceite<br>'+
          '<input type="checkbox" id="otroPintu'+contCodigoPinturas+'"> Otros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDescPintu'+contCodigoPinturas+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridPintu'+contCodigoPinturas+'" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
          '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPintura('+contCodigoPinturas+');"></i>'+
      '</a></div>'+
        '</div>';

        var cadenaPinturas = '';
        var cont = 0;
        arrDibujoPinturas.forEach(function (item, index, array) 
        {
          arrCodigoPinturas[cont].descripcionPintu = $("#descripcionPintu"+item.id+"").val();
          arrCodigoPinturas[cont].calArenaPintu = document.getElementById("calArenaPintu"+item.id+"").checked;
          arrCodigoPinturas[cont].acrilicoPintu = document.getElementById("acrilicoPintu"+item.id+"").checked;
          arrCodigoPinturas[cont].aceitePintu = document.getElementById("aceitePintu"+item.id+"").checked;
          arrCodigoPinturas[cont].otroPintu = document.getElementById("otroPintu"+item.id+"").checked;
          arrCodigoPinturas[cont].otrosDescPintu = $("#otrosDescPintu"+item.id+"").val();
          arrCodigoPinturas[cont].integridPintu = $("#integridPintu"+item.id+"").val();
          cont++;
        });
     
        arrCodigoPinturas.push({descripcionPintu:'N',calArenaPintu:'',acrilicoPintu:'',aceitePintu:'',otroPintu:'',otrosDescPintu:'',integridPintu:''});
        arrDibujoPinturas.push({scd_codigo:htmlPinturas, id: contCodigoPinturas});
        contCodigoPinturas++;

        cont=1;
        for (var i=0; i<arrDibujoPinturas.length;i++)
        {
          cadenaPinturas = cadenaPinturas + arrDibujoPinturas[i].scd_codigo;
          cont++;
        }
      
        $('#crearGrillaPinturas').html(cadenaPinturas); 

        var conto=0;
        arrDibujoPinturas.forEach(function (item, index, array) 
        { 
          $("#descripcionPintu"+item.id+"").val(arrCodigoPinturas[conto].descripcionPintu);
          document.getElementById("calArenaPintu"+item.id+"").checked = (arrCodigoPinturas[conto].calArenaPintu); 
          document.getElementById("acrilicoPintu"+item.id+"").checked = (arrCodigoPinturas[conto].acrilicoPintu); 
          document.getElementById("aceitePintu"+item.id+"").checked = (arrCodigoPinturas[conto].aceitePintu);
          document.getElementById("otroPintu"+item.id+"").checked = (arrCodigoPinturas[conto].otroPintu);
          $("#otrosDescPintu"+item.id+"").val(arrCodigoPinturas[conto].otrosDescPintu); 
          $("#integridPintu"+item.id+"").val(arrCodigoPinturas[conto].integridPintu);

          conto=conto+1;
        });

    }

     function eliminarGrillaPintura($id){
        if(contCodigoPinturas>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaPinturas='';
                var pos=-1;
                 var cont = 0;
                arrDibujoPinturas.forEach(function (item, index, array) 
                {
                  arrCodigoPinturas[cont].descripcionPintu = $("#descripcionPintu"+item.id+"").val();
                  arrCodigoPinturas[cont].calArenaPintu = document.getElementById("calArenaPintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].acrilicoPintu = document.getElementById("acrilicoPintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].aceitePintu = document.getElementById("aceitePintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].otroPintu = document.getElementById("otroPintu"+item.id+"").checked;
                  arrCodigoPinturas[cont].otrosDescPintu = $("#otrosDescPintu"+item.id+"").val();
                  arrCodigoPinturas[cont].integridPintu = $("#integridPintu"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoPinturas.map(function(e) {return e.id;}).indexOf($id);
            
                arrCodigoPinturas.splice(pos,1);
                arrDibujoPinturas.splice(pos,1);  
               cont=1;
                for (var i=0; i<arrDibujoPinturas.length;i++)
                {
                  cadenaPinturas = cadenaPinturas + arrDibujoPinturas[i].scd_codigo;
                  cont++;
                }
              
                $('#crearGrillaPinturas').html(cadenaPinturas); 
                 var conto=0;
                arrDibujoPinturas.forEach(function (item, index, array) 
                { 
                  $("#descripcionPintu"+item.id+"").val(arrCodigoPinturas[conto].descripcionPintu);
                  document.getElementById("calArenaPintu"+item.id+"").checked = (arrCodigoPinturas[conto].calArenaPintu); 
                  document.getElementById("acrilicoPintu"+item.id+"").checked = (arrCodigoPinturas[conto].acrilicoPintu); 
                  document.getElementById("aceitePintu"+item.id+"").checked = (arrCodigoPinturas[conto].aceitePintu);
                  document.getElementById("otroPintu"+item.id+"").checked = (arrCodigoPinturas[conto].otroPintu);
                  $("#otrosDescPintu"+item.id+"").val(arrCodigoPinturas[conto].otrosDescPintu); 
                  $("#integridPintu"+item.id+"").val(arrCodigoPinturas[conto].integridPintu);

                  conto=conto+1;
                });
            });
        }
    }


var arrDibujoPIN=[];
var aarrCodigoPinturas = [];
var contPact = 0;

  function editarGrillaPinturas(longitudGrilla){
  var contPin=1;
  arrDibujoPIN=[];
  var htmlPIN;
  for (var i=1; i<longitudGrilla;i++)
        {
      htmlPIN = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
            '<select id="descripcionPIN'+contPin+'" class="form-control">'+
              '<option value="N" selected="selected">..Seleccionar..</option>'+
              '<option value="Policromia">Policromia</option>'+
              '<option value="Bicromia">Bicromia</option>'+
              '<option value="Monocromia">Monocromia</option>'+
            '</select> '+
          '</div>'+
         '<div class="form-group col-md-2">'+
        '</div>'+
        '<div class="form-group col-md-3">'+
          '<input type="checkbox" id="calArenaPIN'+contPin+'"> Cal<br>'+
          '<input type="checkbox" id="acrilicoPIN'+contPin+'"> Acrilica<br>'+
          '<input type="checkbox" id="aceitePIN'+contPin+'"> Aceite<br>'+
          '<input type="checkbox" id="otroPIN'+contPin+'"> Otros'+
        '</div>'+
        '<div class="form-group col-md-2">'+
        '<input type="text" id="otrosDescPIN'+contPin+'" class="form-control"  placeholder="Otras Descrip.">'+
        '</div>'+
           '<div class="form-group col-md-2">'+
            '<select id="integridPIN'+contPin+'" class="form-control">'+
              '<option value="N" selected="selected">....Seleccionar...</option>'+
              '<option value="Original">Original</option>'+
              '<option value="Nuevo">Nuevo</option>'+
              '<option value="Modificado">Modificado</option>'+
              '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
          '</div>'+
        '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPintura('+contPin+');"></i>'+
      '</a></div>'+
        '</div>';
       arrDibujoPIN.push({scd_codigo:htmlPIN,id: contPin});
        contPin++;
        }
        var cadenaPIN='';
        var cont=1;
        for (var i=0; i<arrDibujoPIN.length;i++)
        {
          cadenaPIN=cadenaPIN + arrDibujoPIN[i].scd_codigo;
          cont++;
        }

      $('#acrearGrillaPinturas').html(cadenaPIN);
  }

function actualizarPinturas(){
    var htmlPinturas;
        htmlPinturas='<div class="col-md-12">'+
            '<div class="form-group col-md-2">'+
              '<select id="descripcionPIN'+contPact+'" class="form-control">'+
                '<option value="N" selected="selected">..Seleccionar..</option>'+
                '<option value="Policromia">Policromia</option>'+
                '<option value="Bicromia">Bicromia</option>'+
                '<option value="Monocromia">Monocromia</option>'+
              '</select> '+
            '</div>'+
          '<div class="form-group col-md-2">'+
          '</div>'+
          '<div class="form-group col-md-3">'+
            '<input type="checkbox" id="calArenaPIN'+contPact+'"> Cal<br>'+
            '<input type="checkbox" id="acrilicoPIN'+contPact+'"> Acrilica<br>'+
            '<input type="checkbox" id="aceitePIN'+contPact+'"> Aceite<br>'+
            '<input type="checkbox" id="otroPIN'+contPact+'"> Otros'+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '<input type="text" id="otrosDescPIN'+contPact+'" class="form-control"  placeholder="Otras Descrip.">'+
          '</div>'+
             '<div class="form-group col-md-2">'+
              '<select id="integridPIN'+contPact+'" class="form-control">'+
                '<option value="N" selected="selected">....Seleccionar...</option>'+
                '<option value="Original">Original</option>'+
                '<option value="Nuevo">Nuevo</option>'+
                '<option value="Modificado">Modificado</option>'+
                '<option value="Original y Nuevo">Original y Nuevo</option>'+
              '</select> '+
            '</div>'+
          '<div class="col-md-1">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPintura('+contPact+');"></i>'+
        '</a></div>'+
          '</div>';
        var cadenaPinturas = '';
        var cont = 0;
        arrDibujoPIN.forEach(function (item, index, array) 
        {
          aarrCodigoPinturas[cont].adescripcionPintu = $("#descripcionPIN"+item.id+"").val();
          aarrCodigoPinturas[cont].acalArenaPintu = document.getElementById("calArenaPIN"+item.id+"").checked;
          aarrCodigoPinturas[cont].aacrilicoPintu = document.getElementById("acrilicoPIN"+item.id+"").checked;
          aarrCodigoPinturas[cont].aaceitePintu = document.getElementById("aceitePIN"+item.id+"").checked;
          aarrCodigoPinturas[cont].aotroPintu = document.getElementById("otroPIN"+item.id+"").checked;
          aarrCodigoPinturas[cont].aotrosDescPintu = $("#otrosDescPIN"+item.id+"").val();
          aarrCodigoPinturas[cont].aintegridPintu = $("#integridPIN"+item.id+"").val();
          cont++;
        });
        aarrCodigoPinturas.push({adescripcionPintu:'N',acalArenaPintu:'',aacrilicoPintu:'',aaceitePintu:'',aotroPintu:'',aotrosDescPintu:'',aintegridPintu:''});
        arrDibujoPIN.push({scd_codigo:htmlPinturas, id: contPact});
        contPact++;
        cont=1;
        for (var i=0; i<arrDibujoPIN.length;i++)
        {
          cadenaPinturas = cadenaPinturas + arrDibujoPIN[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaPinturas').html(cadenaPinturas); 
        var conto=0;
        arrDibujoPIN.forEach(function (item, index, array) 
        { 
          $("#descripcionPIN"+item.id+"").val(aarrCodigoPinturas[conto].adescripcionPintu);
          document.getElementById("calArenaPIN"+item.id+"").checked = (aarrCodigoPinturas[conto].acalArenaPintu); 
          document.getElementById("acrilicoPIN"+item.id+"").checked = (aarrCodigoPinturas[conto].aacrilicoPintu); 
          document.getElementById("aceitePIN"+item.id+"").checked = (aarrCodigoPinturas[conto].aaceitePintu);
          document.getElementById("otroPIN"+item.id+"").checked = (aarrCodigoPinturas[conto].aotroPintu);
          $("#otrosDescPIN"+item.id+"").val(aarrCodigoPinturas[conto].aotrosDescPintu); 
          $("#integridPIN"+item.id+"").val(aarrCodigoPinturas[conto].aintegridPintu);
          conto=conto+1;
        });

    }


     function eliminarPintura($id){
        if(contPact>2)
        {
            swal({title: "Esta seguro de eliminar?",
                text: "Se eliminará el argumento seleccionado!",
                type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
            }, function(){
                var cadenaPinturas='';
                var pos=-1;
                 var cont = 0;
                arrDibujoPIN.forEach(function (item, index, array) 
                {
                  aarrCodigoPinturas[cont].adescripcionPintu = $("#descripcionPIN"+item.id+"").val();
                  aarrCodigoPinturas[cont].acalArenaPintu = document.getElementById("calArenaPIN"+item.id+"").checked;
                  aarrCodigoPinturas[cont].aacrilicoPintu = document.getElementById("acrilicoPIN"+item.id+"").checked;
                  aarrCodigoPinturas[cont].aaceitePintu = document.getElementById("aceitePIN"+item.id+"").checked;
                  aarrCodigoPinturas[cont].aotroPintu = document.getElementById("otroPIN"+item.id+"").checked;
                  aarrCodigoPinturas[cont].aotrosDescPintu = $("#otrosDescPIN"+item.id+"").val();
                  aarrCodigoPinturas[cont].aintegridPintu = $("#integridPIN"+item.id+"").val();
                  cont++;
                });
                pos = arrDibujoPIN.map(function(e) {return e.id;}).indexOf($id);
            
                aarrCodigoPinturas.splice(pos,1);
                arrDibujoPIN.splice(pos,1);  
               cont=1;
                for (var i=0; i<arrDibujoPIN.length;i++)
                {
                  cadenaPinturas = cadenaPinturas + arrDibujoPIN[i].scd_codigo;
                  cont++;
                }
              
                $('#acrearGrillaPinturas').html(cadenaPinturas); 
                 var conto=0;
                arrDibujoPIN.forEach(function (item, index, array) 
                { 
                  $("#descripcionPIN"+item.id+"").val(aarrCodigoPinturas[conto].adescripcionPintu);
                  document.getElementById("calArenaPIN"+item.id+"").checked = (aarrCodigoPinturas[conto].acalArenaPintu); 
                  document.getElementById("acrilicoPIN"+item.id+"").checked = (aarrCodigoPinturas[conto].aacrilicoPintu); 
                  document.getElementById("aceitePIN"+item.id+"").checked = (aarrCodigoPinturas[conto].aaceitePintu);
                  document.getElementById("otroPIN"+item.id+"").checked = (aarrCodigoPinturas[conto].aotroPintu);
                  $("#otrosDescPIN"+item.id+"").val(aarrCodigoPinturas[conto].aotrosDescPintu); 
                  $("#integridPIN"+item.id+"").val(aarrCodigoPinturas[conto].aintegridPintu);
                  conto=conto+1;
                });
            });
        }
   
    }



    /////FIN GRILLA PINTURAS////

    //--------------------GRILLA PISO INICIO--------------------//

var contCodigoPiso = 2;
var arrCodigoPiso = [];
var arrDibujoPiso = [];
htmlPiso='<div class="col-md-12">'+
'<div class="form-group col-md-2">'+
'<select id="descripcionPiso1" class="form-control">'+
'<option value="N" selected="selected">..Seleccionar..</option>'+
'<option value="Interior">Interior</option>'+
'<option value="Aceras">Aceras</option>'+
'</select> '+
'</div>'+
'<div class="form-group col-md-1">'+
'</div>'+
'<div class="form-group col-md-2">'+
'<input type="checkbox" id="baldosasPiso1">Baldosas<br>'+
'<input type="checkbox" id="mosaicosPiso1">Mosaicos<br>'+
'<input type="checkbox" id="porcelanatoPiso1"> Porcelanato<br>'+
'<input type="checkbox" id="plajaPiso1"> P.Laja<br>'+
'<input type="checkbox" id="prioPiso1"> P.Rio<br>'+
'<input type="checkbox" id="ladrillosPiso1"> Ladrillos<br>'+
'</div>'+
'<div class="form-group col-md-2">'+
'<input type="checkbox" id="gramaPiso1"> Grama<br>'+
'<input type="checkbox" id="tierraPiso1"> Tierra<br>'+
'<input type="checkbox" id="concretoPiso1"> Concreto<br>'+
'<input type="checkbox" id="ceramicaPiso1"> Ceramica<br>'+
'<input type="checkbox" id="maderaPiso1"> Madera<br>'+
'<input type="checkbox" id="otrosPiso1"> Otros'+
'</div>'+
'<div class="form-group col-md-2">'+
'<input type="text" id="otrosDescPiso1" class="form-control"  placeholder="Otras Descrip.">'+
'</div>'+
'<div class="form-group col-md-2">'+
'<select id="integridPiso1" class="form-control">'+
'<option value="N" selected="selected">..Seleccionar..</option>'+
'<option value="Original">Original</option>'+
'<option value="Nuevo">Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'<option value="Original y Nuevo">Original y Nuevo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPiso(1);"></i>'+
'</a></div>'+
'</div>';

var cadenaPiso = '';  
arrCodigoPiso.push({descripcionPisoA:'N', baldosasPisoA: '', mosaicosPisoA: '', porcelanatoPisoA: '', plajaPisoA: '', prioPisoA: '', ladrillosPisoA: '', gramaPisoA: '', tierraPisoA: '', concretoPisoA: '', ceramicaPisoA: '', maderaPisoA: '',otrosPisoA: '', otroDescPisoA:'', integridadPisoA:''});
arrDibujoPiso.push({scd_codigo:htmlPiso,id: 1});

cadenaPiso=arrDibujoPiso[0].scd_codigo;
console.log("cadenaPiso",cadenaPiso);
$('#crearGrillaPisoado').html(cadenaPiso);
$("#descripcionPiso1").val(arrCodigoPiso[0].descripcionPisoA); 
document.getElementById("baldosasPiso1").checked = (arrCodigoPiso[0].baldosasPisoA); 
document.getElementById("mosaicosPiso1").checked = (arrCodigoPiso[0].mosaicosPisoA);
document.getElementById("porcelanatoPiso1").checked = (arrCodigoPiso[0].porcelanatoPisoA);
document.getElementById("plajaPiso1").checked = (arrCodigoPiso[0].plajaPisoA);
document.getElementById("prioPiso1").checked = (arrCodigoPiso[0].prioPisoA); 
document.getElementById("ladrillosPiso1").checked = (arrCodigoPiso[0].ladrillosPisoA);
document.getElementById("gramaPiso1").checked = (arrCodigoPiso[0].gramaPisoA);
document.getElementById("tierraPiso1").checked = (arrCodigoPiso[0].tierraPisoA);
document.getElementById("concretoPiso1").checked = (arrCodigoPiso[0].concretoPisoA); 
document.getElementById("ceramicaPiso1").checked = (arrCodigoPiso[0].ceramicaPisoA);
document.getElementById("maderaPiso1").checked = (arrCodigoPiso[0].maderaPisoA);
document.getElementById("otrosPiso1").checked = (arrCodigoPiso[0].otrosPisoA);
$("#otrosDescPiso1").val(arrCodigoPiso[0].otroDescPisoA); 
$("#integridPiso1").val(arrCodigoPiso[0].integridadPisoA);

function grillaPiso(){
  var htmlPiso;
  htmlPiso='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionPiso'+contCodigoPiso+'" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="Interior">Interior</option>'+
  '<option value="Aceras">Aceras</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-1">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="checkbox" id="baldosasPiso'+contCodigoPiso+'">Baldosas<br>'+
  '<input type="checkbox" id="mosaicosPiso'+contCodigoPiso+'">Mosaicos<br>'+
  '<input type="checkbox" id="porcelanatoPiso'+contCodigoPiso+'"> Porcelanato<br>'+
  '<input type="checkbox" id="plajaPiso'+contCodigoPiso+'"> P.Laja<br>'+
  '<input type="checkbox" id="prioPiso'+contCodigoPiso+'"> P.Rio<br>'+
  '<input type="checkbox" id="ladrillosPiso'+contCodigoPiso+'"> Ladrillos<br>'+
  '</div>'+
  '<div class="form-group col-md-2">'+

  '<input type="checkbox" id="gramaPiso'+contCodigoPiso+'"> Grama<br>'+
  '<input type="checkbox" id="tierraPiso'+contCodigoPiso+'"> Tierra<br>'+
  '<input type="checkbox" id="concretoPiso'+contCodigoPiso+'"> Concreto<br>'+
  '<input type="checkbox" id="ceramicaPiso'+contCodigoPiso+'"> Ceramica<br>'+
  '<input type="checkbox" id="maderaPiso'+contCodigoPiso+'"> Madera<br>'+
  '<input type="checkbox" id="otrosPiso'+contCodigoPiso+'"> Otros'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="otrosDescPiso'+contCodigoPiso+'" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridPiso'+contCodigoPiso+'" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="Original">Original</option>'+
  '<option value="Nuevo">Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '<option value="Original y Nuevo">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPiso('+contCodigoPiso+');"></i>'+
  '</a></div>'+
  '</div>';
  var cadenaPiso = '';
  var cont = 0;
  arrDibujoPiso.forEach(function (item, index, array){
    arrCodigoPiso[cont].descripcionPisoA = $("#descripcionPiso"+item.id+"").val();     
    arrCodigoPiso[cont].baldosasPisoA = document.getElementById("baldosasPiso"+item.id+"").checked; 
    arrCodigoPiso[cont].mosaicosPisoA = document.getElementById("mosaicosPiso"+item.id+"").checked;
    arrCodigoPiso[cont].porcelanatoPisoA = document.getElementById("porcelanatoPiso"+item.id+"").checked;
    arrCodigoPiso[cont].plajaPisoA = document.getElementById("plajaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].prioPisoA = document.getElementById("prioPiso"+item.id+"").checked; 
    arrCodigoPiso[cont].ladrillosPisoA = document.getElementById("ladrillosPiso"+item.id+"").checked;
    arrCodigoPiso[cont].gramaPisoA = document.getElementById("gramaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].tierraPisoA = document.getElementById("tierraPiso"+item.id+"").checked;
    arrCodigoPiso[cont].concretoPisoA = document.getElementById("concretoPiso"+item.id+"").checked; 
    arrCodigoPiso[cont].ceramicaPisoA = document.getElementById("ceramicaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].maderaPisoA = document.getElementById("maderaPiso"+item.id+"").checked;
    arrCodigoPiso[cont].otrosPisoA = document.getElementById("otrosPiso"+item.id+"").checked;
    arrCodigoPiso[cont].otroDescPisoA = $("#otrosDescPiso"+item.id+"").val();
    arrCodigoPiso[cont].integridadPisoA = $("#integridPiso"+item.id+"").val();
    cont++;
  });
  arrCodigoPiso.push({descripcionPisoA:'N', baldosasPisoA: '', mosaicosPisoA: '', porcelanatoPisoA: '', plajaPisoA: '', prioPisoA: '', ladrillosPisoA: '', gramaPisoA: '', tierraPisoA: '', concretoPisoA: '', ceramicaPisoA: '', maderaPisoA: '',otrosPisoA: '', otroDescPisoA:'', integridadPisoA:''});
  arrDibujoPiso.push({scd_codigo:htmlPiso, id: contCodigoPiso});
  contCodigoPiso++;
  cont=1;
  for (var i=0; i<arrDibujoPiso.length;i++){
    cadenaPiso = cadenaPiso + arrDibujoPiso[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaPisoado').html(cadenaPiso); 
  var conto=0;
  arrDibujoPiso.forEach(function (item, index, array) { 
    $("#descripcionPiso"+item.id+"").val(arrCodigoPiso[conto].descripcionPisoA); 
    document.getElementById("baldosasPiso"+item.id+"").checked = (arrCodigoPiso[conto].baldosasPisoA); 
    document.getElementById("mosaicosPiso"+item.id+"").checked = (arrCodigoPiso[conto].mosaicosPisoA);
    document.getElementById("porcelanatoPiso"+item.id+"").checked = (arrCodigoPiso[conto].porcelanatoPisoA);
    document.getElementById("plajaPiso"+item.id+"").checked = (arrCodigoPiso[conto].plajaPisoA);
    document.getElementById("prioPiso"+item.id+"").checked = (arrCodigoPiso[conto].prioPisoA); 
    document.getElementById("ladrillosPiso"+item.id+"").checked = (arrCodigoPiso[conto].ladrillosPisoA);
    document.getElementById("gramaPiso"+item.id+"").checked = (arrCodigoPiso[conto].gramaPisoA);
    document.getElementById("tierraPiso"+item.id+"").checked = (arrCodigoPiso[conto].tierraPisoA);
    document.getElementById("concretoPiso"+item.id+"").checked = (arrCodigoPiso[conto].concretoPisoA); 
    document.getElementById("ceramicaPiso"+item.id+"").checked = (arrCodigoPiso[conto].ceramicaPisoA);
    document.getElementById("maderaPiso"+item.id+"").checked = (arrCodigoPiso[conto].maderaPisoA);
    document.getElementById("otrosPiso"+item.id+"").checked = (arrCodigoPiso[conto].otrosPisoA);
    $("#otrosDescPiso"+item.id+"").val(arrCodigoPiso[conto].otroDescPisoA); 
    $("#integridPiso"+item.id+"").val(arrCodigoPiso[conto].integridadPisoA); 
    conto=conto+1;
  });
}

function eliminarGrillaPiso($id){
  if(contCodigoPiso>2){
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaPiso ='';
      var pos=-1;
      var cont = 0;
      arrDibujoPiso.forEach(function (item, index, array) {
        arrCodigoPiso[cont].descripcionPisoA = $("#descripcionPiso"+item.id+"").val();     
        arrCodigoPiso[cont].baldosasPisoA = document.getElementById("baldosasPiso"+item.id+"").checked; 
        arrCodigoPiso[cont].mosaicosPisoA = document.getElementById("mosaicosPiso"+item.id+"").checked;
        arrCodigoPiso[cont].porcelanatoPisoA = document.getElementById("porcelanatoPiso"+item.id+"").checked;
        arrCodigoPiso[cont].plajaPisoA = document.getElementById("plajaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].prioPisoA = document.getElementById("prioPiso"+item.id+"").checked; 
        arrCodigoPiso[cont].ladrillosPisoA = document.getElementById("ladrillosPiso"+item.id+"").checked;
        arrCodigoPiso[cont].gramaPisoA = document.getElementById("gramaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].tierraPisoA = document.getElementById("tierraPiso"+item.id+"").checked;
        arrCodigoPiso[cont].concretoPisoA = document.getElementById("concretoPiso"+item.id+"").checked; 
        arrCodigoPiso[cont].ceramicaPisoA = document.getElementById("ceramicaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].maderaPisoA = document.getElementById("maderaPiso"+item.id+"").checked;
        arrCodigoPiso[cont].otrosPisoA = document.getElementById("otrosPiso"+item.id+"").checked;
        arrCodigoPiso[cont].otroDescPisoA = $("#otrosDescPiso"+item.id+"").val();
        arrCodigoPiso[cont].integridadPisoA = $("#integridPiso"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoPiso.map(function(e) {return e.id;}).indexOf($id);
      //console.log("poos",pos);
      arrCodigoPiso.splice(pos,1);
      arrDibujoPiso.splice(pos,1);
      //console.log("arrCodigo",arrCodigoPiso);
      cont=1;
      for (var i=0; i<arrDibujoPiso.length;i++) {
        cadenaPiso = cadenaPiso + arrDibujoPiso[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaPisoado').html(cadenaPiso);
      var conto=0;
      arrDibujoPiso.forEach(function (item, index, array) { 
        $("#descripcionPiso"+item.id+"").val(arrCodigoPiso[conto].descripcionPisoA); 
        document.getElementById("baldosasPiso"+item.id+"").checked = (arrCodigoPiso[conto].baldosasPisoA); 
        document.getElementById("mosaicosPiso"+item.id+"").checked = (arrCodigoPiso[conto].mosaicosPisoA);
        document.getElementById("porcelanatoPiso"+item.id+"").checked = (arrCodigoPiso[conto].porcelanatoPisoA);
        document.getElementById("plajaPiso"+item.id+"").checked = (arrCodigoPiso[conto].plajaPisoA);
        document.getElementById("prioPiso"+item.id+"").checked = (arrCodigoPiso[conto].prioPisoA); 
        document.getElementById("ladrillosPiso"+item.id+"").checked = (arrCodigoPiso[conto].ladrillosPisoA);
        document.getElementById("gramaPiso"+item.id+"").checked = (arrCodigoPiso[conto].gramaPisoA);
        document.getElementById("tierraPiso"+item.id+"").checked = (arrCodigoPiso[conto].tierraPisoA);
        document.getElementById("concretoPiso"+item.id+"").checked = (arrCodigoPiso[conto].concretoPisoA); 
        document.getElementById("ceramicaPiso"+item.id+"").checked = (arrCodigoPiso[conto].ceramicaPisoA);
        document.getElementById("maderaPiso"+item.id+"").checked = (arrCodigoPiso[conto].maderaPisoA);
        document.getElementById("otrosPiso"+item.id+"").checked = (arrCodigoPiso[conto].otrosPisoA);
        $("#otrosDescPiso"+item.id+"").val(arrCodigoPiso[conto].otroDescPisoA); 
        $("#integridPiso"+item.id+"").val(arrCodigoPiso[conto].integridadPisoA); 
        conto=conto+1;
      });
    });
  }
}

//-------editar y actualizar pisos------------
var arrDibujoP=[];
var aarrCodigoPiso = [];
var contPactual = 0;

function editarGrillaPiso(longitudGrilla){
  var contPiso=1;
  arrDibujoP=[];
  var htmlPiso;
  for (var i=1; i<longitudGrilla;i++)
        {
      htmlPiso = '<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionPi'+contPiso+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Interior">Interior</option>'+
      '<option value="Aceras">Aceras</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="baldosasPi'+contPiso+'">Baldosas<br>'+
      '<input type="checkbox" id="mosaicosPi'+contPiso+'">Mosaicos<br>'+
      '<input type="checkbox" id="porcelanatoPi'+contPiso+'"> Porcelanato<br>'+
      '<input type="checkbox" id="plajaPi'+contPiso+'"> P.Laja<br>'+
      '<input type="checkbox" id="prioPi'+contPiso+'"> P.Rio<br>'+
      '<input type="checkbox" id="ladrillosPi'+contPiso+'"> Ladrillos<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="gramaPi'+contPiso+'"> Grama<br>'+
      '<input type="checkbox" id="tierraPi'+contPiso+'"> Tierra<br>'+
      '<input type="checkbox" id="concretoPi'+contPiso+'"> Concreto<br>'+
      '<input type="checkbox" id="ceramicaPi'+contPiso+'"> Ceramica<br>'+
      '<input type="checkbox" id="maderaPi'+contPiso+'"> Madera<br>'+
      '<input type="checkbox" id="otrosPi'+contPiso+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescPi'+contPiso+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridPi'+contPiso+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPiso('+contCodigoPiso+');"></i>'+
      '</a></div>'+
      '</div>';
       arrDibujoP.push({scd_codigo:htmlPiso,id: contPiso});
        contPiso++;
        }
        var cadenaPiso='';
        var cont=1;
        for (var i=0; i<arrDibujoP.length;i++)
        {
          cadenaPiso=cadenaPiso + arrDibujoP[i].scd_codigo;
          cont++;
        }

        $('#acrearGrillaPiso').html(cadenaPiso);
  }


function actualizarPiso(){
  var htmlPisoAc;
  htmlPisoAc='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionPi'+contPactual+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Interior">Interior</option>'+
      '<option value="Aceras">Aceras</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="baldosasPi'+contPactual+'">Baldosas<br>'+
      '<input type="checkbox" id="mosaicosPi'+contPactual+'">Mosaicos<br>'+
      '<input type="checkbox" id="porcelanatoPi'+contPactual+'"> Porcelanato<br>'+
      '<input type="checkbox" id="plajaPi'+contPactual+'"> P.Laja<br>'+
      '<input type="checkbox" id="prioPi'+contPactual+'"> P.Rio<br>'+
      '<input type="checkbox" id="ladrillosPi'+contPactual+'"> Ladrillos<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="gramaPi'+contPactual+'"> Grama<br>'+
      '<input type="checkbox" id="tierraPi'+contPactual+'"> Tierra<br>'+
      '<input type="checkbox" id="concretoPi'+contPactual+'"> Concreto<br>'+
      '<input type="checkbox" id="ceramicaPi'+contPactual+'"> Ceramica<br>'+
      '<input type="checkbox" id="maderaPi'+contPactual+'"> Madera<br>'+
      '<input type="checkbox" id="otrosPi'+contPactual+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescPi'+contPactual+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridPi'+contPactual+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPiso('+contPactual+');"></i>'+
      '</a></div>'+
      '</div>';
  var cadenaPiso = '';
  var cont = 0;
  arrDibujoP.forEach(function (item, index, array){
    aarrCodigoPiso[cont].adescripcionPisoA = $("#descripcionPi"+item.id+"").val();     
    aarrCodigoPiso[cont].abaldosasPisoA = document.getElementById("baldosasPi"+item.id+"").checked; 
    aarrCodigoPiso[cont].amosaicosPisoA = document.getElementById("mosaicosPi"+item.id+"").checked;
    aarrCodigoPiso[cont].aporcelanatoPisoA = document.getElementById("porcelanatoPi"+item.id+"").checked;
    aarrCodigoPiso[cont].aplajaPisoA = document.getElementById("plajaPi"+item.id+"").checked;
    aarrCodigoPiso[cont].aprioPisoA = document.getElementById("prioPi"+item.id+"").checked; 
    aarrCodigoPiso[cont].aladrillosPisoA = document.getElementById("ladrillosPi"+item.id+"").checked;
    aarrCodigoPiso[cont].agramaPisoA = document.getElementById("gramaPi"+item.id+"").checked;
    aarrCodigoPiso[cont].atierraPisoA = document.getElementById("tierraPi"+item.id+"").checked;
    aarrCodigoPiso[cont].aconcretoPisoA = document.getElementById("concretoPi"+item.id+"").checked; 
    aarrCodigoPiso[cont].aceramicaPisoA = document.getElementById("ceramicaPi"+item.id+"").checked;
    aarrCodigoPiso[cont].amaderaPisoA = document.getElementById("maderaPi"+item.id+"").checked;
    aarrCodigoPiso[cont].aotrosPisoA = document.getElementById("otrosPi"+item.id+"").checked;
    aarrCodigoPiso[cont].aotroDescPisoA = $("#otrosDescPi"+item.id+"").val();
    aarrCodigoPiso[cont].aintegridadPisoA = $("#integridPi"+item.id+"").val();
    cont++;
  });
  aarrCodigoPiso.push({adescripcionPisoA:'N', abaldosasPisoA: '', amosaicosPisoA: '', aporcelanatoPisoA: '', aplajaPisoA: '', aprioPisoA: '', aladrillosPisoA: '', agramaPisoA: '', atierraPisoA: '', aconcretoPisoA: '', aceramicaPisoA: '', amaderaPisoA: '',aotrosPisoA: '', aotroDescPisoA:'', aintegridadPisoA:''});
  arrDibujoP.push({scd_codigo:htmlPisoAc, id: contPactual});
  contPactual++;
  cont=1;
  for (var i=0; i<arrDibujoP.length;i++){
    cadenaPiso = cadenaPiso + arrDibujoP[i].scd_codigo;
    cont++;
  }
  $('#acrearGrillaPiso').html(cadenaPiso); 
  var conto=0;
  arrDibujoP.forEach(function (item, index, array) { 
    $("#descripcionPi"+item.id+"").val(aarrCodigoPiso[conto].adescripcionPisoA); 
    document.getElementById("baldosasPi"+item.id+"").checked = (aarrCodigoPiso[conto].abaldosasPisoA); 
    document.getElementById("mosaicosPi"+item.id+"").checked = (aarrCodigoPiso[conto].amosaicosPisoA);
    document.getElementById("porcelanatoPi"+item.id+"").checked = (aarrCodigoPiso[conto].aporcelanatoPisoA);
    document.getElementById("plajaPi"+item.id+"").checked = (aarrCodigoPiso[conto].aplajaPisoA);
    document.getElementById("prioPi"+item.id+"").checked = (aarrCodigoPiso[conto].aprioPisoA); 
    document.getElementById("ladrillosPi"+item.id+"").checked = (aarrCodigoPiso[conto].aladrillosPisoA);
    document.getElementById("gramaPi"+item.id+"").checked = (aarrCodigoPiso[conto].agramaPisoA);
    document.getElementById("tierraPi"+item.id+"").checked = (aarrCodigoPiso[conto].atierraPisoA);
    document.getElementById("concretoPi"+item.id+"").checked = (aarrCodigoPiso[conto].aconcretoPisoA); 
    document.getElementById("ceramicaPi"+item.id+"").checked = (aarrCodigoPiso[conto].aceramicaPisoA);
    document.getElementById("maderaPi"+item.id+"").checked = (aarrCodigoPiso[conto].amaderaPisoA);
    document.getElementById("otrosPi"+item.id+"").checked = (aarrCodigoPiso[conto].aotrosPisoA);
    $("#otrosDescPi"+item.id+"").val(aarrCodigoPiso[conto].aotroDescPisoA); 
    $("#integridPi"+item.id+"").val(aarrCodigoPiso[conto].aintegridadPisoA); 
    conto=conto+1;
  });
}


function eliminarPiso($id){
  if(contPactual>2){
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaPiso ='';
      var pos=-1;
      var cont = 0;
      arrDibujoP.forEach(function (item, index, array) {
        aarrCodigoPiso[cont].adescripcionPisoA = $("#descripcionPi"+item.id+"").val();     
        aarrCodigoPiso[cont].abaldosasPisoA = document.getElementById("baldosasPi"+item.id+"").checked; 
        aarrCodigoPiso[cont].amosaicosPisoA = document.getElementById("mosaicosPi"+item.id+"").checked;
        aarrCodigoPiso[cont].aporcelanatoPisoA = document.getElementById("porcelanatoPi"+item.id+"").checked;
        aarrCodigoPiso[cont].aplajaPisoA = document.getElementById("plajaPi"+item.id+"").checked;
        aarrCodigoPiso[cont].aprioPisoA = document.getElementById("prioPi"+item.id+"").checked; 
        aarrCodigoPiso[cont].aladrillosPisoA = document.getElementById("ladrillosPi"+item.id+"").checked;
        aarrCodigoPiso[cont].agramaPisoA = document.getElementById("gramaPi"+item.id+"").checked;
        aarrCodigoPiso[cont].atierraPisoA = document.getElementById("tierraPi"+item.id+"").checked;
        aarrCodigoPiso[cont].aconcretoPisoA = document.getElementById("concretoPi"+item.id+"").checked; 
        aarrCodigoPiso[cont].aceramicaPisoA = document.getElementById("ceramicaPi"+item.id+"").checked;
        aarrCodigoPiso[cont].amaderaPisoA = document.getElementById("maderaPi"+item.id+"").checked;
        aarrCodigoPiso[cont].aotrosPisoA = document.getElementById("otrosPi"+item.id+"").checked;
        aarrCodigoPiso[cont].aotroDescPisoA = $("#otrosDescPi"+item.id+"").val();
        aarrCodigoPiso[cont].aintegridadPisoA = $("#integridPi"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoP.map(function(e) {return e.id;}).indexOf($id);
      aarrCodigoPiso.splice(pos,1);
      arrDibujoP.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoP.length;i++) {
        cadenaPiso = cadenaPiso + arrDibujoP[i].scd_codigo;
        cont++;
      }
      $('#acrearGrillaPiso').html(cadenaPiso);
      var conto=0;
      arrDibujoP.forEach(function (item, index, array) { 
         $("#descripcionPi"+item.id+"").val(aarrCodigoPiso[conto].adescripcionPisoA); 
        document.getElementById("baldosasPi"+item.id+"").checked = (aarrCodigoPiso[conto].abaldosasPisoA); 
        document.getElementById("mosaicosPi"+item.id+"").checked = (aarrCodigoPiso[conto].amosaicosPisoA);
        document.getElementById("porcelanatoPi"+item.id+"").checked = (aarrCodigoPiso[conto].aporcelanatoPisoA);
        document.getElementById("plajaPi"+item.id+"").checked = (aarrCodigoPiso[conto].aplajaPisoA);
        document.getElementById("prioPi"+item.id+"").checked = (aarrCodigoPiso[conto].aprioPisoA); 
        document.getElementById("ladrillosPi"+item.id+"").checked = (aarrCodigoPiso[conto].aladrillosPisoA);
        document.getElementById("gramaPi"+item.id+"").checked = (aarrCodigoPiso[conto].agramaPisoA);
        document.getElementById("tierraPi"+item.id+"").checked = (aarrCodigoPiso[conto].atierraPisoA);
        document.getElementById("concretoPi"+item.id+"").checked = (aarrCodigoPiso[conto].aconcretoPisoA); 
        document.getElementById("ceramicaPi"+item.id+"").checked = (aarrCodigoPiso[conto].aceramicaPisoA);
        document.getElementById("maderaPi"+item.id+"").checked = (aarrCodigoPiso[conto].amaderaPisoA);
        document.getElementById("otrosPi"+item.id+"").checked = (aarrCodigoPiso[conto].aotrosPisoA);
        $("#otrosDescPi"+item.id+"").val(aarrCodigoPiso[conto].aotroDescPisoA); 
        $("#integridPi"+item.id+"").val(aarrCodigoPiso[conto].aintegridadPisoA); 
        conto=conto+1;
      });
    });
  }
}


//--------------------GRILLA PISO FIN-------------------//
//--------------------GRILLA puertas de aceso INICIO--------------------//

var contCodigoPuertasAcceso = 2;
var arrCodigoPuertasAcceso= [];
var arrDibujoPuertasAcceso = [];
htmlPuertasAcceso='<div class="col-md-12">'+
'<div class="form-group col-md-2">'+
'<select id="descripcionPuertasAcceso1" class="form-control">'+
'<option value="N" selected="selected">..Seleccionar..</option>'+
'<option value="Arco">Arco</option>'+
'<option value="Recto">Recto</option>'+
'<option value="Recto con Esquinas">Recto con Esquinas</option>'+
'<option value="Texturizado">Texturizado</option>'+
'<option value="Enchape">Enchape</option>'+
'</select> '+
'</div>'+
'<div class="form-group col-md-2">'+
'</div>'+
'<div class="form-group col-md-3">'+
'<input type="checkbox" id="maderaPuertasAcceso1">Madera<br>'+
'<input type="checkbox" id="metalPuertasAcceso1"> Metal<br>'+
'<input type="checkbox" id="otrosPuertasAcceso1"> Otros<br>'+
'</div>'+
'<div class="form-group col-md-2">'+
'<input type="text" id="otrosDescPuertasAcceso1" class="form-control"  placeholder="Otras Descrip.">'+
'</div>'+
'<div class="form-group col-md-2">'+
'<select id="integridPuertasAcceso1" class="form-control">'+
'<option value="-1" selected="selected">..hhhhh.</option>'+
'<option value="Original">Original</option>'+
'<option value="Nuevo">Nuevo</option>'+
'<option value="Modificado">Modificado</option>'+
'<option value="Original y Nuevo">Original y Nuevo</option>'+
'</select> '+
'</div>'+
'<div class="col-md-1">'+
'<a style="cursor:pointer;" type="button">'+
'<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuertasAcceso(1);"></i>'+
'</a></div>'+
'</div>';

var cadenaPuertasAcceso = '';  
arrCodigoPuertasAcceso.push({descripcionPuertasAcceso:'N', maderaPuertaAcceso: '', metalPuertaAcceso: '', otrosPuertaAcceso: '', otroDescPuertasAccesoA:'', integridadPuertasAccesoA:''});
arrDibujoPuertasAcceso.push({scd_codigo:htmlPuertasAcceso,id: 1});
cadenaPuertasAcceso=arrDibujoPuertasAcceso[0].scd_codigo;
//console.log("cadenaPuertasAcceso",cadenaPuertasAcceso);
$('#crearGrillaPuertasAcceso').html(cadenaPuertasAcceso);

$("#descripcionPuertasAcceso1").val(arrCodigoPuertasAcceso[0].descripcionPuertasAcceso); 
document.getElementById("maderaPuertasAcceso1").checked = (arrCodigoPuertasAcceso[0].maderaPuertaAcceso); 
document.getElementById("metalPuertasAcceso1").checked = (arrCodigoPuertasAcceso[0].metalPuertaAcceso);
document.getElementById("otrosPuertasAcceso1").checked = (arrCodigoPuertasAcceso[0].otrosPuertaAcceso);
$("#otrosDescPuertasAcceso1").val(arrCodigoPuertasAcceso[0].otroDescPuertasAccesoA); 
$("#integridPuertasAcceso1").val(arrCodigoPuertasAcceso[0].integridadPuertasAccesoA);

function grillaPuertasAcceso(){
  var htmlPuertasAcceso;
  htmlPuertasAcceso='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionPuertasAcceso'+contCodigoPuertasAcceso+'" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="Arco">Arco</option>'+
  '<option value="Recto">Recto</option>'+
  '<option value="Recto con Esquinas">Recto con Esquinas</option>'+
  '<option value="Texturizado">Texturizado</option>'+
  '<option value="Enchape">Enchape</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '</div>'+
  '<div class="form-group col-md-3">'+
  '<input type="checkbox" id="maderaPuertasAcceso'+contCodigoPuertasAcceso+'">Madera<br>'+
  '<input type="checkbox" id="metalPuertasAcceso'+contCodigoPuertasAcceso+'">Metal<br>'+
  '<input type="checkbox" id="otrosPuertasAcceso'+contCodigoPuertasAcceso+'">Otros<br>'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="otrosDescPuertasAcceso'+contCodigoPuertasAcceso+'" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridPuertasAcceso'+contCodigoPuertasAcceso+'" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="Original">Original</option>'+
  '<option value="Nuevo">Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '<option value="Original y Nuevo">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaPuertasAcceso('+contCodigoPuertasAcceso+');"></i>'+
  '</a></div>'+
  '</div>';
  var cadenaPuertasAcceso = '';
  var cont = 0;
  arrDibujoPuertasAcceso.forEach(function (item, index, array) 
  {
    arrCodigoPuertasAcceso[cont].descripcionPuertasAcceso = $("#descripcionPuertasAcceso"+item.id+"").val();     
    arrCodigoPuertasAcceso[cont].maderaPuertaAcceso = document.getElementById("maderaPuertasAcceso"+item.id+"").checked; 
    arrCodigoPuertasAcceso[cont].metalPuertaAcceso = document.getElementById("metalPuertasAcceso"+item.id+"").checked;
    arrCodigoPuertasAcceso[cont].otrosPuertaAcceso = document.getElementById("otrosPuertasAcceso"+item.id+"").checked;
    arrCodigoPuertasAcceso[cont].otroDescPuertasAccesoA = $("#otrosDescPuertasAcceso"+item.id+"").val();
    arrCodigoPuertasAcceso[cont].integridadPuertasAccesoA = $("#integridPuertasAcceso"+item.id+"").val();
    cont++;
  });

  arrCodigoPuertasAcceso.push({descripcionPuertasAcceso:'N', maderaPuertaAcceso: '', metalPuertaAcceso: '', otrosPuertaAcceso: '', otroDescPuertasAccesoA:'', integridadPuertasAccesoA:''});
  arrDibujoPuertasAcceso.push({scd_codigo:htmlPuertasAcceso, id: contCodigoPuertasAcceso});
  contCodigoPuertasAcceso++;

  cont=1;
  for (var i=0; i<arrDibujoPuertasAcceso.length;i++)
  {
    cadenaPuertasAcceso = cadenaPuertasAcceso + arrDibujoPuertasAcceso[i].scd_codigo;
    cont++;
  }
  $('#crearGrillaPuertasAcceso').html(cadenaPuertasAcceso); 
  var conto=0;
  arrDibujoPuertasAcceso.forEach(function (item, index, array) 
  { 
    $("#descripcionPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].descripcionPuertasAcceso); 
    document.getElementById("maderaPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].maderaPuertaAcceso); 
    document.getElementById("metalPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].metalPuertaAcceso);
    document.getElementById("otrosPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].otrosPuertaAcceso);
    $("#otrosDescPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].otroDescPuertasAccesoA); 
    $("#integridPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].integridadPuertasAccesoA); 
    conto=conto+1;
  });

}


  function eliminarGrillaPuertasAcceso($id){
    if(contCodigoPuertasAcceso>2)
    {
      swal({title: "Esta seguro de eliminar?",
        text: "Se eliminará el argumento seleccionado!",
        type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
      }, function(){
        var cadenaPuertasAcceso ='';
        var pos=-1;
        var cont = 0;
        arrDibujoPuertasAcceso.forEach(function (item, index, array) 
        {
          arrCodigoPuertasAcceso[cont].descripcionPuertasAcceso = $("#descripcionPuertasAcceso"+item.id+"").val();                      
          arrCodigoPuertasAcceso[cont].maderaPuertaAcceso = document.getElementById("maderaPuertasAcceso"+item.id+"").checked; 
          arrCodigoPuertasAcceso[cont].metalPuertaAcceso = document.getElementById("metalPuertasAcceso"+item.id+"").checked;
          arrCodigoPuertasAcceso[cont].otrosPuertaAcceso = document.getElementById("otrosPuertasAcceso"+item.id+"").checked;
          arrCodigoPuertasAcceso[cont].otroDescPuertasAccesoA = $("#otrosDescPuertasAcceso"+item.id+"").val();
          arrCodigoPuertasAcceso[cont].integridadPuertasAccesoA = $("#integridPuertasAcceso"+item.id+"").val();

          cont++;
        });
        pos = arrDibujoPuertasAcceso.map(function(e) {return e.id;}).indexOf($id);
        //console.log("poos",pos);
        arrCodigoPuertasAcceso.splice(pos,1);
        arrDibujoPuertasAcceso.splice(pos,1);
        //console.log("arrCodigo",arrCodigoPuertasAcceso);

        cont=1;
        for (var i=0; i<arrDibujoPuertasAcceso.length;i++)
        {
          cadenaPuertasAcceso = cadenaPuertasAcceso + arrDibujoPuertasAcceso[i].scd_codigo;
          cont++;
        }
        $('#crearGrillaPuertasAcceso').html(cadenaPuertasAcceso);
        var conto=0;
        arrDibujoPuertasAcceso.forEach(function (item, index, array) 
        { 
          $("#descripcionPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].descripcionPuertasAcceso);                 
          document.getElementById("maderaPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].maderaPuertaAcceso); 
          document.getElementById("metalPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].metalPuertaAcceso);
          document.getElementById("otrosPuertasAcceso"+item.id+"").checked = (arrCodigoPuertasAcceso[conto].otrosPuertaAcceso);
          $("#otrosDescPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].otroDescPuertasAccesoA); 
          $("#integridPuertasAcceso"+item.id+"").val(arrCodigoPuertasAcceso[conto].integridadPuertasAccesoA); 
          conto=conto+1;
        });
      });
    }
    //console.log("arrCodigoFinal",arrCodigoPuertasAcceso);
  }

var arrDibujoPAx=[];
var aarrCodigoPuertasAcceso = [];
var contPAactual = 0;
function editarPuertasAcceso(longitudGrilla){
  var contPAX=1;
  arrDibujoPAx=[];
  var htmlPAx;
  for (var i=1; i<longitudGrilla;i++)
        {
      htmlPAx = '<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionPA'+contPAX+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Arco">Arco</option>'+
      '<option value="Recto">Recto</option>'+
      '<option value="Recto con Esquinas">Recto con Esquinas</option>'+
      '<option value="Texturizado">Texturizado</option>'+
      '<option value="Enchape">Enchape</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<input type="checkbox" id="maderaPA'+contPAX+'">Madera<br>'+
      '<input type="checkbox" id="metalPA'+contPAX+'"> Metal<br>'+
      '<input type="checkbox" id="otrosPA'+contPAX+'"> Otros<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescPA'+contPAX+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridPA'+contPAX+'" class="form-control">'+
      '<option value="-1" selected="selected">..hhhhh.</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPuertasAcceso('+contPAX+');"></i>'+
      '</a></div>'+
      '</div>';
       arrDibujoPAx.push({scd_codigo:htmlPAx,id: contPAX});
        contPAX++;
        }
        var cadenaPAx='';
        var cont=1;
        for (var i=0; i<arrDibujoPAx.length;i++)
        {
          cadenaPAx=cadenaPAx + arrDibujoPAx[i].scd_codigo;
          cont++;
        }

        $('#acrearGrillaPuertasAcceso').html(cadenaPAx);
    }


 function actualizarPuertasAcceso(){
  var htmlPAact;
  htmlPAact='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionPA'+contPAactual+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Arco">Arco</option>'+
      '<option value="Recto">Recto</option>'+
      '<option value="Recto con Esquinas">Recto con Esquinas</option>'+
      '<option value="Texturizado">Texturizado</option>'+
      '<option value="Enchape">Enchape</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '</div>'+
      '<div class="form-group col-md-3">'+
      '<input type="checkbox" id="maderaPA'+contPAactual+'">Madera<br>'+
      '<input type="checkbox" id="metalPA'+contPAactual+'"> Metal<br>'+
      '<input type="checkbox" id="otrosPA'+contPAactual+'"> Otros<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescPA'+contPAactual+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridPA'+contPAactual+'" class="form-control">'+
      '<option value="-1" selected="selected">..hhhhh.</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarPuertasAcceso('+contPAactual+');"></i>'+
      '</a></div>'+
      '</div>';
  var cadenaPuertasAcceso = '';
  var cont = 0;
  arrDibujoPAx.forEach(function (item, index, array) 
  {
    aarrCodigoPuertasAcceso[cont].adescripcionPuertasAcceso = $("#descripcionPA"+item.id+"").val();     
    aarrCodigoPuertasAcceso[cont].amaderaPuertaAcceso = document.getElementById("maderaPA"+item.id+"").checked; 
    aarrCodigoPuertasAcceso[cont].ametalPuertaAcceso = document.getElementById("metalPA"+item.id+"").checked;
    aarrCodigoPuertasAcceso[cont].aotrosPuertaAcceso = document.getElementById("otrosPA"+item.id+"").checked;
    aarrCodigoPuertasAcceso[cont].aotroDescPuertasAccesoA = $("#otrosDescPA"+item.id+"").val();
    aarrCodigoPuertasAcceso[cont].aintegridadPuertasAccesoA = $("#integridPA"+item.id+"").val();
    cont++;
  });

  aarrCodigoPuertasAcceso.push({adescripcionPuertasAcceso:'N', amaderaPuertaAcceso: '', ametalPuertaAcceso: '', aotrosPuertaAcceso: '', aotroDescPuertasAccesoA:'', aintegridadPuertasAccesoA:''});
  arrDibujoPAx.push({scd_codigo:htmlPAact, id: contPAactual});
  contPAactual++;

  cont=1;
  for (var i=0; i<arrDibujoPAx.length;i++)
  {
    cadenaPuertasAcceso = cadenaPuertasAcceso + arrDibujoPAx[i].scd_codigo;
    cont++;
  }
  $('#acrearGrillaPuertasAcceso').html(cadenaPuertasAcceso); 
  var conto=0;
  arrDibujoPAx.forEach(function (item, index, array) 
  { 
    $("#descripcionPA"+item.id+"").val(aarrCodigoPuertasAcceso[conto].adescripcionPuertasAcceso); 
    document.getElementById("maderaPA"+item.id+"").checked = (aarrCodigoPuertasAcceso[conto].amaderaPuertaAcceso); 
    document.getElementById("metalPA"+item.id+"").checked = (aarrCodigoPuertasAcceso[conto].ametalPuertaAcceso);
    document.getElementById("otrosPA"+item.id+"").checked = (aarrCodigoPuertasAcceso[conto].aotrosPuertaAcceso);
    $("#otrosDescPA"+item.id+"").val(aarrCodigoPuertasAcceso[conto].aotroDescPuertasAccesoA); 
    $("#integridPA"+item.id+"").val(aarrCodigoPuertasAcceso[conto].aintegridadPuertasAccesoA); 
    conto=conto+1;
  });

}


  function eliminarPuertasAcceso($id){
    if(contPAactual>2)
    {
      swal({title: "Esta seguro de eliminar?",
        text: "Se eliminará el argumento seleccionado!",
        type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
      }, function(){
        var cadenaPuertasAcceso ='';
        var pos=-1;
        var cont = 0;
        arrDibujoPAx.forEach(function (item, index, array) 
        {
        aarrCodigoPuertasAcceso[cont].adescripcionPuertasAcceso = $("#descripcionPA"+item.id+"").val();     
        aarrCodigoPuertasAcceso[cont].amaderaPuertaAcceso = document.getElementById("maderaPA"+item.id+"").checked; 
        aarrCodigoPuertasAcceso[cont].ametalPuertaAcceso = document.getElementById("metalPA"+item.id+"").checked;
        aarrCodigoPuertasAcceso[cont].aotrosPuertaAcceso = document.getElementById("otrosPA"+item.id+"").checked;
        aarrCodigoPuertasAcceso[cont].aotroDescPuertasAccesoA = $("#otrosDescPA"+item.id+"").val();
        aarrCodigoPuertasAcceso[cont].aintegridadPuertasAccesoA = $("#integridPA"+item.id+"").val();
        cont++;
        });
        pos = arrDibujoPAx.map(function(e) {return e.id;}).indexOf($id);
        aarrCodigoPuertasAcceso.splice(pos,1);
        arrDibujoPAx.splice(pos,1);
        cont=1;
        for (var i=0; i<arrDibujoPAx.length;i++)
        {
          cadenaPuertasAcceso = cadenaPuertasAcceso + arrDibujoPAx[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaPuertasAcceso').html(cadenaPuertasAcceso);
        var conto=0;
        arrDibujoPAx.forEach(function (item, index, array) 
        { 
         $("#descripcionPA"+item.id+"").val(aarrCodigoPuertasAcceso[conto].adescripcionPuertasAcceso); 
         document.getElementById("maderaPA"+item.id+"").checked = (aarrCodigoPuertasAcceso[conto].amaderaPuertaAcceso); 
         document.getElementById("metalPA"+item.id+"").checked = (aarrCodigoPuertasAcceso[conto].ametalPuertaAcceso);
         document.getElementById("otrosPA"+item.id+"").checked = (aarrCodigoPuertasAcceso[conto].aotrosPuertaAcceso);
         $("#otrosDescPA"+item.id+"").val(aarrCodigoPuertasAcceso[conto].aotroDescPuertasAccesoA); 
         $("#integridPA"+item.id+"").val(aarrCodigoPuertasAcceso[conto].aintegridadPuertasAccesoA); 
          conto=conto+1;
        });
      });
    }
  }







    //--------------------GRILLA puerta de acceso FIN-------------------//
    //--------------------GRILLA Mobiliario Urbano--------------------//

    var contCodigoMovilidadUrbano = 2;
    var arrCodigoMovilidadUrbano = [];
    var arrDibujoMovilidadUrbano = [];


    htmlMovilidadUrbano='<div class="col-md-12">'+
    '<div class="form-group col-md-2">'+
    '<select id="descripcionMovilidadUrbano1" class="form-control">'+
    '<option value="N" selected="selected">..Seleccionar..</option>'+
    '<option value="Asientos">Asientos</option>'+
    '<option value="Balaustrada">Balaustrada</option>'+
    '<option value="Bancas">Bancas</option>'+
    '<option value="Basureros">Basureros</option>'+
    '<option value="Cabinas Tel.">Cabinas Tel.</option>'+
    '</select> '+
    '</div>'+
    '<div class="form-group col-md-1">'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<input type="checkbox" id="maderamobiliariourbano1">Madera<br>'+
    '<input type="checkbox" id="metal1">Metal<br>'+
    '<input type="checkbox" id="barro1"> Barro<br>'+
    '<input type="checkbox" id="cal1"> Cal<br>'+
    '<input type="checkbox" id="ladrillomobiliariourbano1">Ladrillo<br>'+
    '<input type="checkbox" id="piedramobiliariourbano1"> Piedra<br>'+
    '</div>'+
    '<div class="form-group col-md-2">'+

    '<input type="checkbox" id="adobe1">Adobe<br>'+
    '<input type="checkbox" id="concreto1"> Concreto<br>'+
    '<input type="checkbox" id="fcarbon1">F. carbon<br>'+
    '<input type="checkbox" id="vidrio1">Vidrio<br>'+
    '<input type="checkbox" id="fibrocem1">Fibro<br>'+
    '<input type="checkbox" id="otrosmobiliariourbano1"> Otros'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<input type="text" id="otrosDescMovilidadUrbano1" class="form-control"  placeholder="Otras Descrip.">'+
    '</div>'+
    '<div class="form-group col-md-2">'+
    '<select id="integridMovilidadUrbano1" class="form-control">'+
    '<option value="N" selected="selected">....Seleccionar...</option>'+
    '<option value="Original">Original</option>'+
    '<option value="Nuevo">Nuevo</option>'+
    '<option value="Modificado">Modificado</option>'+
    '<option value="Original y Nuevo">Original y Nuevo</option>'+
    '</select> '+
    '</div>'+
    '<div class="col-md-1">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaMovilidadUrbano(1);"></i>'+
    '</a></div>'+
    '</div>';

    var cadenaMovilidadUrbano = '';  
    arrCodigoMovilidadUrbano.push({descripcionMovilidadUrbano: 'N',maderaMovilidadUrbano: '',metalMovilidadUrbano: '',barroMovilidadUrbano: '',calMovilidadUrbano: '',ladrilloMovilidadUrbano: '',piedraMovilidadUrbano: '',adobeMovilidadUrbano: '',concretoMovilidadUrbano: '',fcarbonMovilidadUrbano: '',vidrioMovilidadUrbano: '',fibrocemMovilidadUrbano: '',otrosMovilidadUrbano: '',otroDescMovilidadUrbanoMovilidadUrbano: '',integridadMovilidadUrbanoMovilidadUrbano: ''});
    arrDibujoMovilidadUrbano.push({scd_codigo:htmlMovilidadUrbano,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaMovilidadUrbano=arrDibujoMovilidadUrbano[0].scd_codigo;
    //console.log("cadenaMovilidadUrbano",cadenaMovilidadUrbano);
    $('#crearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano);

    $("#descripcionMovilidadUrbano1").val(arrCodigoMovilidadUrbano[0].descripcionMovilidadUrbano); 
    document.getElementById("maderamobiliariourbano1").checked = (arrCodigoMovilidadUrbano[0].maderaMovilidadUrbano); 
    document.getElementById("metal1").checked = (arrCodigoMovilidadUrbano[0].metalMovilidadUrbano);
    document.getElementById("barro1").checked = (arrCodigoMovilidadUrbano[0].barroMovilidadUrbano);
    document.getElementById("cal1").checked = (arrCodigoMovilidadUrbano[0].calMovilidadUrbano);
    document.getElementById("ladrillomobiliariourbano1").checked = (arrCodigoMovilidadUrbano[0].ladrilloMovilidadUrbano); 
    document.getElementById("piedramobiliariourbano1").checked = (arrCodigoMovilidadUrbano[0].piedraMovilidadUrbano);
    document.getElementById("adobe1").checked = (arrCodigoMovilidadUrbano[0].adobeMovilidadUrbano);
    document.getElementById("concreto1").checked = (arrCodigoMovilidadUrbano[0].concretoMovilidadUrbano);
    document.getElementById("fcarbon1").checked = (arrCodigoMovilidadUrbano[0].fcarbonMovilidadUrbano); 
    document.getElementById("vidrio1").checked = (arrCodigoMovilidadUrbano[0].vidrioMovilidadUrbano);
    document.getElementById("fibrocem1").checked = (arrCodigoMovilidadUrbano[0].fibrocemMovilidadUrbano);
    document.getElementById("otrosmobiliariourbano1").checked = (arrCodigoMovilidadUrbano[0].otrosMovilidadUrbano);
    $("#otrosDescMovilidadUrbano1").val(arrCodigoMovilidadUrbano[0].otroDescMovilidadUrbanoMovilidadUrbano); 
    $("#integridMovilidadUrbano1").val(arrCodigoMovilidadUrbano[0].integridadMovilidadUrbanoMovilidadUrbano);

    function grillaMovilidadUrbano(){
      var htmlMovilidadUrbano;
      htmlMovilidadUrbano='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionMovilidadUrbano'+contCodigoMovilidadUrbano+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Asientos">Asientos</option>'+
      '<option value="Balaustrada">Balaustrada</option>'+
      '<option value="Bancas">Bancas</option>'+
      '<option value="Basureros">Basureros</option>'+
      '<option value="Cabinas Tel.">Cabinas Tel.</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="maderamobiliariourbano'+contCodigoMovilidadUrbano+'">Madera<br>'+
      '<input type="checkbox" id="metal'+contCodigoMovilidadUrbano+'">Metal<br>'+
      '<input type="checkbox" id="barro'+contCodigoMovilidadUrbano+'"> Barro<br>'+
      '<input type="checkbox" id="cal'+contCodigoMovilidadUrbano+'"> Cal<br>'+
      '<input type="checkbox" id="ladrillomobiliariourbano'+contCodigoMovilidadUrbano+'">Ladrillo<br>'+
      '<input type="checkbox" id="piedramobiliariourbano'+contCodigoMovilidadUrbano+'"> Piedra<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+

      '<input type="checkbox" id="adobe'+contCodigoMovilidadUrbano+'">Adobe<br>'+
      '<input type="checkbox" id="concreto'+contCodigoMovilidadUrbano+'"> Concreto<br>'+
      '<input type="checkbox" id="fcarbon'+contCodigoMovilidadUrbano+'">F. carbon<br>'+
      '<input type="checkbox" id="vidrio'+contCodigoMovilidadUrbano+'">Vidrio<br>'+
      '<input type="checkbox" id="fibrocem'+contCodigoMovilidadUrbano+'">Fibro<br>'+
      '<input type="checkbox" id="otrosmobiliariourbano'+contCodigoMovilidadUrbano+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescMovilidadUrbano'+contCodigoMovilidadUrbano+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridMovilidadUrbano'+contCodigoMovilidadUrbano+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaMovilidadUrbano('+contCodigoMovilidadUrbano+');"></i>'+
      '</a></div>'+
      '</div>';

      var cadenaMovilidadUrbano = '';
      var cont = 0;
      arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
      {
        arrCodigoMovilidadUrbano[cont].descripcionMovilidadUrbano = $("#descripcionMovilidadUrbano"+item.id+"").val();     
        arrCodigoMovilidadUrbano[cont].maderaMovilidadUrbano = document.getElementById("maderamobiliariourbano"+item.id+"").checked; 
        arrCodigoMovilidadUrbano[cont].metalMovilidadUrbano = document.getElementById("metal"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].barroMovilidadUrbano = document.getElementById("barro"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].calMovilidadUrbano = document.getElementById("cal"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].ladrilloMovilidadUrbano = document.getElementById("ladrillomobiliariourbano"+item.id+"").checked; 
        arrCodigoMovilidadUrbano[cont].piedraMovilidadUrbano = document.getElementById("piedramobiliariourbano"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].adobeMovilidadUrbano = document.getElementById("adobe"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].concretoMovilidadUrbano = document.getElementById("concreto"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].fcarbonMovilidadUrbano = document.getElementById("fcarbon"+item.id+"").checked; 
        arrCodigoMovilidadUrbano[cont].vidrioMovilidadUrbano = document.getElementById("vidrio"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].fibrocemMovilidadUrbano = document.getElementById("fibrocem"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].otrosMovilidadUrbano = document.getElementById("otrosmobiliariourbano"+item.id+"").checked;
        arrCodigoMovilidadUrbano[cont].otroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDescMovilidadUrbano"+item.id+"").val();
        arrCodigoMovilidadUrbano[cont].integridadMovilidadUrbanoMovilidadUrbano = $("#integridMovilidadUrbano"+item.id+"").val();
        cont++;
      });

      arrCodigoMovilidadUrbano.push({descripcionMovilidadUrbano: 'N',maderaMovilidadUrbano: '',metalMovilidadUrbano: '',barroMovilidadUrbano: '',calMovilidadUrbano: '',ladrilloMovilidadUrbano: '',piedraMovilidadUrbano: '',adobeMovilidadUrbano: '',concretoMovilidadUrbano: '',fcarbonMovilidadUrbano: '',vidrioMovilidadUrbano: '',fibrocemMovilidadUrbano: '',otrosMovilidadUrbano: '',otroDescMovilidadUrbanoMovilidadUrbano: '',integridadMovilidadUrbanoMovilidadUrbano: ''});
      arrDibujoMovilidadUrbano.push({scd_codigo:htmlMovilidadUrbano, id: contCodigoMovilidadUrbano});
      contCodigoMovilidadUrbano++;

      cont=1;
      for (var i=0; i<arrDibujoMovilidadUrbano.length;i++)
      {
        cadenaMovilidadUrbano = cadenaMovilidadUrbano + arrDibujoMovilidadUrbano[i].scd_codigo;
        cont++;
      }
      
      $('#crearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano); 

      var conto=0;
      arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
      { 
        $("#descripcionMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].descripcionMovilidadUrbano); 
        document.getElementById("maderamobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].maderaMovilidadUrbano); 
        document.getElementById("metal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].metalMovilidadUrbano);
        document.getElementById("barro"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].barroMovilidadUrbano);
        document.getElementById("cal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].calMovilidadUrbano);
        document.getElementById("ladrillomobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].ladrilloMovilidadUrbano); 
        document.getElementById("piedramobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].piedraMovilidadUrbano);
        document.getElementById("adobe"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].adobeMovilidadUrbano);
        document.getElementById("concreto"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].concretoMovilidadUrbano);
        document.getElementById("fcarbon"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fcarbonMovilidadUrbano); 
        document.getElementById("vidrio"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].vidrioMovilidadUrbano);
        document.getElementById("fibrocem"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fibrocemMovilidadUrbano);
        document.getElementById("otrosmobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].otrosMovilidadUrbano);
        $("#otrosDescMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].otroDescMovilidadUrbanoMovilidadUrbano); 
        $("#integridMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].integridadMovilidadUrbanoMovilidadUrbano); 

        conto=conto+1;
      });

        ////console.log("arrCodigoMovilidadUrbano",arrCodigoMovilidadUrbano);
      }


      function eliminarGrillaMovilidadUrbano($id){
        if(contCodigoMovilidadUrbano>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaMovilidadUrbano ='';
            var pos=-1;
            var cont = 0;
            arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
            {
              arrCodigoMovilidadUrbano[cont].descripcionMovilidadUrbano = $("#descripcionMovilidadUrbano"+item.id+"").val();     
              arrCodigoMovilidadUrbano[cont].maderaMovilidadUrbano = document.getElementById("maderamobiliariourbano"+item.id+"").checked; 
              arrCodigoMovilidadUrbano[cont].metalMovilidadUrbano = document.getElementById("metal"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].barroMovilidadUrbano = document.getElementById("barro"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].calMovilidadUrbano = document.getElementById("cal"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].ladrilloMovilidadUrbano = document.getElementById("ladrillomobiliariourbano"+item.id+"").checked; 
              arrCodigoMovilidadUrbano[cont].piedraMovilidadUrbano = document.getElementById("piedramobiliariourbano"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].adobeMovilidadUrbano = document.getElementById("adobe"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].concretoMovilidadUrbano = document.getElementById("concreto"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].fcarbonMovilidadUrbano = document.getElementById("fcarbon"+item.id+"").checked; 
              arrCodigoMovilidadUrbano[cont].vidrioMovilidadUrbano = document.getElementById("vidrio"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].fibrocemMovilidadUrbano = document.getElementById("fibrocem"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].otrosMovilidadUrbano = document.getElementById("otrosmobiliariourbano"+item.id+"").checked;
              arrCodigoMovilidadUrbano[cont].otroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDescMovilidadUrbano"+item.id+"").val();
              arrCodigoMovilidadUrbano[cont].integridadMovilidadUrbanoMovilidadUrbano = $("#integridMovilidadUrbano"+item.id+"").val();

              cont++;
            });
            pos = arrDibujoMovilidadUrbano.map(function(e) {return e.id;}).indexOf($id);
            //console.log("poos",pos);
            arrCodigoMovilidadUrbano.splice(pos,1);
            arrDibujoMovilidadUrbano.splice(pos,1);
            //console.log("arrCodigo",arrCodigoMovilidadUrbano);

            cont=1;
            for (var i=0; i<arrDibujoMovilidadUrbano.length;i++)
            {
              cadenaMovilidadUrbano = cadenaMovilidadUrbano + arrDibujoMovilidadUrbano[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano);
            var conto=0;
            arrDibujoMovilidadUrbano.forEach(function (item, index, array) 
            { 

              $("#descripcionMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].descripcionMovilidadUrbano); 
              document.getElementById("maderamobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].maderaMovilidadUrbano); 
              document.getElementById("metal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].metalMovilidadUrbano);
              document.getElementById("barro"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].barroMovilidadUrbano);
              document.getElementById("cal"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].calMovilidadUrbano);
              document.getElementById("ladrillomobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].ladrilloMovilidadUrbano); 
              document.getElementById("piedramobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].piedraMovilidadUrbano);
              document.getElementById("adobe"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].adobeMovilidadUrbano);
              document.getElementById("concreto"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].concretoMovilidadUrbano);
              document.getElementById("fcarbon"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fcarbonMovilidadUrbano); 
              document.getElementById("vidrio"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].vidrioMovilidadUrbano);
              document.getElementById("fibrocem"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].fibrocemMovilidadUrbano);
              document.getElementById("otrosmobiliariourbano"+item.id+"").checked = (arrCodigoMovilidadUrbano[conto].otrosMovilidadUrbano);
              $("#otrosDescMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].otroDescMovilidadUrbanoMovilidadUrbano); 
              $("#integridMovilidadUrbano"+item.id+"").val(arrCodigoMovilidadUrbano[conto].integridadMovilidadUrbanoMovilidadUrbano); 
              conto=conto+1;
            });
          });
        }
        ///////console.log("arrCodigoFinal",arrCodigoAcab);
      }

    //-----editar yactualizar mobiliario urbano--------

   var arrDibujoMUr=[];
   var aarrMobiliarioUrbano = [];
   var contMUactual = 0;
   function editarMobiliarioUrbano(longitudGrilla){
    var contMUr=1;
    arrDibujoMUr=[];
    var htmlMUr;
    for (var i=1; i<longitudGrilla;i++)
          {
        htmlMUr = '<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
          '<select id="descMUr'+contMUr+'" class="form-control">'+
          '<option value="N" selected="selected">..Seleccionar..</option>'+
          '<option value="Asientos">Asientos</option>'+
          '<option value="Balaustrada">Balaustrada</option>'+
          '<option value="Bancas">Bancas</option>'+
          '<option value="Basureros">Basureros</option>'+
          '<option value="Cabinas Tel.">Cabinas Tel.</option>'+
          '</select> '+
          '</div>'+
          '<div class="form-group col-md-1">'+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="maderMUr'+contMUr+'">Madera<br>'+
          '<input type="checkbox" id="metalMUr'+contMUr+'">Metal<br>'+
          '<input type="checkbox" id="barroMUr'+contMUr+'"> Barro<br>'+
          '<input type="checkbox" id="calMUr'+contMUr+'"> Cal<br>'+
          '<input type="checkbox" id="ladrilloMUr'+contMUr+'">Ladrillo<br>'+
          '<input type="checkbox" id="piedraMUr'+contMUr+'"> Piedra<br>'+
          '</div>'+
          '<div class="form-group col-md-2">'+

          '<input type="checkbox" id="adobeMUr'+contMUr+'">Adobe<br>'+
          '<input type="checkbox" id="concretoMUr'+contMUr+'"> Concreto<br>'+
          '<input type="checkbox" id="fcarbonMUr'+contMUr+'">F. carbon<br>'+
          '<input type="checkbox" id="vidrioMUr'+contMUr+'">Vidrio<br>'+
          '<input type="checkbox" id="fibrocemMUr'+contMUr+'">Fibro<br>'+
          '<input type="checkbox" id="otrosMUr'+contMUr+'"> Otros'+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '<input type="text" id="otrosDesOUr'+contMUr+'" class="form-control"  placeholder="Otras Descrip.">'+
          '</div>'+
            '<div class="form-group col-md-2">'+
            '<select id="integridMUr'+contMUr+'" class="form-control">'+
            '<option value="N" selected="selected">....Seleccionar...</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
            '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
            '</div>'+
            '<div class="col-md-1">'+
            '<a style="cursor:pointer;" type="button">'+
            '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarMobiliarioUrbano('+contMUr+');"></i>'+
            '</a></div>'+
            '</div>';
         arrDibujoMUr.push({scd_codigo:htmlMUr,id: contMUr});
          contMUr++;
          }
          var cadenaMUr='';
          var cont=1;
          for (var i=0; i<arrDibujoMUr.length;i++)
          {
            cadenaMUr=cadenaMUr + arrDibujoMUr[i].scd_codigo;
            cont++;
          }
          $('#acrearGrillaMovilidadUrbanoado').html(cadenaMUr);
        }


    function actualizarMobiliarioUrbano(){
      var htmlMUact;
      htmlMUact='<div class="col-md-12">'+
          '<div class="form-group col-md-2">'+
          '<select id="descMUr'+contMUactual+'" class="form-control">'+
          '<option value="N" selected="selected">..Seleccionar..</option>'+
          '<option value="Asientos">Asientos</option>'+
          '<option value="Balaustrada">Balaustrada</option>'+
          '<option value="Bancas">Bancas</option>'+
          '<option value="Basureros">Basureros</option>'+
          '<option value="Cabinas Tel.">Cabinas Tel.</option>'+
          '</select> '+
          '</div>'+
          '<div class="form-group col-md-1">'+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '<input type="checkbox" id="maderMUr'+contMUactual+'">Madera<br>'+
          '<input type="checkbox" id="metalMUr'+contMUactual+'">Metal<br>'+
          '<input type="checkbox" id="barroMUr'+contMUactual+'"> Barro<br>'+
          '<input type="checkbox" id="calMUr'+contMUactual+'"> Cal<br>'+
          '<input type="checkbox" id="ladrilloMUr'+contMUactual+'">Ladrillo<br>'+
          '<input type="checkbox" id="piedraMUr'+contMUactual+'"> Piedra<br>'+
          '</div>'+
          '<div class="form-group col-md-2">'+

          '<input type="checkbox" id="adobeMUr'+contMUactual+'">Adobe<br>'+
          '<input type="checkbox" id="concretoMUr'+contMUactual+'"> Concreto<br>'+
          '<input type="checkbox" id="fcarbonMUr'+contMUactual+'">F. carbon<br>'+
          '<input type="checkbox" id="vidrioMUr'+contMUactual+'">Vidrio<br>'+
          '<input type="checkbox" id="fibrocemMUr'+contMUactual+'">Fibro<br>'+
          '<input type="checkbox" id="otrosMUr'+contMUactual+'"> Otros'+
          '</div>'+
          '<div class="form-group col-md-2">'+
          '<input type="text" id="otrosDesOUr'+contMUactual+'" class="form-control"  placeholder="Otras Descrip.">'+
          '</div>'+
            '<div class="form-group col-md-2">'+
            '<select id="integridMUr'+contMUactual+'" class="form-control">'+
            '<option value="N" selected="selected">....Seleccionar...</option>'+
            '<option value="Original">Original</option>'+
            '<option value="Nuevo">Nuevo</option>'+
            '<option value="Modificado">Modificado</option>'+
            '<option value="Original y Nuevo">Original y Nuevo</option>'+
            '</select> '+
            '</div>'+
            '<div class="col-md-1">'+
            '<a style="cursor:pointer;" type="button">'+
            '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarMobiliarioUrbano('+contMUactual+');"></i>'+
            '</a></div>'+
            '</div>';
      var cadenaMovilidadUrbano = '';
      var cont = 0;
      arrDibujoMUr.forEach(function (item, index, array) 
      {
        aarrMobiliarioUrbano[cont].adescripcionMovilidadUrbano = $("#descMUr"+item.id+"").val();     
        aarrMobiliarioUrbano[cont].amaderaMovilidadUrbano = document.getElementById("maderMUr"+item.id+"").checked; 
        aarrMobiliarioUrbano[cont].ametalMovilidadUrbano = document.getElementById("metalMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].abarroMovilidadUrbano = document.getElementById("barroMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].acalMovilidadUrbano = document.getElementById("calMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].aladrilloMovilidadUrbano = document.getElementById("ladrilloMUr"+item.id+"").checked; 
        aarrMobiliarioUrbano[cont].apiedraMovilidadUrbano = document.getElementById("piedraMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].aadobeMovilidadUrbano = document.getElementById("adobeMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].aconcretoMovilidadUrbano = document.getElementById("concretoMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].afcarbonMovilidadUrbano = document.getElementById("fcarbonMUr"+item.id+"").checked; 
        aarrMobiliarioUrbano[cont].avidrioMovilidadUrbano = document.getElementById("vidrioMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].afibrocemMovilidadUrbano = document.getElementById("fibrocemMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].aotrosMovilidadUrbano = document.getElementById("otrosMUr"+item.id+"").checked;
        aarrMobiliarioUrbano[cont].aotroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDesOUr"+item.id+"").val();
        aarrMobiliarioUrbano[cont].aintegridadMovilidadUrbanoMovilidadUrbano = $("#integridMUr"+item.id+"").val();
        cont++;
      });

      aarrMobiliarioUrbano.push({adescripcionMovilidadUrbano: 'N',amaderaMovilidadUrbano: '',ametalMovilidadUrbano: '',abarroMovilidadUrbano: '',acalMovilidadUrbano: '',aladrilloMovilidadUrbano: '',apiedraMovilidadUrbano: '',aadobeMovilidadUrbano: '',aconcretoMovilidadUrbano: '',afcarbonMovilidadUrbano: '',avidrioMovilidadUrbano: '',afibrocemMovilidadUrbano: '',aotrosMovilidadUrbano: '',aotroDescMovilidadUrbanoMovilidadUrbano: '',aintegridadMovilidadUrbanoMovilidadUrbano: ''});
      arrDibujoMUr.push({scd_codigo:htmlMUact, id: contMUactual});
      contMUactual++;

      cont=1;
      for (var i=0; i<arrDibujoMUr.length;i++)
      {
        cadenaMovilidadUrbano = cadenaMovilidadUrbano + arrDibujoMUr[i].scd_codigo;
        cont++;
      }
      
      $('#acrearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano); 

      var conto=0;
      arrDibujoMUr.forEach(function (item, index, array) 
      { 
        $("#descMUr"+item.id+"").val(aarrMobiliarioUrbano[conto].adescripcionMovilidadUrbano); 
        document.getElementById("maderMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].amaderaMovilidadUrbano); 
        document.getElementById("metalMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].ametalMovilidadUrbano);
        document.getElementById("barroMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].abarroMovilidadUrbano);
        document.getElementById("calMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].acalMovilidadUrbano);
        document.getElementById("ladrilloMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aladrilloMovilidadUrbano); 
        document.getElementById("piedraMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].apiedraMovilidadUrbano);
        document.getElementById("adobeMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aadobeMovilidadUrbano);
        document.getElementById("concretoMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aconcretoMovilidadUrbano);
        document.getElementById("fcarbonMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].afcarbonMovilidadUrbano); 
        document.getElementById("vidrioMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].avidrioMovilidadUrbano);
        document.getElementById("fibrocemMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].afibrocemMovilidadUrbano);
        document.getElementById("otrosMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aotrosMovilidadUrbano);
        $("#otrosDesOUr"+item.id+"").val(aarrMobiliarioUrbano[conto].aotroDescMovilidadUrbanoMovilidadUrbano); 
        $("#integridMUr"+item.id+"").val(aarrMobiliarioUrbano[conto].aintegridadMovilidadUrbanoMovilidadUrbano); 

        conto=conto+1;
      });

      }


      function eliminarMobiliarioUrbano($id){
        if(contMUactual>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaMovilidadUrbano ='';
            var pos=-1;
            var cont = 0;
            arrDibujoMUr.forEach(function (item, index, array) 
            {
            aarrMobiliarioUrbano[cont].adescripcionMovilidadUrbano = $("#descMUr"+item.id+"").val();     
            aarrMobiliarioUrbano[cont].amaderaMovilidadUrbano = document.getElementById("maderMUr"+item.id+"").checked; 
            aarrMobiliarioUrbano[cont].ametalMovilidadUrbano = document.getElementById("metalMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].abarroMovilidadUrbano = document.getElementById("barroMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].acalMovilidadUrbano = document.getElementById("calMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].aladrilloMovilidadUrbano = document.getElementById("ladrilloMUr"+item.id+"").checked; 
            aarrMobiliarioUrbano[cont].apiedraMovilidadUrbano = document.getElementById("piedraMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].aadobeMovilidadUrbano = document.getElementById("adobeMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].aconcretoMovilidadUrbano = document.getElementById("concretoMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].afcarbonMovilidadUrbano = document.getElementById("fcarbonMUr"+item.id+"").checked; 
            aarrMobiliarioUrbano[cont].avidrioMovilidadUrbano = document.getElementById("vidrioMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].afibrocemMovilidadUrbano = document.getElementById("fibrocemMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].aotrosMovilidadUrbano = document.getElementById("otrosMUr"+item.id+"").checked;
            aarrMobiliarioUrbano[cont].aotroDescMovilidadUrbanoMovilidadUrbano = $("#otrosDesOUr"+item.id+"").val();
            aarrMobiliarioUrbano[cont].aintegridadMovilidadUrbanoMovilidadUrbano = $("#integridMUr"+item.id+"").val();
            cont++;
            });
            pos = arrDibujoMUr.map(function(e) {return e.id;}).indexOf($id);
            arrCodigoMovilidadUrbano.splice(pos,1);
            arrDibujoMUr.splice(pos,1);
            cont=1;
            for (var i=0; i<arrDibujoMUr.length;i++)
            {
              cadenaMovilidadUrbano = cadenaMovilidadUrbano + arrDibujoMUr[i].scd_codigo;
              cont++;
            }
            $('#acrearGrillaMovilidadUrbanoado').html(cadenaMovilidadUrbano);
            var conto=0;
            arrDibujoMUr.forEach(function (item, index, array) 
            { 
            $("#descMUr"+item.id+"").val(aarrMobiliarioUrbano[conto].adescripcionMovilidadUrbano); 
            document.getElementById("maderMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].amaderaMovilidadUrbano); 
            document.getElementById("metalMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].ametalMovilidadUrbano);
            document.getElementById("barroMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].abarroMovilidadUrbano);
            document.getElementById("calMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].acalMovilidadUrbano);
            document.getElementById("ladrilloMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aladrilloMovilidadUrbano); 
            document.getElementById("piedraMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].apiedraMovilidadUrbano);
            document.getElementById("adobeMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aadobeMovilidadUrbano);
            document.getElementById("concretoMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aconcretoMovilidadUrbano);
            document.getElementById("fcarbonMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].afcarbonMovilidadUrbano); 
            document.getElementById("vidrioMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].avidrioMovilidadUrbano);
            document.getElementById("fibrocemMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].afibrocemMovilidadUrbano);
            document.getElementById("otrosMUr"+item.id+"").checked = (aarrMobiliarioUrbano[conto].aotrosMovilidadUrbano);
            $("#otrosDesOUr"+item.id+"").val(aarrMobiliarioUrbano[conto].aotroDescMovilidadUrbanoMovilidadUrbano); 
            $("#integridMUr"+item.id+"").val(aarrMobiliarioUrbano[conto].aintegridadMovilidadUrbanoMovilidadUrbano); 
              conto=conto+1;
            });
          });
        }
    }
    //--------------------GRILLA Mobiliario Urbano FIN-------------------//

  //--------------------GRILLA Escaleras--------------------//

   var contCodigoEscaleras = 2;
   var arrCodigoEscaleras = [];
   var arrDibujoEscaleras = [];
   htmlEscaleras='<div class="col-md-12">'+
   '<div class="form-group col-md-2">'+
   '<select id="descripcionEscaleras1" class="form-control">'+
   '<option value="N" selected="selected">..Seleccionar..</option>'+
   '<option value="Recta">Recta</option>'+
   '<option value="Imperial">Imperial</option>'+
   '<option value="Caracol">Caracol</option>'+
   '<option value="U">U</option>'+
   '<option value="L">L</option>'+
   '</select> '+
   '</div>'+
   '<div class="form-group col-md-1">'+
   '</div>'+
   '<div class="form-group col-md-2">'+
   '<input type="checkbox" id="maderaEscalerass1">Madera<br>'+
   '<input type="checkbox" id="metalEscaleras1">Metal<br>'+
   '<input type="checkbox" id="ConcretoEscaleras1"> Concreto<br>'+
   '<input type="checkbox" id="PiedraEscaleras1">Piedra<br>'+
   '</div>'+
   '<div class="form-group col-md-2">'+

   '<input type="checkbox" id="ladrilloEscaleras1">Ladrillo<br>'+
   '<input type="checkbox" id="tierraEscaleras1">Tierra<br>'+
   '<input type="checkbox" id="otrosEscaleras1"> Otros'+
   '</div>'+
   '<div class="form-group col-md-2">'+
   '<input type="text" id="otrosDescEscaleras1" class="form-control"  placeholder="Otras Descrip.">'+
   '</div>'+
   '<div class="form-group col-md-2">'+
   '<select id="integridEscaleras1" class="form-control">'+
   '<option value="N" selected="selected">....Seleccionar...</option>'+
   '<option value="Original">Original</option>'+
   '<option value="Nuevo">Nuevo</option>'+
   '<option value="Modificado">Modificado</option>'+
   '<option value="Original y Nuevo">Original y Nuevo</option>'+
   '</select> '+
   '</div>'+
   '<div class="col-md-1">'+
   '<a style="cursor:pointer;" type="button">'+
   '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaEscaleras(1);"></i>'+
   '</a></div>'+
   '</div>';

   var cadenaEscaleras = '';  
   arrCodigoEscaleras.push({descripcionEscaleras: 'N',maderaEscaleras: '', metalEscaleras: '', ConcretoEscaleras: '', PiedraEscaleras: '', ladrilloEscaleras: '', tierraEscaleras: '', otrosEscaleras: '', otroDescEscaleras: '',integridadEscaleras: ''});
   arrDibujoEscaleras.push({scd_codigo:htmlEscaleras,id: 1});
    ////console.log("arrCodigo",arrCodigoUso);
    
    cadenaEscaleras=arrDibujoEscaleras[0].scd_codigo;
    //console.log("cadenaEscaleras",cadenaEscaleras);
    $('#crearGrillaEscalerasado').html(cadenaEscaleras);


    $("#descripcionEscaleras1").val(arrCodigoEscaleras[0].descripcionEscaleras); 

    document.getElementById("maderaEscalerass1").checked = (arrCodigoEscaleras[0].maderaEscaleras); 
    document.getElementById("metalEscaleras1").checked = (arrCodigoEscaleras[0].metalEscaleras);
    document.getElementById("ConcretoEscaleras1").checked = (arrCodigoEscaleras[0].ConcretoEscaleras);
    document.getElementById("PiedraEscaleras1").checked = (arrCodigoEscaleras[0].PiedraEscaleras);
    document.getElementById("ladrilloEscaleras1").checked = (arrCodigoEscaleras[0].ladrilloEscaleras); 
    document.getElementById("tierraEscaleras1").checked = (arrCodigoEscaleras[0].tierraEscaleras);
    document.getElementById("otrosEscaleras1").checked = (arrCodigoEscaleras[0].otrosEscaleras);
    $("#otrosDescEscaleras1").val(arrCodigoEscaleras[0].otroDescEscaleras); 
    $("#integridEscaleras1").val(arrCodigoEscaleras[0].integridadEscaleras);


    function grillaEscaleras(){
      var htmlEscaleras;
      htmlEscaleras='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descripcionEscaleras'+contCodigoEscaleras+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Recta">Recta</option>'+
      '<option value="Imperial">Imperial</option>'+
      '<option value="Caracol">Caracol</option>'+
      '<option value="U">U</option>'+
      '<option value="L">L</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="maderaEscalerass'+contCodigoEscaleras+'">Madera<br>'+
      '<input type="checkbox" id="metalEscaleras'+contCodigoEscaleras+'">Metal<br>'+
      '<input type="checkbox" id="ConcretoEscaleras'+contCodigoEscaleras+'"> Concreto<br>'+
      '<input type="checkbox" id="PiedraEscaleras'+contCodigoEscaleras+'">Piedra<br>'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="ladrilloEscaleras'+contCodigoEscaleras+'">Ladrillo<br>'+
      '<input type="checkbox" id="tierraEscaleras'+contCodigoEscaleras+'">Tierra<br>'+
      '<input type="checkbox" id="otrosEscaleras'+contCodigoEscaleras+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescEscaleras'+contCodigoEscaleras+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridEscaleras'+contCodigoEscaleras+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaEscaleras('+contCodigoEscaleras+');"></i>'+
      '</a></div>'+
      '</div>';



      var cadenaEscaleras = '';
      var cont = 0;
      arrDibujoEscaleras.forEach(function (item, index, array) 
      {
        arrCodigoEscaleras[cont].descripcionEscaleras = $("#descripcionEscaleras"+item.id+"").val();     
        arrCodigoEscaleras[cont].maderaEscaleras = document.getElementById("maderaEscalerass"+item.id+"").checked; 
        arrCodigoEscaleras[cont].metalEscaleras = document.getElementById("metalEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].ConcretoEscaleras = document.getElementById("ConcretoEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].PiedraEscaleras = document.getElementById("PiedraEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].ladrilloEscaleras = document.getElementById("ladrilloEscaleras"+item.id+"").checked; 
        arrCodigoEscaleras[cont].tierraEscaleras = document.getElementById("tierraEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].otrosEscaleras = document.getElementById("otrosEscaleras"+item.id+"").checked;
        arrCodigoEscaleras[cont].otroDescEscaleras = $("#otrosDescEscaleras"+item.id+"").val();
        arrCodigoEscaleras[cont].integridadEscaleras = $("#integridEscaleras"+item.id+"").val();
        cont++;
      });

      arrCodigoEscaleras.push({descripcionEscaleras: 'N',maderaEscaleras: '', metalEscaleras: '', ConcretoEscaleras: '', PiedraEscaleras: '', ladrilloEscaleras: '', tierraEscaleras: '', otrosEscaleras: '', otroDescEscaleras: '',integridadEscaleras: ''});
      arrDibujoEscaleras.push({scd_codigo:htmlEscaleras, id: contCodigoEscaleras});
      contCodigoEscaleras++;

      cont=1;
      for (var i=0; i<arrDibujoEscaleras.length;i++)
      {
        cadenaEscaleras = cadenaEscaleras + arrDibujoEscaleras[i].scd_codigo;
        cont++;
      }
      
      $('#crearGrillaEscalerasado').html(cadenaEscaleras); 

      var conto=0;
      arrDibujoEscaleras.forEach(function (item, index, array) 
      { 
        $("#descripcionEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].descripcionEscaleras);
        document.getElementById("maderaEscalerass"+item.id+"").checked = (arrCodigoEscaleras[conto].maderaEscaleras);  
        document.getElementById("metalEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].metalEscaleras);
        document.getElementById("ConcretoEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].ConcretoEscaleras);
        document.getElementById("PiedraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].PiedraEscaleras);
        document.getElementById("ladrilloEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].ladrilloEscaleras); 
        document.getElementById("tierraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].tierraEscaleras);
        document.getElementById("otrosEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].otrosEscaleras);
        $("#otrosDescEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].otroDescEscaleras); 
        $("#integridEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].integridadEscaleras); 

        conto=conto+1;
      });

    }


      function eliminarGrillaEscaleras($id){
        if(contCodigoEscaleras>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaEscaleras ='';
            var pos=-1;
            var cont = 0;
            arrDibujoEscaleras.forEach(function (item, index, array) 
            {
              arrCodigoEscaleras[cont].descripcionEscaleras = $("#descripcionEscaleras"+item.id+"").val();     
              arrCodigoEscaleras[cont].maderaEscaleras = document.getElementById("maderaEscalerass"+item.id+"").checked; 
              arrCodigoEscaleras[cont].metalEscaleras = document.getElementById("metalEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].ConcretoEscaleras = document.getElementById("ConcretoEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].PiedraEscaleras = document.getElementById("PiedraEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].ladrilloEscaleras = document.getElementById("ladrilloEscaleras"+item.id+"").checked; 
              arrCodigoEscaleras[cont].tierraEscaleras = document.getElementById("tierraEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].otrosEscaleras = document.getElementById("otrosEscaleras"+item.id+"").checked;
              arrCodigoEscaleras[cont].otroDescEscaleras = $("#otrosDescEscaleras"+item.id+"").val();
              arrCodigoEscaleras[cont].integridadEscaleras = $("#integridEscaleras"+item.id+"").val();
              cont++;
            });
            pos = arrDibujoEscaleras.map(function(e) {return e.id;}).indexOf($id);
            arrCodigoEscaleras.splice(pos,1);
            arrDibujoEscaleras.splice(pos,1);
            cont=1;
            for (var i=0; i<arrDibujoEscaleras.length;i++)
            {
              cadenaEscaleras = cadenaEscaleras + arrDibujoEscaleras[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaEscalerasado').html(cadenaEscaleras);
            var conto=0;
            arrDibujoEscaleras.forEach(function (item, index, array) 
            { 
              $("#descripcionEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].descripcionEscaleras); 
              document.getElementById("maderaEscalerass"+item.id+"").checked = (arrCodigoEscaleras[conto].maderaEscaleras); 
              document.getElementById("metalEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].metalEscaleras);
              document.getElementById("ConcretoEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].ConcretoEscaleras);
              document.getElementById("PiedraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].PiedraEscaleras);
              document.getElementById("ladrilloEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].ladrilloEscaleras); 
              document.getElementById("tierraEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].tierraEscaleras);
              document.getElementById("otrosEscaleras"+item.id+"").checked = (arrCodigoEscaleras[conto].otrosEscaleras);             

              $("#otrosDescEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].otroDescEscaleras); 
              $("#integridEscaleras"+item.id+"").val(arrCodigoEscaleras[conto].integridadEscaleras); 
              conto=conto+1;
            });
          });
        }
      }

  //----editar y actualizar escaleras----------
  var arrDibujoEsca=[];
  var aarrCodigoEscaleras = [];
  var contEactual = 0;

  function editarGrillaEscaleras(longitudGrilla){
    var contEsca=1;
    arrDibujoEsca=[];
    var htmlEsca;
    for (var i=1; i<longitudGrilla;i++){
      htmlEsca ='<div class="col-md-12">'+
         '<div class="form-group col-md-2">'+
         '<select id="descEsca'+contEsca+'" class="form-control">'+
         '<option value="N" selected="selected">..Seleccionar..</option>'+
         '<option value="Recta">Recta</option>'+
         '<option value="Imperial">Imperial</option>'+
         '<option value="Caracol">Caracol</option>'+
         '<option value="U">U</option>'+
         '<option value="L">L</option>'+
         '</select> '+
         '</div>'+
         '<div class="form-group col-md-1">'+
         '</div>'+
         '<div class="form-group col-md-2">'+
         '<input type="checkbox" id="maderaEsca'+contEsca+'">Madera<br>'+
         '<input type="checkbox" id="metalEsca'+contEsca+'">Metal<br>'+
         '<input type="checkbox" id="ConcretoEsca'+contEsca+'"> Concreto<br>'+
         '<input type="checkbox" id="PiedraEsca'+contEsca+'">Piedra<br>'+
         '</div>'+
         '<div class="form-group col-md-2">'+

         '<input type="checkbox" id="ladrilloEsca'+contEsca+'">Ladrillo<br>'+
         '<input type="checkbox" id="tierraEsca'+contEsca+'">Tierra<br>'+
         '<input type="checkbox" id="otrosEsca'+contEsca+'"> Otros'+
         '</div>'+
         '<div class="form-group col-md-2">'+
         '<input type="text" id="otrosDescEsca'+contEsca+'" class="form-control"  placeholder="Otras Descrip.">'+
         '</div>'+
         '<div class="form-group col-md-2">'+
         '<select id="integridEsca'+contEsca+'" class="form-control">'+
         '<option value="N" selected="selected">....Seleccionar...</option>'+
         '<option value="Original">Original</option>'+
         '<option value="Nuevo">Nuevo</option>'+
         '<option value="Modificado">Modificado</option>'+
         '<option value="Original y Nuevo">Original y Nuevo</option>'+
         '</select> '+
         '</div>'+
         '<div class="col-md-1">'+
         '<a style="cursor:pointer;" type="button">'+
         '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarEscaleras('+contEsca+');"></i>'+
         '</a></div>'+
         '</div>';
         arrDibujoEsca.push({scd_codigo:htmlEsca,id: contEsca});
          contEsca++;
          }
          var cadenaEsca='';
          var cont=1;
          for (var i=0; i<arrDibujoEsca.length;i++)
          {
            cadenaEsca=cadenaEsca + arrDibujoEsca[i].scd_codigo;
            cont++;
          }
          $('#acrearGrillaEscalerasado').html(cadenaEsca);
        }


         
function actualizarEscaleras(){
      var htmlEscAct;
      htmlEscAct='<div class="col-md-12">'+
         '<div class="form-group col-md-2">'+
         '<select id="descEsca'+contEactual+'" class="form-control">'+
         '<option value="N" selected="selected">..Seleccionar..</option>'+
         '<option value="Recta">Recta</option>'+
         '<option value="Imperial">Imperial</option>'+
         '<option value="Caracol">Caracol</option>'+
         '<option value="U">U</option>'+
         '<option value="L">L</option>'+
         '</select> '+
         '</div>'+
         '<div class="form-group col-md-1">'+
         '</div>'+
         '<div class="form-group col-md-2">'+
         '<input type="checkbox" id="maderaEsca'+contEactual+'">Madera<br>'+
         '<input type="checkbox" id="metalEsca'+contEactual+'">Metal<br>'+
         '<input type="checkbox" id="ConcretoEsca'+contEactual+'"> Concreto<br>'+
         '<input type="checkbox" id="PiedraEsca'+contEactual+'">Piedra<br>'+
         '</div>'+
         '<div class="form-group col-md-2">'+

         '<input type="checkbox" id="ladrilloEsca'+contEactual+'">Ladrillo<br>'+
         '<input type="checkbox" id="tierraEsca'+contEactual+'">Tierra<br>'+
         '<input type="checkbox" id="otrosEsca'+contEactual+'"> Otros'+
         '</div>'+
         '<div class="form-group col-md-2">'+
         '<input type="text" id="otrosDescEsca'+contEactual+'" class="form-control"  placeholder="Otras Descrip.">'+
         '</div>'+
         '<div class="form-group col-md-2">'+
         '<select id="integridEsca'+contEactual+'" class="form-control">'+
         '<option value="N" selected="selected">....Seleccionar...</option>'+
         '<option value="Original">Original</option>'+
         '<option value="Nuevo">Nuevo</option>'+
         '<option value="Modificado">Modificado</option>'+
         '<option value="Original y Nuevo">Original y Nuevo</option>'+
         '</select> '+
         '</div>'+
         '<div class="col-md-1">'+
         '<a style="cursor:pointer;" type="button">'+
         '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarEscaleras('+contEactual+');"></i>'+
         '</a></div>'+
         '</div>';
      var cadenaEscaleras = '';
      var cont = 0;
      arrDibujoEsca.forEach(function (item, index, array) 
      {
        aarrCodigoEscaleras[cont].adescripcionEscaleras = $("#descEsca"+item.id+"").val();     
        aarrCodigoEscaleras[cont].amaderaEscaleras = document.getElementById("maderaEsca"+item.id+"").checked; 
        aarrCodigoEscaleras[cont].ametalEscaleras = document.getElementById("metalEsca"+item.id+"").checked;
        aarrCodigoEscaleras[cont].aConcretoEscaleras = document.getElementById("ConcretoEsca"+item.id+"").checked;
        aarrCodigoEscaleras[cont].aPiedraEscaleras = document.getElementById("PiedraEsca"+item.id+"").checked;
        aarrCodigoEscaleras[cont].aladrilloEscaleras = document.getElementById("ladrilloEsca"+item.id+"").checked; 
        aarrCodigoEscaleras[cont].atierraEscaleras = document.getElementById("tierraEsca"+item.id+"").checked;
        aarrCodigoEscaleras[cont].aotrosEscaleras = document.getElementById("otrosEsca"+item.id+"").checked;
        aarrCodigoEscaleras[cont].aotroDescEscaleras = $("#otrosDescEsca"+item.id+"").val();
        aarrCodigoEscaleras[cont].aintegridadEscaleras = $("#integridEsca"+item.id+"").val();
        cont++;
      });

      aarrCodigoEscaleras.push({adescripcionEscaleras: 'N',amaderaEscaleras: '', ametalEscaleras: '', aConcretoEscaleras: '', aPiedraEscaleras: '', aladrilloEscaleras: '', atierraEscaleras: '', aotrosEscaleras: '', aotroDescEscaleras: '', aintegridadEscaleras: ''});
      arrDibujoEsca.push({scd_codigo:htmlEscAct, id: contEactual});
      contEactual++;

      cont=1;
      for (var i=0; i<arrDibujoEsca.length;i++)
      {
        cadenaEscaleras = cadenaEscaleras + arrDibujoEsca[i].scd_codigo;
        cont++;
      }
      
      $('#acrearGrillaEscalerasado').html(cadenaEscaleras); 

      var conto=0;
      arrDibujoEsca.forEach(function (item, index, array) 
      { 
        $("#descEsca"+item.id+"").val(aarrCodigoEscaleras[conto].adescripcionEscaleras);
        document.getElementById("maderaEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].amaderaEscaleras);  
        document.getElementById("metalEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].ametalEscaleras);
        document.getElementById("ConcretoEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aConcretoEscaleras);
        document.getElementById("PiedraEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aPiedraEscaleras);
        document.getElementById("ladrilloEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aladrilloEscaleras); 
        document.getElementById("tierraEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].atierraEscaleras);
        document.getElementById("otrosEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aotrosEscaleras);
        $("#otrosDescEsca"+item.id+"").val(aarrCodigoEscaleras[conto].aotroDescEscaleras); 
        $("#integridEsca"+item.id+"").val(aarrCodigoEscaleras[conto].aintegridadEscaleras); 
        conto=conto+1;
      });

    }

      function eliminarEscaleras($id){
        if(contEactual>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaEscaleras ='';
            var pos=-1;
            var cont = 0;
            arrDibujoEsca.forEach(function (item, index, array) 
            {
              aarrCodigoEscaleras[cont].adescripcionEscaleras = $("#descEsca"+item.id+"").val();     
              aarrCodigoEscaleras[cont].amaderaEscaleras = document.getElementById("maderaEsca"+item.id+"").checked; 
              aarrCodigoEscaleras[cont].ametalEscaleras = document.getElementById("metalEsca"+item.id+"").checked;
              aarrCodigoEscaleras[cont].aConcretoEscaleras = document.getElementById("ConcretoEsca"+item.id+"").checked;
              aarrCodigoEscaleras[cont].aPiedraEscaleras = document.getElementById("PiedraEsca"+item.id+"").checked;
              aarrCodigoEscaleras[cont].aladrilloEscaleras = document.getElementById("ladrilloEsca"+item.id+"").checked; 
              aarrCodigoEscaleras[cont].atierraEscaleras = document.getElementById("tierraEsca"+item.id+"").checked;
              aarrCodigoEscaleras[cont].aotrosEscaleras = document.getElementById("otrosEsca"+item.id+"").checked;
              aarrCodigoEscaleras[cont].aotroDescEscaleras = $("#otrosDescEsca"+item.id+"").val();
              aarrCodigoEscaleras[cont].aintegridadEscaleras = $("#integridEsca"+item.id+"").val();
              cont++;
            });
            pos = arrDibujoEsca.map(function(e) {return e.id;}).indexOf($id);
            aarrCodigoEscaleras.splice(pos,1);
            arrDibujoEsca.splice(pos,1);
            cont=1;
            for (var i=0; i<arrDibujoEsca.length;i++)
            {
              cadenaEscaleras = cadenaEscaleras + arrDibujoEsca[i].scd_codigo;
              cont++;
            }
            $('#acrearGrillaEscalerasado').html(cadenaEscaleras);
            var conto=0;
            arrDibujoEsca.forEach(function (item, index, array) 
            { 
            $("#descEsca"+item.id+"").val(aarrCodigoEscaleras[conto].adescripcionEscaleras);
            document.getElementById("maderaEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].amaderaEscaleras);  
            document.getElementById("metalEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].ametalEscaleras);
            document.getElementById("ConcretoEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aConcretoEscaleras);
            document.getElementById("PiedraEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aPiedraEscaleras);
            document.getElementById("ladrilloEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aladrilloEscaleras); 
            document.getElementById("tierraEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].atierraEscaleras);
            document.getElementById("otrosEsca"+item.id+"").checked = (aarrCodigoEscaleras[conto].aotrosEscaleras);
            $("#otrosDescEsca"+item.id+"").val(aarrCodigoEscaleras[conto].aotroDescEscaleras); 
            $("#integridEsca"+item.id+"").val(aarrCodigoEscaleras[conto].aintegridadEscaleras); 
            conto=conto+1;
            });
          });
        }
      }
//--------------------GRILLA Escalesras FIN-------------------//
//--------------------GRILLA CARACTERISTICAS DE LA VEGETACION--------------------//

var contCodigoCarVegetacion = 2;
var arrCodigoCarVegetacion = [];
var arrDibujoCarVegetacion = [];
 htmlCarVegetacion='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionCarVegetacion1" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="Arboles">Arboles</option>'+
  '<option value="Arbustos">Arbustos</option>'+
  '<option value="Trepadoras">Trepadoras</option>'+
  '<option value="Herbaceas">Herbaceas</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="cantidadCarVegetacion1" class="form-control">'+
  '</div>'+
  '<div class="form-group col-md-1">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="checkbox" id="sinFloracionCarVegetacion1">Sin Floracion<br>'+
  '<input type="checkbox" id="caducaCarVegetacion1">Caduca<br>'+
  '<input type="checkbox" id="perenneCarVegetacion1">Perenne<br>'+
  '<input type="checkbox" id="otrosCarVegetacion1"> Otros'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="otrosDescCarVegetacion1" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridCarVegetacion1" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="Original">Original</option>'+
  '<option value="Nuevo">Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '<option value="Original y Nuevo">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaCarVegetacion(1);"></i>'+
  '</a></div>'+
  '</div>';


var cadenaCarVegetacion = '';  
arrCodigoCarVegetacion.push({descripcionCarVegetacion: 'N', cantidadCarVegetacion: '', sinFloracionCarVegetacion: '', caducaCarVegetacion: '', perenneCarVegetacion: '', otrosCarVegetacion: '', otroDescCarVegetacion: '', integridadCarVegetacion: ''});
arrDibujoCarVegetacion.push({scd_codigo:htmlCarVegetacion,id: 1});


cadenaCarVegetacion=arrDibujoCarVegetacion[0].scd_codigo;
//console.log("cadenaCarVegetacion",cadenaCarVegetacion);
$('#crearGrillaCarVegetacionado').html(cadenaCarVegetacion);

$("#descripcionCarVegetacion1").val(arrCodigoCarVegetacion[0].descripcionCarVegetacion);
$("#cantidadCarVegetacion1").val(arrCodigoCarVegetacion[0].cantidadCarVegetacion);  
document.getElementById("sinFloracionCarVegetacion1").checked = (arrCodigoCarVegetacion[0].sinFloracionCarVegetacion);
document.getElementById("caducaCarVegetacion1").checked = (arrCodigoCarVegetacion[0].caducaCarVegetacion); 
document.getElementById("perenneCarVegetacion1").checked = (arrCodigoCarVegetacion[0].perenneCarVegetacion);
document.getElementById("otrosCarVegetacion1").checked = (arrCodigoCarVegetacion[0].otrosCarVegetacion);
$("#otrosDescCarVegetacion1").val(arrCodigoCarVegetacion[0].otroDescCarVegetacion); 
$("#integridCarVegetacion1").val(arrCodigoCarVegetacion[0].integridadCarVegetacion);


function grillaCarVegetacion(){
  var htmlCarVegetacion;
  htmlCarVegetacion='<div class="col-md-12">'+
  '<div class="form-group col-md-2">'+
  '<select id="descripcionCarVegetacion'+contCodigoCarVegetacion+'" class="form-control">'+
  '<option value="N" selected="selected">..Seleccionar..</option>'+
  '<option value="Arboles">Arboles</option>'+
  '<option value="Arbustos">Arbustos</option>'+
  '<option value="Trepadoras">Trepadoras</option>'+
  '<option value="Herbaceas">Herbaceas</option>'+
  '</select> '+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="cantidadCarVegetacion'+contCodigoCarVegetacion+'" class="form-control">'+
  '</div>'+
  '<div class="form-group col-md-1">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="checkbox" id="sinFloracionCarVegetacion'+contCodigoCarVegetacion+'">Sin Floracion<br>'+
  '<input type="checkbox" id="caducaCarVegetacion'+contCodigoCarVegetacion+'">Caduca<br>'+
  '<input type="checkbox" id="perenneCarVegetacion'+contCodigoCarVegetacion+'">Perenne<br>'+
  '<input type="checkbox" id="otrosCarVegetacion'+contCodigoCarVegetacion+'"> Otros'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<input type="text" id="otrosDescCarVegetacion'+contCodigoCarVegetacion+'" class="form-control"  placeholder="Otras Descrip.">'+
  '</div>'+
  '<div class="form-group col-md-2">'+
  '<select id="integridCarVegetacion'+contCodigoCarVegetacion+'" class="form-control">'+
  '<option value="N" selected="selected">....Seleccionar...</option>'+
  '<option value="Original">Original</option>'+
  '<option value="Nuevo">Nuevo</option>'+
  '<option value="Modificado">Modificado</option>'+
  '<option value="Original y Nuevo">Original y Nuevo</option>'+
  '</select> '+
  '</div>'+
  '<div class="col-md-1">'+
  '<a style="cursor:pointer;" type="button">'+
  '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaCarVegetacion('+contCodigoCarVegetacion+');"></i>'+
  '</a></div>'+
  '</div>';

  var cadenaCarVegetacion = '';
  var cont = 0;
  arrDibujoCarVegetacion.forEach(function (item, index, array) {
    arrCodigoCarVegetacion[cont].descripcionCarVegetacion = $("#descripcionCarVegetacion"+item.id+"").val();     
    arrCodigoCarVegetacion[cont].cantidadCarVegetacion = $("#cantidadCarVegetacion"+item.id+"").val();
    arrCodigoCarVegetacion[cont].sinFloracionCarVegetacion = document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked;
    arrCodigoCarVegetacion[cont].caducaCarVegetacion = document.getElementById("caducaCarVegetacion"+item.id+"").checked; 
    arrCodigoCarVegetacion[cont].perenneCarVegetacion = document.getElementById("perenneCarVegetacion"+item.id+"").checked;
    arrCodigoCarVegetacion[cont].otrosCarVegetacion = document.getElementById("otrosCarVegetacion"+item.id+"").checked;
    arrCodigoCarVegetacion[cont].otroDescCarVegetacion = $("#otrosDescCarVegetacion"+item.id+"").val();
    arrCodigoCarVegetacion[cont].integridadCarVegetacion = $("#integridCarVegetacion"+item.id+"").val();
    cont++;
  });

  arrCodigoCarVegetacion.push({descripcionCarVegetacion: 'N', cantidadCarVegetacion: '', sinFloracionCarVegetacion: '', caducaCarVegetacion: '', perenneCarVegetacion: '', otrosCarVegetacion: '', otroDescCarVegetacion: '', integridadCarVegetacion: ''});
  arrDibujoCarVegetacion.push({scd_codigo:htmlCarVegetacion, id: contCodigoCarVegetacion});
  contCodigoCarVegetacion++;

  cont=1;
  for (var i=0; i<arrDibujoCarVegetacion.length;i++)
  {
    cadenaCarVegetacion = cadenaCarVegetacion + arrDibujoCarVegetacion[i].scd_codigo;
    cont++;
  }
  
  $('#crearGrillaCarVegetacionado').html(cadenaCarVegetacion); 

  var conto=0;
  arrDibujoCarVegetacion.forEach(function (item, index, array) 
  { 

    $("#descripcionCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].descripcionCarVegetacion);
    $("#cantidadCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].cantidadCarVegetacion);  
    document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].sinFloracionCarVegetacion);
    document.getElementById("caducaCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].caducaCarVegetacion); 
    document.getElementById("perenneCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].perenneCarVegetacion);
    document.getElementById("otrosCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].otrosCarVegetacion);
    $("#otrosDescCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].otroDescCarVegetacion); 
    $("#integridCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].integridadCarVegetacion);
    conto=conto+1;
  });
}


function eliminarGrillaCarVegetacion($id){
  if(contCodigoCarVegetacion>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaCarVegetacion ='';
      var pos=-1;
      var cont = 0;
      arrDibujoCarVegetacion.forEach(function (item, index, array) 
      {
        arrCodigoCarVegetacion[cont].descripcionCarVegetacion = $("#descripcionCarVegetacion"+item.id+"").val();     
        arrCodigoCarVegetacion[cont].cantidadCarVegetacion = $("#cantidadCarVegetacion"+item.id+"").val();
        arrCodigoCarVegetacion[cont].sinFloracionCarVegetacion = document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked;
        arrCodigoCarVegetacion[cont].caducaCarVegetacion = document.getElementById("caducaCarVegetacion"+item.id+"").checked; 
        arrCodigoCarVegetacion[cont].perenneCarVegetacion = document.getElementById("perenneCarVegetacion"+item.id+"").checked;
        arrCodigoCarVegetacion[cont].otrosCarVegetacion = document.getElementById("otrosCarVegetacion"+item.id+"").checked;
        arrCodigoCarVegetacion[cont].otroDescCarVegetacion = $("#otrosDescCarVegetacion"+item.id+"").val();
        arrCodigoCarVegetacion[cont].integridadCarVegetacion = $("#integridCarVegetacion"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoCarVegetacion.map(function(e) {return e.id;}).indexOf($id);
      //console.log("poos",pos);
      arrCodigoCarVegetacion.splice(pos,1);
      arrDibujoCarVegetacion.splice(pos,1);
      //console.log("arrCodigo",arrCodigoCarVegetacion);

      cont=1;
      for (var i=0; i<arrDibujoCarVegetacion.length;i++)
      {
        cadenaCarVegetacion = cadenaCarVegetacion + arrDibujoCarVegetacion[i].scd_codigo;
        cont++;
      }
      $('#crearGrillaCarVegetacionado').html(cadenaCarVegetacion);
      var conto=0;
      arrDibujoCarVegetacion.forEach(function (item, index, array) 
      {    
      $("#descripcionCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].descripcionCarVegetacion);
      $("#cantidadCarVegetacion"+item.id+"").val(arrCodigoCarVegetacion[conto].cantidadCarVegetacion);  
      document.getElementById("sinFloracionCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].sinFloracionCarVegetacion);
      document.getElementById("caducaCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].caducaCarVegetacion); 
      document.getElementById("perenneCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].perenneCarVegetacion);
      document.getElementById("otrosCarVegetacion"+item.id+"").checked = (arrCodigoCarVegetacion[conto].otrosCarVegetacion);
      $("#otrosDescCarVegetacion").val(arrCodigoCarVegetacion[conto].otroDescCarVegetacion); 
      $("#integridCarVegetacion").val(arrCodigoCarVegetacion[conto].integridadCarVegetacion);
        conto=conto+1;
      });
    });
  }
}

//----editar y actualizar características de la vegetación-------
var arrDibujoCVeg=[];
var aarrCodigoCarVegetacion = [];
var contVactualizar = 0;
function editarGrillaCarVegetacion(longitudGrilla){
  var contCVeg = 1;
  arrDibujoCVeg = [];
  var htmlCVeg;
  for (var i=1; i<longitudGrilla;i++){
    htmlCVeg ='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descCVeg'+contCVeg+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Arboles">Arboles</option>'+
      '<option value="Arbustos">Arbustos</option>'+
      '<option value="Trepadoras">Trepadoras</option>'+
      '<option value="Herbaceas">Herbaceas</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="cantidadCVeg'+contCVeg+'" class="form-control">'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="sinFlorCVeg'+contCVeg+'">Sin Floracion<br>'+
      '<input type="checkbox" id="caducaCVeg'+contCVeg+'">Caduca<br>'+
      '<input type="checkbox" id="perenneCVeg'+contCVeg+'">Perenne<br>'+
      '<input type="checkbox" id="otrosCVeg'+contCVeg+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescCVeg'+contCVeg+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridCVeg'+contCVeg+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarCarVegetacion('+contCVeg+');"></i>'+
      '</a></div>'+
      '</div>';
       arrDibujoCVeg.push({scd_codigo:htmlCVeg,id: contCVeg});
        contCVeg++;
        }
        var cadenaCVeg='';
        var cont=1;
        for (var i=0; i<arrDibujoCVeg.length;i++)
        {
          cadenaCVeg=cadenaCVeg + arrDibujoCVeg[i].scd_codigo;
          cont++;
        }
        $('#acrearGrillaCarVegetacionado').html(cadenaCVeg);
  }

function actualizarCarVegetacion(){
  var htmlCVact;
  htmlCVact='<div class="col-md-12">'+
      '<div class="form-group col-md-2">'+
      '<select id="descCVeg'+contVactualizar+'" class="form-control">'+
      '<option value="N" selected="selected">..Seleccionar..</option>'+
      '<option value="Arboles">Arboles</option>'+
      '<option value="Arbustos">Arbustos</option>'+
      '<option value="Trepadoras">Trepadoras</option>'+
      '<option value="Herbaceas">Herbaceas</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="cantidadCVeg'+contVactualizar+'" class="form-control">'+
      '</div>'+
      '<div class="form-group col-md-1">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="checkbox" id="sinFlorCVeg'+contVactualizar+'">Sin Floracion<br>'+
      '<input type="checkbox" id="caducaCVeg'+contVactualizar+'">Caduca<br>'+
      '<input type="checkbox" id="perenneCVeg'+contVactualizar+'">Perenne<br>'+
      '<input type="checkbox" id="otrosCVeg'+contVactualizar+'"> Otros'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<input type="text" id="otrosDescCVeg'+contVactualizar+'" class="form-control"  placeholder="Otras Descrip.">'+
      '</div>'+
      '<div class="form-group col-md-2">'+
      '<select id="integridCVeg'+contVactualizar+'" class="form-control">'+
      '<option value="N" selected="selected">....Seleccionar...</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-1">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarCarVegetacion('+contVactualizar+');"></i>'+
      '</a></div>'+
      '</div>';
  var cadenaCarVegetacion = '';
  var cont = 0;
    arrDibujoCVeg.forEach(function (item, index, array) {
    aarrCodigoCarVegetacion[cont].adescripcionCarVegetacion = $("#descCVeg"+item.id+"").val();     
    aarrCodigoCarVegetacion[cont].acantidadCarVegetacion = $("#cantidadCVeg"+item.id+"").val();
    aarrCodigoCarVegetacion[cont].asinFloracionCarVegetacion = document.getElementById("sinFlorCVeg"+item.id+"").checked;
    aarrCodigoCarVegetacion[cont].acaducaCarVegetacion = document.getElementById("caducaCVeg"+item.id+"").checked; 
    aarrCodigoCarVegetacion[cont].aperenneCarVegetacion = document.getElementById("perenneCVeg"+item.id+"").checked;
    aarrCodigoCarVegetacion[cont].aotrosCarVegetacion = document.getElementById("otrosCVeg"+item.id+"").checked;
    aarrCodigoCarVegetacion[cont].aotroDescCarVegetacion = $("#otrosDescCVeg"+item.id+"").val();
    aarrCodigoCarVegetacion[cont].aintegridadCarVegetacion = $("#integridCVeg"+item.id+"").val();
    cont++;
  });

  aarrCodigoCarVegetacion.push({adescripcionCarVegetacion: 'N', acantidadCarVegetacion: '', asinFloracionCarVegetacion: '', acaducaCarVegetacion: '', aperenneCarVegetacion: '', aotrosCarVegetacion: '', aotroDescCarVegetacion: '', aintegridadCarVegetacion: ''});
  arrDibujoCVeg.push({scd_codigo:htmlCVact, id: contVactualizar});
  contVactualizar++;

  cont=1;
  for (var i=0; i<arrDibujoCVeg.length;i++)
  {
    cadenaCarVegetacion = cadenaCarVegetacion + arrDibujoCVeg[i].scd_codigo;
    cont++;
  }
  
  $('#acrearGrillaCarVegetacionado').html(cadenaCarVegetacion); 

  var conto=0;
  arrDibujoCVeg.forEach(function (item, index, array) 
  { 

    $("#descCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].adescripcionCarVegetacion);
    $("#cantidadCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].acantidadCarVegetacion);  
    document.getElementById("sinFlorCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].asinFloracionCarVegetacion);
    document.getElementById("caducaCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].acaducaCarVegetacion); 
    document.getElementById("perenneCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].aperenneCarVegetacion);
    document.getElementById("otrosCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].aotrosCarVegetacion);
    $("#otrosDescCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].aotroDescCarVegetacion); 
    $("#integridCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].aintegridadCarVegetacion);
    conto=conto+1;
  });
}


function eliminarCarVegetacion($id){
  if(contVactualizar>2)
  {
    swal({title: "Esta seguro de eliminar?",
      text: "Se eliminará el argumento seleccionado!",
      type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
    }, function(){
      var cadenaCarVegetacion ='';
      var pos=-1;
      var cont = 0;
      arrDibujoCVeg.forEach(function (item, index, array) 
      {
      aarrCodigoCarVegetacion[cont].adescripcionCarVegetacion = $("#descCVeg"+item.id+"").val();     
      aarrCodigoCarVegetacion[cont].acantidadCarVegetacion = $("#cantidadCVeg"+item.id+"").val();
      aarrCodigoCarVegetacion[cont].asinFloracionCarVegetacion = document.getElementById("sinFlorCVeg"+item.id+"").checked;
      aarrCodigoCarVegetacion[cont].acaducaCarVegetacion = document.getElementById("caducaCVeg"+item.id+"").checked; 
      aarrCodigoCarVegetacion[cont].aperenneCarVegetacion = document.getElementById("perenneCVeg"+item.id+"").checked;
      aarrCodigoCarVegetacion[cont].aotrosCarVegetacion = document.getElementById("otrosCVeg"+item.id+"").checked;
      aarrCodigoCarVegetacion[cont].aotroDescCarVegetacion = $("#otrosDescCVeg"+item.id+"").val();
      aarrCodigoCarVegetacion[cont].aintegridadCarVegetacion = $("#integridCVeg"+item.id+"").val();
        cont++;
      });
      pos = arrDibujoCVeg.map(function(e) {return e.id;}).indexOf($id);
      aarrCodigoCarVegetacion.splice(pos,1);
      arrDibujoCVeg.splice(pos,1);
      cont=1;
      for (var i=0; i<arrDibujoCVeg.length;i++)
      {
        cadenaCarVegetacion = cadenaCarVegetacion + arrDibujoCVeg[i].scd_codigo;
        cont++;
      }
      $('#acrearGrillaCarVegetacionado').html(cadenaCarVegetacion);
      var conto=0;
      arrDibujoCVeg.forEach(function (item, index, array) 
      { 
      $("#descCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].adescripcionCarVegetacion);
      $("#cantidadCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].acantidadCarVegetacion);  
      document.getElementById("sinFlorCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].asinFloracionCarVegetacion);
      document.getElementById("caducaCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].acaducaCarVegetacion); 
      document.getElementById("perenneCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].aperenneCarVegetacion);
      document.getElementById("otrosCVeg"+item.id+"").checked = (aarrCodigoCarVegetacion[conto].aotrosCarVegetacion);
      $("#otrosDescCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].aotroDescCarVegetacion); 
      $("#integridCVeg"+item.id+"").val(aarrCodigoCarVegetacion[conto].aintegridadCarVegetacion);
        conto=conto+1;
      });
    });
  }
}

    //--------------------GRILLA CARCATERISTICAS DE LA VEGETACION FIN-------------------//
    //--------------------GRILLA Detalles Artisitcos--------------------//
    var contCodigoDetaArtistoco = 2;
    var arrCodigoDetaArtistoco = [];
    var arrDibujoDetaArtistoco = [];
    htmlDetaArtistoco='<div class="col-md-12">'+
    '<div class="form-group col-md-5">'+
    '<select id="descripcionDetaArtistoco1" class="form-control">'+
    '<option value="D" selected="selected">..Seleccionar..</option>'+
    '<option value="Pinturas Mural">Pinturas Mural</option>'+
    '<option value="Esculuturas">Esculuturas</option>'+
    '<option value="Caracol">Caracol</option>'+
    '<option value="Enchapes de Ceramica">Enchapes de Ceramica</option>'+
    '<option value="Herrajes">Herrajes</option>'+
    '<option value="Portada">Portada</option>'+
    '</select> '+
    '</div>'+
    '<div class="form-group col-md-5">'+
    '<select id="integridDetaArtistoco1" class="form-control">'+
    '<option value="I" selected="selected">....Seleccionar...</option>'+
    '<option value="Original">Original</option>'+
    '<option value="Nuevo">Nuevo</option>'+
    '<option value="Modificado">Modificado</option>'+
    '<option value="Original y Nuevo">Original y Nuevo</option>'+
    '</select> '+
    '</div>'+
    '<div class="col-md-2">'+
    '<a style="cursor:pointer;" type="button">'+
    '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaDetaArtistoco(1);"></i>'+
    '</a></div>'+
    '</div>';
    var cadenaDetaArtistoco = '';  
    arrCodigoDetaArtistoco.push({descripcionDetalleArtistoco: 'D',integridadDetaArtistoco: 'I'});
    arrDibujoDetaArtistoco.push({scd_codigo:htmlDetaArtistoco,id: 1});
    //console.log("arrCodigo",arrCodigoUso);
    cadenaDetaArtistoco=arrDibujoDetaArtistoco[0].scd_codigo;
    console.log("cadenaDetaArtistoco",cadenaDetaArtistoco);
    $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco);
    $("#descripcionDetaArtistoco1").val(arrCodigoDetaArtistoco[0].descripcionDetalleArtistoco);
    $("#integridDetaArtistoco1").val(arrCodigoDetaArtistoco[0].integridadDetaArtistoco);


    function grillaDetaArtistoco(){
      var htmlDetaArtistoco;
      htmlDetaArtistoco='<div class="col-md-12">'+
      '<div class="form-group col-md-5">'+
      '<select id="descripcionDetaArtistoco'+contCodigoDetaArtistoco+'" class="form-control">'+
      '<option value="D" selected="selected">..Seleccionar..</option>'+
      '<option value="Pinturas Mural">Pinturas Mural</option>'+
      '<option value="Esculuturas">Esculuturas</option>'+
      '<option value="Caracol">Caracol</option>'+
      '<option value="Enchapes de Ceramica">Enchapes de Ceramica</option>'+
      '<option value="Herrajes">Herrajes</option>'+
      '<option value="Portada">Portada</option>'+
      '</select> '+
      '</div>'+
      '<div class="form-group col-md-5">'+
      '<select id="integridDetaArtistoco'+contCodigoDetaArtistoco+'" class="form-control">'+
      '<option value="I" selected="selected">....Seleccionar...</option>'+
      '<option value="Original">Original</option>'+
      '<option value="Nuevo">Nuevo</option>'+
      '<option value="Modificado">Modificado</option>'+
      '<option value="Original y Nuevo">Original y Nuevo</option>'+
      '</select> '+
      '</div>'+
      '<div class="col-md-2">'+
      '<a style="cursor:pointer;" type="button">'+
      '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarGrillaDetaArtistoco('+contCodigoDetaArtistoco+');"></i>'+
      '</a></div>'+
      '</div>';
      var cadenaDetaArtistoco = '';
      var cont = 0;
      arrDibujoDetaArtistoco.forEach(function (item, index, array) 
      {
        arrCodigoDetaArtistoco[cont].descripcionDetalleArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
        arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
        cont++;
      });
      arrCodigoDetaArtistoco.push({descripcionDetalleArtistoco: 'D', integridadDetaArtistoco: 'I'});
      arrDibujoDetaArtistoco.push({scd_codigo:htmlDetaArtistoco, id: contCodigoDetaArtistoco});
      contCodigoDetaArtistoco++;
      cont=1;
      for (var i=0; i<arrDibujoDetaArtistoco.length;i++)
      {
        cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDetaArtistoco[i].scd_codigo;
        cont++;
      }  
      $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco); 
      var contda=0;
      arrDibujoDetaArtistoco.forEach(function (item, index, array) 
      { 
        $("#descripcionDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[contda].descripcionDetalleArtistoco); 
        $("#integridDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[contda].integridadDetaArtistoco); 

        contda=contda+1;
      });

      }


      function eliminarGrillaDetaArtistoco($id){
        if(contCodigoDetaArtistoco>2)
        {
          swal({title: "Esta seguro de eliminar?",
            text: "Se eliminará el argumento seleccionado!",
            type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
          }, function(){
            var cadenaDetaArtistoco ='';
            var pos=-1;
            var cont = 0;
            arrDibujoDetaArtistoco.forEach(function (item, index, array) 
            {
              arrCodigoDetaArtistoco[cont].descripcionDetaArtistoco = $("#descripcionDetaArtistoco"+item.id+"").val();     
              arrCodigoDetaArtistoco[cont].integridadDetaArtistoco = $("#integridDetaArtistoco"+item.id+"").val();
              cont++;
            });
            pos = arrDibujoDetaArtistoco.map(function(e) {return e.id;}).indexOf($id);
            arrCodigoDetaArtistoco.splice(pos,1);
            arrDibujoDetaArtistoco.splice(pos,1);
            cont=1;
            for (var i=0; i<arrDibujoDetaArtistoco.length;i++)
            {
              cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDetaArtistoco[i].scd_codigo;
              cont++;
            }
            $('#crearGrillaDetaArtistocoado').html(cadenaDetaArtistoco);
            var conto=0;
            arrDibujoDetaArtistoco.forEach(function (item, index, array) 
            { 
              $("#descripcionDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].descripcionDetaArtistoco); 
              $("#integridDetaArtistoco"+item.id+"").val(arrCodigoDetaArtistoco[conto].integridadDetaArtistoco); 
              conto=conto+1;
            });
          });
        }
    }

//----editar y actualizar detalle artístico----------------------
var arrDibujoDArt = [];
var aarrCodigoDetaArtistoco = [];
var contDAact = 0;
  function editarDetaArtistoco(longitudGrilla){
    var contDArt = 1;
    arrDibujoDArt = [];
    var htmlDArt;
    for (var i=1; i<longitudGrilla;i++){
      htmlDArt ='<div class="col-md-12">'+
        '<div class="form-group col-md-5">'+
        '<select id="descripDA'+contDArt+'" class="form-control">'+
        '<option value="D" selected="selected">..Seleccionar..</option>'+
        '<option value="Pinturas Mural">Pinturas Mural</option>'+
        '<option value="Esculuturas">Esculuturas</option>'+
        '<option value="Caracol">Caracol</option>'+
        '<option value="Enchapes de Ceramica">Enchapes de Ceramica</option>'+
        '<option value="Herrajes">Herrajes</option>'+
        '<option value="Portada">Portada</option>'+
        '</select> '+
        '</div>'+
        '<div class="form-group col-md-5">'+
        '<select id="integridadDA'+contDArt+'" class="form-control">'+
        '<option value="I" selected="selected">....Seleccionar...</option>'+
        '<option value="Original">Original</option>'+
        '<option value="Nuevo">Nuevo</option>'+
        '<option value="Modificado">Modificado</option>'+
        '<option value="Original y Nuevo">Original y Nuevo</option>'+
        '</select> '+
        '</div>'+
        '<div class="col-md-2">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarDetaArtistoco('+contDArt+');"></i>'+
        '</a></div>'+
        '</div>';
         arrDibujoDArt.push({scd_codigo:htmlDArt,id: contDArt});
          contDArt++;
          }
          var cadenaDArt='';
          var cont=1;
          for (var i=0; i<arrDibujoDArt.length;i++)
          {
            cadenaDArt=cadenaDArt + arrDibujoDArt[i].scd_codigo;
            cont++;
          }
          $('#acrearGrillaDetaArtistocoado').html(cadenaDArt);
        }

  function actualizarDetaArtistoco(){
      var htmlDAact;
      htmlDAact='<div class="col-md-12">'+
        '<div class="form-group col-md-5">'+
        '<select id="descripDA'+contDAact+'" class="form-control">'+
        '<option value="D" selected="selected">..Seleccionar..</option>'+
        '<option value="Pinturas Mural">Pinturas Mural</option>'+
        '<option value="Esculuturas">Esculuturas</option>'+
        '<option value="Caracol">Caracol</option>'+
        '<option value="Enchapes de Ceramica">Enchapes de Ceramica</option>'+
        '<option value="Herrajes">Herrajes</option>'+
        '<option value="Portada">Portada</option>'+
        '</select> '+
        '</div>'+
        '<div class="form-group col-md-5">'+
        '<select id="integridadDA'+contDAact+'" class="form-control">'+
        '<option value="I" selected="selected">....Seleccionar...</option>'+
        '<option value="Original">Original</option>'+
        '<option value="Nuevo">Nuevo</option>'+
        '<option value="Modificado">Modificado</option>'+
        '<option value="Original y Nuevo">Original y Nuevo</option>'+
        '</select> '+
        '</div>'+
        '<div class="col-md-2">'+
        '<a style="cursor:pointer;" type="button">'+
        '<i class="fa fa-trash fa-2x fa-fw" style="color:#ef5350" title="Eliminar argumento" onclick="eliminarDetaArtistoco('+contDAact+');"></i>'+
        '</a></div>'+
        '</div>';
      var cadenaDetaArtistoco = '';
      var cont = 0;
      arrDibujoDArt.forEach(function (item, index, array) 
      {
        aarrCodigoDetaArtistoco[cont].adescripcionDetalleArtistoco = $("#descripDA"+item.id+"").val();     
        aarrCodigoDetaArtistoco[cont].aintegridadDetaArtistoco = $("#integridadDA"+item.id+"").val();
        cont++;
      });
      aarrCodigoDetaArtistoco.push({adescripcionDetalleArtistoco: 'D', aintegridadDetaArtistoco: 'I'});
      arrDibujoDArt.push({scd_codigo:htmlDAact, id: contDAact});
      contDAact++;
      cont=1;
      for (var i=0; i<arrDibujoDArt.length;i++)
      {
        cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDArt[i].scd_codigo;
        cont++;
      }  
      $('#acrearGrillaDetaArtistocoado').html(cadenaDetaArtistoco); 
      var contda=0;
      arrDibujoDArt.forEach(function (item, index, array) 
      { 
        $("#descripDA"+item.id+"").val(aarrCodigoDetaArtistoco[contda].adescripcionDetalleArtistoco); 
        $("#integridadDA"+item.id+"").val(aarrCodigoDetaArtistoco[contda].aintegridadDetaArtistoco); 

        contda=contda+1;
      });

    }

    function eliminarDetaArtistoco($id){
      if(contDAact>2)
      {
        swal({title: "Esta seguro de eliminar?",
          text: "Se eliminará el argumento seleccionado!",
          type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Si, Eliminar!",closeOnConfirm: true
        }, function(){
          var cadenaDetaArtistoco ='';
          var pos=-1;
          var cont = 0;
          arrDibujoDArt.forEach(function (item, index, array) 
          {
            aarrCodigoDetaArtistoco[cont].adescripcionDetalleArtistoco = $("#descripDA"+item.id+"").val();     
            aarrCodigoDetaArtistoco[cont].aintegridadDetaArtistoco = $("#integridadDA"+item.id+"").val();
            cont++;
          });
          pos = arrDibujoDArt.map(function(e) {return e.id;}).indexOf($id);
          aarrCodigoDetaArtistoco.splice(pos,1);
          arrDibujoDArt.splice(pos,1);
          cont=1;
          for (var i=0; i<arrDibujoDArt.length;i++)
          {
            cadenaDetaArtistoco = cadenaDetaArtistoco + arrDibujoDArt[i].scd_codigo;
            cont++;
          }
          $('#acrearGrillaDetaArtistocoado').html(cadenaDetaArtistoco);
          var conto=0;
          arrDibujoDArt.forEach(function (item, index, array) 
          { 
              $("#descripDA"+item.id+"").val(aarrCodigoDetaArtistoco[conto].adescripcionDetalleArtistoco); 
            $("#integridadDA"+item.id+"").val(aarrCodigoDetaArtistoco[conto].aintegridadDetaArtistoco);
            conto=conto+1;
          });
        });
      }
    }

    //--------------------GRILLA Detalles Artisitcos-------------------//


/////FIN GRILLA ACABADOS DE MURO////


</script>

<script  src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script >

  $('textarea').ckeditor();


</script>
@endpush