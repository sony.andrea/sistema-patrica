<div class="modal fade modal-default" id="myUpdateBienesArqM" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     {!! Form::open(['class'=>'form-vertical','id'=>'form_resitem_update'])!!}
     <div class="modal-header">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="row">
           <div class="col-md-12">
            <section class="content-header">
             <div class="header_title">
              <h3>
                Editar datos de Bienes Arqueologicos (Muebles)
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </h3>
            </div>
          </section>
        </div>   
      </div>
    </div>
    <div  style="padding:20px">
      <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">   
      <input type="hidden" name="acasoId_m" id="acasoId_m">
      <input type="hidden" name="anroFicha_m" id="anroFicha_m">
      <input type="hidden" name="acreadorM_arq" id="acreadorM_arq">

      <div class="col-md-12">
        <div class="form-group col-md-6">
          <label class="control-label">
            Proveniencia del bien Mueble:
          </label>
          <select class="form-control" id="aprovenienciaMueble_m" >
            <option value=""> Selecionar</option> 
            <option value="Contexto Arqueologico"> Contexto Arqueologico </option>
            <option value="Colección"> Colección</option>
            <option value=" Otro"> Otro</option>   
          </select> 
        </div>
        <div class="form-group col-md-6">
          <label class="control-label">
            Relación de la Pieza con el Yacimiento:
          </label>
          <input id="arelacionPieza_m" type="text" class="form-control"  placeholder="Relación de la Pieza">
        </div>

      </div> 
      <div class="col-md-12">
       <div class="form-group col-md-6">
        <label class="control-label">
          Ubicacion Actual:
        </label>
        <input id="aubicacionActual_m" type="text" class="form-control"  placeholder="Ubicación Actual"> 
      </div>
    </div>
     <div class="col-md-12">
      <div class="form-group col-md-12">
        <label class="control-label">
          Descripción del bien Mueble:
        </label>
        <textarea name="textarea" rows="4" cols="50" class="form-control " id="aDescripcionMueble_m"></textarea>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group col-md-12">
        <label class="control-label">
          Material:
        </label>
        <textarea name="textarea" rows="4" cols="50" class="form-control " id="amaterial_m"></textarea>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group col-md-12">
        <label class="control-label">
          Técnica:
        </label>
        <textarea name="textarea" rows="4" cols="50" class="form-control " id="atecnica_m"></textarea>
      </div>
    </div>
    <div class="col-md-12">
     <div class="form-group col-md-12">
      <label class="control-label">
       Manufactura:
     </label>
     <textarea name="textarea" rows="4" cols="50" class="form-control " id="amanufactura_m"></textarea>
   </div>
</div>
  <div class="col-md-12">
   <div class="form-group col-md-12">
    <label class="control-label">
      Decoración::
    </label>
    <textarea name="textarea" rows="4" cols="50" class="form-control " id="adecoracion_m"></textarea>
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Atributos:
 </label>
 <textarea name="textarea" rows="4" cols="50" class="form-control " id="aatributos_m"></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-6">
  <label class="control-label">
    Estado de Conservación:
  </label>
  <select class="form-control" id="aestadoConserva_m" >
    <option value=""> Selecionar...</option> 
    <option value="Bueno"> Bueno</option>
    <option value="Malo"> Malo</option>
    <option value="Regular"> Regular</option>  
  </select> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   Descripción del Estado:
 </label>
 <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripEstado_m"></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
    Observaciones:
  </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="aobservacion_m"></textarea>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   Cronología:
 </label>
 <input id="acronologia_m" type="text" class="form-control"  placeholder="Cronología"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
    Función:
  </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="afuncion_m"></textarea>
  
</div>
</div>
<div class="modal-footer">
 <button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
 <button class="btn btn-primary" id="actualizarMuebleArqueologico" data-dismiss="modal" type="button">
  <i class="glyphicon glyphicon-pencil"></i> Modificar</button>
</div> 
{!! Form::close() !!} 
</div>
</div>
</div>
</div>
</div>
</div>