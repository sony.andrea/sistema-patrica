<div class="modal fade modal-default" id="myUpdateBienesArqS" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     {!! Form::open(['class'=>'form-vertical','id'=>'form_resitem_update'])!!}
     <div class="modal-header">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="row">
           <div class="col-md-12">
            <section class="content-header">
             <div class="header_title">
              <h3>

                Editar datos de Bienes Arqueologicos (Sitios)

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </h3>
            </div>
          </section>
        </div>   
      </div>
    </div>
    <div  style="padding:20px">
      <input type="hidden" name="csrf-token" value=" {{ csrf_token(
      ) }}" id="token">   
      <input type="hidden" name="acasoId_s" id="acasoId_s">
      <input type="hidden" name="anroFicha" id="anroFicha">
      <input type="hidden" name="acreador_arq" id="acreador_arq">


     <div class="col-md-12">
        <div class="panel-heading">
          <label class="control-label" style="color:#275c26";>
          INFORMACION GENERAL:
          </label>
        </div>
    </div>
    
    <div class="col-md-12">
     <div class="form-group col-md-6">
      <label class="control-label">
        Denominación:
      </label>
      <input id="adenominacion" type="text" class="form-control"  placeholder="Denominacion"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
       PUNTO DE UBICACIÓN EN EL MUNICIPIO:
     </label>
     <input id="aubiMunicipio" type="text" class="form-control"  placeholder="Ubicacion Municipio"> 
   </div>
 </div>
 <div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
      UBICACIÓN MACRO-DISTRITO:
    </label>
    <input id="aubiMacrodistrito"  type="text" class="form-control"  placeholder="Ubicacion Macrodistrito">
  </div>
  <div class="form-group col-md-6">
    <label class="control-label">
      UBICACIÓN LOCAL:
    </label>
    <input id="aubiLocal"  type="text" class="form-control"  placeholder="Ubicacion Local">
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN ELEMENTOS INMUEBLES:
 </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripInmuebles"></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   UBICACIÓN ESPECÍFICA DEL POLÍGONO::
 </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="aubicacionPoligono"></textarea>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
      <label class="control-label">
        Estado de Conservación:
      </label>
      <select class="form-control" id="aestadoConserva" >
          <option value=""> Selecionar</option> 
          <option value="Bueno"> Bueno</option>
          <option value="Malo"> Malo</option>
          <option value="Regular"> Regular</option>  
        </select> 
    </div>
</div>
 <div class="col-md-12">
        <div class="panel-heading">
          <label class="control-label" style="color:#275c26";>
          RELACIÓN DEL SITIO O YACIMIENTO CON OTRAS AREAS:
          </label>
        </div>
  </div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN DE ESTADO::
 </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripEstado"></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
    Observaciones:
  </label>
   <textarea name="textarea" rows="4" cols="50" class="form-control " id="aobservacion"></textarea>
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   TAFONOMIA:
 </label>
 <input id="atafonomia" type="text" class="form-control"  placeholder="Tafonomia"> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
    DESCRIPCIÓN DEL CONTEXTO INMEDIATO EN AREA RURAL:
  </label>
   <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripContextoUrbana"></textarea> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN DEL CONTEXTO INMEDIATO EN AREA URBANA:
 </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripContextoRural"></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN TAFONOMIA::
  </label>
   <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripTafonomia"></textarea>
</div>
</div>
<div class="col-md-12">
        <div class="panel-heading">
          <label class="control-label" style="color:#275c26";>
          RELACIÓN DEL SITIO O YACIMIENTO CON OTRAS AREAS:
          </label>
        </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
   DESCRIPCIÓN GENERAL DEL YACIMIENTO:
 </label>
 <input id="adescripYacimiento" type="text" class="form-control"  placeholder="Cronología"> 
</div>
<div class="form-group col-md-6">
  <label class="control-label">
    PRESENCIA DE ELEMENTOS MUEBLES:
  </label>
  <input id="aelementos" type="text" class="form-control"  placeholder="Descripción"> 
</div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN:
 </label>
  <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescrip"></textarea>
</div>
</div> 
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
    DESCRIPCIÓN PRESENCIA DE ELEMENTOS MUEBLES::
  </label>
   <textarea name="textarea" rows="4" cols="50" class="form-control " id="adescripcionMueble"></textarea> 
</div>
</div>

<!--////////////Formulario 2/////////////////////////-->
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       MATERIAL ASOCIADO AL YACIMIENTO ARQUEOLOGICO:
      </label><br>
      <label class="control-label" style="color:#275c26";>
       EVALUACIÓN SUPERFICIAL:
      </label>
    </div>
</div>  
</script>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  DESCRIPCIÓN DE LOS ELEMENTOS INMUEBLES:
  </label>
   <select id="adescripcionElem_ar" class="form-control">
    <option value="-1">Seleccionar...</option>
    <option value="Almacenes">Almacenes</option>
    <option value="Arquictetura Ceremonial">Arquictetura Ceremonial</option>
    <option value="Arte Repestre">Arte Repestre</option>
    <option value="Caminos">Caminos</option>
    <option value="Entierros">Entierros</option>
    <option value="Habitaciones">Habitaciones</option>
    <option value="Socavones">Socavones</option>
    <option value="Talleres">Talleres</option>
    <option value="Terraceano">Terraceano</option>
    <option value="Torres Funerarias">Torres Funerarias</option>
    </select> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN CRONOLÓGIA:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="acronologiaDesc_ar"></textarea>
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  CRONOLOGÍA:
  </label><br>
       <input type="checkbox" id="acronologia_ar1"> Arcaico tardío<br>
       <input type="checkbox" id="acronologia_ar2"> Arcaico temprano/medio<br>
       <input type="checkbox" id="acronologia_ar3"> Colonial<br>
       <input type="checkbox" id="acronologia_ar4"> Formativo tardío<br>
       <input type="checkbox" id="acronologia_ar5"> Formativo temprano/medio<br>
       <input type="checkbox" id="acronologia_ar6"> Inca<br>
       <input type="checkbox" id="acronologia_ar7"> Intermedio tardío<br>
       <input type="checkbox" id="acronologia_ar8"> Republicano<br>
       <input type="checkbox" id="acronologia_ar9"> Tiwanaku<br>
      
       
  </div>
<div class="form-group col-md-6">
  <label class="control-label">
  FUNCIONALIDAD DEL SITIO:
  </label><br>
       <input type="checkbox" id="afuncionalidad_ar1"> Administrativa<br>
       <input type="checkbox" id="afuncionalidad_ar2"> Caminera<br>
       <input type="checkbox" id="afuncionalidad_ar3"> Ceremonial<br>
       <input type="checkbox" id="afuncionalidad_ar4"> Defensiva<br>
       <input type="checkbox" id="afuncionalidad_ar5"> Doméstica<br>
       <input type="checkbox" id="afuncionalidad_ar6"> Funeraria<br>
       <input type="checkbox" id="afuncionalidad_ar7"> Manufactura<br>
       <input type="checkbox" id="afuncionalidad_ar8"> Productiva<br>
   </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   TAMAÑO DEL SITIO O YACIMIENTO:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="atamañoSitio_ar"></textarea>
  </div>
  </div>
  <div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       EVALUACIÓN DE SUBSUELO:
      </label>
  </div>
</div> 
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  TIPO DE INTERVENCIÓN:
  </label><br>
       <input type="checkbox" id="aintervencion_ar1"> Apertura de Área<br>
       <input type="checkbox" id="aintervencion_ar2"> Calas Auger<br>
       <input type="checkbox" id="aintervencion_ar3"> Examen de perfiles<br>
       <input type="checkbox" id="aintervencion_ar4"> Hallazgo Accidental<br>
       <input type="checkbox" id="aintervencion_ar5"> Sondeos<br>
  </div>
<div class="form-group col-md-6">
            <label class="control-label">
                NUMERO DE POZOS   
            </label>
            <input id="anumeroPozo_ar" type="text" class="form-control"  placeholder="Numero Pozos"> 
        </div>
</div> 

<div class="col-md-12">
      <div class="form-group col-md-6">
          <label class="control-label">
                ÁREA DE INTERVENCIÓN(m2):
          </label>
          <input id="aareaInter_ar" type="text" class="form-control"  placeholder="Area de Intervencion"></div>
      <div class="form-group col-md-6">
          <label class="control-label">
               PROFUNDIDAD MÍNIMA(cm):
          </label>
          <input id="aprofundidadMin_ar" type="text" class="form-control"  placeholder="Profundidad Minima(cm)"> 
      </div>
</div>
<div class="col-md-12">
      <div class="form-group col-md-6">
          <label class="control-label">
              PROFUNDIDAD MÁXIMA(cm): 
          </label>
          <input id="aprofundidadMax_ar" type="text" class="form-control"  placeholder="Profundidad Maxima(cm)"> 
      </div>
  </div>
<div class="col-md-12">
  <div class="form-group col-md-6">
      <label class="control-label">
      ELEMENTOS INMUEBLES
      </label><br>
         <input type="checkbox" id="ainmuebles_ar1"> Almacenes<br>
         <input type="checkbox" id="ainmuebles_ar2"> Arquitectura Ceremonial<br>
         <input type="checkbox" id="ainmuebles_ar3"> Arte Repestre<br>
         <input type="checkbox" id="ainmuebles_ar4"> Caminos<br>
         <input type="checkbox" id="ainmuebles_ar5"> Entierros<br>
         <input type="checkbox" id="ainmuebles_ar6"> Habitaciones<br>
         <input type="checkbox" id="ainmuebles_ar7"> Obras hidráulicas<br>
         <input type="checkbox" id="ainmuebles_ar8"> Socavones<br>
         <input type="checkbox" id="ainmuebles_ar9"> Talleres<br>
         <input type="checkbox" id="ainmuebles_ar10"> Terraceado<br>
         <input type="checkbox" id="ainmuebles_ar11"> Torres Funerarias<br>
    </div>
    <div class="form-group col-md-6">
    <label class="control-label">
    ELEMENTOS MUEBLES:
    </label><br>
     <input type="checkbox" id="amuebles_ar1"> Cerámica<br>
     <input type="checkbox" id="amuebles_ar2"> Líticos<br>
     <input type="checkbox" id="amuebles_ar3"> Metales<br>
     <input type="checkbox" id="amuebles_ar4"> Óseos Animales<br>
     <input type="checkbox" id="amuebles_ar5"> Óseos Humanos<br>
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN ELEMENTOS INMUEBLES:

  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="adescripcionInmu_ar"></textarea>
  </div>
</div> 
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN ELEMENTOS MUEBLES:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="adescripcionMue_ar"></textarea>
  </div>
  </div>
<div class="col-md-12">
  <div class="form-group col-md-12">
  <label class="control-label">
   OBSERVACIONES:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="aobservaciones_ar"></textarea>
  </div>
</div>
<!--////////FIN FORMULARIO 2//////-->   
<div class="modal-footer">
 <button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
 <button class="btn btn-primary" id="actualizarSitioArqueologico" data-dismiss="modal" type="button">
  <i class="glyphicon glyphicon-pencil"></i> Modificar
</button>
</div> 
{!! Form::close() !!} 
</div>
</div>
</div>
</div>
</div>
</div>
