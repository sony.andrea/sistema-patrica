<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myCreateBienesArq" tabindex="-5">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            {!! Form::open(['class'=>'form-vertical forArq-validate','id'=>'formArq_create'])!!}
            <!--Inicia content-->
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 container-fluit">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3>
                                    Nueva Ficha Bienes Arqueologicos
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </h3>
                            </div>
                            <div class="panel-body">
                              <div class="caption">
                                <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
                                <input type="hidden" name="ccodPatrimonio" id="ccodPatrimonio">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">Tipo de Patrimonio:</label>
                                        <!--<label class="control-label col-md-4">Tipo de contribuyente:</label>-->
                                        <select  id="tipo-contribuyente" class="form-control" onclick="tipoContribuyente(this)">
                                            <option selected value=" ">Seleccione...</option>
                                            <option value="1">Bienes Muebles Arqueológicos</option>
                                            <option value="2">Sitio o yacimiento Arqueológico</option>
                                        </select>
                                    </div>   
                                </div>
                                    <!--inicio div mueble-->
                                  <div id="panelMuebleArq" style="display: none;">
                                  <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                          <label class="control-label">
                                             Creador:
                                          </label>
                                          <input id="ccreadorM_arq" type="text" class="form-control" disabled>
                                      </div>
                                  </div>       
                                  <div class="col-md-12">
                                      <div class="panel-heading">
                                          <label class="control-label" style="color:#275c26";>
                                              CONTEXTO ORIGINAL:
                                          </label>
                                      </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-6">
                                      <label class="control-label">
                                        Proveniencia del bien Mueble::
                                      </label>
                                        <select class="form-control" id="cprovenienciaMueble_m" >
                                            <option value=""> Selecionar</option> 
                                            <option value="Contexto Arqueologico"> Contexto Arqueologico </option>
                                            <option value="Colección"> Colección</option>
                                            <option value=" Otro"> Otro</option>  
                                        </select> 
                                    </div>
                                  <div class="form-group col-md-6">
                                    <label class="control-label">
                                        Relación de la Pieza con el Yacimiento:
                                    </label>
                                      <input id="crelacionPieza_m" type="text" class="form-control"  placeholder="Relación de la Pieza">
                                  </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-6">
                                      <label class="control-label">
                                         Ubicacion Actual:
                                      </label>
                                         <input id="cubicacionActual_m" type="text" class="form-control"  placeholder="Ubicación Actual"> 
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                        Descripción del bien Mueble:
                                      </label>
                                        <textarea name="textarea" rows="4" cols="50" class="form-control " id="cDescripcionMueble_m"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                        Material:
                                      </label>
                                      <textarea name="textarea" rows="4" cols="50" class="form-control " id="cmaterial_m"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                        Técnica:
                                      </label>
                                       <textarea name="textarea" rows="4" cols="50" class="form-control " id="ctecnica_m"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                         Manufactura:
                                      </label>
                                       <textarea name="textarea" rows="4" cols="50" class="form-control " id="cmanufactura_m"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                        Decoración::
                                      </label>
                                       <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdecoracion_m"></textarea> 
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                       Atributos:
                                      </label>
                                     <textarea name="textarea" rows="4" cols="50" class="form-control " id="catributos_m"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                     <div class="form-group col-md-6">
                                      <label class="control-label">
                                        Estado de Conservación:
                                      </label>
                                        <select class="form-control" id="cestadoConserva_m" >
                                          <option value=""> Selecionar</option> 
                                          <option value="Bueno"> Bueno</option>
                                          <option value="Malo"> Malo</option>
                                          <option value="Regular"> Regular</option>  
                                        </select> 
                                     </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                        Descripción del Estado:
                                      </label>
                                       <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripEstado_m"></textarea> 
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                      <label class="control-label">
                                        Observaciones:
                                      </label>
                                        <textarea name="textarea" rows="4" cols="50" class="form-control " id="cobservacion_m"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-6">
                                      <label class="control-label">
                                       Cronología:
                                      </label>
                                      <input id="ccronologia_m" type="text" class="form-control"  placeholder="Cronología"> 
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                     <label class="control-label">
                                      Función:
                                     </label>
                                      <textarea name="textarea" rows="4" cols="50" class="form-control " id="cfuncion_m"></textarea> 
                                    </div>
                                  </div>
                               <div class="modal-footer">
                              <button class="btn btn-primary" data-dismiss="modal" type="button">
                               <i class="fa fa-close"></i> Cancelar</button>
                              <button class="btn btn-default "  type="button" onClick="limpiarM()">
                               <i class="fa fa-eraser"></i> Limpiar
                              </button>
                              <a type = "button" class = "btn btn-primary" id="registrarMuebleArqueologico" data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
                              </div>
                              </div>
<!--fin div mueble-->
<!--inicio div yacimiento-->
<div id="panelYacimientoArq" style="display: none;">
    <div class="col-md-12">
      <div class="col-md-6 form-group">
            <label class="control-label">
               CREADOR:
            </label>
            <input id="ccreadorS_arq" type="text" class="form-control" disabled>
      </div>
    </div>
  
  <div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
        INFORMACION GENERAL:
      </label>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-6">
      <label class="control-label">
        DENOMINACION:
      </label>
        <input id="cdenominacion" type="text" class="form-control"  placeholder="Denominacion"> 
    </div>
    <div class="form-group col-md-6">
      <label class="control-label">
        PUNTO DE UBICACIÓN EN EL MUNICIPIO:
      </label>
        <input id="cubiMunicipio" type="text" class="form-control"  placeholder="Ubicacion Municipio"> 
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-6">
      <label class="control-label">
        UBICACIÓN MACRO-DISTRITO:
      </label>
      <input id="cubiMacrodistrito"  type="text" class="form-control"  placeholder="Ubicacion Macrodistrito">
    </div>
  <div class="form-group col-md-6">
    <label class="control-label">
      UBICACIÓN LOCAL:
    </label>
    <input id="cubiLocal"  type="text" class="form-control"  placeholder="Ubicacion Local">
  </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        DESCRIPCIÓN ELEMENTOS INMUEBLES:
      </label>
      <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripInmuebles"></textarea>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        UBICACIÓN ESPECÍFICA DEL POLÍGONO:
      </label>
    <textarea name="textarea" rows="4" cols="50" class="form-control " id="cubicacionPoligono"></textarea>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-6">
      <label class="control-label">
        ESTADO DE CONSERVACION:
      </label>
      <select class="form-control" id="cestadoConserva" >
        <option value=""> Selecionar</option> 
        <option value="Bueno"> Bueno</option>
        <option value="Malo"> Malo</option>
        <option value="Regular"> Regular</option>  
      </select> 
    </div>
  </div>
  <div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
        RELACIÓN DEL SITIO O YACIMIENTO CON OTRAS AREAS:
      </label>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        DESCRIPCIÓN DE ESTADO:
      </label>
     <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripEstado_s"></textarea>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        Observaciones:
      </label>
      <textarea name="textarea" rows="4" cols="50" class="form-control " id="cobservacion"></textarea> 
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-6">
      <label class="control-label">
       TAFONOMIA:
      </label>
      <input id="ctafonomia" type="text" class="form-control"  placeholder="Tafonomia"> 
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        DESCRIPCIÓN DEL CONTEXTO INMEDIATO EN AREA RURAL:
      </label>
      <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripContextoUrbana"></textarea>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
        DESCRIPCIÓN DEL CONTEXTO INMEDIATO EN AREA URBANA:
      </label>
      <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripContextoRural"></textarea>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-md-12">
      <label class="control-label">
       DESCRIPCIÓN TAFONOMIA::
     </label>
     <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripTafonomia"></textarea>
    </div>
  </div>

<div class="col-md-12">
  <div class="panel-heading">
    <label class="control-label" style="color:#275c26";>
      RELACIÓN DEL SITIO O YACIMIENTO CON OTRAS AREAS:
    </label>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-6">
    <label class="control-label">
     DESCRIPCIÓN GENERAL DEL YACIMIENTO:
    </label>
    <input id="cdescripYacimiento" type="text" class="form-control"  placeholder="Cronología"> 
  </div>
<div class="form-group col-md-6">
  <label class="control-label">
    PRESENCIA DE ELEMENTOS MUEBLES:
  </label>
  <input id="celementos" type="text" class="form-control"  placeholder="Descripción"> 
</div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
     DESCRIPCIÓN:
    </label>
    <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescrip"></textarea>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group col-md-12">
    <label class="control-label">
      DESCRIPCIÓN PRESENCIA DE ELEMENTOS MUEBLES:
    </label>
    <textarea name="textarea" rows="4" cols="50" class="form-control " id="cdescripcionMueble"></textarea>
  </div>
</div>

<!--////////////Formulario 2/////////////////////////-->
<div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       MATERIAL ASOCIADO AL YACIMIENTO ARQUEOLOGICO:
      </label><br>
      <label class="control-label" style="color:#275c26";>
       EVALUACIÓN SUPERFICIAL:
      </label>
    </div>
</div>  
</script>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  DESCRIPCIÓN DE LOS ELEMENTOS INMUEBLES:
  </label>
   <select id="cdescripcionElem_ar" class="form-control">
    <option value="-1">Seleccionar...</option>
    <option value="Almacenes">Almacenes</option>
    <option value="Arquictetura Ceremonial">Arquictetura Ceremonial</option>
    <option value="Arte Repestre">Arte Repestre</option>
    <option value="Caminos">Caminos</option>
    <option value="Entierros">Entierros</option>
    <option value="Habitaciones">Habitaciones</option>
    <option value="Socavones">Socavones</option>
    <option value="Talleres">Talleres</option>
    <option value="Terraceano">Terraceano</option>
    <option value="Torres Funerarias">Torres Funerarias</option>
    </select> 
</div>
</div>
<div class="col-md-12">
<div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN CRONOLÓGIA:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="ccronologiaDesc_ar"></textarea>
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  CRONOLOGÍA:
  </label><br>
       <input type="checkbox" id="ccronologia_ar1"> Arcaico tardío<br>
       <input type="checkbox" id="ccronologia_ar2"> Arcaico temprano/medio<br>
       <input type="checkbox" id="ccronologia_ar3"> Colonial<br>
       <input type="checkbox" id="ccronologia_ar4"> Formativo tardío<br>
       <input type="checkbox" id="ccronologia_ar5"> Formativo temprano/medio<br>
       <input type="checkbox" id="ccronologia_ar6"> Inca<br>
       <input type="checkbox" id="ccronologia_ar7"> Intermedio tardío<br>
       <input type="checkbox" id="ccronologia_ar8"> Republicano<br>
       <input type="checkbox" id="ccronologia_ar9"> Tiwanaku<br>
      
       
  </div>
<div class="form-group col-md-6">
  <label class="control-label">
  FUNCIONALIDAD DEL SITIO:
  </label><br>
       <input type="checkbox" id="cfuncionalidad_ar1"> Administrativa<br>
       <input type="checkbox" id="cfuncionalidad_ar2"> Caminera<br>
       <input type="checkbox" id="cfuncionalidad_ar3"> Ceremonial<br>
       <input type="checkbox" id="cfuncionalidad_ar4"> Defensiva<br>
       <input type="checkbox" id="cfuncionalidad_ar5"> Doméstica<br>
       <input type="checkbox" id="cfuncionalidad_ar6"> Funeraria<br>
       <input type="checkbox" id="cfuncionalidad_ar7"> Manufactura<br>
       <input type="checkbox" id="cfuncionalidad_ar8"> Productiva<br>
   </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   TAMAÑO DEL SITIO O YACIMIENTO:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="ctamañoSitio_ar"></textarea>
  </div>
  </div>
  <div class="col-md-12">
    <div class="panel-heading">
      <label class="control-label" style="color:#275c26";>
       EVALUACIÓN DE SUBSUELO:
      </label>
  </div>
</div> 
<div class="col-md-12">
 <div class="form-group col-md-6">
  <label class="control-label">
  TIPO DE INTERVENCIÓN:
  </label><br>
       <input type="checkbox" id="cintervencion_ar1"> Apertura de Área<br>
       <input type="checkbox" id="cintervencion_ar2"> Calas Auger<br>
       <input type="checkbox" id="cintervencion_ar3"> Examen de perfiles<br>
       <input type="checkbox" id="cintervencion_ar4"> Hallazgo Accidental<br>
       <input type="checkbox" id="cintervencion_ar5"> Sondeos<br>
  </div>
<div class="form-group col-md-6">
            <label class="control-label">
                NUMERO DE POZOS   
            </label>
            <input id="cnumeroPozo_ar" type="text" class="form-control"  placeholder="Numero Pozos"> 
        </div>
</div> 

<div class="col-md-12">
      <div class="form-group col-md-6">
          <label class="control-label">
                ÁREA DE INTERVENCIÓN(m2):
          </label>
          <input id="careaInter_ar" type="text" class="form-control"  placeholder="Area de Intervencion"></div>
      <div class="form-group col-md-6">
          <label class="control-label">
               PROFUNDIDAD MÍNIMA(cm):
          </label>
          <input id="cprofundidad_ar" type="text" class="form-control"  placeholder="Profundidad Minima(cm)"> 
      </div>
</div>
<div class="col-md-12">
      <div class="form-group col-md-6">
          <label class="control-label">
              PROFUNDIDAD MÁXIMA(cm): 
          </label>
          <input id="cprofundidadMax_ar" type="text" class="form-control"  placeholder="Profundidad Maxima(cm)"> 
      </div>
  </div>
<div class="col-md-12">
  <div class="form-group col-md-6">
      <label class="control-label">
      ELEMENTOS INMUEBLES
      </label><br>
         <input type="checkbox" id="cinmuebles_ar1"> Almacenes<br>
         <input type="checkbox" id="cinmuebles_ar2"> Arquitectura Ceremonial<br>
         <input type="checkbox" id="cinmuebles_ar3"> Arte Repestre<br>
         <input type="checkbox" id="cinmuebles_ar4"> Caminos<br>
         <input type="checkbox" id="cinmuebles_ar5"> Entierros<br>
         <input type="checkbox" id="cinmuebles_ar6"> Habitaciones<br>
         <input type="checkbox" id="cinmuebles_ar7"> Obras hidráulicas<br>
         <input type="checkbox" id="cinmuebles_ar8"> Socavones<br>
         <input type="checkbox" id="cinmuebles_ar9"> Talleres<br>
         <input type="checkbox" id="cinmuebles_ar10"> Terraceado<br>
         <input type="checkbox" id="cinmuebles_ar11"> Torres Funerarias<br>
    </div>
    <div class="form-group col-md-6">
    <label class="control-label">
    ELEMENTOS MUEBLES:
    </label><br>
     <input type="checkbox" id="cmuebles_ar1"> Cerámica<br>
     <input type="checkbox" id="cmuebles_ar2"> Líticos<br>
     <input type="checkbox" id="cmuebles_ar3"> Metales<br>
     <input type="checkbox" id="cmuebles_ar4"> Óseos Animales<br>
     <input type="checkbox" id="cmuebles_ar5"> Óseos Humanos<br>
  </div>
</div>
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN ELEMENTOS INMUEBLES:

  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="cdescripcionInmu_ar"></textarea>
  </div>
</div> 
<div class="col-md-12">
 <div class="form-group col-md-12">
  <label class="control-label">
   DESCRIPCIÓN ELEMENTOS MUEBLES:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="cdescripcionMue_ar"></textarea>
  </div>
  </div>
<div class="col-md-12">
  <div class="form-group col-md-12">
  <label class="control-label">
   OBSERVACIONES:
  </label>
<textarea name="textarea" rows="4" cols="100" class="form-control" id="cobservaciones_ar"></textarea>
  </div>
</div>
<!--////////FIN FORMULARIO 2//////-->
<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i> Cancelar</button>
  <button class="btn btn-default "  type="button" onClick="limpiarS()">
    <i class="fa fa-eraser"></i> Limpiar
  </button>
  <a a href="'+url+'" type = "button" class = "btn btn-primary"  id="registrarSitioArqueologico" data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
</div>
</div>
<!--fin div yacimiento-->

</div>
</div>
</div>
</div>
</div>
</div>

<!--end content--> 
{!! Form::close() !!} 
</div>
</div>
</div>

<script type="text/javascript">
    function tipoContribuyente(v){
        console.log("vvvv",v);
        if (v.value==1) {
            document.getElementById("panelMuebleArq").style="display:block;";
            document.getElementById("panelYacimientoArq").style="display:none;";
        }
        if (v.value==2) {
            document.getElementById("panelMuebleArq").style="display:none;";
            document.getElementById("panelYacimientoArq").style="display:block;";
        } 

    }
</script>
