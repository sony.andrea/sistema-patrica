@extends('backend.template.app')
@section('main-content')

@include('backend.patrimonioCultural.bienesArqueologicos.partial.modalCreate')
@include('backend.patrimonioCultural.bienesArqueologicos.partial.modalUpdateM')
@include('backend.patrimonioCultural.bienesArqueologicos.partial.modalUpdateS')

<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>BUSQUEDA DE FICHAS</b></div>
    <div class="panel-body">     
      <br>
      <div class="col-md-12">
       <div class="form-group col-md-4">
        <label class="control-label">
          Tipo Patrimonio Cultural:
        </label>
       <input id="tipoPatriCultural" type="text" class="form-control" value="BIENES ARQUEOLOGICOS" disabled>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Campo:
        </label>
        <select class="form-control" name="camposPorPatrimonio" id="camposPorPatrimonio">
          <option value="-1">Seleccionar...</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label class="control-label">
          Valor:
        </label>
        <input type="text" id="valorPatrimonio" class="form-control" name="valorPatrimonio">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="container">
        <div class="col-md-6"></div>
        <div class="col-md-6"> 
          <button type ="button" class = "btn btn-primary" onclick="buscarPatrimonioTipoCampo(1)"> <i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-primary" onclick="limpiarPrincipal()"> <i class="fa fa fa-eraser"></i> Limpiar</button>
          <button type="button" class="btn btn-primary" data-target="#myCreateBienesArq" 
          data-toggle="modal"><i class="fa fa-plus"></i> Nueva Ficha</button>
       
     </div>
   </div>
 </div>
</div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-body">
        <div id="listBienArqueologico">
        </div>  
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@push('scripts')
<script>
 
  var _usrid={{$usuarioId}};
  var correlativog="";
  function buscarPatrimonioPorTipo(paging_pagenumbe)
  { 
    document.getElementById("listado_campo_valor").style = "display:none;";
    document.getElementById("listado_principal").style = "display:block;";
    var paging_pagesize = $( "#reg" ).val(); 
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1212',"parametros": '{"paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'}; 
    var band = 'list_principal';
    buscar(formData, band); 
  }
 function buscarPatrimonioTipoCampo(paging_pagenumbe){
  document.getElementById("listado_campo_valor").style = "display:block;";
    document.getElementById("listado_principal").style = "display:none;";
    var paging_pagesize = $( "#reg_campo" ).val(); 
    var campoPatrimonio = $( '#camposPorPatrimonio' ).val();
    var valorPatrimonio = $( '#valorPatrimonio' ).val();
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-1213',"parametros": '{"xcampo_id":' +campoPatrimonio+ ', "xcampo_valor":"' +valorPatrimonio+ '","xpaging_pagesize":'+paging_pagesize+',"xpaging_pagenumber":'+paging_pagenumbe+'}'};  
    var band = 'list_por_campo'; 
    buscar(formData, band);  

  }
    function limpiarPrincipal()
  {
    $( '#camposPorPatrimonio' ).val("-1");
    $( '#valorPatrimonio' ).val("");
  }

  /* function listarPatrimonioCultural(g_tipo){
    
    var tipo = 3;
    var formData  = {"identificador": 'SERVICIO_PATRIMONIO-933',"parametros": '{"xtipo_id":' +tipo+ '}'}; 
    buscar(formData);  
  }*/


function selectCampos()
{  
  var tipoCampoPatrimonio = 2;
  document.getElementById("camposPorPatrimonio").length=1;
  ////////console.log(tipoCampoPatrimonio,333333333333);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {         
      var formData = {"identificador": 'SERVICIO_PATRIMONIO-931',"parametros": '{"ytipoid":'+tipoCampoPatrimonio+'}'};
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },
       success: function(dataIN) {  
        ////////console.log("listadoCampos",dataIN);
        for(var i = 0; i < dataIN.length; i++){
         var campo_id = dataIN[i].xcampo_id;
         var campo_tipo = JSON.parse(dataIN[i].xcampo_data).campoTipo;   
         document.getElementById("camposPorPatrimonio").innerHTML += '<option value=' +campo_id+ '>' +campo_tipo+ '</option>';            
       }
     },     
     error: function (xhr, status, error) { }
   });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
}

function obtenercorrelativo(tipoPatrimonio)
{
 $.ajax({
  type        : 'GET',
  url         : urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": 'SERVICIO_PATRIMONIO-936',"parametros":'{"xtipoPat":"'+tipoPatrimonio+'"}'};
    ////////console.log("formdataaa",formData);
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
      var data=dataIN;
      correlativog=dataIN[0].sp_correlativo_patrimonio4;       
    ////////console.log("correl99999999999999999ativog",correlativog);
  },     
  error: function (xhr, status, error) { }
});
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

}

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SERVICIO_PATRIMONIO-1023","parametros": '{"yusrid":'+_usrid+'}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
       //////console.log(dataIN);
       var primerNombre = dataIN[0].vprs_nombres;
       var primerApellido = dataIN[0].vprs_paterno;
       var segundoApellido = dataIN[0].vprs_materno;
       var cedulaIdentidad = dataIN[0].vprs_ci;
       var razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
       
       $("#ccreadorM_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
       $("#ccreadorS_arq").val(primerNombre+' '+primerApellido+' '+segundoApellido);
     },     
     error: function (xhr, status, error) { }
   });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};

function darBaja(id, g_tipo)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SERVICIO_PATRIMONIO-934","parametros": '{"xcas_id":'+id+', "xcas_usr_id":'+_usrid+'}'};
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
           buscarPatrimonioPorTipo(1); 
           swal("Patrimonio!", "Fue eliminado correctamente!", "success");

         },
         error: function( result ) 
         {
          swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};


  function buscar(formData, band){

    htmlListaBus  = '';
    htmlListaBus  = '<h3><label id="ws-select"></label></h3>';
    htmlListaBus += '<div class="row">';
    htmlListaBus += '<div class="col-md-12" >' ;
    htmlListaBus += '<table id="lts-patrimonio" class="table table-striped" cellspacing="0" width="100%" >';
    htmlListaBus += '<thead><tr><th align="center">Nro.</th>' ;
    htmlListaBus += '<th align="center" class="form-natural">Opciones</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Nro de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Código de Ficha</th>';
    htmlListaBus += '<th align="left" class="form-natural">Denominación</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Fecha</th>' ;
    htmlListaBus += '<th align="left" class="form-natural">Adjuntos</th>' ;
    htmlListaBus += '<th width="12%" align="center" class="form-natural">Impresiones</th></tr>' ;
    htmlListaBus += '</thead>'; 
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {
          var datos = dataIN;
          console.log('busquedaaaaa',dataIN);
          var tam = datos.length;
        ////console.log('tam',tam);
        if(dataIN == "[{ }]"){
          tam = 0;
        }; 
        var tipoContribuyenteEnvio = $( "#tipo-contribuyente" ).val();

        for (var i = 0; i < tam; i++){ 
          var cas_id = JSON.parse(dataIN[i].data).cas_id;
          var numero_ficha = JSON.parse(dataIN[i].data).cas_nro_caso;
          ////console.log("numero_ficha",numero_ficha);
          var dataP= JSON.parse(dataIN[i].data);
          var g_tipo= 'PTR_BMA'; 
          var g_tipoArq=dataP.tipo_arq; 
          var nroFicha=dataP.cas_nro_caso;
          var denominacion = JSON.parse(dataIN[i].data).ptr_den;        
         var codigo_ficha = JSON.parse(dataIN[i].data).cas_nombre_caso;
         var codigo_catastral = JSON.parse(dataIN[i].data).ptr_cod_cat;
         var fecha_registro = JSON.parse(dataIN[i].data).cas_registrado;
         fecha_registro = fecha_registro.split('T');
         fecha_registro = fecha_registro[0];


          ////////console.log("nroFicha",nroFicha); 
          if ( numero_ficha == null) {
           numero_ficha = '';
         }
         if ( denominacion == null) {
           denominacion = '';                  
         }
         if ( codigo_ficha == null) {
           codigo_ficha = '';
         }
         if ( codigo_catastral == null) {
           codigo_catastral = '';
         }
         if ( denominacion == 'PTR_DEN_ACT') {
           denominacion = '';                  
         }
         htmlListaBus += '<tr>';
         htmlListaBus += '<td align="left">'+(i+1)+'</td>';
         htmlListaBus += '<td align="center">';
         htmlListaBus+='<button class="btncirculo btn-xs btn-danger" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' + cas_id +',\''+ g_tipo +'\');">';
         htmlListaBus+='<i class="glyphicon glyphicon-trash"></i></button>';

         if ( g_tipoArq == '1') {
         htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateBienesArqM" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarBienesArqueologicos('+cas_id+');"><i class="glyphicon glyphicon-pencil"></i></button>'; 
       }

       if ( g_tipoArq == '2') {

        htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-target="#myUpdateBienesArqS" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editarBienesArqueologicos('+cas_id+');"><i class="glyphicon glyphicon-pencil"></i></button>';
      }
         
      
      htmlListaBus += '</td>';
      htmlListaBus += '<td align="left" class="form-natural">'+numero_ficha+'</td>'; 
      htmlListaBus += '<td align="left" class="form-natural">'+codigo_ficha+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+denominacion+'</td>';                           
      htmlListaBus += '<td align="left" class="form-natural">'+fecha_registro+'</td>';
      var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
      url = url.replace('casosId1', cas_id);
      url = url.replace('numeroficha1', numero_ficha);

      htmlListaBus+='<td style="text-align: center;"><a href="'+url+'" class="btncirculo btn-xs btn-default" fa fa-plus-square pull-right"  > <i class="fa fa-folder"></i></a>';
      htmlListaBus+='</td>';
      htmlListaBus+='<td align="center">';
      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Ficha" type="button" onClick= "generarProforma(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-print"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Archivo Fotografico" type="button" onClick= "imprimirArchivoFotografico(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-camera"></i></button> &nbsp;';

      htmlListaBus+='<button class="btncirculo btn-xs btn-primary" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Imprimir Todo" type="button" onClick= "imprimirFichaTodo(' + cas_id +',\''+ g_tipo +'\' );">';
      htmlListaBus+='<i class="glyphicon glyphicon-list"></i></button>';

      htmlListaBus+='</td>';

      htmlListaBus += '</tr>';
    } 
    htmlListaBus += '</table></div></div>';
    htmlListaBus +='<div>';
    htmlListaBus +='<ul class="pager">';
    if(band =='list_principal'){
    htmlListaBus +='<li><a href="#" onClick="btnAnterior_prin()">Previous</a></li>';
    htmlListaBus +='<li><a href="#" onClick="btnSiguiente_prin()">Next</a></li>';
    }else{
    htmlListaBus +='<li><a href="#" onClick="btnAnterior_campo_valor()">Previous</a></li>';
    htmlListaBus +='<li><a href="#" onClick="btnSiguiente_campo_valor()">Next</a></li>';  
    }
    htmlListaBus+='</ul>';
    htmlListaBus +='</div>';

    $( '#listBienArqueologico' ).html(htmlListaBus);
   },
  error: function(result) {
    swal( "Error..!", "Error....", "error" );
  }
});
},
error: function(result) {
  swal( "Error..!", "No se puedo guardar los datos", "error" );
},
});
};

/*-----------------BIENES ARQUEOLOGICOS-----------------------*/

$( "#registrarMuebleArqueologico" ).click(function() 
{
  obtenercorrelativo('PTR_BMA');
  setTimeout(function(){

  var ccodPatrimonio_m = 'PTR_BMA'+correlativog+'/2018';
  var ccreadorM_arq = $("#ccreadorM_arq").val();
  ////////console.log("ccreador_arq",ccreador_arq);
  var cprovenienciaMueble_m = document.getElementById('cprovenienciaMueble_m').value;
  var crelacionPieza_m = $("#crelacionPieza_m").val();
  var cubicacionActual_m = $("#cubicacionActual_m").val();
  var cDescripcionMueble_m = $("#cDescripcionMueble_m").val();
  var cmaterial_m = $("#cmaterial_m").val();
  var ctecnica_m = $("#ctecnica_m").val();
  var cmanufactura_m = $("#cmanufactura_m").val();
  var cdecoracion_m = $("#cdecoracion_m").val();
  var catributos_m = $("#catributos_m").val();
  var cestadoConserva_m = document.getElementById('cestadoConserva_m').value;
  var cdescripEstado_m = $("#cdescripEstado_m").val();
  var cobservacion_m = $("#cobservacion_m").val();
  var ccronologia_m = $("#ccronologia_m").val();
  var cfuncion_m = $("#cfuncion_m").val();
  var g_tipo = 'PTR_BMA';

  //////console.log("cDescripcionMueble_m",cDescripcionMueble_m);

  var cDescripcionMueble_m = caracteresEspecialesRegistrar(cDescripcionMueble_m);
  var cmaterial_m = caracteresEspecialesRegistrar(cmaterial_m);
  var ctecnica_m = caracteresEspecialesRegistrar(ctecnica_m);
  var cmanufactura_m = caracteresEspecialesRegistrar(cmanufactura_m);
  var cdecoracion_m = caracteresEspecialesRegistrar(cdecoracion_m);
  var catributos_m = caracteresEspecialesRegistrar(catributos_m);
  var cdescripEstado_m = caracteresEspecialesRegistrar(cdescripEstado_m);
  var cobservacion_m = caracteresEspecialesRegistrar(cobservacion_m);
  var cfuncion_m = caracteresEspecialesRegistrar(cfuncion_m);
 //////console.log("cDescripcionMueble_m",cDescripcionMueble_m);

//console.log("ccreador_arq",ccreador_arq);
 var cas_data_m ='{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"1","PTR_CREA":"'+ccreadorM_arq+'","PTR_PROVIENE_C":"'+cprovenienciaMueble_m+'","PTR_RELACION":"'+crelacionPieza_m+'","PTR_UBIC_ACT":"'+cubicacionActual_m+'","PTR_DESC_BIEN":"'+cDescripcionMueble_m+'","PTR_MATERIAL":"'+cmaterial_m+'","PTR_TECN":"'+ctecnica_m+'","PTR_MANF":"'+cmanufactura_m+'","PTR_DECOR":"'+cdecoracion_m+'","PTR_ATRIB":"'+catributos_m+'","PTR_EST_CONS":"'+cestadoConserva_m+'","PTR_DESC_ESTADO":"'+cdescripEstado_m+'","PTR_OBS":"'+cobservacion_m+'","PTR_CRONO":"'+ccronologia_m+'","PTR_FUNC":"'+cfuncion_m+'"}';
 var jdata_m = JSON.stringify(cas_data_m);
  //////console.log("jdataaaa",jdata_m);

  var xcas_nro_caso = correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = '';
  var xcas_tipo_hr = '';
  var xcas_id_padre = 1;
  var xcampo_f = ''; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata_m+',"xcas_nombre_caso":"'+ccodPatrimonio_m+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
  //////console.log("formData",formData);

  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

    //var ccodPatrimonio = $("#ccodPatrimonio").val();
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },                  
     success: function(data){ 

      //////console.log("dataaaa",data);
      buscarPatrimonioPorTipo(1);
      swal( "Exíto..!", "Se registro correctamente los bienes arqueologicos...!", "success" );
      //$( "#formBienesArqM_create").data('bootstrapValidator').resetForm();
      //$( "#myCreateBienesArqMueble" ).modal('toggle');
      $( "#cprovenienciaMueble_m" ).val(""); 
      $( "#crelacionPieza_m" ).val(""); 
      $( "#cubicacionActual_m" ).val(""); 
      $( "#cDescripcionMueble_m" ).val(""); 
      $( "#cmaterial_m" ).val("");
      $( "#ctecnica_m" ).val("");
      $( "#cmanufactura_m" ).val("");
      $( "#cdecoracion_m" ).val("");
      $( "#catributos_m" ).val("");
      $( "#cestadoConserva_m").val("");
      $( "#cdescripEstado_m" ).val("");
      $( "#cobservacion_m" ).val("");
      $( "#ccronologia_m" ).val("");
      $( "#cfuncion_m" ).val("");

      var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
      setTimeout(function(){
        var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', xcas_nro_caso);
        //console.log("url",url);
        location.href = url;
      }, 1000);
    },
    error: function(result) { 
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
}); 

}, 500); 
});

$( "#registrarSitioArqueologico" ).click(function() 
{
  obtenercorrelativo('PTR_BMA');
  setTimeout(function(){
    var ccreador_arq = $("#ccreadorS_arq").val();   
    var cdenominacion = $( "#cdenominacion" ).val();
    var ccodPatrimonio = 'PTR_BMA'+correlativog+'/2018';
    var cubiMunicipio = $( "#cubiMunicipio" ).val();
    var cubiMacrodistrito = $("#cubiMacrodistrito").val();
    var cubiLocal = $("#cubiLocal").val();
    var cdescripInmuebles = $("#cdescripInmuebles").val();
    var cestadoConserva = document.getElementById('cestadoConserva').value;
  //////////console.log("estdoconserva",cestadoConserva);
  var cubicacionPoligono = $("#cubicacionPoligono").val();
  var cdescripEstad_s = $("#cdescripEstado_s").val();
  ////////console.log(cdescripEstad_s);
  var cobservacion = $("#cobservacion").val();
  var ctafonomia = $("#ctafonomia").val();
  var cdescripContextoUrbana = $("#cdescripContextoUrbana").val();
  var cdescripContextoRural = $("#cdescripContextoRural").val();
  var cdescripTafonomia = $("#cdescripTafonomia").val();
  var cdescripYacimiento = $("#cdescripYacimiento").val();
  var celementos = $("#celementos").val();
  var cdescrip = $("#cdescrip").val();
  var cdescripcionMueble = $("#cdescripcionMueble").val();
  var g_tipo = 'PTR_BMA';

  var cdescripInmuebles = caracteresEspecialesRegistrar(cdescripInmuebles);
  var cubicacionPoligono = caracteresEspecialesRegistrar(cubicacionPoligono);
  var cdescripEstad_s = caracteresEspecialesRegistrar(cdescripEstad_s);
  var cobservacion = caracteresEspecialesRegistrar(cobservacion);
  var cdescripContextoUrbana = caracteresEspecialesRegistrar(cdescripContextoUrbana);
  var cdescripContextoRural = caracteresEspecialesRegistrar(cdescripContextoRural);
  var cdescripTafonomia = caracteresEspecialesRegistrar(cdescripTafonomia);
  var cdescrip = caracteresEspecialesRegistrar(cdescrip);
  var cdescripcionMueble = caracteresEspecialesRegistrar(cdescripcionMueble);

  ////////////////////////formulario 2/////////////////////

  var PTR_IA_DESC_ELEM_INM = $("#cdescripcionElem_ar").val();
  var PTR_CRONO_DESC = $("#ccronologiaDesc_ar").val();
  var PTR_IA_TAM_YACIM = $("#ctamañoSitio_ar").val();
  var PTR_IA_NUM_POZ = $("#cnumeroPozo_ar").val();
  var PTR_IA_AR_INTER = $("#careaInter_ar").val();
  var PTR_IA_PROF_MIN = $("#cprofundidad_ar").val();
  var PTR_IA_PROF_MAX = $("#cprofundidadMax_ar").val();
  var PTR_IA_ELEM_INM_DESC = $("#cdescripcionMue_ar").val();
  var PTR_IA_ELEM_MUEB_DESC = $("#cdescripcionMue_ar").val();
  var PTR_IA_OBS = $("#cobservaciones_ar").val();

  var PTR_CRONO_DESC = caracteresEspecialesRegistrar(PTR_CRONO_DESC);
  var PTR_IA_TAM_YACIM = caracteresEspecialesRegistrar(PTR_IA_TAM_YACIM);
  var PTR_IA_ELEM_INM_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_INM_DESC);
  var PTR_IA_ELEM_MUEB_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_MUEB_DESC);
  var PTR_IA_OBS = caracteresEspecialesRegistrar(PTR_IA_OBS);

  var ccronologia_ar1 = document.getElementById("ccronologia_ar1").checked;
  var ccronologia_ar2 = document.getElementById("ccronologia_ar2").checked;
  var ccronologia_ar3 = document.getElementById("ccronologia_ar3").checked;
  var ccronologia_ar4 = document.getElementById("ccronologia_ar4").checked;
  var ccronologia_ar5 = document.getElementById("ccronologia_ar5").checked;
  var ccronologia_ar6 = document.getElementById("ccronologia_ar6").checked;
  var ccronologia_ar7 = document.getElementById("ccronologia_ar7").checked;
  var ccronologia_ar8 = document.getElementById("ccronologia_ar8").checked;
  var ccronologia_ar9 = document.getElementById("ccronologia_ar9").checked;

   var PTR_CRONO = '[{"tipo":"CHKM"},{"resid":215,"estado":'+ccronologia_ar1+',"resvalor":"Arcaico tardío"},{"resid":214,"estado":'+ccronologia_ar2+',"resvalor":"Arcaico temprano-medio"},{"resid":221,"estado":'+ccronologia_ar3+',"resvalor":"Colonial"},{"resid":217,"estado":'+ccronologia_ar4+',"resvalor":"Formativo tardío"},{"resid":216,"estado":'+ccronologia_ar5+',"resvalor":"Formativo temprano-medio"},{"resid":220,"estado":'+ccronologia_ar6+',"resvalor":"Inca"},{"resid":219,"estado":'+ccronologia_ar7+',"resvalor":"Intermedio tardío"},{"resid":222,"estado":'+ccronologia_ar8+',"resvalor":"Republicano"},{"resid":218,"estado":'+ccronologia_ar9+',"resvalor":"Tiwanaku"}]';

   //console.log("PTR_CRONOOOOO",PTR_CRONO);

  var cfuncionalidad_ar1 = document.getElementById("cfuncionalidad_ar1").checked;
  var cfuncionalidad_ar2 = document.getElementById("cfuncionalidad_ar2").checked;
  var cfuncionalidad_ar3 = document.getElementById("cfuncionalidad_ar3").checked;
  var cfuncionalidad_ar4 = document.getElementById("cfuncionalidad_ar4").checked;
  var cfuncionalidad_ar5 = document.getElementById("cfuncionalidad_ar5").checked;
  var cfuncionalidad_ar6 = document.getElementById("cfuncionalidad_ar6").checked;
  var cfuncionalidad_ar7 = document.getElementById("cfuncionalidad_ar7").checked;
  var cfuncionalidad_ar8 = document.getElementById("cfuncionalidad_ar8").checked;

  var PTR_IA_FUNC = '[{"tipo":"CHKM"},{"resid":230,"estado":'+cfuncionalidad_ar1+',"resvalor":"Administrativa"},{"resid":229,"estado":'+cfuncionalidad_ar2+',"resvalor":"Caminera"},{"resid":228,"estado":'+cfuncionalidad_ar3+',"resvalor":"Ceremonial"},{"resid":226,"estado":'+cfuncionalidad_ar4+',"resvalor":"Defensiva"},{"resid":223,"estado":'+cfuncionalidad_ar5+',"resvalor":"Doméstica"},{"resid":227,"estado":'+cfuncionalidad_ar6+',"resvalor":"Funeraria"},{"resid":225,"estado":'+cfuncionalidad_ar7+',"resvalor":"Manufactura"},{"resid":224,"estado":'+cfuncionalidad_ar8+',"resvalor":"Productiva"}]';

  var cintervencion_ar1 = document.getElementById("cintervencion_ar1").checked;
  var cintervencion_ar2 = document.getElementById("cintervencion_ar2").checked;
  var cintervencion_ar3 = document.getElementById("cintervencion_ar3").checked;
  var cintervencion_ar4 = document.getElementById("cintervencion_ar4").checked;
  var cintervencion_ar5 = document.getElementById("cintervencion_ar5").checked;

  var PTR_IA_TIP_INTER ='[{"tipo":"CHKM"},{"resid":235,"estado":'+cintervencion_ar1+',"resvalor":"Apertura de Área"},{"resid":233,"estado":'+cintervencion_ar2+',"resvalor":"Calas Auger"},{"resid":232,"estado":'+cintervencion_ar3+',"resvalor":"Examen de perfiles"},{"resid":231,"estado":'+cintervencion_ar4+',"resvalor":"Hallazgo Accidental"},{"resid":234,"estado":'+cintervencion_ar5+',"resvalor":"Sondeos"}]';

  var cinmuebles_ar1 = document.getElementById("cinmuebles_ar1").checked; 
  var cinmuebles_ar2 = document.getElementById("cinmuebles_ar2").checked;
  var cinmuebles_ar3 = document.getElementById("cinmuebles_ar3").checked;
  var cinmuebles_ar4 = document.getElementById("cinmuebles_ar4").checked;
  var cinmuebles_ar5 = document.getElementById("cinmuebles_ar5").checked;
  var cinmuebles_ar6 = document.getElementById("cinmuebles_ar6").checked;
  var cinmuebles_ar7 = document.getElementById("cinmuebles_ar7").checked;
  var cinmuebles_ar8 = document.getElementById("cinmuebles_ar8").checked;
  var cinmuebles_ar9 = document.getElementById("cinmuebles_ar9").checked;
  var cinmuebles_ar10 = document.getElementById("cinmuebles_ar10").checked;
  var cinmuebles_ar11 = document.getElementById("cinmuebles_ar11").checked;
  
  var PTR_IA_ELEM_INM ='[{"tipo":"CHKM"},{"resid":164,"estado":'+cinmuebles_ar1+',"resvalor":"Almacenes"},{"resid":162,"estado":'+cinmuebles_ar2+',"resvalor":"Arquitectura Ceremonial"},{"resid":171,"estado":'+cinmuebles_ar3+',"resvalor":"Arte Repestre"},{"resid":165,"estado":'+cinmuebles_ar4+',"resvalor":"Caminos"},{"resid":167,"estado":'+cinmuebles_ar5+',"resvalor":"Entierros"},{"resid":166,"estado":'+cinmuebles_ar6+',"resvalor":"Habitaciones"},{"resid":172,"estado":'+cinmuebles_ar7+',"resvalor":"Obras hidráulicas"},{"resid":169,"estado":'+cinmuebles_ar8+',"resvalor":"Socavones"},{"resid":170,"estado":'+cinmuebles_ar9+',"resvalor":"Talleres"},{"resid":168,"estado":'+cinmuebles_ar10+',"resvalor":"Terraceado"},{"resid":163,"estado":'+cinmuebles_ar11+',"resvalor":"Torres Funerarias"}]';

   var cmuebles_ar1 = document.getElementById("cmuebles_ar1").checked;
   var cmuebles_ar2 = document.getElementById("cmuebles_ar2").checked;
   var cmuebles_ar3 = document.getElementById("cmuebles_ar3").checked;
   var cmuebles_ar4 = document.getElementById("cmuebles_ar4").checked;
   var cmuebles_ar5 = document.getElementById("cmuebles_ar5").checked;
 
  var PTR_IA_ELEM_MUEB = '[{"tipo":"CHKM"},{"resid":209,"estado":'+cmuebles_ar1+',"resvalor":"Cerámica"},{"resid":210,"estado":'+cmuebles_ar2+',"resvalor":"Líticos"},{"resid":211,"estado":'+cmuebles_ar3+',"resvalor":"Metales"},{"resid":213,"estado":'+cmuebles_ar4+',"resvalor":"Óseos Animales"},{"resid":212,"estado":'+cmuebles_ar5+',"resvalor":"Óseos Humanos"}]';

  var cas_data = '{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"2","PTR_CREA":"'+ccreador_arq+'","PTR_DEN":"'+cdenominacion+'","PTR_MUNI":"'+cubiMunicipio+'","PTR_MACRO":"'+cubiMacrodistrito+'","PTR_LUG":"'+cubiLocal+'","PTR_IA_ELEM_DESC":"'+cdescripInmuebles+'", "PTR_IA_EST_CONS":"'+cestadoConserva+'","PTR_IA_POLIG":"'+cubicacionPoligono+'", "PTR_IA_DESC_EST_CONS":"'+cdescripEstad_s+'","PTR_IA_OBS_EST":"'+cobservacion+'", "PTR_IA_TAFON":"'+ctafonomia+'","PTR_IA_DESC_RUR":"'+cdescripContextoRural+'", "PTR_IA_DESC_URB":"'+cdescripContextoUrbana+'","PTR_IA_TAFON_DESC":"'+cdescripTafonomia+'","PTR_IA_YACIM":"'+cdescripYacimiento+'","PTR_IA_ELEM_MUEBLE":"'+celementos+'","PTR_IA_DESC":"'+cdescrip+'","PTR_IA_ELEM_MUEB_DESC":"'+cdescripcionMueble+'",';

  var cas_data2 = '"PTR_IA_DESC_ELEM_INM":"'+PTR_IA_DESC_ELEM_INM+'","PTR_CRONO_DESC":"'+PTR_CRONO_DESC+'","PTR_CRONO":'+PTR_CRONO+',"PTR_IA_FUNC":'+PTR_IA_FUNC+',"PTR_IA_TAM_YACIM":"'+PTR_IA_TAM_YACIM+'","PTR_IA_TIP_INTER":'+PTR_IA_TIP_INTER+',"PTR_IA_NUM_POZ":"'+PTR_IA_NUM_POZ+'","PTR_IA_AR_INTER":"'+PTR_IA_AR_INTER+'","PTR_IA_PROF_MIN":"'+PTR_IA_PROF_MIN+'","PTR_IA_PROF_MAX":"'+PTR_IA_PROF_MAX+'","PTR_IA_ELEM_INM":'+PTR_IA_ELEM_INM+',"PTR_IA_ELEM_MUEB":'+PTR_IA_ELEM_MUEB+',"PTR_IA_ELEM_INM_DESC":"'+PTR_IA_ELEM_INM_DESC+'","PTR_IA_ELEM_MUEB_DESC":"'+PTR_IA_ELEM_MUEB_DESC+'","PTR_IA_OBS":"'+PTR_IA_OBS+'"}';

  var cas_data_total  = cas_data + cas_data2;
    console.log("cas_data_total",cas_data_total);
  var jdata = JSON.stringify(cas_data_total);
  //console.log("stringyfi",cas_data_total);

  var xcas_nro_caso = correlativog;
  var xcas_act_id = 137;
  var xcas_usr_actual_id = 1155;
  var xcas_estado_paso = 'Recibido';
  var xcas_nodo_id = 1;
  var xcas_usr_id = 1;
  var xcas_ws_id = 1;
  var xcas_asunto = 'asunto';
  var xcas_tipo_hr = 'hora';
  var xcas_id_padre = 1;
  var xcampo_f ='campo'; 

  var formData = {"identificador": "SERVICIO_PATRIMONIO-941", "parametros":'{"xcas_nro_caso":'+xcas_nro_caso+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata+',"xcas_nombre_caso":"'+ccodPatrimonio+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
  //////////console.log("formDataaaa",formData);
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {

      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer '+token,
       },                  
       success: function(data){ 

        console.log("dataaaaaaaaaaREgistrarSitios",data);
        buscarPatrimonioPorTipo(1);
        swal( "Exíto..!", "Se registro correctamente los bienes arqueologicos...!", "success" );
        $( "#cdenominacion" ).val(""); 
        $( "#cubiMunicipio" ).val(""); 
        $( "#cubiMacrodistrito" ).val(""); 
        $( "#cubiLocal" ).val(""); 
        $( "#cdescripInmuebles" ).val(""); 
        $( "#cestadoConserva" ).val(""); 
        $( "#cubicacionPoligono" ).val(""); 
        $( "#cdescripEstado" ).val(""); 
        $( "#cobservacion" ).val(""); 
        $( "#ctafonomia" ).val(""); 
        $( "#cdescripContextoUrbana" ).val(""); 
        $( "#cdescripContextoRural" ).val(""); 
        $( "#cdescripTafonomia" ).val(""); 
        $( "#cdescripYacimiento" ).val(""); 
        $( "#celementos" ).val("");
        $( "#cdescrip" ).val(""); 
        $( "#cdescripcionMueble" ).val("");

        var idCasos =data[0].sp_insertar_p_bienes_arqueologicos1;
        setTimeout(function(){
      
          var url = '{{ url("adjuntosPatrimonio",["casosId"=>"casosId1","numeroficha"=>"numeroficha1"])}}';
        url = url.replace('casosId1', idCasos);
        url = url.replace('numeroficha1', xcas_nro_caso);
          //console.log("url",url);
          location.href = url;
        }, 1000);
      },
      error: function(result) { 
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  

}, 500);
});

function editarBienesArqueologicos(cas_id){

    var formData = {"identificador": "SERVICIO_PATRIMONIO-1016", "parametros":'{"id_caso":'+cas_id+'}'};

    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {

       $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+token,
        },
        success: function(dataIN) {

          var idCaso = JSON.parse(dataIN[0].data).cas_id;
          var nroFicha = JSON.parse(dataIN[0].data).cas_nro_caso;
          var dataP= JSON.parse(dataIN[0].data);
          var g_tipo=dataP.cas_datos.g_tipo; 
          var g_tipoArq=dataP.cas_datos.TIPO_ARQ;
          console.log("g_tipoArq",g_tipoArq); 

           if ( g_tipo == 'PTR_BMA') {
              if(g_tipoArq == '1'){

            var PTR_DESC_BIEN = caracteresEspecialesActualizar(dataP.cas_datos.PTR_DESC_BIEN);
            var PTR_MATERIAL = caracteresEspecialesActualizar(dataP.cas_datos.PTR_MATERIAL);
            var PTR_TECN = caracteresEspecialesActualizar(dataP.cas_datos.PTR_TECN);
            var PTR_MANF = caracteresEspecialesActualizar(dataP.cas_datos.PTR_MANF);
            var PTR_DECOR = caracteresEspecialesActualizar(dataP.cas_datos.PTR_DECOR);
            var PTR_ATRIB = caracteresEspecialesActualizar(dataP.cas_datos.PTR_ATRIB);
            var PTR_DESC_ESTADO = caracteresEspecialesActualizar(dataP.cas_datos.PTR_DESC_ESTADO);
            var PTR_OBS = caracteresEspecialesActualizar(dataP.cas_datos.PTR_OBS);
            var PTR_FUNC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_FUNC);

            $("#acasoId_m").val(idCaso);
            $("#acreadorM_arq").val(dataP.cas_datos.PTR_CREA);
            $("#anroFicha_m").val(nroFicha);
            $("#aprovenienciaMueble_m").val(dataP.cas_datos.PTR_PROVIENE_C);
            $("#arelacionPieza_m").val(dataP.cas_datos.PTR_RELACION);
            $("#aubicacionActual_m").val(dataP.cas_datos.PTR_UBIC_ACT);
            $("#aDescripcionMueble_m").val(PTR_DESC_BIEN);
            $("#amaterial_m").val(PTR_MATERIAL);
            $("#atecnica_m").val(PTR_TECN);
            $("#amanufactura_m").val(PTR_MANF);
            $("#adecoracion_m").val(PTR_DECOR);
            $("#aatributos_m").val(PTR_ATRIB);
            $("#aestadoConserva_m").val(dataP.cas_datos.PTR_EST_CONS);
            $("#adescripEstado_m").val(PTR_DESC_ESTADO);
            $("#aobservacion_m").val(PTR_OBS);
            $("#acronologia_m").val(dataP.cas_datos.PTR_CRONO);
            $("#afuncion_m").val(PTR_FUNC);
              
              }

             if(g_tipoArq == '2'){

              var cronologia =dataP.cas_datos.PTR_CRONO;
              var funcionSitio = dataP.cas_datos.PTR_IA_FUNC;
              var tipoIntervencion = dataP.cas_datos.PTR_IA_TIP_INTER;
              var elemInmuebles = dataP.cas_datos.PTR_IA_ELEM_INM;
              var elemMuebles = dataP.cas_datos.PTR_IA_ELEM_MUEB;
  
              document.getElementById("acronologia_ar1").checked = cronologia[1].estado;
              document.getElementById("acronologia_ar2").checked = cronologia[2].estado;
              document.getElementById("acronologia_ar3").checked = cronologia[3].estado;
              document.getElementById("acronologia_ar4").checked = cronologia[4].estado;
              document.getElementById("acronologia_ar5").checked = cronologia[5].estado;
              document.getElementById("acronologia_ar6").checked = cronologia[6].estado;
              document.getElementById("acronologia_ar7").checked = cronologia[7].estado;
              document.getElementById("acronologia_ar8").checked = cronologia[8].estado;
              document.getElementById("acronologia_ar9").checked = cronologia[9].estado;

              document.getElementById("afuncionalidad_ar1").checked = funcionSitio[1].estado;
              document.getElementById("afuncionalidad_ar2").checked = funcionSitio[2].estado;
              document.getElementById("afuncionalidad_ar3").checked = funcionSitio[3].estado;
              document.getElementById("afuncionalidad_ar4").checked = funcionSitio[4].estado;
              document.getElementById("afuncionalidad_ar5").checked = funcionSitio[5].estado;
              document.getElementById("afuncionalidad_ar6").checked = funcionSitio[6].estado;
              document.getElementById("afuncionalidad_ar7").checked = funcionSitio[7].estado;
              document.getElementById("afuncionalidad_ar8").checked = funcionSitio[8].estado;

              document.getElementById("aintervencion_ar1").checked = tipoIntervencion[1].estado;
              document.getElementById("aintervencion_ar2").checked = tipoIntervencion[2].estado;
              document.getElementById("aintervencion_ar3").checked = tipoIntervencion[3].estado;
              document.getElementById("aintervencion_ar4").checked = tipoIntervencion[4].estado;
              document.getElementById("aintervencion_ar5").checked = tipoIntervencion[5].estado;

              document.getElementById("ainmuebles_ar1").checked = elemInmuebles[1].estado;
              document.getElementById("ainmuebles_ar2").checked = elemInmuebles[2].estado;
              document.getElementById("ainmuebles_ar3").checked = elemInmuebles[3].estado;
              document.getElementById("ainmuebles_ar4").checked = elemInmuebles[4].estado;
              document.getElementById("ainmuebles_ar5").checked = elemInmuebles[5].estado;
              document.getElementById("ainmuebles_ar6").checked = elemInmuebles[6].estado;
              document.getElementById("ainmuebles_ar7").checked = elemInmuebles[7].estado;
              document.getElementById("ainmuebles_ar8").checked = elemInmuebles[8].estado;
              document.getElementById("ainmuebles_ar9").checked = elemInmuebles[9].estado;
              document.getElementById("ainmuebles_ar10").checked = elemInmuebles[10].estado;
              document.getElementById("ainmuebles_ar11").checked = elemInmuebles[11].estado;

              document.getElementById("amuebles_ar1").checked = elemMuebles[1].estado;
              document.getElementById("amuebles_ar2").checked = elemMuebles[2].estado;
              document.getElementById("amuebles_ar3").checked = elemMuebles[3].estado;
              document.getElementById("amuebles_ar4").checked = elemMuebles[4].estado;
              document.getElementById("amuebles_ar5").checked = elemMuebles[5].estado;

              var PTR_CREA =  dataP.cas_datos.PTR_CREA;
              var PTR_IA_ELEM_MUEBLE = dataP.cas_datos.PTR_IA_ELEM_MUEBLE;
              console.log("PTR_CREA_MUEBLE",PTR_CREA);
              var PTR_IA_YACIM = dataP.cas_datos.PTR_IA_YACIM;
              var PTR_IA_TAFON = dataP.cas_datos.PTR_IA_TAFON;
              var PTR_IA_EST_CONS = dataP.cas_datos.PTR_IA_EST_CONS;
              var PTR_LUG = dataP.cas_datos.PTR_LUG;
              var PTR_MACRO = dataP.cas_datos.PTR_MACRO;
              var PTR_MUNI = dataP.cas_datos.PTR_MUNI;
              var PTR_DEN = dataP.cas_datos.PTR_DEN;
              var PTR_IA_PROF_MAX = dataP.cas_datos.PTR_IA_PROF_MAX;
              var PTR_IA_PROF_MIN = dataP.cas_datos.PTR_IA_PROF_MIN;
              var PTR_IA_AR_INTER = dataP.cas_datos.PTR_IA_AR_INTER;
              var PTR_IA_NUM_POZ = dataP.cas_datos.PTR_IA_NUM_POZ;
              var PTR_IA_DESC_ELEM_INM = dataP.cas_datos.PTR_IA_DESC_ELEM_INM;

              var PTR_IA_ELEM_DESC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_ELEM_DESC);
              var PTR_IA_POLIG = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_POLIG);
              var PTR_IA_DESC_EST_CONS = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_DESC_EST_CONS);
              var PTR_IA_OBS_EST = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_OBS_EST);
              var PTR_IA_DESC_RUR = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_DESC_RUR);
              var PTR_IA_DESC_URB = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_DESC_URB);
              var PTR_IA_TAFON_DESC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_TAFON_DESC);
              var PTR_IA_DESC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_DESC);
              var PTR_IA_ELEM_MUEB_DESC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_ELEM_MUEB_DESC);
            ////////formulario 2/////////
              var PTR_CRONO_DESC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_CRONO_DESC);
              var PTR_IA_TAM_YACIM = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_TAM_YACIM);
              var PTR_IA_ELEM_INM_DESC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_ELEM_INM_DESC);
              var PTR_IA_ELEM_MUEB_DESC = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_ELEM_MUEB_DESC);
              var PTR_IA_OBS = caracteresEspecialesActualizar(dataP.cas_datos.PTR_IA_OBS);

              $("#acasoId_s").val(idCaso);
              $("#anroFicha").val(nroFicha);
              $("#acreador_arq").val(PTR_CREA);
              $("#adenominacion").val(PTR_DEN);
              $("#aubiMunicipio").val(PTR_MUNI);
              $("#aubiMacrodistrito").val(PTR_MACRO);
              $("#aubiLocal").val(PTR_LUG);
              $("#adescripInmuebles").val(PTR_IA_ELEM_DESC);
              $("#aestadoConserva").val(PTR_IA_EST_CONS);
              $("#aubicacionPoligono").val(PTR_IA_POLIG);
              $("#adescripEstado").val(PTR_IA_DESC_EST_CONS);
              $("#aobservacion").val(PTR_IA_OBS_EST);
              $("#atafonomia").val(PTR_IA_TAFON);
              $("#adescripContextoUrbana").val(PTR_IA_DESC_URB);
              $("#adescripContextoRural").val(PTR_IA_DESC_RUR);
              $("#adescripTafonomia").val(PTR_IA_TAFON_DESC);
              $("#adescripYacimiento").val(PTR_IA_YACIM);
              $("#aelementos").val(PTR_IA_ELEM_MUEBLE);
              $("#adescrip").val(PTR_IA_DESC);
              $("#adescripcionMueble").val(PTR_IA_ELEM_MUEB_DESC);


              $("#adescripcionElem_ar").val(PTR_IA_DESC_ELEM_INM);
              $("#acronologiaDesc_ar").val(PTR_CRONO_DESC);
              $("#atamañoSitio_ar").val(PTR_IA_TAM_YACIM);
              $("#anumeroPozo_ar").val(PTR_IA_NUM_POZ);
              $("#aareaInter_ar").val(PTR_IA_AR_INTER);
              $("#aprofundidadMin_ar").val(PTR_IA_PROF_MIN);
              $("#aprofundidadMax_ar").val(PTR_IA_PROF_MAX);
              $("#adescripcionInmu_ar").val(PTR_IA_ELEM_INM_DESC);
              $("#adescripcionMue_ar").val(PTR_IA_ELEM_MUEB_DESC);
              $("#aobservaciones_ar").val(PTR_IA_OBS);
             }
            }
           },
        error: function(result) {
          swal( "Error..!", "Error....", "error" );
        }
      });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
      });
};


function limpiarM(){
  $( "#cprovenienciaMueble_m" ).val(""); 
  $( "#crelacionPieza_m" ).val(""); 
  $( "#cubicacionActual_m" ).val(""); 
  $( "#cDescripcionMueble_m" ).val(""); 
  $( "#cmaterial_m" ).val("");
  $( "#ctecnica_m" ).val("");
  $( "#cmanufactura_m" ).val("");
  $( "#cdecoracion_m" ).val("");
  $( "#catributos_m" ).val("");
  $( "#cestadoConserva_m").val("");
  $( "#cdescripEstado_m" ).val("");
  $( "#cobservacion_m" ).val("");
  $( "#ccronologia_m" ).val("");
  $( "#cfuncion_m" ).val("");

}
function limpiarS(){
  $( "#cdenominacion" ).val(""); 
  $( "#cubiMunicipio" ).val(""); 
  $( "#cubiMacrodistrito" ).val(""); 
  $( "#cubiLocal" ).val(""); 
  $( "#cdescripInmuebles" ).val(""); 
  $( "#cestadoConserva" ).val(""); 
  $( "#cubicacionPoligono" ).val(""); 
  $( "#cdescripEstado" ).val(""); 
  $( "#cobservacion" ).val(""); 
  $( "#ctafonomia" ).val(""); 
  $( "#cdescripContextoUrbana" ).val(""); 
  $( "#cdescripContextoRural" ).val(""); 
  $( "#cdescripTafonomia" ).val(""); 
  $( "#cdescripYacimiento" ).val(""); 
  $( "#celementos" ).val("");
  $( "#cdescrip" ).val(""); 
  $( "#cdescripcionMueble" ).val(""); 

}

$( "#actualizarMuebleArqueologico" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      //////////console.log("aqui registro muebles");
      var acasoId_m = $( "#acasoId_m" ).val();
      var anroFicha_m = $( "#anroFicha_m" ).val();
      var acodPatrimonio = 'PTR_BMA'+anroFicha_m+'/2018';
      var acreadorM_arq = $( "#acreadorM_arq").val();
      var aprovenienciaMueble = document.getElementById('aprovenienciaMueble_m').value;
      var arelacionPieza = $( "#arelacionPieza_m" ).val();
      //////////console.log("aprovenienciaMueble",aprovenienciaMueble);
      //////////console.log("arelacionPieza",arelacionPieza);
      var aubicacionActual = $("#aubicacionActual_m").val();
      var aDescripcionMueble_m = $("#aDescripcionMueble_m").val();
      var amaterial_m = $("#amaterial_m").val();
      var atecnica_m =$("#atecnica_m").val();
      var amanufactura_m = $("#amanufactura_m").val();
      var adecoracion_m = $("#adecoracion_m").val();
      var aatributos_m = $("#aatributos_m").val();
      var aestadoConserva = document.getElementById('aestadoConserva_m').value;
      var adescripEstado_m = $("#adescripEstado_m").val();
      var aobservacion_m = $("#aobservacion_m").val();
      var acronologia = $("#acronologia_m").val();
      var afuncion_m = $("#afuncion_m").val();
      var g_tipo = 'PTR_BMA';

      var aDescripcionMueble_m = caracteresEspecialesRegistrar(aDescripcionMueble_m);
      var amaterial_m = caracteresEspecialesRegistrar(amaterial_m);
      var atecnica_m = caracteresEspecialesRegistrar(atecnica_m);
      var amanufactura_m = caracteresEspecialesRegistrar(amanufactura_m);
      var adecoracion_m = caracteresEspecialesRegistrar(adecoracion_m);
      var aatributos_m = caracteresEspecialesRegistrar(aatributos_m);
      var adescripEstado_m = caracteresEspecialesRegistrar(adescripEstado_m);
      var aobservacion_m = caracteresEspecialesRegistrar(aobservacion_m);
      var afuncion_m = caracteresEspecialesRegistrar(afuncion_m);

      var cas_data = '{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"1","PTR_CREA":"'+acreadorM_arq+'","PTR_PROVIENE_C":"'+aprovenienciaMueble+'","PTR_RELACION":"'+arelacionPieza+'","PTR_UBIC_ACT":"'+aubicacionActual+'","PTR_DESC_BIEN":"'+aDescripcionMueble_m+'","PTR_MATERIAL":"'+amaterial_m+'","PTR_TECN":"'+atecnica_m+'","PTR_MANF":"'+amanufactura_m+'","PTR_DECOR":"'+adecoracion_m+'","PTR_ATRIB":"'+aatributos_m+'","PTR_EST_CONS":"'+aestadoConserva+'","PTR_DESC_ESTADO":"'+adescripEstado_m+'","PTR_OBS":"'+aobservacion_m+'","PTR_CRONO":"'+acronologia+'","PTR_FUNC":"'+afuncion_m+'"}';
      var jdata = JSON.stringify(cas_data);
      //////////console.log("jdataaaa",jdata);

      var xcas_act_id = 137;
      var xcas_usr_actual_id = 1155;
      var xcas_estado_paso = 'Recibido';
      var xcas_nodo_id = 1;
      var xcas_usr_id = 1;
      var xcas_ws_id = 1;
      var xcas_asunto = 'asunto';
      var xcas_tipo_hr = 'hora';
      var xcas_id_padre = 1;
      var xcampo_f ='campo'; 

      var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_m+',"xcas_nro_caso":'+anroFicha_m+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata+',"xcas_nombre_caso":"'+acodPatrimonio+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
      ////////console.log("formData",formData);

      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: { 
         'authorization': 'Bearer' + token,
       },
       success: function(data){
        buscarPatrimonioPorTipo(1);
        swal( "Exíto..!", "Se actualizo correctamente el registro del Bienes Arqueologicos...!", "success" );
      },
      error: function(result) {
        swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
      }
    });         
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
});

$( "#actualizarSitioArqueologico" ).click(function(){
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      //////////console.log("aqui registro sitios");
      var acasoId_m = $( "#acasoId_s" ).val();
      var anroFicha = $( "#anroFicha" ).val();
      var acreador_arq = $( "#acreador_arq" ).val();
      console.log("acreador_arq",acreador_arq);
      var adenominacion =$( "#adenominacion" ).val();
      var acodPatrimonio = 'PTR_BMA'+(anroFicha)+'/2018';
      var aprovenienciaMueble2 = 'aoleccion';
      var aubiMunicipio = $( "#aubiMunicipio" ).val();
      var aubiMacrodistrito =$("#aubiMacrodistrito").val();
      var aubiLocal = $("#aubiLocal").val();
      var adescripInmuebles = $("#adescripInmuebles").val();
      var aestadoConserva = $("#aestadoConserva").val();
      var aubicacionPoligono =$("#aubicacionPoligono").val();
      var adescripEstado = $("#adescripEstado").val();
      var aobservacion = $("#aobservacion").val();
      var atafonomia = $("#atafonomia").val();
      var adescripContextoUrbana = $("#adescripContextoUrbana").val();
      var adescripContextoRural = $("#adescripContextoRural").val();
      var adescripTafonomia = $("#adescripTafonomia").val();
      var adescripYacimiento = $("#adescripYacimiento").val();
      var aelementos = $("#aelementos").val();
      var adescrip = $("#adescrip").val();
      var adescripcionMueble = $("#adescripcionMueble").val();
      var g_tipo = 'PTR_BMA';

      var adescripInmuebles = caracteresEspecialesRegistrar(adescripInmuebles);
      var aubicacionPoligono = caracteresEspecialesRegistrar(aubicacionPoligono);
      var adescripEstado = caracteresEspecialesRegistrar(adescripEstado);
      var aobservacion = caracteresEspecialesRegistrar(aobservacion);
      var adescripContextoUrbana = caracteresEspecialesRegistrar(adescripContextoUrbana);
      var adescripContextoRural = caracteresEspecialesRegistrar(adescripContextoRural);
      var adescripTafonomia = caracteresEspecialesRegistrar(adescripTafonomia);
      var adescrip = caracteresEspecialesRegistrar(adescrip);
      var adescripcionMueble = caracteresEspecialesRegistrar(adescripcionMueble);

     
  ////////////////////////formulario 2/////////////////////

  var PTR_IA_DESC_ELEM_INM = $("#adescripcionElem_ar").val();
  var PTR_CRONO_DESC = $("#acronologiaDesc_ar").val();
  var PTR_IA_TAM_YACIM = $("#atamañoSitio_ar").val();
  var PTR_IA_NUM_POZ = $("#anumeroPozo_ar").val();
  var PTR_IA_AR_INTER = $("#aareaInter_ar").val();
  var PTR_IA_PROF_MIN = $("#aprofundidad_ar").val();
  var PTR_IA_PROF_MAX = $("#aprofundidadMax_ar").val();
  var PTR_IA_ELEM_INM_DESC = $("#adescripcionMue_ar").val();
  var PTR_IA_ELEM_MUEB_DESC = $("#adescripcionMue_ar").val();
  var PTR_IA_OBS = $("#aobservaciones_ar").val();

  var PTR_CRONO_DESC = caracteresEspecialesRegistrar(PTR_CRONO_DESC);
  var PTR_IA_TAM_YACIM = caracteresEspecialesRegistrar(PTR_IA_TAM_YACIM);
  var PTR_IA_ELEM_INM_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_INM_DESC);
  var PTR_IA_ELEM_MUEB_DESC = caracteresEspecialesRegistrar(PTR_IA_ELEM_MUEB_DESC);
  var PTR_IA_OBS = caracteresEspecialesRegistrar(PTR_IA_OBS);

  var ccronologia_ar1 = document.getElementById("acronologia_ar1").checked;
  var ccronologia_ar2 = document.getElementById("acronologia_ar2").checked;
  var ccronologia_ar3 = document.getElementById("acronologia_ar3").checked;
  var ccronologia_ar4 = document.getElementById("acronologia_ar4").checked;
  var ccronologia_ar5 = document.getElementById("acronologia_ar5").checked;
  var ccronologia_ar6 = document.getElementById("acronologia_ar6").checked;
  var ccronologia_ar7 = document.getElementById("acronologia_ar7").checked;
  var ccronologia_ar8 = document.getElementById("acronologia_ar8").checked;
  var ccronologia_ar9 = document.getElementById("acronologia_ar9").checked;

  var PTR_CRONO = '[{"tipo":"CHKM"},{"resid":215,"estado":'+ccronologia_ar1+',"resvalor":"Arcaico tardío"},{"resid":214,"estado":'+ccronologia_ar2+',"resvalor":"Arcaico temprano-medio"},{"resid":221,"estado":'+ccronologia_ar3+',"resvalor":"Colonial"},{"resid":217,"estado":'+ccronologia_ar4+',"resvalor":"Formativo tardío"},{"resid":216,"estado":'+ccronologia_ar5+',"resvalor":"Formativo temprano-medio"},{"resid":220,"estado":'+ccronologia_ar6+',"resvalor":"Inca"},{"resid":219,"estado":'+ccronologia_ar7+',"resvalor":"Intermedio tardío"},{"resid":222,"estado":'+ccronologia_ar8+',"resvalor":"Republicano"},{"resid":218,"estado":'+ccronologia_ar9+',"resvalor":"Tiwanaku"}]';

  var cfuncionalidad_ar1 = document.getElementById("afuncionalidad_ar1").checked;
  var cfuncionalidad_ar2 = document.getElementById("afuncionalidad_ar2").checked;
  var cfuncionalidad_ar3 = document.getElementById("afuncionalidad_ar3").checked;
  var cfuncionalidad_ar4 = document.getElementById("afuncionalidad_ar4").checked;
  var cfuncionalidad_ar5 = document.getElementById("afuncionalidad_ar5").checked;
  var cfuncionalidad_ar6 = document.getElementById("afuncionalidad_ar6").checked;
  var cfuncionalidad_ar7 = document.getElementById("afuncionalidad_ar7").checked;
  var cfuncionalidad_ar8 = document.getElementById("afuncionalidad_ar8").checked;

  var PTR_IA_FUNC = '[{"tipo":"CHKM"},{"resid":230,"estado":'+cfuncionalidad_ar1+',"resvalor":"Administrativa"},{"resid":229,"estado":'+cfuncionalidad_ar2+',"resvalor":"Caminera"},{"resid":228,"estado":'+cfuncionalidad_ar3+',"resvalor":"Ceremonial"},{"resid":226,"estado":'+cfuncionalidad_ar4+',"resvalor":"Defensiva"},{"resid":223,"estado":'+cfuncionalidad_ar5+',"resvalor":"Doméstica"},{"resid":227,"estado":'+cfuncionalidad_ar6+',"resvalor":"Funeraria"},{"resid":225,"estado":'+cfuncionalidad_ar7+',"resvalor":"Manufactura"},{"resid":224,"estado":'+cfuncionalidad_ar8+',"resvalor":"Productiva"}]';

  var cintervencion_ar1 = document.getElementById("aintervencion_ar1").checked;
  var cintervencion_ar2 = document.getElementById("aintervencion_ar2").checked;
  var cintervencion_ar3 = document.getElementById("aintervencion_ar3").checked;
  var cintervencion_ar4 = document.getElementById("aintervencion_ar4").checked;
  var cintervencion_ar5 = document.getElementById("aintervencion_ar5").checked;

 var PTR_IA_TIP_INTER ='[{"tipo":"CHKM"},{"resid":235,"estado":'+cintervencion_ar1+',"resvalor":"Apertura de Área"},{"resid":233,"estado":'+cintervencion_ar2+',"resvalor":"Calas Auger"},{"resid":232,"estado":'+cintervencion_ar3+',"resvalor":"Examen de perfiles"},{"resid":231,"estado":'+cintervencion_ar4+',"resvalor":"Hallazgo Accidental"},{"resid":234,"estado":'+cintervencion_ar5+',"resvalor":"Sondeos"}]';

  var cinmuebles_ar1 = document.getElementById("ainmuebles_ar1").checked; 
  //console.log("cinmuebles_ar1",cinmuebles_ar1);
  var cinmuebles_ar2 = document.getElementById("ainmuebles_ar2").checked;
  //console.log("cinmuebles_ar2",cinmuebles_ar2);
  var cinmuebles_ar3 = document.getElementById("ainmuebles_ar3").checked;
  var cinmuebles_ar4 = document.getElementById("ainmuebles_ar4").checked;
  var cinmuebles_ar5 = document.getElementById("ainmuebles_ar5").checked;
  var cinmuebles_ar6 = document.getElementById("ainmuebles_ar6").checked;
  var cinmuebles_ar7 = document.getElementById("ainmuebles_ar7").checked;
  var cinmuebles_ar8 = document.getElementById("ainmuebles_ar8").checked;
  var cinmuebles_ar9 = document.getElementById("ainmuebles_ar9").checked;
  var cinmuebles_ar10 = document.getElementById("ainmuebles_ar10").checked;
  var cinmuebles_ar11 = document.getElementById("ainmuebles_ar11").checked;
  
  var PTR_IA_ELEM_INM ='[{"tipo":"CHKM"},{"resid":164,"estado":'+cinmuebles_ar1+',"resvalor":"Almacenes"},{"resid":162,"estado":'+cinmuebles_ar2+',"resvalor":"Arquitectura Ceremonial"},{"resid":171,"estado":'+cinmuebles_ar3+',"resvalor":"Arte Repestre"},{"resid":165,"estado":'+cinmuebles_ar4+',"resvalor":"Caminos"},{"resid":167,"estado":'+cinmuebles_ar5+',"resvalor":"Entierros"},{"resid":166,"estado":'+cinmuebles_ar6+',"resvalor":"Habitaciones"},{"resid":172,"estado":'+cinmuebles_ar7+',"resvalor":"Obras hidráulicas"},{"resid":169,"estado":'+cinmuebles_ar8+',"resvalor":"Socavones"},{"resid":170,"estado":'+cinmuebles_ar9+',"resvalor":"Talleres"},{"resid":168,"estado":'+cinmuebles_ar10+',"resvalor":"Terraceado"},{"resid":163,"estado":'+cinmuebles_ar11+',"resvalor":"Torres Funerarias"}]';

   var cmuebles_ar1 = document.getElementById("amuebles_ar1").checked;
   var cmuebles_ar2 = document.getElementById("amuebles_ar2").checked;
   var cmuebles_ar3 = document.getElementById("amuebles_ar3").checked;
   var cmuebles_ar4 = document.getElementById("amuebles_ar4").checked;
   var cmuebles_ar5 = document.getElementById("amuebles_ar5").checked;
 
  var PTR_IA_ELEM_MUEB = '[{"tipo":"CHKM"},{"resid":209,"estado":'+cmuebles_ar1+',"resvalor":"Cerámica"},{"resid":210,"estado":'+cmuebles_ar2+',"resvalor":"Líticos"},{"resid":211,"estado":'+cmuebles_ar3+',"resvalor":"Metales"},{"resid":213,"estado":'+cmuebles_ar4+',"resvalor":"Óseos Animales"},{"resid":212,"estado":'+cmuebles_ar5+',"resvalor":"Óseos Humanos"}]';
  
  var cas_data = '{"g_tipo":"'+g_tipo+'","TIPO_ARQ":"2","PTR_CREA":"'+acreador_arq+'","PTR_DEN":"'+adenominacion+'","PTR_MUNI":"'+aubiMunicipio+'","PTR_MACRO":"'+aubiMacrodistrito+'","PTR_LUG":"'+aubiLocal+'","PTR_IA_ELEM_DESC":"'+adescripInmuebles+'", "PTR_IA_EST_CONS":"'+aestadoConserva+'","PTR_IA_POLIG":"'+aubicacionPoligono+'", "PTR_IA_DESC_EST_CONS":"'+adescripEstado+'","PTR_IA_OBS_EST":"'+aobservacion+'", "PTR_IA_TAFON":"'+atafonomia+'","PTR_IA_DESC_RUR":"'+adescripContextoRural+'", "PTR_IA_DESC_URB":"'+adescripContextoUrbana+'","PTR_IA_TAFON_DESC":"'+adescripTafonomia+'","PTR_IA_YACIM":"'+adescripYacimiento+'","PTR_IA_ELEM_MUEBLE":"'+aelementos+'","PTR_IA_DESC":"'+adescrip+'","PTR_IA_ELEM_MUEB_DESC":"'+adescripcionMueble+'",';

   var cas_data2 = '"PTR_IA_DESC_ELEM_INM":"'+PTR_IA_DESC_ELEM_INM+'","PTR_CRONO_DESC":"'+PTR_CRONO_DESC+'","PTR_CRONO":'+PTR_CRONO+',"PTR_IA_FUNC":'+PTR_IA_FUNC+',"PTR_IA_TAM_YACIM":"'+PTR_IA_TAM_YACIM+'","PTR_IA_TIP_INTER":'+PTR_IA_TIP_INTER+',"PTR_IA_NUM_POZ":"'+PTR_IA_NUM_POZ+'","PTR_IA_AR_INTER":"'+PTR_IA_AR_INTER+'","PTR_IA_PROF_MIN":"'+PTR_IA_PROF_MIN+'","PTR_IA_PROF_MAX":"'+PTR_IA_PROF_MAX+'","PTR_IA_ELEM_INM":'+PTR_IA_ELEM_INM+',"PTR_IA_ELEM_MUEB":'+PTR_IA_ELEM_MUEB+',"PTR_IA_ELEM_INM_DESC":"'+PTR_IA_ELEM_INM_DESC+'","PTR_IA_ELEM_MUEB_DESC":"'+PTR_IA_ELEM_MUEB_DESC+'","PTR_IA_OBS":"'+PTR_IA_OBS+'"}';

  var cas_data_total  = cas_data + cas_data2;
    //console.log("cas_data_total",cas_data_total);
  var jdata = JSON.stringify(cas_data_total);
    var xcas_act_id = 137;
    var xcas_usr_actual_id = 1155;
    //var xcas_nombre_caso='PTR_BMA77/2018' ;
    var xcas_estado_paso = 'Recibido';
    var xcas_nodo_id = 1;
    var xcas_usr_id = 1;
    var xcas_ws_id = 1;
    var xcas_asunto = 'asunto';
    var xcas_tipo_hr = 'hora';
    var xcas_id_padre = 1;
    var xcampo_f ='campo'; 

    var formData = {"identificador": "SERVICIO_PATRIMONIO-943", "parametros":'{"xcas_id":'+acasoId_m+',"xcas_nro_caso":'+anroFicha+',"xcas_act_id":'+xcas_act_id+',"xcas_usr_actual_id":'+xcas_usr_actual_id+',"xcas_datos":'+jdata+',"xcas_nombre_caso":"'+acodPatrimonio+'","xcas_estado_paso":"'+xcas_estado_paso+'","xcas_nodo_id":'+xcas_nodo_id+',"xcas_usr_id":'+xcas_usr_id+',"xcas_ws_id":'+xcas_ws_id+',"xcas_asunto":"'+xcas_asunto+'","xcas_tipo_hr":"'+xcas_tipo_hr+'","xcas_id_padre":'+xcas_id_padre+',"xcampo_f":"'+xcampo_f+'"}'};
    //////////console.log("formData",formData);
    
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: { 
       'authorization': 'Bearer' + token,
     },
     success: function(data){
   //console.log("dataaaaaaActualiza",data);
      buscarPatrimonioPorTipo(1);
      swal( "Exíto..!", "Se actualizo correctamente el registro del Bienes Arqueologicos...!", "success" );

    },
    error: function(result) {
      swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
    }
  });         
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});  
});

function generarProforma(id_cas, g_tipo){
     setTimeout(function(){
     var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerar',['datos'=>'d1']) }}"; 
     urlPdf = urlPdf.replace('d1', id_cas);
     console.log("urlPdf",urlPdf);
     window.open(urlPdf);
   }, 500);
  }
function imprimirArchivoFotografico(id_cas, g_tipo){
     setTimeout(function(){
     var urlPdf = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerarArchFoto',['datos'=>'d1']) }}"; 
     urlPdf = urlPdf.replace('d1', id_cas);
     window.open(urlPdf); 
   }, 500);
  }

function imprimirFichaTodo(id_cas, g_tipo){
     setTimeout(function(){
     var urlFicha = "{{ action('reportesPatrimonioCultural\reporteBienArqueologico@getGenerarFichaTodo',['datos'=>'d1']) }}"; 
     urlFicha = urlFicha.replace('d1', id_cas);
    //console.log("urlFicha",urlFicha);
    window.open(urlFicha); 
  }, 500);
  }

$(document).ready(function (){

 buscarPatrimonioPorTipo(1);
 selectCampos();
 cargarDatosUsuario();
});
</script>

<script  src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script >

  $('textarea').ckeditor();


</script>
@endpush