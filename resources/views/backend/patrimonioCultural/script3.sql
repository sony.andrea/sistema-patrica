CREATE OR REPLACE FUNCTION patrimonio_cultural.sp_eliminar_adjuntos(
    xdoc_idd integer,
    xdoc_usuario integer
    )
  RETURNS boolean AS
$BODY$
BEGIN

  UPDATE patrimonio_cultural.dms_gt_documentos
  SET doc_estado = 'B', doc_usuario = xdoc_usuario, doc_modificacion = now()
  WHERE doc_idd  = xdoc_idd;
   
     RETURN true;
END;
$BODY$sp_eliminar_patrimonio_cultural
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION patrimonio_cultural.sp_eliminar_adjuntos(integer, integer)
  OWNER TO postgres;

=============================0
$_conn1=coneccionBD("18");
$_tipo1="Select";
$_query1="select * from patrimonio_cultural.sp_eliminar_adjuntos(
$ydoc_idd,
$ydoc_usuario
)";
$_dataDB1=ejecutar_Query($_conn1,$_query1,$_tipo1);
echo($_dataDB1);