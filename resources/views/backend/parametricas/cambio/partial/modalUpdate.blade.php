<div class="modal fade modal-default" id="myUpdate" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            {!! Form::open(['class'=>'form-vertical','id'=>'form_cambioufv_update'])!!}
            <div class="modal-header">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <section class="content-header">
                                    <div class="header_title">
                                        <h3>
                                            Actualizar Cambio UFV
                                            <small>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                 <span aria-hidden="true">&times;</span>
                                                </button>
                                            </small>
                                        </h3>
                                    </div>
                                </section>
                            </div>   
                        </div>
                    </div>
                    <div class="panel-body">
                        <div style="padding:20px">
                            <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">         
                            <input type="hidden" name="id2" id="id2">
                            <input type="hidden" name="cat_id" id="cat_id">
                            <div class="col-md-12">
                                <div class="form-group col-md-4"> 
                                    <label class="control-label"> 
                                         Valor:
                                    </label>
                                    <input id="valor2" name="valor2" type="number" step="0.01" class="form-control" placeholder="Ingrese Valor">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">
                                        Valor de Catalogo:
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        <select class=" form-control" placeholder ="Seleccionar Valor de Catalogo" id ="valorcat2" name="valorcat2">
                                            <option value="-1">Seleccionar Valor de Catalogo</option>
                                            <option value="3">SUS.</option>
                                            <option value="4">BS.</option>
                                        </select>
                                    </span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">
                                        Fecha de registro:
                                    </label>
                                    <input id="fecha2" name="fecha2" type="date" class="form-control" placeholder="Ingrese Fecha de registro">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                       <button class="btn btn-primary" type="submit" id="actualizar" data-dismiss="modal" type="button">
                            <i class="glyphicon glyphicon-pencil"></i> Modificar
                       </button> 
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
