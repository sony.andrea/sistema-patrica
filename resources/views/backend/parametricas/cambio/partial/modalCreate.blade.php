<div class="modal fade modal-default" id="myCreate" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            {!! Form::open(['class'=>'form-vertical','id'=>'form_cambioufv_create'])!!}
            <div class="modal-header">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <section class="content-header">
                                    <div class="header_title">
                                        <h3>
                                            Registrar Cambio UFV
                                            <small>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                 <span aria-hidden="true">&times;</span>
                                                </button>
                                            </small>
                                        </h3>
                                    </div>
                                </section>
                            </div>   
                        </div>
                    </div>
                    <div class="panel-body">
                        <div  style="padding:20px">
                            <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">         
                            <input type="hidden" name="id1" id="id1">
                            <div class="col-md-12">
                                <div class="form-group col-md-4"> 
                                    <label class="control-label"> 
                                     Valor:
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        <input id="valor1" name="valor1" type="number" step="0.01" class="form-control" placeholder="Ingrese Valor">
                                    </span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">
                                        Valor de Catalogo:
                                    </label>
                                    <span class="block input-icon input-icon-right">
                                        <select class=" form-control" placeholder ="Seleccionar Valor de Catalogo" id ="valorcat1" name="valorcat1">
                                            <option value="-1">Seleccionar Valor de Catalogo</option>
                                            <option value="3">SUS.</option>
                                            <option value="4">BS.</option>
                                        </select>
                                    </span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">
                                        Fecha de registro:
                                    </label>
                                    <input id="fecha1" name="fecha1" type="date" class="form-control" placeholder="Ingrese Fecha de registro">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" onClick="limpiar()">
                            <i class="fa fa-eraser"></i> Limpiar
                        </button>
                        <a type = "submit" class = "btn btn-primary fa fa-save" data-dismiss="modal" id="registrar"> Guardar</a>  
                    </div> 
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
