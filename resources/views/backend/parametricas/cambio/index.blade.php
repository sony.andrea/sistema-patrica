@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.cambio.partial.modalCreate')
@include('backend.parametricas.cambio.partial.modalUpdate')
<div class="row">
  <div class="col-md-12">
    <section class="content-header">
      <div class="header_title">
       <h3>
        Cambio UFV
      </h3>
  </div>
</section>
</div>
</div>
      @include('backend.componentes.encabezadoPaginacion')
<br>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
      </div>
      <div class="table-responsive">
        <div id="listCambioUfv">
        </div>  
      </div>
      @endsection
      @push('scripts')
      <script>
 //var urlEjecutaRegla = 'http://192.168.6.113:80/MotorServicio/public/api/reglaNegocio/ejecutarWeb';
 //var id_usu = {{$usuarioid}};

///------------LISTAR---
function listar(paging_pagenumber){
  LtsCambioUfv = '';
  LtsCambioUfv += '<div class="row">';
  LtsCambioUfv += '<div class="col-md-12" >' ;
  LtsCambioUfv += '<table id="" class="table responsive table-striped" cellspacing="1" width="100%" >';
  LtsCambioUfv += '<thead><tr><th align="left">Nro.</th><th align="center" class="form-nobajas-cabecera">Opciones</th><th align="left">Valor</th><th align="left">Valor Catalogo</th><th align="left">Fecha de Registro</th></tr></thead>';  
  var seleccion = $("#opcion").val();
  var paging_pagesize = $("#reg").val();
  var formData = {"identificador": 'SERVICIO_SIERRA-2',"parametros": '{"xtipolista":"'+seleccion+'","paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumber+'}'};
  console.log("formData",formData)
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token){
      $.ajax({
        type        : 'POST',
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer ' + token,
        },
        success: function(dataIN){
          var datos = dataIN;
          if (datos !="[{ }]") {
            var tam = datos.length;
          }
          else{
            var tam = 0;
          }
          for (var i = 0; i < tam; i++){
            var estado = JSON.parse(datos[i].data).cambio_estado;
             var fecharegistro = JSON.parse(datos[i].data).cambio_registrado.split("T");
            LtsCambioUfv += '<tr><td align="left">'+(i+1)+'</td>';
            LtsCambioUfv += '<td class="form-nobajas"><button class="btncirculo btn-xs btn-primary btn-estado-'+estado+'" fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar('+JSON.parse(datos[i].data).cambio_id+', \''+JSON.parse(datos[i].data).cambio_data.valor+'\', \''+JSON.parse(datos[i].data).cambio_data.catalogo_id+'\', \''+JSON.parse(datos[i].data).cambio_registrado+'\');"><i class="glyphicon glyphicon-pencil"></i></button><button onClick="darBaja('+JSON.parse(datos[i].data).cambio_id+');" class="btncirculo btn-xs btn-danger btn-estado-'+estado+'" style="background:#FA8072" type="button" data-toggle="modal" data-placement="top" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button></td>';
            LtsCambioUfv += '<td align="left">'+JSON.parse(datos[i].data).cambio_data.valor+'</td>';
            if(JSON.parse(datos[i].data).cambio_data.catalogo_id == 4){
              LtsCambioUfv +='</td>'+'<td align="left">'+ 'BS.'+'</td>';
            } 
            else {
              if(JSON.parse(datos[i].data).cambio_data.catalogo_id == 3){
                LtsCambioUfv +='</td>'+'<td align="left">'+ 'SUS.'+'</td>';
              }
            }
             
            LtsCambioUfv += '<td align="left">'+fecharegistro[0]+'</td></tr>';
          }
          LtsCambioUfv += '</table></div></div>';
          LtsCambioUfv += '<ul class="pager">';
          LtsCambioUfv += '<li><a href="#" style="color:blue;" onClick="btnAnterior()">Anterior</a></li>';
          LtsCambioUfv += '<li><a href="#" style="color:blue;" onClick="btnSiguiente()">Siguiente</a></li>';
          LtsCambioUfv += '</ul>';
          LtsCambioUfv += '</div>';
          LtsCambioUfv += '</table>';
          $('#listCambioUfv').html(LtsCambioUfv);
          bloquearBoton();
        },
        error: function (xhr, status, error){
        }
      });
    },
    error: function(result){
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

function bloquearBoton(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}

///-------------LIMPIAR INPUTS---
function limpiar(){
  var id1= $( "#id" ).val('');
  var v1= $( "#valor1" ).val('');
  var vc1= $( "#valorcat1" ).val('');
  var f1= $( "#fecha1" ).val('');
}
///-------------INSERTAR NUEVO REGISTRO---
$( "#registrar" ).click(function() {
  var v1 = $( "#valor1" ).val();
  var vc1 = $( "#valorcat1" ).val();
  var f1= $( "#fecha1" ).val();
  var data = '{"valor":"'+v1+'","catalogo_id":"'+vc1+'"}';
  var jdata = JSON.stringify(data);
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      var formData = {"identificador": 'SERVICIO_SIERRA-3',"parametros": '{"xcambio_data":'+jdata+',"xcambio_usr_id":'+id_usu+',"xcambio_registrado":"'+f1+'"}'};
      console.log("registro",formData);
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer ' + token,
        },
        success: function(dataIN) { 
          console.log(dataIN);
          swal("La Actividad Eventual!", "Se registrado correctamente!", "success");
          limpiar();   
          listar(1);   
        },
        error: function(result) {
          swal( "Error..!", "Verifique que los campos esten llenados Gracias...!", "error" );
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
});

///-------------ELIMINAR------
function darBaja(id){

  swal({ 
    title: "Esta seguro de eliminar el Item?",
    text: "Presione ok para eliminar el registro de la base de datos!",
    type: "warning",   showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Eliminar!",
    closeOnConfirm: false
  }, 
  function(){
    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_SIERRA-5',"parametros": '{"xcambio_id":'+id+',"xcambio_usr_id":'+id_usu+'}'};
        console.log("id",formData);
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
            'authorization': 'Bearer ' + token,
          },
          success: function(dataIN){
            listar(1);
            swal("Item!", "Fue eliminado correctamente!", "success");
          },
          error: function (xhr, status, error) {
          }
        });
      },
      error: function(result) {
        swal("Opss..!", "Hubo algun problema...!", "error")
      }
    });
  });
};

///---RECUPERAR DATOS A ACTUALIZAR---
function editar(cambio_id, cambio_valor, catalogo_id, cambio_registrado){
  console.log("cambioR",cambio_id, cambio_valor, catalogo_id, cambio_registrado);
  $("#id2").val(cambio_id);
  $("#valor2").val(cambio_valor);
  $("#valorcat2").val(catalogo_id);
  $("#cat_id").val(catalogo_id);
  var fecharegistro = cambio_registrado.split("T");
  $("#fecha2").val(fecharegistro[0]);
  //console.log("id",id2,"v2",v2,"vvv",vvvv,"reg2",reg2);
}

///--------------ACTUALIZAR---
$( "#actualizar" ).click(function() {
  var id2 = $( "#id2" ).val();
  var v2 = $( "#valor2" ).val();
  var catid2 = $( "#valorcat2" ).val();
  var reg2 = $( "#fecha2" ).val();
  console.log("id",id2,"v2",v2,"catid2",catid2,"reg2",reg2);
  var data2 = '{"valor":"'+v2+'","catalogo_id":"'+catid2+'"}';
  var jdata2 = JSON.stringify(data2);

  var formData = {"identificador": 'SERVICIO_SIERRA-4',"parametros": '{"xcambio_id":'+id2+',"xcambio_data":'+jdata2+',"xcambio_usr_id":'+id_usu+',"xcambio_registrado":"'+reg2+'"}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer ' + token,
        },
        success: function(data){
          swal( "Exito..!", "Se Actualizo correctamente...!", "success" );
          listar(1);
        },
        error: function (xhr, status, error) {
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "Verifique que los campos esten llenados Gracias...!", "error" );
    }
  });
});


$(document).ready(function(){
  listar(1);
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#form_cambioufv_create').bootstrapValidator({
      message: 'Este valor no es válido',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        valor1: {
          row: '.form-group',
          validators: {
            notEmpty: {
              message: 'Ingrese el valor'
            },
            stringLength:{
              min: 1,
              max: 5,
              message: 'Sobrepasa el numero de caracteres permitidos'
            },
          }
        },
        valorcat1: {
          row: '.form-group',
          validators: {
            notEmpty: {
              message: 'Ingrese el alto'
            },
            regexp: {
              regexp: /^[0-9]+$/,
              message: 'Sólo numeros'
            },
          }
        },
        fecha1: {
          row: '.form-group',
          validators: {
            date: {
              format: 'YYYY/MM/DD',
              message: 'The value is not a valid date'
            }
          }
        },
      }
    }).on('error.field.bv', function(e, data) {
      if (data.bv.getSubmitButton()) {
        data.bv.disableSubmitButtons(false);
      }
    }).on('success.field.fv', function(e, data) {
      if (data.bv.getSubmitButton()) {
        data.bv.disableSubmitButtons(false);
      }
    });
    $('#form_cambioufv_update').bootstrapValidator({
      message: 'Este valor no es válido',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        valor2: {
          row: '.form-group',
          validators: {
            notEmpty: {
              message: 'Ingrese el valor'
            },
            stringLength:{
              min: 1,
              max: 5,
              message: 'Sobrepasa el numero de caracteres permitidos'
            },
          }
        },
        valorcat2: {
          row: '.form-group',
          validators: {
            notEmpty: {
              message: 'Ingrese Gestión'
            },
            regexp: {
              regexp: /^[0-9]+$/,
              message: 'Sólo numeros'
            },
          }
        },
        fecha2: {
          row: '.form-group',
          validators: {
            date: {
              format: 'YYYY/MM/DD',
              message: 'The value is not a valid date'
            }
          }
        },
      }
    })
  });
</script>

@endpush