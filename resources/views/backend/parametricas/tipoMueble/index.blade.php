@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.tipoMueble.partial.modalCreate')
@include('backend.parametricas.tipoMueble.partial.modalUpdate')

<div class="row">
  <div class="col-md-12">
    <section class="content-header">
      <div class="header_title">
        <h3>   
          Tipo Mueble
        </h3> 
      <br>
    </div>
  </section>
</div>
</div>
 @include('backend.componentes.encabezadoPaginacion'); 
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class= "box-header with-border"></div> 
        <div class="box-body"></div>
          <div id="listTipoMueble"></div>
    </div>
  </div>
</div>
  @endsection
  @push('scripts')

<script>
  var id_usu= {{$usuarioid}};

function listar(paging_pagenumbe)
{
  htmlLista='';
  htmlLista+= '<div class="row">';
  htmlLista+= '<div class="col-md-12" >' ;
  htmlLista+= '<table id="lts-listEmpresasFunerarias" class="table table-striped" cellspacing="0" width="100%" >';
  htmlLista+= '<thead>';
  htmlLista+= '<tr>';
  htmlLista+= '<th align="center"></th>';
  htmlLista+= '<th align="center">Nro.</th>';
  htmlLista+= '<th align="center" class="form-nobajas-cabecera">Opciones</th>';
  htmlLista+= '<th align="left">Descripción</th>';
  htmlLista+= '</tr>';
  htmlLista+= '</thead>';

  var opcion= $( "#opcion" ).val();
  var paging_pagesize = $( "#reg" ).val();
  var formData = {"identificador":'SISTEMA_VALLE-503',"parametros":'{"xtipolista":"'+opcion+'","paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'};
 
    $.ajax(
  {
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) 
    { 
      $.ajax(
      {
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: 
        {
          'authorization': 'Bearer'+token,
        },
        success: function(dataIN)
        {
          if (dataIN!= "[{ }]") 
          {
            var tam= dataIN.length;
          }
          for (var i= 0; i< tam; i++) 
          {
                var estado_=JSON.parse(dataIN[i].data).tipo_mueble_estado;
                htmlLista+='<tr><td align="left"></td>';
                htmlLista+='<td align="left">'+(i+1)+'</td>';
                htmlLista+='<td class="form-nobajas"><button class="btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar('+JSON.parse(dataIN[i].data).tipo_mueble_id+', \''+JSON.parse(dataIN[i].data).jsonb.descripcion+'\');" ><i class="glyphicon glyphicon-pencil"></i></button>  <button class="btncirculo btn-xs btn-danger btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' +JSON.parse(dataIN[i].data).tipo_mueble_id+ ');"><i class="glyphicon glyphicon-trash"></i></button></td>';
  
                htmlLista+='<td align="left">'+JSON.parse(dataIN[i].data).jsonb.descripcion+'</td>';
          }

            htmlLista+='</table></div></div>';
            htmlLista +='<div>';
            htmlLista +='<ul class="pager">';
            htmlLista +='<li><a href="#" onClick="btnAnterior()">Anterior</a></li>';
            htmlLista +='<li><a href="#" onClick="btnSiguiente()">Siguiente</a></li>';
            htmlLista+='</ul>';
            htmlLista +='</div>';
            $('#listTipoMueble').html(htmlLista);    
            bloquearBoton();
          },
            error: function (xhr, status, error) {
         }
       });
      },
            error: function(result) {
            swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};         

$( "#registrar" ).click(function() 
 { 
    var opcion="activo";
    $.ajax(
    {
       type        : 'GET',
       url         : urlToken,
       data        : '',
      success: function(token) 
     {
      var cdescripcionTipoMueble = $("#cdescripcionTipoMueble").val();
      var data2='{"descripcion":"'+cdescripcionTipoMueble+'"}';
      var jdata2 = JSON.stringify(data2)
      var formData = {"identificador":'SISTEMA_VALLE-504',"parametros": '{"xtipo_mueble_data":'+jdata2+', "xtipo_mueble_usr_id":'+id_usu+'}'};

       $.ajax(
       {
         type        : 'POST',            
         url         : urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: 
          {
            'authorization': 'Bearer '+ token ,
          },
          success: function(dataIN)
          { 
            swal( "Exito..!", "Se Registro correctamente...!", "success" );
            listar(1);
            $('#form-tipoMueble-create').data('bootstrapValidator').resetForm();
            $( "#cdescripcionTipoMueble" ).val("");  
            document.getElementById("opcion").value='activo';
          },
            error: function(result) 
          {
             swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
          },
        }
       );
      },
           error: function(result) 
           {
              swal( "Error..!", "No se puedo guardar los datos", "error" );
           },
     });  
 });

function limpiar()
{
  $( "#cdescripcionTipoMueble" ).val(""); 
}
  
function bloquearBoton()
{
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
} 

function editar(id,descrip)
{
  $("#aidTipoMueble").val(id);
  $("#adescripcionTipoMueble").val(descrip); 
}

$( "#actualizar" ).click(function() 
{ 
  var opcion="activo";
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) { 
      var aidTipoMueble=$("#aidTipoMueble").val();
      var adescripcionTipoMueble=$("#adescripcionTipoMueble").val();
    
      var data2='{"descripcion":"'+adescripcionTipoMueble+'"}';
      var jdata2 = JSON.stringify(data2);
      var formData = {"identificador": 'SISTEMA_VALLE-505',"parametros":'{"xtipo_mueble_id":"'+aidTipoMueble+'", "xtipo_mueble_data":'+jdata2+', "xtipo_mueble_usr_id":"'+id_usu+'"}'};
      console.log("Actualizar ",formData);
      
      $.ajax(
      {
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: 
       {
         'authorization': 'Bearer '+ token,
       },

       success: function(data)
       {
          swal( "Exito..!", "Se Actualizo correctamente ..!", "success" );
          listar(1);
          $('#form-tipoMueble-update').data('bootstrapValidator').resetForm();   
        },
      error: function(result) 
        {
          swal( "Alerta..!", "Verifique que los campos esten llenados Correctamente Gracias...!", "warning" );
        }
     });
        
    },
      error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
});

function darBaja(id){
  swal({   
    title: "Esta seguro de eliminar?",
    text: "Presione Ok para eliminar el registro de la base de datos!",
    type: "warning",   showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Ok",
    closeOnConfirm: false
       }, function(){
          $.ajax({
          type        : 'GET',
          url         :  urlToken,
          data        : '',
          success: function(token) {

          var formData = {"identificador": 'SISTEMA_VALLE-506', "parametros":'{"xtipo_mueble_id":'+id+', "xtipo_mueble_usr_id":'+id_usu+'}'};
          $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
                       'authorization': 'Bearer '+ token,
                   },
          success: function(dataIN){
          swal("Tipo Mueble!", "Fue eliminado correctamente!", "success");
          listar(1);
           },
         error: function(xhr, status, error) {
         swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
         }
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });  
  });
}

$(document).ready(function (){
  listar(1);

  $('#form-tipoMueble-create').bootstrapValidator({
      message: 'Este valor no es valido....',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          cdescripcionTipoMueble: {
          row: '.form-group',
              validators: {
                notEmpty: {
                 message: 'Ingrese un dato válido'
                }, 
                  stringLength:{
                        min: 1,
                        max: 40,
                        message: 'Sobrepasa el numero de caracteres permitidos'
                  },
                    regexp: {
                     regexp: /^[A-Za-z 0-9_-]+$/,
                     message: 'No se aceptan caracteres especiales'
                  },                 
                }
          },
      }
    }).on('error.field.bv', function(e, data) 
    {
      if (data.bv.getSubmitButton()) {
        data.bv.disableSubmitButtons(false);
      }
    }).on('success.field.fv', function(e, data) 
    {
      if (data.bv.getSubmitButton()) {
        data.bv.disableSubmitButtons(false);
      }
    });

    $('#form-tipoMueble-update').bootstrapValidator({
        message: 'Este valor no es valido....',
        feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
        },
         fields: {
          cdescripcionTipoMueble: {
          row: '.form-group',
              validators: {
                notEmpty: {
                 message: 'Ingrese un dato válido'
                }, 
                  stringLength:{
                        min: 1,
                        max: 40,
                        message: 'Sobrepasa el numero de caracteres permitidos'
                  },
                    regexp: {
                     regexp: /^[A-Za-z 0-9_-]+$/,
                     message: 'No se aceptan caracteres especiales'
                  },                 
                }
          },
      }
    }).on('error.field.bv', function(e, data) 
    {
      if (data.bv.getSubmitButton()) 
        {
          data.bv.disableSubmitButtons(false);
        }
    }).on('success.field.fv', function(e, data)
     {
      if (data.bv.getSubmitButton()) 
        {
          data.bv.disableSubmitButtons(false);
        }
    });
}); 

</script>
@endpush
