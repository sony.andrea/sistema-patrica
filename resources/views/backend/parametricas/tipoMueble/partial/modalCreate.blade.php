  <div class="modal fade modal-default" id="myCreate" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="row">
           <div class="col-md-12">
            <section class="panel panel-info">
             <div class="panel-heading">
              <h4>
                Registrar Tipo Mueble
                <small>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  
                </small>
              </h4>
            </div>
          </section>
        </div>   
      </div>
    </div>
    <div  style="padding:20px">
      <input type="hidden" name="csrf-token" value="{{csrf_token() }}" id="token">
      </td>            
         {!! Form::open(['id'=>'form-tipoMueble-create'])!!}
    
    <div class="col-md-7">
      <div class="form-group col-md-6">
        <label class="control-label">
           Descripción:
        </label>
        <input id="cdescripcionTipoMueble"  name="cdescripcionTipoMueble" type="text" class="form-control to-upper-case"  placeholder="Descripcion" >
      </div> 
   

   </div>
   
    
    {!! Form::close() !!}
    <div class="modal-footer">
      <button class="btn btn-default "  type="button" onClick="limpiar()">
        <i class="fa fa-eraser"></i> Limpiar
      </button>
      <a type = "button" class = "btn btn-primary"  id="registrar"  data-dismiss="modal"><i class="fa fa-save"></i> Guardar</a>        
    </div> 
  </div>
</div>
</div>
</div>
