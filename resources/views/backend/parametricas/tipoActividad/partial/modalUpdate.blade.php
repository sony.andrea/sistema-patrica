<div class="modal fade modal-default" data-backdrop= "static" data-keyboard= "false" id= "myUpdate3" tabindex= "-5">
  <div class= "modal-dialog" role= "document">
    <div class= "modal-content">
      {!! Form::open(['class'=>'form-vertical','id'=>'upact'])!!}
      <div class= "modal-body">
        <div class= "row">
          <div class= "col-xs-12 container-fluit">
            <div class= "panel panel-info">
              <div class= "panel-heading">
                <h4>
                    Modificar Tipo Actividad 
                    <button type= "button" class= "close" data-dismiss= "modal" aria-label= "Close">
                      <span aria-hidden= "true">&times;</span>
                    </button>
                </h4>
              </div>
              <div class= "panel-body">
                <div class= "caption">
                <hr>
                <input id= "token" name= "csrf-token" type= "hidden" value= "{{ csrf_token() }}">
                    <input id= "idacteven2" name= "idacteven2"  type= "hidden" value= "">
                    <div class= "row">
                        <div class= "col-md-12">
                            <div class= "form-group">
                                <div class= "col-sm-12">
                                    <label class= "control-label">
                                        Actividad:
                                    </label>
                                    {!! Form::text('actividad2', null, array('placeholder' => 'Ingrese actividad ','maxlength'=>'15','class' => 'form-control','id'=>'actividad2')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class= "row">
                        <div class= "col-md-12">
                            <div class= "form-group">
                                <div class= "col-sm-12">
                                    <label class= "control-label">
                                        Gestión:
                                    </label>
                                    {!! Form::number('gestion2', null, array('placeholder' => 'Ingrese Gestión ','maxlength'=>'4','class' => 'form-control','id'=>'gestion2')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class= "row">
                        <div class= "col-md-12">
                            <div class= "form-group">
                                <div class= "col-sm-12">
                                    <label class= "control-label">
                                        Clase:
                                    </label>
                                    {!! Form::text('clase2', null, array('placeholder' => 'Ingrese Clase ','maxlength'=>'3','class' => 'form-control','id'=>'clase2')) !!}
                                </div>
                            </div>
                        </div>
                        <div class= "col-md-12">
                            <div class= "form-group">
                                <div class= "col-sm-12">
                                    <label class= "control-label">
                                        Tipo:
                                    </label>
                                    {!! Form::text('tipo2', null, array('placeholder' => 'Ingrese Tipo ','maxlength'=>'3','class' => 'form-control','id'=>'tipo2')) !!}
                                </div>
                            </div>
                        </div>
                    </div>  
                </hr>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class= "modal-footer">
        <button class= "btn btn-primary" id= "actualizar" data-dismiss= "modal" type= "button">
            <i class= "glyphicon glyphicon-pencil"></i> Modificar
        </button>
      </div>
        {!! Form::close() !!}
    </div>
  </div>
</div>



