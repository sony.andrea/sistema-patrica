<div class="modal fade modal-default" data-backdrop= "static" data-keyboard= "false" id= "myCreate" tabindex= "-5">
  <div class= "modal-dialog" role= "document">
    <div class= "modal-content">
      {!! Form::open(['class'=>'form-vertical','id'=>'newact'])!!}
      <div class= "modal-body">
        <div class= "row">
          <div class= "col-xs-12 container-fluit">
            <div class= "panel panel-info">
              <div class= "panel-heading">
                <h4>
                    Registrar Tipo Actividad
                    <button type= "button" class= "close" data-dismiss= "modal" aria-label= "Close">
                      <span aria-hidden= "true">&times;</span>
                    </button>
                </h4>
              </div>
              <div class= "panel-body">
                <div class= "caption">
                   <hr>
                        <div class= "row">
                            <div class= "col-md-12">
                                <div class= "form-group">
                                    <div class= "col-sm-12">
                                        <label>
                                            Actividad:
                                        </label>
                                        <span class= "block input-icon input-icon-right">
                                            {!! Form::text('actividad1', null, array('placeholder' => 'Ingrese Actividad','maxlength'=>'20','class' => 'form-control','id'=>'actividad1')) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class= "row">
                            <div class= "col-md-12">
                                <div class= "form-group">
                                    <div class= "col-sm-12">
                                        <label>
                                            Gestión:
                                        </label>
                                        <span class= "block input-icon input-icon-right">
                                            {!! Form::number('gestion1', null, array('placeholder' => 'Ingrese Gestion','maxlength'=>'4','class' => 'form-control','id'=>'gestion1')) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class= "row">
                            <div class= "col-md-12">
                                <div class= "form-group">
                                    <div class= "col-sm-12">
                                        <label>
                                            Clase:
                                        </label>
                                        <span class= "block input-icon input-icon-right">
                                            {!! Form::text('clase1', null, array('placeholder' => 'Ingrese Clase','maxlength'=>'3','class' => 'form-control','id'=>'clase1')) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class= "row">
                            <div class= "col-md-12">
                                <div class= "form-group">
                                    <div class= "col-sm-12">
                                        <label>
                                            Tipo:
                                        </label>
                                        <span class= "block input-icon input-icon-right">
                                            {!! Form::text('tipo1', null, array('placeholder' => 'Ingrese Tipo','maxlength'=>'3','class' => 'form-control','id'=>'tipo1')) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </hr>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class= "modal-footer">
            <button class= "btn btn-default"  onClick= "limpiar()" type= "button">
            <i class= "fa fa-eraser"></i> Limpiar
            </button>
            <button class= "btn btn-primary" id= "registrar" data-dismiss= "modal" type= "button">
            <i class= "fa fa-save"></i> Guardar
            </button>
        </div>
         {!! Form::close() !!}
    </div>
  </div>
</div>
