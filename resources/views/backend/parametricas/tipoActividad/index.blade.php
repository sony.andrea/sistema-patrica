@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.tipoActividad.partial.modalCreate')
@include('backend.parametricas.tipoActividad.partial.modalUpdate')
<div class= "row">
   <div class= "col-md-12">
      <section class= "content-header">
       <div class= "header_title">
         <h3>      
            Tipo Actividad
         </h3>
       </div>
      </section>
   </div>
</div>
@include('backend.componentes.encabezadoPaginacion')

<br>
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-header with-border">
      </div>
      <div class= "box-body">
        <div id= "listatipoactividad">
        </div>  
      </div>
@endsection
@push('scripts')
<script>
var id_usu= {{$usid}};

///---FUNCION LISTAR---///
function listar(paging_pagenumber){
  LtsTipAct = '';
  LtsTipAct+= '<div class= "row">';
  LtsTipAct+= '<div class= "col-md-12" >' ;
  LtsTipAct+= '<table id= "" class= "table responsive table-striped" cellspacing= "1" width= "100%">';
  LtsTipAct+= '<thead><tr><th align= "left">Nro.</th><th align= "center" class= "">Opciones</th><th align= "left">Actividad</th><th align= "left">Gestión</th><th align= "left">Clase</th><th align= "left">Tipo</th></tr></thead>';  
  var seleccion= $("#seleccion").val();
  var paging_pagesize= $("#reg").val();
  var formData= {"identificador": 'SERVICIO_SIERRA-8', "parametros": '{"xtipolista": "'+seleccion+'", "paging_pagesize": '+paging_pagesize+', "paging_pagenumber":'+paging_pagenumber+'}'};
   $.ajax({
        type        : 'GET',
        url         :  urlToken,
        data        : '',
        success: function(token) {
          console.log(token);
          $.ajax({
          type        : 'POST',
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
          'authorization': 'Bearer' + token,
        },
          success: function(dataIN) {
              for (var i= 0; i< dataIN.length; i++) {

            console.log(JSON.parse(dataIN[i].data).tipo_actividad_data);

              }

            console.log(dataIN);
            
          var datos= dataIN;
              if (datos!= "[{ }]") {
              var tam= datos.length;}
          for (var i= 0; i< tam; i++) {
            var estado_=JSON.parse(dataIN[i].data).tipo_actividad_estado;
            LtsTipAct+= '<tr><td align= "left">'+(i+1)+'</td>';
            LtsTipAct+= '<td class= ""><button class= "btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target= "#myUpdate3" data-toggle= "modal" data-placement= "top" title= "Modificar" type= "button" onClick= "editar('+JSON.parse(datos[i].data).tipo_actividad_id+',\''+JSON.parse(datos[i].data).tipo_actividad_actividad+'\','+JSON.parse(datos[i].data).tipo_actividad_gestion+',\''+JSON.parse(datos[i].data).tipo_actividad_clase+'\',\''+JSON.parse(datos[i].data).tipo_actividad_tipo+'\');"><i class= "glyphicon glyphicon-pencil"></i></button><button onClick= "darBaja('+JSON.parse(datos[i].data).tipo_actividad_id+');" class= "btncirculo btn-xs btn-danger btn-estado-'+estado_+'" style= "background:#FA8072" type= "button" data-toggle= "modal" data-placement= "top" title= "Eliminar"><i class= "glyphicon glyphicon-trash"></i></button></td>';
            LtsTipAct+= '<td align= "left">'+JSON.parse(datos[i].data).tipo_actividad_data.actividad+'</td>';
            LtsTipAct+= '<td align= "left">'+JSON.parse(datos[i].data).tipo_actividad_data.getion+'</td>';
            LtsTipAct+= '<td align= "left">'+JSON.parse(datos[i].data).tipo_actividad_data.clase+'</td>';
            LtsTipAct+= '<td align= "left">'+JSON.parse(datos[i].data).tipo_actividad_data.tipo+'</td>';
            LtsTipAct+= '</tr>';
             }
                    LtsTipAct+= '</table></div></div>';
                    LtsTipAct+= '<ul class= "pager">';
                    LtsTipAct+= '<li><a href= "#" style= "color:blue;" onClick= "btnAnterior()">Anterior</a></li>';
                    LtsTipAct+= '<li><a href= "#" style= "color:blue;" onClick= "btnSiguiente()">Siguiente</a></li>';
                    LtsTipAct+= '</ul>';
                    LtsTipAct+= '</div>';
                    LtsTipAct+= '</table>';
                    $('#listatipoactividad').html(LtsTipAct);
                    bloquearBoton();
             },
             error: function (xhr, status, error) {
             }
         });
       },
       error: function(result) {
       swal( "Error..!", "No se puedo guardar los datos", "error" );
       },
  });
};

 function bloquearBoton(){
   $(".btn-estado-B").prop("disabled", true);
   $(".btn-estado-B").css("opacity", '0.5');
   }


///---FUNCION LIMPIAR---///
function limpiar(){
  var id1= $("#idacteven1").val('');
  var act1= $("#actividad1").val('');
  var gest1= $("#gestion1").val('');
  var cls1= $("#clase1").val('');
  var tip1= $("#tipo1").val('');
  }

///---FUNCION REGISTRAR---///
$("#registrar").click(function() {
    var act1= $("#actividad1").val();
    var gest1= $("#gestion1").val();
    var cls1= $("#clase1").val();
    var tip1= $("#tipo1").val();
    var valor ='{"valor":111111,"valor1":111111,"valor2":"wqwqwqw"  }';
    var tipo_actividad = '{"actividad":"'+act1+'","getion":'+gest1+',"clase":"'+cls1+'","tipo":"'+tip1+'"}';
  tipo_actividad = JSON.stringify(tipo_actividad);
    $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {
      console.log(tipo_actividad,id_usu,888888888);
      var formData = {"identificador": 'SERVICIO_SIERRA-10',"parametros": '{"xtipo_actividad_data": '+tipo_actividad+', "xtipo_actividad_usr_id": '+id_usu +'}'};
        $.ajax({
           type        : 'POST',            
           url         : urlRegla,
           data        : formData,
           dataType    : 'json',
           crossDomain : true,
           headers: {
            'authorization': 'Bearer' + token,
             },
            success: function(dataIN) { 
              swal("La Actividad Eventual!", "Se registrado correctamente!", "success");
              limpiar();   
              listar(1);   
              },
          error: function(result) {
            swal("Error..!", "Verifique que los campos esten llenados Gracias...!", "error");
          }
        });
    },
    error: function(result) {
      swal("Error..!", "No se puedo guardar los datos", "error");
     },
  });
});

///---FUNCION ELIMINAR---///
function darBaja(id){
  swal({ title: "Esta seguro de eliminar el Item?",
    text: "Presione ok para eliminar el registro de la base de datos!",
    type: "warning",   showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Eliminar!",
    closeOnConfirm: false
     }, 
     function(){
       $.ajax({
          type        : 'GET',
          url         : urlToken,
          data        : '',
          success: function(token) {
            var formData= {"identificador": 'SERVICIO_SIERRA-13', "parametros": '{"xtipo_actividad_id": '+id+', "xtipo_actividad_usr_id": '+id_usu+'}'};
            $.ajax({
              type        : 'POST',
              url         : urlRegla,
              data        : formData,
              dataType    : 'json',
              crossDomain : true,
              headers: {
                'authorization': 'Bearer' + token,
               },
              success: function(dataIN){
                listar(1);
                swal("Item!", "Fue eliminado correctamente!", "success");
                },
              error: function(xhr, status, error) {
                    }
             });
           },
          error: function(result) {swal("Opss..!", "Hubo algun problema...!", "error")}
       });
     });
 };

///---FUNCION EDITAR---///
function editar(id, actividad, gestion, clase, tipo){
    $("#idacteven2").val(id);
    $("#actividad2").val(actividad);
    $("#gestion2").val(gestion);
    $("#clase2").val(clase);
    $("#tipo2").val(tipo);
  }

///---FUNCION ACTUALIZAR---///
$("#actualizar").click(function() {
    var id2= $("#idacteven2").val();
    var actividad2= $("#actividad2").val();
    var gestion2= $("#gestion2").val();
    var clase2= $("#clase2").val();
    var tipo2= $("#tipo2").val();
    var formData= {"identificador": 'SERVICIO_SIERRA-26', "parametros": '{"ida": '+id2+', "acta": "'+actividad2+'", "gesta": '+gestion2+', "clsa": "'+clase2+'", "tipa": "'+tipo2+'", "user": '+id_usu+'}'};
    console.log(formData);
      $.ajax({
        type        : 'GET',
        url         : urlToken,
        data        : '',
        success: function(token) {
          $.ajax({
            type        : 'POST',
            url         : urlRegla,
            data        : formData,
            dataType    : 'json',
            crossDomain : true,
            headers: {
              'authorization': 'Bearer' + token,
            },
            success: function(data){
              swal("Exito..!", "Se Actualizo correctamente...!", "success");
              listar(1);
             },
            error: function (xhr, status, error) {
             }
           });
        },
        error: function(result) {
               swal("Error..!", "Verifique que los campos esten llenados Gracias...!", "error");
          }
      });
 });

$(document).ready(function(){
  listar(1);
}); 

</script>
<script type= "text/javascript">
        $(document).ready(function() {
          $('#newact').bootstrapValidator({
            message: 'Este valor no es válido',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
            actividad1: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                         message: 'Ingrese Actividad'
                        },
                        regexp: {
                            regexp: /(\s*[a-zA-Z]+$)/,
                            message: 'Sólo texto'
                        },
                    }
                },
            gestion1: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                            message: 'Ingrese el alto'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Sólo numeros'
                        },
                    }
                },
            clase1: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                            message: 'Ingrese el frente'
                        },
                        regexp: {
                            regexp: /(\s*[a-zA-Z]+$)/,
                            message: 'Sólo texto'
                        },
                    }
                },
            tipo1: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                            message: 'Ingrese Codigo Auxiliar'
                        },
                        regexp: {
                            regexp: /(\s*[a-zA-Z]+$)/,
                            message: 'Sólo texto'
                        },
                    }
                },
        }
      }).on('error.field.bv', function(e, data) {
        if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
      }).on('success.field.fv', function(e, data) {
        if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
      });
      $('#upact').bootstrapValidator({
            message: 'Este valor no es válido',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
            actividad2: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                         message: 'Ingrese Actividad'
                        },
                        regexp: {
                            regexp: /(\s*[a-zA-Z]+$)/,
                            message: 'Sólo letras'
                        },
                    }
                },
              gestion2: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                            message: 'Ingrese Gestión'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Sólo numeros'
                        },
                    }
                },
              clase2: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                            message: 'Ingrese Clase'
                        },
                        regexp: {
                            regexp: /(\s*[a-zA-Z]+$)/,
                            message: 'Sólo letras'
                        },
                    }
                },
              tipo2: {
                    row: '.form-group',
                    validators: {
                        notEmpty: {
                            message: 'Ingrese Tipo'
                        },
                        regexp: {
                            regexp: /(\s*[a-zA-Z]+$)/,
                            message: 'Sólo letras'
                        },
                    }
                },
            }
      })
 
   });
</script>
<script>
var paging_pagesize = $( "#reg" ).val();
 var paging_pagenumbe = 1;

 ///---FUNCION PAGINADO---///
 function btnAnterior(){
   paging_pagenumbe--;
   listar(paging_pagenumbe);
  }

 function btnSiguiente(){
  paging_pagenumbe++;
  listar(paging_pagenumbe);
  }

 function listarSelect(){
   paging_pagenumbe=1;
   listar(paging_pagenumbe);
  };

</script>
@endpush
