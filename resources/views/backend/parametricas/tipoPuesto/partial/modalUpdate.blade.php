<div class="modal fade modal-default" id="myUpdate" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
         <div class="col-md-12">
          <section class="panel panel-info">
           <div class="panel-heading">
            <h4>
              Editar datos del Tipo Puesto
              <small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </small>
            </h4>
          </div>
        </section>
      </div>   
    </div>
  </div>
  <div  style="padding:20px">
    <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">
    </td>            
    {!! Form::open(['class'=>'form-vertical','id'=>'form_resitem_update'])!!} 
    <input type="hidden" name="atipoPuesto_id" id="atipoPuesto_id">
    <div class="col-md-12">
      <div class="form-group col-md-6">
        <label class="control-label">
          Categoria:
        </label>
        <input id="acategoria"  name="acategoria" type="text" class="form-control not-negative" >
      </div>
      <div class="form-group col-md-6">
        <label class="control-label">
          Descripción:
        </label>
        <input id="adescripcion"  name="adescripcion" type="text" class="form-control not-negative" >
      </div>
    </div>
    {!! Form::close() !!}
    <div class="modal-footer">
      <a type = "button" class = "btn btn-primary"  id="actualizar" data-dismiss="modal"><i class="fa fa-save"></i> Modificar</a>        
    </div> 
  </div>
</div>
</div>
</div>

