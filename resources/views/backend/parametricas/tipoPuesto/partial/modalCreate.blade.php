<style type="text/css">
  #busqueda {
    height: 300px;
    width: auto;
    background: #f1f1f1;
    overflow-y: scroll;
  }
</style>
<div class="modal fade modal-default" id="myCreate" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="row">
             <div class="col-md-12">
              <section class="content-header">
               <div class="header_title">
                <h3>
                  Registrar Tipo Puesto
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h3>
              </div>
            </section>
          </div>   
        </div>
      </div>
      <div  style="padding:20px">
        <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">  {!! Form::open(['class'=>'form-vertical','id'=>'form_resitem_create'])!!}        
        <div class="col-md-12">
          <div class="form-group col-md-6">
            <label class="control-label">
              Categoria:
            </label>
            <input id="ccategoria"  name="ccategoria" type="text" class="form-control"  placeholder="Ingrese Categoria" >
          </div>
          <div class="form-group col-md-6">
            <label class="control-label">
              Descripcion:
            </label>
            <input id="cdescripcion" name="cdescripcion" type="text" class="form-control"  placeholder="Ingrese Descripcion">
          </div>   
        </div>
        <div class="col-md-12">
        </div>    
        {!! Form::close() !!}
        <div class="modal-footer">
          <button class="btn btn-default"  type="button" onClick="Limpiar()">
            <i class="fa fa-eraser"></i> Limpiar
          </button>
          <button class="btn btn-primary" id="registrar" data-dismiss="modal" type="button" >
            <i class="fa fa-save"></i> Guardar
          </button> 
        </div> 
      </div>
</div>
</div>
</div>
</div>
</div>