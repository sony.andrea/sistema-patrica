@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.tipoPuesto.partial.modalCreate')
@include('backend.parametricas.tipoPuesto.partial.modalUpdate')
<div class="row">
  <div class="col-md-12">
    <section class="content-header">
      <div class="header_title">
        <h3>   
          Tipo Puesto
        </h3>
      </div>
    </section> 
  </div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-header with-border">
      </div>
      <div class= "box-body">
        <div id="listTipoPuesto">
        </div>  
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')

<script>
  var _usrid= {{$usuarioid}};

function listar(paging_pagenumber)
{ 
  htmlLista='';
  htmlLista+= '<div class="row">';
  htmlLista+= '<div class="col-md-12" >' ;
  htmlLista+= '<table id="lts-listTipoPuesto" class="table table-striped" cellspacing="0" width="100%" >';
  htmlLista+= '<thead>';
  htmlLista+= '<tr>';
  htmlLista+= '<th align="center">Nro.</th>';
  htmlLista+= '<th align="center" class="form-nobajas-cabecera">Opciones</th>';
  htmlLista+= '<th align="left">Categoria</th>';
  htmlLista+= '<th align="left">Descripción</th>';
  htmlLista+= '</tr>';
  htmlLista+= '</thead>';
  
  var opcion=$( "#opcion" ).val();
  var paging_pagesize = $( "#reg" ).val();
  var formData = {"identificador": 'SISTEMA_VALLE-509',"parametros": '{"xtipolista":"'+opcion+'", "paging_pagesize":'+paging_pagesize+', "paging_pagenumber":'+paging_pagenumber+'}'};
  $.ajax(
  {
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) 
    { 
      $.ajax(
      {
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: 
        {
          'authorization': 'Bearer'+token,
        },
        success: function(dataIN)
        {
          if (dataIN!= "[{ }]") 
          {
            var tam= dataIN.length;
          }
          for (var i= 0; i< tam; i++) 
          {
            var estado=JSON.parse(dataIN[i].data).tipo_puesto_estado;
            var tipo_puesto_id= JSON.parse(dataIN[i].data).tipo_puesto_id;
            var categoria= JSON.parse(dataIN[i].data).tipo_puesto_data.categoria;
            var descripcion= JSON.parse(dataIN[i].data).tipo_puesto_data.descripcion; 
                    
                    htmlLista+='<tr>';
            htmlLista+='<tr><td align="left">'+(i+1)+'</td>';
            htmlLista+='<td class="form-nobajas"><button class="btncirculo btn-xs btn-primary btn-estado-'+estado+'" fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar('+JSON.parse(dataIN[i].data).tipo_puesto_id+', \''+JSON.parse(dataIN[i].data).tipo_puesto_data.categoria+'\',\''+JSON.parse(dataIN[i].data).tipo_puesto_data.descripcion+'\');" ><i class="glyphicon glyphicon-pencil"></i></button>  <button class="btncirculo btn-xs btn-danger btn-estado-'+estado+'" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' +JSON.parse(dataIN[i].data).tipo_puesto_id+ ');"><i class="glyphicon glyphicon-trash"></i></button></td>';
            htmlLista+='<td align="left">'+JSON.parse(dataIN[i].data).tipo_puesto_data.categoria+'</td>';
            htmlLista+='<td align="left">'+JSON.parse(dataIN[i].data).tipo_puesto_data.descripcion+'</td>';
          }
          htmlLista +='</table></div></div>';
          htmlLista +='<div>';
          htmlLista +='<ul class="pager">';
          htmlLista +='<li><a href="#" onClick="btnAnterior()">Anterior</a></li>';
          htmlLista +='<li><a href="#" onClick="btnSiguiente()">Siguiente</a></li>';
          htmlLista+='</ul>';
          htmlLista +='</div>';              
          $('#listTipoPuesto').html(htmlLista);
          bloquearBoton();
        },     
        error: function (xhr, status, error) { }
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
};

function bloquearBoton()
{
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}

function darBaja(id)
{
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    {
      swal(
      {   
        title: "Esta seguro de eliminar?",
        text: "Presione Ok para eliminar el registro de la base de datos!",
        type: "warning",   showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      }, function()
      {
        var formData = {"identificador": "SISTEMA_VALLE-511","parametros": '{"xtipo_puesto_id":'+id+', "xtipo_puesto_usr_id":'+_usrid+'}'};
        $.ajax(
        { 
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
            'authorization': 'Bearer '+token,
          },
          success: function( data )
          {
            listar(1);
            swal("Tipo Puesto!", "Fue eliminado correctamente!", "success");
          },
          error: function( result ) 
          {
            swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
          }
        });
      });
    },
    error: function(result) 
    {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

function Limpiar()
{
  $( "#ccategoria" ).val("");  
  $( "#cdescripcion" ).val("");
  $('#form_resitem_create').data('bootstrapValidator').resetForm();
}

$( "#registrar" ).click(function() 
{ 
  $.ajax(
  {
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) 
    {
      var ccategoria = $("#ccategoria").val();
      var cdescripcion = $("#cdescripcion").val();
      var data2='{"categoria":"'+ccategoria+'","descripcion":"'+cdescripcion+'"}';
      var jdata2 = JSON.stringify(data2);
      var formData = {"identificador":'SISTEMA_VALLE-508',"parametros": '{"xtipo_puesto_data":'+jdata2+', "xtipo_puesto_usr_id":'+_usrid+'}'};
      $.ajax(
      {
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: 
        {
          'authorization': 'Bearer '+ token ,
        },
        success: function(dataIN)
        { 
          listar(1);
          swal( "Exito..!", "Se Registro correctamente el Tipo Puesto...!", "success" );
          $('#form_resitem_create').data('bootstrapValidator').resetForm();
          $( "#ccategoria" ).val("");  
          $( "#cdescripcion" ).val("");
        },
        error: function(result)
        {
          swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
        },
      });
    },
        error: function(result) 
        {
          swal( "Error..!", "No se puedo guardar los datos", "error" );
        },
  });  
});

function editar(id, categoria, descripcion)
{
  $( "#atipoPuesto_id" ).val(id);
  $( "#acategoria" ).val(categoria);
  $( "#adescripcion" ).val(descripcion); 
};

$( "#actualizar" ).click(function() 
{ 
  $.ajax(
  {
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) 
    { 
      var atipoPuesto_id=$("#atipoPuesto_id").val();
      var acategoria=$("#acategoria").val();
      var adescripcion= $( "#adescripcion" ).val();
      var data2='{"categoria":"'+acategoria+'","descripcion":"'+adescripcion+'"}';
      var jdata2 = JSON.stringify(data2);
      var formData = {"identificador": 'SISTEMA_VALLE-510',"parametros":'{"xtipo_puesto_id":"'+atipoPuesto_id+'", "xtipo_puesto_data":'+jdata2+',"xtipo_puesto_usr_id":"'+_usrid+'"}'};
      $.ajax(
      {
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers:
        {
         'authorization': 'Bearer '+ token,
        },
        success: function(data)
        {
          listar(1);
          swal( "Exito..!", "Se Actualizo correctamente el Tipo Puesto..!", "success" );
          $('#form_resitem_update').data('bootstrapValidator').resetForm();
        },
        error: function(result) 
        {
          swal( "Alerta..!", "Verifique que los campos esten llenados Correctamente Gracias...!", "warning" );
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
});

$(document).ready(function ()
{
  listar(1);
  document.getElementById("listTipoPuesto").length=1;
$('#form_resitem_create').bootstrapValidator(
  {
    message: 'Este valor no es valido....',
    feedbackIcons: 
    {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: 
    {
      ccategoria:
      {
        row:'.form-group',
        validators:
        {
          notEmpty:
          {
            message:"Ingrese nombre de la categoria"
          },
          regexp: 
          {
            regexp: /^[A-Za-z 0-9_-]+$/,
            message: 'No se aceptan caracteres especiales'
          },
        }
      },
      cdescripcion:
      {
        row:'.form-group',
        validators:
        {
          notEmpty:
          {
            message:"Ingrese descripcion"
          },
          regexp: 
          {
            regexp: /^[A-Za-z 0-9_-]+$/,
            message: 'No se aceptan caracteres especiales'
          },
        }
      },
    }
  }).on('error.field.bv', function(e, data) 
    {
      if (data.bv.getSubmitButton()) 
        {
          data.bv.disableSubmitButtons(false);
        }
    }).on('success.field.fv', function(e, data) 
      {
        if (data.bv.getSubmitButton()) 
          {
            data.bv.disableSubmitButtons(false);
          }
      });
  $('#form_resitem_update').bootstrapValidator(
  {
    message: 'Este valor no es valido....',
    feedbackIcons: 
    {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: 
    {
      acategoria: 
      {
        row: '.form-group',
         validators:
        {
          notEmpty:
          {
            message:"Ingrese nombre de la categoria"
          },
          regexp: 
          {
            regexp: /^[A-Za-z 0-9_-]+$/,
            message: 'No se aceptan caracteres especiales'
          },
        }
      },
      adescripcion:
      {
        row:'.form-group',
        validators:
        {
          notEmpty:
          {
            message:"Ingrese descripcion"
          },
          regexp: 
          {
            regexp: /^[A-Za-z 0-9_-]+$/,
            message: 'No se aceptan caracteres especiales'
          },
        }
      },      
    }    
  }).on('error.field.bv', function(e, data) 
    {
      if (data.bv.getSubmitButton()) 
        {
          data.bv.disableSubmitButtons(false);
        }
    }).on('success.field.fv', function(e, data) 
      {
        if (data.bv.getSubmitButton()) 
        {
          data.bv.disableSubmitButtons(false);
        }
      });
}); 
</script>
@endpush
