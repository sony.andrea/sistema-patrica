@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.mercaderia.partial.modalCreate')
@include('backend.parametricas.mercaderia.partial.modalUpdate')
<div class= "row">
   <div class= "col-md-12">
      <section class= "content-header">
       <div class= "header_title">
         <h3>      
            Listado  Mercaderias

         </h3>
       </div>
      </section>
   </div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<br>
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-header with-border">
      </div>
      <div class= "box-body">
        <div id= "listMercaderia">
        </div>  
      </div>
@endsection
@push('scripts')
<script>
var _usrid = {{$usuarioid}};
var total = 0;
var opcion = '';

///---LISTAR MERCADERIA---///
function listar(paging_pagenumber){
  htmlListamrc = '';
  htmlListamrc += '<div class= "row">';
  htmlListamrc += '<div class= "col-md-12" >' ;
  htmlListamrc += '<table id= "lts-mercaderia" class= "table table-striped table-hover" cellspacing= "0" width= "100%">';
  htmlListamrc += '<thead><tr><th align= "center">Nro.</th><th align= "center">Opciones</th><th align= "left">Descripción</th><th align= "left">Clase</th><th align= "left">Tipo</th></thead>';
  var opcion = $( "#opcion" ).val();
  var paging_pagesize = $( "#reg" ).val();
  var formData = {"identificador": 'SERVICIO_SIERRA-6' ,"parametros": '{"xtipolista": "'+opcion+'","paging_pagesize": '+paging_pagesize+',"paging_pagenumber": '+paging_pagenumber+'}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer' + token,
        },
        success: function(dataIN) {
          var datos = dataIN;
          for (var i = 0; i < datos.length; i++) {
            var datosM = JSON.parse(datos[i].data);
            var mercaderiaData = datosM.mercaderia_data;
            htmlListamrc += '<tr><td align= "left">'+(i+1)+'</td>';
            var estado_= datosM.mercaderia_estado;
            htmlListamrc += '<td><button class= "btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target= "#myUpdate" data-toggle= "modal" data-placement= "top" title= "Modificar" type= "button" onClick= "editar('+datosM.mercaderia_id+',\''+mercaderiaData.descripcion+'\''+ ',\''+mercaderiaData.clase+'\''+',\''+mercaderiaData.tipo+'\''+');"><i class= "glyphicon glyphicon-pencil"></i></button><button class= "btncirculo btn-xs btn-danger btn-estado-'+estado_+'" style= "background:#FA8072" fa fa-plus-square pull-right" data-toggle= "modal" data-placement= "top" title= "Eliminar" type= "button" onClick= "darBaja('+datosM.mercaderia_id+');"><i class= "glyphicon glyphicon-trash"></i></button></td>';
            htmlListamrc+= '<td align= "left">'+mercaderiaData.descripcion+'</td>';
            if(mercaderiaData.clase== 'CE'){
              htmlListamrc+='</td>'+'<td align= "left">'+'Mercaderias Aceptables'+'</td>';
            }
            else {
              htmlListamrc+='</td>'+'<td align= "left">'+'Mercaderias Inaceptables'+'</td>';
            }
            if(mercaderiaData.tipo== 'T'){
              htmlListamrc+='</td>'+'<td align= "left">'+'Carga general'+'</td>';
            } 
            if(mercaderiaData.tipo== 'X'){
              htmlListamrc+='</td>'+'<td align= "left">'+'Alimentos perecederos'+'</td>';
            } 
            if( mercaderiaData.tipo == 'A'){
              htmlListamrc+='</td>'+'<td align= "left">'+'Mercancias peligrosas'+'</td>';
            } 
            if(mercaderiaData.tipo == 'C'){
              htmlListamrc+= '</td>'+'<td align= "left">'+'Mercancias valoradas'+'</td>';
            } 
            if(mercaderiaData.tipo== 'N'){
              htmlListamrc+= '</td>'+'<td align= "left">'+'Aranceles'+'</td>';
            } 
            htmlListamrc+= '</tr>';
          };
          htmlListamrc += '</table></div></div>';
          htmlListamrc += '<div>';
          htmlListamrc += '<ul class= "pager">';
          htmlListamrc += '<li><a href= "#" onClick= "btnAnterior()">Anterior</a></li>';
          htmlListamrc += '<li><a href= "#" onClick= "btnSiguiente()">Siguiente</a></li>';
          htmlListamrc += '</ul>';
          htmlListamrc += '</div>';
          $('#listMercaderia').html(htmlListamrc);
          bloquearBoton();
        },
        error: function (xhr, status, error) {
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
};

function bloquearBoton(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}

///---FUNCION ELIMINAR---///
function darBaja(id){
  var usuario = _usrid;
  swal({   
    title: "Esta seguro de eliminar la Mercaderia?",
    text: "Presione ok para eliminar el registro de la base de datos!",
    type: "warning",   showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Eliminar!",
    closeOnConfirm: false
  }, function(){
    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_SIERRA-11',"parametros": '{"xmercaderia_id":'+id+', "xmercaderia_usr_id":'+usuario+'}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
             'authorization': 'Bearer '+ token,
          },
          success: function(data){
            swal("Mercaderia!", "Fue eliminado correctamente!", "success");
          },
          error: function(result) {
            swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
          }
        });
      },
    }); 
    listar(1); 
  });
};

///---FUNCION REGISTRAR---///
$("#registrar").click(function() {
  var descripcion = $("#descripcion" ).val();
  var clase = $("#clase" ).val();
  var tipo = $("#tipo" ).val();
  var dataMercaderia = '{"descripcion":"'+descripcion+'","clase":"'+clase+'","tipo":"'+tipo+'"}';
  dataMercaderia = JSON.stringify(dataMercaderia);
  var usuario = _usrid;
  var formData = {"identificador": 'SERVICIO_SIERRA-7' ,"parametros"  : '{"xmercaderia_data":'+dataMercaderia+', "xmercaderia_usr_id":'+ usuario+'}'};  
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {    
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
         'authorization': 'Bearer ' + token,
        },
        success: function(dataIN) {
          swal( "Exito..!", "Se guardo correctamente los datos!", "success" );
          $("#descripcion" ).val('');
          $("#clase" ).val("");
          $("#tipo" ).val("");
          document.ready = document.getElementById("opcion").value = 'activo';
          listar(1);
        },
        error: function(result) {
          swal( "Error..!", "No se puedo guardar los datos", "error" );
        }
      });
    },   
  });
});

///---FUNCION EDITAR---///
function editar(id,descripicion,tipo,clase){
   $( "#mercaderia_id1" ).val(id);
   $( "#descripcione" ).val(descripicion);
   $( "#clasee" ).val(tipo);
   $( "#tipoe" ).val(clase);
 };

///---FUNCION ACTUALIZAR---/// 
$( "#actualizar" ).click(function() { 
  var idmercarderia = $( "#mercaderia_id1" ).val();
  var descripcione = $("#descripcione" ).val();
  var clasee = $("#clasee" ).val();
  var tipoe = $("#tipoe" ).val();
  var usuario = 1;
  var dataMercaderia = '{"descripcion":"'+descripcione+'","clase":"'+clasee+'","tipo":"'+tipoe+'"}';
  dataMercaderia = JSON.stringify(dataMercaderia);
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {
      var formData = {"identificador": 'SERVICIO_SIERRA-9' ,"parametros"  : '{"xmercaderia_id":' + idmercarderia+ ' ,"xmercaderia_data":'+dataMercaderia+', "xmercaderia_usr_id":' + usuario + ' }'};   
      $.ajax({
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: {
         'authorization': 'Bearer ' + token,
       },
       success: function(dataIN) {
        swal( "Exito..!", "Se modifico correctamente los datos!", "success" );               
      },
      error: function(result) {
        swal( "Error..!", "No se puedo mofigicar los datos", "error" );
      }
    });
      listar(1);
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });
});

///---UNCION LIMPIAR---///
function Limpiar(){
  $("#descripcion" ).val('');
  $("#clase" ).val('');
  $( "#tipo").val('');
};

$(document).ready(function (){
   listar(1);
      $('#mercaderiaform').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        descripcion: {
          row: '.form-group',
          validators: {
            notEmpty: {
              message: 'Ingrese un código válido '
            },
            regexp: {
             regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
             message: 'No se aceptan caracteres especiales'
           },
           stringLength: {
            min: 2,
            max: 40,
            message: 'Se requiere mas de 2 caracteres y un limite de 40'
          }
        }
      },
      clase: {
        row: '.form-group',
        validators: {
          notEmpty: {
            message: 'Ingrese un clasificador válido'
          },
          regexp: {
            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
            message: 'No se aceptan caracteres especiales'
          },
          stringLength: {
            min: 1,
            max: 15,
            message: 'Se requiere mas de 1 caracter y un limite de 15'
          }
        }
      },
       tipo: {
        row: '.form-group',
        validators: {
          notEmpty: {
            message: 'Ingrese un clasificador válido'
          },
          regexp: {
            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
            message: 'No se aceptan caracteres especiales'
          },
          stringLength: {
            min: 1,
            max: 15,
            message: 'Se requiere mas de 1 caracter y un limite de 15'
          }
        }
      }

    }
  }).on('error.field.bv', function(e, data) {
    if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
  }).on('success.field.fv', function(e, data) {
    if (data.bv.getSubmitButton()) {data.bv.disableSubmitButtons(false);}
  });
  $('#dataForm').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      ide: {
        message: 'La descripción no es valida',
        validators: {
          notEmpty: {
            message: 'Ingrese una descripción válida'
          },
          regexp: {
            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/\s/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
            message: 'No se aceptan caracteres especiales'
          },
          stringLength: {
            min: 3,
            max: 40,
            message: 'Se requiere mas de 3 caracteres y un limite de 40'
          }
        }
      },
      mercaderiae: {
        row: '.form-group',
        validators: {
          notEmpty: {
            message: 'Ingrese un código válido '
          },
          regexp: {
           regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
           message: 'No se aceptan caracteres especiales'
         },
         stringLength: {
          min: 2,
          max: 20,
          message: 'Se requiere mas de 2 caracteres y un limite de 20'
        }
      }
    },
    clasee: {
      row: '.form-group',
      validators: {
        notEmpty: {
          message: 'Ingrese un clasificador válido'
        },
        regexp: {
          regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
          message: 'No se aceptan caracteres especiales'
        },
        stringLength: {
          min: 1,
          max: 15,
          message: 'Se requiere mas de 1 caracter y un limite de 15'
        }
      }
    },
     tipoe: {
      row: '.form-group',
      validators: {
        notEmpty: {
          message: 'Ingrese un clasificador válido'
        },
        regexp: {
          regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
          message: 'No se aceptan caracteres especiales'
        },
        stringLength: {
          min: 1,
          max: 15,
          message: 'Se requiere mas de 1 caracter y un limite de 15'
        }
      }
    }
  }
});
});
</script>
@endpush

