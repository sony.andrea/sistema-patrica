  
<div class= "modal fade modal-default" data-backdrop= "static" data-keyboard= "false" id= "myUpdate" tabindex= "-5">
  <div class= "modal-dialog" role= "document">
    <div class= "modal-content">
      <div class= "modal-body">
        <div class= "row">
          <div class= "col-xs-12 container-fluit">
            <div class= "panel panel-info">
              <div class= "panel-heading">
                <h4>
                 Editar Datos de la Mercaderia
                 <button type= "button" class= "close" data-dismiss= "modal" aria-label= "Close">
                  <span aria-hidden= "true">&times;</span>
                </button>
              </h4>
            </div>
              <div class= "panel-body">
                <div class= "caption">
                <hr>
                {!! Form::open(['id' => 'dataForm'])!!}
                <input id= "token" name= "csrf-token" type= "hidden" value= "{{ csrf_token() }}">
                <input id= "mercaderia_id1" name= "mercaderia_id1"  type= "hidden" value= "">
                <div class= "row">
                  <div class= "col-md-12">
                    <div class= "form-group">
                      <div class= "col-sm-12">
                        <label>
                          Descripcion:
                        </label>
                        {!! Form::text('descripcione', null, array('placeholder' => 'Ingrese scripcion ','maxlength'=>'20','class' => 'form-control','id'=>'descripcione')) !!}
                      </div>
                    </div>
                  </div>
                </div>
                <div class= "row">
                  <div class= "col-md-6">
                    <div class= "form-group">
                      <div class= "col-sm-12">
                        <label>
                          Clase:
                        </label>
                        {!! Form::text('clase', null, array('placeholder' => 'ingrese clase','maxlength'=>'2','class' => 'form-control','id'=>'clasee')) !!}
                      </div>
                    </div>
                  </div>
                  <div class= "col-md-6">
                    <div class= "form-group">
                      <div class= "col-sm-12">
                        <label>
                         Tipo:
                       </label>
                       {!! Form::text('tipo', null, array('placeholder' => 'ingrese tipo','maxlength'=>'1','class' => 'form-control','id'=>'tipoe')) !!}
                     </div>
                   </div>
                 </div>
               </div>  
             </input>
           </input>
          </hr>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
       {!! Form::close() !!}
       <div class= "modal-footer">
        <button class= "btn btn-primary" id= "actualizar" data-dismiss= "modal" type= "button">
          <i class= "glyphicon glyphicon-pencil"></i> Modificar
        </button>
      </div> 
    </div>
  </div>
</div>

