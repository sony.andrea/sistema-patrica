<div class= "modal fade modal-default" data-backdrop= "static" data-keyboard= "false" id= "myCreate" tabindex= "-5">
  <div class= "modal-dialog" role= "document">
    <div class= "modal-content">
      <div class= "modal-body">
        <div class= "row">
          <div class= "col-xs-12 container-fluit">
            <div class= "panel panel-info">
              <div class= "panel-heading">
                <h4 align="center">Registrar Boleto
                  <button type= "button" class= "close" data-dismiss= "modal" aria-label= "Close">
                  <span aria-hidden= "true">&times;</span>
                  </button>
                </h4>
              </div>
              <div class= "panel-body">
                <div class= "caption">
                <hr>
                {!! Form::open(['id' => 'mercaderiaform'])!!}
                <input id= "token" name= "csrf-token" type= "hidden" value= "{{ csrf_token() }}">
                <input id= "id" name= "asoc_id" type= "hidden" value= "">
                  <div class= "row">
                    <div class= "col-md-12">
                      <div class= "form-group">
                        <div class= "col-sm-6">
                          <label>Nit:</label>
                          {!! Form::text('idnit', null, array('placeholder' => 'Ingrese nit ','maxlength'=>'20','class' => 'form-control','id'=>'nit','readonly')) !!}
                        </div>
                        <div class= "col-sm-6">
                        <label>Razon Social:</label>
                        {!! Form::text('idRazon', null, array('placeholder' => 'Ingrese Razon ','maxlength'=>'20','class' => 'form-control','id'=>'razon','readonly')) !!}
                        </div>
                      </div>
                    </div>
                  </div><br><br>
                  <div class= "row">
                    <div class= "col-md-12">
                      <div class= "form-group">
                        <div class= "col-sm-4">
                        <label>Total Niños:</label>
                        {!! Form::number('idniño', null, array('placeholder' => 'Ingrese CI ','maxlength'=>'20','class' => 'form-control','id'=>'niño','readonly')) !!}
                        </div>
                        <div class= "col-sm-4">
                        <label>Total Adultos:</label>
                        {!! Form::number('idadulto', null, array('placeholder' => 'Ingrese Edad ','maxlength'=>'20','class' => 'form-control','id'=>'adulto','readonly')) !!}
                        </div>
                        <div class= "col-sm-4">
                        <label>Total Tercera Edad:</label>
                        {!! Form::number('idterceedad', null, array('placeholder' => 'Ingrese terceedad','maxlength'=>'20','class' => 'form-control','id'=>'terceedad','readonly')) !!}
                        </div>
                      </div>
                    </div>
                  </div><br><br>
                  <div class= "row">
                    <div class= "col-md-12">
                      <div class= "form-group">
                        <div class= "col-sm-4">
                        <label>Monto Total:</label>
                        {!! Form::number('idcosto', null, array('placeholder' => 'Ingrese CI ','maxlength'=>'20','class' => 'form-control','id'=>'costo','readonly')) !!}
                        </div>
                        <div class= "col-sm-4">
                        <label>Efectivo:</label>
                        {!! Form::number('idefectivoo', null, array('placeholder' => 'Ingrese Efectivo ','maxlength'=>'20','class' => 'form-control','id'=>'efectivo')) !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </input>
                </input>
                </hr>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
    <div class= "modal-footer">
    <button class= "btn btn-default reset-inputs "  type= "button" onClick= "Limpiar2()">
    <i class= "fa fa-eraser"></i> Limpiar
    </button>
    <button class= "btn btn-primary" id= "Registrar" data-dismiss= "modal" type= "button" >
    <i class= "fa fa-save"></i> Guardar
    </button>    
  </div>
</div>
</div>
</div>

