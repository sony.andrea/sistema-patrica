      @extends('backend.template.app')
    @section('main-content')
    <style type="text/css">
      #listMonto li{
        text-decoration: none;
        list-style:none;
      }
    </style>
        <div id="formulario1" style="display: none;">
<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>RECAUDACIONES DE ZOOLOGICO</b></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">Operador:</div>
          <div class="col-md-3">
            <input type="text" id="nombresUsuario" class="form-control" name="nombresUsuario" readonly>
          </div>
          <div class="col-md-2">Nro. de Identificación:</div>
          <div class="col-md-3">
            <input type="text" id="identificacion" class="form-control" name="identificacion" readonly>
          </div>
        </div>
      </div>
      <br>
     
      <br>
    </div>
  </div>
   <br>
  
</section>
</div>


<div id="formulario" style="display: none;">
<form role="form">
   <div class= "panel panel-info">
              <div class= "panel-heading">
                <h4 >
                    Datos del Ciudadano
                    <button type= "button" class= "close" data-dismiss= "modal" aria-label= "Close">
                      <span aria-hidden= "true">&times;</span>
                    </button>
                </h4>
              </div>
              <div class= "panel-body">
                <div class= "caption">
                <hr>
                {!! Form::open(['id' => 'mercaderiaform2'])!!}
                <input id= "token" name= "csrf-token" type= "hidden" value= "{{ csrf_token() }}">
                    <input id= "id" name= "asoc_id" type= "hidden" value= "">
                            <div class= "row">
                            <div class= "col-md-12">
                                <div class= "form-group">
                                    <div class= "col-sm-4">
                                        <label>
                                            CI:
                                        </label>
                                         {!! Form::number('idci', null, array('placeholder' => 'Ingrese CI ','maxlength'=>'20','class' => 'form-control','id'=>'ci2')) !!}
                                    </div>
                                     <div class= "col-sm-4">
                                        <label>
                                            Nacionalidad:
                                        </label>
                                          <span class= "block input-icon input-icon-right">
                                            <select class= " form-control" placeholder = "Seleccionar Clase de Nacionalidad" id = "nacionalidad2" name= "clase">
                                              <option value= "Boliviano">Boliviano </option>
                                              <option value= "Extranjero">Extranjero</option>
                                               
                                            </select>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class= "row">
                            <div class= "col-md-12">
                                <div class= "form-group">
                                    <div class= "col-sm-4">
                                        <label>
                                            Nombre Completo:
                                        </label>
                                         {!! Form::text('idnombre', null, array('placeholder' => 'Ingrese Nombre ','maxlength'=>'20','class' => 'form-control','id'=>'nombre2')) !!}
                                    </div>
                                     <div class= "col-sm-4">
                                        <label>
                                            Apellido Paterno:
                                        </label>
                                         {!! Form::text('idapellidop', null, array('placeholder' => 'Ingrese Apellido ','maxlength'=>'20','class' => 'form-control','id'=>'apellidop2')) !!}
                                    </div>
                                      <div class= "col-sm-4">
                                        <label>
                                            Apellido Materno:
                                        </label>
                                         {!! Form::text('idapellidom', null, array('placeholder' => 'Ingrese Apellido Materno ','maxlength'=>'20','class' => 'form-control','id'=>'apellidom2')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                 
                       
                    </input>
                </input>
                </hr>
                </div>
              </div>
              
            </div>
   

  
</form>
</div>
     <div id="boletover" style="display: none;">
    <div class="row">
      <div class="col-md-12">
        <section class="content-header">
         <h4 align="center"> <b>Servicios</b></h4>
       <div class="panel panel-info">
        <br>
        <div class="row">
          <div class="col-md-12">
          <div class="col-md-4">
           <h5><b> Lista de idItemR</b></h5> 
           </div>
            <div class="col-md-4">
              <select name="idItemR" id="idItemR" class="form-control" onChange='obtenerDatosidItemR()'>
               <option>Selecciona idItemR...</option>
             </select>
             <input type="hidden" name="descripcionItem" id="descripcionItem">
           </div>
           <div class="col-md-4 form-group">
            <span class="control-label col-md-1">Bs.</span> 
            <div class="col-md-3">
              <input type="number" name="monto" class="form-control" id="monto" disabled value="0">
            </div>
            <span class="control-label col-md-2">Cantidad:</span> 
            <div class="col-md-4">
              <input type="number" name="cantidad" class="form-control" id="cantidad" value="1">
              <button class="btn btn-primary fa fa-plus-square pull-left" id="exe">&nbsp;Adicionar</button>
            </div>
          </div>
        </div>
      </div>
       <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-4">
             <b>DESCRIPCION DEL ITEM</b>
          </div>
          <div class="col-md-2" id="listMonto">
             <b>COSTO POR UNIDAD</b>
          </div>
          <div class="col-md-1">
            <b>CANTIDAD</b>
          </div>
          <div class="col-md-1">
            <b>TOTAL</b>
          </div>
          <div class="col-md-3"></div>
          </div>
    <div class="row">
          <div class="col-md-5">
          <ol id="idItemR-ol"></ol><br>
          </div>
          <div class="col-md-2" id="listMonto">
          <ul id="monto-ul" ></ul>
          </div>
          <div class="col-md-1" id="listMonto">
            <ul id="cantidad-ul" ></ul>
          </div>
          <div class="col-md-3" id="listMonto">
            <ul id="montoTotal-ul" ></ul>
          </div>
         <div class="col-md-1"></div>
      </div>
    <br>  <br> <br>
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal">     
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-md-6">Total a Pagar:</label>
              <div class="col-md-6">
                <input type="text" id="total" name="total" class="form-control" placeholder="0" align="center" readonly>
              </div>
            </div>
          </div>
        </form>
        <form class="form-horizontal">
          <div class="col-md-6">               
            <div class="form-natural">
              <div class="form-group">
                <label class="control-label col-md-2">Bs.</label>
                <div class="col-md-10">
                  <button type="button" id="liquidardeuda" class="btn btn-primary"  onclick="liquidar(),guardar()" data-toggle="modal">Liquidar</button>
                  
                </div>
              </div>
            </div>
          </div>
        </form>
      </div> 
          <div class= "row">
                            <div class= "col-md-12">
                                <div class= "form-group">
                                    <div class= "col-sm-4">
                                        <label>
                                            Nombre Completo:
                                        </label>
                                         {!! Form::text('idnombre', null, array('placeholder' => 'Ingrese Nombre ','maxlength'=>'20','class' => 'form-control','id'=>'nombre2')) !!}
                                    </div>
                                     <div class= "col-sm-4">
                                        <label>
                                            Apellido Paterno:
                                        </label>
                                         {!! Form::text('idapellidop', null, array('placeholder' => 'Ingrese Apellido ','maxlength'=>'20','class' => 'form-control','id'=>'apellidop2')) !!}
                                    </div>
                                      <div class= "col-sm-4">
                                        <label>
                                            Apellido Materno:
                                        </label>
                                         {!! Form::text('idapellidom', null, array('placeholder' => 'Ingrese Apellido Materno ','maxlength'=>'20','class' => 'form-control','id'=>'apellidom2')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                 
    </div>
    
    </div> 
     </section>
    </div>
    </div>
<div id="siguiente1">
    <div class="form-group col-md-11">
      <a class="btn btn-primary  pull-right" id="irUbicacion" onclick="enviarDatos()">Siguiente</a>
    </div>
  </div>
     <div id="factura">
    <div class="form-group col-md-3">
      <a class="btn btn-primary  pull-right" id="irUbicacion" onclick="obtenerFactura()">factura</a>
       <a class="btn btn-primary  pull-right" id="irUbicacion" onclick="esconde()">dasasas</a>
    </div>
  </div>

  </div>


<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title">
        <h3 align="center" >      
      Listado  Grupo Zoologico
           
         </h3>
       </div>
      </section>
   </div>
</div>
<div class="row">
  <div class="col-md-6">
  </div>
  <div class="col-md-3">
   <label >Mostrar Registros:</label> 
   <select class="form-control" id="reg" name="reg" onclick="listarSelect()">
    <option value=10>10</option>
    <option value=15>15</option>
    <option value=20>20</option>
    <option value=30>30</option>
   </select>
  </div>
<div class="col-md-3">
  <label >Listar por:</label> 
  <select class="form-control" id="opcion" name="opcion" onclick="listarSelect()">
    <option value="activo" selected="selected" > ACTIVOS</option>
    <option value="inactivo">INACTIVOS</option>
    <option value="todo">TODO</option>
  </select>
  </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
            </div>
            <div class="box-body">
    <div id="no-more-tables">
    <div id="listboleto">
    </div>
</div>



    @endsection
    @push('scripts')
    <script>
        var _usrid = {{$usuarioid}};
        var datosItem='';
        var datosItem2='';
        var bloqueItem='';
        var bloqueItem2='';
        var unidadrecaudadora=0; 
        var nombreApellido='';
        var ciContribuyente='';
        var cantidad=1;
        var fum=0;
        var count = 0;
        var tipoIdenContri = "CI";
        var nombreContri = "";
        var direccionContri = "";
        var razonSocial = "";
        var cedulaIdentidad = "";
        var liItem="";
        var liMonto="";
        var liCantidad="";
        var liMontoTotal="";
        var totalMonto=0;
        var fechaHoy = '2018-06-14';


function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SISTEMA_VALLE-356","parametros": '{"yusrid":"'+_usrid+'"}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
     console.log(dataIN);
     var primerNombre = dataIN[0].vprs_nombres;
     var primerApellido = dataIN[0].vprs_paterno;
     var segundoApellido = dataIN[0].vprs_materno;
     cedulaIdentidad = dataIN[0].vprs_ci;
     razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
     $("#nombresUsuario").val(primerNombre+' '+primerApellido+' '+segundoApellido);
     $("#identificacion").val(cedulaIdentidad);
     datosUsuario = '{"razonSocial":"'+razonSocial+'","numeroIdentificacion":"'+cedulaIdentidad+'","tipoIdentificacion":"CI."}';
     datosUsuario = JSON.stringify(datosUsuario);
    },     
    error: function (xhr, status, error) { }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});

};



esconde
function esconde(){
document.getElementById("formulario1").style="display:block;";
}
function obtenerDatosidItemR(){
var datosItem = document.getElementById("idItemR").value;
console.log(datosItem,777);
var cadena = datosItem.split(",");
console.log(cadena);
var a = cadena[2];
console.log(a,8888);
$('#monto').val(a);
}

 function obtenerFactura(){   
 var ciudadano = $("#nombre2" ).val();
  var apellidop = $("#apellidop2" ).val();
  var apellidom = $("#apellidom2" ).val();
  var ciciudadano = $("#ci2" ).val();
  var nacionalidadciudadano = $("#nacionalidad2" ).val();

    var bloque = bloqueItem ;
  var bloque2 = bloque.substr(0,bloque.length-1);
  bloque2= bloque2+ ']';
  console.log(bloque2.length);
  bloque2=JSON.parse(bloque2);
  var det1='[';
   console.log(bloque2.length,bloque2);

     for(var i=0; i< bloque2.length; i++){
                
            console.log(bloque2[i].Monto);

            det1 =det1 + '{ "concepto": "'+bloque2[i].descripcionItem+'", "cantidad": 1, "monto": '+bloque2[i].Monto+' },'; 
                
              } 
det1 = det1.substr(0,det1.length-1);
  det1= det1+ ']';

                console.log(det1);
               //.....Descripcion de  factura
                  var descripcionFac = 'Zoologico';
                  var cantidadFac =4;
                  var montoFac = 4;
                  //.....Datos   factura
                  var nombreFac = ciudadano;
                  var documentoFac =ciciudadano;
                  var montototalFac = 4;
                 //fechahoy=new Date();                
                  $.ajax({
                    type        : 'GET',
                    url         :  'prueba/VALLE/public/v0.0/getTokenFac',
                    data        : '',
                    success: function(token) { 
                    //"idItemR":"1","descripcionItem":"Niño","Monto":5 3333322                     
                    var det = det1; 
                    var formData = '{ "factura": { "ci_nit": '+documentoFac+', "nombres": "'+nombreFac+'", "fecha": "'+fechaHoy+'", "idUsuario": 49, "idSucursal": 124, "efectivo": 100, "detalles": '+det+', "serialTarjeta": "", "fum":0 } }';
                      $.ajax({
                       type        : 'POST',            
                       url         : urlFactura,
                       data        : formData,
                       dataType    : 'json',
                       crossDomain : true,
                       headers: {
                         'authorization': 'Bearer '+token,
                         'Content-Type': 'application/json',
                       },success: function(dataIN) {                              
                              var urlFactura =  dataIN.data.url;  
                              window.open(urlFactura, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=1000");                            
                       },          
                       error: function (xhr, status, error) { }
                     });
                    },
                    error: function(result) {
                      swal( "Error..!", "No se puedo guardar los datos", "error" );
                    },
                  });
  
  };
      function obtenerItemRecaudadorPorUnidadRecaudadora()
      {
        var unidad_recaudadora = 'ZOOLOGICO';
        $.ajax({
          type        : 'GET',
          url         :  urlToken,
          data        : '',
          success: function(token) {
            var formData = {"identificador": 'SERVICIO_VALLE-308',"parametros": '{"modulo":"'+unidad_recaudadora+'"}'};
            $.ajax({
             type        : 'POST',            
             url         : urlRegla,
             data        : formData,
             dataType    : 'json',
             crossDomain : true,
             headers: {
               'authorization': 'Bearer'+ token,
             },
             success: function(dataIN) {
               if (dataIN!="[{ }]") {
                var tam=dataIN.length;
              }  
              for(var i=0; i< tam; i++){

                var dataItem = JSON.parse(dataIN[0].sp_obtener_item_modulo);
                var tam2 = dataItem.length;
                for(var j=0; j< tam2; j++){
                  console.log(dataItem[j]);
                  var item = dataItem[j].costo;
                  var itemDesc = dataItem[j].descripcionItem;
                  console.log(item,55555);
                  var id = dataItem[j].idItem;
                  var todocampo= id +','+itemDesc+','+item;
                  console.log(todocampo,111);
                  document.getElementById("idItemR").innerHTML += '<option value="'+todocampo+'">'+itemDesc+'</option>';
                }
              }             
            },     
            error: function (xhr, status, error) {console.log(error); }
          });
          },
          error: function(result) {
            swal( "Error..!", "No se puedo guardar los datos", "error" );
          },
        }); 
      }

    $(function(){ 
       bloqueItem = '[';
       bloqueItem2 = '[';
      $('#exe').click(function() {
        count++;
        var monto=$('#monto').val();
        var cantidad=$('#cantidad').val();
          var combo = document.getElementById("idItemR");
          var selectedItem = combo.options[combo.selectedIndex].text;
        
          var idItemR=$('#idItemR').val();
            console.log(idItemR,4444);
            var cadena = idItemR.split(",");
            console.log(cadena);
            var a = cadena[0];
            idItemR=a;

          var descripcionItem=$('#idItemR').val();
          console.log(idItemR,4444);
            var cadena = descripcionItem.split(",");
            console.log(cadena);
            var a = cadena[1];
            descripcionItem=a;

          var cantidad=$('#cantidad').val();
          var monto=$('#monto').val();
          var Monto=monto*cantidad;
          monto=parseInt(monto);
           datosItem2={idItemR,descripcionItem,Monto,cantidad};
          datosItem={idItemR,descripcionItem,Monto};
          datosItem=JSON.stringify(datosItem);  
          datosItem2=JSON.stringify(datosItem2);   
          totalMonto=totalMonto + Monto;

          bloqueItem=(bloqueItem.concat(datosItem)).concat(',');
          bloqueItem2=(bloqueItem.concat(datosItem2)).concat(',');

          $('#total').val(totalMonto);
           liItem=$('<li>',{text:selectedItem},'<br>');
           liMonto=$('<li>',{text:"Bs. "+monto});
           liCantidad=$('<li>',{text:cantidad});
           liMontoTotal=$('<li>',{text:"Bs. "+Monto});
          $('#idItemR-ol').append(liItem);          
          $('#monto-ul').append(liMonto);
          $('#cantidad-ul').append(liCantidad);          
          $('#montoTotal-ul').append(liMontoTotal); 

     });

    });

$(document).ready(function(){
    obtenerItemRecaudadorPorUnidadRecaudadora();

    document.getElementById("boletover").style="none";
    document.getElementById("formulario").style="display:block;";
     
    // <div id="formulario1" style="display: none;">


});
var idusu = {{$usuarioid}};
function liquidar(){
  var bloque = bloqueItem ;
  var bloque2 = bloque.substr(0,bloque.length-1);
  bloque2= bloque2+ ']';
  //console.log(bloqueItem,bloque2);
  //------------------ DATOS A ENVIAR Y MODICAR
  var idDeudas = '0';
  var fecha = new Date();
  var gestiones = fecha.getFullYear();
  var contribuyente = '{"razonSocial":"'+razonSocial +'","numeroIdentificacion":"'+cedulaIdentidad+'","tipoIdentificacion":"'+tipoIdenContri+'"}';
  var dataGlosa = '{"cabecera":"ZOOLOGICO","detalles":{"detalleA":"datos de puesto","detalleB":"puesto"},"pie":"","observaciones":""}';
  var transacciones = bloque2;
  
//----------------------------
  var tipo = 'ZOOLOGICO';
  dataGlosa = JSON.stringify(dataGlosa);  
  contribuyente = JSON.stringify(contribuyente);
  generarFum1(idDeudas,gestiones,contribuyente,dataGlosa,transacciones,tipo,idusu);
}

function guardar(){
 var ciudadano = $("#nombre2" ).val();
  var apellidop = $("#apellidop2" ).val();
  var apellidom = $("#apellidom2" ).val();
  var ciciudadano = $("#ci2" ).val();
  var nacionalidadciudadano = $("#nacionalidad2" ).val();
  var nombre=$('#nombresUsuario').val();
  var ci=$('#identificacion').val();
        // var databoleto = '{"nacionalidad": "'+nacionalidad+'", "monto": '+monto+', "datos_ciudadano": { "nombre": "'+nombre+'", "apellidop": "'+apellidop+'", "apellidom": "'+apellidom+'" ,"ci":'+ci+' , "edad": '+edad+'}}';
     
  //var monto = $("#Monto" ).val();

  //console.log(nombre,apellidop,apellidom,ci,edad,nacionalidad,monto,'registra dos');
  //var databoleto1 = '{"descripcion":"'+descripcion+'","clase":"'+clase+'","tipo":"'+tipo+'"}';

 //var databoleto = '{}';
 var bloque = bloqueItem;
   console.log(bloqueItem,1);
 console.log(bloque4,3333322);

  var bloque2 = bloque.substr(0,bloque.length-2);
  var bloque3 = bloque2;
  var bloque4 = bloque3.substr(2,bloque3.length);
  console.log(bloque4,3333322);

 bloque2 = bloque2;
    console.log(bloque2,222);
bloque4='{'+bloque4+'}';

    bloque2 = JSON.stringify(bloque2);
    //bloque4 = JSON.stringify(bloque4);
    console.log(bloque4);

     var databoleto = '{"nombreoOperedor": "'+nombre+'","ci": '+ci+' ,"montoTotal": '+totalMonto+', "datos_boleto":['+ bloque4+'],"datos_ciudadano": { "nombre": "'+ciudadano+'", "apellidop": "'+apellidop+'", "apellidom": "'+apellidom+'" ,"ci":'+ciciudadano+' ,"nacionalidad":"'+nacionalidadciudadano+'"}}';
     databoleto = JSON.stringify(databoleto);
      console.log(databoleto,11);
  var usuario = _usrid;
  var formData = {"identificador": 'SERVICIO_VALLE-290 ' ,"parametros"  : '{"xboleto_id_ciudadano":'+1+',"xboleto_data":'+databoleto+', "xboleto_usr_id":'+ 1+'}'};  
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {    
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
         'authorization': 'Bearer ' + token,
       },
       success: function(dataIN) {
        swal( "Exito..!", "Se guardo correctamente los datos!", "success" );
       
        },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      }
    });
    },   
  });
  

}
function enviarDatos(){
  var nombre=$('#nombresUsuario').val();
  var ci=$('#identificacion').val();
  var dat1 = {nombre,ci};
  dat1 = JSON.stringify(dat1);
  var urlInh = '{{ url("datosBoleto",[ "datos"=>"dat1"])}}';
  urlInh = urlInh.replace('dat1',dat1);
  $("#irUbicacion").attr("href", urlInh);
 

}

function generarProforma(){
  imprimirFum1(); 
  //imprimirFum1();
  setTimeout(function(){
    var datosDatFum = datoFum;
    //var datosDatFum = JSON.stringify(datosDatFum);
    var urlPdf= "{{ action('reportes\proformaCementerioController@getGenerar',['datos'=>'d1']) }}";  
    urlPdf = urlPdf.replace('d1', datosDatFum);
    window.open(urlPdf);
    //$("#generarPdf").attr("href", urlPdf); 
  }, 500);
}
function listar1(paging_pagenumber){
  htmlListGrup = '';
  htmlListGrup += '<div class= "row">';
  htmlListGrup += '<div class= "col-md-12" >' ;
  htmlListGrup += '<table id= "lts-boleto" class= "table table-striped table-hover" cellspacing= "0" width= "100%">';
  htmlListGrup += '<thead><tr><th align= "center">Nro.</th><th align= "center">Opciones</th><th align= "left">Nombre completo</th><th align= "left">Pueso del encargado </th><th align= "left">Cantidad</th></thead>';
  var opcion = $( "#opcion" ).val();
  var paging_pagesize = $( "#reg" ).val();
  var formData = {"identificador": 'SERVICIO_VALLE-277' ,"parametros": '{"xtipolista": "'+opcion+'","paging_pagesize": '+paging_pagesize+',"paging_pagenumber": '+paging_pagenumber+'}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer' + token,
        },
        success: function(dataIN) {
          console.log(dataIN,44444);
          var datos = dataIN;
          var tam = dataIN.length;
            if (dataIN=="[{ }]") {
                tam=0;
                htmlListGrup+= "no hay datos";
                    console.log("no hay datos")
              }


          for (var i = 0; i < tam; i++) {
            var datosGrupo = JSON.parse(datos[i].data);
            var grupoData = datosGrupo.grupo_data;
            //var datociudano= boletoData.datos_ciudadano;
            //"{"grupo_id":11,"grupo_ciudadano_id":1,"grupo_data":{"hora": "10:00 am", "fecha": "2018-06-28", "gmail": "9", "responsable": "PREDRO", "datos_grupos": {"gia": "richar", "nombre": "richar", "cantidad": 9}, "puesto_responsable": "2"},"grupo_registrado":"2018-06-11T16:14:54.226314","grupo_modificado":"2018-06-11T16:14:54.226314","grupo_usr_id":1,"grupo_estado":"activo"}"
            console.log(grupoData,3333);
          ///
            htmlListGrup += '<tr><td align= "left">'+(i+1)+'</td>';
            var estado_ = datosGrupo.grupo_estado;
            htmlListGrup += '<td><button class= "btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target= "#myUpdate" data-toggle= "modal" data-placement= "top" title= "Modificar" type= "button" onClick= "editar('+datosGrupo.mercaderia_id+',\''+grupoData.descripcion+'\''+ ',\''+grupoData.clase+'\''+',\''+grupoData.tipo+'\''+');"><i class= "glyphicon glyphicon-pencil"></i></button><button class= "btncirculo btn-xs btn-danger btn-estado-'+estado_+'" style= "background:#FA8072" fa fa-plus-square pull-right" data-toggle= "modal" data-placement= "top" title= "Eliminar" type= "button" onClick= "darBaja('+datosGrupo.grupo_id+');"><i class= "glyphicon glyphicon-trash"></i></button></td>';
            htmlListGrup+= '<td align= "left">'+grupoData.responsable+'</td>';

            htmlListGrup+= '<td align= "left">'+grupoData.puesto_responsable+'</td>';
            htmlListGrup+= '<td align= "left">'+grupoData.datos_grupos.cantidad+'</td>';
            htmlListGrup+= '</tr>';
          };
          htmlListGrup += '</table></div></div>';
          htmlListGrup += '<div>';
          htmlListGrup += '<ul class= "pager">';
          htmlListGrup += '<li><a href= "#" onClick= "btnAnterior()">Anterior</a></li>';
          htmlListGrup += '<li><a href= "#" onClick= "btnSiguiente()">Siguiente</a></li>';
          htmlListGrup += '</ul>';
          htmlListGrup += '</div>';
          $('#listboleto').html(htmlListGrup);
          bloquearBoton();
        },
        error: function (xhr, status, error) { 
          console.log('error',44444);
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
      console.log('error no mostrar datos',44444);
    },
  });  
};
function bloquearBoton(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}


function darBaja(id){

  swal({   
    title: "Esta seguro de eliminar la boleto?",
    text: "Presione ok para eliminar el registro de la base de datos!",
    type: "warning",   showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Eliminar!",
    closeOnConfirm: false
  }, function(){
    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_VALLE-280',"parametros": '{"xgrupo_id":'+id+', "xgrupo_usr_id":'+1+'}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
           'authorization': 'Bearer '+ token,
         },
         success: function(data){
          swal("grupo!", "Fue eliminado correctamente!", "success");
        },
        error: function(result) {
          swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      },
    }); 
  });
};


$(document).ready(function (){
 listar1(1);
 cargarDatosUsuario();


 });
</script>

<script >
   var paging_pagesize = $( "#reg" ).val();
 var paging_pagenumbe = 1;
 function btnAnterior(){
     paging_pagenumbe--;
   listar1( paging_pagenumbe);
 }

 function btnSiguiente(){

  paging_pagenumbe++;
  listar1(paging_pagenumbe);
  }

  function listarSelect(){
   paging_pagenumbe=1;
   listar1(paging_pagenumbe);
  };


</script>
    @endpush

