@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.zoologico.partial.modalCreate')
@include('backend.parametricas.zoologico.partial.modalUpdate')
<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>RECAUDACIONES DE ZOOLOGICO</b></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">Operador:</div>
          <div class="col-md-3">
            <input type="text" id="nombresUsuario" class="form-control" name="nombresUsuario" readonly>
          </div>
          <div class="col-md-2">Nro. de Identificación:</div>
          <div class="col-md-3">
            <input type="text" id="identificacion" class="form-control" name="identificacion" readonly>
          </div>
        </div>
      </div>
      <br>
       <br>
   <div class="row">

  <div class="col-md-12">
    <div class="col-md-3">
      <a class="btn btn-primary" id="btnLiquidar" target="_blank" onclick="liquidarAnt()"  >Liquidar</a>
    </div>
    <div class="col-md-1">
    <label >Monto Niño:</label> 
       <input type="text" id="ninhos" class="form-control" name="ninhos" readonly>
    </div>
    <div class="col-md-1">
    <label >Monto Adulto:</label> 
       <input type="text" id="adultoSS" class="form-control" name="adultoSS" readonly>
    </div>
    <div class="col-md-1">
    <label >Monto Mayor:</label> 
       <input type="text" id="adultoMayor" class="form-control" name="adultoMayor" readonly >
    </div>
    <div class="col-md-1">
    <label >Monto Total:</label> 
       <input type="text" id="montoTotal" class="form-control" name="montoTotal" readonly >
    </div>
      <div class="col-md-5">
      
    </div>
  </div>
</div> 
    </div>
  </div>
</section>
<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title">
        <h3 align="center" >      
      Listado  boletos Zoologico
         </h3>
       </div>
      </section>
   </div>
</div>
<div class="row">
  <div class="col-md-6">
  </div>
  <div class="col-md-3">
   <label >Mostrar Registros:</label> 
   <select class="form-control" id="reg" name="reg" onclick="listarSelect()">
    <option value=10>10</option>
    <option value=15>15</option>
    <option value=20>20</option>
    <option value=100>100</option>
   </select>
  </div>
<div class="col-md-3">
  <label >Listar por:</label> 
  <select class="form-control" id="opcion" name="opcion" onclick="listarSelect()">
    <option value="activo" selected="selected" > ACTIVOS</option>
    <option value="inactivo">INACTIVOS</option>
    <option value="todo">TODO</option>
  </select>
  </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
            </div>
            <div class="box-body">
    <div id="no-more-tables">
    <div id="listboleto">
    </div>
</div>
@endsection
@push('scripts')
<script>
var _usrid = {{$usuarioid}};
var total = 0;
var opcion = '';
var datosItem = [];
var tam;
var sumaA=0;
var sumaN =0;
var sumaT =0;
var tamaño = 0;
var idBoletos = '';
var grupo = 'ZOOLOGICO';

function cargarDatosUsuario(){
  console.log('sasa');
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SISTEMA_VALLE-356","parametros": '{"yusrid":"'+_usrid+'"}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
     console.log(dataIN);
     var primerNombre = dataIN[0].vprs_nombres;
     var primerApellido = dataIN[0].vprs_paterno;
     var segundoApellido = dataIN[0].vprs_materno;
     cedulaIdentidad = dataIN[0].vprs_ci;
     razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
     $("#nombresUsuario").val(primerNombre+' '+primerApellido+' '+segundoApellido);
     $("#identificacion").val(cedulaIdentidad);
     datosUsuario = '{"razonSocial":"'+razonSocial+'","numeroIdentificacion":"'+cedulaIdentidad+'","tipoIdentificacion":"CI."}';
     datosUsuario = JSON.stringify(datosUsuario);
    },     
    error: function (xhr, status, error) { }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});
};
function listar(paging_pagenumber){
  htmlListBlt = '';
  htmlListBlt += '<div class= "row">';
  htmlListBlt += '<div class= "col-md-12" >' ;
  htmlListBlt += '<table id= "lts-boleto" class= "table table-striped table-hover" cellspacing= "0" width= "100%">';
  htmlListBlt += '<thead><tr><th align= "center">Nro.</th><th align= "center">Opciones</th><th align= "left">Nombre completo</th><th align= "left">bs</th><th align= "left">Nacionalidad</th></thead>';
  var opcion = $( "#opcion" ).val();
  var paging_pagesize = $( "#reg" ).val();
  var formData = {"identificador": 'SERVICIO_VALLE-289' ,"parametros": '{"xtipolista": "'+opcion+'","paging_pagesize": '+paging_pagesize+',"paging_pagenumber": '+paging_pagenumber+'}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer' + token,
        },
        success: function(dataIN) {
        var tam = dataIN.length;
        if (dataIN == "[{ }]"){
          tam = 0;
          htmlListBlt += ' <tr ><td align= "center"> NO HAY DATOS</td> </tr>';}
          var datos = dataIN;
          tamaño=dataIN.length;
          for (var i = 0; i < tam; i++) {
            idBoletos = idBoletos +  JSON.parse(datos[i].data).boleto_id + ',';
            var datosBlt = JSON.parse(datos[i].data);
            var boletoData = datosBlt.boleto_data;
            var datociudano= boletoData.datos_ciudadano;
            htmlListBlt += '<tr><td align= "left">'+(i+1)+'</td>';
            var estado_= datosBlt.boleto_estado;
            htmlListBlt += '<td><button class= "btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target= "#myUpdate" data-toggle= "modal" data-placement= "top" title= "Modificar" type= "button" onClick= "editar('+datosBlt.mercaderia_id+',\''+boletoData.descripcion+'\''+ ',\''+boletoData.clase+'\''+',\''+boletoData.tipo+'\''+');"><i class= "glyphicon glyphicon-pencil"></i></button><button class= "btncirculo btn-xs btn-danger btn-estado-'+estado_+'" style= "background:#FA8072" fa fa-plus-square pull-right" data-toggle= "modal" data-placement= "top" title= "Eliminar" type= "button" onClick= "darBaja('+datosBlt.boleto_id+');"><i class= "glyphicon glyphicon-trash"></i></button></td>';
            htmlListBlt+= '<td align= "left">'+boletoData.datos_ciudadano.nombre+'   '+boletoData.datos_ciudadano.apellidop+ '   '+boletoData.datos_ciudadano.apellidom+'</td>';
            htmlListBlt+= '<td align= "left">'+boletoData.montoTotal+'</td>';
            htmlListBlt+= '<td align= "left">'+boletoData.datos_ciudadano.nacionalidad+'</td>';
            htmlListBlt+= '</tr>';
          };
          htmlListBlt += '</table></div></div>';
          htmlListBlt += '<div>';
          htmlListBlt += '<ul class= "pager">';
          htmlListBlt += '<li><a href= "#" onClick= "btnAnterior()">Anterior</a></li>';
          htmlListBlt += '<li><a href= "#" onClick= "btnSiguiente()">Siguiente</a></li>';
          htmlListBlt += '</ul>';
          htmlListBlt += '</div>';
          $('#listboleto').html(htmlListBlt);
          bloquearBoton();
        },
        error: function (xhr, status, error) { 
          console.log('error',44444);
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
      console.log('error no mostrar datos',44444);
    },
  });  
};

function bloquearBoton(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}
function darBaja(id){

  swal({   
    title: "Esta seguro de eliminar la boleto?",
    text: "Presione ok para eliminar el registro de la base de datos!",
    type: "warning",   showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Eliminar!",
    closeOnConfirm: false
  }, function(){
    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_VALLE-280',"parametros": '{"xgrupo_id":'+id+', "xgrupo_usr_id":'+1+'}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
           'authorization': 'Bearer '+ token,
         },
         success: function(data){
          swal("grupo!", "Fue eliminado correctamente!", "success");
        },
        error: function(result) {
          swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      },
    }); 
  });
};

function cargardatosoperador(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}


///---FUNCION ELIMINAR---\\\
function eliminarBloque(){
idBoletos =idBoletos.substr(0,idBoletos.length-1);
var cadena = idBoletos.split(",");
var tam =cadena.length; 
for (var i = 0; i < tam; i++) {
      var j = cadena[i];
      darBajaboleto(j);
  };
    listar(1);
};

function darBajaboleto(id){

    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_VALLE-292',"parametros": '{"xboleto_id":'+id+', "xboleto_usr_id":'+_usrid+'}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
           'authorization': 'Bearer '+ token,
         },
         success: function(data){
          swal("boleto!", "Fue eliminado correctamente!", "success");
        },
        error: function(result) {
          swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      },
    }); 
};

function listarboletos(){
    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_VALLE-359',"parametros": '{}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
           'authorization': 'Bearer '+ token,
         },
         success: function(dataBol){
          for (var i = 0; i < dataBol.length; i++) {
            idBoletos = idBoletos +  JSON.parse(dataBol[i].data).boleto_id + ',';
          };
        },
        error: function(result) {
          swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      },
    }); 
};

function consumir(){
  var usuario = _usrid;

    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_VALLE-308',"parametros":'{"modulo":"'+grupo+'"}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
           'authorization': 'Bearer '+ token,
         },
            //sandra.macuchapi
            //http://192.168.5.141:3048 
         success: function(data){
          datosItem = JSON.parse(data[0].sp_obtener_item_modulo);
              tam = datosItem.length;
        },
        error: function(result) {
          swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      },
    }); 
};

function llenarCombos1(){
  document.getElementById("item").length=0;  
  tam = datosItem.length;
  if(datosItem == "[{ }]"){
    tam = 0; };
  for(var i=0;i<tam;i++){ 
    document.getElementById("item").innerHTML += "<option value='"+datosItem[i].costo+"'>"+datosItem[i].items+"</option>";
  };
};

function sumar(){
  var formData = {"identificador": 'SERVICIO_VALLE-359' ,"parametros": '{}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer' + token,
        },
        success: function(dataIN) {
          console.log(dataIN.length,5555);
          var datos = dataIN;
          sumaA =0;
          sumaN =0;
          sumaT =0;
         var tam = dataIN.length;
        if (dataIN == "[{ }]"){
          tam = 0; }
          for (var i = 0; i < tam; i++) {
            var datosBlt = JSON.parse(datos[i].data);
            var boletoData = datosBlt.boleto_data;
            var datosbol= boletoData.datos_boleto;
            var  valor = datosbol.length
            for (var j = 0; j < valor; j++) {
                if( datosbol[j].items=="Adulto" ){sumaA=sumaA + datosbol[j].montoTotalPorItem;}
                if( datosbol[j].items=="Niño" ){sumaN= sumaN + datosbol[j].montoTotalPorItem;}
                if( datosbol[j].items=="Adulto Mayor" ){sumaT=sumaT + datosbol[j].montoTotalPorItem;}
              };
          };
         console.log('adultos',sumaA,'niño' ,sumaN,'mayor',sumaT);
          $("#ninhos").val(sumaN);
          $("#adultoSS").val(sumaA);
          $("#adultoMayor").val(sumaT);
          var to=sumaN+sumaA+sumaT;
             $("#montoTotal").val(to);

           
        },
        error: function (xhr, status, error) { 
          console.log('error',44444);
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
      console.log('error no mostrar datos',44444);
    },
  });  
};
///---FUNCION REGISTRAR---\\\
$("#registrar").click(function() {
  var monto = document.getElementById("item").value;
  var combo = document.getElementById("item");
  var item = combo.options[combo.selectedIndex].text;
  var nombre = $("#nombre" ).val();
  var apellidop = $("#apellidop" ).val();
  var apellidom = $("#apellidom" ).val();
  var ci = $("#ci" ).val();
  var edad = $("#edad" ).val();
  var nacionalidad = $("#nacionalidad" ).val();
  var databoleto = '{"nacionalidad": "'+nacionalidad+'", "monto": '+monto+', "datos_ciudadano": { "nombre": "'+nombre+'", "apellidop": "'+apellidop+'", "apellidom": "'+apellidom+'" ,"ci":'+ci+' , "edad": '+edad+'}}';
  databoleto = JSON.stringify(databoleto);
  var usuario = _usrid;
  var formData = {"identificador": 'SERVICIO_VALLE-290 ' ,"parametros"  : '{"xboleto_id_ciudadano":'+1+',"xboleto_data":'+databoleto+', "xboleto_usr_id":'+ usuario+'}'};  
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {    
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
         'authorization': 'Bearer ' + token,
       },
       success: function(dataIN) {
        swal( "Exito..!", "Se guardo correctamente los datos!", "success" );
        $("#descripcion" ).val('');
        $("#clase" ).val("");
        $("#tipo" ).val("");
        document.ready = document.getElementById("opcion").value = 'activo';
        listar(1);
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      }
    });
    },   
  });
});

$("#registrar2").click(function() {
  var monto = document.getElementById("item2").value;
  var combo = document.getElementById("item2");
  var item = combo.options[combo.selectedIndex].text;
  var nombre = $("#nombre2" ).val();
  var apellidop = $("#apellidop2" ).val();
  var apellidom = $("#apellidom2" ).val();
  var ci = $("#ci2" ).val();
  var edad = $("#edad2" ).val();
  var nacionalidad = $("#nacionalidad2" ).val();
  var databoleto = '{"nacionalidad": "'+nacionalidad+'", "monto": '+monto+', "datos_ciudadano": { "nombre": "'+nombre+'", "apellidop": "'+apellidop+'", "apellidom": "'+apellidom+'" ,"ci":'+ci+' , "edad": '+edad+'}}';
  databoleto = JSON.stringify(databoleto);
  var usuario = _usrid;
  var formData = {"identificador": 'SERVICIO_VALLE-290 ' ,"parametros"  : '{"xboleto_id_ciudadano":'+1+',"xboleto_data":'+databoleto+', "xboleto_usr_id":'+ usuario+'}'};  
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {    
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
         'authorization': 'Bearer ' + token,
       },
       success: function(dataIN) {
        swal( "Exito..!", "Se guardo correctamente los datos!", "success" );
        $("#descripcion" ).val('');
        $("#clase" ).val("");
        $("#tipo" ).val("");
        document.ready = document.getElementById("opcion").value = 'activo';
        listar(1);
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      }
    });
    },   
  });
});
///---UNCION LIMPIAR---///
function Limpiar(){
  $("#descripcion" ).val('');
  $("#clase" ).val('');
  $( "#tipo").val('');
  $("#apellidop2" ).val('');
  $("#apellidom2" ).val('');
  $("#ci2" ).val('');
  $("#edad2" ).val('');
  $("#nacionalidad2" ).val('');
  $("#Monto2" ).val('');
};
function Limpiar2(){
  $("#descripcion" ).val('');
  $("#clase" ).val('');
  $( "#tipo").val('');
  $("#nombre" ).val('');
  $("#apellidop" ).val('');
  $("#apellidom" ).val('');
  $("#ci" ).val('');
  $("#edad" ).val('');
  $("#nacionalidad" ).val('');
  $("#Monto" ).val('');
};

$(document).ready(function (){
 listar(1);
 consumir();
 cargarDatosUsuario();
 sumar();
});
var paging_pagesize = $( "#reg" ).val();
var paging_pagenumbe = 1;

function btnAnterior(){
  paging_pagenumbe--;
  listar( paging_pagenumbe);
};

function btnSiguiente(){
  paging_pagenumbe++;
  listar(paging_pagenumbe);
};

function listarSelect(){
  paging_pagenumbe=1;
  listar(paging_pagenumbe);
};

function liquidarAnt(){ 
  var monto = sumaA+sumaN+sumaT;
  console.log(monto);
  monto= monto+' ';
  console.log(monto);
  var nombre=$('#nombresUsuario').val();
  var ci=$('#identificacion').val();
  var array = [  nombre,ci,grupo,monto];
  array = array.toString();
  codigoQRfum2(array); 
  setTimeout(function(){ 
  liquidar();  
  }, 500);
}

function liquidar(){ 

    var nombre=$('#nombresUsuario').val();
  var ci=$('#identificacion').val();
   swal({   title: "Confirmar Registro FUM?",
        text: "Presione ok para registrar FUM!",
        type: "success",   showCancelButton: true,
        confirmButtonColor: "#1a9e2a",
        confirmButtonText: "OK",
        closeOnConfirm: false
        }, function(){     
        var idDeudas = '0';
        var fecha = new Date();
        var gestiones = fecha.getFullYear();
        var contribuyente = '{"razonSocial":"'+nombre+' ","numeroIdentificacion":"'+ci+'","tipoIdentificacion":"CI","codigo":"'+imagenQRfum+'"}';

      var dataGlosa = '{"tipoPersona":"NOMBRE COMPLETO", "cabecera":"ZOOLOGICO","detalles":{"detalleA":"Mallaza","detalleB":"Mallaza "},"pie":"","observaciones":""}';
      var transacciones = '[{"idItemR":1,"descripcionItem":"Adulto","Monto":'+sumaA+'},{"idItemR":2,"descripcionItem":"Niño","Monto":'+sumaN+'},{"idItemR":3,"descripcionItem":"Adulto Mayor","Monto":'+sumaT+'}]';
      var tipo = 'ZOOLOGICO';

        dataGlosa = JSON.stringify(dataGlosa);  
        contribuyente = JSON.stringify(contribuyente);
        generarFum1(idDeudas,gestiones,contribuyente,dataGlosa,transacciones,tipo,_usrid); 
         setTimeout(function(){
              imprimirFum1();  
         }, 500);
        generarProforma();


});
}
function generarProforma(){  
  eliminarBloque();
   setTimeout(function(){
   //......esperando  
  }, 500);
  setTimeout(function(){
    var datosDatFum = datoFum;   
    //var datosDatFum = JSON.stringify(datosDatFum);
    var urlPdf= "{{ action('reportes\proformaCementerioController@getGenerar',['datos'=>'d1']) }}";  
    urlPdf = urlPdf.replace('d1', datosDatFum);
   //actualizarfum(urlPdf, 1);
    window.open(urlPdf);   
  }, 500);
  
}


</script>
@endpush