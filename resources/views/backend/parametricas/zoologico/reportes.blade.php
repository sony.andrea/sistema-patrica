@extends('backend.template.app')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <section class="content-header">
      <div class="header_title">
        <h3 align="">   Lista de Reporte </h3>
      </div>
    </section>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="col-md-2"></div>
      <div class="col-md-2"><label >Fecha inicio:</label> 
      <input type="date" id="fechainicio" class="form-control" name="fechainicio" value="<?php echo date('Y-m-d'); ?>" >
      </div>
      <div class="col-md-2"><label >Fecha Final</label> 
      <input type="date" id="fechafinal" class="form-control" name="fechafinal" value="<?php echo date('Y-m-d'); ?>"  >
      </div>
      <div class="col-md-3"><label >Mostrar Registros:</label> 
      <select class="form-control" id="reg" name="reg" onclick="listarSelect()">
      <option value=10>10</option>
      <option value=5>5</option>
      <option value=20>20</option>
      <option value=16145>16144</option>
      </select>
      </div>
      <div class="col-md-3"><label >Listar por:</label> 
      <select class="form-control" id="opcion" name="opcion" onclick="listarSelect()">
      <option value="activo" > ACTIVOS</option>
      <option value="inactivo">INACTIVOS</option>
      <option value="todo" selected="selected" >TODO</option>
      </select>
      </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-12">
   <div class="col-md-6"></div>
    <div class="col-md-3">
      <a class="btn btn-primary  pull-right" id="irUbicacion" onclick="listar(1)">Sacar reporte</a>
    </div>
    <div class="col-md-3">
      <a class="btn btn-primary  pull-right" id="irUbicacion" onclick="generarReporte()">imprimir</a>
    </div>
  </div>
</div> 
<br>
<div class="row">
  <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border"></div>
      <div class="box-body">
    <div id="no-more-tables">
    <div id="listMercado">
    </div>
</div>

@endsection
@push('scripts')
<script>
var datosZona = [];
var datosVia = [];
var datosCatalogo = [];
var tipo = '';
var idusu = {{$usuarioid}};
var total=0;
var opcion='';
var fechaHoy = '2018-06-14';
var datosUsuario="";
var datofum='';
var datosReportes = '';
function generarReporte(){
  listar(); 
  setTimeout(function(){
  var datosDatFum = {datoFum,datosUsuario};
  var datosDatFum = JSON.stringify(datosDatFum);
  var fechax1= $("#fechainicio" ).val();
  var fechaz1 = $("#fechafinal" ).val();
  var datosReportes = '[{"fechax": "'+fechax1+'", "fechay": "'+fechaz1+'" }]' ;
   //var datosReportes = JSON.stringify(datosReportes);
  var urlPdf= "{{ action('parametricas\zoologicoreporteController@getGenerar',['datos'=>'d1']) }}";  
  urlPdf = urlPdf.replace('d1', datosReportes);
  window.open(urlPdf); 
  }, 500);
}

function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SISTEMA_VALLE-356","parametros": '{"yusrid":"'+idusu+'"}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
     var primerNombre = dataIN[0].vprs_nombres;
     var primerApellido = dataIN[0].vprs_paterno;
     var segundoApellido = dataIN[0].vprs_materno;
     var cedulaIdentidad = dataIN[0].vprs_ci;
     var  razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
     datosUsuario = '{"razonSocial":"'+razonSocial+'","numeroIdentificacion":"'+cedulaIdentidad+'","tipoIdentificacion":"CI."}';
     datosUsuario = JSON.stringify(datosUsuario);
    },     
    error: function (xhr, status, error) { }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});
};
///---FUNCION LISTAR---///
function listar(paging_pagenumbe){
  var fechax1= $("#fechainicio" ).val();
 var fechaz1 = $("#fechafinal" ).val();
  htmlListaMercado ='';
  htmlListaMercado+='<div class="row">';
  htmlListaMercado+='<div class="col-md-12" >' ;
  htmlListaMercado+='<table id="lts-mercado" class="table table-striped table-hover" cellspacing="0" width="100%" >';
  htmlListaMercado+='<thead><tr><th align="center">Nro.</th><th align="center" class="form-nobajas-cabecera">Opciones</th><th align="left">Fecha</th><th align="left">Nombre Completo</th><th align="left">Monto</th></thead>';
  var opcion=$("#opcion").val();
  var paging_pagesize = $( "#reg" ).val();
   var formData = {"identificador": ' SERVICIO_VALLE-468',"parametros": '{"xtipolista":"'+opcion+'","paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+',"fechax":"'+fechax1+'","fechaz":"'+fechaz1+'"}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer' + token,
        },
        success: function(dataReporteBoleto) {
          datoFum = dataReporteBoleto;
          var tam=dataReporteBoleto.length;
          if (dataReporteBoleto=="[{ }]") {tam = 0;};
          for (var i = 0; i < tam; i++) { 
            var datosBoleto = JSON.parse(dataReporteBoleto[i].data);
            var estado_ = datosBoleto.mercado_estado;
            htmlListaMercado+='<tr><td align="left">'+(i+1)+'</td>';
            htmlListaMercado+='<td class="form-nobajas"><button  class="btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar();"><i class="glyphicon glyphicon-pencil"></i></button>  <button class="btncirculo btn-xs btn-danger btn-estado-'+estado_+'" style="background:#FA8072"   fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja('+datosBoleto.mercado_id+');"><i class="glyphicon glyphicon-trash"></i></button></td>';
            htmlListaMercado +='<td align="left">'+datosBoleto.fechax+'</td>';
            htmlListaMercado +='<td align="left">'+datosBoleto.boleto_data.datos_ciudadano.nombre+' '+ datosBoleto.boleto_data.datos_ciudadano.apellidop+' '+ datosBoleto.boleto_data.datos_ciudadano.apellidom+'</td> ';
            htmlListaMercado +='<td align="left">'+datosBoleto.boleto_data.montoTotal+'</td>';
            htmlListaMercado += '</tr>';
          }
          htmlListaMercado +='</table></div></div>';
          htmlListaMercado +='<div>';
          htmlListaMercado +='<ul class="pager">';
          htmlListaMercado +='<li><a href="#" onClick="btnAnterior()">Anterior</a></li>';
          htmlListaMercado +='<li><a href="#" onClick="btnSiguiente()">Siguiente</a></li>';
          htmlListaMercado +='</ul>';
          htmlListaMercado +='</div>';
          $('#listMercado').html(htmlListaMercado);
          bloquearBoton();
        },
        error: function (xhr, status, error) {
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
  };

function bloquearBoton(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}

$(document).ready(function (){
 cargarDatosUsuario();
  listar(1);
});

var paging_pagesize = $( "#reg" ).val();
var paging_pagenumbe = 1;
function btnAnterior(){paging_pagenumbe--;listar( paging_pagenumbe);}
function btnSiguiente(){paging_pagenumbe++;listar(paging_pagenumbe);}
function listarSelect(){paging_pagenumbe=1;listar(paging_pagenumbe);};
</script>
@endpush
