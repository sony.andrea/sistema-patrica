    @extends('backend.template.app')
    @section('main-content')
@include('backend.parametricas.zoologico.partial.modalCreate')
@include('backend.parametricas.zoologico.partial.modalUpdate')


    <style type="text/css">
      #listMonto li{
        text-decoration: none;
        list-style:none;
      }
</style>
<div id="formulario1" style="display: ;">  
<section class="content-header">
  <div class="panel panel-info">
    <div class="panel-heading"><b>RECAUDACIONES DE ZOOLOGICO</b></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">Operador:</div>
          <div class="col-md-3">
            <input type="text" id="nombresUsuario" class="form-control" name="nombresUsuario" readonly>
          </div>
          <div class="col-md-2">Nro. de Identificación:</div>
          <div class="col-md-3">
            <input type="text" id="identificacion" class="form-control" name="identificacion" readonly>
          </div>
        </div>
      </div>
      <br>
      <br>
       <div class= "row">
            <div class= "col-md-12">
              <div class= "form-group">
                <div class= "col-sm-4">
                  <button type="button" id="liquidardeuda" class="btn btn-primary"  onclick="ocultar()" data-toggle="modal">Servicios Del Zoologico</button>
                </div>
                <div class= "col-sm-4">
                 <button type="button" id="ocultar1" class="btn btn-primary"  onclick="mostrarGrupo()" data-toggle="modal">Listado de Grupo</button>
                </div>
                <div class= "col-sm-4">
                  
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
</section>
</div>
<div id="formulario" style="display:none; ;">
<section class="content-header">
<form role="form">
  <div class= "panel panel-info">
    <div class= "panel-heading">
      <h4 >Datos del Ciudadano
     </h4>
    </div>
    <div class= "panel-body">
        {!! Form::open(['id' => 'boleto'])!!}
          <div class= "row">
            <div class= "col-md-12">
              <div class= "form-group">
                <div class= "col-sm-4">
                  <label>CI:</label>
                  {!! Form::number('idci', null, array('placeholder' => 'Ingrese CI ','maxlength'=>'20','class' => 'form-control','id'=>'ci2')) !!}
                </div>
                <div class= "col-sm-4">
                <label>Nacionalidad:</label>
                <span class= "block input-icon input-icon-right">
                <select class= " form-control" placeholder = "Seleccionar Clase de Nacionalidad" id = "nacionalidad2" name= "clase">
                  <option value= "Boliviano">Boliviano </option>
                  <option value= "Extranjero">Extranjero</option>
                </select>
                </span>
                </div>
              </div>
            </div>
          </div>
          <div class= "row">
            <div class= "col-md-12">
              <div class= "form-group">
                <div class= "col-sm-4">
                  <label>Nombre Completo:</label>
                  {!! Form::text('idnombre', null, array('placeholder' => 'Ingrese Nombre ','maxlength'=>'20','class' => 'form-control','id'=>'nombre2')) !!}
                </div>
                <div class= "col-sm-4">
                  <label>Apellido Paterno:</label>
                  {!! Form::text('idapellidop', null, array('placeholder' => 'Ingrese Apellido ','maxlength'=>'20','class' => 'form-control','id'=>'apellidop2')) !!}
                </div>
                <div class= "col-sm-4">
                  <label>Apellido Materno</label>
                  {!! Form::text('idapellidom', null, array('placeholder' => 'Ingrese Apellido Materno ','maxlength'=>'20','class' => 'form-control','id'=>'apellidom2')) !!}
                </div>
              </div>
            </div>
          
        </div>
        <br>
     
</div>
</div>
</form>

</section>
   <div id="formulario2" style="display: none;">
   <section class="content-header">
    <div ><label>  Servicios         </label>    </div>
   <div class="panel panel-info">
   <br>
        <div class="row">
          <div class="col-md-12">
          <div class="col-md-4">
           <h5><b> Lista de items</b></h5> 
           </div>
            <div class="col-md-4">
              <select name="idItemR" id="items" class="form-control" onChange='obtenerDatosidItemR()'>
               <option>Selecciona items...</option>
             </select>
             <input type="hidden" name="descripcionItem" id="descripcionItem">
           </div>
           <div class="col-md-4 form-group">
            <span class="control-label col-md-1">Bs.</span> 
            <div class="col-md-3">
              <input type="number" name="monto" class="form-control" id="monto" disabled value="0">
            </div>
            <span class="control-label col-md-2">Cantidad:</span> 
            <div class="col-md-4">
              <input type="number" name="cantidad" class="form-control" id="cantidad" value="1">
              <button class="btn btn-primary fa fa-plus-square pull-left" id="exe">&nbsp;Adicionar</button>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-4">
             <b>DESCRIPCION DEL ITEM</b>
          </div>
          <div class="col-md-2" id="listMonto">
             <b>COSTO POR UNIDAD</b>
          </div>
          <div class="col-md-1">
            <b>CANTIDAD</b>
          </div>
          <div class="col-md-1">
            <b>TOTAL</b>
          </div>
          <div class="col-md-3"></div>
      </div>
      <div class="row">
          <div class="col-md-5">
          <ol id="items-ol"></ol><br>
          </div>
          <div class="col-md-2" id="listMonto">
          <ul id="monto-ul" ></ul>
          </div>
          <div class="col-md-1" id="listMonto">
            <ul id="cantidad-ul" ></ul>
          </div>
          <div class="col-md-3" id="listMonto">
            <ul id="montoTotal-ul" ></ul>
          </div>
         <div class="col-md-1"></div>
      </div>
         <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal">     
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-6">Total a Pagar:</label>
              <div class="col-md-6">
                <input type="text" id="total" name="total" class="form-control" placeholder="0" align="center" readonly>
              </div>
            </div>
          </div>
        </form>
         <form class="form-horizontal">     
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-6">Monto en Efectivo:</label>
              <div class="col-md-6">
                     {!! Form::number('idefectivo', null, array('placeholder' => 'Ingrese Efectivo ','maxlength'=>'20','class' => 'form-control','id'=>'efectivoci')) !!}
              </div>
            </div>
          </div>
        </form>
        <form class="form-horizontal">
          <div class="col-md-4">               
            <div class="form-natural">
              <div class="form-group">
                <label class="control-label col-md-2">Bs.</label>
                <div class="col-md-3">
                  <button type="button" id="liquidardeuda" class="btn btn-primary"  onclick="guardar()" data-toggle="modal">Liquidar</button>
                </div>
                 <div class="col-md-3">
                </div>
              </div>
            </div>
          </div>
        </form>
      </div> 
      <div class="col-md-6">
      </div>   
    </div>
          <div class= "row">
            <div class= "col-md-12">
              <div class= "form-group">
                <div class= "col-sm-2">
               
                </div>
                 <div class= "col-sm-2">
                  <br>
                      <br>
              
                
                </div>
                <div class= "col-sm-2">
                  <label>Cambio:</label>
                  {!! Form::number('idCambio', null, array('placeholder' => 'Cambio ','maxlength'=>'20','class' => 'form-control','id'=>'cambio','readonly')) !!}
                </div>
                 <div class= "col-sm-2">
                   <br>
                      <br>
                  <label>Bs.</label>
               
                </div>
                <div class= "col-sm-4">
                    <br>
                        
                  <a class="btn btn-primary  " id="irUbiaaacacion" onclick="obtenerFactura()">factura</a>
                </div>
              </div>
            </div>
          
        </div>
   <div class="row">
      <div class="col-md-12">
       

      <div class="form-group col-md-6">
           
      </div>
    </div>
  </div>

  </section>
</div>
</div>
<div id="listadogrupo" style="display:none;">  
<div class="row">
   <div class="col-md-12">
      <section class="content-header">
       <div class="header_title with-border">
        <h3 align="center" >      
      Listado  Grupo Zoologico
         </h3>
         <br>
       </div>
      </section>
   </div>
</div>
<div class="row">
  <div class="col-md-2">


  </div>
    <div class="col-md-2" align="right">
    <label > </label> <br>
<a align="right" class="btn btn-primary  " id="buscar" onclick="buscarfec()">buscar grupos </a>

  </div>
  <div class="col-md-2">
    <label >Fecha:</label> 
      <input type="date" id="fechaActual" class="form-control" name="fechaActual" value="<?php echo date('Y-m-d'); ?>" >
      
  </div>
  <div class="col-md-3">
   <label >Mostrar Registros:</label> 
   <select class="form-control" id="reg" name="reg" onclick="listarSelect()">
    <option value=10>10</option>
    <option value=15>15</option>
    <option value=20>20</option>
    <option value=30>30</option>
   </select>
  </div>
<div class="col-md-3">
  <label >Listar por:</label> 
  <select class="form-control" id="opcion" name="opcion" onclick="listarSelect()">
    <option value="activo" selected="selected" > ACTIVOS</option>
    <option value="inactivo">INACTIVOS</option>
    <option value="todo">TODO</option>
  </select>
  </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
            </div>
            <div class="box-body">
    <div id="no-more-tables">
    <div id="listboleto">
    </div>
</div>
</div>
@endsection
@push('scripts')
<script>
var usrid = {{$usuarioid}}
var count = 0;
var tipoIdenContri = "CI";
var nombreContri = "";
var direccionContri = "";
var razonSocial = "";
var cedulaIdentidad = "";
var liItem="";
var liMonto="";
var liCantidad="";
var liMontoTotal="";
var totalMonto=0;
var datosItem='';
var bloqueItem='';
var bloqueItemgrupo='';
var fechaHoy = '2018-06-20';
var v = 1;
var totaln= 0;
var totala= 0;
var totalm= 0;
var razonsocial= 0;
var nitoci= 0;
var costototali=0;
var efec=0;
var nombre_institucion='';
var idGrupo12 = 0;
var fechaActual ='2018-06-20'
///carga  usuario
function buscarfec(){
fechaActual = $("#fechaActual").val();
listar1(1,fechaActual);
};
function cargarDatosUsuario(){
 $.ajax({
  type        : 'GET',
  url         :  urlToken,
  data        : '',
  success: function(token) {         
    var formData = {"identificador": "SISTEMA_VALLE-356","parametros": '{"yusrid":"'+usrid+'"}'};
    $.ajax({
     type        : 'POST',            
     url         : urlRegla,
     data        : formData,
     dataType    : 'json',
     crossDomain : true,
     headers: {
       'authorization': 'Bearer '+token,
     },
     success: function(dataIN) {
     console.log(dataIN);
     var primerNombre = dataIN[0].vprs_nombres;
     var primerApellido = dataIN[0].vprs_paterno;
     var segundoApellido = dataIN[0].vprs_materno;
     cedulaIdentidad = dataIN[0].vprs_ci;
     razonSocial = primerNombre+' '+primerApellido+' '+segundoApellido;
     $("#nombresUsuario").val(primerNombre+' '+primerApellido+' '+segundoApellido);
     $("#identificacion").val(cedulaIdentidad);
     datosUsuario = '{"razonSocial":"'+razonSocial+'","numeroIdentificacion":"'+cedulaIdentidad+'","tipoIdentificacion":"CI."}';
     datosUsuario = JSON.stringify(datosUsuario);
    },     
    error: function (xhr, status, error) { }
  });
  },
  error: function(result) {
    swal( "Error..!", "No se puedo guardar los datos", "error" );
  },
});
};

function obtenerItemRecaudadorPorUnidadRecaudadora()
      {
        var unidad_recaudadora = 'ZOOLOGICO';
        $.ajax({
          type        : 'GET',
          url         :  urlToken,
          data        : '',
          success: function(token) {
            var formData = {"identificador": 'SERVICIO_VALLE-308',"parametros": '{"modulo":"'+unidad_recaudadora+'"}'};
            $.ajax({
             type        : 'POST',            
             url         : urlRegla,
             data        : formData,
             dataType    : 'json',
             crossDomain : true,
             headers: {
               'authorization': 'Bearer'+ token,
             },
             success: function(dataIN) {
               if (dataIN!="[{ }]") {
                var tam=dataIN.length;
              }  
              for(var i=0; i< tam; i++){

                var dataItem = JSON.parse(dataIN[0].sp_obtener_item_modulo);
                var tam2 = dataItem.length;
                for(var j=0; j< tam2; j++){
                  var item = dataItem[j].costo;
                  var itemDesc = dataItem[j].descripcionItem;
                  var id = dataItem[j].idItem;
                  var todocampo= id +','+itemDesc+','+item;
                  document.getElementById("items").innerHTML += '<option value="'+todocampo+'">'+itemDesc+'</option>';
                }
              }             
            },     
            error: function (xhr, status, error) {console.log(error); }
          });
          },
          error: function(result) {
            swal( "Error..!", "No se puedo guardar los datos", "error" );
          },
        }); 
      }
//"{"grupo_id":15,"grupo_data":{"hora": "10:00 am", "fecha": "2018-05-29", "gmail": "1", "responsable": "LAURA", "datos_grupos": {"gia": "richar", "nombre": "richar", "cantidad": 78}, "puesto_responsable": "PROFESOR"},"fechax":"2018-06-14"}"


function listar1(paging_pagenumber,fechaActual){
  var fechaprueba='2018-07-02';
  htmlListGrup = '';
  htmlListGrup += '<div class= "row">';
  htmlListGrup += '<div class= "col-md-12" >' ;
  htmlListGrup += '<table id= "lts-boleto" class= "table table-striped table-hover" cellspacing= "0" width= "100%">';
  htmlListGrup += '<thead><tr><th align= "center">Nro.</th><th align= "center">Opciones</th><th align= "left">Nombre de la Intitucion</th><th align= "left">Nombre del Encargado</th><th align= "left">costo Total</th><th align= "left">Fecha de Visita</th></thead>';
  var opcion = $( "#opcion" ).val();
  var paging_pagesize = $( "#reg" ).val();
  var formData = {"identificador": 'SERVICIO_VALLE-639' ,"parametros": '{"xtipolista": "'+opcion+'","paging_pagesize": '+paging_pagesize+',"paging_pagenumber": '+paging_pagenumber+',"fechax":"'+fechaActual+'"}'};
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) {
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer' + token,
        },
        success: function(dataIN) {
          var datos = dataIN;
          var tam = dataIN.length;
          //{"cargo": "DIRECTORA", "corre": "CARRILLO@GMAIL.COM", "tot_ad": 20, "tot_ni": 25, "cant_ad": 2, "cant_ni": 5, "celular": "75275275", "materno": "SUAREZ", "nom_aux": "ANA MARIA RIVAS", "paterno": "CARRILLO", "tot_adm": 7, "cant_adm": 1, "asigna_aux": "1", "fac_ci_nit": 95989699, "validacion": "1", "costo_total": 52, "hora_visita": "08:00 am", "nombre_guia": "FAVIAN CARRASCO POZO", "fecha_visita": "2018-06-30", "razon_social": "ALEX FERNANDEZ", "correo_institucion": "JUNIN@GMAIL.COM", "nombre_institucion": "U. E. JUNIN", "nombre_responsable": "ELENA", "telefono_institucion": "2323232", "direccion_institucion": "VILLA ARMONIA"}
          //"{"grupo_id":42,"grupo_data":{"asigna_aux": "1", "cargo_resp": "DIRECTOR DIRRECTOR", "fac_ci_nit": 741, "validacion": "1", "cant_ninhos": 200, "correo_inst": "HG", "correo_resp": "BV NN", "hora_visita": "01:00 pm", "nombre_guia": "PEPE", "nombre_inst": "FINAL FINAL", "nombre_resp": "HH", "cant_adultos": 20, "celular_resp": "212", "fecha_visita": "2018-07-05", "materno_resp": "", "nom_auxiliar": "UUU", "paterno_resp": "GHJ", "telefono_inst": "9652", "cant_admayores": 5, "direccion_inst": "FGHJM", "tot_precio_nin": 1000, "cant_visitantes": 225, "tot_precio_adul": 200, "fac_razon_social": "NUEVO Y FINAL", "precio_total_pag": 1235, "tot_precio_admayor": 35},"fechax":"2018-06-29","grupo_estado":"activo"}"
            if (dataIN=="[{ }]") {
                tam=0;
                htmlListGrup += ' <tr ><td align= "center"> YA NO HAY GRUPOS PARA EL DIA</td> </tr>';
              }
          for (var i = 0; i < tam; i++) {
            var datosGrupo = JSON.parse(datos[i].data);
            var grupoData = datosGrupo.grupo_data;
            htmlListGrup += '<tr><td align= "left">'+(i+1)+'</td>';
            var estado_ = datosGrupo.grupo_estado;
            htmlListGrup += '<td><button class= "btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target= "#myCreate" data-toggle= "modal" data-placement= "top" title= "guardagrupo" type= "button" onClick= "guardagrupo('+grupoData.precio_total_pag+', \''+grupoData.fac_razon_social+'\''+','+grupoData.cant_adultos+','+grupoData.cant_ninhos+','+grupoData.cant_admayores+','+grupoData.fac_ci_nit+','+grupoData.tot_precio_nin+','+grupoData.tot_precio_adul+','+grupoData.tot_precio_admayor+', \''+grupoData.nombre_inst+'\''+','+ datosGrupo.grupo_id +');"><i class= "glyphicon glyphicon-pencil"></i></button><button class= "btncirculo btn-xs btn-danger btn-estado-'+estado_+'" style= "background:#FA8072" fa fa-plus-square pull-right" data-toggle= "modal" data-placement= "top" title= "Eliminar" type= "button" onClick= "darBaja('+datosGrupo.grupo_id+');"><i class= "glyphicon glyphicon-trash"></i></button></td>';
            htmlListGrup+= '<td align= "left">'+grupoData.nombre_inst +'</td>';
            htmlListGrup+= '<td align= "left">'+grupoData.nombre_resp+' '+grupoData.paterno_resp + ' '+grupoData.materno_resp+'</td>';
            htmlListGrup+= '<td align= "left">'+grupoData.tot_precio_nin+'</td>';
            htmlListGrup+= '<td align= "left">'+grupoData.fecha_visita+'</td>';
            htmlListGrup+= '</tr>';
          };
          htmlListGrup += '</table></div></div>';
          htmlListGrup += '<div>';
          htmlListGrup += '<ul class= "pager">';
          htmlListGrup += '<li><a href= "#" onClick= "btnAnterior()">Anterior</a></li>';
          htmlListGrup += '<li><a href= "#" onClick= "btnSiguiente()">Siguiente</a></li>';
          htmlListGrup += '</ul>';
          htmlListGrup += '</div>';
          $('#listboleto').html(htmlListGrup);
          bloquearBoton();
        },
        error: function (xhr, status, error) { 
          console.log('error',44444);
        }
      });
    },
    error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
      console.log('error no mostrar datos',44444);
    },
  });  
};
function bloquearBoton(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}

function guardagrupo(costototal,razon,adulto,niño,terceedad,ci_nit,tn,ta,tm,nombreIndtitucion,idGrupo){
  nombre_institucion = nombreIndtitucion;
  console.log('iidGrupo',idGrupo);
  $("#nit").val(ci_nit);
  $("#razon").val(razon);
  $("#niño").val(niño);
  $("#adulto").val(adulto);
  $("#terceedad").val(terceedad);
  $("#costo").val(costototal);
  totaln= tn;
  totala= ta;
  totalm= tm;
  razonsocial= razon;
  nitoci= ci_nit;
  costototali=costototal;
  idGrupo12=idGrupo;
};

$("#Registrar").click(function() {
  var nitreg = $("#nit").val();
  var razonreg = $("#razon").val();
  var niñoreg = $("#niño").val();
  var adultoreg = $("#adulto").val();
  var terceedadreg = $("#terceedad").val();
  var costoreg = $("#costo").val();
  var nombre=$('#nombresUsuario').val();
  var ci=$('#identificacion').val();
  efec=$('#efectivo').val();
  console.log(nitreg,razonreg,niñoreg,adultoreg,terceedadreg,costoreg);
  var  bloqueitems = '{"items": "Niño", "monto": 5, "codigo": "1", "cantidad": "'+niñoreg+'", "montoTotalPorItem": '+totaln+'}, {"items": "Adulto", "monto": 10, "codigo": "2", "cantidad": "'+adultoreg+'", "montoTotalPorItem": '+totala+'}, {"items": "Adulto Mayor", "monto": 7, "codigo": "3", "cantidad": "'+terceedadreg+'", "montoTotalPorItem": '+totalm+'}';
    bloqueItemgrupo = bloqueitems;
  var databoleto = '{"nombreoOperedor": "'+nombre+'","ci": '+ci+' ,"montoTotal": '+costoreg+', "datos_boleto":['+ bloqueitems+'],"datos_ciudadano": { "nombre": "'+nombre_institucion+'", "apellidop": "", "apellidom": "" ,"ci":'+nitreg+' ,"nacionalidad":"bolivianos"}}';
  databoleto = JSON.stringify(databoleto);
  var formData = {"identificador": 'SERVICIO_VALLE-290 ' ,"parametros"  : '{"xboleto_id_ciudadano":'+1+',"xboleto_data":'+databoleto+', "xboleto_usr_id":'+ usrid+'}'};  
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {    
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
         'authorization': 'Bearer ' + token,
       },
       success: function(dataIN) {
        swal( "Exito..!", "Se guardo correctamente los datos del grupo!", "success" );
            obtenerFacturagrupo();
        document.ready = document.getElementById("opcion").value = 'activo';

        darBajagrupo(idGrupo12);
        listar1(1,fechaActual);
   
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      }
    });
    },   
  });
});
function obtenerFacturagrupo(){ 
  var efectivo = efec;
  var ciudadano = razonsocial
  var ciciudadano = nitoci
  var bloque = bloqueItemgrupo ;
  var bloque2 = bloque;
  bloque2= '['+bloque2+ ']';
  bloque2=JSON.parse(bloque2);
  var det1='[';
  for(var i=0; i< bloque2.length; i++){
  det1 =det1 + '{ "concepto": "'+bloque2[i].items+'", "cantidad": '+bloque2[i].cantidad+', "monto": '+bloque2[i].montoTotalPorItem+' },';} 
  det1 = det1.substr(0,det1.length-1);
  det1= det1+ ']';
  var descripcionFac = 'Zoologico';
  var cantidadFac =4;
  var montoFac = 4;
  var nombreFac = ciudadano;
  var documentoFac =ciciudadano;
  var montototalFac = 4;
  fechahoy =new Date();                
  $.ajax({
    type: 'GET',
    url :  'prueba/VALLE/public/v0.0/getTokenFac',
                    data        : '',
                    success: function(token) { 
                   var det = '[ { "concepto": "Uso de Parqueo", "cantidad": 1, "monto":77 } ]';                      
                    var det = det1; 
                    var formData = '{ "factura": { "ci_nit": '+documentoFac+', "nombres": "'+nombreFac+'", "fecha": "'+fechaHoy+'", "idUsuario": 49, "idSucursal": 124, "efectivo":  '+efectivo+', "detalles": '+det+', "serialTarjeta": "", "fum":0 } }';
                      $.ajax({
                       type        : 'POST',            
                       url         : urlFactura,
                       data        : formData,
                       dataType    : 'json',
                       crossDomain : true,
                       headers: {
                         'authorization': 'Bearer '+token,
                         'Content-Type': 'application/json',
                       },success: function(dataIN) {                              
                              var urlFactura =  dataIN.data.url;  
                              window.open(urlFactura, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=1000");                            
                       },          
                       error: function (xhr, status, error) { }
                     });
                    },
                    error: function(result) {
                      swal( "Error..!", "No se puedo guardar los datos", "error" );
                    },
                  });
  
  };
function darBaja(id){
  swal({   
    title: "Esta seguro de eliminar la Grupo?",
    text: "Presione ok para eliminar el registro de la base de datos!",
    type: "warning",   showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Eliminar!",
    closeOnConfirm: false
  }, function(){
    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_VALLE-280',"parametros": '{"xgrupo_id":'+id+', "xgrupo_usr_id":'+1+'}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
           'authorization': 'Bearer '+ token,
         },
         success: function(data){
          swal("grupo!", "Fue eliminado correctamente!", "success");
          listar1(1,fechaActual);
        },
        error: function(result) {
          swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      },
    }); 
  });
};
function darBajagrupo(id){
    $.ajax({
      type        : 'GET',
      url         : urlToken,
      data        : '',
      success: function(token) {
        var formData = {"identificador": 'SERVICIO_VALLE-280',"parametros": '{"xgrupo_id":'+id+', "xgrupo_usr_id":'+1+'}'};
        $.ajax({
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: {
           'authorization': 'Bearer '+ token,
         },
         success: function(data){
          swal("grupo!", "Fue eliminado correctamente!", "success");
   
        },
        error: function(result) {
          swal("Error..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
        }
      });
      },
    }); 
  
};

function obtenerDatosidItemR(){
var datosItem = document.getElementById("items").value;
var cadena = datosItem.split(",");
var a = cadena[2];
$('#monto').val(a);
}
   $(function(){ 
      var cantidad=1;  
      $('#exe').click(function() {
        count++;
        var monto=$('#monto').val();
        var cantidad=$('#cantidad').val();
        if(monto!= 0 && cantidad>0){
        }
        if(monto!= 0 && cantidad>0){
          var combo = document.getElementById("items");
          var selectedItem = combo.options[combo.selectedIndex].text;
          console.log(selectedItem);
          var items=$('#items').val();
          var cadena = items.split(",");
          console.log(cadena);
          var a = cadena[1];
          items=a;
          var codigo=$('#items').val();
          var cadena2 = codigo.split(",");
          console.log(cadena2);
          var b = cadena[0];
          codigo=b;
          console.log(codigo);
          var cantidad=$('#cantidad').val();
          var monto=$('#monto').val();
          var montoTotalPorItem=monto*cantidad;
          monto=parseInt(monto);
          datosItem={items,codigo,monto,cantidad,montoTotalPorItem};
          datosItem=JSON.stringify(datosItem);         
          totalMonto=totalMonto + montoTotalPorItem;
          bloqueItem=(bloqueItem.concat(datosItem)).concat(',');
          $('#total').val(totalMonto);
          var liItem=$('<li>',{text:selectedItem},'<br>');
          var liMonto=$('<li>',{text:"Bs. "+monto});
          var liCantidad=$('<li>',{text:cantidad});
          var liMontoTotal=$('<li>',{text:"Bs. "+montoTotalPorItem});
          $('#items-ol').append(liItem);          
          $('#monto-ul').append(liMonto);
          $('#cantidad-ul').append(liCantidad);          
          $('#montoTotal-ul').append(liMontoTotal);      
        }else{
         swal("Opss..!", "Seleccione Items e ingrese la cantidad", "warning");
       }      
     });
    });

function guardar(){
var efectivo = $("#efectivoci" ).val();
    if( efectivo>totalMonto  ){
         calcula();
 var ciudadano = $("#nombre2" ).val();
  var apellidop = $("#apellidop2" ).val();
  var apellidom = $("#apellidom2" ).val();
  var ciciudadano = $("#ci2" ).val();
  var nacionalidadciudadano = $("#nacionalidad2" ).val();
  var nombre=$('#nombresUsuario').val();
  var ci=$('#identificacion').val();
  var bloque = bloqueItem ;
  var bloque2 = bloque.substr(0,bloque.length-1);
  var databoleto = '{"nombreoOperedor": "'+nombre+'","ci": '+ci+' ,"montoTotal": '+totalMonto+', "datos_boleto":['+ bloque2+'],"datos_ciudadano": { "nombre": "'+ciudadano+'", "apellidop": "'+apellidop+'", "apellidom": "'+apellidom+'" ,"ci":'+ciciudadano+' ,"nacionalidad":"'+nacionalidadciudadano+'"}}';
  databoleto = JSON.stringify(databoleto);
  var formData = {"identificador": 'SERVICIO_VALLE-290 ' ,"parametros"  : '{"xboleto_id_ciudadano":'+1+',"xboleto_data":'+databoleto+', "xboleto_usr_id":'+ 1+'}'};  
  $.ajax({
    type        : 'GET',
    url         : urlToken,
    data        : '',
    success: function(token) {    
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
         'authorization': 'Bearer ' + token,
       },
       success: function(dataIN) {
        swal( "Exito..!", "Se guardo correctamente los datos!", "success" );
       
        },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      }
    });
    },   
  }); 
        }
        else{
     swal("Opss..!", "El Monto no es Suficiente", "warning");

        }
}

function Limpiar(){
  $("#nombre2" ).val('');
  $("#apellidop2" ).val('');
  $("#apellidom2").val('');
  $("#ci2" ).val('');
  $('#items-ol').empty();
  $('#monto-ul').empty();
  $('#montoTotal-ul').empty();
  $('#cantidad-ul').empty();
  $("#total").val('');
  $("#efectivoci").val('');
  $("#cambio").val('');
  $('#cantidad').val('2');
  totalMonto=0;
  bloqueItem='';
};

function obtenerFactura(){ 

var efectivo = $("#efectivoci" ).val();
var ciudadano = $("#nombre2" ).val();
var apellidop = $("#apellidop2" ).val();
var apellidom = $("#apellidom2" ).val();
var ciciudadano = $("#ci2" ).val();
var nacionalidadciudadano = $("#nacionalidad2" ).val();
var bloque = bloqueItem ;
var bloque2 = bloque.substr(0,bloque.length-1);
  bloque2= '['+bloque2+ ']';
  console.log(bloque2.length);
  bloque2=JSON.parse(bloque2);
  var det1='[';
   console.log(bloque2.length,bloque2);
    for(var i=0; i< bloque2.length; i++){
      det1 =det1 + '{ "concepto": "'+bloque2[i].items+'", "cantidad": '+bloque2[i].cantidad+', "monto": '+bloque2[i].montoTotalPorItem+' },';} 
        det1 = det1.substr(0,det1.length-1);
        det1= det1+ ']';
    var descripcionFac = 'Zoologico';
    var cantidadFac =4;
    var montoFac = 4;
    var nombreFac = ciudadano;
    var documentoFac =ciciudadano;
    var montototalFac = 4;
                 fechahoy =new Date();                
                  $.ajax({
                    type        : 'GET',
                    url         :  'prueba/VALLE/public/v0.0/getTokenFac',
                    data        : '',
                    success: function(token) { 
                   var det = '[ { "concepto": "Uso de Parqueo", "cantidad": 1, "monto":77 } ]';                      
                    var det = det1; 
                    var formData = '{ "factura": { "ci_nit": '+documentoFac+', "nombres": "'+nombreFac+'", "fecha": "'+fechaHoy+'", "idUsuario": 49, "idSucursal": 124, "efectivo":  '+efectivo+', "detalles": '+det+', "serialTarjeta": "", "fum":0 } }';
                      $.ajax({
                       type        : 'POST',            
                       url         : urlFactura,
                       data        : formData,
                       dataType    : 'json',
                       crossDomain : true,
                       headers: {
                         'authorization': 'Bearer '+token,
                         'Content-Type': 'application/json',
                       },success: function(dataIN) {                              
                              var urlFactura =  dataIN.data.url;  
                              window.open(urlFactura, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=1000");
                              Limpiar();                            
                       },          
                       error: function (xhr, status, error) { }
                     });
                    },
                    error: function(result) {
                      swal( "Error..!", "No se puedo guardar los datos", "error" );
                    },
                  });
  
  };
function enviarDatos(){
  console.log(7777);
  var nombre=$('#nombresUsuario').val();
  var ci=$('#identificacion').val();
  var dat1 = {nombre,ci};
  dat1 = JSON.stringify(dat1);
  var urlInh = '{{ url("datosBoleto",[ "datos"=>"dat1"])}}';
  urlInh = urlInh.replace('dat1',dat1);
  $("#irUbicacion").attr("href", urlInh);
};


function ocultar(){
document.getElementById("formulario2").style="display:block;";
document.getElementById("formulario").style="display:block;";
document.getElementById("listadogrupo").style="display:none;";

};
function calcula(){
var efectivo = $("#efectivoci" ).val();
var cambio = efectivo-totalMonto

$("#cambio" ).val(cambio);
};

function mostrarGrupo(){

document.getElementById("listadogrupo").style="display:block;";
document.getElementById("formulario2").style="display:none;";
document.getElementById("formulario").style="display:none;";
 
 console.log("VERDADDA",v)


 

};


var paging_pagesize = $( "#reg" ).val();
var paging_pagenumbe = 1;
function btnAnterior(){paging_pagenumbe--;listar1( paging_pagenumbe ,fechaActual);}

function btnSiguiente(){paging_pagenumbe++;listar1(paging_pagenumbe,fechaActual);}

function listarSelect(){ paging_pagenumbe=1;listar1(paging_pagenumbe,fechaActual);
  };

//







$(document).ready(function(){

fechaActual = $("#fechaActual").val();
   listar1(1 ,fechaActual);
  cargarDatosUsuario();
  obtenerItemRecaudadorPorUnidadRecaudadora();
  var hoy = new Date();
var dia = hoy.getDate(); 
var mes = hoy.getMonth();
var anio= hoy.getFullYear();
  var fecha_actual = dia+"-"+mes+"-"+anio;
  
   console.log(fecha_actual ,dia,mes,anio,5555);


});
</script>
@endpush