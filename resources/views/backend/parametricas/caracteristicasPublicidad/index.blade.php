@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.caracteristicasPublicidad.partial.modalCreate')
@include('backend.parametricas.caracteristicasPublicidad.partial.modalUpdate')
<div class= "row">
   <div class= "col-md-12">
      <section class= "content-header">
       <div class= "header_title">
         <h3>      
            Caracteristicas Publicidad

         </h3>
       </div>
      </section>
   </div>
</div>
@include('backend.componentes.encabezadoPaginacion')
<br>
<div class= "row">
  <div class= "col-md-12">
    <div class= "box">
      <div class= "box-header with-border">
      </div>
      <div class= "box-body">
        <div id= "listCaracteristica">
        </div>  
      </div>


@endsection
@push('scripts')

<script>

var idusu = {{$usrid}};
var total = 0;
var opcion = '';

  function listar(paging_pagenumbe){
  htmlListaFac = '';
  htmlListaFac+='<div class="row">';
  htmlListaFac+='<div class="col-md-12" >';
  htmlListaFac+='<table id="lts-caracteristica" class="table table-striped" cellspacing="0" width="100%" >';
  htmlListaFac+='<thead>';
  htmlListaFac+='<tr>';
  htmlListaFac+='<th align="center">Numero</th>';
  htmlListaFac+='<th align="left">Opciones</th>';
  htmlListaFac+='<th align="left">Caracteristica</th>';
  htmlListaFac+='<th align="left">Indice</th>';
  htmlListaFac+='</tr>';
  htmlListaFac+='</thead>';

    var opcion=$( "#opcion" ).val();
    var paging_pagesize = $( "#reg" ).val();

    var formData = {"identificador":'SERVICIO_SIERRA-14',"parametros": '{"xtipolista":"'+opcion+'","paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumbe+'}'};

    $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {
        $.ajax({
         type        : 'POST',            
         url         : urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
           'authorization': 'Bearer' + token,
         },
         success: function(dataIN) {
          
          if (dataIN!="[{ }]") {
           var tam=dataIN.length;
         }                             
         for (var i = 0; i < tam; i++) 
         { 
          //var datosM = JSON.parse(datos[i].data);
          //var mercaderiaData = datosM.mercaderia_data;
          var estado_=JSON.parse(dataIN[i].data).caracteristica_publicidad_estado;
         
          htmlListaFac+='<tr><td align="left">'+(i+1)+'</td>';

          htmlListaFac+='<td class="form-nobajas"><button class="btncirculo btn-xs btn-primary btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar('+JSON.parse(dataIN[i].data).caracteristica_publicidad_id+', \''+JSON.parse(dataIN[i].data).caracteristica_publicidad_data.indice+'\',\''+JSON.parse(dataIN[i].data).caracteristica_publicidad_data.caracteristica+'\');" ><i class="glyphicon glyphicon-pencil"></i></button>  <button class="btncirculo btn-xs btn-danger btn-estado-'+estado_+'" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' +JSON.parse(dataIN[i].data).caracteristica_publicidad_id+');"><i class="glyphicon glyphicon-trash"></i></button></td>';

            htmlListaFac+='<td align="left">'+JSON.parse(dataIN[i].data).caracteristica_publicidad_data.caracteristica+'</td>'+
             
              '<td align="left">'+ JSON.parse(dataIN[i].data).caracteristica_publicidad_data.indice+'</td>'+

              '</tr>';

        }
        htmlListaFac +='</table></div></div>';
        htmlListaFac +='<div>';
        htmlListaFac +='<ul class="pager">';
        htmlListaFac +='<li><a href="#" onClick="btnAnterior()">Anterior</a></li>';
        htmlListaFac +='<li><a href="#" onClick="btnSiguiente()">Siguiente</a></li>';
        htmlListaFac +='</ul>';
        htmlListaFac +='</div>';


        $('#listCaracteristica').html(htmlListaFac);    
        bloquearBoton();
      },
      error: function (xhr, status, error) {
      }
    });
      },
      error: function(result) {
       swal( "Error..!", "No se puedo guardar los datos", "error" );
     },
   });  
  };

function bloquearBoton(){
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}

$( "#registrar" ).click(function() 
    {  
      var id1 = $( "#id1" ).val();
      var caracteristica1 = $( "#caracteristica1" ).val();
      var indice1= $( "#indice1" ).val();
      var data = '{"indice":"'+indice1+'","caracteristica":"'+caracteristica1+'"}';
      var jdata = JSON.stringify(data);
      var viausrid1 = idusu;
      
      var formData = {"identificador": ' SERVICIO_SIERRA-15',"parametros": '{"xcaracteristica_publicidad_data":'+jdata+',"xcaracteristica_publicidad_usr_id":'+viausrid1+'}'};
      $.ajax({
       type     : 'GET',
       url       :urlToken,
       data      :'',
       success: function(token){
        
        $.ajax({
         type        : 'POST',            
         url         : urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
           'authorization': 'Bearer' + token,
         },
         success: function(data){
          swal( "Exito..!", "Se Registro correctamente la Via...!", "success" );
          //$('#validacreate').data('bootstrapValidator').resetForm();
           //$( "#myCreate" ).modal('toggle');
           $( "#id1" ).val(''); 
           $( "#caracteristica1" ).val(''); 
           $( "#indice1" ).val(''); 
           
           document.ready = document.getElementById("opcion").value = 'activo';
           listar(1);
         },

         error: function(result) {  
           }
         });
      },
      
    });
   
  });

function editar(id, indice, caracteristica){
  $("#id2").val(id);
  $("#indice2").val(indice);
  $("#caracteristica2").val(caracteristica);
  
}

$( "#actualizar" ).click(function() {     
    var viausrid1 = idusu;
    var id2 = $( "#id2" ).val();
    var indice2 = $( "#indice2" ).val();
    var caracteristica2 = $( "#caracteristica2" ).val();
    var data = '{"indice":"'+indice2+'","caracteristica":"'+caracteristica2+'"}';
    var jdata = JSON.stringify(data);
     $.ajax({
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) {
     var formData = {"identificador": 'SERVICIO_SIERRA-16',"parametros": '{"xcaracteristica_publicidad_id":'+id2+',"xcaracteristica_publicidad_data":'+jdata+',"xcaracteristica_publicidad_usr_id":'+ viausrid1 +'}'};
      $.ajax({
         type        : 'POST',            
         url         :  urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: {
             'authorization': 'Bearer ' + token,
         },
         success: function(dataIN) {
            swal( "Exito..!", "Se modifico correctamente los datos!", "success" );
         },
         error: function(result) {
          swal( "Error..!", "No se puedo mofigicar los datos", "error" );
         }
        });
      },
      error: function(result) {
        swal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });
    listar(1);
});

function darBaja(id){

  var usuid= idusu;
  swal({   title: "Esta seguro de eliminar la Vía?",
   text: "Presione ok para eliminar el registro de la base de datos!",
   type: "warning",   showCancelButton: true,
   confirmButtonColor: "#DD6B55",
   confirmButtonText: "Si, Eliminar!",
   closeOnConfirm: false
 }, function()
 {
  
  $.ajax({
    type     : 'GET',
    url       :urlToken,
    data      :'',
    success: function(token){
      var formData = {"identificador": ' SERVICIO_SIERRA-17',"parametros": '{"xcaracteristica_publicidad_id":'+id+',"xcaracteristica_publicidad_usr_id":'+usuid+'}'};
      $.ajax({
        type        : 'POST',            
        url         : urlRegla,
        data        : formData,
        dataType    : 'json',
        crossDomain : true,
        headers: {
          'authorization': 'Bearer '+ token ,
        },
        success: function(data)
        {
          
          swal("Via!", "Fue eliminado correctamente!", "success");
        },
        error: function (result) 
        {
          swal("Error!", "No se puede eliminar los datos", "error"); 
        }
      });
    },
  });
  listar(1);
  });
  };

$(document).ready(function (){
  listar(1);
}); 

</script>    

<script type="text/javascript">
 $('.publicidad-validate').bootstrapValidator({
  message: 'This value is not valid',
  feedbackIcons: {
    valid: 'glyphicon glyphicon-ok',
    
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    caracteristica: {
      message: 'La caracteristica  es incorrecto',
      validators: {
        notEmpty: {
          message: 'Ingrese una caracteristica  válida'
        },
        regexp: {
          regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9/\s/a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/,
          message: 'No se aceptan carecteres especiales'
        },
        stringLength: {
          min: 1,
          message: 'Minimo 1 caracter'
        }
      }
    },
    indice: {
      message: 'El indice  es incorrecto',
      validators: {
        notEmpty: {
          message: 'Ingrese un indice válido'
        },
        stringLength: {
          min: 1,
          message: 'Minimo 1 caracter'
        }
      }
    },
  }
});

 $('.resetBtn').click(function() {
  $('.publicidad-validate').data('bootstrapValidator').resetForm(true);
});
</script>
@endpush
