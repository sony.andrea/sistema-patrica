<div class="modal fade modal-default" id="myUpdate" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical publicidad-validate','id'=>'dataForm'])!!}
      <div class="modal-header">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="row">
             <div class="col-md-12">
              <section class="content-header">
               <div class="header_title">
                <h4>
                 Editar datos Caracteristica Publicidad
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </h4>
            </div>
          </section>
        </div>   
      </div>
    </div>

    <div  style="padding:20px">
      <input type="hidden" class="id" name="id2" id="id2">
      <div class="col-md-12">
        <div class="form-group col-md-4">
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            <div class="form-group col-md-12">
             <label class="control-label">
              Caracteristica : 
            </label>  
            <input type="text" class="form-control" id="caracteristica2" name="caracteristica2">     
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group col-md-12">
           <label class="control-label">
            Indice : 
          </label>  
          <input type="text" class="form-control" id="indice2" name="indice2">     
        </div>
      </div>
    </div>
  </div>

  
  <div class="modal-footer">
    <button class="btn btn-primary" id="actualizar" data-dismiss="modal" type="button">
      <i class="glyphicon glyphicon-pencil"></i> Modificar
    </button>
  </div>  
</div>

</div>
</div>

{!! Form::close() !!}
</div>
</div>
</div>


