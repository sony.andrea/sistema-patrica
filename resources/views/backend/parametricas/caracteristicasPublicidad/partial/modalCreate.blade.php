<div class="modal fade modal-default" id="myCreate" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['class'=>'form-vertical publicidad-validate','id'=>'dataForm'])!!}
      <div class="modal-header">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="row">
             <div class="col-md-12">
              <section class="content-header">
               <div class="header_title">
                <h3>
                  Nueva Caracteristica Publicidad
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h3>
              </div>
            </section>
          </div>   
        </div>
      </div>

      <div  style="padding:20px">
        <input type="hidden" name="csrf-token" value=" {{ csrf_token() }}" id="token">     
        <input type="hidden" name="id1" id="id1">
        <div class="col-md-12">
          <div class="form-group col-md-4">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <div class="form-group col-md-12">
               <label class="control-label">
                Caracteristica : 
              </label>  
              <input type="text" class="form-control caracteristica"  name="caracteristica1" id="caracteristica1">     
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group col-md-12">
             <label class="control-label">
              Indice : 
            </label>  
            <input type="text" class="form-control indice"  name="indice1"  id="indice1">     
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal-footer">
     <button class="btn btn-default "  type="button" onClick="Limpiar()">
      <i class="fa fa-eraser"></i> Limpiar
    </button>
    <button class="btn btn-primary" id="registrar" data-dismiss="modal" type="button" >
      <i class="fa fa-save"></i> Guardar
    </button> 
  </div>   
</div>

</div>
</div>

{!! Form::close() !!}
</div>
</div>
</div>



