<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myCreate" tabindex="-5">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 container-fluit">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>
                     
                     Registrar Usuario Parqueo
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h4>
              </div>
              <div class="panel-body">
                <div class="caption">
                    {!! Form::open(['id' => 'mercaderiaform'])!!}
                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                        <input id="idUsuario" name="idUsuario" type="hidden" value="">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        
                                         <label class="control-label">Placa:</label>                                              
                                         <select  id="placa1" name="placa1"  class="form-control">
                                          <option value="">Seleccionar..</option>
                                        </select>
                                      
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>
                                            Hora Ingreso:
                                        </label>
                                        <span class="block input-icon input-icon-right">
                                            {!! Form::time('hora1', null, array('placeholder' => 'Ingrese Código','class' => 'form-control','id'=>'hora1')) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            </div>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>
                                            ipo Vehículo:
                                        </label>
                                        <span class="block input-icon input-icon-right">
                                            {!! Form::text('tvehiculo1', null, array('placeholder' => 'Ingrese Descripción','maxlength'=>'10','class' => 'form-control','id'=>'tvehiculo1')) !!}

                                        </span>
                                    </div>
                                </div>
                            </div>
                        
                        </div>   
                        </input>
                    </input>
                    {!! Form::close() !!}
                </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-default "  type="button" onClick="Limpiar()">
                    <i class="fa fa-eraser"></i> Limpiar
                </button>
                <button class="btn btn-primary" id="registrar" data-dismiss="modal" type="button" >
                    <i class="fa fa-save"></i> Guardar
                </button>    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 


    

