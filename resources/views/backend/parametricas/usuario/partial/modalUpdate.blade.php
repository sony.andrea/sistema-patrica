 
<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myUpdate" tabindex="-5">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 container-fluit">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>
                    Modificar Usuario Parqueo
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </h4>
              </div>
              <div class="panel-body">
                <div class="caption">
                <hr>
                     <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                    <input id="id_usuario2" name="id_usuario2"  type="hidden" value="">
                             <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        
                                        <label>
                                            Placa:
                                        </label>
                                        <input type="text" id="placa2" name="placa2"  class="form-control to-upper-case">
                                      
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>
                                            Nombre:
                                        </label>
                                        <input type="text" id="nombre2" name="nombre2"  class="form-control">
                                    </div>
                                </div>
                            </div>
                            </div>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                         <label>
                                            Documento:
                                        </label>
                                       <input type="nomber" id="documento2" name="documento2"  class="form-control">
                                    </div>
                                </div>
                            </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>
                                            Tipo Vehículo:
                                        </label>
                                     <select class= " form-control" placeholder = "Seleccionar Clase de Mercaderia" id = "tvehiculo2" name= "tvehiculo2">
                                                <option value= "-1">Seleccionar Tipo Vehículo</option>
                                                <option value= "automovil">Auto</option>
                                                <option value= "moto">Moto  </option>
                                                <option value= "municipal">Municipal  </option>
                                            </select>
                                    </div>
                                </div>
                            </div>
                        </div>  
                     </input>
                   </input>
                  </hr>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="modal-footer">
            <button class="btn btn-default" data-dismiss="modal" type="button">
            <i class="fa fa-close"></i>Cancelar
            </button>
            <button class="btn btn-primary" id="actualizar" data-dismiss="modal" type="button">
                <i class="glyphicon glyphicon-pencil"></i> Modificar
            </button>
        </div>
    </div>
  </div>
</div>

