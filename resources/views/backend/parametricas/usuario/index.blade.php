@extends('backend.template.app')
@section('main-content')
@include('backend.parametricas.usuario.partial.modalCreate')
@include('backend.parametricas.usuario.partial.modalUpdate')
<div class="row">
  <div class="col-md-12">
    <section class="content-header">
      <div class="header_title">
        <h3>      
          Listado de Usuarios Parqueo
          <small>
            <button class="btn btn-primary fa fa-plus pull-right" data-target="#myCreate" data-toggle="modal">&nbsp;Nuevo</button>
          </small>
        </h3>
      </div>
    </section>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-6">
  </div>
  <div class="col-md-3">
   <label >Mostrar Registros:</label> 
   <select class="form-control" id="reg" name="reg" onclick="listarSelect()">
    <option value=10>10</option>
    <option value=15>15</option>
    <option value=20>20</option>
    <option value=16145>16144</option>
   </select>
  </div>
<div class="col-md-3">
  <label >Listar por:</label> 
  <select class="form-control" id="opcion" name="opcion" onclick="listarSelect()">
    <option value="activo" selected="selected" > ACTIVOS</option>
    <option value="inactivo">INACTIVOS</option>
    <option value="todo">TODO</option>
  </select>
  </div>
</div>
<br><br>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
      </div>
      <div class="box-body">
        <div id="listUsuario">
        </div>  
      </div>
@endsection
@push('scripts')
<script>
var idusu = {{$usuarioid}};



function listar(paging_pagenumber)
{
  htmlLista ='';
  htmlLista+='<div class="row">';
  htmlLista+='<div class="col-md-12" >' ;
  htmlLista+='<table id="lts-listzone" class="table responsive table-striped" cellspacing="0" width="100%" >';
  htmlLista+='<thead>';
  htmlLista+='<tr>';
  htmlLista+='<th align="center">Nro.</th>';
  htmlLista+='<th align="center" class="form-nobajas-cabecera">Opciones</th>';
  htmlLista+='<th align="left">Placa</th>';
   htmlLista+='<th align="left">Nombre</th>';
    htmlLista+='<th align="left">Documento</th>';
     htmlLista+='<th align="left">Tipo Vehículo</th>';
  htmlLista+='</tr>';
  htmlLista+='</thead>';
  var opcion=$( "#opcion" ).val();
  var paging_pagesize = $( "#reg" ).val();
  var formData = {"identificador":'SERVICIO_VALLE-518',"parametros": '{"xtipolista":"'+opcion+'","paging_pagesize":'+paging_pagesize+',"paging_pagenumber":'+paging_pagenumber+'}'}; 
console.log('qqqqqqq',formData);
    $.ajax(
    {
      type        : 'GET',
      url         :  urlToken,
      data        : '',
      success: function(token) 
      { 
        $.ajax(
        {
          type        : 'POST',            
          url         : urlRegla,
          data        : formData,
          dataType    : 'json',
          crossDomain : true,
          headers: 
          {
           'authorization': 'Bearer' + token,
          },
          success: function(dataIN) 
          {
            if (dataIN!="[{ }]") 
            {
              var tam=dataIN.length;
            }                            
            for (var i = 0; i < tam; i++) 
            { 
              var estado=JSON.parse(dataIN[i].data).usuario_estado;
              htmlLista+='<tr><td align="left">'+(i+1)+'</td>';
              htmlLista+='<td class="form-nobajas"><button class="btncirculo btn-xs btn-primary btn-estado-'+estado+'" fa fa-plus-square pull-right" data-target="#myUpdate" data-toggle="modal" data-placement="top" title="Modificar" type="button" onClick= "editar('+JSON.parse(dataIN[i].data).usuario_id+', \''+JSON.parse(dataIN[i].data).usuario_data.placa+'\', \''+JSON.parse(dataIN[i].data).usuario_data.nombre+'\', \''+JSON.parse(dataIN[i].data).usuario_data.documento+'\', \''+JSON.parse(dataIN[i].data).usuario_data.tipo_vehiculo+'\');" ><i class="glyphicon glyphicon-pencil"></i></button>  <button class="btncirculo btn-xs btn-danger btn-estado-'+estado+'" fa fa-plus-square pull-right" data-toggle="modal" data-placement="top" title="Eliminar" type="button" onClick= "darBaja(' +JSON.parse(dataIN[i].data).usuario_id+ ');"><i class="glyphicon glyphicon-trash"></i></button></td>';
              htmlLista+='<td align="left">'+JSON.parse(dataIN[i].data).usuario_data.placa+'</td>';
               htmlLista+='<td align="left">'+JSON.parse(dataIN[i].data).usuario_data.nombre+'</td>';
                htmlLista+='<td align="left">'+JSON.parse(dataIN[i].data).usuario_data.documento+'</td>';
                 htmlLista+='<td align="left">'+JSON.parse(dataIN[i].data).usuario_data.tipo_vehiculo+'</td>';
            }
              htmlLista +='</table></div></div>';
              htmlLista +='<div>';
              htmlLista +='<ul class="pager">';
              htmlLista +='<li><a href="#" onClick="btnAnterior()">Anterior</a></li>';
              htmlLista +='<li><a href="#" onClick="btnSiguiente()">Siguiente</a></li>';
              htmlLista+='</ul>';
              htmlLista +='</div>';
              $('#listUsuario').html(htmlLista);    
              bloquearBoton();
          },
          error: function (xhr, status, error) { }
        });
      },
          error: function(result) 
          {
            swal( "Error..!", "No se puedo guardar los datos", "error" );
          },
    });  
};


function bloquearBoton()
{
  $(".btn-estado-inactivo").prop("disabled", true);
  $(".btn-estado-inactivo").css("opacity", '0.5');
}


$( "#registrar" ).click(function() 
 { 
    var opcion="activo";
    $.ajax(
    {
       type        : 'GET',
       url         : urlToken,
       data        : '',
      success: function(token) 
     {
      var placa1=$("#placa1").val();
      var nombre1 = $("#nombre1").val();
      var documento1 = $("#documento1").val();
      var tvehiculo1 = $("#tvehiculo1").val();

      var data2='{"placa":"'+placa1+'","nombre":"'+nombre1+'","documento":"'+documento1+'","tipo_vehiculo":"'+tvehiculo1+'"}';
      var jdata2 = JSON.stringify(data2);

      var formData = {"identificador":'SERVICIO_VALLE-524',"parametros": '{"xusuario_data":'+jdata2+', "xusuario_usr_id":'+idusu+'}'};
      console.log('INSERTARRRRRRR',formData);
       $.ajax(
       {
         type        : 'POST',            
         url         : urlRegla,
         data        : formData,
         dataType    : 'json',
         crossDomain : true,
         headers: 
          {
            'authorization': 'Bearer '+ token ,
          },
          success: function(dataIN)
          { 
            swal( "Exito..!", "Se Registro correctamente...!", "success" );
            listar(1);
         
          },
            error: function(result) 
          {
             swal( "Alerta..!", "Verifique que los campos esten llenados Gracias...!", "warning" );
              },
            }
           );
          },
         error: function(result) 
        {
        wal( "Error..!", "No se puedo guardar los datos", "error" );
      },
    });  
 });

function Limpiar()
{
  $("#placa1").val('');
  $("#nombre1").val('');
  $("#documento1").val('');
  $("#tvehiculo1").val('');
}


function editar(xusuario_id,placa2, nombre2,documento2,tvehiculo2)
{
  $("#id_usuario2").val(xusuario_id);
  $("#placa2").val(placa2); 
  $("#nombre2").val(nombre2);  
  $("#documento2").val(documento2);
  $("#tvehiculo2").val(tvehiculo2);  
}

//------------------------

$( "#actualizar" ).click(function() 
{ 
  var opcion="activo";
  $.ajax({
    type        : 'GET',
    url         :  urlToken,
    data        : '',
    success: function(token) { 
      var id_usuario2 = $("#id_usuario2").val();
      var placa2=$("#placa2").val();
      var nombre2 = $("#nombre2").val();
      var documento2 = $("#documento2").val();
      var tvehiculo2 = $("#tvehiculo2").val();

      var data2='{"placa":"'+placa2+'","nombre":"'+nombre2+'","documento":"'+documento2+'","tipo_vehiculo":"'+tvehiculo2+'"}';
      var jdata2 = JSON.stringify(data2);
      var formData = {"identificador":'SERVICIO_VALLE-525',"parametros": '{"xusuario_id":'+id_usuario2+',"xusuario_data":'+jdata2+', "xusuario_usr_id":'+idusu+'}'};
      
      $.ajax(
      {
       type        : 'POST',            
       url         : urlRegla,
       data        : formData,
       dataType    : 'json',
       crossDomain : true,
       headers: 
       {
         'authorization': 'Bearer '+ token,
       },

       success: function(data)
       {
          swal( "Exito..!", "Se Actualizo correctamente ..!", "success" );
          listar(1);
          $('#actividadUpdate').data('bootstrapValidator').resetForm();   
        },
      error: function(result) 
        {
          swal( "Alerta..!", "Verifique que los campos esten llenados Correctamente Gracias...!", "warning" );
        }
     });
        
    },
      error: function(result) {
      swal( "Error..!", "No se puedo guardar los datos", "error" );
    },
  });  
});



function darBaja(id){
  swal({   title: "Esta seguro de eliminar el Usuario Parqueo?",
      text: "Presione ok para eliminar el registro de la base de datos!",
      type: "warning",   showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si, Eliminar!",
      closeOnConfirm: false
    }, 
    function(){
      $.ajax({
        type        : 'GET',
        url         :  urlToken,
        data        : '',
        success: function(token) {
          var formData = {"identificador": 'SERVICIO_VALLE-526',"parametros": '{"xusuario_id":'+id+', "xusuario_usr_id":'+idusu+'}'};
          $.ajax({
            type        : 'POST',            
            url         : urlRegla,
            data        : formData,
            datosUype    : 'json',
            crossDomain : true,
            headers: {
              'authorization': 'Bearer '+ token,
            },
            success: function(data){
              //listar_mercado('altas',10,1);
              swal("Usuario Parqueo!", "Fue eliminado correctamente!", "success");
            },
            error: function(result) {
              swal("Opss..!", "Hubo algun problema al obtener los datos Gracias...!", "error")
            }
          });
        },
        
      });
      listar(1);  
    });
};


 $(".to-upper-case").on('focus keyup keypress blur change',function() {
    this.value = this.value.toLocaleUpperCase();
   });


$(document).ready(function (){
  listar(1);
  //listar_Combos();
}); 
</script>

<script >
   var paging_pagesize = $( "#reg" ).val();
 var paging_pagenumbe = 1;
 function btnAnterior(){
     paging_pagenumbe--;
   listar( paging_pagenumbe);
 }

 function btnSiguiente(){

  paging_pagenumbe++;
  listar(paging_pagenumbe);
  }

  function listarSelect(){
   paging_pagenumbe=1;
   listar(paging_pagenumbe);
  };
</script>
@endpush

